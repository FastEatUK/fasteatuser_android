package com.edelivery;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Patterns;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;


import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.ConfirmationData;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.UserDataResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;

import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.View;

import com.edelivery.adapter.ViewPagerAdapter;

import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LanguageHelper;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.internal.ImageRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.OAuthProvider;
import com.twitter.sdk.android.core.*;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Response;

import static com.edelivery.utils.Const.google.GOOGLE_SIGN_IN;

public class LoginActivity extends BaseAppCompatActivity implements LocationHelper
        .OnLocationReceived {

    public LocationHelper locationHelper;
    public LanguageHelper languageHelper;
    public ViewPagerAdapter adapter;
    public CallbackManager callbackManager;

    private TwitterAuthClient twitterAuthClient;

    private CustomFontEditTextView etLoginEmail, etLoginPassword;
    private CustomFontTextView tvForgotPassword, tvsigup;
    private CheckBox cbTcPolicy;
    private Button SiginButton;
    private ImageView btnFb, btnGoogle, btnTwitter, btnApple;
    private CustomDialogVerification forgotPassDialog;
    private TextInputLayout tilEmail;
    private LinearLayout llSocialLogin;
    ConfirmationData confirmationData;
    CustomFontTextView tvPolicy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

       /* FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);*/

        Utils.hideCustomProgressDialog();
        try {
//            logger = (AppEventsLogger) AppEventsLogger.newLogger(LoginActivity.this);

            if (preferenceHelper != null) {
                preferenceHelper.clearVerification();
                preferenceHelper.logout();
            }
            initTwitter();
            findViewById();
            setViewListener();
            setClickableString();
            if (getCallingActivity() == null && currentBooking != null) {
                currentBooking.clearCurrentBookingModel();
            }
            locationHelper = new LocationHelper(this);
            locationHelper.setLocationReceivedLister(this);
            languageHelper = new LanguageHelper(this);
            callbackManager = CallbackManager.Factory.create();
            twitterAuthClient = new TwitterAuthClient();
        } catch (Exception e) {
            Log.e("LoginActivity onCreate " , e.getMessage());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (locationHelper != null) {
            locationHelper.onStart();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (locationHelper != null) {
            locationHelper.onStop();
        }
    }

    @Override
    protected boolean isValidate() {
        String msg = null;
        if (TextUtils.equals(tilEmail.getHint().toString().trim(), getResources().getString(R.string
                .text_email_or_phone))) {
            if (Patterns.EMAIL_ADDRESS.matcher(etLoginEmail.getText().toString()).matches
                    ()) {
                msg = validatePassword();
            } else if (Patterns.PHONE.matcher(etLoginEmail.getText().toString().trim()).matches
                    ()) {
                msg = validatePassword();
            } else {
                msg = getString(R.string.msg_please_enter_valid_email_or_phone);
                etLoginEmail.setError(msg);
                etLoginEmail.requestFocus();
            }

        } else if (TextUtils.equals(tilEmail.getHint().toString().trim(), getResources().getString(R
                .string.text_phone))) {
            if (!Patterns.PHONE.matcher(etLoginEmail.getText().toString().trim()).matches
                    ()) {
                msg = getString(R.string.msg_please_enter_valid_phone);
                etLoginEmail.setError(msg);
                etLoginEmail.requestFocus();
            } else {
                msg = validatePassword();
            }
        } else if (TextUtils.equals(tilEmail.getHint().toString().trim(), getResources().getString(R
                .string.text_email))) {
            if (!Patterns.EMAIL_ADDRESS.matcher(etLoginEmail.getText().toString().trim()).matches
                    ()) {
                msg = getString(R.string.msg_please_enter_valid_email);
                etLoginEmail.setError(msg);
                etLoginEmail.requestFocus();
            } else {
                msg = validatePassword();
            }

        }
        if (!cbTcPolicy.isChecked()) {
            msg = getResources().getString(R.string.msg_plz_accept_tc);
            Utils.showToast(msg, LoginActivity.this);
            return false;
        } else {
            return TextUtils.isEmpty(msg);
        }
    }

    @Override
    protected void findViewById() {
        etLoginEmail = findViewById(R.id.etLoginEmail);
        etLoginPassword = findViewById(R.id.etLoginPassword);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        cbTcPolicy = findViewById(R.id.cbTcPolicy);
        tvPolicy = findViewById(R.id.tvPolicy);
        SiginButton = findViewById(R.id.btn_signin);
        btnFb = findViewById(R.id.btnFb);
        btnGoogle = findViewById(R.id.btnGoogle);
        btnTwitter = findViewById(R.id.btnTwitter);
        tvsigup = findViewById(R.id.tv_signup);
        tilEmail = findViewById(R.id.tilEmail);
        llSocialLogin = findViewById(R.id.llSocialButton);
        btnApple = findViewById(R.id.btnApple);

    }

    @Override
    protected void setViewListener() {
        enabledLoginBy();
        tvForgotPassword.setOnClickListener(this);
        btnFb.setOnClickListener(this);
        btnGoogle.setOnClickListener(this);
        btnTwitter.setOnClickListener(this);
        btnApple.setOnClickListener(this);
        tvsigup.setOnClickListener(this);
        SiginButton.setOnClickListener(this);
        //do something
    }

    @Override
    protected void onBackNavigation() {
        //do something

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GOOGLE_SIGN_IN:
                // result from google
                getGoogleSignInResult(data);
                break;

            case Const.LOGIN_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    setResult(Activity.RESULT_OK);
                    finish();
                }
                break;
        }
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (twitterAuthClient != null) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onClick(View view) {
        //do something
        switch (view.getId()) {
            case R.id.btn_signin:
                Log.i("", "ok");
                if (isValidate()) {


                    login("");
                }
                break;
            case R.id.tvForgotPassword:
                openForgotPasswordDialog();
                break;

            case R.id.btnFb:
                loginWithFacebook();
                break;
            case R.id.btnGoogle:
                loginWithGoogle();
                break;
            case R.id.btnTwitter:
                loginWithTwitter();
                break;

            case R.id.btnApple:
                LoginWithApple();
                break;

            case R.id.tv_signup:
                if (getCallingActivity() != null) {
                    Log.i("LoginActivityget", "from checkout goint registration screen");
                    Intent intent = new Intent(this, RegistrationActivity.class);
                    startActivityForResult(intent, Const.LOGIN_REQUEST);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    Intent intent = new Intent(this, RegistrationActivity.class);
                    startActivity(intent);

                }

            default:
                // do with default
                break;
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        //Do som thing

    }

    @Override
    public void onConnected(Bundle bundle) {
        //Do som thing

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //Do som thing

    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do som thing
    }

    @Override
    public void onBackPressed() {
        if (getCallingActivity() == null) {
            goToHomeActivity();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

    private void initTwitter() {
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(getResources().getString(R.string
                        .TWITTER_CONSUMER_KEY),
                        getResources().getString(R.string.TWITTER_CONSUMER_SECRET)))
                .debug(BuildConfig.DEBUG)
                .build();
        Twitter.initialize(config);
    }

    /**
     * Login with Twitter
     */
    public void loginWithTwitter() {

        try {
            TwitterCore.getInstance().getSessionManager().clearActiveSession();
            if (twitterAuthClient != null) {
                twitterAuthClient.authorize(LoginActivity.this, new com.twitter.sdk.android.core
                        .Callback<TwitterSession>
                        () {


                    @Override
                    public void success(final Result<TwitterSession> result1) {
                        AppLog.Log("TWITTER_LOGIN", "success");


                        final TwitterSession twitterSession = result1.data;
                        //  fetchTwitterEmail(twitterSession);

                        twitterAuthClient.requestEmail(twitterSession, new Callback<String>() {
                            @Override
                            public void success(Result<String> result) {
                                //here it will give u only email and rest of other information u can get from TwitterSession
                                Log.d("response_final", "User Id : " + twitterSession.getUserId() + "\nScreen Name : " + twitterSession.getUserName() + "\nEmail Id : " + result.data);
                            }

                            @Override
                            public void failure(TwitterException exception) {
                                Toast.makeText(LoginActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
                            }
                        });


                        Utils.showCustomProgressDialog(LoginActivity.this, false);
                        twitterAuthClient.requestEmail(result1.data, new com.twitter.sdk.android.core
                                .Callback<String>() {

                            @Override
                            public void success(Result<String> result2) {
                                Utils.hideCustomProgressDialog();

                                /*   if (isLoginTabSelect()) {*/
                                login(String.valueOf(result1.data.getUserId()));
                               /* } else {

                                    registerFragment.updateUiForSocialLogin(result2.data,
                                            String.valueOf(result1.data.getUserId()),
                                            result1.data.getUserName(), "", null);
                                }*/
                            }

                            @Override
                            public void failure(TwitterException exception) {
                                Utils.hideCustomProgressDialog();
                                AppLog.handleException("TWITTER_LOGIN", exception);
                            }
                        });
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        Utils.hideCustomProgressDialog();
                        AppLog.handleException("TWITTER_LOGIN", exception);
                    }
                });
            }
        } catch (Exception e) {
            Log.e("loginWithTwitter() " , e.getMessage());
        }
    }

    /**
     * Login with Facebook
     */
    public void loginWithFacebook() {
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        /*    if (isLoginTabSelect()) {*/
                        login(loginResult.getAccessToken().getUserId());
                     /*   } else {
                            getFacebookSignInResult(loginResult);
                        }*/
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        AppLog.Log("FB_RESULT", "failed");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        AppLog.Log("FB_RESULT", exception.getMessage());
                    }
                });
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(Const.Facebook
                .PUBLIC_PROFILE, Const.Facebook.EMAIL));
    }

    /**
     * Login with Google
     */
    public void loginWithGoogle() {
        Auth.GoogleSignInApi.revokeAccess(locationHelper.googleApiClient)
                .setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                AppLog.Log("REVOKE_ACCESS_GOOGLE", status.toString());
                                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent
                                        (locationHelper.googleApiClient);
                                startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
                            }
                        });
    }

   /* private void getFacebookSignInResult(LoginResult loginResult) {
        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject json_object,
                            GraphResponse response) {
                        try {
                            Profile profile = Profile.getCurrentProfile();
                            if (profile != null) {
                                registerFragment.updateUiForSocialLogin(json_object.getString(Const
                                                .Facebook.EMAIL),
                                        profile.getId(), profile.getFirstName(), profile.getLastName(), ImageRequest.getProfilePictureUri
                                                (profile.getId(), 250,
                                                        250));

                            }
                        } catch (JSONException e) {
                            AppLog.handleException(Const.Tag.REGISTER_FRAGMENT, e);
                        }

                    }

                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }*/

    public void LoginWithApple() {

        String TAG = "apple";
        OAuthProvider.Builder provider = OAuthProvider.newBuilder("apple.com");
        List<String> scopes =
                new ArrayList<String>() {
                    {
                        add("email");
                        add("name");
                    }
                };
        provider.setScopes(scopes);
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        Task<AuthResult> pending = firebaseAuth.getPendingAuthResult();
        if (pending != null) {
            pending.addOnSuccessListener(authResult -> {
                Log.e(TAG, "checkPending:onSuccess:" + authResult.getUser().getDisplayName());
// Get the user profile with authResult.getUser() and
// authResult.getAdditionalUserInfo(), and the ID
// token from Apple with authResult.getCredential().
                login(authResult.getUser().getUid());
                //  updateUiForSocialLogin(authResult.getUser().getEmail(),authResult.getUser().getUid(),authResult.getUser().getDisplayName(),"",null);
            }).addOnFailureListener(e -> Log.e(TAG, "checkPending:onFailure", e));
        } else {
            Log.d(TAG, "pending: null");

            firebaseAuth.startActivityForSignInWithProvider(this, provider.build())
                    .addOnSuccessListener(
                            authResult -> {
// Sign-in successful!
                                Log.e(TAG, "activitySignIn:onSuccess:" + authResult.getUser());
                                FirebaseUser user = authResult.getUser();
                                Log.e(TAG, "user.getDisplayName()" + user.getDisplayName());
                                Log.e(TAG, "user.getEmail()" + user.getEmail());
                                Log.e(TAG, "user.getuserid()" + user.getUid());
                                // updateUiForSocialLogin(user.getEmail(),user.getUid(),user.getDisplayName(),"",null);
                                login(user.getUid());
                            })
                    .addOnFailureListener(
                            e -> Log.e(TAG, "activitySignIn:onFailure", e));
        }
    }

    private void getGoogleSignInResult(Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent
                (data);
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            /*  if (isLoginTabSelect()) {*/
            login(acct.getId());
            /*} else {
                if (acct != null) {
                    String firstName;
                    String lastName = "";
                    if (acct.getDisplayName().contains(" ")) {
                        String[] strings = acct.getDisplayName().split(" ");
                        firstName = strings[0];
                        lastName = strings[1];
                    } else {
                        firstName = acct.getDisplayName();
                    }

                    registerFragment.updateUiForSocialLogin(acct.getEmail(), acct.getId(),
                            firstName,
                            lastName, acct
                                    .getPhotoUrl());
                }
            }*/


        } else {
            // Signed out, show unauthenticated UI.
            AppLog.Log("GOOGLE_RESULT", "failed");
        }
    }


    public void login(String socialId) {

        JSONObject jsonObject = new JSONObject();
        try {
            if (TextUtils.isEmpty(socialId)) {

                jsonObject.put(Const.Params.EMAIL, etLoginEmail.getText().toString());
                jsonObject.put(Const.Params.PASS_WORD, etLoginPassword.getText().toString());
                jsonObject.put(Const.Params.SOCIAL_ID, socialId);
                jsonObject.put(Const.Params.LOGIN_BY, Const.MANUAL);
            } else {
                jsonObject.put(Const.Params.EMAIL, "");
                jsonObject.put(Const.Params.PASS_WORD, "");
                jsonObject.put(Const.Params.SOCIAL_ID, socialId);
                jsonObject.put(Const.Params.LOGIN_BY, Const.SOCIAL);
            }
            jsonObject.put(Const.Params.CART_UNIQUE_TOKEN, preferenceHelper.getAndroidId());
            jsonObject.put(Const.Params.DEVICE_TYPE, Const.ANDROID);
            jsonObject.put(Const.Params.DEVICE_TOKEN, preferenceHelper
                    .getDeviceToken());
            jsonObject.put(Const.Params.APP_VERSION, getAppVersion());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.LOG_IN_FRAGMENT, e);
        }

        Log.i("login_parameters", jsonObject.toString());
        Utils.showCustomProgressDialog(LoginActivity.this, false);
        ApiInterface loginInterface = ApiClient.getClient().create(ApiInterface.class);
        final Call<UserDataResponse> login = loginInterface.login(ApiClient.makeJSONRequestBody
                (jsonObject));
        login.enqueue(new retrofit2.Callback<UserDataResponse>() {
            @Override
            public void onResponse(Call<UserDataResponse> call, Response<UserDataResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                AppLog.Log("login_responce", ApiClient.JSONResponse(response.body()));
                if (response.body() != null) {
                    if (response.body().getUser() != null) {


                        if (response.body().getUser().isIsPhoneNumberVerified()) {
                            if (parseContent.parseUserStorageData(response)) {
                                Utils.showMessageToast(response.body().getMessage(), LoginActivity.this);
                                CurrentBooking.getInstance().setBookCityId("");
                                //loginActivity.goToHomeActivity();

                                if (getCallingActivity() == null) {
                                    goToHomeActivity();
                                } else {
                                    setResult(Activity.RESULT_OK);
                                    finish();
                                }
                            }

                        } else {
                            confirmationData = new ConfirmationData();
                            confirmationData.setMobileNo(response.body().getUser().getPhone());
                            confirmationData.setUser_id(response.body().getUser().getId());
                            confirmationData.setContrycode(response.body().getUser().getCountryPhoneCode());
                            GoforConfirmation(confirmationData);
                        }
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), LoginActivity.this);
                    }

                }

            }

            @Override
            public void onFailure(Call<UserDataResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.LOG_IN_FRAGMENT, t);
            }
        });
    }

    private void openForgotPasswordDialog() {
        if (forgotPassDialog != null && forgotPassDialog.isShowing()) {
            return;
        }

        forgotPassDialog = new CustomDialogVerification
                (LoginActivity.this, getString(R.string.text_forgot_password),
                        getString(R.string.msg_forgot_password),
                        getString(R.string.text_cancel),
                        getString(R.string.text_ok), null,
                        getString(R.string
                                .text_email), false, InputType.TYPE_CLASS_TEXT, InputType
                        .TYPE_TEXT_VARIATION_EMAIL_ADDRESS) {
            @Override
            public void onClickLeftButton() {
                dismiss();
            }

            @Override
            public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                           CustomFontEditTextView etDialogEditTextTwo) {

                String email = etDialogEditTextTwo.getText().toString();
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches
                        ()) {
                    etDialogEditTextTwo.setError(getString(R.string
                            .msg_please_enter_valid_email));
                } else {
                    forgotPassword(email);
                    dismiss();
                }


            }
        };
        forgotPassDialog.show();
    }

    private void forgotPassword(String email) {
        Utils.showCustomProgressDialog(LoginActivity.this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.TYPE, Const.Type.USER);
            jsonObject.put(Const.Params.EMAIL, email);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.LOG_IN_FRAGMENT, e);
        }
        Log.i("forgotPassword", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.forgotPassword(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new retrofit2.Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Log.i("forgotPassword", jsonObject.toString());
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response)) {

                    if (response.body().isSuccess()) {
                        Utils.showMessageToast(response.body().getMessage(), LoginActivity.this);
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), LoginActivity.this);
                    }

                }

            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.LOG_IN_FRAGMENT, t);
            }
        });


    }

    private void enabledLoginBy() {
        if (preferenceHelper.getIsLoginByEmail() && preferenceHelper
                .getIsLoginByPhone()) {
            tilEmail.setHint(getResources().getString(R.string.text_email_or_phone));
        } else if (preferenceHelper.getIsLoginByPhone()) {
            tilEmail.setHint(getResources().getString(R.string.text_phone));
        } else {
            tilEmail.setHint(getResources().getString(R.string.text_email));
        }
    }

    private String validatePassword() {
        String msg = null;
        if (TextUtils.isEmpty(etLoginPassword.getText().toString())) {
            msg = getString(R.string.msg_enter_password);
            etLoginPassword.setError(msg);
            etLoginPassword.requestFocus();
        } else if (etLoginPassword.getText().toString().length() < 6) {
            msg = getString(R.string.msg_please_enter_valid_password);
            etLoginPassword.setError(msg);
            etLoginPassword.requestFocus();
        }
        return msg;
    }

    private void checkSocialLoginISOn(boolean isSocialLogin) {
        if (isSocialLogin) {
            llSocialLogin.setVisibility(View.VISIBLE);
        } else {
            llSocialLogin.setVisibility(View.GONE);
        }
    }

    public void setClickableString() {
        SpannableString SpanString = new SpannableString(
                getString(R.string.txt_sign_in_privacy));

        ClickableSpan

                teremsAndCondition = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent mIntent = new Intent(LoginActivity.this, WebViewActivity.class);
                mIntent.putExtra("web", "terms");
                startActivity(mIntent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.linkColor = ContextCompat.getColor(getApplicationContext(), R.color.color_white);
                ds.setUnderlineText(false);
            }

        };

// Character starting from 32 - 45 is Terms and condition.
// Character starting from 49 - 63 is privacy policy.

        ClickableSpan privacy = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

                Intent mIntent = new Intent(LoginActivity.this, WebViewActivity.class);
                mIntent.putExtra("web", "privacy");
                startActivity(mIntent);

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.linkColor = ContextCompat.getColor(getApplicationContext(), R.color.color_white);
                ds.setUnderlineText(false);
            }
        };

        int start_terms = getString(R.string.txt_sign_in_privacy).indexOf(getString(R.string.text_t_and_c));
        int end_terms = getString(R.string.txt_sign_in_privacy).indexOf(getString(R.string.text_t_and_c)) + getString(R.string.text_t_and_c).length();
        int start_privacy = getString(R.string.txt_sign_in_privacy).indexOf(getString(R.string.text_policy));
        int end_privacy = getString(R.string.txt_sign_in_privacy).indexOf(getString(R.string.text_policy)) + getString(R.string.text_policy).length();
        Log.i("setClickableString", "start_terms" + start_terms + ", end_terms" + end_terms);
        SpanString.setSpan(teremsAndCondition, start_terms, end_terms, 0);
        SpanString.setSpan(privacy, start_privacy, end_privacy, 0);
        SpanString.setSpan(new UnderlineSpan(), start_terms, end_terms, 0);
        SpanString.setSpan(new UnderlineSpan(), start_privacy, end_privacy, 0);
        SpanString.setSpan(new StyleSpan(Typeface.BOLD), start_terms, end_terms, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        SpanString.setSpan(new StyleSpan(Typeface.BOLD), start_privacy, end_privacy, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        tvPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        tvPolicy.setText(SpanString, TextView.BufferType.SPANNABLE);
        tvPolicy.setHighlightColor(Color.TRANSPARENT);
        tvPolicy.setSelected(true);
    }

}
