package com.edelivery;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.Massdialgue;
import com.edelivery.fragments.HomeFragment;
import com.edelivery.fragments.StoresFragment;
import com.edelivery.fragments.UserFragment;
import com.edelivery.models.datamodels.ConfirmationData;
import com.edelivery.models.datamodels.Deliveries;
import com.edelivery.models.hereresponce.HereDistanceResponce;
import com.edelivery.models.mapboxdirectionmatrix.DirectionMatrixResponse;
import com.edelivery.models.responsemodels.*;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.service.LocationTrack;
import com.edelivery.utils.*;
import com.freshchat.consumer.sdk.Freshchat;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.here.android.mpa.common.ApplicationContext;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.GeocodeRequest;
import com.here.android.mpa.search.GeocodeResult;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.ReverseGeocodeRequest;
import com.livechatinc.inappchat.ChatWindowConfiguration;
import com.livechatinc.inappchat.ChatWindowErrorType;
import com.livechatinc.inappchat.ChatWindowView;
import com.livechatinc.inappchat.models.NewMessageModel;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.MapboxDirections;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.geojson.Point;

import io.supercharge.shimmerlayout.ShimmerLayout;
import okhttp3.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import android.os.Handler;

import static com.edelivery.utils.Const.LOGIN_REQUEST;
import static com.edelivery.utils.Const.REQUEST_CHECK_SETTINGS;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.os.Bundle;

public class HomeActivity extends BaseAppCompatActivity implements LocationHelper
        .OnLocationReceived {


    private LocationHelper locationHelper;
    private CustomDialogVerification customDialogVerification;
    private Dialog dialogEmailOrPhoneVerification;
    private CustomFontEditTextView etDialogEditTextOne, etDialogEditTextTwo;
    private OtpResponse otpResponse;
    private String phone, email, currentAddress;
    private CustomDialogAlert exitDialog;
    private Menu bottomNavigationMenu;
    private BottomNavigationView bottomNavigationView;
    public Location currentLocation, favLocation;
    public ImageView btnFreshchatFab;
    private boolean initComplete = false;
    private boolean Isusernotchecked = false;
    public LinearLayout ivEmpty;
    public LocationTrack locationTrack;
    public String str_country, str_country_Code, str_arealvel_1, str_locality, str_arealvel_2;
    private String distance;
    GeoCoordinate origin, desti;
    Handler handler = new Handler();
    Runnable runnable;
    private int selectedTab = 0;
    int delay = 5 * 1000; //Delay for 15 seconds.  One second = 1000 milliseconds.
    //skeleton
    public LinearLayout skeletonLayout;
    public ShimmerLayout shimmer;
    public LayoutInflater inflater;

    ChatWindowView emmbeddedChatWindow;
    com.livechatinc.inappchat.ChatWindowConfiguration configuration;
    private Object ChatWindowEventsListener;
    String paramName = "", paramEmail = "";
    Boolean chatStart = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/ClanPro-News.otf");
        setContentView(R.layout.activity_home);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        // Utils.showCustomProgressDialogForHome(this, false); //hide progressDialog in initBootomBar method
        // Use this line to hide progressdilaog : Utils.hideCustomProgressDialogForHome(); //also use this in home and storefragment


        initLiveChatData();

        setShimmerEffect();
        CheckverifiedUser();
        initBottomBar();
        /*getLatlngForAPI();*/
        initToolBar();
        findViewById();
        setViewListener();
        initLocationHelper();
        IsToolbarNavigationVisible(false);

        // swipeFragmentAccordingToDeliveries();


        //goToHomeFragmentDifferentTransition();
        favLocation = new Location("");


    }

    public void initLiveChatData() {
        configureChatWindow();
        initializeAndImplementLiveChat();  //initialize chat
    }

    private void getLatlngForAPI() {
        locationTrack = new LocationTrack(this);
        if (locationTrack.canGetLocation()) {
            if (String.valueOf(locationTrack.getLatitude()) != "0.0") {
                Double paramLongitude = locationTrack.getLongitude();
                Double paramLatitude = locationTrack.getLatitude();


                getGeocodeDataFromLocation(new LatLng(locationTrack.getLatitude(), locationTrack.getLongitude()));
               /* callDeliverystorelist();
                Log.e("latlng_home", paramLatitude + "name" + paramLongitude)*/
            }
        } else {
            // locationTrack.showSettingsAlert()
        }
    }

   /* private void callDeliverystorelist() {


        if (location.getCoordinate() != null) {
            latLng = new LatLng(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude());
        }

        if (!location.getAddress().getCountryName().isEmpty()) {
            str_country = location.getAddress().getCountryName();
        }
        if (!location.getAddress().getCountryCode().isEmpty()) {
            str_country_Code = location.getAddress().getCountryCode();
        }
        if (!location.getAddress().getCity().isEmpty()) {
            str_locality = location.getAddress().getCity();
        }

        if (isCurrentLogin()) {
            if (preferenceHelper.getIsPhoneNumberVerified()) {
                getDeliveryStoreInCity(str_country, str_country_Code, str_locality, "",
                        "", latLng, myPlace, str_locality);
            }
        } else if (!isCurrentLogin()) {

            getDeliveryStoreInCity(str_country, str_country_Code, str_locality, "",
                    "", latLng, myPlace, str_locality);
        }
    }*/

    public void setShimmerEffect() {

        skeletonLayout = findViewById(R.id.skeletonLayout);
        shimmer = findViewById(R.id.shimmerSkeleton);
        this.inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        showSkeleton(true);

    }

    public void animateReplaceSkeleton(View listView) {

        listView.setVisibility(View.VISIBLE);
        listView.setAlpha(0f);
        listView.animate().alpha(1f).setDuration(8000).start();

       /* skeletonLayout.animate().alpha(0f).setDuration(1000).withEndAction(new Runnable() {
            @Override
            public void run() {
                showSkeleton(false);
            }
        }).start();*/

    }

    public int getSkeletonRowCount(Context context) {
        int pxHeight = getDeviceHeight(context);
        int skeletonRowHeight = (int) getResources()
                .getDimension(R.dimen.row_layout_height); //converts to pixel
        return (int) Math.ceil(pxHeight / skeletonRowHeight);
    }

    public int getDeviceHeight(Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return metrics.heightPixels;
    }

    public void showSkeleton(boolean show) {

        if (show) {

            skeletonLayout.removeAllViews();

            int skeletonRows = getSkeletonRowCount(getActivity());
            for (int i = 0; i <= skeletonRows; i++) {
                ViewGroup rowLayout = (ViewGroup) inflater
                        .inflate(R.layout.skeleton_row_layout, null);
                skeletonLayout.addView(rowLayout);
            }
            shimmer.setVisibility(View.VISIBLE);
            shimmer.startShimmerAnimation();
            skeletonLayout.setVisibility(View.VISIBLE);
            skeletonLayout.bringToFront();

            animateReplaceSkeleton(shimmer);
        } else {
            shimmer.stopShimmerAnimation();
            shimmer.setVisibility(View.GONE);
        }
    }


    public void callSwingAnimationChat() {
        callChatAnimation();
        handler.postDelayed(runnable = new Runnable() {
            public void run() {

                callChatAnimation();
                handler.postDelayed(runnable, delay);
            }
        }, delay);


    }

    public void callChatAnimation() {
        if (selectedTab == 0) {
            Animation animSlide = AnimationUtils.loadAnimation(getApplicationContext(),
                    R.anim.slide);
            btnFreshchatFab.startAnimation(animSlide);
        }

    }

    void pendingpopupshow(String message) {
        if (!preferenceHelper.getpopmassage().equals("")) {
            massdialgue = new Massdialgue(HomeActivity.this, message) {
                @Override
                public void onClick(View v) {
                    super.onClick(v);
                    switch (v.getId()) {
                        case R.id.iv_close:
                            preferenceHelper.putpopupmassage("");
                            dismiss();
                            break;
                    }
                }
            };
            massdialgue.show();
            preferenceHelper.putisshowpopup(false);
            preferenceHelper.putpopupmassage("");
        }

    }


    private void initLocationHelper() {
        locationHelper = new LocationHelper(this);
        locationHelper.setLocationReceivedLister(this);
        if (locationHelper != null) {
            locationHelper.onStart();
        }
        // updateUIBottomNavigationView(isCurrentLogin());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (!isCurrentLogin() && bottomNavigationMenu != null && bottomNavigationMenu.findItem(R.id.action_home) != null) {
            //btnFreshchatFab.setVisibility(View.VISIBLE);
            bottomNavigationMenu.findItem(R.id.action_home).setChecked(true);
        }
        // updateUIBottomNavigationView(isCurrentLogin());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (locationHelper != null) {
            locationHelper.onStart();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (locationHelper != null) {
            locationHelper.onStop();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        emmbeddedChatWindow.onActivityResult(requestCode, resultCode, data);
        Log.e("ActivityResult", "ActivityResult");


        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        initComplete = true;
                        locationHelper.startLocationUpdate();


                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("startLocationUpdate()", "no");
                        // The user was asked to change settings, but chose not to
                        finishAffinity();
                        break;
                    default:
                        break;
                }
                break;
            case LOGIN_REQUEST:
                updateUIBottomNavigationView(isCurrentLogin());
                Log.i("updateUIBottomNavigationView", "LOGIN_REQUEST");
                bottomNavigationMenu.findItem(R.id.action_home).setChecked(true);
                break;
        }
    }


    @Override
    protected boolean isValidate() {
        return false;
    }


    @Override
    protected void findViewById() {
        initFreshchat();

        btnFreshchatFab = findViewById(R.id.btn_freshchat_fab);
        ivEmpty = findViewById(R.id.ivEmpty);
        btnFreshchatFab.setOnClickListener(this);
        //do something

    }

    @Override
    protected void setViewListener() {
        //do something
    }

    @Override
    protected void onBackNavigation() {
        //do something
    }


    private void initBottomBar() {
        bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);

        bottomNavigationMenu = bottomNavigationView.getMenu();
        if (bottomNavigationView != null) {
            bottomNavigationView.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_home:
                                    selectedTab = 0;
                                    //ivEmpty.setVisibility(View.GONE);

                                    toolbar.setVisibility(View.GONE);
                                    setTitleOnToolBar("");
                                    btnFreshchatFab.setVisibility(View.VISIBLE);
                                    Isusernotchecked = false;
                                    swipeFragmentAccordingToDeliveries();

                                    Log.i("swipeFragment", "action_home");
                                    break;

                                case R.id.action_basket:
                                    selectedTab = 1;
                                    // toolbar.setVisibility(View.VISIBLE);
                                    btnFreshchatFab.setVisibility(View.GONE);
                                    goToCartActivity();
                                    break;

                                case R.id.action_orders:
                                    selectedTab = 2;
                                    toolbar.setVisibility(View.VISIBLE);
                                    btnFreshchatFab.setVisibility(View.GONE);
                                    goToOrdersActivity();
                                    break;

                                case R.id.action_user:
                                    selectedTab = 3;
                                    Isusernotchecked = true;
                                    btnFreshchatFab.setVisibility(View.GONE);
                                    if (isCurrentLogin()) {
                                        goToUserFragment();
                                    } else {
                                        Intent i = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(i);
                                        /*  goToLoginActivityForResult(HomeActivity.this);*/
                                    }

                                    break;
                                default:
                                    //do something
                                    break;
                            }
                            return true;
                        }
                    });

//        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        }
    }

    /**
     * this method will help to make transition of one fragment to another
     *
     * @param fragment
     * @param addToBackStack
     * @param isAnimate
     * @param tag
     */
    public void addFragment(Fragment fragment, boolean addToBackStack,
                            boolean isAnimate, String tag) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (isAnimate) {
            ft.setCustomAnimations(R.anim.slide_in_right,
                    R.anim.slide_out_left, R.anim.slide_in_left,
                    R.anim.slide_out_right);
        }
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(R.id.contain_frame, fragment, tag);
        ft.commitAllowingStateLoss();
    }

    public void replaceFragmentFromStack(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.contain_frame, fragment);
        ft.commitAllowingStateLoss();
    }

    private void goToHomeFragment() {
        toolbar.setVisibility(View.GONE);
        // Utils.showCustomProgressDialog(HomeActivity.this,false);
        // Log.i("showCustomProgressDialoga","show");
        if (getSupportFragmentManager() != null && getSupportFragmentManager().findFragmentByTag(Const.Tag.HOME_FRAGMENT) == null) {
            HomeFragment homeFragment = new HomeFragment();
            addFragment(homeFragment, false, true, Const.Tag.HOME_FRAGMENT);
            //  Utils.hideCustomProgressDialog();
            Log.i("showCustomProgressDialoga", "hide");

        }
        // Utils.hideCustomProgressDialog();
        //   Log.i("showCustomProgressDialoga","hide");

    }

    private void goToStoreFragment(Deliveries deliveries) {
        toolbar.setVisibility(View.GONE);
        if (getSupportFragmentManager() != null && getSupportFragmentManager().findFragmentByTag(Const.Tag.STORE_FRAGMENT) == null) {

            StoresFragment storesFragment = new StoresFragment();
            if (storesFragment != null) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(Const.DELIVERY_STORE, deliveries);
                storesFragment.setArguments(bundle);
                addFragment(storesFragment, false, false, Const.Tag.STORE_FRAGMENT);
            }
        }
    }

    private void goToHomeFragmentDifferentTransition() {
        if (getSupportFragmentManager() != null && getSupportFragmentManager().findFragmentByTag(Const.Tag.HOME_FRAGMENT) == null) {
            HomeFragment homeFragment = new HomeFragment();
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();

            ft.setCustomAnimations(R.anim.fade_in_out,
                    R.anim.slide_out_left, R.anim.fade_in_out,
                    R.anim.slide_out_right);
            ft.replace(R.id.contain_frame, homeFragment, Const.Tag.HOME_FRAGMENT);
            ft.commitAllowingStateLoss();
        }
    }

    private void goToUserFragment() {
        if (getSupportFragmentManager() != null && getSupportFragmentManager().findFragmentByTag(Const.Tag.USER_FRAGMENT) == null) {
            UserFragment userFragment = new UserFragment();
            toolbar.setVisibility(View.VISIBLE);
            addFragment(userFragment, false, true, Const.Tag.USER_FRAGMENT);
        }
    }

    private void goToOrdersActivity() {
        Intent intent = new Intent(this, OrdersActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_freshchat_fab:
                if (!chatStart) {
                    initializeAndImplementLiveChat();  //initialize chat
                }
                emmbeddedChatWindow.showChatWindow();
               /*Intent intent = new Intent(getActivity(), LiveChatActivity.class);
                startActivity(intent);*/
              /*  Intent mIntent = new Intent(HomeActivity.this, WebViewActivity.class);
                mIntent.putExtra("web", "chat");
                startActivity(mIntent);*/
                /*Freshchat.showConversations(getApplicationContext());*/
                return;
            default:
                return;

        }

        //do something
    }


    public void configureChatWindow() {
        emmbeddedChatWindow = findViewById(R.id.embedded_chat_window);

        if (!PreferenceHelper.getInstance(getApplicationContext()).getUserId().equals("")) {
            paramName = PreferenceHelper.getInstance(getApplicationContext()).getFirstName() + " " + PreferenceHelper.getInstance(getApplicationContext()).getLastName();
            if (!PreferenceHelper.getInstance(getApplicationContext()).getEmail().equals("")) {
                paramEmail = PreferenceHelper.getInstance(getApplicationContext()).getEmail();
            }
        } else {
            paramName = "guest";
            paramEmail = "";
        }
        Log.e("name", paramName + paramEmail);


       /* HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("KEY_LICENCE_NUMBER", getString(R.string.livechat_lisense_key));
        *//*hashMap.put("KEY_GROUP_ID", group_id);*//*
        hashMap.put("KEY_VISITOR_NAME", paramName);
        hashMap.put("KEY_VISITOR_EMAIL", paramEmail);
        Log.e("parameter", hashMap.toString());*/
        /* params.put(CUSTOM_PARAM_PREFIX + key, customVariables.get(key))*/

        configuration = new ChatWindowConfiguration(getString(R.string.livechat_lisense_key), "", paramName, paramEmail, null);
    }


    public void initializeAndImplementLiveChat() {
        if (!chatStart) {
            if (!emmbeddedChatWindow.isInitialized()) {
                emmbeddedChatWindow = ChatWindowView.createAndAttachChatWindowInstance(getActivity());
                emmbeddedChatWindow.setUpWindow(configuration);
                emmbeddedChatWindow.setUpListener(new ChatWindowView.ChatWindowEventsListener() {
                    @Override
                    public void onChatWindowVisibilityChanged(boolean visible) {
                        chatStart = true;
                        Log.e("chatVisible2", String.valueOf(visible));
                    }

                    @Override
                    public void onNewMessage(NewMessageModel message, boolean windowVisible) {
                        Log.e("chatVisible2", "new_message");
                    }

                    @Override
                    public void onStartFilePickerActivity(Intent intent, int requestCode) {
                        //for upload media in chat
                        intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("*/*");
                        startActivityForResult(intent, requestCode);
                        Log.e("chatVisible3", "pick_file");
                    }

                    @Override
                    public boolean onError(ChatWindowErrorType errorType, int errorCode, String errorDescription) {

                        return false;
                    }

                    @Override
                    public boolean handleUri(Uri uri) {

                        return false;
                    }
                });
                emmbeddedChatWindow.initialize();
            }
        }


    }

    @Override
    public void onBackPressed() {
        openExitDialog();
    }

    private void checkDocumentUploadAndApproved() {
        try {
            if (preferenceHelper != null) {
                if (preferenceHelper.getIsApproved()) {
                    closedAdminApprovedDialog();
                    if (preferenceHelper.getIsAdminDocumentMandatory() && !preferenceHelper
                            .getIsUserAllDocumentsUpload()) {
                        goToDocumentActivity(true);
                    } else {
                        if (currentBooking.isHaveOrders()) {
                            openEmailOrPhoneConfirmationDialog(getResources().getString(R
                                    .string.text_confirm_detail), getResources().getString(R
                                    .string.msg_plz_confirm_your_detail), getResources()
                                    .getString(R.string
                                            .text_log_out), getResources().getString(R.string
                                    .text_ok));
                        }

                    }

                } else {
                    if (preferenceHelper.getIsAdminDocumentMandatory() && !preferenceHelper
                            .getIsUserAllDocumentsUpload()) {
                        goToDocumentActivity(true);
                    } else {
                        openAdminApprovedDialog();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("checkDocumentUploadAndApproved() ", e.getMessage().toString());
        }
    }


    protected void openExitDialog() {
        if (emmbeddedChatWindow.isShown()) {
            emmbeddedChatWindow.hideChatWindow();
        } else {
            try {
                if (exitDialog != null && exitDialog.isShowing()) {
                    return;
                }
                exitDialog = new CustomDialogAlert(this, this
                        .getResources()
                        .getString(R.string.text_exit), this.getResources()
                        .getString(R.string.msg_are_you_sure), this.getResources()
                        .getString(R.string.text_cancel), this.getResources()
                        .getString(R.string.text_ok)) {
                    @Override
                    public void onClickLeftButton() {
                        dismiss();

                    }

                    @Override
                    public void onClickRightButton() {
                        dismiss();
                        CurrentBooking.getInstance().setBookCityId("");
                        finish();
                    }
                };
                exitDialog.show();
            } catch (Exception e) {
                Log.e("openExitDialog() ", e.getMessage());
            }
        }


    }

    /**
     * this method called webservice for get OTP for mobile or email
     *
     * @param jsonObject
     */
    private void getOtpVerify(JSONObject jsonObject) {
        Utils.showCustomProgressDialog(this, false);
        if (customDialogVerification != null && customDialogVerification.isShowing()) {
            return;
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OtpResponse> otpResponseCall = apiInterface.getOtpVerify(ApiClient
                .makeJSONRequestBody(jsonObject));
        otpResponseCall.enqueue(new Callback<OtpResponse>() {
            @Override
            public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {

                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    AppLog.Log("SMS_OTP", response.body().getOtpForSms());
                    AppLog.Log("EMAIL_OTP", response.body().getOtpForEmail());
                    otpResponse = response.body();
                    switch (checkWitchOtpValidationON()) {
                        case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                            openEmailOrPhoneOTPVerifyDialog(otpResponse.getOtpForEmail(),
                                    otpResponse
                                            .getOtpForSms(), getResources().getString(R
                                            .string.text_email_otp), getResources().getString
                                            (R.string
                                                    .text_phone_otp), true);
                            break;
                        case Const.SMS_VERIFICATION_ON:
                            openEmailOrPhoneOTPVerifyDialog(otpResponse.getOtpForEmail(),
                                    otpResponse
                                            .getOtpForSms(), "", getResources().getString(R
                                            .string
                                            .text_phone_otp), false);
                            break;
                        case Const.EMAIL_VERIFICATION_ON:
                            openEmailOrPhoneOTPVerifyDialog(otpResponse.getOtpForEmail(),
                                    otpResponse
                                            .getOtpForSms(),
                                    "", getResources().getString(R
                                            .string
                                            .text_email_otp), false);
                            break;
                        default:
                            // do with default
                            break;
                    }


                } else {
                    Log.i("Utils.showErrorToast", "ok1");
                    Utils.showErrorToast(response.body().getErrorCode(), HomeActivity.this);
                }


            }

            @Override
            public void onFailure(Call<OtpResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
            }
        });

    }

    /**
     * this method open dialog which confirm user email or mobile detail
     *
     * @param titleDialog      set dialog title
     * @param messageDialog    set dialog message
     * @param titleLeftButton  set dialog left button text
     * @param titleRightButton set dialog right button text
     */

    private void openEmailOrPhoneConfirmationDialog(String titleDialog, String messageDialog,
                                                    String titleLeftButton, String
                                                            titleRightButton) {
        CustomFontTextView tvDialogEdiTextMessage, tvDialogEditTextTitle, btnDialogEditTextLeft,
                btnDialogEditTextRight;
        TextInputLayout dialogItlOne;
        LinearLayout llConfirmationPhone;
        CustomFontEditTextView etRegisterCountryCode;

        if (customDialogVerification != null && customDialogVerification.isShowing()
                || dialogEmailOrPhoneVerification != null && dialogEmailOrPhoneVerification
                .isShowing()) {
            return;
        }
        dialogEmailOrPhoneVerification = new Dialog(this);
        dialogEmailOrPhoneVerification.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogEmailOrPhoneVerification.setContentView(R.layout.dialog_confrimation_email_or_phone);

        tvDialogEdiTextMessage = (CustomFontTextView) dialogEmailOrPhoneVerification.findViewById
                (R.id
                        .tvDialogAlertMessage);
        tvDialogEditTextTitle = (CustomFontTextView) dialogEmailOrPhoneVerification.findViewById
                (R.id.tvDialogAlertTitle);
        btnDialogEditTextLeft = (CustomFontTextView) dialogEmailOrPhoneVerification.findViewById
                (R.id.btnDialogAlertLeft);
        btnDialogEditTextRight = (CustomFontTextView) dialogEmailOrPhoneVerification.findViewById
                (R.id.btnDialogAlertRight);
        etDialogEditTextOne = (CustomFontEditTextView) dialogEmailOrPhoneVerification
                .findViewById(R.id
                        .etDialogEditTextOne);
        etDialogEditTextTwo = (CustomFontEditTextView) dialogEmailOrPhoneVerification
                .findViewById(R.id
                        .etDialogEditTextTwo);
        etRegisterCountryCode = (CustomFontEditTextView) dialogEmailOrPhoneVerification
                .findViewById(R.id
                        .etRegisterCountryCode);
        etDialogEditTextOne.setText(preferenceHelper.getEmail());
        etDialogEditTextTwo.setText(preferenceHelper.getPhoneNumber());
        etRegisterCountryCode.setText(preferenceHelper.getPhoneCountyCodeCode());

        llConfirmationPhone = (LinearLayout) dialogEmailOrPhoneVerification.findViewById(R.id
                .llConfirmationPhone);
        dialogItlOne = (TextInputLayout) dialogEmailOrPhoneVerification.findViewById(R.id
                .dialogItlOne);

        btnDialogEditTextLeft.setOnClickListener(this);
        btnDialogEditTextRight.setOnClickListener(this);

        tvDialogEditTextTitle.setText(titleDialog);
        tvDialogEdiTextMessage.setText(messageDialog);
        btnDialogEditTextLeft.setText(titleLeftButton);
        btnDialogEditTextRight.setText(titleRightButton);


        btnDialogEditTextRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Const.Params.ID, preferenceHelper.getUserId());
                    jsonObject.put(Const.Params.TYPE, String.valueOf(Const.Type.USER));
                    switch (checkWitchOtpValidationON()) {
                        case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                            if (Patterns.EMAIL_ADDRESS.matcher(etDialogEditTextOne.getText
                                    ().toString()).matches()) {
                                if (etDialogEditTextTwo.getText().toString().trim().length()
                                        > preferenceHelper.getMaxPhoneNumberLength() ||
                                        etDialogEditTextTwo.getText()
                                                .toString().trim()
                                                .length
                                                        () < preferenceHelper
                                                .getMinPhoneNumberLength()) {

                                    etDialogEditTextTwo.setError(getResources().getString(R
                                            .string.msg_please_enter_valid_mobile_number) + " " +
                                            "" + preferenceHelper.getMinPhoneNumberLength() +
                                            getResources().getString(R
                                                    .string
                                                    .text_or)
                                            + preferenceHelper.getMaxPhoneNumberLength() + " " +
                                            getResources().getString
                                                    (R.string
                                                            .text_digits));
                                } else {
                                    jsonObject.put(Const.Params.EMAIL, etDialogEditTextOne
                                            .getText().toString());
                                    jsonObject.put(Const.Params.PHONE, etDialogEditTextTwo.getText
                                            ().toString());
                                    jsonObject.put(Const.Params.COUNTRY_PHONE_CODE, preferenceHelper
                                            .getPhoneCountyCodeCode());
                                    dialogEmailOrPhoneVerification.dismiss();
                                    email = etDialogEditTextOne.getText().toString();
                                    phone = etDialogEditTextTwo.getText().toString();
                                    getOtpVerify(jsonObject);
                                }

                            } else {
                                etDialogEditTextOne.setError(getResources().getString(R.string
                                        .msg_please_enter_valid_email));
                            }
                            break;
                        case Const.SMS_VERIFICATION_ON:
                            if (etDialogEditTextTwo.getText().toString().trim().length()
                                    > preferenceHelper.getMaxPhoneNumberLength() ||
                                    etDialogEditTextTwo.getText()
                                            .toString().trim()
                                            .length
                                                    () < preferenceHelper
                                            .getMinPhoneNumberLength()) {

                                etDialogEditTextTwo.setError(getResources().getString(R
                                        .string.msg_please_enter_valid_mobile_number) + " " +
                                        "" + preferenceHelper.getMinPhoneNumberLength() +
                                        getResources().getString(R
                                                .string
                                                .text_or)
                                        + preferenceHelper.getMaxPhoneNumberLength() + " " +
                                        getResources().getString
                                                (R.string
                                                        .text_digits));
                            } else {
                                jsonObject.put(Const.Params.PHONE, etDialogEditTextTwo.getText
                                        ().toString());
                                jsonObject.put(Const.Params.COUNTRY_PHONE_CODE, preferenceHelper
                                        .getPhoneCountyCodeCode());
                                dialogEmailOrPhoneVerification.dismiss();
                                phone = etDialogEditTextTwo.getText().toString();
                                getOtpVerify(jsonObject);
                            }
                            break;
                        case Const.EMAIL_VERIFICATION_ON:
                            if (Patterns.EMAIL_ADDRESS.matcher(etDialogEditTextOne.getText
                                    ().toString()).matches()) {
                                jsonObject.put(Const.Params.EMAIL, etDialogEditTextOne
                                        .getText().toString());
                                dialogEmailOrPhoneVerification.dismiss();
                                email = etDialogEditTextOne.getText().toString();
                                getOtpVerify(jsonObject);
                            } else {
                                etDialogEditTextOne.setError(getResources().getString(R.string
                                        .msg_please_enter_valid_email));
                            }
                            break;
                        default:
                            // do with default
                            break;
                    }

                } catch (JSONException e) {
                    Log.e("btnDialogEditTextRight ", e.getMessage());
                    AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, e);
                }
            }
        });
        btnDialogEditTextLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOut();
                dialogEmailOrPhoneVerification.dismiss();

            }
        });
        WindowManager.LayoutParams params = dialogEmailOrPhoneVerification.getWindow()
                .getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialogEmailOrPhoneVerification.setCancelable(false);

        switch (checkWitchOtpValidationON()) {
            case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                etDialogEditTextOne.setVisibility(View.VISIBLE);
                dialogItlOne.setVisibility(View.VISIBLE);
                llConfirmationPhone.setVisibility(View.VISIBLE);
                dialogEmailOrPhoneVerification.show();
                break;
            case Const.EMAIL_VERIFICATION_ON:
                etDialogEditTextOne.setVisibility(View.VISIBLE);
                dialogItlOne.setVisibility(View.VISIBLE);
                llConfirmationPhone.setVisibility(View.GONE);
                dialogEmailOrPhoneVerification.show();
                break;
            case Const.SMS_VERIFICATION_ON:
                etDialogEditTextOne.setVisibility(View.GONE);
                dialogItlOne.setVisibility(View.GONE);
                llConfirmationPhone.setVisibility(View.VISIBLE);
                dialogEmailOrPhoneVerification.show();
                break;
            default:
                etDialogEditTextOne.setVisibility(View.GONE);
                dialogItlOne.setVisibility(View.GONE);
                llConfirmationPhone.setVisibility(View.GONE);
                break;
        }
    }

    /**
     * this method open dialog which is help to verify OTP witch is send to email or mobile
     *
     * @param otpEmailVerification set email otp number
     * @param otpSmsVerification   set mobile otp number
     * @param editTextOneHint      set hint text in edittext one
     * @param ediTextTwoHint       set hint text in edittext two
     * @param isEditTextOneVisible set true edittext one visible
     */
    private void openEmailOrPhoneOTPVerifyDialog(final String otpEmailVerification, final String
            otpSmsVerification, String editTextOneHint, String ediTextTwoHint, boolean
                                                         isEditTextOneVisible) {

        if (customDialogVerification != null && customDialogVerification.isShowing()) {
            return;
        }

        customDialogVerification = new CustomDialogVerification
                (this,
                        getResources().getString(R.string.text_verify_detail), getResources()
                        .getString(R
                                .string.msg_verify_detail), getResources().getString(R.string
                        .text_log_out)
                        , getResources().getString(R.string.text_ok), editTextOneHint,
                        ediTextTwoHint,
                        isEditTextOneVisible,
                        InputType.TYPE_CLASS_NUMBER, InputType.TYPE_CLASS_NUMBER) {
            @Override
            public void onClickLeftButton() {
                customDialogVerification.dismiss();
                logOut();

            }

            @Override
            public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                           CustomFontEditTextView etDialogEditTextTwo) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Const.Params.USER_ID,
                            preferenceHelper.getUserId());
                    jsonObject.put(Const.Params.SERVER_TOKEN,
                            preferenceHelper.getSessionToken());

                    switch (checkWitchOtpValidationON()) {
                        case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                            if (TextUtils.equals(etDialogEditTextOne.getText().toString(),
                                    otpEmailVerification)) {
                                if (TextUtils.equals(etDialogEditTextTwo
                                                .getText().toString(),
                                        otpSmsVerification)) {
                                    jsonObject.put(Const.Params.IS_PHONE_NUMBER_VERIFIED,
                                            true);
                                    jsonObject.put(Const.Params.IS_EMAIL_VERIFIED,
                                            true);
                                    jsonObject.put(Const.Params.EMAIL, email);
                                    jsonObject.put(Const.Params.PHONE, phone);
                                    customDialogVerification.dismiss();
                                    setOTPVerification(jsonObject);
                                } else {
                                    etDialogEditTextTwo.setError(getResources().getString(R.string
                                            .msg_sms_otp_wrong));
                                }

                            } else {
                                etDialogEditTextOne.setError(getResources().getString(R.string
                                        .msg_email_otp_wrong));
                            }
                            break;
                        case Const.SMS_VERIFICATION_ON:
                            if (TextUtils.equals(etDialogEditTextTwo
                                            .getText().toString(),
                                    otpSmsVerification)) {
                                jsonObject.put(Const.Params.IS_PHONE_NUMBER_VERIFIED,
                                        true);
                                jsonObject.put(Const.Params.PHONE, phone);
                                customDialogVerification.dismiss();
                                setOTPVerification(jsonObject);
                            } else {
                                etDialogEditTextTwo.setError(getResources().getString(R.string
                                        .msg_sms_otp_wrong));
                            }
                            break;
                        case Const.EMAIL_VERIFICATION_ON:
                            if (TextUtils.equals(etDialogEditTextTwo.getText().toString(),
                                    otpEmailVerification)) {
                                jsonObject.put(Const.Params.IS_EMAIL_VERIFIED,
                                        true);
                                jsonObject.put(Const.Params.EMAIL, email);
                                customDialogVerification.dismiss();
                                setOTPVerification(jsonObject);
                            } else {
                                etDialogEditTextTwo.setError(getResources().getString(R.string
                                        .msg_email_otp_wrong));
                            }
                            break;
                        default:
                            // do with default
                            break;
                    }

                } catch (JSONException e) {
                    Log.e("onClickRightButton ", e.getMessage());
                    AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
                }
            }

        };
        customDialogVerification.show();
    }


    /**
     * this method called a webservice for get user detail
     */
    private void getUserDetail() {
        //Utils.showCustomProgressDialog(this, false);
        if (preferenceHelper != null) {
            JSONObject jsonObject = new JSONObject();
            try {
                if (preferenceHelper.getUserId() != null) {
                    jsonObject.put(Const.Params.USER_ID,
                            preferenceHelper.getUserId());
                }
                if (preferenceHelper.getSessionToken() != null) {
                    jsonObject.put(Const.Params.SERVER_TOKEN,
                            preferenceHelper.getSessionToken());
                }
                if (getAppVersion() != null) {
                    jsonObject.put(Const.Params.APP_VERSION, getAppVersion());
                }
                if (preferenceHelper.getAndroidId() != null) {
                    jsonObject.put(Const.Params.CART_UNIQUE_TOKEN, preferenceHelper.getAndroidId());
                }
            } catch (JSONException e) {
                Log.e("getUserDetail() ", e.getMessage());
                AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
            }

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<UserDataResponse> responseCall = apiInterface.getUserDetail(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<UserDataResponse>() {
                @Override
                public void onResponse(Call<UserDataResponse> call, Response<UserDataResponse> response) {
                    if (response != null && response.body() != null && parseContent != null && parseContent.isSuccessful(response) && parseContent.parseUserStorageData(response)) {
                        checkDocumentUploadAndApproved();
                        Utils.hideCustomProgressDialog();

                    }

                }

                @Override
                public void onFailure(Call<UserDataResponse> call, Throwable t) {
                    AppLog.handleThrowable(HomeActivity.class.getName(), t);
                    Utils.hideCustomProgressDialog();
                }
            });
        }
    }

    /**
     * this method called a webservice for set otp verification result in web
     */
    private void setOTPVerification(JSONObject jsonObject) {
        Utils.showCustomProgressDialog(this, false);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.setOtpVerification(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && response.body().isSuccess()) {
                    preferenceHelper.putIsEmailVerified(response.body().isSuccess());
                    preferenceHelper.putIsPhoneNumberVerified(response.body().isSuccess());
                    preferenceHelper.putEmail(email);
                    preferenceHelper.putPhoneNumber(phone);
                    Utils.showMessageToast(response.body().getMessage(), HomeActivity.this);
                } else {
                    Log.i("Utils.showErrorToast", "ok2");
                    Utils.showErrorToast(response.body().getErrorCode(), HomeActivity.this);
                }


            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(HomeActivity.class.getName(), t);
            }
        });
    }


    private void updateUIBottomNavigationView(boolean isUpdate) {

        Log.i("updateUIBottomNavigationView", "true");
        try {
            if (bottomNavigationView != null && bottomNavigationView
                    .getChildAt(0) != null) {
                BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView
                        .getChildAt(0);
                if (menuView != null && bottomNavigationMenu != null && bottomNavigationMenu.findItem(R.id.action_user) != null) {
                    menuView.findViewById(R.id.action_orders).setVisibility(View.GONE);
                    if (isUpdate) {
                        bottomNavigationMenu.findItem(R.id.action_user).setTitle(getResources().getString(R
                                .string
                                .text_user));
                        menuView.findViewById(R.id.action_orders).setVisibility(View.VISIBLE);
                        getUserDetail();
                    } else {
                        bottomNavigationMenu.findItem(R.id.action_user).setTitle(getResources().getString(R
                                .string
                                .text_login));
                        menuView.findViewById(R.id.action_orders).setVisibility(View.GONE);
                    }
                }
            }
        } catch (Exception e) {
            Log.i("updateUIBottomNavigationView", e.toString());
            Log.e("updateUIBottomNavigationView() ", e.getMessage());
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        AppLog.Log("LOCATION_HELPER", "GoogleClientConnected");
        Log.i("onconnnected", "yes");

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            Log.i("onconnnected", "yes");

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission
                    .ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Const
                    .PERMISSION_FOR_LOCATION);

        } else {
            //Do the stuff that requires permission...
            locationHelper.getLastLocation(this, new OnSuccessListener<Location>() {

                @Override
                public void onSuccess(Location location) {

                    currentLocation = location;
                    if (currentLocation != null) {
                        Log.i("LOCATION_HELPER", "Last location");
                    }
                    //
                    if (currentLocation == null) {
                        if (!locationHelper.isOpenGpsDialog()) {
                            // locationHelper.setOpenGpsDialog(true);
                            locationHelper.setLocationSettingRequest(HomeActivity.this,
                                    REQUEST_CHECK_SETTINGS, new
                                            OnSuccessListener() {
                                                @Override
                                                public void onSuccess(Object o) {
                                                    // doCodeAfterLocationGet();
                                                    Log.i("location:onconnected", "onSuccess");
                                                }
                                            }, new LocationHelper.NoGPSDeviceFoundListener() {
                                        @Override
                                        public void noFound() {
                                            // swipeFragmentAccordingToDeliveries();
                                            Log.i("location:onconnected", "noFound");
                                        }
                                    });
                        }
                    } else {
                        Log.i("onconnectedelse", "yes");
                        Log.i("onconnectedelse", currentLocation.toString());
                        if (!Isusernotchecked) {
                            doCodeAfterLocationGet();
                        }

                    }

                }
            });

        }
    }


    private void doCodeAfterLocationGet() {
        //Utils.showCustomProgressDialog(HomeActivity.this, false);
        try {
            AppLog.Log(Const.Tag.HOME_FRAGMENT, "GoogleClientConnected");
            if (TextUtils.isEmpty(currentBooking.getBookCityId())) {
                getCart();
                if (currentLocation != null && !isCurrentLogin()) {

                    Log.i("docodeif", "yes");
                    getGeocodeDataFromLocation(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
                } else {
                    //   Utils.hideCustomProgressDialog();
                    getFavAddressList();
                    Log.i("docodeelse", "yes");
                    Log.i("docodeelse", currentLocation.toString());

                    LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                    Log.i("docodeelse", latLng.toString());


                    getGeocodeDataFromLocation(new LatLng(latLng.latitude, latLng.longitude));

                }
            } else {

                //setAddressOnToolbar(currentBooking.getCurrentAddress());
                //  change fragment when user already select home
                if (bottomNavigationMenu.findItem(R.id.action_home).isChecked()) {
                    Log.i("swipeFragment", "doCodeAfterLocationGet");
                    swipeFragmentAccordingToDeliveries();

                } else {
                    Utils.hideCustomProgressDialog();
                }
                AppLog.Log("CITY", currentBooking.getCurrentCity());
            }
        } catch (Exception e) {
            Utils.hideCustomProgressDialog();
            Log.e("doCodeAfterLocationGet() ", e.getMessage());
        }

        /*try{
            AppLog.Log(Const.Tag.HOME_FRAGMENT, "GoogleClientConnected");
            if (currentBooking.getBookCityId() != null && !TextUtils.isEmpty(currentBooking.getBookCityId())) {
                getCart();
                if (currentLocation != null && !isCurrentLogin()) {
                    getGeocodeDataFromLocation(currentLocation);
                } else {
                    //   Utils.hideCustomProgressDialog();
                    getFavAddressList();
                }
            } else {
                Utils.hideCustomProgressDialog();
                setAddressOnToolbar(currentBooking.getCurrentAddress());
                //  change fragment when user already select home
                if (bottomNavigationMenu.findItem(R.id.action_home).isChecked()) {
                    swipeFragmentAccordingToDeliveries();
                }
                AppLog.Log("CITY", currentBooking.getCurrentCity());
            }
        }catch (Exception e){
            Log.e("doCodeAfterLocationGet() " + e.getMessage());
        }*/

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppLog.Log(HomeActivity.class.getSimpleName(), "GoogleClientFlied");
    }

    @Override
    public void onConnectionSuspended(int i) {
        AppLog.Log(HomeActivity.class.getSimpleName(), "GoogleClientSuspended");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("onlocationchanged", "getgeocodedata");
        if (currentLocation == null) {
            currentLocation = location;
            if (currentLocation != null & !Isusernotchecked) {
                //  if (currentLocation != null && !isCurrentLogin()) {
                getGeocodeDataFromLocation(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
                Log.i("onlocationchanged", "getgeocodedata");
                //}

                //locationHelper.stopLocationUpdate();
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_LOCATION:
                    this.recreate();
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        locationHelper.onStop();
                        locationHelper.onStart();
                    }
                    break;
                default:
                    //do with default
                    break;
            }
        }
    }

    /**
     * this method used to called webservice for get Delivery type according to param
     *
     * @param country      country name in string
     * @param countryCode  country code (91) in string
     * @param city         city name in string
     * @param subAdminArea subAdminArea in string
     * @param adminArea    adminArea in string
     * @param cityLatLng   location of city
     */

    private void getDeliveryStoreInCity(String country, String countryCode, String city, String
            subAdminArea, String adminArea, final LatLng cityLatLng, final String address, String
                                                cityCode) {


        if (currentBooking != null) {
            // Utils.showCustomProgressDialog(this, false);
            currentBooking.setCurrentAddress(address);
            currentBooking.setCurrentLatLng(cityLatLng);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Const.Params.USER_ID, preferenceHelper
                        .getUserId());
                jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                        .getSessionToken());
                jsonObject.put(Const.Params.CART_UNIQUE_TOKEN, preferenceHelper.getAndroidId());

                /*jsonObject.put(Const.Params.COUNTRY, "United Kingdom");
                jsonObject.put(Const.Params.ADDRESS, "Thornbridge, Washington NE38 8TJ, UK");
                jsonObject.put(Const.Params.COUNTRY_CODE, "GB");
                jsonObject.put(Const.Params.COUNTRY_CODE_2, "GB");
                jsonObject.put(Const.Params.CITY_CODE, "England");
                jsonObject.put(Const.Params.LATITUDE, 54.89711219999999);
                jsonObject.put(Const.Params.LONGITUDE, -1.4964260000000422);*/

                jsonObject.put(Const.Params.COUNTRY, country);
                jsonObject.put(Const.Params.ADDRESS, address);
                jsonObject.put(Const.Params.COUNTRY_CODE, countryCode);
                jsonObject.put(Const.Params.COUNTRY_CODE_2, countryCode);
                jsonObject.put(Const.Params.CITY_CODE, cityCode);
                jsonObject.put(Const.Params.LATITUDE, cityLatLng.latitude);
                jsonObject.put(Const.Params.LONGITUDE, cityLatLng.longitude);

                int count = 0;
                if (TextUtils.isEmpty(city)) {
                    jsonObject.put(Const.Params.CITY1, "");
                } else {
                    jsonObject.put(Const.Params.CITY1, city);
                    count++;
                }
                if (TextUtils.isEmpty(subAdminArea)) {
                    jsonObject.put(Const.Params.CITY2, "");
                } else {
                    jsonObject.put(Const.Params.CITY2,/*"England"*/subAdminArea);
                    count++;
                }
                if (TextUtils.isEmpty(adminArea)) {
                    jsonObject.put(Const.Params.CITY3, "");
                } else {
                    jsonObject.put(Const.Params.CITY3,/*"Tyne and Wear"*/ adminArea);
                    count++;
                }

            } catch (Exception e) {
                Log.e("getDeliveryStoreInCity ", e.getMessage());
                AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
            }

            Log.i("getDeliveryStoreInCity", "Params :" + String.valueOf(jsonObject));

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<DeliveryStoreResponse> responseCall = apiInterface.getDeliveryStoreList(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<DeliveryStoreResponse>() {
                @SuppressLint("LongLogTag")
                @Override
                public void onResponse(Call<DeliveryStoreResponse> call,
                                       Response<DeliveryStoreResponse> response) {
                    Log.i("getdeliverylocationcityif", "ok");
                    Log.i("getdeliverylocationcityif", response.toString());

                    AppLog.Log("getDeliveryStoreInCity", "Responce :" + ApiClient.JSONResponse(response.body()));
                    if (response != null && parseContent != null && parseContent.parseDeliveryStore(response)) {

                        Log.i("getDeliveryStoreInCity", "ok");

                        if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {

                            Log.i("getDistanceMatrixif", "ok");
                            getDistanceMatrix();
                        } else {
                            Log.i("swipeFragment", "getDeliveryStoreInCity");

                            swipeFragmentAccordingToDeliveries();
                        }
                        Utils.hideCustomProgressDialog();
                    } else {
                        currentBooking.setDeliveryAvailable(false);
                        currentBooking.getDeliveryStoreList().clear();
                        Utils.showErrorToast(response.body().getErrorCode(), HomeActivity.this);
                        currentBooking.setCityCode(cityCode);
                        currentBooking.setCountry(country);
                        currentBooking.setCountryCode(countryCode);
                        currentBooking.setCountryCode2(countryCode);
                        currentBooking.setPaymentLongitude(cityLatLng.longitude);
                        currentBooking.setPaymentLatitude(cityLatLng.latitude);
                        currentBooking.setCity1(city);
                        currentBooking.setCity2("");
                        currentBooking.setCity3("");
                        Log.i("swipeFragment", "getDeliveryStoreInCity");
                        swipeFragmentAccordingToDeliveries();
                        Utils.hideCustomProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<DeliveryStoreResponse> call, Throwable t) {
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, t);

                    if (isCurrentLogin()) {
                        if (preferenceHelper.getIsPhoneNumberVerified()) {
                            getDeliveryStoreInCity(str_country, str_country_Code, str_locality, "",
                                    "", cityLatLng, address, cityCode);
                        }
                    } else if (!isCurrentLogin()) {

                        getDeliveryStoreInCity(str_country, str_country_Code, str_locality, "",
                                "", cityLatLng, address, cityCode);
                    }
                }
            });
        }
    }

    /**
     * this method called a webservice for get distance and time witch is provided by Google
     */
    private void getDistanceMatrix() {
        // Utils.showCustomProgressDialog(this, false);
        //HashMap<String, String> hashMap = new HashMap<>();
        if (parseContent != null && currentBooking != null && currentBooking.getCityLocation() != null && currentBooking.getCityLocation().size() > 0 && currentBooking.getCityLocation().get(0) != null && currentBooking.getCityLocation().get(1) != null && preferenceHelper != null) {

          /*  HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("sources", "1");
            hashMap.put("annotations", "distance,duration");
            hashMap.put("access_token", getResources().getString(R.string
                    .MAP_BOX_ACCESS_TOKEN));
//            ApiInterface apiInterface = ApiClient.getClientMapBox().create(ApiInterface.class);

            //   Call<DirectionMatrixResponse> call = apiInterface.getDistanceMatrixApi(currentBooking.getCityLocation().get(1)+","+currentBooking.getCityLocation().get(0)+";"+currentBooking.getPaymentLongitude()+","+currentBooking.getPaymentLatitude(), hashMap);

            //swipe latlaong first user and seond store latlong used
            *//*Call<DirectionMatrixResponse> call = apiInterface.getDistanceMatrixApi(currentBooking.getCityLocation().get(1) + "," + currentBooking.getCityLocation().get(0) + ";" + currentBooking.getPaymentLongitude() + "," + currentBooking.getPaymentLatitude(), hashMap);*//*
             */
            origin = new GeoCoordinate(currentBooking.getCityLocation().get(0), currentBooking.getCityLocation().get(1));
            desti = new GeoCoordinate(currentBooking.getPaymentLatitude(), currentBooking.getPaymentLongitude());

            Log.i("getDistanceMatrix", "start" + origin + "desti" + desti);

            if (origin != null && desti != null) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(Const.HERE_API_KEY, getResources().getString(R.string.here_api_key));
                hashMap.put(Const.HERE_WAYPOINT0, getResources().getString(R.string.here_waypoint) + currentBooking.getCityLocation().get(0) + "," + currentBooking.getCityLocation().get(1));
                hashMap.put(Const.HERE_WAYPOINT1, getResources().getString(R.string.here_waypoint) + currentBooking.getPaymentLatitude() + "," + currentBooking.getPaymentLongitude());
                hashMap.put(Const.HERE_MODE, getResources().getString(R.string.here_mode));
                ApiInterface apiInterface = ApiClient.getClientMapBox().create(ApiInterface.class);

                Call<HereDistanceResponce> call = apiInterface.getHereDistanceMatrix(hashMap);
                call.enqueue(new Callback<HereDistanceResponce>() {
                    @Override
                    public void onResponse(Call<HereDistanceResponce> call, Response<HereDistanceResponce> response) {

                        double timeSecond = 0;
                        double distance = 0;
                        Log.i("Call<ResponseBody>", "ok");
                        Utils.hideCustomProgressDialog();

                        if (response != null) {

                            Log.i("Call<ResponseBody>", response.toString());
                            if (response.body() != null) {

                                if (response.body().getResponse().getRoute().get(0).getSummary() != null) {


                                    distance = response.body().getResponse().getRoute().get(0).getSummary().getDistance();
                                    Log.d("DISTANCE_MATRIX", "Distance=" + distance + " " + "Time=" +
                                            timeSecond);
                                    if (response != null && parseContent != null && currentBooking != null && parseContent.isSuccessful(response)) {
                                        // HashMap<String, String> map = parseContent.parsDistanceMatrix(response);
                                        //  if (map != null && !map.isEmpty()) {
                                        AppLog.Log("DISTANCE_MATRIX", "Distance=" + distance);
                                        Double radius = Double.valueOf(distance) / 1609.344;
                                        AppLog.Log("aaa", "radius=" + radius);

                                        Log.i("currentBookingRadius()", String.valueOf(currentBooking.getCityRadius()));
                                        if (radius <= currentBooking.getCityRadius()) {
                                            currentBooking.setDeliveryAvailable(true);

                                            Log.i("swipeFragment", "getDistanceMatrix");
                                            swipeFragmentAccordingToDeliveries();
                                        } else {
                                            currentBooking.setDeliveryAvailable(false);
                                            Log.i("swipeFragment", "getDistanceMatrix");
                                            swipeFragmentAccordingToDeliveries();
                                        }
                                    }
                                }
                            } else {
                                Log.i("swipeFragment", "getDistanceMatrixfalse");
                                swipeFragmentAccordingToDeliveries();
                            }

                        } else {
                            Log.i("swipeFragment", "getDistanceMatrix");
                            swipeFragmentAccordingToDeliveries();
                        }
                    }

                    @Override
                    public void onFailure(Call<HereDistanceResponce> call, Throwable t) {
                        AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, t);
                        Utils.hideCustomProgressDialog();
                        Log.d("call", "==== Failure =====" + t.getLocalizedMessage());
                    }
                });

            }

            /*Log.i("routs","origin "+origin+", desti"+desti);
            Double totalkm=origin.distanceTo(desti);
            Log.e("distancecall", "totalKM is " + totalkm);
            Double radius = Double.valueOf(totalkm) / 1000;
            AppLog.Log("aaaa", "radius=" + radius);
            if (radius <= currentBooking.getCityRadius()) {
                currentBooking.setDeliveryAvailable(true);
            } else {
                currentBooking.setDeliveryAvailable(false);
            }
            swipeFragmentAccordingToDeliveries();
            Utils.hideCustomProgressDialog();
*/
//               Call<DirectionMatrixResponse> call = apiInterface.getDistanceMatrixApi(currentBooking.getPaymentLongitude()+","+currentBooking.getPaymentLatitude()+";"+currentBooking.getCityLocation().get(1)+","+currentBooking.getCityLocation().get(0), hashMap);
//               Call<DirectionMatrixResponse> call = apiInterface.getDistanceMatrixApi("-1.501195,54.908721"+";"+"-1.549433,54.955061", hashMap);

           /* call.enqueue(new Callback<DirectionMatrixResponse>() {
                @Override
                public void onResponse(Call<DirectionMatrixResponse> call, Response<DirectionMatrixResponse> response) {
                    Utils.hideCustomProgressDialog();
                    if (response.body() != null) {
                        if (response.body().getCode().equalsIgnoreCase("Ok")) {

                            double timeSecond = 0;
                            double distance = 0;
                            if (response.body().getDistances() != null) {
                                distance = Double.parseDouble(String.valueOf(response.body().getDistances().get(0).get(0)));
                                Log.d("Distancecall", "====Distance Response =====" + response.body().getDistances().get(0).get(0));
                            }

                            if (response.body().getDurations() != null) {
                                timeSecond = Double.parseDouble(String.valueOf(response.body().getDurations().get(0).get(0)));
                                Log.d("call", "==== Response =====" + response.body().getDurations().get(0).get(0));
                            }
                            Log.d("DISTANCE_MATRIX", "Distance=" + distance + " " + "Time=" +
                                    timeSecond);
                            if (response != null && parseContent != null && currentBooking != null && parseContent.isSuccessful(response)) {
                                // HashMap<String, String> map = parseContent.parsDistanceMatrix(response);
                                //  if (map != null && !map.isEmpty()) {

                                AppLog.Log("DISTANCE_MATRIX", "Distance=" + distance);
                                Double radius = Double.valueOf(distance) / 1609.344;
                                AppLog.Log("aaa", "radius=" + radius);
                                Utils.hideCustomProgressDialog();
                                if (radius <= currentBooking.getCityRadius()) {
                                    currentBooking.setDeliveryAvailable(true);
                                } else {
                                    currentBooking.setDeliveryAvailable(false);
                                }
                            }
                        }
                        swipeFragmentAccordingToDeliveries();
                    }
                }

                @Override
                public void onFailure(Call<DirectionMatrixResponse> call, Throwable t) {
                    AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, t);
                    Utils.hideCustomProgressDialog();
                    Log.d("call", "==== Failure =====" + t.getLocalizedMessage());
                }
            });*/
        }
    }

    /**
     * this method called webservice for get Data from LatLng which is provided by Google
     *
     * @param location on map
     */
    private void getGeocodeDataFromLocation(LatLng location) {
        MapEngine mapEngine = MapEngine.getInstance();
        ApplicationContext appContext = new ApplicationContext(getApplicationContext());
        mapEngine.init(appContext, new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(Error error) {
                if (error == OnEngineInitListener.Error.NONE) {

                    initComplete = true;
                    // Post initialization code goes here
                    Log.i("MapEngine init", error.toString());
                    GeoCoordinate geoCoordinate = new GeoCoordinate(location.latitude, location.longitude);
                    Log.i("geoCoordinate", geoCoordinate.toString());
                    reversegeocode_request(geoCoordinate);


                } else {
                    Utils.hideCustomProgressDialog();
                    // handle factory initialization failure
                    Log.i("MapEngine init", error.toString());
                }
            }
        });






        /*MapboxGeocoding reverseGeocode = MapboxGeocoding.builder()
                .accessToken(getResources().getString(R.string
                        .MAP_BOX_ACCESS_TOKEN))
                .query(Point.fromLngLat(location.longitude, location.latitude))
                .build();

        reverseGeocode.enqueueCall(new Callback<GeocodingResponse>() {
            @Override
            public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {

                if (response != null) {
                    List<CarmenFeature> results = response.body().features();
                    if (results.size() > 0) {
                        Log.d("TAG", "onResponse: " + response.body().toJson());
                        LatLng latLng = null;

                        if (response.body().query().size() > 0) {
                            latLng = new LatLng(Double.parseDouble(response.body().query().get(1).toString()), Double.parseDouble(response.body().query().get(0).toString()));
                            ParseContent.getInstance().address = currentAddress;
                            ParseContent.getInstance().latLng = new com.mapbox.mapboxsdk.geometry.LatLng(Double.parseDouble(response.body().query().get(1).toString()), Double.parseDouble(response.body().query().get(0).toString()));

                        }

                        if (response.body().features().size() > 0) {
                            if (response.body().features().get(0).placeName() != null && !response.body().features().get(0).placeName().equals("")) {
                                setAddressOnToolbar(currentAddress);
                                currentAddress = response.body().features().get(0).placeName();
                                currentBooking.setDeliveryAddress(currentAddress);

                            }
                        }


                        if (currentAddress != null) {
                            setAddressOnToolbar(currentAddress);
                        }

                        if (response.body().features().size() > 0) {
                            if (response.body().features().get(0).context().size() > 0) {
                                for (int i = 0; i < response.body().features().get(0).context().size(); i++) {
                                    if (response.body().features().get(0).context().get(i).id().contains("locality")) {
                                    } else if (response.body().features().get(0).context().get(i).id().contains("place")) {

                                    } else if (response.body().features().get(0).context().get(i).id().contains("district")) {
                                        if (response.body().features().get(0).context().get(i).text() != null) {
                                            str_arealvel_2 = response.body().features().get(0).context().get(i).text();
                                            str_locality = response.body().features().get(0).context().get(i).text();
                                        } else {
                                            str_arealvel_2 = "";
                                            str_locality = "";
                                        }
                                    } else if (response.body().features().get(0).context().get(i).id().contains("region")) {
                                        if (response.body().features().get(0).context().get(i).text() != null) {
                                            str_arealvel_1 = response.body().features().get(0).context().get(i).text();
                                        } else {
                                            str_arealvel_1 = "";
                                        }
                                    } else if (response.body().features().get(0).context().get(i).id().contains("country")) {
                                        if (response.body().features().get(0).context().get(i).shortCode() != null) {
                                            str_country_Code = Utils.capitaliseName(response.body().features().get(0).context().get(i).shortCode());

                                        } else {
                                            str_country_Code = "";
                                        }
                                        if (response.body().features().get(0).context().get(i).text() != null) {
                                            str_country = response.body().features().get(0).context().get(i).text();
                                        } else {
                                            str_country = "";
                                        }
                                    } else {

                                    }
                                }

                                Log.d("new", "====== OK ========" + str_country + " ... " + str_country_Code + " ... " + str_locality + " ... " + str_arealvel_1 + " ... " +
                                        str_arealvel_2 + " ... " + latLng + " ... " + currentAddress + " ... " + str_locality);
                                getDeliveryStoreInCity(str_country, str_country_Code, str_locality, str_arealvel_1,
                                        str_arealvel_2, latLng, currentAddress, str_locality);

                            }
                        } else {

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GeocodingResponse> call, Throwable t) {
                Toast.makeText(HomeActivity.this, "" + R.string.error_code_413, Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
    }

    private void getCart() {
        //Utils.showCustomProgressDialog(HomeActivity.this, false);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CartResponse> orderCall = apiInterface.getCart(ApiClient.makeJSONRequestBody
                (getCommonParam()));
        orderCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {


                if (response != null && parseContent.isSuccessful(response) && response.body() != null)
                    parseContent.parseCart(response);

                Utils.hideCustomProgressDialog();
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, t);
            }
        });

    }

    /**
     * this method load fragment according to delivery or category list
     */
    private void swipeFragmentAccordingToDeliveries() {
        // toolbar.setVisibility(View.GONE);
        //Utils.showCustomProgressDialog(HomeActivity.this,false);
        Log.i("swipeFragment", "show");
        try {
            //btnFreshchatFab.setVisibility(View.VISIBLE);
            updateUIBottomNavigationView(isCurrentLogin());
            Log.i("updateUIBottomNavigationView", "fromswipe");
            if (currentBooking != null && currentBooking.getDeliveryStoreList() != null && currentBooking.getDeliveryStoreList().size() > 0) {
                if (currentBooking.getDeliveryStoreList().size() == 1) {

                    goToStoreFragment(currentBooking.getDeliveryStoreList().get(0));
                    Log.i("updateUIBottomNavigationView", "goint store");
                } else {
                    goToHomeFragment();
                    Log.i("updateUIBottomNavigationView", "goint home");
                }
            } else {

                if (!Isusernotchecked) {
                    Log.i("bottomNavigationMenu", "isChecked");
                } else {
                    Log.i("bottomNavigationMenu", "isunChecked");
                }

                goToHomeFragment();
                Log.i("updateUIBottomNavigationView", "goint home1");
            }
            if (currentAddress != null) {
                setAddressOnToolbar(currentAddress);
            }
        } catch (Exception e) {
            Log.e("swipeFragmentAccordingToDeliveries() ", e.getMessage());
        }
        Utils.hideCustomProgressDialog();
        Log.i("showCustomProgressDialogb", "hide");
    }


    private void setAddressOnToolbar(String address) {
        if (address != null && !TextUtils.isEmpty(address)) {
            /*if (currentBooking.getDeliveryLatLng() != null){
                ParseContent.getInstance().address = address;
                ParseContent.getInstance().latLng =
                        new com.mapbox.mapboxsdk.geometry.LatLng(currentBooking.getDeliveryLatLng().latitude,currentBooking.getDeliveryLatLng().longitude);
            }*/
          /*  setTitleOnToolBar(getResources().getString(R.string
                    .text_ASAP) + " " + address);*/

        } else {
           /* setTitleOnToolBar(getResources().getString(R.string
                    .text_ASAP));*/
        }
    }

    private void getFavAddressList() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        } catch (JSONException e) {
            Log.e("getFavAddressList()", e.getMessage());
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<FavouriteAddressResponse> responseCall = apiInterface.getFavouriteAddressList(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<FavouriteAddressResponse>() {
            @Override
            public void onResponse(Call<FavouriteAddressResponse> call, Response<FavouriteAddressResponse>
                    response) {

                try {
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().getSuccess()) {
                        if (!response.body().getFavouriteAddresses().isEmpty()) {
                            currentBooking.setFavAddressList(response.body().getFavouriteAddresses());
                            favLocation.setLatitude(currentBooking.getFavAddressList().get(0).getLocation().get(0));
                            favLocation.setLongitude(currentBooking.getFavAddressList().get(0).getLocation().get(1));
                            ///getGeocodeDataFromLocation(new LatLng(favLocation.getLatitude(),favLocation.getLongitude()));
                        } else {
                            /* goToFavouriteAddressActivity(true);*/
                        }
                    } else {

                        Log.i("Utils.showErrorToast", "ok3");
                        Utils.showErrorToast(response.body().getErrorCode(), HomeActivity.this);
                    }
                } catch (Exception e) {
                }

                Utils.hideCustomProgressDialog();
            }

            @Override
            public void onFailure(Call<FavouriteAddressResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);

            }
        });
    }

    @Override
    protected void onResume() {
        toolbar.setVisibility(View.GONE);
        super.onResume();

        Log.i("getshowpopup", String.valueOf(preferenceHelper.getshowpopup()));
        /* if (preferenceHelper.getshowpopup()) {*/
        if (preferenceHelper.getpopmassage() != null && !preferenceHelper.getpopmassage().equals("")) {
            pendingpopupshow(preferenceHelper.getpopmassage());
        }
        /*}*/

        if (locationHelper != null) {
            locationHelper.onStart();

        }
        callSwingAnimationChat();

        updateUIBottomNavigationView(isCurrentLogin());
        //Log.i("updateUIBottomNavigationView","from resume");


    }

    private void reversegeocode_request(GeoCoordinate location) {
        Log.i("reversegeocode_request", "yes");
        ReverseGeocodeRequest revGecodeRequest = new ReverseGeocodeRequest(new GeoCoordinate(location));

        revGecodeRequest.execute(new ResultListener<com.here.android.mpa.search.Location>() {
            @Override
            public void onCompleted(com.here.android.mpa.search.Location location, ErrorCode errorCode) {
                if (errorCode == ErrorCode.NONE) {

//                        etSearch.setText(location.getAddress().toString().replace("\n", " "));
                    String myPlace = "";
                    if (location != null && location.getAddress() != null) {
                        if (!TextUtils.isEmpty(location.getAddress().getText())) {
                            myPlace = location.getAddress().getText();
                        }
                    }
                    setAddressOnToolbar(myPlace);
                    currentBooking.setDeliveryAddress(myPlace);
                    currentAddress = myPlace;
                    ParseContent.getInstance().address = currentAddress;
                    LatLng latLng = null;

                    if (location.getCoordinate() != null) {
                        latLng = new LatLng(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude());
                    }


                    if (!location.getAddress().getCountryName().isEmpty()) {
                        str_country = location.getAddress().getCountryName();
                    }
                    if (!location.getAddress().getCountryCode().isEmpty()) {
                        str_country_Code = location.getAddress().getCountryCode();
                    }
                    if (!location.getAddress().getCity().isEmpty()) {
                        str_locality = location.getAddress().getCity();
                    }


                    if (isCurrentLogin()) {
                        if (preferenceHelper.getIsPhoneNumberVerified()) {
                            getDeliveryStoreInCity(str_country, str_country_Code, str_locality, "",
                                    "", latLng, myPlace, str_locality);
                        }
                    } else if (!isCurrentLogin()) {

                        getDeliveryStoreInCity(str_country, str_country_Code, str_locality, "",
                                "", latLng, myPlace, str_locality);
                    }


                    Log.i("reversegeocode_request", "yes");
                } else {
                    Log.i("reversegeocode_request", errorCode.toString());
                    Utils.hideCustomProgressDialog();
                    currentBooking.setDeliveryAddress("error code:" + errorCode);
                    setAddressOnToolbar("error code:" + errorCode);

                }
            }
        });

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();


    }

    public void getdataifgpsoepnyes(Location Location) {


    }

}
