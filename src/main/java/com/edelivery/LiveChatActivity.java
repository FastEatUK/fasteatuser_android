package com.edelivery;

import androidx.appcompat.app.AppCompatActivity;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.edelivery.utils.PreferenceHelper;
import com.livechatinc.inappchat.ChatWindowConfiguration;
import com.livechatinc.inappchat.ChatWindowErrorType;
import com.livechatinc.inappchat.ChatWindowView;
import com.livechatinc.inappchat.ChatWindowView.ChatWindowEventsListener;
import com.livechatinc.inappchat.models.NewMessageModel;

import java.util.HashMap;
import java.util.Map;

import static com.edelivery.utils.Const.LOGIN_REQUEST;
import static com.edelivery.utils.Const.REQUEST_CHECK_SETTINGS;
import static com.livechatinc.inappchat.ChatWindowView.*;

public class LiveChatActivity extends AppCompatActivity /*implements ChatWindowEventsListener */ {

    ChatWindowView emmbeddedChatWindow;
    com.livechatinc.inappchat.ChatWindowConfiguration configuration;
    private Object ChatWindowEventsListener;
    String paramName = "", paramEmail = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_chat);

        emmbeddedChatWindow = findViewById(R.id.embedded_chat_window);

        getNameAndEmailForLiveChat();
        configureChatWindow();
        initializeAndImplementLiveChat();  //initialize chat

    }

    public void getNameAndEmailForLiveChat() {
        if (!PreferenceHelper.getInstance(getApplicationContext()).getUserId().equals("")) {
            paramName = PreferenceHelper.getInstance(getApplicationContext()).getFirstName() + " " + PreferenceHelper.getInstance(getApplicationContext()).getLastName();
            if (!PreferenceHelper.getInstance(getApplicationContext()).getEmail().equals("")) {
                paramEmail = PreferenceHelper.getInstance(getApplicationContext()).getEmail();
            }

        } else {
            paramName = "FastEat";
            paramEmail = "info@fasteat.biz";
        }

        Log.e("name", paramName + paramEmail);
    }

    public void configureChatWindow() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("KEY_LICENCE_NUMBER", getString(R.string.livechat_lisense_key));
        /*hashMap.put("KEY_GROUP_ID", group_id);*/
        hashMap.put("KEY_VISITOR_NAME",paramName);
        hashMap.put("KEY_VISITOR_EMAIL",paramEmail);
        Log.e("parameter", hashMap.toString());
        /* params.put(CUSTOM_PARAM_PREFIX + key, customVariables.get(key))*/

        configuration = new ChatWindowConfiguration(getString(R.string.livechat_lisense_key),"",paramName,paramEmail,hashMap);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /* if (emmbeddedChatWindow != null)*/
        emmbeddedChatWindow.onActivityResult(requestCode, resultCode, data);
        Log.e("ActivityResult", "ActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void initializeAndImplementLiveChat() {
        if (!emmbeddedChatWindow.isInitialized()) {
            emmbeddedChatWindow.setUpWindow(configuration);
            emmbeddedChatWindow.setUpListener(new ChatWindowEventsListener() {
                @Override
                public void onChatWindowVisibilityChanged(boolean visible) {
                    if (!visible) {
                        //for click event of minimize
                        finish();
                    }
                }

                @Override
                public void onNewMessage(NewMessageModel message, boolean windowVisible) {
                    Log.e("chatVisible2", "new_message");
                }

                @Override
                public void onStartFilePickerActivity(Intent intent, int requestCode) {
                    //for upload media in chat
                    intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("*/*");
                    startActivityForResult(intent, requestCode);
                    Log.e("chatVisible3", "pick_file");
                }

                @Override
                public boolean onError(ChatWindowErrorType errorType, int errorCode, String errorDescription) {
                    Log.e("chatVisibleError4", errorDescription);
                    return false;
                }

                @Override
                public boolean handleUri(Uri uri) {
                    Log.e("chatVisible5", uri.toString());
                    return false;
                }
            });
            emmbeddedChatWindow.initialize();

        }
        emmbeddedChatWindow.showChatWindow();


    }


}