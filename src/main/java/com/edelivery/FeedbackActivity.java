package com.edelivery;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;

import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Store;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

public class FeedbackActivity extends BaseAppCompatActivity implements TextView
        .OnEditorActionListener {
    private ImageView ivProviderImageFeedback;
    private CustomFontTextView tvProviderNameFeedback;
    private RatingBar ratingBarFeedback;
    private CustomFontEditTextView etFeedbackReview;
    private CustomFontButton btnSubmitFeedback;
    private Store store;
    private String orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_feedback));
        findViewById();
        setViewListener();
        loadData();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        ivProviderImageFeedback = (ImageView) findViewById(R.id
                .ivProviderImageFeedback);
        tvProviderNameFeedback = (CustomFontTextView) findViewById(R.id
                .tvProviderNameFeedback);
        ratingBarFeedback = (RatingBar) findViewById(R.id.ratingBarFeedback);
        etFeedbackReview = (CustomFontEditTextView) findViewById(R.id
                .etFeedbackReview);
        btnSubmitFeedback = (CustomFontButton) findViewById(R.id.btnSubmitFeedback);
    }

    @Override
    protected void setViewListener() {
        btnSubmitFeedback.setOnClickListener(this);
        tvSkip.setOnClickListener(this);
        etFeedbackReview.setOnEditorActionListener(this);
        ratingBarFeedback.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                ratingBarFeedback.setRating(v);
            }
        });
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.btnSubmitFeedback:
                submitRatting();
                break;
            case R.id.tvSkip:
                setResult(Activity.RESULT_OK);
                finish();
                break;
            default:
                // do with default
                break;
        }
    }

    private void submitRatting() {
        if (ratingBarFeedback.getRating() == 0) {
            Utils.showToast(getResources().getString(R.string.msg_plz_give_rating),
                    FeedbackActivity.this);
        } else {
            giveFeedback();
        }
    }


    /**
     * this method call a webservice for give feedback to delivery man
     */

    private void giveFeedback() {
        Utils.showCustomProgressDialog(this, false);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper
                    .getUserId());
            jsonObject.put(Const.Params.ORDER_ID, orderId);
            if (store == null) {
                jsonObject.put(Const.Params.USER_RATING_TO_PROVIDER, ratingBarFeedback
                        .getRating());
                jsonObject.put(Const.Params.USER_REVIEW_TO_PROVIDER, etFeedbackReview
                        .getText().toString());

                responseCall = apiInterface.setFeedbackProvider(ApiClient
                        .makeJSONRequestBody(jsonObject));
            } else {
                jsonObject.put(Const.Params.USER_RATING_TO_STORE, ratingBarFeedback
                        .getRating());
                jsonObject.put(Const.Params.USER_REVIEW_TO_STORE, etFeedbackReview
                        .getText().toString());

                responseCall = apiInterface.setFeedbackStore(ApiClient
                        .makeJSONRequestBody(jsonObject));
            }

            AppLog.Log("FEED_BACK_PARAM", jsonObject.toString());


            responseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    Utils.hideCustomProgressDialog();
                    if (parseContent.isSuccessful(response)) {
                        if (response != null && response.body() != null && response.body().isSuccess()) {
                            Utils.showMessageToast(response.body().getMessage(),
                                    FeedbackActivity.this);
                            setResult(Activity.RESULT_OK);
                            finish();
                        } else {
                            Utils.showErrorToast(response.body().getErrorCode(),
                                    FeedbackActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(OrderCompleteActivity.class.getName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(OrderCompleteActivity.class.getName(), e);
        }

    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        switch (textView.getId()) {
            case R.id.etFeedbackReview:
                if (i == EditorInfo.IME_ACTION_DONE) {
                    submitRatting();
                    return true;
                }
                break;
            default:
                //Do som thing
                break;
        }

        return false;
    }

    private void loadData() {
        if (getIntent() != null) {
            store = getIntent().getExtras().getParcelable(Const.SELECTED_STORE);
            orderId = getIntent().getExtras().getString(Const.Params.ORDER_ID);
            ratingBarFeedback.setRating(getIntent().getExtras().getFloat(Const.Params.RATING));
        }
        Glide.with(this).load(PreferenceHelper.getInstance(getActivity()).getIMAGE_BASE_URL() + store.getImageUrl()).dontAnimate()
                .placeholder
                        (ResourcesCompat.getDrawable(this
                                .getResources(), R.drawable.placeholder, null)).fallback
                (ResourcesCompat
                        .getDrawable
                                (this.getResources(), R.drawable.placeholder, null)).into
                (ivProviderImageFeedback);
        tvProviderNameFeedback.setText(store.getName());


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
