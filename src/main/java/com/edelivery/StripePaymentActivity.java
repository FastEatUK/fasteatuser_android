package com.edelivery;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.accounts.AuthenticatorException;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.edelivery.adapter.CardsliderAdapter;
import com.edelivery.adapter.InvoiceAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.Billingdetails;
import com.edelivery.models.datamodels.Card;
import com.edelivery.models.datamodels.Invoice;
import com.edelivery.models.datamodels.PaymentGateway;
import com.edelivery.models.datamodels.UpdateCardResponce;
import com.edelivery.models.responsemodels.AvailableProviderResponse;
import com.edelivery.models.responsemodels.CardsResponse;
import com.edelivery.models.responsemodels.CreateOrederResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.PaymentGatewayResponse;
import com.edelivery.models.responsemodels.PaymentIntent;
import com.edelivery.models.responsemodels.PaymentIntentResponce;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.CardRequirements;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentMethodTokenizationParameters;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.TransactionInfo;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.gson.JsonSyntaxException;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.GooglePayConfig;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.PaymentMethodCreateParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StripePaymentActivity extends BaseAppCompatActivity {

    private RecyclerView rcvInvoice, rcvcards;
    private Button addcard, paynow;
    private CustomFontTextViewTitle tvInvoiceOderTotal;
    public CustomFontTextView tvError;
    public List<PaymentGateway> paymentGateways = new ArrayList<>();
    public String clientSecret;
    public ArrayList<Card> cardList = new ArrayList<>();
    private Card selectCard;
    private Stripe mStripe;
    public static PaymentGateway gatewayItem;
    private Dialog addCardDialog, addCVVDialog;
    private CustomFontEditTextView etAddCVV;
    private CardsliderAdapter cardsliderAdapter;
    private LinearLayout Gogglepay, Checkoutdata;
    PaymentsClient paymentsClient;
    Boolean Is_googlepay = false, Is_cardpay = false;
    private boolean Is_Gpay = false;
    static boolean isComingFromCheckout;

    RelativeLayout rlGpay, rlRecycler;
    LinearLayout lltxtPayCard;


    ConfirmPaymentIntentParams cpip;
    PaymentMethod.BillingDetails billingDetails = null;
    String total = "";
   /* public StripePaymentActivity(PaymentsClient paymentsClient) {
        this.paymentsClient = paymentsClient;
    }*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stripe_payment);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_payments));
        logger = (AppEventsLogger) AppEventsLogger.newLogger(StripePaymentActivity.this);
        findViewById();
        setViewListener();
        getExtraData();
        getPaymentGateWays();


    }


    private void getPaymentGateWays() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();


        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper
                    .getUserId());
            jsonObject.put(Const.Params.COUNTRY, currentBooking.getCountry());
            jsonObject.put(Const.Params.COUNTRY_CODE, currentBooking.getCountryCode());
            jsonObject.put(Const.Params.COUNTRY_CODE_2, currentBooking.getCountryCode2());
            jsonObject.put(Const.Params.LATITUDE, currentBooking.getPaymentLatitude());
            jsonObject.put(Const.Params.LONGITUDE, currentBooking.getPaymentLongitude());
            jsonObject.put(Const.Params.CITY1, currentBooking.getCity1());
            jsonObject.put(Const.Params.CITY2, currentBooking.getCity2());
            jsonObject.put(Const.Params.CITY3, currentBooking.getCity3());
            jsonObject.put(Const.Params.CITY_CODE, currentBooking.getCityCode());
            jsonObject.put(Const.Params.TYPE, Const.Type.USER);
            jsonObject.put(Const.Params.VERSION, Const.Params.version_value);
            if (paynow.getVisibility() == View.VISIBLE) {
                jsonObject.put(Const.Params.CITY_ID, currentBooking.getCartCityId());
            } else {
                jsonObject.put(Const.Params.CITY_ID, "");
            }
            jsonObject.put(Const.Params.ORDER_PAYMENT_ID, currentBooking.getOrderPaymentId());
            jsonObject.put(Const.Params.AMOUNT_ID, currentBooking.getTotalInvoiceAmount());
            jsonObject.put(Const.Params.COUNTRY_CURRENCY, currentBooking.getCurrency());
            jsonObject.put(Const.Params.COUNTRY_CURRENCY_CODE, currentBooking.getCurrencyCode());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("getPaymentGateWays", jsonObject.toString());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentGatewayResponse> responseCall = apiInterface.getPaymentGateway(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<PaymentGatewayResponse>() {
            @Override
            public void onResponse(Call<PaymentGatewayResponse> call, Response<PaymentGatewayResponse> response) {
                Utils.hideCustomProgressDialog();
                AppLog.Log("PAYMENT_GATEWAY_responce", ApiClient.JSONResponse(response.body()));

                Log.e("error_main", String.valueOf(response.isSuccessful()));


                if (parseContent.isSuccessful(response)) {

                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        paymentGateways = new ArrayList<>();
                        currentBooking.setCurrencyCode(response.body().getWalletCurrencyCode());
                        setWalletAmount(response.body().getWalletAmount(), response.body()
                                .getWalletCurrencyCode());
                        if (response.body().getPaymentGateway() != null) {
                            paymentGateways.addAll(response.body().getPaymentGateway());
                        }



                       /* if (response.body().getPaymentIntent() != null
                                && !TextUtils.isEmpty(response.body().getPaymentIntent().getClient_secret())) {
                            clientSecret = response.body().getPaymentIntent().getClient_secret();
                        }*/
                        setSelectedPaymentGateway(getString(R.string.text_stripe));
                        cardList.clear();
                        if (response.body().getCards() != null) {
                            cardList.addAll(response.body().getCards());
                            for (int i = 0; i < cardList.size(); i++) {
                                Log.i("PAYMENT_GATEWAY_card", "Name:" + cardList.get(i).getCardHolderName());
                                Log.i("PAYMENT_GATEWAY_card", "Numbur:" + cardList.get(i).getCardNumber());
                                //   Log.i("","Numbur:"+cardList.get(i).get());
                            }


                            setselectedcard();
                            setcardslider();
                            /*rcvcards.setVisibility(View.VISIBLE);*/

                            if (!isComingFromCheckout) {
                                //coming from payment activity
                                paynow.setVisibility(View.GONE);
                            } else {

                            }
                        }

                    } else {

                        callDataHide();
                        Utils.showErrorToast(response.body().getErrorCode(), StripePaymentActivity.this);
                        if (!isComingFromCheckout) {
                            //coming from payment activity
                            paynow.setVisibility(View.GONE);
                        } else {

                        }
                    }


                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), StripePaymentActivity.this);
                    callDataHide();
                    if (!isComingFromCheckout) {
                        //coming from payment activity
                        paynow.setVisibility(View.GONE);
                    } else {

                    }
                }
            }

            @Override
            public void onFailure(Call<PaymentGatewayResponse> call, Throwable t) {


                Utils.hideCustomProgressDialog();

                callDataHide();

                if (!isComingFromCheckout) {
                    //coming from payment in setting

                } else {
                    //coming from checkout
                    try {
                        //callErrorFunction(throwable);

                        if (t instanceof SocketTimeoutException) {
                            Utils.showToast(getResources().getString(R.string.connection_timeout), getActivity());
                        } else if (t instanceof NetworkErrorException) {
                            Utils.showToast(getResources().getString(R.string.network_error), getActivity());
                        } else if (t instanceof AuthenticatorException) {
                            Utils.showToast(getResources().getString(R.string.authentiation_error), getActivity());
                        } else if (t instanceof NumberFormatException) {
                            Log.e("Failure", "amount_so_small");
                        } else if (t instanceof JsonSyntaxException) {
                            rlRecycler.setVisibility(View.GONE);
                            Log.e("Failure1", "amount_so_small");
                            String error1 = String.valueOf(t.getCause()).replace("For input string:", "");
                            String message = error1.replace("\"", "");
                            message = message.replace("java.lang.NumberFormatException:", "");
                            /* Utils.showToast(message, getActivity());*/
                            Log.e("message", message.toString());
                         /*  String find1 =   String.valueOf(t.getCause()).codePointCount(' ');
                           Log.e("find",find1);*/
                            tvError.setText(message);
                        } else {
                            Utils.showToast(getResources().getString(R.string.something_went_wrong), getActivity());
                        }


                    } catch (Exception e) {
                        Log.e("error_failure_main", e.toString());
                    }
                }

                   /*  paynow.setClickable(false);
                Gogglepay.setClickable(false);
                addcard.setClickable(false);*/


            }
        });

    }

    public void callDataHide() {
        if (!isComingFromCheckout) {
            //coming from payment in setting
            addcard.setClickable(true);
        } else {
            //coming from checkout
            rlGpay.setVisibility(View.GONE);
            rlRecycler.setVisibility(View.VISIBLE);
            lltxtPayCard.setVisibility(View.GONE);
            paynow.setVisibility(View.GONE);
        }
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        Checkoutdata = findViewById(R.id.LLforcheckeoutdata);
        rcvInvoice = (RecyclerView) findViewById(R.id.rcvInvoice);
        tvError = (CustomFontTextView) findViewById(R.id.tvError);
        rcvcards = (RecyclerView) findViewById(R.id.rcvcardslider);
        tvInvoiceOderTotal = (CustomFontTextViewTitle) findViewById(R.id.tvInvoiceOderTotal);
        addcard = (Button) findViewById(R.id.addcard);
        paynow = (Button) findViewById(R.id.btn_pay);
        Gogglepay = (LinearLayout) findViewById(R.id.g_paybutton);
        lltxtPayCard = (LinearLayout) findViewById(R.id.lltxtPayCard);

        rlGpay = (RelativeLayout) findViewById(R.id.rlGpay);
        rlRecycler = (RelativeLayout) findViewById(R.id.rlRecycler);


    }

    @Override
    protected void setViewListener() {

        addcard.setOnClickListener(this);
        paynow.setOnClickListener(this);
        Gogglepay.setOnClickListener(this);

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_pay:
                // gatewayItem.setId(Const.Payment.STRIPE);
                Is_Gpay = false;
                paynow.setClickable(false);
                Gogglepay.setClickable(false);
                addcard.setClickable(false);

                if (!isComingFromCheckout) {
                    //coming from payment in setting
                } else {
                    //coming from checkout
                    checkAvailableProvider();
                }

                break;

            case R.id.addcard:
                goforaddcard();
                break;

            case R.id.g_paybutton:
                // gatewayItem.setId(Const.Payment.G_PAY);
                Is_Gpay = true;
                paynow.setClickable(false);
                addcard.setClickable(false);
                Gogglepay.setClickable(false);

                if (!isComingFromCheckout) {
                    //coming from payment in setting
                } else {
                    //coming from checkout
                    checkAvailableProvider();
                }

                Log.i("Googlepayclick", "ok");

                break;

            default:
                break;
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void getExtraData() {
        if (getIntent() != null) {

            if (getIntent().getSerializableExtra(Const.Tag.PAYMENT_ACTIVITY_Invoice) != null) {
                ArrayList<Invoice> myList = (ArrayList<Invoice>) getIntent().getSerializableExtra(Const.Tag.PAYMENT_ACTIVITY_Invoice);
           /* Log.i("invoice",myList.get(0).getTitle());
            Log.i("invoice",myList.get(1).getTitle());*/
                rcvInvoice.setLayoutManager(new LinearLayoutManager(this));
                rcvInvoice.setNestedScrollingEnabled(false);
                rcvInvoice.setAdapter(new InvoiceAdapter(myList));
                Checkoutdata.setVisibility(View.VISIBLE);
                setTvInvoiceOderTotal();
            }
            if (!getIntent().getExtras().getBoolean(Const.Tag.PAYMENT_ACTIVITY)) {
                isComingFromCheckout = getIntent().getExtras().getBoolean(Const.Tag.PAYMENT_ACTIVITY);
                if (!isComingFromCheckout) {
                    //coming from payment activity
                    paynow.setVisibility(View.GONE);
                    Checkoutdata.setVisibility(View.GONE);
                    getAllCards();
                    rlGpay.setVisibility(View.GONE);
                    lltxtPayCard.setVisibility(View.GONE);

                }
            } else {
                //coming from checkout screen

                //condition for zero
                if (String.valueOf(currentBooking.getTotalInvoiceAmount()).equals("0") ||
                        String.valueOf(currentBooking.getTotalInvoiceAmount()).equals("0.0")
                        || String.valueOf(currentBooking.getTotalInvoiceAmount()).equals("0.00")
                        || (int) (currentBooking.getTotalInvoiceAmount()) < 1) {

                    rlGpay.setVisibility(View.GONE);
                    rlRecycler.setVisibility(View.INVISIBLE);
                    lltxtPayCard.setVisibility(View.GONE);

                } else {
                    rlRecycler.setVisibility(View.VISIBLE);
                    getAllCards();
                }

                isComingFromCheckout = getIntent().getExtras().getBoolean(Const.Tag.PAYMENT_ACTIVITY);
            }


        }

    }

    private void setTvInvoiceOderTotal() {
        total = /*currentBooking.getCurrency()*/"£ " + parseContent.decimalTwoDigitFormat.format
                (currentBooking.getTotalInvoiceAmount());
        //Log.i("setTvInvoiceOderTotal",currentBooking.getCurrency());
        tvInvoiceOderTotal.setText(total);
        paynow.setText("Pay" + " " + total);


    }

    private void setWalletAmount(double amount, String currency) {
        preferenceHelper.putWalletAmount((float) amount);
        preferenceHelper.putWalletCurrencyCode(currency);
        // tvWalletAmount.setText(parseContent.decimalTwoDigitFormat.format(amount) + " " + currency);
    }

    private void setcardslider() {

        int width = getApplicationContext().getResources().getDimensionPixelSize(R.dimen._35sdp);
        if (cardList.isEmpty()) {
            //coming from checkout
            if (String.valueOf(currentBooking.getTotalInvoiceAmount()).equals("0") ||
                    String.valueOf(currentBooking.getTotalInvoiceAmount()).equals("0.0")
                    || String.valueOf(currentBooking.getTotalInvoiceAmount()).equals("0.00")
                    || (int) (currentBooking.getTotalInvoiceAmount()) < 1) {

                if (!isComingFromCheckout) {
                    //coming from payment screen in setting
                    paynow.setVisibility(View.GONE);
                    addcard.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, addcard.getLayoutParams().height));

                } else {
                    //coming from checkout screen
                    paynow.setVisibility(View.VISIBLE);
                }
            } else {
                Utils.showToast("Please add at least one card", StripePaymentActivity.this);
                addcard.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, addcard.getLayoutParams().height));
                paynow.setVisibility(View.GONE);
            }


        } else {
            addcard.setLayoutParams(new RelativeLayout.LayoutParams(width, addcard.getLayoutParams().height));
            if (!isComingFromCheckout) {
                //coming from payment screen in setting
                paynow.setVisibility(View.GONE);
            } else {
                //coming from checkout screen
                paynow.setVisibility(View.VISIBLE);
            }
        }
        if (cardsliderAdapter != null) {
            cardsliderAdapter.notifyDataSetChanged();
        } else {
            rcvcards.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            cardsliderAdapter = new CardsliderAdapter(cardList, StripePaymentActivity.this, getApplicationContext());
            rcvcards.setAdapter(cardsliderAdapter);

        }
    }

    private void setselectedcard() {
        selectCard = null;
        for (Card card : cardList) {
            if (card.getIsDefault()) {
                selectCard = card;
                return;
            }
        }
    }

    private void setSelectedPaymentGateway(String paymentName) {

        for (PaymentGateway gatewayItem : paymentGateways) {
            if (TextUtils.equals(gatewayItem.getName(), paymentName)) {
                this.gatewayItem = gatewayItem;
                if (this.gatewayItem != null && this.gatewayItem.getName().equalsIgnoreCase(getString(R.string.text_stripe))) {

                    /*     String mStripe_Pk = this.gatewayItem.getPaymentKeyId();*/
                    /* String mStripe_Pk = "pk_test_XwYfEHXj88Z7IhdTZSgUfOtx";*/
                    String mStripe_Pk = "pk_live_LHfXVcvWm8klCp4dEjGJw1LT";
                    try {
                        PaymentConfiguration.init(StripePaymentActivity.this, mStripe_Pk);
                        mStripe = new Stripe(StripePaymentActivity.this, PaymentConfiguration.getInstance(StripePaymentActivity.this).getPublishableKey());
                    } catch (Exception e) {
                    }
                }
                return;
            }

        }
    }

    private void checkAvailableProvider() {
        Utils.showCustomProgressDialog(this, false);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.LATITUDE, currentBooking.getStoreLocation().get(0));
            jsonObject.put(Const.Params.LONGITUDE, currentBooking.getStoreLocation().get(1));
            jsonObject.put(Const.Params.CITY_ID, currentBooking.getBookCityId());
            jsonObject.put(Const.Params.STORE_ID, currentBooking.getSelectedStoreId());
            if (gatewayItem == null) {
                jsonObject.put(Const.Params.IS_PAYMENT_MODE_CASH, false);
            } else {
                jsonObject.put(Const.Params.IS_PAYMENT_MODE_CASH, gatewayItem.isPaymentModeCash());
            }
            //  jsonObject.put(Const.Params.IS_PAYMENT_MODE_CASH,gatewayItem.isPaymentModeCash());///* gatewayItem.isPaymentModeCash()*/

        } catch (JSONException e) {
            AppLog.handleException(PaymentActivity.class.getName(), e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AvailableProviderResponse> responseCall = apiInterface.checkStoreProviderAvailable(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<AvailableProviderResponse>() {
            @Override
            public void onResponse(Call<AvailableProviderResponse> call, Response<AvailableProviderResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().getSuccess()) {
                    if (!response.body().getProviders().isEmpty()) {
                        // createOrder();
                        //
                        //for zero amount
                        if (String.valueOf(currentBooking.getTotalInvoiceAmount()).equals("0") ||
                                String.valueOf(currentBooking.getTotalInvoiceAmount()).equals("0.0")
                                || String.valueOf(currentBooking.getTotalInvoiceAmount()).equals("0.00")
                                || (int) (currentBooking.getTotalInvoiceAmount()) < 1) {

                            payForZeroAmount();

                        } else {
                            checkPaymentGatewayForPayOrder();
                        }


                    } else {
                        paynow.setClickable(true);
                        addcard.setClickable(true);
                        Gogglepay.setClickable(true);
                        Utils.showToast(getResources().getString(R.string
                                .msg_no_provider_found), StripePaymentActivity.this);
                    }
                } else {
                    paynow.setClickable(true);
                    addcard.setClickable(true);
                    Gogglepay.setClickable(true);
                    Utils.showToast(getResources().getString(R.string
                            .msg_no_provider_found), StripePaymentActivity.this);
                }

            }

            @Override
            public void onFailure(Call<AvailableProviderResponse> call, Throwable t) {

                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                try {
                    callErrorFunction(t);
                } catch (Exception e) {
                    Log.e("error_failure_main", e.toString());
                }


            }
        });

    }

    private void setClientSecret() {

    }


    private void checkPaymentGatewayForPayOrder() {
        if (gatewayItem != null) {
            switch (gatewayItem.getId()) {
                case Const.Payment.STRIPE:
                    if (Is_Gpay) {
                        callgooglepay();
                    } else {
                        // Log.i("Const.Payment.STRIPE", "clientSecret :" + clientSecret + "\n" + "card number:" + selectCard.getCardNumber() + "\n" + "name:" + selectCard.getCardHolderName());
                        Log.i("Const.Payment.STRIPE", gatewayItem.getPaymentKeyId());
                        if (selectCard != null && !TextUtils.isEmpty(selectCard.getCardNumber())
                                && !TextUtils.isEmpty(selectCard.getCardExpiryDate())) {

                            String cardMonth = "", cardYear = "";
                            if (selectCard.getCardExpiryDate().contains(",")) {
                                String cardArray[] = selectCard.getCardExpiryDate().split(",");
                                if (cardArray.length == 2) {
                                    cardMonth = cardArray[0];
                                    if (!TextUtils.isEmpty(cardArray[1])) {
                                        if (cardArray[1].length() == 4) {
                                            cardYear = cardArray[1].substring(2, 4);
                                        } else {
                                            cardYear = cardArray[1];
                                        }
                                    }
                                }
                            } else if (selectCard.getCardExpiryDate().contains("/")) {
                                String cardArray[] = selectCard.getCardExpiryDate().split("/");
                                if (cardArray.length == 2) {
                                    cardMonth = cardArray[0];
                                    if (!TextUtils.isEmpty(cardArray[1])) {
                                        if (cardArray[1].length() == 2) {
                                            cardYear = cardArray[1];
                                        } else {
                                            cardYear = cardArray[1];
                                        }
                                    }
                                }
                            }

                            if (!TextUtils.isEmpty(cardMonth) && !TextUtils.isEmpty(cardYear)) {

                                if (!TextUtils.isEmpty(selectCard.getCardcvv())) {
                                    MakePaymentWithCardAftervalidate(cardMonth, cardYear, selectCard.getCardcvv());
                                } else {
                                    openAddCVVDialog(cardMonth, cardYear);
                                    Utils.showToast("Card Cvv not found Please Enter cvv", StripePaymentActivity.this);
                                }

                            } else {
                                paynow.setClickable(true);
                                addcard.setClickable(true);
                                Gogglepay.setClickable(true);
                                Utils.showToast(getString(R.string.msg_card_alert), StripePaymentActivity.this);
                            }

                        } else {
                            paynow.setClickable(true);
                            addcard.setClickable(true);
                            Gogglepay.setClickable(true);
                            Utils.showToast(getString(R.string.msg_number_invalid), StripePaymentActivity.this);
                        }
                    }
                    break;

              /*  case Const.Payment.G_PAY:
                    Log.i("Const.Payment.G_PAY", "ok");
                    callgooglepay();
                    break;*/

                default:
                    break;

            }
        } else {
            Utils.showToast(getString(R.string.something_went_wrong), getActivity());
        }
    }

    private void openAddCVVDialog(final String cardMonth, final String cardYear) {
        if (addCVVDialog != null && addCVVDialog.isShowing()) {
            return;
        }
        addCVVDialog = new Dialog(StripePaymentActivity.this);
        addCVVDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addCVVDialog.setContentView(R.layout.dialog_opencvv);
        etAddCVV = (CustomFontEditTextView) addCVVDialog.findViewById(R.id.etCVV);
        addCVVDialog.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etAddCVV.getText().toString().trim())) {
                    Utils.showToast(getString(R.string.msg_enter_cvv),
                            StripePaymentActivity.this);
                    etAddCVV.requestFocus();
                } else {
                    addCVVDialog.dismiss();
                    hideCVVdialogkeyboard();
                    hideAllViewkeyboard();
                    Utils.hideSoftKeyboard(StripePaymentActivity.this);
                    MakePaymentWithCardAftervalidate(cardMonth, cardYear, etAddCVV.getText().toString());
                    UpdateCardInformation(etAddCVV.getText().toString());

                }
            }
        });
        addCVVDialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCVVDialog.dismiss();
                hideCVVdialogkeyboard();
                hideAllViewkeyboard();
                Utils.hideSoftKeyboard(StripePaymentActivity.this);
            }
        });
        WindowManager.LayoutParams params = addCVVDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        addCVVDialog.getWindow().setAttributes(params);
        addCVVDialog.setCancelable(false);
        addCVVDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        addCVVDialog.show();
    }

    void MakePaymentWithCardAftervalidate(String cardMonth, String cardYear, String Cvv) {


        billingDetails = new PaymentMethod.BillingDetails.Builder().

                setEmail(preferenceHelper.getEmail()).
                setName(preferenceHelper.getFirstName() + " " + preferenceHelper.getLastName()).
                setPhone(preferenceHelper.getPhoneNumber()).
                build();


        com.stripe.android.model.Card mCard = new com.stripe.android.model.Card
                .Builder(selectCard.getCardNumber(),
                Integer.parseInt(cardMonth),
                Integer.parseInt(cardYear),
                Cvv)
                .build();

                    /*Log.e("tagCard", "selectCard.getCard_number() is " + selectCard.getCard_number());
                    Log.e("tagCard", "selectCard.cardMonth() is " + cardMonth);
                    Log.e("tagCard", "selectCard.cardYear() is " + cardYear);
                    Log.e("tagCard", "etAddCVV is " + etAddCVV.getText().toString());*/

        if (mCard.validateCard()) {
            Log.i("tagCard", "card is validated");
            Utils.showCustomProgressDialog(StripePaymentActivity.this, false);


            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put(Const.Params.TYPE, Const.Type.USER);
                jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
                jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
                jsonObject.put(Const.Params.AMOUNT_ID, currentBooking.getTotalInvoiceAmount());
                //  jsonObject.put(Const.Params.TEST, Const.Params.version_value);
                jsonObject.put(Const.Params.COUNTRY_CURRENCY_CODE, currentBooking.getCurrencyCode());
            } catch (JSONException e) {
                AppLog.handleException(PaymentActivity.class.getName(), e);
            }
            Log.i("setClientSecret", jsonObject.toString());

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<PaymentIntentResponce> responseCall = apiInterface.getpaymentintent(ApiClient
                    .makeJSONRequestBody(jsonObject));

            responseCall.enqueue(new Callback<PaymentIntentResponce>() {
                @Override
                public void onResponse(Call<PaymentIntentResponce> call, Response<PaymentIntentResponce> response) {

                    AppLog.Log("setClientSecret_responce", ApiClient.JSONResponse(response.body()));
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            if (response.body().getPaymentIntent().getClientSecret() != null) {

                                clientSecret = response.body().getPaymentIntent().getClientSecret();


                                mStripe.confirmPayment(StripePaymentActivity.this,
                                        ConfirmPaymentIntentParams
                                                .createWithPaymentMethodCreateParams(
                                                        PaymentMethodCreateParams.create(mCard.toPaymentMethodParamsCard(), billingDetails),
                                                        clientSecret, Const.STRIPE_RETURN_URL));


                            }
                        } else {
                            Utils.hideCustomProgressDialog();
                            paynow.setClickable(true);
                            addcard.setClickable(true);
                            Gogglepay.setClickable(true);
                            Utils.showErrorToast(response.body().getMessage(), StripePaymentActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<PaymentIntentResponce> call, Throwable t) {
                    AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                    Utils.hideCustomProgressDialog();
                    try {
                        callErrorFunction(t);
                    } catch (Exception e) {
                        Log.e("error_failure_main", e.toString());
                    }
                }
            });


            Log.i("tagCard", billingDetails.email);
            // Utils.hideCustomProgressDialog();

            //  payOrderPayment();
        } else {
            Utils.showToast(getString(R.string.msg_card_invalid), StripePaymentActivity.this);
        }
    }

    void UpdateCardInformation(String cvv) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.TYPE, Const.Type.USER);
            jsonObject.put(Const.Params.CARD_ID, selectCard.getId());
            jsonObject.put(Const.Params.CARD_CVV, cvv);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("UpdateCardInformation", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UpdateCardResponce> responseCall = apiInterface.Updateoldcard(ApiClient
                .makeJSONRequestBody(jsonObject));

        responseCall.enqueue(new Callback<UpdateCardResponce>() {
            @Override
            public void onResponse(Call<UpdateCardResponce> call, Response<UpdateCardResponce> response) {
                Utils.hideCustomProgressDialog();

                if (response.body().getSuccess()) {
                    AppLog.Log("UpdateCardInformation_responce", ApiClient.JSONResponse(response.body()));
                } else {
                    paynow.setClickable(true);
                    addcard.setClickable(true);
                    Gogglepay.setClickable(true);
                    Utils.showToast("Card Information is Not Updated", StripePaymentActivity.this);
                }


            }

            @Override
            public void onFailure(Call<UpdateCardResponce> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                Utils.hideCustomProgressDialog();
                try {
                    callErrorFunction(t);
                } catch (Exception e) {
                    Log.e("error_failure_main", e.toString());
                }

            }
        });


    }

    private void hideCVVdialogkeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            if (etAddCVV != null) {
                imm.hideSoftInputFromWindow(etAddCVV.getWindowToken(), 0);
            }
        }
    }

    private void hideAllViewkeyboard() {
        View view = StripePaymentActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
        if (getWindow() != null) {
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
        }
        if (addCVVDialog != null && addCVVDialog.getWindow() != null) {
            addCVVDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == Const.LOAD_PAYMENT_DATA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {

                    try {
                        onGooglePayResult(data);

                        Log.i("onGooglePayResult", "ok");
                    } catch (JSONException e) {
                        Log.i("onGooglePayResult", e.toString());
                        e.printStackTrace();
                    }

                }


            } else if (resultCode == Activity.RESULT_CANCELED) {
                Utils.showToast(getString(R.string.error_payment_google), StripePaymentActivity.this);
            } else if (requestCode == AutoResolveHelper.RESULT_ERROR) {
                final Status status =
                        AutoResolveHelper.getStatusFromIntent(data);

                Log.d("LOAD_PAYMENT_DATA_REQUEST_CODE", status.getStatusMessage());
                Utils.showToast(getString(R.string.error_payment_google), StripePaymentActivity.this);

            }
        }
        if (requestCode == Const.ADD_STRIPE_CARD) {
            if (resultCode == Activity.RESULT_OK) {
                Log.i("ADD_STRIPE_CARD", "ok");
                getAllCards();
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
        if (mStripe != null) {
            mStripe.onPaymentResult(requestCode, data, new ApiResultCallback<PaymentIntentResult>() {
                @Override
                public void onSuccess(@NonNull PaymentIntentResult result) {
                    if (!Utils.isInternetConnected(StripePaymentActivity.this)) {
                        Utils.hideCustomProgressDialog();
                        openInternetDialog(StripePaymentActivity.this);
                    } else {
                        Log.e("tagCard", "transaction id is " + result.getIntent().getId());
                        //   payOrderPayment(result.getIntent().getId());


                        payOrderPayment(result.getIntent().getId());


                    }
                }

                @Override
                public void onError(@NonNull Exception e) {
                    Utils.hideCustomProgressDialog();
                    Utils.showToast(e.getMessage(), getActivity());
//                    Log.e("tagCard", "error while doing payment " + e.getMessage());
                }
            });
        } else {
            Utils.hideCustomProgressDialog();
        }

    }

    private void payForZeroAmount() {
        if (gatewayItem != null) {
            Utils.showCustomProgressDialog(this, false);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
                jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
                jsonObject.put(Const.Params.IS_PAYMENT_MODE_CASH, false);
                jsonObject.put(Const.Params.PAYMENT_ID, gatewayItem.getId());
                jsonObject.put(Const.Params.ORDER_PAYMENT_ID, currentBooking.getOrderPaymentId());
                jsonObject.put(Const.Params.TRANSACTION_ID, "StripeID");

                jsonObject.put(Const.Params.CART_ID, currentBooking.getCartId());

            } catch (JSONException e) {
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
            }

            Log.i("payOrderPayment", jsonObject.toString());

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<IsSuccessResponse> responseCall = apiInterface.payOrderUsingZeroPayment(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    Utils.hideCustomProgressDialog();
                    AppLog.Log("payOrderPayment_responce", ApiClient.JSONResponse(response.body()));
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        //   if (response.body().isPaymentPaid()) {
                        Utils.showMessageToast(response.body().getMessage(), StripePaymentActivity
                                .this);

                        logAddedPaymentInfoEvent(true);
                        Utils.showMessageToast(response.body().getMessage(), StripePaymentActivity.this);
                        preferenceHelper.putAndroidId(Utils.generateRandomString());
                        currentBooking.clearCart();
                        currentBooking.setFutureOrder(false);
                        String orderId = response.body().getOrderId();
                        String storeName = response.body().getStoreName();
                       /* Log.i("response.body()", storeName);
                        Log.i("response.body()", orderId);*/
                        goToThankYouActivity(orderId, storeName);

                        //  createOrder();
                       /* } else {
                            Utils.showToast(getResources().getString(R.string
                                    .msg_payment_flied), StripePaymentActivity.this);
                        }*/

                    } else {

                        Utils.showErrorToast(response.body().getErrorCode(), StripePaymentActivity.this);
                    }

                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    try {
                        callErrorFunction(t);
                    } catch (Exception e) {
                        Log.e("error_failure_main", e.toString());
                    }
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                }
            });
        }
    }

    private void payOrderPayment(String transactionID) {
        if (gatewayItem != null && !TextUtils.isEmpty(transactionID)) {
            Utils.showCustomProgressDialog(this, false);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
                jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
                if (gatewayItem == null) {
                    jsonObject.put(Const.Params.IS_PAYMENT_MODE_CASH, false);
                } else {
                    jsonObject.put(Const.Params.IS_PAYMENT_MODE_CASH, gatewayItem.isPaymentModeCash());
                }
                /* jsonObject.put(Const.Params.IS_PAYMENT_MODE_CASH, gatewayItem.isPaymentModeCash());*/
                jsonObject.put(Const.Params.PAYMENT_ID, gatewayItem.getId());
                jsonObject.put(Const.Params.ORDER_PAYMENT_ID, currentBooking
                        .getOrderPaymentId());
                jsonObject.put(Const.Params.TRANSACTION_ID, transactionID);

                jsonObject.put(Const.Params.CART_ID, currentBooking.getCartId());

            } catch (JSONException e) {
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
            }

            Log.i("payOrderPayment", jsonObject.toString());

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<IsSuccessResponse> responseCall = apiInterface.payOrderPayment(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    Utils.hideCustomProgressDialog();
                    AppLog.Log("payOrderPayment_responce", ApiClient.JSONResponse(response.body()));
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        //   if (response.body().isPaymentPaid()) {
                        Utils.showMessageToast(response.body().getMessage(), StripePaymentActivity
                                .this);

                        logAddedPaymentInfoEvent(true);
                        Utils.showMessageToast(response.body().getMessage(), StripePaymentActivity.this);
                        preferenceHelper.putAndroidId(Utils.generateRandomString());
                        currentBooking.clearCart();
                        currentBooking.setFutureOrder(false);
                        String orderId = response.body().getOrderId();
                        String storeName = response.body().getStoreName();
                        Log.i("response.body()", storeName);
                        Log.i("response.body()", orderId);
                        goToThankYouActivity(orderId, storeName);

                        //  createOrder();
                       /* } else {
                            Utils.showToast(getResources().getString(R.string
                                    .msg_payment_flied), StripePaymentActivity.this);
                        }*/

                    } else {

                        try {

                            if (response.body() != null) {
                                Utils.showErrorToast(response.body().getErrorCode(), StripePaymentActivity.this);
                            }
                        } catch (Exception e) {

                        }


                    }

                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                    try {
                        callErrorFunction(t);
                    } catch (Exception e) {
                        Log.e("error_failure_main", e.toString());
                    }

                }
            });
        }
    }

    public void logAddedPaymentInfoEvent(boolean success) {
        Bundle params = new Bundle();
        params.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, success ? 1 : 0);
        logger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_PAYMENT_INFO, params);

    }

   /* private void createOrder() {
        Utils.showCustomProgressDialog(this, false);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.CART_ID, currentBooking.getCartId());

            if (currentBooking.isFutureOrder()) {
                jsonObject.put(Const.Params.IS_SCHEDULE_ORDER, currentBooking.isFutureOrder());
                jsonObject.put(Const.Params.ORDER_START_AT, currentBooking
                        .getFutureOrderSelectedTimeUTCMillis());
            }

        } catch (JSONException e) {
            AppLog.handleException(PaymentActivity.class.getName(), e);
        }

        AppLog.Log("CREATE_ORDER", jsonObject.toString());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CreateOrederResponse> responseCall = apiInterface.createOrder(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<CreateOrederResponse>() {
            @Override
            public void onResponse(Call<CreateOrederResponse> call, Response<CreateOrederResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().getSuccess()) {

                    logPurchasedEvent(currentBooking.getCartProductItemWithAllSpecificationList().size(), "", "", Const.Facebook.FB_CURRENCY_CODE, currentBooking.getTotalInvoiceAmount());

                    Utils.showMessageToast(response.body().getMessage(), StripePaymentActivity
                            .this);
                    preferenceHelper.putAndroidId(Utils.generateRandomString());
                    currentBooking.clearCart();
                    currentBooking.setFutureOrder(false);
                    String orderId = response.body().getOrderId();
                    String storeName = response.body().getStoreName();
                    logPurchasedEvent(currentBooking.getCartProductItemWithAllSpecificationList().size(), "", "", Const.Facebook.FB_CURRENCY_CODE, currentBooking.getTotalInvoiceAmount());
                    goToThankYouActivity(orderId, storeName);
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), StripePaymentActivity
                            .this);
                }

            }

            @Override
            public void onFailure(Call<CreateOrederResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
            }
        });
    }*/

    // After Purchased Order add facebook event
    public void logPurchasedEvent(int numItems, String contentType, String contentId, String currency, double price) {
        Bundle params = new Bundle();
        params.putInt(AppEventsConstants.EVENT_PARAM_NUM_ITEMS, numItems);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);

        if (currency != null && !currency.equals("")) {
            Log.d("currency", "--------- currency ----------" + currency);
            logger.logPurchase(BigDecimal.valueOf(price), Currency.getInstance(currency), params);
        }

    }

    private void goToThankYouActivity(String orderId, String storeName) {
        paynow.setClickable(true);
        addcard.setClickable(true);
        Gogglepay.setClickable(true);
        Intent intent = new Intent(this, ThankYouActivity.class);
        intent.putExtra(Const.Params.ORDER_ID, orderId);
        intent.putExtra(Const.Params.STORE_ID, storeName);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void openDeleteCard(final int position) {
        CustomDialogAlert customDialogAlert = new CustomDialogAlert(StripePaymentActivity.this, getApplicationContext()
                .getResources()
                .getString(R.string.text_delete_card), getApplicationContext().getResources()
                .getString(R.string.msg_are_you_sure), getApplicationContext().getResources()
                .getString(R.string.text_cancel), getApplicationContext().getResources()
                .getString(R.string.text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                dismiss();
                deleteCreditCard(position);

            }
        };
        customDialogAlert.show();

    }

    private void deleteCreditCard(final int deleteCardPosition) {
        Utils.showCustomProgressDialog(StripePaymentActivity.this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, this.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.USER_ID, this
                    .preferenceHelper
                    .getUserId());
            jsonObject.put(Const.Params.CARD_ID, cardList.get
                    (deleteCardPosition).getId());
            jsonObject.put(Const.Params.TYPE, Const.Type.USER);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.deleteCreditCard(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    if (cardList.get(deleteCardPosition).getIsDefault()) {
                        cardList.remove(deleteCardPosition);
                        if (!cardList.isEmpty()) {
                            selectCreditCard(0);
                        } else {
                            setselectedcard();
                            setcardslider();
                        }
                    } else {
                        cardList.remove(deleteCardPosition);
                        setselectedcard();
                    }
                    cardsliderAdapter.notifyDataSetChanged();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), StripePaymentActivity.this);
                }

            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                try {
                    callErrorFunction(t);
                } catch (Exception e) {
                    Log.e("error_failure_main", e.toString());
                }

            }
        });

    }

    public void selectCreditCard(final int selectedCardPosition) {
        Utils.showCustomProgressDialog(StripePaymentActivity.this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, this.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.USER_ID, this
                    .preferenceHelper
                    .getUserId());

            jsonObject.put(Const.Params.CARD_ID, cardList.get
                    (selectedCardPosition).getId());
            jsonObject.put(Const.Params.TYPE, Const.Type.USER);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CardsResponse> responseCall = apiInterface.selectCreditCard(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<CardsResponse>() {
            @Override
            public void onResponse(Call<CardsResponse> call, Response<CardsResponse> response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    for (Card card : cardList) {
                        card.setIsDefault(false);
                    }
                    cardList.get(selectedCardPosition).setIsDefault(response.body()
                            .getCard()
                            .getIsDefault());
                    setselectedcard();
                    cardsliderAdapter.notifyDataSetChanged();
                } /*else {
                    // error code 1016  hide the mesage
                     Utils.showErrorToast(response.body().getErrorCode(), paymentActivity);
                }*/
            }

            @Override
            public void onFailure(Call<CardsResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                try {
                    callErrorFunction(t);
                } catch (Exception e) {
                    Log.e("error_failure_main", e.toString());
                }

            }
        });
    }

    public void goforaddcard() {
        Intent intent = new Intent(StripePaymentActivity.this, AddStripeCardActivity.class);
        startActivityForResult(intent, Const.ADD_STRIPE_CARD);
    }

    private void getAllCards() {
        Utils.showCustomProgressDialog(StripePaymentActivity.this, false);

        JSONObject hashMap = new JSONObject();
        try {
            hashMap.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            hashMap.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

        Log.i("getAllCards", hashMap.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CardsResponse> responseCall = apiInterface.getAllCreditCards(ApiClient
                .makeJSONRequestBody(hashMap));
        responseCall.enqueue(new Callback<CardsResponse>() {
            @Override
            public void onResponse(Call<CardsResponse> call, Response<CardsResponse> response) {

                AppLog.Log("getAllCards_responce", ApiClient.JSONResponse(response.body()));
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    cardList.clear();
                    cardList.addAll(response.body().getCards());
                    setselectedcard();
                    setcardslider();
                } else {
                    setcardslider();
                    Utils.showErrorToast(response.body().getErrorCode(), StripePaymentActivity.this);
                    if (!isComingFromCheckout) {
                        //coming from payment activity
                        paynow.setVisibility(View.GONE);
                    } else {

                    }

                }

            }

            @Override
            public void onFailure(Call<CardsResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                if (!isComingFromCheckout) {
                    //coming from payment activity
                    paynow.setVisibility(View.GONE);
                } else {

                }
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                try {
                    callErrorFunction(t);
                } catch (Exception e) {
                    Log.e("error_failure_main", e.toString());
                }

            }
        });

    }


    void callgooglepay() {
        paymentsClient = Wallet.getPaymentsClient(StripePaymentActivity.this, new Wallet.WalletOptions.Builder()
                .setEnvironment(WalletConstants.ENVIRONMENT_PRODUCTION)
                .build());


        IsReadyToPayRequest request = null;
        try {
            request = createIsReadyToPayRequest();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        IsReadyToPayRequest finalRequest = request;
        paymentsClient.isReadyToPay(request)
                .addOnCompleteListener(
                        new OnCompleteListener<Boolean>() {
                            public void onComplete(Task<Boolean> task) {
                                if (task.isSuccessful()) {

                                    if (finalRequest != null) {
                                        Log.i("callgooglepay()", "ok");
                                        // Log.i("callgooglepay()", "ok");
                                        AutoResolveHelper.resolveTask(
                                                paymentsClient.loadPaymentData(createPaymentDataRequest()),
                                                StripePaymentActivity.this,
                                                Const.LOAD_PAYMENT_DATA_REQUEST_CODE);
                                    } else {
                                        //  dismissAlertDialog();
                                        Utils.showToast(getString(R.string.error_payment_google), StripePaymentActivity.this);
                                    }
                                    // show Google Pay as payment option
                                } else {

                                    Utils.showToast(getString(R.string.error_payment_google), StripePaymentActivity.this);
                                    // hide Google Pay as payment option
                                }
                            }
                        }
                );


    }


    @NonNull
    private IsReadyToPayRequest createIsReadyToPayRequest() throws JSONException {
        final JSONArray allowedAuthMethods = new JSONArray();
        allowedAuthMethods.put("PAN_ONLY");
        allowedAuthMethods.put("CRYPTOGRAM_3DS");

        final JSONArray allowedCardNetworks = new JSONArray();
        allowedCardNetworks.put("AMEX");
        allowedCardNetworks.put("DISCOVER");
        allowedCardNetworks.put("JCB");
        allowedCardNetworks.put("MASTERCARD");
        allowedCardNetworks.put("VISA");

        final JSONObject isReadyToPayRequestJson = new JSONObject();
        isReadyToPayRequestJson.put("allowedAuthMethods", allowedAuthMethods);
        isReadyToPayRequestJson.put("allowedCardNetworks", allowedCardNetworks);

        return IsReadyToPayRequest.fromJson(isReadyToPayRequestJson.toString());
    }

    @NonNull
    private PaymentDataRequest createPaymentDataRequest() {
        JSONObject tokenizationSpec = null;
        try {
            // Log.i("tokenizationSpec", tokenizationSpec.toString());
            tokenizationSpec = new GooglePayConfig(gatewayItem.getPaymentKeyId()).getTokenizationSpecification();
            Log.i("tokenizationSpec", tokenizationSpec.toString());
        } catch (JSONException t) {
            Log.i("tokenizationSpec", t.toString());
            t.printStackTrace();
        }
        JSONObject cardPaymentMethod = null;
        try {
            cardPaymentMethod = new JSONObject()
                    .put("type", "CARD")
                    .put(
                            "parameters",
                            new JSONObject()
                                    .put("allowedAuthMethods", new JSONArray()
                                            .put("PAN_ONLY")
                                            .put("CRYPTOGRAM_3DS"))
                                    .put("allowedCardNetworks",
                                            new JSONArray()
                                                    .put("AMEX")
                                                    .put("DISCOVER")
                                                    .put("JCB")
                                                    .put("MASTERCARD")
                                                    .put("VISA"))

                                    // require billing address
                                    .put("billingAddressRequired", true)
                                    .put(
                                            "billingAddressParameters",
                                            new JSONObject()
                                                    // require full billing address
                                                    .put("format", "FULL")

                                                    // require phone number
                                                    .put("phoneNumberRequired", true)
                                    )
                    )
                    .put("tokenizationSpecification", tokenizationSpec);
            Log.i("cardPaymentMethod", cardPaymentMethod.toString());
        } catch (JSONException e) {
            Log.i("cardPaymentMethod", e.toString());
            e.printStackTrace();
        }

        // create PaymentDataRequest
        String paymentDataRequest = null;
        try {
            paymentDataRequest = new JSONObject()
                    .put("apiVersion", 2)
                    .put("apiVersionMinor", 0)
                    .put("allowedPaymentMethods",
                            new JSONArray().put(cardPaymentMethod))
                    .put("transactionInfo", new JSONObject()
                            .put("totalPrice", String.valueOf(currentBooking.getTotalInvoiceAmount()))
                            .put("totalPriceStatus", "FINAL")
                            .put("currencyCode", currentBooking.getCurrencyCode())
                    )
                    /* .put("merchantInfo", new JSONObject()
                             .put("merchantName", "Example Merchant"))
 */
                    // require email address
                    .put("emailRequired", true)
                    .toString();
        } catch (JSONException e) {
            Log.i("paymentDataRequest", e.toString());
            e.printStackTrace();
        }


        return PaymentDataRequest.fromJson(paymentDataRequest);
    }

    private void onGooglePayResult(@NonNull Intent data) throws JSONException {

        final PaymentData paymentData = PaymentData.getFromIntent(data);
        if (paymentData == null) {
            return;
        }

        final PaymentMethodCreateParams paymentMethodCreateParams =
                PaymentMethodCreateParams.createFromGooglePay(
                        new JSONObject(paymentData.toJson()));


        Utils.showCustomProgressDialog(this, false);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.Params.TYPE, Const.Type.USER);
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.AMOUNT_ID, currentBooking.getTotalInvoiceAmount());
            //     jsonObject.put(Const.Params.TEST, Const.Params.version_value);
            jsonObject.put(Const.Params.COUNTRY_CURRENCY_CODE, currentBooking.getCurrencyCode());
        } catch (JSONException e) {
            AppLog.handleException(PaymentActivity.class.getName(), e);
        }
        Log.i("setClientSecret", jsonObject.toString());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentIntentResponce> responseCall = apiInterface.getpaymentintent(ApiClient
                .makeJSONRequestBody(jsonObject));

        responseCall.enqueue(new Callback<PaymentIntentResponce>() {
            @Override
            public void onResponse(Call<PaymentIntentResponce> call, Response<PaymentIntentResponce> response) {
                Utils.hideCustomProgressDialog();
                AppLog.Log("setClientSecret_responce", ApiClient.JSONResponse(response.body()));
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        if (response.body().getPaymentIntent().getClientSecret() != null) {

                            clientSecret = response.body().getPaymentIntent().getClientSecret();


                            mStripe.confirmPayment(StripePaymentActivity.this,
                                    ConfirmPaymentIntentParams
                                            .createWithPaymentMethodCreateParams(
                                                    paymentMethodCreateParams,
                                                    clientSecret, Const.STRIPE_RETURN_URL));


                        }
                    } else {
                        paynow.setClickable(true);
                        addcard.setClickable(true);
                        Gogglepay.setClickable(true);
                        Utils.showErrorToast(response.body().getMessage(), StripePaymentActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(Call<PaymentIntentResponce> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                Utils.hideCustomProgressDialog();
                try {
                    callErrorFunction(t);
                } catch (Exception e) {
                    Log.e("error_failure_main", e.toString());
                }

            }
        });




      /*  mStripe.createPaymentMethod(
                paymentMethodCreateParams,
                new ApiResultCallback<PaymentMethod>() {
                    @Override
                    public void onSuccess(@NonNull PaymentMethod result) {
                        Log.i("paymentMethodid",result.id);

                    }
                    @Override
                    public void onError(@NonNull Exception e) {

                        Utils.showToast(getString(R.string.error_payment_google),StripePaymentActivity.this);
                    }
                }
        );*/


    }


}