package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;


import com.edelivery.component.CustomDialogAlert;
import com.edelivery.models.responsemodels.AppSettingDetailResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;



import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreenActivity extends BaseAppCompatActivity implements BaseAppCompatActivity
        .NetworkListener {





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        PreferenceHelper.getInstance(getActivity()).putIMAGE_BASE_URL(BuildConfig.BASE_URL);


        saveAndroidId();
        checkPermission();
    }

    /**
     * method used to call a webservice for get admin setting detail for  device type
     */
    private void getSettingsDetail() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.TYPE,Const.Type.USER);
            jsonObject.put(Const.Params.DEVICE_TYPE,Const.ANDROID);
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.SPLASH_SCREEN_ACTIVITY, e);
        }
        Log.i("getSettingsDetail", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AppSettingDetailResponse> detailResponseCall = apiInterface.getAppSettingDetail
                (ApiClient.makeJSONRequestBody(jsonObject));
        detailResponseCall.enqueue(new Callback<AppSettingDetailResponse>() {
            @Override
            public void onResponse(Call<AppSettingDetailResponse> call,
                                   Response<AppSettingDetailResponse> response)
            {

                if (response != null && response.body() != null && parseContent.isSuccessful(response) && parseContent.parseAppSettingDetail(response)) {



                    if (response.body().isOpenUpdateDialog() && checkVersionCode(response.body()
                            .getVersionCode())) {
                        openUpdateAppDialog(response.body().isForceUpdate());


                    } else if (!preferenceHelper.getIsHideWelcomeScreen()) {
                        goToWelcomeScreen();
                    } else if (ContextCompat.checkSelfPermission(SplashScreenActivity.this, android
                            .Manifest.permission
                            .ACCESS_COARSE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED && ContextCompat
                            .checkSelfPermission(SplashScreenActivity.this,
                                    android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED) {
                        goToMandatoryDeliveryLocationActivity();
                    } else {
                        goToHomeActivity();
                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call<AppSettingDetailResponse> call, Throwable t) {
                Log.e("call_error2","error");


                AppLog.handleThrowable(Const.Tag.SPLASH_SCREEN_ACTIVITY, t);
                Log.i(Const.Tag.SPLASH_SCREEN_ACTIVITY, t.toString());
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Const.ACTION_SETTINGS:
                    checkIfGpsOrInternetIsEnable();
                    break;
                default:
                    // do with default
                    break;
            }
        }
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        //do something
    }

    @Override
    protected void setViewListener() {
        //do something
    }

    @Override
    protected void onBackNavigation() {
        //do something
    }


    /**
     * this method will check play service is updated
     *
     * @return
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 12)
                        .show();
            } else {
                AppLog.Log("Google Paly Service", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        //do something
    }

    @Override
    public void onStart() {
        super.onStart();
      /*  Branch branch = Branch.getInstance();
        // Branch init
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    // params are the deep linked params associated with the link that the user
                    // clicked -> was re-directed to this app
                    // params will be empty if no data found
                    // ... insert custom logic here ...
                    Log.i("BRANCH SDK DATA", referringParams.toString());
                    try {
                        JSONObject jsonObject = new JSONObject(referringParams.toString());
                        String storeId = jsonObject.optString("~stage");
                        if (!TextUtils.isEmpty(storeId)) {
                           // storeId = storeId.replace("'", "");
                        }
                    } catch (JSONException e) {
                        AppLog.handleException(Const.Tag.SPLASH_SCREEN_ACTIVITY, e);
                    }
                } else {
                    Log.i("BRANCH SDK ERROR", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);*/
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    /**
     * this method is check that internet or GPS is ON or OFF
     */
    private void checkIfGpsOrInternetIsEnable() {
        if (!Utils.isInternetConnected(this)) {
            openInternetDialog(this);
            setNetworkListener(this);
        } else {
            closedEnableDialogInternet();
            if (checkPlayServices()) {
                getSettingsDetail();
            }
        }

    }

    /**
     * this method will check particular  permission will be granted by user or not
     */
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission
                            .ACCESS_FINE_LOCATION, android.Manifest.permission
                            .ACCESS_COARSE_LOCATION},
                    Const.PERMISSION_FOR_LOCATION);
        } else {
            //Do the stuff that requires permission...
            checkIfGpsOrInternetIsEnable();
        }
    }

    /**
     * this method will make decision according to permission result
     *
     * @param grantResults set result from system or OS
     */
    private void goWithLocationPermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            checkIfGpsOrInternetIsEnable();
        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            checkIfGpsOrInternetIsEnable();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_LOCATION:
                    goWithLocationPermission(grantResults);
                    break;
                default:
                    //do with default
                    break;
            }
        }

    }

    @Override
    public void onNetworkChange(boolean isEnable) {
        checkIfGpsOrInternetIsEnable();
    }


    void openUpdateAppDialog(final boolean isForceUpdate) {
        String btnNegative;
        if (isForceUpdate) {
            btnNegative = getResources()
                    .getString(R.string.text_exit);
        } else {
            btnNegative =
                    getResources()
                            .getString(R.string.text_later);
        }
        CustomDialogAlert customDialogAlert = new CustomDialogAlert(this, this
                .getResources()
                .getString(R.string.text_update_app), this.getResources()
                .getString(R.string.msg_new_app_update_available), btnNegative, this.getResources()
                .getString(R.string.text_update)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
                if (isForceUpdate) {
                    finishAffinity();
                } else {
                    goToHomeActivity();
                }

            }

            @Override
            public void onClickRightButton() {
                final String appPackageName = getPackageName(); // getPackageName() from Context
                // or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
                            + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    AppLog.handleException(SplashScreenActivity.class.getName(), anfe);
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google" +
                            ".com/store/apps/details?id=" + appPackageName)));

                }
                dismiss();
                finishAffinity();
            }
        };
        customDialogAlert.show();
    }

    /**
     * this method will check that is our app is updated or not ,according to admin app version
     * code
     *
     * @param code
     * @return
     */
    boolean checkVersionCode(String code) {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionCode < Integer
                    .valueOf(code);
        } catch (PackageManager.NameNotFoundException e) {
            AppLog.handleException(SplashScreenActivity.class.getName(), e);
        }
        return false;
    }


    private void saveAndroidId() {
        if (TextUtils.isEmpty(preferenceHelper.getAndroidId())) {
            preferenceHelper.putAndroidId(Utils.generateRandomString());
        }
    }

    private void goToWelcomeScreen() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


}
