package com.edelivery;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.util.Pair;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.edelivery.adapter.PlaceAutocompleteAdapter;
import com.edelivery.adapter.StoreAdapter;
import com.edelivery.animation.AlphaInAnimationAdapter;
import com.edelivery.animation.ResizeAnimation;
import com.edelivery.animation.ScaleInAnimationAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.component.TagView;
import com.edelivery.fragments.StoresFragment;
import com.edelivery.models.datamodels.Ads;
import com.edelivery.models.datamodels.Deliveries;
import com.edelivery.models.datamodels.RemoveFavourite;
import com.edelivery.models.datamodels.Store;
import com.edelivery.models.datamodels.StoreClosedResult;
import com.edelivery.models.responsemodels.AvailableProviderResponse;
import com.edelivery.models.responsemodels.SetFavouriteResponse;
import com.edelivery.models.responsemodels.StoreResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.mapbox.android.core.location.*;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdate;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.picker.PlacePicker;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import mapboxutils.CustomEventMapBoxView;
import mapboxutils.MapBoxMapReady;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*import static com.edelivery.BuildConfig.BASE_URL;*/
import static com.edelivery.utils.Const.REQUEST_CHECK_SETTINGS;

public class StoresActivity extends BaseAppCompatActivity implements  PermissionsListener, OnMapReadyCallback, MapBoxMapReady  {

    private RecyclerView rcvStore;
    private StoreAdapter storeAdapter;
    private ArrayList<Store> storeListOriginal = new ArrayList<>();;
    private ArrayList<Store> storeListFiltered = new ArrayList<>();;
    private ArrayList<Ads> ads = new ArrayList<>();;
    private SwipeRefreshLayout srlSelectedStore;
    private LinearLayout llStoreFilter;
    private CustomFontTextView btnPriceOne, btnPriceTwo, btnPriceThree, btnPriceFour,
            selectedPrice, selectedTime, selectedDistance, selectCategory, btnTimeThree, btnTimeOne,
            btnTimeTwo,
            btnDistanceOne,
            btnDistanceTwo, btnDistanceThree, btnStore, btnItem, btnTag;
    private CustomFontButton btnResetFilter, btnApplyFilter;
    private ImageView ivClearText;
    private CustomFontEditTextView etStoreSearch;
    private LinearLayout ivEmpty, llCategoryFilter;
    private CameraUpdate cu;
    private ArrayList<Integer> storePrices = new ArrayList<>();;
    private int storeTime = 0;
    private double storeDistance = 0;
    private Deliveries deliveries;
    private Bitmap storeBitmap;
    private ArrayList<String> selectedTagList = new ArrayList<>();;
    private TagView tagView;
    private ImageView ivCategoryFilter,close;

    public MapboxMap mapboxMap;
    private LocationEngine locationEngine;
    private PermissionsManager permissionsManager;
    private long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    private long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
    LocationEngineRequest mLocationEngineRequest;
    private MainActivityLocationCallback callback = new MainActivityLocationCallback(StoresActivity.this);
    private MapView mapbox;
    private boolean isCurrentLocationGet = false;
    public static double str_Latitude = 0.0, str_Longitude = 0.0;
    private String geojsonSourceLayerId = "geojsonSourceLayerId";

    @SuppressLint("MissingPermission")
    private void initLocationEngine()
    {
        locationEngine = LocationEngineProvider.getBestLocationEngine(this);

        mLocationEngineRequest = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        locationEngine.requestLocationUpdates(mLocationEngineRequest, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            initLocationEngine();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }





    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {

        if (granted) {
            if (mapboxMap.getStyle() != null) {
                enableLocationComponent(mapboxMap.getStyle());
            }
        }
    }

    @Override
    public void mapBoxMapReady(final MapboxMap mapboxMap, Style style) {
        this.mapboxMap = mapboxMap;

        enableLocationComponent(style);
        mapboxMap.addOnCameraIdleListener(new MapboxMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                LatLng mLatLng = mapboxMap.getCameraPosition().target;

                if (mLatLng != null) {
                    Geocoder geocoder = new Geocoder(StoresActivity.this, Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(
                                mLatLng.getLatitude(),
                                mLatLng.getLongitude(),
                                1);
                        if (addresses != null && addresses.size() > 0 && addresses.get(0) != null) {
                            //Log.e("tagMap", "onResponse: " + addresses.get(0).toString());
                            if (addresses.get(0).getCountryName() != null) {
                                Log.e("tagMap", "onResponse: " + addresses.get(0).getCountryName());
                                Log.d("call","========= Location Lat ==========="+addresses.get(0).getLatitude());
                                Log.d("call","========= Location Lon ==========="+addresses.get(0).getLongitude());
                                str_Latitude=addresses.get(0).getLatitude();
                                str_Longitude=addresses.get(0).getLongitude();

                            } }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        checkLocationPermission(null);
                        break;
                    case Activity.RESULT_CANCELED:
                        //locationHelper.setOpenGpsDialog(false);
                        break;
                    default:
                        break;
                }
                break;
            case Const.REQUEST_CODE_AUTOCOMPLETE:
                if (data != null)
                {
                    CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);
                    if(selectedCarmenFeature!=null)
                    {
                    if (mapboxMap != null) {

                        Style style = mapboxMap.getStyle();
                        if (style != null) {
                            GeoJsonSource source = style.getSourceAs(geojsonSourceLayerId);
                            if (source != null) {
                                source.setGeoJson(FeatureCollection.fromFeatures(
                                        new Feature[]{Feature.fromJson(selectedCarmenFeature.toJson())}));
                            }

                            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                                    new CameraPosition.Builder()
                                            .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                                    ((Point) selectedCarmenFeature.geometry()).longitude()))
                                            .zoom(10)
                                            .build()), 4000);

                        }
                    }

                }
                }
                break;
            case Const.REQUEST_CODE:
                CarmenFeature carmenFeature = PlacePicker.getPlace(data);
                //acDeliveryAddress.setText(carmenFeature.placeName());

        }
    }

    private void checkLocationPermission(LatLng latLng) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission
                            .ACCESS_FINE_LOCATION, android.Manifest.permission
                            .ACCESS_COARSE_LOCATION},
                    Const.PERMISSION_FOR_LOCATION);
        } else {
            moveCameraFirstMyLocation(true, latLng);
        }
    }



    public void moveCameraFirstMyLocation(boolean isAnimate, LatLng latLng) {
        LatLng latLngOfMyLocation = null;
        if (latLng == null) {


        } else {
            latLngOfMyLocation = latLng;
        }
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLngOfMyLocation).zoom(9).build();

       // locationHelper.setOpenGpsDialog(false);
    }






    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    public class MainActivityLocationCallback
            implements LocationEngineCallback<LocationEngineResult> {

        private final WeakReference<StoresActivity> activityWeakReference;

        MainActivityLocationCallback(StoresActivity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(LocationEngineResult result) {
            StoresActivity activity = activityWeakReference.get();

            if (activity != null) {
                Location location = result.getLastLocation();

                if (location == null) {
                    return;
                }

                if (!isCurrentLocationGet) {
                    isCurrentLocationGet = true;
                    zoomCamera(new LatLng(location.getLatitude(), location.getLongitude()));
                }
            }
        }

        @Override
        public void onFailure(@NonNull Exception exception) {
            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
            StoresActivity activity = activityWeakReference.get();
            if (activity != null) {
                Toast.makeText(activity, exception.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void zoomCamera(LatLng latLng) {
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                new CameraPosition.Builder()
                        .target(latLng)
                        .zoom(10)
                        .build()), 4000);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Mapbox.getInstance(this, getResources().getString(R.string
                .MAP_BOX_ACCESS_TOKEN));

        super.onCreate(savedInstanceState);
        setupWindowAnimations();
        setContentView(R.layout.activity_stores);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        Log.d("activity","======= Store activity ============");
        CheckverifiedUser();
        loadExtraData();
        downloadStorePin();
        mapbox = findViewById(R.id.mapBoxView);
        mapbox.onCreate(savedInstanceState);

        new CustomEventMapBoxView(getApplicationContext(), mapbox, this);

        findViewById();
        setViewListener();
        initToolBar();
        //mapViewStore.onCreate(savedInstanceState);
        storeListOriginal = new ArrayList<>();
        storeListFiltered = new ArrayList<>();
        storePrices = new ArrayList<>();
        ads = new ArrayList<>();
        selectedTagList = new ArrayList<>();
      //  setToolbarRightIcon3(R.drawable.ic_location_on_black_24dp, this);
        setToolbarRightIcon2(R.drawable.filter_store, this);
        srlSelectedStore.setRefreshing(true);
        initRcvStore();
        loadStoreList();
        setFilterPriceData();
        initTagView();




    }


    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
       // mapViewStore = (MapView) findViewById(R.id.mapViewStore);







        rcvStore = (RecyclerView) findViewById(R.id.rcvStore);
        srlSelectedStore = (SwipeRefreshLayout) findViewById(R.id.srlSelectedStore);
        llStoreFilter = (LinearLayout) findViewById(R.id.llSoreFilter);
        btnPriceOne = (CustomFontTextView) findViewById(R.id.btnPriceOne);
        btnPriceTwo = (CustomFontTextView) findViewById(R.id.btnPriceTwo);
        btnPriceThree = (CustomFontTextView) findViewById(R.id.btnPriceThree);
        btnPriceFour = (CustomFontTextView) findViewById(R.id.btnPriceFour);
        btnApplyFilter = (CustomFontButton) findViewById(R.id.btnApplyFilter);
        btnResetFilter = (CustomFontButton) findViewById(R.id.btnResetFilter);
        btnTimeThree = (CustomFontTextView) findViewById(R.id.btnTimeThree);
        btnTimeOne = (CustomFontTextView) findViewById(R.id.btnTimeOne);
        btnTimeTwo = (CustomFontTextView) findViewById(R.id.btnTimeTwo);
        ivEmpty = (LinearLayout) findViewById(R.id.ivEmpty);
        etStoreSearch = (CustomFontEditTextView) findViewById(R.id.etStoreSearch);
        ivClearText = (ImageView) findViewById(R.id
                .ivClearDeliveryAddressTextMap);
        btnDistanceOne = (CustomFontTextView) findViewById(R.id.btnDistanceOne);
        btnDistanceTwo = (CustomFontTextView) findViewById(R.id.btnDistanceTwo);
        btnDistanceThree = (CustomFontTextView) findViewById(R.id.btnDistanceThree);
        llStoreFilter.setVisibility(View.GONE);
        tagView = (TagView) findViewById(R.id.tag_group);
        ivCategoryFilter = (ImageView) findViewById(R.id.ivCategoryFilter);
        llCategoryFilter = (LinearLayout) findViewById(R.id.llCategoryFilter);
        llCategoryFilter.setVisibility(View.GONE);
        btnStore = (CustomFontTextView) findViewById(R.id.btnStore);
        btnItem = (CustomFontTextView) findViewById(R.id.btnItem);
        btnTag = (CustomFontTextView) findViewById(R.id.btnTag);

    }

    @Override
    protected void setViewListener() {
        //do something
        ivCategoryFilter.setOnClickListener(this);
        //mapViewStore.getMapAsync(this);
        srlSelectedStore.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadStoreList();
            }
        });
        btnPriceOne.setOnClickListener(this);
        btnPriceTwo.setOnClickListener(this);
        btnPriceThree.setOnClickListener(this);
        btnPriceFour.setOnClickListener(this);
        btnTimeOne.setOnClickListener(this);
        btnTimeTwo.setOnClickListener(this);
        btnTimeThree.setOnClickListener(this);
        btnApplyFilter.setOnClickListener(this);
        btnResetFilter.setOnClickListener(this);
        ivClearText.setOnClickListener(this);
        ivClearText.setVisibility(View.GONE);
        btnStore.setOnClickListener(this);
        btnItem.setOnClickListener(this);
        btnTag.setOnClickListener(this);
        etStoreSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                storeFilter(s);
                if (s.length() > 0) {
                    ivClearText.setVisibility(View.VISIBLE);
                } else {
                    ivClearText.setVisibility(View.GONE);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnDistanceOne.setOnClickListener(this);
        btnDistanceTwo.setOnClickListener(this);
        btnDistanceThree.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    /**
     * filter store list store,item and tag wise
     */
    private void storeFilter(String filter, String filterBy) {
        if (storeAdapter != null) {
            storeAdapter.setFilterBy(filterBy);
            storeAdapter.getFilter().filter(filter.trim());
        }
    }

    private void storeFilter(CharSequence filter) {
        if (storeAdapter != null) {
            storeAdapter.getFilter().filter(filter.toString().trim());
        }
    }

    @Override
    public void onClick(View view) {
        //do something
        switch (view.getId()) {
            case R.id.ivToolbarRightIcon3:
                updateUiMap();
                break;
            case R.id.ivToolbarRightIcon2:
                updateUiStoreFilter();
                break;

            case R.id.btnResetFilter:
                loadStoreList();
                break;
            case R.id.btnApplyFilter:
                applyFiltered(!storePrices.isEmpty(), storeTime > 0, storeDistance > 0,
                        !selectedTagList.isEmpty());
                updateUiStoreFilter();
                etStoreSearch.getText().clear();
                break;
            case R.id.ivClearDeliveryAddressTextMap:
                etStoreSearch.getText().clear();
                storeFilter(etStoreSearch.getText().toString());
                break;
            case R.id.ivCategoryFilter:
                slidStoreSearchFilterView();
                break;
            default:
                // do with default
                break;
        }
        checkSelectedPrice(view);
        checkSelectedTime(view);
        checkSelectedDistance(view);
        checkSelectedCategory(view);
    }

    private void initRcvStore() {
        rcvStore.setLayoutManager(new LinearLayoutManager(this));
        storeAdapter = new StoreAdapter(this, storeListOriginal, ads) {
            @Override
            public void onSelected(View view, int position) {
                Store store = storeAdapter.getStoreArrayList().get(position);
                currentBooking.setStoreClosed(store.isStoreClosed());
                checkAvailableProvider(store, view);


            }

            @Override
            public void setFavourites(int position, boolean isFavourite) {
                if (isFavourite) {
                    removeAsFavoriteStore(storeAdapter.getStoreArrayList().get(position));
                } else {
                    setFavoriteStore(storeAdapter.getStoreArrayList().get(position));
                }

            }
        };
        AlphaInAnimationAdapter animationAdapter = new AlphaInAnimationAdapter(storeAdapter);
        rcvStore.setAdapter(new ScaleInAnimationAdapter(animationAdapter));
    }

    private void checkAvailableProvider(final Store store, final View view) {
        Utils.showCustomProgressDialog(this, false);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.LATITUDE, store.getLocation().get(0));
            jsonObject.put(Const.Params.LONGITUDE, store.getLocation().get(1));
            jsonObject.put(Const.Params.CITY_ID, currentBooking.getBookCityId());
            jsonObject.put(Const.Params.STORE_ID, currentBooking.getSelectedStoreId());
            //  jsonObject.put(Const.Params.IS_PAYMENT_MODE_CASH, gatewayItem.isPaymentModeCash());
        } catch (JSONException e) {
            AppLog.handleException(PaymentActivity.class.getName(), e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AvailableProviderResponse> responseCall = apiInterface.checkStoreProviderAvailable(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<AvailableProviderResponse>() {
            @Override
            public void onResponse(Call<AvailableProviderResponse> call, Response<AvailableProviderResponse>
                    response) {

                AppLog.Log("CHECK_PROVIDER_AVAILABLE", ApiClient.JSONResponse(response.body()));

                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().getSuccess()) {
                        if (!response.body().getProviders().isEmpty()) {
                            if (TextUtils.equals(storeAdapter.getFilterBy(), getResources().getString(R
                                    .string.text_item)) && !TextUtils.isEmpty(etStoreSearch.getText()
                                    .toString())) {
                                goToStoreProductActivity(store, view, etStoreSearch.getText().toString()
                                        , true);
                            } else {
                                goToStoreProductActivity(store, view, null, true);
                            }
                        } else {
                            openProviderNotFoundDialog(store, view);
                        }
                    } else {
                        openProviderNotFoundDialog(store, view);
                    }

            }

            @Override
            public void onFailure(Call<AvailableProviderResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
            }
        });
    }

    private void openProviderNotFoundDialog(final Store store, final View view) {
        String msg = getResources().getString(R.string.msg_provider_not_available);
        CustomDialogAlert customDialogAlert = new CustomDialogAlert(StoresActivity.this, getResources().getString(R.string
                .text_attention), msg, getResources().getString(R.string.text_ok), getResources().getString(R.string.text_cancel)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
                if (TextUtils.equals(storeAdapter.getFilterBy(), getResources().getString(R
                        .string.text_item)) && !TextUtils.isEmpty(etStoreSearch.getText()
                        .toString())) {
                    goToStoreProductActivity(store, view, etStoreSearch.getText().toString()
                            , true);
                } else {
                    goToStoreProductActivity(store, view, null, true);
                }
            }

            @Override
            public void onClickRightButton() {
                dismiss();
            }
        };
        customDialogAlert.show();
    }

    /**
     * this method call a webservice for get Store list in city
     *
     * @param cityId          cityId in string
     * @param deliveryStoreId id in string
     */

    private void getStoreList(String cityId, String deliveryStoreId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.CITY_ID, cityId);
            jsonObject.put(Const.Params.STORE_DELIVERY_ID, deliveryStoreId);
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper
                    .getUserId());
            jsonObject.put(Const.Params.CART_UNIQUE_TOKEN, preferenceHelper.getAndroidId());
            jsonObject.put(Const.Params.LATITUDE, currentBooking.getCurrentLatLng().latitude);
            jsonObject.put(Const.Params.LONGITUDE, currentBooking.getCurrentLatLng().longitude);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, e);
        }
        Log.i("getStoreList",jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StoreResponse> responseCall = apiInterface.getSelectedStoreList(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<StoreResponse>() {
            @Override
            public void onResponse(Call<StoreResponse> call, Response<StoreResponse> response) {

                AppLog.Log("STORE_RESPONSE", ApiClient.JSONResponse(response.body()));
                storeListOriginal.clear();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        if (response.body().getStores() != null)
                        {

                            if(response.body().getStores().size()>0) {
                                for (Store store : response.body().getStores())
                                {
                                    StoreClosedResult storeClosedResult = Utils.checkStoreOpenAndClosed
                                            (StoresActivity.this, store.getStoreTime(), response.body
                                                    ().getServerTime(), CurrentBooking.getInstance()
                                                    .getTimeZone(), false, 0);
                                    store.setStoreClosed(storeClosedResult.isStoreClosed());
                                    store.setReOpenTime(storeClosedResult.getReOpenAt());

                                    LatLng latlng_distance=new LatLng(currentBooking.getCurrentLatLng().latitude,currentBooking.getCurrentLatLng().longitude);
                                    store.setDistance(distanceTo(latlng_distance,
                                            new LatLng(store
                                                    .getLocation().get(0), store
                                                    .getLocation().get(1)), true));

                                    store.setCurrency(CurrentBooking.getInstance().getCurrency());
                                    store.setPriceRattingAndTag(Utils.getStringPriceAndTag(store
                                            .getFamousProductsTags
                                                    (), store.getPriceRating(), store
                                            .getCurrency()));
                                    if (currentBooking.isFutureOrder()) {
                                        if (store.isTakingScheduleOrder()) {
                                            storeListOriginal.add(store);
                                        }
                                    } else {
                                        storeListOriginal.add(store);
                                    }
                                }
                            }

                            ads.clear();
                            if (response.body().getAds() != null) {
                                ads.addAll(response.body().getAds());
                            }
                            updateUiStoreList();
                            resetFilter();
                            setStoreMarkerOnMap();
                            if (storeAdapter != null) {
                                storeAdapter.setStoreArrayList(storeListOriginal);
                                storeAdapter.setFilterBy(getResources().getString(R.string
                                        .text_store));
                                storeAdapter.notifyDataSetChanged();
                            }
                            etStoreSearch.setHint(getResources().getString(R.string
                                    .text_search_by) + " " +
                                    getResources().getString(R.string.text_store));
                            resetSelectedCategory();
                            setSelectedCategory(btnStore);
                        }
                    } else {
                        Log.i("Utils.showErrorToast","ok5");
                        Utils.showErrorToast(response.body().getErrorCode(), StoresActivity.this);

                }
                srlSelectedStore.setRefreshing(false);
                etStoreSearch.getText().clear();
            }

            @Override
            public void onFailure(Call<StoreResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void goToStoreProductActivity(Store store, View view, String filter, boolean
            isAnimated) {
        Intent intent = new Intent(this, StoreProductActivity.class);
        intent.putExtra(Const.SELECTED_STORE, store);
        intent.putExtra(Const.FILTER, filter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && isAnimated) {

            Pair<View, String> p1 = Pair.create(view.findViewById(R.id.llStoreCard),
                    getResources().getString(R.string
                            .transition_string_store_card));
            Pair<View, String> p2 = Pair.create(view.findViewById(R.id.ivStoreImage),
                    getResources().getString(R.string
                            .transition_string_store_image));

            ActivityOptionsCompat options =
                    ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1,
                            p2);
            ActivityCompat.startActivity(this, intent, options.toBundle());
        } else {
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

    }

    public void goToStoreProductActivity(String storeId) {
        Intent intent = new Intent(this, StoreProductActivity.class);
        intent.putExtra(Const.STORE_DETAIL, storeId);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void gotToStoreProductActivity(Store store) {
        if(store != null) {
            Intent intent = new Intent(this, StoreProductActivity.class);
            intent.putExtra(Const.SELECTED_STORE, store);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

    }

    private void loadStoreList() {
        srlSelectedStore.setRefreshing(true);
        getStoreList(CurrentBooking.getInstance().getBookCityId(), deliveries.getId());
        setTitleOnToolBar(deliveries.getDeliveryName());
    }

    private void loadExtraData() {
        if (getIntent().getExtras() != null) {
            deliveries = getIntent().getExtras().getParcelable(Const
                    .DELIVERY_STORE);
        }

    }


    /**
     * update ui when select price in filter
     *
     * @param view
     */
    private void setSelectedPrice(CustomFontTextView view) {
        selectedPrice = view;
        view.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                .selector_round_shape_black_solid, null));
        view.setTextColor(ResourcesCompat.getColor(getResources(), R.color.color_white, null));
    }

    /**
     * update ui when select time in filter
     *
     * @param view
     */
    private void setSelectedTime(CustomFontTextView view) {
        selectedTime = view;
        view.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                .selector_round_shape_black_solid, null));
        view.setTextColor(ResourcesCompat.getColor(getResources(), R.color.color_white, null));
    }

    /**
     * update ui when select distance in filter
     *
     * @param view
     */
    private void setSelectedDistance(CustomFontTextView view) {
        if (view != null) {
            selectedDistance = view;
            view.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                    .selector_round_shape_black_solid, null));
            view.setTextColor(ResourcesCompat.getColor(getResources(), R.color.color_white, null));
        }


    }

    /**
     * update ui when select Category in filter
     *
     * @param view
     */
    private void setSelectedCategory(CustomFontTextView view) {
        selectCategory = view;
        view.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                .selector_round_shape_black_solid, null));
        view.setTextColor(ResourcesCompat.getColor(getResources(), R.color.color_white, null));
    }

    /**
     * this method manage store filer view according to price
     *
     * @param view
     */
    private void checkSelectedPrice(View view) {
        switch (view.getId()) {
            case R.id.btnPriceOne:
                if (storePrices.contains(Const.Store.STORE_PRICE_ONE)) {
                    storePrices.remove((Object) Const.Store.STORE_PRICE_ONE);
                    resetSelectedPrice((CustomFontTextView) view);
                } else {
                    storePrices.add(Const.Store.STORE_PRICE_ONE);
                    setSelectedPrice((CustomFontTextView) view);

                }
                break;
            case R.id.btnPriceTwo:
                if (storePrices.contains(Const.Store.STORE_PRICE_TWO)) {
                    storePrices.remove((Object) Const.Store.STORE_PRICE_TWO);
                    resetSelectedPrice((CustomFontTextView) view);
                } else {
                    storePrices.add(Const.Store.STORE_PRICE_TWO);
                    setSelectedPrice((CustomFontTextView) view);
                }
                break;
            case R.id.btnPriceThree:
                if (storePrices.contains(Const.Store.STORE_PRICE_THREE)) {
                    storePrices.remove((Object) Const.Store.STORE_PRICE_THREE);
                    resetSelectedPrice((CustomFontTextView) view);
                } else {
                    storePrices.add(Const.Store.STORE_PRICE_THREE);
                    setSelectedPrice((CustomFontTextView) view);
                }
                break;
            case R.id.btnPriceFour:
                if (storePrices.contains(Const.Store.STORE_PRICE_FOUR)) {
                    storePrices.remove((Object) Const.Store.STORE_PRICE_FOUR);
                    resetSelectedPrice((CustomFontTextView) view);
                } else {
                    storePrices.add(Const.Store.STORE_PRICE_FOUR);
                    setSelectedPrice((CustomFontTextView) view);
                }
                break;

            default:
                // do something
                break;
        }
    }


    /**
     * this method manage store filer view according to time
     *
     * @param view
     */
    private void checkSelectedTime(View view) {

        switch (view.getId()) {
            case R.id.btnTimeOne:
                storeTime = Const.Store.STORE_TIME_20;
                resetSelectedTime();
                setSelectedTime((CustomFontTextView) view);
                break;
            case R.id.btnTimeTwo:
                storeTime = Const.Store.STORE_TIME_60;
                resetSelectedTime();
                setSelectedTime((CustomFontTextView) view);
                break;
            case R.id.btnTimeThree:
                storeTime = Const.Store.STORE_TIME_120;
                resetSelectedTime();
                setSelectedTime((CustomFontTextView) view);
                break;

            default:
                // do with default
                break;
        }
    }

    /**
     * this method manage search filter
     *
     * @param view
     */
    private void checkSelectedCategory(View view) {

        switch (view.getId()) {
            case R.id.btnStore:
                storeFilter(etStoreSearch.getText().toString(), getResources().getString(R.string
                        .text_store));
                etStoreSearch.setHint(getResources().getString(R.string.text_search_by) + " " +
                        getResources().getString(R.string.text_store));
                resetSelectedCategory();
                setSelectedCategory((CustomFontTextView) view);
                break;
            case R.id.btnItem:
                storeFilter(etStoreSearch.getText().toString(), getResources().getString(R.string
                        .text_item));
                etStoreSearch.setHint(getResources().getString(R.string.text_search_by) + " " +
                        getResources().getString(R.string.text_item));
                resetSelectedCategory();
                setSelectedCategory((CustomFontTextView) view);
                break;
            case R.id.btnTag:
                storeFilter(etStoreSearch.getText().toString(), getResources().getString(R.string
                        .text_tag));
                etStoreSearch.setHint(getResources().getString(R.string.text_search_by) + " " +
                        getResources().getString(R.string.text_tag));
                resetSelectedCategory();
                setSelectedCategory((CustomFontTextView) view);
                break;

            default:
                // do with default
                break;
        }

    }

    /**
     * this method manage store filer view according to time
     *
     * @param view
     */
    private void checkSelectedDistance(View view) {

        switch (view.getId()) {
            case R.id.btnDistanceOne:
                storeDistance = Const.Store.STORE_DISTANCE_5;
                resetSelectedDistance();
                setSelectedDistance((CustomFontTextView) view);
                break;
            case R.id.btnDistanceTwo:
                storeDistance = Const.Store.STORE_DISTANCE_15;
                resetSelectedDistance();
                setSelectedDistance((CustomFontTextView) view);
                break;
            case R.id.btnDistanceThree:
                storeDistance = Const.Store.STORE_DISTANCE_25;
                resetSelectedDistance();
                setSelectedDistance((CustomFontTextView) view);
                break;

            default:
                // do with default
                break;
        }
    }

    /**
     * reset ui select price in filter
     */
    private void resetAllSelectedPrice() {
        if (selectedPrice != null) {
            btnPriceOne.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                    .selector_round_shape_black, null));
            btnPriceOne.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                    .color_app_text, null));
            btnPriceTwo.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                    .selector_round_shape_black, null));
            btnPriceTwo.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                    .color_app_text, null));
            btnPriceThree.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                    .selector_round_shape_black, null));
            btnPriceThree.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                    .color_app_text, null));
            btnPriceFour.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                    .selector_round_shape_black, null));
            btnPriceFour.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                    .color_app_text, null));
        }
    }

    private void resetSelectedPrice(CustomFontTextView view) {
        view.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                .selector_round_shape_black, null));
        view.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                .color_app_text, null));
    }

    /**
     * reset ui select time in filter
     */
    private void resetSelectedTime() {
        if (selectedTime != null) {
            selectedTime.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                    .selector_round_shape_black, null));
            selectedTime.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                    .color_app_text, null));
        }
    }

    /**
     * reset ui select distance in filter
     */
    private void resetSelectedDistance() {
        if (selectedDistance != null) {
            selectedDistance.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                    .selector_round_shape_black, null));
            selectedDistance.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                    .color_app_text, null));
        }
    }

    /**
     * reset ui select category in filter
     */
    private void resetSelectedCategory() {
        if (selectCategory != null) {
            //etStoreSearch.getText().clear();
            selectCategory.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                    .selector_round_shape_black, null));
            selectCategory.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                    .color_app_text, null));
        }
    }

    /**
     * reset store filter ui
     */
    private void resetFilter() {
        resetSelectedTime();
        resetAllSelectedPrice();
        resetSelectedDistance();
        storeDistance = 0;
        storeTime = 0;
        storePrices.clear();
        storeListFiltered.clear();
        tagView.clearSelected();
        selectedTagList.clear();

    }

    private void setupWindowAnimations() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        }
    }

    private void updateUiStoreList() {
        if (storeListOriginal.isEmpty()) {
            ivEmpty.setVisibility(View.VISIBLE);
            rcvStore.setVisibility(View.GONE);
        } else {
            ivEmpty.setVisibility(View.GONE);
            rcvStore.setVisibility(View.VISIBLE);
        }

    }

    private void updateUiStoreFilter() {
        if (llStoreFilter.getVisibility() == View.VISIBLE) {
            llStoreFilter.setVisibility(View.GONE);
            mapbox.setVisibility(View.GONE);
            setToolbarRightIcon2(R.drawable.filter_store, this);
        //    setToolbarRightIcon3(R.drawable.ic_location_on_black_24dp, this);

        } else {
            llStoreFilter.setVisibility(View.VISIBLE);
            mapbox.setVisibility(View.GONE);
            ivToolbarRightIcon3.setImageDrawable(null);
            setToolbarRightIcon2(R.drawable.ic_cancel, this);
        }
    }

    private void updateUiMap()
    {




        if (mapbox.getVisibility() == View.VISIBLE) {
            mapbox.setVisibility(View.GONE);
            llStoreFilter.setVisibility(View.GONE);
            setToolbarRightIcon2(R.drawable.filter_store, this);
         //   setToolbarRightIcon3(R.drawable.ic_location_on_black_24dp, this);
        } else {
            mapbox.setVisibility(View.VISIBLE);
            llStoreFilter.setVisibility(View.GONE);
            ivToolbarRightIcon2.setImageDrawable(null);
            setToolbarRightIcon3(R.drawable.ic_cancel, this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (cu != null) {
                       // googleMap.moveCamera(cu);
                    }

                }
            }, 500);
        }
    }

    /**
     * apply store filter to get store list as per filter value
     *
     * @param isAnySelectedStorePrice check is any price selected by user
     * @param isDistanceSelected      check is any distance selected by user
     * @param isTimeSelected          check is any time selected by user
     */
    private void applyFiltered(boolean isAnySelectedStorePrice, boolean isTimeSelected, boolean
            isDistanceSelected, boolean isAnyTagSelected) {
        if (storeListOriginal.isEmpty()) {
            return;
        }
        storeListFiltered.clear();
        if (!isTimeSelected) {
            storeTime = Integer.MAX_VALUE;
        }
        if (!isDistanceSelected) {
            storeDistance = Double.MAX_VALUE;
        }
        if (!isAnySelectedStorePrice && !isTimeSelected && !isDistanceSelected &&
                !isAnyTagSelected) {
            storeListFiltered.addAll(storeListOriginal);
        } else {
            for (Store store : storeListOriginal) {

                if (store.getDeliveryTime() <= storeTime && store.getDistance() <=
                        storeDistance) {
                    if (isAnySelectedStorePrice) {
                        if (storePrices.contains(store.getPriceRating())) {
                            storeListFiltered.add(store);
                        }
                    } else {
                        storeListFiltered.add(store);
                    }

                }

            }

            if (isAnyTagSelected) {
                ArrayList<Store> arrayList = new ArrayList<>();
                for (Store filterStore : storeListFiltered) {
                    boolean isAdded = false;
                    for (String selectedTag : selectedTagList) {
                        if (filterStore.getFamousProductsTags().contains(selectedTag)) {
                            isAdded = true;
                        }
                    }
                    if (isAdded) {
                        arrayList.add(filterStore);
                    }

                }
                storeListFiltered.clear();
                storeListFiltered.addAll(arrayList);
            }
        }
        storeAdapter.setStoreArrayList(storeListFiltered);
        storeAdapter.notifyDataSetChanged();
    }

    /**
     * set price view in for store filter
     */

    private void setFilterPriceData() {
        btnPriceOne.setText(currentBooking.getCurrency());
        btnPriceTwo.setText(currentBooking.getCurrency() + currentBooking
                .getCurrency());
        btnPriceThree.setText(currentBooking.getCurrency() + currentBooking
                .getCurrency() +
                currentBooking.getCurrency());
        btnPriceFour.setText(currentBooking.getCurrency() + currentBooking
                .getCurrency() +
                currentBooking.getCurrency() + currentBooking.getCurrency());
    }


    /**
     * This method is used to setUpMap option which help to load map as per option
     */
    private void setUpMap() {

        /*googleMap.getUiSettings().setTiltGesturesEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);




        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View myContentView = getLayoutInflater().inflate(
                        R.layout.layout_marker_info, null);
                Store store = (Store) marker.getTag();
                if (marker.getTag() == null) {
                    return null;
                }
                CustomFontTextViewTitle tvTitle = (CustomFontTextViewTitle) myContentView
                        .findViewById(R.id.title);
                CustomFontTextView tvSnippet = (CustomFontTextView) myContentView
                        .findViewById(R.id.snippet);
                CustomFontTextView tvOpen = (CustomFontTextView) myContentView.findViewById(R.id
                        .tvOpen);
                if (store != null) {
                    tvOpen.setText(store.getReOpenTime());
                    tvTitle.setText(store.getName());
                    if (store.getDeliveryTime() == 0) {
                        tvSnippet.setVisibility(View.GONE);
                    } else {
                        tvSnippet.setText(store.getDeliveryTime() + " " +
                                getResources().getString(R.string.unit_mins));
                    }

                }


                return myContentView;
            }
        });*/

       /* googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                gotToStoreProductActivity((Store) marker.getTag());
            }
        });*/




        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission
                .ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat
                .checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
      //  googleMap.setMyLocationEnabled(true);
     //   googleMap.getUiSettings().setMyLocationButtonEnabled(true);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_LOCATION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        moveCameraFirstMyLocation(true, null);
                    }
                    break;
                default:
                    break;
            }
        }

    }



    private void setStoreMarkerOnMap() {
    /*    if (googleMap != null) {
            googleMap.clear();
            LatLngBounds.Builder bounds = new LatLngBounds.Builder();
            for (Store store : storeListOriginal) {
                if (store.getLocation() != null && !store.getLocation().isEmpty()) {
                    LatLng latLng = new LatLng
                            (store.getLocation().get(0), store.getLocation().get
                                    (1));
                    AppLog.Log("LAT", latLng.toString());
                    MarkerOptions markerOptions = new MarkerOptions().position(latLng)
                            .title(store.getName()).snippet(String.valueOf(store
                                    .getDeliveryTime()) + " " +
                                    getResources().getString(R.string.unit_mins));
                    if (storeBitmap != null) {
                        markerOptions.icon(BitmapDescriptorFactory
                                .fromBitmap(storeBitmap));
                    }
                    googleMap.addMarker(markerOptions).setTag(store);
                    bounds.include(latLng);
                }
            }
            if (currentBooking.getCurrentLatLng() != null) {
                googleMap.addMarker(new MarkerOptions().position(currentBooking.getCurrentLatLng())
                        .title(getResources().getString(R.string.text_drop_location)).icon
                                (BitmapDescriptorFactory
                                        .fromBitmap(Utils
                                                .drawableToBitmap
                                                        (AppCompatResources.getDrawable(this, R
                                                                .drawable.ic_pin_delivery)))))
                        .setSnippet(preferenceHelper.getFirstName() + " " + preferenceHelper
                                .getLastName());
            }
            if (!storeListOriginal.isEmpty()) {
                cu = CameraUpdateFactory.newLatLngBounds(bounds.build(), getResources()
                        .getDimensionPixelSize(R.dimen.dimen_map_bounce));

            }

        }
*/

    }

    @Override
    protected void onStop() {
        if (storeAdapter != null) {
            storeAdapter.stopAdsScheduled();
        }
        mapbox.onStop();
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapbox.onStart();
    }

    @Override
    protected void onPause() {
        mapbox.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mapbox.onDestroy();
        if (locationEngine != null) {
            locationEngine.removeLocationUpdates(callback);
        }
        mapbox.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapbox.onResume();
        if (storeAdapter != null) {
            storeAdapter.notifyDataSetChanged();
        }


    }

    public void downloadStorePin() {
        Glide.with(this)
                .asBitmap()
                .load(PreferenceHelper.getInstance(getApplicationContext()).getIMAGE_BASE_URL() + deliveries.getMapPinUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable
                .driver_car)
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        AppLog.handleException(getClass().getSimpleName(), e);
                        AppLog.Log("STORE_PIN", "Download failed");
                        storeBitmap = Utils.drawableToBitmap(AppCompatResources.getDrawable
                                (StoresActivity.this, R.drawable.ic_pin_pickup));
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        storeBitmap = resource;
                        AppLog.Log("STORE_PIN", "Download Successfully");
                        return true;                    }


                }).dontAnimate().override(getResources().getDimensionPixelSize(R
                        .dimen.store_pin_width)
                , getResources().getDimensionPixelSize(R
                        .dimen.store_pin_width)).preload(getResources()
                        .getDimensionPixelSize(R
                                .dimen.store_pin_width)
                , getResources().getDimensionPixelSize(R
                        .dimen.store_pin_width));

    }

    private void setFavoriteStore(final Store storeData) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.STORE_ID, storeData.getId());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SetFavouriteResponse> call = apiInterface.setFavouriteStore(ApiClient
                .makeJSONRequestBody(jsonObject));
        call.enqueue(new Callback<SetFavouriteResponse>() {
            @Override
            public void onResponse(Call<SetFavouriteResponse> call, Response<SetFavouriteResponse
                    > response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        currentBooking.getFavourite().clear();
                        currentBooking.setFavourite(response.body().getFavouriteStores());
                    }
                    storeAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<SetFavouriteResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
            }
        });
    }

    private void removeAsFavoriteStore(final Store storeData) {
        Utils.showCustomProgressDialog(this, false);
        final ArrayList<String> store = new ArrayList<>();
        store.add(storeData.getId());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SetFavouriteResponse> call = apiInterface.removeAsFavouriteStore(ApiClient
                .makeGSONRequestBody(new RemoveFavourite(preferenceHelper.getSessionToken(),
                        preferenceHelper.getUserId(), store)));
        call.enqueue(new Callback<SetFavouriteResponse>() {
            @Override
            public void onResponse(Call<SetFavouriteResponse> call, Response<SetFavouriteResponse
                    > response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        currentBooking.getFavourite().clear();
                        currentBooking.setFavourite(response.body().getFavouriteStores());
                    }
                    storeAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<SetFavouriteResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
            }
        });
    }


    private double distanceTo(LatLng start, LatLng stop, boolean isUnitKM) {
        if (start != null & stop != null) {
            Location locationFrom = new Location("");
            locationFrom.setLatitude(start.getLatitude());
            locationFrom.setLongitude(start.getLongitude());

            Location locationTo = new Location("");
            locationTo.setLatitude(stop.getLatitude());
            locationTo.setLongitude(stop.getLongitude());
            if (isUnitKM) {
                return locationFrom.distanceTo(locationTo) * 0.001; // Km
            } else {
                return locationFrom.distanceTo(locationTo) * 0.000621371; // mile
            }

        } else {
            return 0;
        }
    }

    private void initTagView() {
        tagView.addTags(deliveries.getFamousProductsTags());
        tagView.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(View v, int position) {

                if (selectedTagList.contains(deliveries.getFamousProductsTags().get(position)
                )) {
                    selectedTagList.remove(deliveries.getFamousProductsTags().get(position));
                    tagView.setSelect(v, false);
                } else {
                    selectedTagList.add(deliveries.getFamousProductsTags().get(position));
                    tagView.setSelect(v, true);

                }


            }
        });
    }

    public void slidStoreSearchFilterView() {
        if (llCategoryFilter.getVisibility() == View.GONE) {
            ivCategoryFilter.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable
                    .ic_arrow_drop_up_black_24dp));
            ResizeAnimation resizeAnimation = new ResizeAnimation(llCategoryFilter, getResources()
                    .getDimensionPixelSize(R.dimen.dimen_filter_height), llCategoryFilter.getHeight
                    ());
            resizeAnimation.setInterpolator(new LinearInterpolator());
            resizeAnimation.setDuration(300);
            llCategoryFilter.startAnimation(resizeAnimation);
            llCategoryFilter.setVisibility(View.VISIBLE);

        } else {
            ivCategoryFilter.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable
                    .ic_arrow_drop_down_black_24dp));
            ResizeAnimation resizeAnimation = new ResizeAnimation(llCategoryFilter, 1,
                    llCategoryFilter.getHeight());
            resizeAnimation.setInterpolator(new LinearInterpolator());
            resizeAnimation.setDuration(300);
            llCategoryFilter.startAnimation(resizeAnimation);
            llCategoryFilter.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    llCategoryFilter.setVisibility(View.GONE);

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }
}
