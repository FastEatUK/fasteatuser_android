package com.edelivery;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.View;

import com.edelivery.fragments.FeedbackFragment;
import com.edelivery.fragments.InvoiceFragment;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.datamodels.Store;
import com.edelivery.models.datamodels.User;
import com.edelivery.models.responsemodels.ActiveOrderResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.utils.Const.Params.ORDER_ID;

public class OrderCompleteActivity extends BaseAppCompatActivity {

    public String orderId;
    public String payment;
    public ActiveOrderResponse activeOrderResponse;
    public Order order;
    public User user;
    public Store store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_complete);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();

        findViewById();
        setViewListener();
        getExtraData();
    }

    @Override
    protected boolean isValidate() {
        // do somethings
        return false;

    }

    @Override
    protected void findViewById() {
        // do somethings
    }

    @Override
    protected void setViewListener() {
        tvSkip.setOnClickListener(this);
        // do somethings
    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvSkip:
                onBackPressed();
                break;
            default:
                // do with default
                break;
        }
    }


    private void getExtraData() {
        if (getIntent().getExtras() != null)
        {

            if (getIntent().getExtras().getBoolean(Const.GO_TO_INVOICE))
            {
                if (getIntent().getExtras().getParcelable(Const.Params.ORDER) instanceof Order) {

                    /// This Order Model Receive from OrderFragment

                    orderId = getIntent().getExtras().getString(ORDER_ID);
                    order = getIntent().getExtras().getParcelable(Const.Params.ORDER);
                    getOrderStatus(orderId);

                } else if ((getIntent().getExtras().getParcelable(Const.Params.ORDER) instanceof ActiveOrderResponse)) {

                    /// This ActiveOrderResponse Model Receive from OrderTrackActivity
                    tvSkip.setVisibility(View.VISIBLE);
                    orderId = getIntent().getExtras().getString(ORDER_ID);
                    activeOrderResponse = getIntent().getExtras().getParcelable(Const.Params.ORDER);
                    goToFeedbackFragment();
                    //  goToInvoiceFragment();

                }
            } else {

                /// This User Model Receive from HistoryDetailFragment
                tvSkip.setVisibility(View.GONE);
                orderId = getIntent().getExtras().getString(ORDER_ID);
                user = getIntent().getExtras().getParcelable(Const.PROVIDER_DETAIL);
                store = getIntent().getExtras().getParcelable(Const.STORE_DETAIL);

                goToFeedbackFragment();
            }

            orderId = getIntent().getExtras().getString(ORDER_ID);
            user = getIntent().getExtras().getParcelable(Const.PROVIDER_DETAIL);
            store = getIntent().getExtras().getParcelable(Const.STORE_DETAIL);
            goToFeedbackFragment();
        }

    }

    /**
     * this method called webservice for get order status
     *
     * @param orderId oderId in string
     */
    private void getOrderStatus(final String orderId) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(ORDER_ID, orderId);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ORDER_TRACK_ACTIVITY, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ActiveOrderResponse> responseCall = apiInterface.getActiveOrderStatus(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<ActiveOrderResponse>() {
            @Override
            public void onResponse(Call<ActiveOrderResponse> call, Response<ActiveOrderResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    AppLog.Log("ORDER_STATUS", ApiClient.JSONResponse(response.body()));
                    activeOrderResponse = response.body();
                        /*if (order.isUserShowInvoice()) {
                            goToFeedbackFragment();
                        } else {
                            goToInvoiceFragment();
                        }*/
                    tvSkip.setVisibility(View.VISIBLE);
                    goToFeedbackFragment();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), OrderCompleteActivity.this);
                }

            }

            @Override
            public void onFailure(Call<ActiveOrderResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.ORDER_TRACK_ACTIVITY, t);
            }
        });

    }

    private void addFragment(Fragment fragment, boolean addToBackStack,
                             boolean isAnimate, String tag) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (isAnimate) {
            ft.setCustomAnimations(R.anim.slide_in_right,
                    R.anim.slide_out_left, R.anim.slide_in_left,
                    R.anim.slide_out_right);
        }
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(R.id.order_contain_frame, fragment, tag);
        ft.commitAllowingStateLoss();
    }

    private void goToInvoiceFragment() {
        if (getSupportFragmentManager().findFragmentByTag(Const.Tag.INVOICE_FRAGMENT) == null) {
            InvoiceFragment invoiceFragment = new InvoiceFragment();
            addFragment(invoiceFragment, false, false, Const.Tag.INVOICE_FRAGMENT);
        }
    }

    public void goToFeedbackFragment() {
        if (getSupportFragmentManager().findFragmentByTag(Const.Tag.FEEDBACK_FRAGMENT) == null) {
            FeedbackFragment feedbackFragment = new FeedbackFragment();
            addFragment(feedbackFragment, false, false, Const.Tag.FEEDBACK_FRAGMENT);
        }
    }

    @Override
    public void onBackPressed() {

        if (getIntent().getExtras().getBoolean(Const.GO_TO_HOME)) {
            goToHomeActivity();
        } else {
            finish();
        }


    }
}
