package com.edelivery;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;

import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import java.util.Currency;


public class ReferralShareActivity extends BaseAppCompatActivity {

    private CustomFontTextView tvWalletAmount, tvShare, tvReferal;
    private CustomFontTextViewTitle tvReferralCode;
    private CustomFontButton btnShareReferral;
    String RegisterReferalWallet = "";
    String FriendReferalWallet = "";

    String txt_share = "Use this referral code when you first register on the FastEat app to get £xx off your first order. You can share your own code to family and friends and get £xx bonus every time someone registers and places their first order (happyface)";
    String dynamic_text_link = "";
    String fallback_url_ios = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referral_share);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_share));
        findViewById();
        //for currency set convertInSymbol(preferenceHelper.getWalletCurrencyCode())

        RegisterReferalWallet = "£" + parseContent.decimalTwoDigitFormat.format(Double.parseDouble(preferenceHelper.getREGISTER_REFERAL_WALLET()));
        FriendReferalWallet = "£" + parseContent.decimalTwoDigitFormat.format(Double.parseDouble(preferenceHelper.getFRIEND_REFERAL_WALLET()));

        tvReferal.setText("Give" + " " + FriendReferalWallet + ", Get " + RegisterReferalWallet);
        setViewListener();
        setReferaltxtAndGenearteSharetxt();


        createDynamiclinkAndShare();

    }


    public void setReferaltxtAndGenearteSharetxt() {
        tvShare.setText("Use this links to refer your friends and family to FastEat. They get " + FriendReferalWallet +
                " off their first order and you get " + RegisterReferalWallet + " when they complete it.");

        tvReferralCode.setText(preferenceHelper.getReferral());
        String emoji = getEmojiByUnicode(0x1F600);
        // dynamic_text_link = " Use " + preferenceHelper.getReferral() + " referral code when you first register on the FastEat app to get "+ RegisterReferalWallet + " off your first order. You can share your own code to family and friends and get "+ FriendReferalWallet + " bonus every time someone registers and places their first order " + emoji;


    }


    public void createDynamiclinkAndShare() {
        Utils.showCustomProgressDialog(getActivity(), false);

        //String pattern  = BASE_URL+"?"+"&apn="+ getPackageName()+"?referral="+preferenceHelper.getReferral();

        fallback_url_ios = "https://apps.apple.com/us/app/fasteat-fast-food-delivered/id1437779858";

        String sharelinktext = "https://www.fasteat.biz/" + "&apn=" + getPackageName() + "?referral=" + preferenceHelper.getReferral();
        Log.e("share_link", sharelinktext);

        //for create dymanic long link
        DynamicLink dynamicLink = createDynamicLink(Uri.parse(sharelinktext),null, "", "");
        Log.e("long_link4",dynamicLink.getUri().toString());

        //convert long link into short link
        Task<ShortDynamicLink> shortLinkTask1 = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(dynamicLink.getUri())
                .buildShortDynamicLink()
        .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
            @Override
            public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                if (task.isSuccessful()) {
                    Utils.hideCustomProgressDialog();
                    // Short link created
                    Uri shortLink = task.getResult().getShortLink();
                    Uri flowchartLink = task.getResult().getPreviewLink();

                    Log.e("link_for_share",shortLink.toString());

                    dynamic_text_link = preferenceHelper.getFirstName() + " " + preferenceHelper.getLastName() + " " + "has sent you a referral code to get " + FriendReferalWallet + " " + "off your first order with FastEat !" + "\n" + preferenceHelper.getFirstName() + " " + preferenceHelper.getLastName() + " " + "will also get " + RegisterReferalWallet + " " + "bonus when you order, everybody wins !!" + "\n" + "Share your own referral code via the app for " + FriendReferalWallet + " " + "every time one of your family or friends orders !" + "\n" +
                            "Referral code - " + preferenceHelper.getReferral() + "\n" + "\n" + shortLink.toString();

                } else {
                    Utils.hideCustomProgressDialog();
                    // Error
                    // ...
                    Log.e("main", " error " + task.getException());
                }
            }
        });




       /* DynamicLink link =   createDynamicLink(Uri.parse(sharelinktext),null,"","");
*/
       /* Log.e("long_link2",link.getUri().toString());*/

        /* + "&ifl="+fallback_url_ios;*/

        /*"&ofl=" + "https://fasteat.biz"+*/
        // http://95.179.226.81:3000/
        //https://www.fasteat.biz/
        // shorten the link

        /*Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(sharelinktext))
                *//*.setLongLink(Uri.parse("https://fasteat.page.link/?link="+"https://www.fasteat.biz/"+"&apn=" +"com.fastEat"+"&ofl="+"https://fasteat.biz"))
                *//*.setDomainUriPrefix("https://fasteat.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.fastEat")
                        .setFallbackUrl(Uri.parse(fallback_url_ios))
                        .setAppStoreId("1437779858")
                        .setMinimumVersion("1.0")
                        .setIpadFallbackUrl(Uri.parse(fallback_url_ios))
                        .setIpadBundleId("com.fastEat")
                        .setCustomScheme("com.fastEat")
                        .build())
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            Utils.hideCustomProgressDialog();
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink();
                            Log.e("main ", "short link " + shortLink.toString());
                            // share app dialog

                            dynamic_text_link = preferenceHelper.getFirstName() + " " + preferenceHelper.getLastName() + " " + "has sent you a referral code to get " + FriendReferalWallet + " " + "off your first order with FastEat !" + "\n" + preferenceHelper.getFirstName() + " " + preferenceHelper.getLastName() + " " + "will also get " + RegisterReferalWallet + " " + "bonus when you order, everybody wins !!" + "\n" + "Share your own referral code via the app for " + FriendReferalWallet + " " + "every time one of your family or friends orders !" + "\n" +
                                    "Referral code - " + preferenceHelper.getReferral() + "\n" + "\n" + shortLink.toString();



                        } else {
                            Utils.hideCustomProgressDialog();
                            // Error
                            // ...
                            Log.e("main", " error " + task.getException());
                        }
                    }
                });
*/




    }



    public  DynamicLink createDynamicLink(Uri deepLink, Uri imageUrl, String title, String description) {
        DynamicLink dynamicLink = getDynamicLinkBuilder(deepLink, imageUrl, title, description)
                .buildDynamicLink();
        String longDynamicLink = String.valueOf(dynamicLink.getUri());
        longDynamicLink += '&' + "ofl" + '=' + "https://fasteat.biz";  //for other platform url
        return FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(longDynamicLink))
                .buildDynamicLink();
    }

    public  DynamicLink.Builder getDynamicLinkBuilder(Uri deepLink, Uri imageUrl, String title, String description) {
        return FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(deepLink)
                .setDomainUriPrefix("https://fasteat.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.fastEat")
                        .setFallbackUrl(Uri.parse(fallback_url_ios))
                        .setAppStoreId("1437779858")
                        .setMinimumVersion("1.0")
                        .setIpadFallbackUrl(Uri.parse(fallback_url_ios))
                        .setIpadBundleId("com.fastEat")
                        .setCustomScheme("com.fastEat")
                        .build());
    }



    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        tvWalletAmount = (CustomFontTextView) findViewById(R.id.tvWalletAmount);
        tvShare = (CustomFontTextView) findViewById(R.id.tvShare);
        tvReferal = (CustomFontTextView) findViewById(R.id.tvReferal);
        tvReferralCode = (CustomFontTextViewTitle) findViewById(R.id.tvReferralCode);
        btnShareReferral = (CustomFontButton) findViewById(R.id.btnShareReferral);
        CustomFontTextView tag2 = (CustomFontTextView) findViewById(R.id.tag2);
        CustomFontTextView tag1 = (CustomFontTextView) findViewById(R.id.tag1);
        Utils.setTagBackgroundRtlView(this, tag1);
        Utils.setTagBackgroundRtlView(this, tag2);
    }

    @Override
    protected void setViewListener() {
        btnShareReferral.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnShareReferral:
                shareAppAndReferral();

                break;
            default:
                // do with default
                break;
        }
    }

    private void shareAppAndReferral() {

        if (dynamic_text_link.equals("")) {
            createDynamiclinkAndShare();
        }

       /* dynamic_text_link = preferenceHelper.getFirstName() + " " + preferenceHelper.getLastName() + " " + "has sent you a referral code to get " + FriendReferalWallet + " " + "off your first order with FastEat !Just go to www.fasteat.biz, download the app and get the " + FriendReferalWallet + " off your first order !" + "\n" + preferenceHelper.getFirstName() + " " + preferenceHelper.getLastName() + " " + "will also get " + RegisterReferalWallet + " " + "bonus when you order, everybody wins !!" + "\n" + "Share your own referral code via the app for " + FriendReferalWallet + " " + "every time one of your family or friends orders !" + "\n" +
                "Referral code - " + preferenceHelper.getReferral() + "\n" + link1;*/


        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, dynamic_text_link /*+ "\n" + url*/);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.msg_share_referral)));
    }

    public String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private String convertInSymbol(String value) {
        Currency cur = Currency.getInstance(value);
        // Get and print the symbol of the currency
        String symbol = cur.getSymbol();

        return symbol;
    }
}
