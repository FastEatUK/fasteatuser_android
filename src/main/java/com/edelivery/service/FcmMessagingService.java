package com.edelivery.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import android.text.TextUtils;

import com.edelivery.HomeActivity;
import com.edelivery.LoginActivity;
import com.edelivery.OrderTrackActivity;
import com.edelivery.OrdersActivity;
import com.edelivery.ProfileActivity;
import com.edelivery.R;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatNotificationConfig;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elluminati on 08-Feb-2017.
 * <p/>
 * This Class is handle a Notification which send by Google FCM server.
 */
public class FcmMessagingService extends FirebaseMessagingService {

    public static final String MESSAGE = "message";
    public static final String LOGIN_IN_OTHER_DEVICE = "2091";
    public static final String STORE_ACCEPTED_YOUR_ORDER = "2001";
    public static final String STORE_START_PREPARING_YOUR_ORDER = "2002";
    public static final String STORE_READY_YOUR_ORDER = "2003";
    public static final String STORE_REJECTED_YOUR_ORDER = "2004";
    public static final String STORE_CANCELLED_YOUR_ORDER = "2005";
    public static final String DELIVERY_MAN_ACCEPTED = "2081";
    public static final String DELIVERY_MAN_COMING = "2082";
    public static final String DELIVERY_MAN_ARRIVED = "2083";
    public static final String DELIVERY_MAN_PICKED_ORDER = "2084";
    public static final String DELIVERY_MAN_STARTED_DELIVERY = "2085";
    public static final String DELIVERY_MAN_ARRIVED_AT_DESTINATION = "2086";
    public static final String DELIVERY_MAN_COMPLETE_ORDER = "2087";
    public static final String DELIVERY_MAN_CANCEL = "2088";
    public static final String ADMIN_APPROVED = "2006";
    public static final String ADMIN_DECLINE = "2007";
    public static final String SEND_NOTIFICATION = "2032";
    public static final String MASS_SEND_NOTIFICATION = "2033";

    private static final String CHANNEL_ID = "channel_01";

    private Map<String, String> data;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        if (remoteMessage != null) {
            AppLog.Log("FcmMessagingService", "From:" + remoteMessage.getFrom());
            AppLog.Log("FcmMessagingService", "Data:" + remoteMessage.getData());

            if (Freshchat.isFreshchatNotification(remoteMessage)) {

                Log.e("FreshChat_Notification", "Freshchat");

                Freshchat.getInstance(this).handleFcmMessage(this, remoteMessage);

                FreshchatNotificationConfig notificationConfig = new FreshchatNotificationConfig()
                        .setNotificationSoundEnabled(true)
                        .setSmallIcon(getNotificationIcon())
                        .setLargeIcon(getNotificationIcon())
                        .launchActivityOnFinish("BaseAppCompatActivity")
                        .setPriority(NotificationCompat.PRIORITY_HIGH);

                Freshchat.getInstance(getApplicationContext()).setNotificationConfig(notificationConfig);
            } else {
                data = remoteMessage.getData();
                String message = data.get(MESSAGE);
                if (!TextUtils.isEmpty(data.get("message1")) && TextUtils.equals(data.get("message1"), "2033") || TextUtils.equals(data.get("message1"), "2032")) {
                    orderStatus(data.get("message1"));

                    try {
                            PreferenceHelper.getInstance(getApplicationContext()).putpopupmassage("");
                            if (!TextUtils.isEmpty(message)) {
                                PreferenceHelper.getInstance(getApplicationContext()).putisshowpopup(true);
                                PreferenceHelper.getInstance(getApplicationContext()).putpopupmassage(message);
                            }

                    } catch (Exception e) {
                        Log.e("Error_execption", e.getMessage().toString());
                    }
                }

                if (!TextUtils.isEmpty(message)) {
                    orderStatus(message);
                }

            }


        }

    }

    private void sendNotification(String message, int activityId) {

        Log.i("sendNotification", String.valueOf(activityId));
        final int random = new Random().nextInt(1000) + 2000;
        Log.i("notificationId", String.valueOf(random));
        int notificationId = random;
        Intent intent = null;
        switch (activityId) {
            case Const.HOME_ACTIVITY:
                intent = getPackageManager().getLaunchIntentForPackage
                        (getPackageName());
                intent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                break;
            case Const.LOGIN_ACTIVITY:
                intent = new Intent(this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;
            case Const.ORDER_TRACK_ACTIVITY:
                intent = new Intent(this, OrderTrackActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(Const.Params.PUSH_DATA1, data.get(Const.Params
                        .PUSH_DATA1));
                startActivity(intent);
                break;
            default:
                // do with default
                break;
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Intent myint = new Intent(this, HomeActivity.class);
      /*  PendingIntent pendingIntent = PendingIntent.getActivity(this, 0  Request code , intent,
                PendingIntent.FLAG_ONE_SHOT);*/
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, myint,
                PendingIntent.FLAG_CANCEL_CURRENT);
        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(getNotificationIcon())
                .setContentTitle(this.getResources().getString(R.string.app_name))
                .setContentText(message).setContentIntent
                        (pIntent).setAutoCancel(true)
                /* .addAction(getNotificationIcon(), this.getResources().getString(R.string.app_name), pIntent)*/
                .setCategory(Notification.CATEGORY_MESSAGE)
                //  .setVibrate(new long[0])


                .setDefaults(Notification.DEFAULT_SOUND).setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setPriority(Notification.PRIORITY_MAX);
        /*.setOngoing(true);*/

        /*Intent appIntent = new Intent(this, HomeActivity.class);
        PendingIntent.getActivity(this, 0, appIntent, PendingIntent.FLAG_UPDATE_CURRENT);
*/


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            String description = "my_package_first_channel";
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name,
                    NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription(description);

            //   mChannel.enableVibration(true);
            //  mChannel.setVibrationPattern(new long[]{100});
            mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationManager.createNotificationChannel(mChannel);
            notificationBuilder.setChannelId(CHANNEL_ID); // Channel ID

            Notification buildNotification = notificationBuilder.build();

            // notificationManager.cancelAll();
            notificationManager.notify(notificationId, buildNotification);
            //  notificationManager.cancel(notificationId);
        }
      /*  if (PreferenceHelper.getInstance(this).getIsPushNotificationSoundOn()) {
            notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND);
        }*/
       /* notificationManager.notify(notificationId, notificationBuilder
                .build());*/

        //  NotificationManager mNotifyMgr = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
    }


    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build
                .VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }

    private void orderStatus(String status) {
        switch (status) {
            case LOGIN_IN_OTHER_DEVICE:
                sendNotification(getMessage(status), Const.LOGIN_ACTIVITY);
                break;
            case STORE_ACCEPTED_YOUR_ORDER:
            case STORE_START_PREPARING_YOUR_ORDER:
            case STORE_READY_YOUR_ORDER:
            case DELIVERY_MAN_PICKED_ORDER:
            case DELIVERY_MAN_STARTED_DELIVERY:
            case DELIVERY_MAN_ARRIVED_AT_DESTINATION:
            case DELIVERY_MAN_COMPLETE_ORDER:
                Log.i("orderStatus", "DELIVERY_MAN_COMPLETE_ORDER");
                sendNotification(getMessage(status), Const.ORDER_TRACK_ACTIVITY);
                sendBroadcast(Const.Action.ACTION_ORDER_STATUS);
                break;
            case ADMIN_APPROVED:
                Log.i("orderStatus", "ADMIN_APPROVED");
                sendNotification(getMessage(status), Const.HOME_ACTIVITY);
                sendBroadcast(Const.Action.ACTION_ADMIN_APPROVED);
                break;

            case MASS_SEND_NOTIFICATION:
                Log.i("orderStatus", "MASS_SEND_NOTIFICATION");

                sendbroadcasteformass(Const.Action.ACTION_MASS_NOTIFICATION, data.get(MESSAGE));
                break;

            case SEND_NOTIFICATION:
                Log.i("orderStatus", "SEND_NOTIFICATION");

                sendbroadcasteformass(Const.Action.SEND_NOTIFICATION, data.get(MESSAGE));

                break;
            case ADMIN_DECLINE:
                sendNotification(getMessage(status), Const.HOME_ACTIVITY);
                sendBroadcast(Const.Action.ACTION_ADMIN_DECLINE);
                break;
            case DELIVERY_MAN_ACCEPTED:
                Log.i("orderStatus", "DELIVERY_MAN_ACCEPTED");
                sendNotification(getMessage(status), Const.ORDER_TRACK_ACTIVITY);
                sendBroadcastWithData(Const.Action.ACTION_DELIVERYMEN_ACCEPTED);
              /*  Intent intent = new Intent(this, OrderTrackActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(Const.Params.PUSH_DATA1, data.get(Const.Params
                        .PUSH_DATA1));
                startActivity(intent);*/
                Log.i("DELIVERY_MAN_ACCEPTED", Const.Params.PUSH_DATA1 + "," + data.get(Const.Params
                        .PUSH_DATA1));


               /* Intent i = new Intent(this, OrderTrackActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Log.d("status","=========== Deliver Accept the Order and Redirect the Current Tab =================");
                this.startActivity(i);*/
                break;
            case DELIVERY_MAN_COMING:
            case DELIVERY_MAN_ARRIVED:
                break;
            case STORE_REJECTED_YOUR_ORDER:
            case STORE_CANCELLED_YOUR_ORDER:
                sendNotification(getMessage(status), Const.HOME_ACTIVITY);
                sendBroadcast(Const.Action.ACTION_ORDER_STATUS);
                break;
            case DELIVERY_MAN_CANCEL:
                sendNotification(getMessage(status), Const.HOME_ACTIVITY);
                sendBroadcast(Const.Action.ACTION_DELIVERYMAN_CANCEL);
                break;
            default:

                if (data.get("message1") != null) {
                    if (TextUtils.equals(data.get("message1"), "2033") || TextUtils.equals(data.get("message1"), "2032")) {
                        if (!isAppForground(this)) {
                            sendNotification(status, Const.HOME_ACTIVITY);
                        }

                    }
                } else {
                    sendNotification(status, Const.HOME_ACTIVITY);
                }


                break;
        }

    }

    private String getMessage(String code) {
        String msg = "";
        String messageCode = Const.PUSH_MESSAGE_PREFIX + code;
        try {
            msg = this.getResources().getString(
                    this.getResources().getIdentifier(messageCode, Const.STRING,
                            this.getPackageName()));
        } catch (Resources.NotFoundException e) {
            msg = messageCode;
            AppLog.Log(FcmMessagingService.class.getName(), msg);
        }
        return msg;
    }


    private void sendBroadcast(String action) {
        sendBroadcast(new Intent(action));
    }

    private void sendbroadcasteformass(String action, String massage) {
        Intent intent = new Intent(action);
        // Bundle bundle = new Bundle();
        intent.putExtra(MESSAGE, massage);
        //  bundle.putString(Const.Params.PUSH_DATA2, data.get(Const.Params.PUSH_DATA2));
        //intent.putExtra(Const.Params.NEW_ORDER, bundle);
        //   Log.i("ACTION_DELIVERYMEN_sending",data.get(Const.Params.PUSH_DATA1));
        sendBroadcast(intent);
    }

    private void sendBroadcastWithData(String action) {
        Intent intent = new Intent(action);
        // Bundle bundle = new Bundle();
        intent.putExtra(Const.Params.PUSH_DATA1, data.get(Const.Params.PUSH_DATA1));
        //  bundle.putString(Const.Params.PUSH_DATA2, data.get(Const.Params.PUSH_DATA2));
        //intent.putExtra(Const.Params.NEW_ORDER, bundle);
        Log.i("ACTION_DELIVERYMEN_sending", data.get(Const.Params.PUSH_DATA1));
        sendBroadcast(intent);
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Freshchat.getInstance(this).setPushRegistrationToken(token);
        AppLog.Log(FcmMessagingService.class.getSimpleName(), "FCM Token Refresh = " + token);
        PreferenceHelper.getInstance(this).putDeviceToken(token);
        if (!TextUtils.equals(token, PreferenceHelper.getInstance(this).getDeviceToken()) &&
                !TextUtils
                        .isEmpty(PreferenceHelper.getInstance(this).getSessionToken())) {
            upDeviceToken(token);
        }
    }

    private void upDeviceToken(String deviceToken) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, PreferenceHelper.getInstance(this)
                    .getSessionToken());
            jsonObject.put(Const.Params.DEVICE_TOKEN, deviceToken);
            jsonObject.put(Const.Params.USER_ID, PreferenceHelper.getInstance(this).getUserId
                    ());
        } catch (JSONException e) {
            AppLog.handleThrowable(FcmMessagingService.class.getSimpleName(), e);
        }


        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.updateDeviceToken(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                if (response != null && response.body() != null && ParseContent.getInstance().isSuccessful(response) && ParseContent.getInstance().isSuccessful(response)) {
                    if (!response.body().isSuccess()) {
                        AppLog.Log(FcmMessagingService.class.getSimpleName(), String.valueOf
                                (response
                                        .body().getErrorCode()));
                    }

                }

            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(FcmMessagingService.class.getSimpleName(), t);
            }
        });

    }

    public static boolean isAppForground(Context context) {

        ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> l = mActivityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo info : l) {
            if (info.uid == context.getApplicationInfo().uid && info.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }
        return false;
    }

}
