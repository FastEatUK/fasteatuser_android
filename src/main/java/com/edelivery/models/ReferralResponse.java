package com.edelivery.models;


import com.edelivery.models.datamodels.OrderPayment;

public class ReferralResponse {


    private OrderPayment order_payment;

    private Boolean success;

    private int message;

    public OrderPayment getOrder_payment() {
        return order_payment;
    }

    public void setOrder_payment(OrderPayment order_payment) {
        this.order_payment = order_payment;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ClassPojo [order_payment = " + order_payment + ", success = " + success + ", message = " + message + "]";
    }

}
