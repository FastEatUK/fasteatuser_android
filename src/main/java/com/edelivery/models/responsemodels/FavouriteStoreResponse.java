package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.Store;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by elluminati on 10-Nov-17.
 */

public class FavouriteStoreResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private Integer message;
    @SerializedName("server_time")
    @Expose
    private String serverTime;
    @SerializedName("favourite_stores")
    @Expose
    private List<Store> favouriteStores = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getMessage() {
        return message;
    }

    public void setMessage(Integer message) {
        this.message = message;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public List<Store> getFavouriteStores() {
        return favouriteStores;
    }

    public void setFavouriteStores(List<Store> favouriteStores) {
        this.favouriteStores = favouriteStores;
    }


}
