package com.edelivery.models.responsemodels;

import java.util.List;

import com.edelivery.models.datamodels.Provider;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AvailableProviderResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("providers")
    @Expose
    private List<Provider> providers = null;
    @SerializedName("total_order_greater_than_three")
    @Expose
    private boolean totalOrderGreaterThanThree;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Provider> getProviders() {
        return providers;
    }

    public void setProviders(List<Provider> providers) {
        this.providers = providers;
    }

    public boolean isTotalOrderGreaterThanThree() {
        return totalOrderGreaterThanThree;
    }

    public void setTotalOrderGreaterThanThree(boolean totalOrderGreaterThanThree) {
        this.totalOrderGreaterThanThree = totalOrderGreaterThanThree;
    }
}
