package com.edelivery.models.responsemodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class IsSuccessResponse {

    @SerializedName("is_payment_paid")
    @Expose
    private boolean isPaymentPaid;
    @SerializedName("is_use_wallet")
    @Expose
    private boolean isWalletUse;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    @Expose
    private int message;

    @SerializedName("error_code")
    @Expose
    private int errorCode;

    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("order_id")
    @Expose
    private String orderId;

    @SerializedName("messages")
    @Expose
    public String messages;

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isWalletUse() {
        return isWalletUse;
    }

    public void setWalletUse(boolean walletUse) {
        isWalletUse = walletUse;
    }

    public boolean isPaymentPaid() {
        return isPaymentPaid;
    }

    public void setPaymentPaid(boolean paymentPaid) {
        isPaymentPaid = paymentPaid;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

}