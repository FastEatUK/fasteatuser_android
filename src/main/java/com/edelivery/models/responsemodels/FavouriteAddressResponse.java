package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.Addresses;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FavouriteAddressResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("favourite_addresses")
    @Expose
    private List<Addresses> favouriteAddresses = null;
    @SerializedName("error_code")
    @Expose
    private int errorCode;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Addresses> getFavouriteAddresses() {
        return favouriteAddresses;
    }

    public void setFavouriteAddresses(List<Addresses> favouriteAddresses) {
        this.favouriteAddresses = favouriteAddresses;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
