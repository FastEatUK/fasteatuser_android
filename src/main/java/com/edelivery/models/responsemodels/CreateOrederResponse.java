package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.Request;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateOrederResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;

    @SerializedName("error_code")
    @Expose
    private int errorCode;
    @SerializedName("message")
    @Expose
    private Integer message;
    @SerializedName("request")
    @Expose
    private Request request;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("order_id")
    @Expose
    private String orderId;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getMessage() {
        return message;
    }

    public void setMessage(Integer message) {
        this.message = message;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }
}
