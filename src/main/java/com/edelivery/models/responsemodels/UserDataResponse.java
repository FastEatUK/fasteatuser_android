package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserDataResponse implements Serializable {

    @SerializedName("success")
    @Expose
    private boolean success;


    @SerializedName("message")
    @Expose
    private int message;

    @SerializedName("user")
    @Expose
    private User user;

    @SerializedName("referral_bonus_to_user_friend")
    @Expose
    public String referralBonusToUserFriend;


    @SerializedName("referral_bonus_to_user")
    @Expose
    public String referralBonusToUser;

    public String getReferralBonusToUserFriend() {
        return referralBonusToUserFriend;
    }

    public void setReferralBonusToUserFriend(String referralBonusToUserFriend) {
        this.referralBonusToUserFriend = referralBonusToUserFriend;
    }

    public String getReferralBonusToUser() {
        return referralBonusToUser;
    }

    public void setReferralBonusToUser(String referralBonusToUser) {
        this.referralBonusToUser = referralBonusToUser;
    }


    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }


    public void setMessage(int message) {
        this.message = message;
    }

    public int getMessage() {
        return message;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @SerializedName("error_code")
    @Expose
    private int errorCode;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @SerializedName("minimum_phone_number_length")
    @Expose
    private int minPhoneNumberLength;

    @SerializedName("maximum_phone_number_length")
    @Expose
    private int maxPhoneNumberLength;

    public int getMinPhoneNumberLength() {
        return minPhoneNumberLength;
    }

    public void setMinPhoneNumberLength(int minPhoneNumberLength) {
        this.minPhoneNumberLength = minPhoneNumberLength;
    }

    public int getMaxPhoneNumberLength() {
        return maxPhoneNumberLength;
    }

    public void setMaxPhoneNumberLength(int maxPhoneNumberLength) {
        this.maxPhoneNumberLength = maxPhoneNumberLength;
    }
}