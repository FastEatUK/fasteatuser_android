package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.OrderPayment;
import com.edelivery.models.datamodels.Service;
import com.edelivery.models.datamodels.Store;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InvoiceResponse {


    @SerializedName("min_order_price")
    private double minOrderPrice;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("payment_gateway_name")
    private String payment;
    @SerializedName("order_payment")
    @Expose
    private OrderPayment orderPayment;
    @SerializedName("store")
    @Expose

    private Store store;
    @SerializedName("server_time")
    @Expose
    private String serverTime;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("service")
    @Expose
    private Service service;
    @SerializedName("message")
    @Expose
    private int message;
    @SerializedName("error_code")
    @Expose
    private int errorCode;


    public double getMinOrderPrice() {
        return minOrderPrice;
    }

    public void setMinOrderPrice(double minOrderPrice) {
        this.minOrderPrice = minOrderPrice;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public OrderPayment getOrderPayment() {
        return orderPayment;
    }

    public void setOrderPayment(OrderPayment orderPayment) {
        this.orderPayment = orderPayment;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}