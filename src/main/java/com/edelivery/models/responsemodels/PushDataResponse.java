package com.edelivery.models.responsemodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PushDataResponse implements Parcelable {

    public static final Parcelable.Creator<PushDataResponse> CREATOR = new Parcelable
            .Creator<PushDataResponse>() {
        @Override
        public PushDataResponse createFromParcel(Parcel source) {
            return new PushDataResponse(source);
        }

        @Override
        public PushDataResponse[] newArray(int size) {
            return new PushDataResponse[size];
        }
    };
    @SerializedName("unique_id")
    private String uniqueId;
    @SerializedName("store_name")
    private String storeName;
    @SerializedName("order_id")
    private String orderId;
    @SerializedName("store_image")
    private String storeImage;

    public PushDataResponse() {
    }

    protected PushDataResponse(Parcel in) {
        this.uniqueId = in.readString();
        this.storeName = in.readString();
        this.orderId = in.readString();
        this.storeImage = in.readString();
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStoreImage() {
        return storeImage;
    }

    public void setStoreImage(String storeImage) {
        this.storeImage = storeImage;
    }

    @Override
    public String toString() {
        return
                "PushDataResponse{" +
                        "unique_id = '" + uniqueId + '\'' +
                        ",store_name = '" + storeName + '\'' +
                        ",order_id = '" + orderId + '\'' +
                        ",store_image = '" + storeImage + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uniqueId);
        dest.writeString(this.storeName);
        dest.writeString(this.orderId);
        dest.writeString(this.storeImage);
    }
}