package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.OrderHistoryDetail;
import com.edelivery.models.datamodels.Store;
import com.edelivery.models.datamodels.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderHistoryDetailResponse {

    @SerializedName("currency")
    private String currency;
    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private int message;
    @SerializedName("order_list")
    private OrderHistoryDetail orderHistoryDetail;
    @SerializedName("error_code")
    @Expose
    private int errorCode;
    @SerializedName("provider_detail")
    private User user;
    @SerializedName("store_detail")
    private Store store;
    @SerializedName("payment_gateway_name")
    private String payment;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public OrderHistoryDetail getOrderHistoryDetail() {
        return orderHistoryDetail;
    }

    public void setOrderHistoryDetail(OrderHistoryDetail orderHistoryDetail) {
        this.orderHistoryDetail = orderHistoryDetail;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }


}