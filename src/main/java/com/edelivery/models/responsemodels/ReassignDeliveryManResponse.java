package com.edelivery.models.responsemodels;

import android.content.Intent;

import com.edelivery.models.datamodels.Provider;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReassignDeliveryManResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private Integer message;
    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("provider_detail")
    @Expose
    private Provider providerDetail;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getMessage() {
        return message;
    }

    public void setMessage(Integer message) {
        this.message = message;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public Provider getProviderDetail() {
        return providerDetail;
    }

    public void setProviderDetail(Provider providerDetail) {
        this.providerDetail = providerDetail;
    }
}
