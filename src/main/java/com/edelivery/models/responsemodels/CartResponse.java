package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.Addresses;
import com.edelivery.models.datamodels.CartOrder;
import com.edelivery.models.datamodels.StoreTime;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartResponse {


    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("is_use_item_tax")
    @Expose
    private boolean isUseItemTax;
    @SerializedName("item_tax")
    @Expose
    private double tax;
    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("destination_addresses")
    private List<Addresses> destinationAddresses;

    @SerializedName("pickup_addresses")
    private List<Addresses> pickupAddresses;

    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private int message;

    @SerializedName("cart_id")
    @Expose
    private String cartId;
    @SerializedName("cart")
    @Expose
    private CartOrder cart;
    @SerializedName("error_code")
    @Expose
    private int errorCode;
    @SerializedName("store_time")
    @Expose
    private List<StoreTime> storeTime;
    @SerializedName("location")
    @Expose
    private List<Double> location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isUseItemTax() {
        return isUseItemTax;
    }

    public void setUseItemTax(boolean useItemTax) {
        this.isUseItemTax = useItemTax;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public List<Addresses> getPickupAddresses() {
        return pickupAddresses;
    }

    public void setPickupAddresses(List<Addresses> pickupAddresses) {
        this.pickupAddresses = pickupAddresses;
    }

    public List<Addresses> getDestinationAddresses() {
        return destinationAddresses;
    }

    public void setDestinationAddresses(List<Addresses> destinationAddresses) {
        this.destinationAddresses = destinationAddresses;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public CartOrder getCart() {
        return cart;
    }

    public void setCart(CartOrder cart) {
        this.cart = cart;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public List<StoreTime> getStoreTime() {
        return storeTime;
    }

    public void setStoreTime(List<StoreTime> storeTime) {
        this.storeTime = storeTime;
    }

    public List<Double> getLocation() {
        return location;
    }

    public void setLocation(List<Double> location) {
        this.location = location;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }
}