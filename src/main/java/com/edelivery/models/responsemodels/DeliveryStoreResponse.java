package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.Ads;
import com.edelivery.models.datamodels.City;
import com.edelivery.models.datamodels.CityData;
import com.edelivery.models.datamodels.Deliveries;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeliveryStoreResponse {

    @SerializedName("deliveries")
    @Expose
    private List<Deliveries> deliveries;

    @SerializedName("ads")
    private List<Ads> ads;
    @SerializedName("city_data")
    @Expose
    private CityData cityData;
    @SerializedName("server_time")
    @Expose
    private String serverTime;
    @SerializedName("city")
    @Expose
    private City city;
    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private int message;
    @SerializedName("error_code")
    @Expose
    private int errorCode;
    @SerializedName("currency_sign")
    @Expose
    private String currencySign;


    @SerializedName("currency_code")
    @Expose
    private String currencycode;

    public List<Deliveries> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(List<Deliveries> deliveries) {
        this.deliveries = deliveries;
    }

    public List<Ads> getAds() {
        return ads;
    }

    public void setAds(List<Ads> ads) {
        this.ads = ads;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getCurrencySign() {
        return currencySign;
    }

    public void setCurrencySign(String currencySign) {
        this.currencySign = currencySign;
    }

    public String getCurrencycode() {
        return currencycode;
    }

    public void setCurrencycode(String currencycode) {
        this.currencycode = currencycode;
    }

    public CityData getCityData() {
        return cityData;
    }

    public void setCityData(CityData cityData) {
        this.cityData = cityData;
    }

}