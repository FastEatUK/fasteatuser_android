package com.edelivery.models.responsemodels;

import com.edelivery.models.datamodels.Countries;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CountriesResponse{

	@SerializedName("success")
	@Expose
	private boolean success;

	@SerializedName("countries")
	@Expose
	private List<Countries> countries;

	@SerializedName("message")
	@Expose
	private int message;

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setCountries(List<Countries> countries){
		this.countries = countries;
	}

	public List<Countries> getCountries(){
		return countries;
	}

	public void setMessage(int message){
		this.message = message;
	}

	public int getMessage(){
		return message;
	}
	@SerializedName("error_code")
	@Expose
	private int errorCode;

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode= errorCode;
	}
}