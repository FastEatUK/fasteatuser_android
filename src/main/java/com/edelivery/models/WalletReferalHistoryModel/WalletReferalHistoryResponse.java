package com.edelivery.models.WalletReferalHistoryModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WalletReferalHistoryResponse {
    @SerializedName("error_code")
    @Expose
    private int errorCode;

    @SerializedName("currency_code")
    @Expose
    private String currencyCode;


    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("register_referal_wallet")
    @Expose
    private String registerReferalWallet;
    @SerializedName("friend_referal_wallet")
    @Expose
    private String friendReferalWallet;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("total_wallets")
    @Expose
    private String totalWallets;



    @SerializedName("wallet_history")
    @Expose
    private List<WalletReferalHistory> walletHistory = null;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getRegisterReferalWallet() {
        return registerReferalWallet;
    }

    public void setRegisterReferalWallet(String registerReferalWallet) {
        this.registerReferalWallet = registerReferalWallet;
    }

    public String getFriendReferalWallet() {
        return friendReferalWallet;
    }

    public void setFriendReferalWallet(String friendReferalWallet) {
        this.friendReferalWallet = friendReferalWallet;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotalWallets() {
        return totalWallets;
    }

    public void setTotalWallets(String totalWallets) {
        this.totalWallets = totalWallets;
    }

    public List<WalletReferalHistory> getWalletHistory() {
        return walletHistory;
    }

    public void setWalletHistory(List<WalletReferalHistory> walletHistory) {
        this.walletHistory = walletHistory;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
