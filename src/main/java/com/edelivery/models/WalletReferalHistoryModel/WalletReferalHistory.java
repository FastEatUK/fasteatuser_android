package com.edelivery.models.WalletReferalHistoryModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class WalletReferalHistory {

    @SerializedName("_id")
    @Expose
    private String id;


    @SerializedName("negative")
    @Expose
    private Boolean negative;


    @SerializedName("unique_id")
    @Expose
    private String uniqueId;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("user_unique_id")
    @Expose
    private String userUniqueId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("__v")
    @Expose
    private String v;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("total_wallet_amount")
    @Expose
    private String totalWalletAmount;
    @SerializedName("added_wallet")
    @Expose
    private String addedWallet;
    @SerializedName("wallet_amount")
    @Expose
    private String walletAmount;
    @SerializedName("wallet_description")
    @Expose
    private String walletDescription;
    @SerializedName("wallet_comment_id")
    @Expose
    private String walletCommentId;
    @SerializedName("wallet_status")
    @Expose
    private String walletStatus;

    @SerializedName("current_rate")
    @Expose
    private String currentRate;
    @SerializedName("to_currency_code")
    @Expose
    private String toCurrencyCode;
    @SerializedName("from_currency_code")
    @Expose
    private String fromCurrencyCode;
    @SerializedName("from_amount")
    @Expose
    private String fromAmount;

    public Boolean getNegative() {
        return negative;
    }

    public void setNegative(Boolean negative) {
        this.negative = negative;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserUniqueId() {
        return userUniqueId;
    }

    public void setUserUniqueId(String userUniqueId) {
        this.userUniqueId = userUniqueId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getTotalWalletAmount() {
        return totalWalletAmount;
    }

    public void setTotalWalletAmount(String totalWalletAmount) {
        this.totalWalletAmount = totalWalletAmount;
    }

    public String getAddedWallet() {
        return addedWallet;
    }

    public void setAddedWallet(String addedWallet) {
        this.addedWallet = addedWallet;
    }

    public String getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(String walletAmount) {
        this.walletAmount = walletAmount;
    }

    public String getWalletDescription() {
        return walletDescription;
    }

    public void setWalletDescription(String walletDescription) {
        this.walletDescription = walletDescription;
    }

    public String getWalletCommentId() {
        return walletCommentId;
    }

    public void setWalletCommentId(String walletCommentId) {
        this.walletCommentId = walletCommentId;
    }

    public String getWalletStatus() {
        return walletStatus;
    }

    public void setWalletStatus(String walletStatus) {
        this.walletStatus = walletStatus;
    }

   /* public WalletReferalInformation getWalletInformation() {
        return walletInformation;
    }

    public void setWalletInformation(WalletReferalInformation walletInformation) {
        this.walletInformation = walletInformation;
    }*/

    public String getCurrentRate() {
        return currentRate;
    }

    public void setCurrentRate(String currentRate) {
        this.currentRate = currentRate;
    }

    public String getToCurrencyCode() {
        return toCurrencyCode;
    }

    public void setToCurrencyCode(String toCurrencyCode) {
        this.toCurrencyCode = toCurrencyCode;
    }

    public String getFromCurrencyCode() {
        return fromCurrencyCode;
    }

    public void setFromCurrencyCode(String fromCurrencyCode) {
        this.fromCurrencyCode = fromCurrencyCode;
    }

    public String getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(String fromAmount) {
        this.fromAmount = fromAmount;
    }
}
