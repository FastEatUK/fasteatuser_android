package com.edelivery.models.WalletReferalHistoryModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
public class WalletReferalInformation {

    @SerializedName("user_friend_id")
    @Expose
    private String userFriendId;

    public String getUserFriendId() {
        return userFriendId;
    }

    public void setUserFriendId(String userFriendId) {
        this.userFriendId = userFriendId;
    }
}