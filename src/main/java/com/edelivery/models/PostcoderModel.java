package com.edelivery.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostcoderModel {

@SerializedName("addressline1")
@Expose
private String addressline1;
@SerializedName("summaryline")
@Expose
private String summaryline;
@SerializedName("number")
@Expose
private String number;
@SerializedName("premise")
@Expose
private String premise;
@SerializedName("street")
@Expose
private String street;
@SerializedName("posttown")
@Expose
private String posttown;
@SerializedName("county")
@Expose
private String county;
@SerializedName("postcode")
@Expose
private String postcode;
@SerializedName("latitude")
@Expose
private String latitude;
@SerializedName("longitude")
@Expose
private String longitude;
@SerializedName("grideasting")
@Expose
private String grideasting;
@SerializedName("gridnorthing")
@Expose
private String gridnorthing;

public String getAddressline1() {
return addressline1;
}

public void setAddressline1(String addressline1) {
this.addressline1 = addressline1;
}

public String getSummaryline() {
return summaryline;
}

public void setSummaryline(String summaryline) {
this.summaryline = summaryline;
}

public String getNumber() {
return number;
}

public void setNumber(String number) {
this.number = number;
}

public String getPremise() {
return premise;
}

public void setPremise(String premise) {
this.premise = premise;
}

public String getStreet() {
return street;
}

public void setStreet(String street) {
this.street = street;
}

public String getPosttown() {
return posttown;
}

public void setPosttown(String posttown) {
this.posttown = posttown;
}

public String getCounty() {
return county;
}

public void setCounty(String county) {
this.county = county;
}

public String getPostcode() {
return postcode;
}

public void setPostcode(String postcode) {
this.postcode = postcode;
}

public String getLatitude() {
return latitude;
}

public void setLatitude(String latitude) {
this.latitude = latitude;
}

public String getLongitude() {
return longitude;
}

public void setLongitude(String longitude) {
this.longitude = longitude;
}

public String getGrideasting() {
return grideasting;
}

public void setGrideasting(String grideasting) {
this.grideasting = grideasting;
}

public String getGridnorthing() {
return gridnorthing;
}

public void setGridnorthing(String gridnorthing) {
this.gridnorthing = gridnorthing;
}

}