package com.edelivery.models.datamodels;

import java.io.Serializable;

public class ConfirmationData implements Serializable {
    private String user_id;
    private String Otp;
    private String MobileNo;
    private String Contrycode;

    public String getContrycode() {
        return Contrycode;
    }

    public void setContrycode(String contrycode) {
        Contrycode = contrycode;
    }

    private String socialtype;
    private boolean isUpdateuser=false;

    public boolean isUpdateuser() {
        return isUpdateuser;
    }

    public void setUpdateuser(boolean updateuser) {
        isUpdateuser = updateuser;
    }

    public String getSocialtype() {
        return socialtype;
    }

    public void setSocialtype(String socialtype) {
        this.socialtype = socialtype;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOtp() {
        return Otp;
    }

    public void setOtp(String otp) {
        Otp = otp;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }
}
