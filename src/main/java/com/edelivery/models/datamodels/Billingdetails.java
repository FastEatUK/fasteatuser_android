package com.edelivery.models.datamodels;

import org.json.JSONObject;

public class Billingdetails {


    public String billingname;
    public String billingemail;

    public String getBillingname() {
        return billingname;
    }

    public void setBillingname(String billingname) {
        this.billingname = billingname;
    }

    public String getBillingemail() {
        return billingemail;
    }

    public void setBillingemail(String billingemail) {
        this.billingemail = billingemail;
    }
}
