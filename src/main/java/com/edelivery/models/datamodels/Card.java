package com.edelivery.models.datamodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Card {

	@SerializedName("_id")
	@Expose
	private String id;
	@SerializedName("unique_id")
	@Expose
	private Integer uniqueId;
	@SerializedName("updated_at")
	@Expose
	private String updatedAt;
	@SerializedName("payment_id")
	@Expose
	private String paymentId;
	@SerializedName("user_type")
	@Expose
	private Integer userType;
	@SerializedName("user_id")
	@Expose
	private String userId;
	@SerializedName("__v")
	@Expose
	private Integer v;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("card_number")
	@Expose
	private String cardNumber;
	@SerializedName("last_four")
	@Expose
	private String lastFour;
	@SerializedName("is_default")
	@Expose
	private Boolean isDefault;
	@SerializedName("customer_id")
	@Expose
	private String customerId;
	@SerializedName("card_holder_name")
	@Expose
	private String cardHolderName;
	@SerializedName("card_expiry_date")
	@Expose
	private String cardExpiryDate;

	@SerializedName("card_cvv")
	@Expose
	private String cardcvv;

	@SerializedName("card_type")
	@Expose
	private String cardType;
	@SerializedName("payment_token")
	@Expose
	private String paymentToken;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(Integer uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getV() {
		return v;
	}

	public void setV(Integer v) {
		this.v = v;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getLastFour() {
		return lastFour;
	}

	public void setLastFour(String lastFour) {
		this.lastFour = lastFour;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public String getCardExpiryDate() {
		return cardExpiryDate;
	}

	public void setCardExpiryDate(String cardExpiryDate) {
		this.cardExpiryDate = cardExpiryDate;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getPaymentToken() {
		return paymentToken;
	}

	public void setPaymentToken(String paymentToken) {
		this.paymentToken = paymentToken;
	}

	public String getCardcvv() {
		return cardcvv;
	}

	public void setCardcvv(String cardcvv) {
		this.cardcvv = cardcvv;
	}
}

