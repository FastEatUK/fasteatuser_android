package com.edelivery.models.datamodels;

import com.google.gson.annotations.SerializedName;

public class Ads {
    @SerializedName("ads_detail")
    private String adsDetail = "";
    @SerializedName("store_id")
    private String storeId;
    @SerializedName("unique_id")
    private int uniqueId;
    @SerializedName("ads_type")
    private int adsType;
    @SerializedName("image_url")
    private String imageUrl;
    @SerializedName("expiry_date")
    private String expiryDate;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("is_ads_redirect_to_store")
    private boolean isAdsRedirectToStore;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("ads_for")
    private int adsFor;
    @SerializedName("is_ads_visible")
    private boolean isAdsVisible;
    @SerializedName("_id")
    private String id;
    @SerializedName("is_ads_have_expiry_date")
    private boolean isAdsHaveExpiryDate;
    @SerializedName("country_id")
    private String countryId;
    @SerializedName("is_ads_block_by_admin")
    private boolean isAdsBlockByAdmin;
    @SerializedName("city_id")
    private String cityId;

    public String getAdsDetail() {
        return adsDetail;
    }

    public void setAdsDetail(String adsDetail) {
        this.adsDetail = adsDetail;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getAdsType() {
        return adsType;
    }

    public void setAdsType(int adsType) {
        this.adsType = adsType;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


    public boolean isIsAdsRedirectToStore() {
        return isAdsRedirectToStore;
    }

    public void setIsAdsRedirectToStore(boolean isAdsRedirectToStore) {
        this.isAdsRedirectToStore = isAdsRedirectToStore;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getAdsFor() {
        return adsFor;
    }

    public void setAdsFor(int adsFor) {
        this.adsFor = adsFor;
    }

    public boolean isIsAdsVisible() {
        return isAdsVisible;
    }

    public void setIsAdsVisible(boolean isAdsVisible) {
        this.isAdsVisible = isAdsVisible;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isIsAdsHaveExpiryDate() {
        return isAdsHaveExpiryDate;
    }

    public void setIsAdsHaveExpiryDate(boolean isAdsHaveExpiryDate) {
        this.isAdsHaveExpiryDate = isAdsHaveExpiryDate;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public boolean isIsAdsBlockByAdmin() {
        return isAdsBlockByAdmin;
    }

    public void setIsAdsBlockByAdmin(boolean isAdsBlockByAdmin) {
        this.isAdsBlockByAdmin = isAdsBlockByAdmin;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

}