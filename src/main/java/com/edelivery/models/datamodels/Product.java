package com.edelivery.models.datamodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Product {


    private boolean isProductFiltered = true;

    @SerializedName("store_id")
    @Expose
    private String storeId;

    @SerializedName("items")
    @Expose
    private List<ProductItem> items;
    @SerializedName("_id")
    private ProductDetail productDetail;


    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public List<ProductItem> getItems() {
        return items;
    }

    public void setItems(List<ProductItem> items) {
        this.items = items;
    }

    public ProductDetail getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(ProductDetail productDetail) {
        this.productDetail = productDetail;
    }


    public Product copy() {
        Product product = new Product();
        product.setStoreId(storeId);
        product.setItems(new ArrayList<ProductItem>());
        product.setProductDetail(productDetail);
        return product;
    }

    public boolean isProductFiltered() {
        return isProductFiltered;
    }

    public void setProductFiltered(boolean productFiltered) {
        isProductFiltered = productFiltered;
    }
}