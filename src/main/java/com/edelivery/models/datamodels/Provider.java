package com.edelivery.models.datamodels;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Provider {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("unique_id")
    @Expose
    private Integer uniqueId;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("provider_type")
    @Expose
    private Integer providerType;
    @SerializedName("admin_type")
    @Expose
    private Integer adminType;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("start_online_time")
    @Expose
    private String startOnlineTime;
    @SerializedName("bearing")
    @Expose
    private double bearing;
    @SerializedName("location")
    @Expose
    private List<Double> location = null;
    @SerializedName("start_active_job_time")
    @Expose
    private String startActiveJobTime;
    @SerializedName("previous_location")
    @Expose
    private List<Double> previousLocation = null;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("location_updated_time")
    @Expose
    private String locationUpdatedTime;
    @SerializedName("is_provider_type_approved")
    @Expose
    private Boolean isProviderTypeApproved;
    @SerializedName("is_document_uploaded")
    @Expose
    private Boolean isDocumentUploaded;
    @SerializedName("is_phone_number_verified")
    @Expose
    private Boolean isPhoneNumberVerified;
    @SerializedName("is_email_verified")
    @Expose
    private Boolean isEmailVerified;
    @SerializedName("is_in_delivery")
    @Expose
    private Boolean isInDelivery;
    @SerializedName("is_online")
    @Expose
    private Boolean isOnline;
    @SerializedName("is_active_for_job")
    @Expose
    private Boolean isActiveForJob;
    @SerializedName("is_approved")
    @Expose
    private Boolean isApproved;
    @SerializedName("total_active_job_time")
    @Expose
    private Double totalActiveJobTime;
    @SerializedName("total_online_time")
    @Expose
    private Double totalOnlineTime;
    @SerializedName("total_completed_requests")
    @Expose
    private Double totalCompletedRequests;
    @SerializedName("total_cancelled_requests")
    @Expose
    private Double totalCancelledRequests;
    @SerializedName("total_rejected_requests")
    @Expose
    private Double totalRejectedRequests;
    @SerializedName("total_accepted_requests")
    @Expose
    private Double totalAcceptedRequests;
    @SerializedName("total_requests")
    @Expose
    private Double totalRequests;
    @SerializedName("current_request")
    @Expose
    private List<Object> currentRequest = null;
    @SerializedName("requests")
    @Expose
    private List<Object> requests = null;
    @SerializedName("total_referrals")
    @Expose
    private float totalReferrals;
    @SerializedName("wallet_currency_code")
    @Expose
    private String walletCurrencyCode;
    @SerializedName("wallet")
    @Expose
    private String wallet;
    @SerializedName("referral_code")
    @Expose
    private String referralCode;
    @SerializedName("store_rate_count")
    @Expose
    private Integer storeRateCount;
    @SerializedName("store_rate")
    @Expose
    private float storeRate;
    @SerializedName("user_rate_count")
    @Expose
    private Integer userRateCount;
    @SerializedName("user_rate")
    @Expose
    private Float userRate;
    @SerializedName("server_token")
    @Expose
    private String serverToken;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("vehicle_number")
    @Expose
    private String vehicleNumber;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("country_phone_code")
    @Expose
    private String countryPhoneCode;
    @SerializedName("app_version")
    @Expose
    private String appVersion;
    @SerializedName("login_by")
    @Expose
    private String loginBy;
    @SerializedName("social_ids")
    @Expose
    private List<Object> socialIds = null;
    @SerializedName("social_id")
    @Expose
    private String socialId;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("account_id")
    @Expose
    private String accountId;
    @SerializedName("bank_id")
    @Expose
    private String bankId;
    @SerializedName("service_id")
    @Expose
    private List<Object> serviceId = null;
    @SerializedName("vehicle_ids")
    @Expose
    private List<String> vehicleIds = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getProviderType() {
        return providerType;
    }

    public void setProviderType(Integer providerType) {
        this.providerType = providerType;
    }

    public Integer getAdminType() {
        return adminType;
    }

    public void setAdminType(Integer adminType) {
        this.adminType = adminType;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getStartOnlineTime() {
        return startOnlineTime;
    }

    public void setStartOnlineTime(String startOnlineTime) {
        this.startOnlineTime = startOnlineTime;
    }

    public double getBearing() {
        return bearing;
    }

    public void setBearing(double bearing) {
        this.bearing = bearing;
    }

    public List<Double> getLocation() {
        return location;
    }

    public void setLocation(List<Double> location) {
        this.location = location;
    }

    public String getStartActiveJobTime() {
        return startActiveJobTime;
    }

    public void setStartActiveJobTime(String startActiveJobTime) {
        this.startActiveJobTime = startActiveJobTime;
    }

    public List<Double> getPreviousLocation() {
        return previousLocation;
    }

    public void setPreviousLocation(List<Double> previousLocation) {
        this.previousLocation = previousLocation;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getLocationUpdatedTime() {
        return locationUpdatedTime;
    }

    public void setLocationUpdatedTime(String locationUpdatedTime) {
        this.locationUpdatedTime = locationUpdatedTime;
    }

    public Boolean getIsProviderTypeApproved() {
        return isProviderTypeApproved;
    }

    public void setIsProviderTypeApproved(Boolean isProviderTypeApproved) {
        this.isProviderTypeApproved = isProviderTypeApproved;
    }

    public Boolean getIsDocumentUploaded() {
        return isDocumentUploaded;
    }

    public void setIsDocumentUploaded(Boolean isDocumentUploaded) {
        this.isDocumentUploaded = isDocumentUploaded;
    }

    public Boolean getIsPhoneNumberVerified() {
        return isPhoneNumberVerified;
    }

    public void setIsPhoneNumberVerified(Boolean isPhoneNumberVerified) {
        this.isPhoneNumberVerified = isPhoneNumberVerified;
    }

    public Boolean getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(Boolean isEmailVerified) {
        this.isEmailVerified = isEmailVerified;
    }

    public Boolean getIsInDelivery() {
        return isInDelivery;
    }

    public void setIsInDelivery(Boolean isInDelivery) {
        this.isInDelivery = isInDelivery;
    }

    public Boolean getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Boolean isOnline) {
        this.isOnline = isOnline;
    }

    public Boolean getIsActiveForJob() {
        return isActiveForJob;
    }

    public void setIsActiveForJob(Boolean isActiveForJob) {
        this.isActiveForJob = isActiveForJob;
    }

    public Boolean getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        this.isApproved = isApproved;
    }

    public Double getTotalActiveJobTime() {
        return totalActiveJobTime;
    }

    public void setTotalActiveJobTime(Double totalActiveJobTime) {
        this.totalActiveJobTime = totalActiveJobTime;
    }

    public Double getTotalOnlineTime() {
        return totalOnlineTime;
    }

    public void setTotalOnlineTime(Double totalOnlineTime) {
        this.totalOnlineTime = totalOnlineTime;
    }

    public Double getTotalCompletedRequests() {
        return totalCompletedRequests;
    }

    public void setTotalCompletedRequests(Double totalCompletedRequests) {
        this.totalCompletedRequests = totalCompletedRequests;
    }

    public Double getTotalCancelledRequests() {
        return totalCancelledRequests;
    }

    public void setTotalCancelledRequests(Double totalCancelledRequests) {
        this.totalCancelledRequests = totalCancelledRequests;
    }

    public Double getTotalRejectedRequests() {
        return totalRejectedRequests;
    }

    public void setTotalRejectedRequests(Double totalRejectedRequests) {
        this.totalRejectedRequests = totalRejectedRequests;
    }

    public Double getTotalAcceptedRequests() {
        return totalAcceptedRequests;
    }

    public void setTotalAcceptedRequests(Double totalAcceptedRequests) {
        this.totalAcceptedRequests = totalAcceptedRequests;
    }

    public Double getTotalRequests() {
        return totalRequests;
    }

    public void setTotalRequests(Double totalRequests) {
        this.totalRequests = totalRequests;
    }

    public List<Object> getCurrentRequest() {
        return currentRequest;
    }

    public void setCurrentRequest(List<Object> currentRequest) {
        this.currentRequest = currentRequest;
    }

    public List<Object> getRequests() {
        return requests;
    }

    public void setRequests(List<Object> requests) {
        this.requests = requests;
    }

    public float getTotalReferrals() {
        return totalReferrals;
    }

    public void setTotalReferrals(float totalReferrals) {
        this.totalReferrals = totalReferrals;
    }

    public String getWalletCurrencyCode() {
        return walletCurrencyCode;
    }

    public void setWalletCurrencyCode(String walletCurrencyCode) {
        this.walletCurrencyCode = walletCurrencyCode;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public Integer getStoreRateCount() {
        return storeRateCount;
    }

    public void setStoreRateCount(Integer storeRateCount) {
        this.storeRateCount = storeRateCount;
    }

    public float getStoreRate() {
        return storeRate;
    }

    public void setStoreRate(float storeRate) {
        this.storeRate = storeRate;
    }

    public Integer getUserRateCount() {
        return userRateCount;
    }

    public void setUserRateCount(Integer userRateCount) {
        this.userRateCount = userRateCount;
    }

    public Float getUserRate() {
        return userRate;
    }

    public void setUserRate(Float userRate) {
        this.userRate = userRate;
    }

    public String getServerToken() {
        return serverToken;
    }

    public void setServerToken(String serverToken) {
        this.serverToken = serverToken;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountryPhoneCode() {
        return countryPhoneCode;
    }

    public void setCountryPhoneCode(String countryPhoneCode) {
        this.countryPhoneCode = countryPhoneCode;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getLoginBy() {
        return loginBy;
    }

    public void setLoginBy(String loginBy) {
        this.loginBy = loginBy;
    }

    public List<Object> getSocialIds() {
        return socialIds;
    }

    public void setSocialIds(List<Object> socialIds) {
        this.socialIds = socialIds;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public List<Object> getServiceId() {
        return serviceId;
    }

    public void setServiceId(List<Object> serviceId) {
        this.serviceId = serviceId;
    }

    public List<String> getVehicleIds() {
        return vehicleIds;
    }

    public void setVehicleIds(List<String> vehicleIds) {
        this.vehicleIds = vehicleIds;
    }
}