package com.edelivery.models.datamodels;

import android.graphics.Bitmap;

import java.io.Serializable;

public class RegistartionData implements Serializable {
    private String Firstname;
    private String Lastname;
    private String SocialType;
    private String SocialId;
    private String Email;
    private String password;
    private Bitmap Image;
    private String ReferalCode;

    public Bitmap getImage() {
        return Image;
    }

    public void setImage(Bitmap image) {
        Image = image;
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String firstname) {
        Firstname = firstname;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String lastname) {
        Lastname = lastname;
    }

    public String getSocialType() {
        return SocialType;
    }

    public void setSocialType(String socialType) {
        SocialType = socialType;
    }

    public String getSocialId() {
        return SocialId;
    }

    public void setSocialId(String socialId) {
        SocialId = socialId;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getReferalCode() {
        return ReferalCode;
    }

    public void setReferalCode(String referalCode) {
        ReferalCode = referalCode;
    }
}
