package com.edelivery.models.datamodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartOrder {

    @SerializedName("user_type")
    private int userType;
    @SerializedName("cart_unique_token")
    @Expose
    private String androidId;
    @SerializedName("store_id")
    @Expose
    private String storeId;
    @SerializedName("pickup_addresses")
    private List<Addresses> pickupAddresses;
    @SerializedName("destination_addresses")
    private List<Addresses> destinationAddresses;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("server_token")
    @Expose
    private String serverToken;
    @SerializedName("order_details")
    @Expose
    private List<CartProducts> products;
    @SerializedName("total_item_tax")
    @Expose
    private double cartOrderTotalTaxPrice;
    @SerializedName("total_cart_price")
    @Expose
    private double cartOrderTotalPrice;
    @SerializedName("order_payment_id")
    @Expose
    private String orderPaymentId;

    public double getCartOrderTotalTaxPrice() {
        return cartOrderTotalTaxPrice;
    }

    public void setCartOrderTotalTaxPrice(double cartOrderTotalTaxPrice) {
        this.cartOrderTotalTaxPrice = cartOrderTotalTaxPrice;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public List<Addresses> getPickupAddresses() {
        return pickupAddresses;
    }

    public void setPickupAddresses(List<Addresses> pickupAddresses) {
        this.pickupAddresses = pickupAddresses;
    }

    public List<Addresses> getDestinationAddresses() {
        return destinationAddresses;
    }

    public void setDestinationAddresses(List<Addresses> destinationAddresses) {
        this.destinationAddresses = destinationAddresses;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getServerToken() {
        return serverToken;
    }

    public void setServerToken(String serverToken) {
        this.serverToken = serverToken;
    }

    public List<CartProducts> getProducts() {
        return products;
    }

    public void setProducts(List<CartProducts> products) {
        this.products = products;
    }

    public double getCartOrderTotalPrice() {
        return cartOrderTotalPrice;
    }

    public void setCartOrderTotalPrice(double cartOrderTotalPrice) {
        this.cartOrderTotalPrice = cartOrderTotalPrice;
    }

    public String getOrderPaymentId() {
        return orderPaymentId;
    }

    public void setOrderPaymentId(String orderPaymentId) {
        this.orderPaymentId = orderPaymentId;
    }


    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }


}