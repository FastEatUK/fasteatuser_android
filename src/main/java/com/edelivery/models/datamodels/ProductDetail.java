package com.edelivery.models.datamodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by elluminati on 18-Apr-17.
 */

public class ProductDetail implements Parcelable {


    @SerializedName("unique_id")
    private int uniqueId;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("image_url")
    private String imageUrl;

    @SerializedName("name")
    private String name;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("details")
    private String details;

    @SerializedName("_id")
    private String id;

    @SerializedName("sequence_number")
    private int sequenceNumber;

    @SerializedName("note")
    private String note;

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDetails() {
        return details;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.uniqueId);
        dest.writeString(this.updatedAt);
        dest.writeString(this.imageUrl);
        dest.writeString(this.name);
        dest.writeString(this.createdAt);
        dest.writeString(this.details);
        dest.writeString(this.id);
        dest.writeInt(this.sequenceNumber);
        dest.writeString(this.note);
    }

    public ProductDetail() {
    }

    protected ProductDetail(Parcel in) {
        this.uniqueId = in.readInt();
        this.updatedAt = in.readString();
        this.imageUrl = in.readString();
        this.name = in.readString();
        this.createdAt = in.readString();
        this.details = in.readString();
        this.id = in.readString();
        this.sequenceNumber = in.readInt();
        this.note = in.readString();
    }

    public static final Creator<ProductDetail> CREATOR = new Creator<ProductDetail>() {
        @Override
        public ProductDetail createFromParcel(Parcel source) {
            return new ProductDetail(source);
        }

        @Override
        public ProductDetail[] newArray(int size) {
            return new ProductDetail[size];
        }
    };
}
