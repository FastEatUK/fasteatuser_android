package com.edelivery.models.datamodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Request {
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("unique_id")
    @Expose
    private Integer uniqueId;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("vehicle_id")
    @Expose
    private Object vehicleId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_unique_id")
    @Expose
    private Integer userUniqueId;
    @SerializedName("request_type_id")
    @Expose
    private String requestTypeId;
    @SerializedName("provider_type")
    @Expose
    private Integer providerType;
    @SerializedName("provider_type_id")
    @Expose
    private String providerTypeId;
    @SerializedName("provider_id")
    @Expose
    private Object providerId;
    @SerializedName("provider_unique_id")
    @Expose
    private Integer providerUniqueId;
    @SerializedName("delivery_status_by")
    @Expose
    private Object deliveryStatusBy;
    @SerializedName("current_provider")
    @Expose
    private String currentProvider;
    @SerializedName("confirmation_code_for_pick_up_delivery")
    @Expose
    private Integer confirmationCodeForPickUpDelivery;
    @SerializedName("confirmation_code_for_complete_delivery")
    @Expose
    private Integer confirmationCodeForCompleteDelivery;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("completed_at")
    @Expose
    private Object completedAt;
    @SerializedName("completed_date_in_city_timezone")
    @Expose
    private Object completedDateInCityTimezone;
    @SerializedName("completed_date_tag")
    @Expose
    private String completedDateTag;
    @SerializedName("provider_previous_location")
    @Expose
    private List<Double> providerPreviousLocation = null;
    @SerializedName("provider_location")
    @Expose
    private List<Double> providerLocation = null;
    @SerializedName("is_forced_assigned")
    @Expose
    private Boolean isForcedAssigned;
    @SerializedName("providers_id_that_rejected_order_request")
    @Expose
    private List<Object> providersIdThatRejectedOrderRequest = null;
    @SerializedName("estimated_time_for_delivery_in_min")
    @Expose
    private Integer estimatedTimeForDeliveryInMin;
    @SerializedName("delivery_status_manage_id")
    @Expose
    private Integer deliveryStatusManageId;
    @SerializedName("delivery_status")
    @Expose
    private Integer deliveryStatus;
    @SerializedName("request_type")
    @Expose
    private Integer requestType;
    @SerializedName("orders")
    @Expose
    private List<Order> orders = null;
    @SerializedName("timezone")
    @Expose
    private String timezone;

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public Object getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Object vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getUserUniqueId() {
        return userUniqueId;
    }

    public void setUserUniqueId(Integer userUniqueId) {
        this.userUniqueId = userUniqueId;
    }

    public String getRequestTypeId() {
        return requestTypeId;
    }

    public void setRequestTypeId(String requestTypeId) {
        this.requestTypeId = requestTypeId;
    }

    public Integer getProviderType() {
        return providerType;
    }

    public void setProviderType(Integer providerType) {
        this.providerType = providerType;
    }

    public String getProviderTypeId() {
        return providerTypeId;
    }

    public void setProviderTypeId(String providerTypeId) {
        this.providerTypeId = providerTypeId;
    }

    public Object getProviderId() {
        return providerId;
    }

    public void setProviderId(Object providerId) {
        this.providerId = providerId;
    }

    public Integer getProviderUniqueId() {
        return providerUniqueId;
    }

    public void setProviderUniqueId(Integer providerUniqueId) {
        this.providerUniqueId = providerUniqueId;
    }

    public Object getDeliveryStatusBy() {
        return deliveryStatusBy;
    }

    public void setDeliveryStatusBy(Object deliveryStatusBy) {
        this.deliveryStatusBy = deliveryStatusBy;
    }

    public String getCurrentProvider() {
        return currentProvider;
    }

    public void setCurrentProvider(String currentProvider) {
        this.currentProvider = currentProvider;
    }

    public Integer getConfirmationCodeForPickUpDelivery() {
        return confirmationCodeForPickUpDelivery;
    }

    public void setConfirmationCodeForPickUpDelivery(Integer confirmationCodeForPickUpDelivery) {
        this.confirmationCodeForPickUpDelivery = confirmationCodeForPickUpDelivery;
    }

    public Integer getConfirmationCodeForCompleteDelivery() {
        return confirmationCodeForCompleteDelivery;
    }

    public void setConfirmationCodeForCompleteDelivery(Integer confirmationCodeForCompleteDelivery) {
        this.confirmationCodeForCompleteDelivery = confirmationCodeForCompleteDelivery;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getCompletedAt() {
        return completedAt;
    }

    public void setCompletedAt(Object completedAt) {
        this.completedAt = completedAt;
    }

    public Object getCompletedDateInCityTimezone() {
        return completedDateInCityTimezone;
    }

    public void setCompletedDateInCityTimezone(Object completedDateInCityTimezone) {
        this.completedDateInCityTimezone = completedDateInCityTimezone;
    }

    public String getCompletedDateTag() {
        return completedDateTag;
    }

    public void setCompletedDateTag(String completedDateTag) {
        this.completedDateTag = completedDateTag;
    }


    public List<Double> getProviderPreviousLocation() {
        return providerPreviousLocation;
    }

    public void setProviderPreviousLocation(List<Double> providerPreviousLocation) {
        this.providerPreviousLocation = providerPreviousLocation;
    }

    public List<Double> getProviderLocation() {
        return providerLocation;
    }

    public void setProviderLocation(List<Double> providerLocation) {
        this.providerLocation = providerLocation;
    }

    public Boolean getIsForcedAssigned() {
        return isForcedAssigned;
    }

    public void setIsForcedAssigned(Boolean isForcedAssigned) {
        this.isForcedAssigned = isForcedAssigned;
    }

    public List<Object> getProvidersIdThatRejectedOrderRequest() {
        return providersIdThatRejectedOrderRequest;
    }

    public void setProvidersIdThatRejectedOrderRequest(List<Object> providersIdThatRejectedOrderRequest) {
        this.providersIdThatRejectedOrderRequest = providersIdThatRejectedOrderRequest;
    }

    public Integer getEstimatedTimeForDeliveryInMin() {
        return estimatedTimeForDeliveryInMin;
    }

    public void setEstimatedTimeForDeliveryInMin(Integer estimatedTimeForDeliveryInMin) {
        this.estimatedTimeForDeliveryInMin = estimatedTimeForDeliveryInMin;
    }

    public Integer getDeliveryStatusManageId() {
        return deliveryStatusManageId;
    }

    public void setDeliveryStatusManageId(Integer deliveryStatusManageId) {
        this.deliveryStatusManageId = deliveryStatusManageId;
    }

    public Integer getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(Integer deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public Integer getRequestType() {
        return requestType;
    }

    public void setRequestType(Integer requestType) {
        this.requestType = requestType;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

}
