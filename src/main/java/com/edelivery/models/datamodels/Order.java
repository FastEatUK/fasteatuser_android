package com.edelivery.models.datamodels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Order implements Parcelable {

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel source) {
            return new Order(source);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };
    @SerializedName("country_detail")
    @Expose
    private Countries countries;
    @SerializedName("request_id")
    @Expose
    private String requestId;
    @SerializedName("is_user_pick_up_order")
    private boolean isUserPickUpOrder;
    @SerializedName("is_confirmation_code_required_at_complete_delivery")
    private boolean isConfirmationCodeRequiredAtCompleteDelivery;
    @SerializedName("total_time")
    @Expose
    private double totalTime;

/*
    public double getTotal_sec() {
        return total_sec;
    }

    public void setTotal_sec(double total_sec) {
        this.total_sec = total_sec;
    }

    @SerializedName("total_sec")
    @Expose
    private double total_sec;
*/


    @SerializedName("delivery_status")
    @Expose
    private int deliveryStatus;
    @SerializedName("request_unique_id")
    @Expose
    private int requestUniqueId;
    @SerializedName("order_status")
    @Expose
    private int orderStatus;
    @SerializedName("is_user_show_invoice")
    @Expose
    private boolean isUserShowInvoice;
    @SerializedName("store_phone")
    @Expose
    private String storePhone;
    @SerializedName("pickup_addresses")
    private List<Addresses> pickupAddresses;
    @SerializedName("destination_addresses")
    private List<Addresses> destinationAddresses;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("store_country_phone_code")
    @Expose
    private String storeCountryPhoneCode;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("promo_code")
    @Expose
    private String promoCode;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("unique_code")
    @Expose
    private int uniqueCode;
    @SerializedName("store_image")
    @Expose
    private String storeImage;
    @SerializedName("total_order_price")
    @Expose
    private double orderTotalPrice;

    @SerializedName("total")
    @Expose
    private double total;
    @SerializedName("cart_detail")
    @Expose
    private OrderData orderData;
    @SerializedName("order_details")
    @Expose
    private List<CartProducts> products;
    @SerializedName("unique_id")
    @Expose
    private int uniqueId;
    @SerializedName("store_id")
    @Expose
    private String storeId;
    @SerializedName("order_id")
    @Expose
    private String orderId;

    public Order() {
    }

    protected Order(Parcel in) {
        this.countries = in.readParcelable(Countries.class.getClassLoader());
        this.requestId = in.readString();
        this.isUserPickUpOrder = in.readByte() != 0;
        this.isConfirmationCodeRequiredAtCompleteDelivery = in.readByte() != 0;
        this.totalTime = in.readDouble();
     //   this.total_sec = in.readDouble();


        this.deliveryStatus = in.readInt();
        this.requestUniqueId = in.readInt();
        this.orderStatus = in.readInt();
        this.isUserShowInvoice = in.readByte() != 0;
        this.storePhone = in.readString();
        this.pickupAddresses = in.createTypedArrayList(Addresses.CREATOR);
        this.destinationAddresses = in.createTypedArrayList(Addresses.CREATOR);
        this.createdAt = in.readString();
        this.storeName = in.readString();
        this.storeCountryPhoneCode = in.readString();
        this.currency = in.readString();
        this.promoCode = in.readString();
        this.id = in.readString();
        this.uniqueCode = in.readInt();
        this.storeImage = in.readString();
        this.orderTotalPrice = in.readDouble();
        this.orderData = in.readParcelable(OrderData.class.getClassLoader());
        this.products = in.createTypedArrayList(CartProducts.CREATOR);
        this.uniqueId = in.readInt();
        this.storeId = in.readString();
        this.orderId = in.readString();
    }

    public static Creator<Order> getCREATOR() {
        return CREATOR;
    }

    public List<CartProducts> getProducts() {
        return products;
    }

    public void setProducts(List<CartProducts> products) {
        this.products = products;
    }

    public OrderData getOrderData() {
        return orderData;
    }

    public void setOrderData(OrderData orderData) {
        this.orderData = orderData;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public int getRequestUniqueId() {
        return requestUniqueId;
    }

    public void setRequestUniqueId(int requestUniqueId) {
        this.requestUniqueId = requestUniqueId;
    }

    public int getDeliveryStatus() {
        return deliveryStatus;

    }

    public void setDeliveryStatus(int deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public List<Addresses> getPickupAddresses() {
        return pickupAddresses;
    }

    public void setPickupAddresses(List<Addresses> pickupAddresses) {
        this.pickupAddresses = pickupAddresses;
    }

    public List<Addresses> getDestinationAddresses() {
        return destinationAddresses;
    }

    public void setDestinationAddresses(List<Addresses> destinationAddresses) {
        this.destinationAddresses = destinationAddresses;
    }

    public boolean isUserPickUpOrder() {
        return isUserPickUpOrder;
    }

    public void setUserPickUpOrder(boolean userPickUpOrder) {
        isUserPickUpOrder = userPickUpOrder;
    }

    public boolean isConfirmationCodeRequiredAtCompleteDelivery() {
        return isConfirmationCodeRequiredAtCompleteDelivery;
    }

    public void setConfirmationCodeRequiredAtCompleteDelivery(boolean confirmationCodeRequiredAtCompleteDelivery) {
        isConfirmationCodeRequiredAtCompleteDelivery = confirmationCodeRequiredAtCompleteDelivery;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getStorePhone() {
        return storePhone;
    }

    public void setStorePhone(String storePhone) {
        this.storePhone = storePhone;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreCountryPhoneCode() {
        return storeCountryPhoneCode;
    }

    public void setStoreCountryPhoneCode(String storeCountryPhoneCode) {
        this.storeCountryPhoneCode = storeCountryPhoneCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(int uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public String getStoreImage() {
        return storeImage;
    }

    public void setStoreImage(String storeImage) {
        this.storeImage = storeImage;
    }

    public double getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public void setOrderTotalPrice(double orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }


    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public boolean isUserShowInvoice() {
        return isUserShowInvoice;
    }

    public void setUserShowInvoice(boolean userShowInvoice) {
        isUserShowInvoice = userShowInvoice;
    }

    public double getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(double totalTime) {
        this.totalTime = totalTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Countries getCountries() {
        return countries;
    }

    public void setCountries(Countries countries) {
        this.countries = countries;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.countries, flags);
        dest.writeString(this.requestId);
        dest.writeByte(this.isUserPickUpOrder ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isConfirmationCodeRequiredAtCompleteDelivery ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.totalTime);
        dest.writeInt(this.deliveryStatus);
        dest.writeInt(this.requestUniqueId);
        dest.writeInt(this.orderStatus);
        dest.writeByte(this.isUserShowInvoice ? (byte) 1 : (byte) 0);
        dest.writeString(this.storePhone);
        dest.writeTypedList(this.pickupAddresses);
        dest.writeTypedList(this.destinationAddresses);
        dest.writeString(this.createdAt);
        dest.writeString(this.storeName);
        dest.writeString(this.storeCountryPhoneCode);
        dest.writeString(this.currency);
        dest.writeString(this.promoCode);
        dest.writeString(this.id);
        dest.writeInt(this.uniqueCode);
        dest.writeString(this.storeImage);
        dest.writeDouble(this.orderTotalPrice);
        dest.writeParcelable(this.orderData, flags);
        dest.writeTypedList(this.products);
        dest.writeInt(this.uniqueId);
        dest.writeString(this.storeId);
        dest.writeString(this.orderId);
    }
}