package com.edelivery.models.datamodels;

import java.io.Serializable;

/**
 * Created by elluminati on 14-Apr-17.
 */

public class Invoice implements Serializable {

    private String title;
    private String subTitle;
    private String price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
