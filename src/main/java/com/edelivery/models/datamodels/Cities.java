package com.edelivery.models.datamodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Cities {

	@SerializedName("is_business")
	@Expose
	private boolean isBusiness;

	@SerializedName("payment_gateway")
	@Expose
	private List<Integer> paymentGateway;

	@SerializedName("timezone")
	@Expose
	private String timezone;

	@SerializedName("city_code")
	@Expose
	private String cityCode;

	@SerializedName("created_at")
	@Expose
	private String createdAt;

	@SerializedName("deliveries_in_city")
	@Expose
	private Object deliveriesInCity;

	@SerializedName("is_promo_apply_for_other")
	@Expose
	private boolean isPromoApplyForOther;

	@SerializedName("city_lat_long")
	@Expose
	private List<Double> cityLatLong;

	@SerializedName("city_name")
	@Expose
	private String cityName;

	@SerializedName("updated_at")
	@Expose
	private String updatedAt;

	@SerializedName("__v")
	@Expose
	private int V;

	@SerializedName("is_cash_payment_mode")
	@Expose
	private boolean isCashPaymentMode;

	@SerializedName("is_other_payment_mode")
	@Expose
	private boolean isOtherPaymentMode;

	@SerializedName("city_radius")
	@Expose
	private int cityRadius;

	@SerializedName("_id")
	@Expose
	private String id;

	@SerializedName("is_promo_apply_for_cash")
	@Expose
	private boolean isPromoApplyForCash;

	@SerializedName("country_id")
	@Expose
	private String countryId;

	public void setIsBusiness(boolean isBusiness){
		this.isBusiness = isBusiness;
	}

	public boolean isIsBusiness(){
		return isBusiness;
	}

	public void setPaymentGateway(List<Integer> paymentGateway){
		this.paymentGateway = paymentGateway;
	}

	public List<Integer> getPaymentGateway(){
		return paymentGateway;
	}

	public void setTimezone(String timezone){
		this.timezone = timezone;
	}

	public String getTimezone(){
		return timezone;
	}

	public void setCityCode(String cityCode){
		this.cityCode = cityCode;
	}

	public String getCityCode(){
		return cityCode;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDeliveriesInCity(Object deliveriesInCity){
		this.deliveriesInCity = deliveriesInCity;
	}

	public Object getDeliveriesInCity(){
		return deliveriesInCity;
	}

	public void setIsPromoApplyForOther(boolean isPromoApplyForOther){
		this.isPromoApplyForOther = isPromoApplyForOther;
	}

	public boolean isIsPromoApplyForOther(){
		return isPromoApplyForOther;
	}

	public void setCityLatLong(List<Double> cityLatLong){
		this.cityLatLong = cityLatLong;
	}

	public List<Double> getCityLatLong(){
		return cityLatLong;
	}

	public void setCityName(String cityName){
		this.cityName = cityName;
	}

	public String getCityName(){
		return cityName;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setV(int V){
		this.V = V;
	}

	public int getV(){
		return V;
	}

	public void setIsCashPaymentMode(boolean isCashPaymentMode){
		this.isCashPaymentMode = isCashPaymentMode;
	}

	public boolean isIsCashPaymentMode(){
		return isCashPaymentMode;
	}

	public void setIsOtherPaymentMode(boolean isOtherPaymentMode){
		this.isOtherPaymentMode = isOtherPaymentMode;
	}

	public boolean isIsOtherPaymentMode(){
		return isOtherPaymentMode;
	}

	public void setCityRadius(int cityRadius){
		this.cityRadius = cityRadius;
	}

	public int getCityRadius(){
		return cityRadius;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setIsPromoApplyForCash(boolean isPromoApplyForCash){
		this.isPromoApplyForCash = isPromoApplyForCash;
	}

	public boolean isIsPromoApplyForCash(){
		return isPromoApplyForCash;
	}

	public void setCountryId(String countryId){
		this.countryId = countryId;
	}

	public String getCountryId(){
		return countryId;
	}
}