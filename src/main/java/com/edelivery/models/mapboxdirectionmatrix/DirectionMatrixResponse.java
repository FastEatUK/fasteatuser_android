
package com.edelivery.models.mapboxdirectionmatrix;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DirectionMatrixResponse {

    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("distances")
    @Expose
    public List<List<String>> distances = null;
    @SerializedName("durations")
    @Expose
    public List<List<String>> durations = null;
    @SerializedName("destinations")

  /*
    @Expose
    public List<Destination> destinations = null;
    @SerializedName("sources")
    @Expose
    public List<Source> sources = null;
*/
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<List<String>> getDistances() {
        return distances;
    }

    public void setDistances(List<List<String>> distances) {
        this.distances = distances;
    }

    public List<List<String>> getDurations() {
        return durations;
    }

    public void setDurations(List<List<String>> durations) {
        this.durations = durations;
    }

    /*public List<Destination> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Destination> destinations) {
        this.destinations = destinations;
    }

    public List<Source> getSources() {
        return sources;
    }

    public void setSources(List<Source> sources) {
        this.sources = sources;
    }
*/
}
