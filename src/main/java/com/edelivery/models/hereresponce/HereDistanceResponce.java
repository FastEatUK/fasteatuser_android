package com.edelivery.models.hereresponce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HereDistanceResponce {

    @SerializedName("response")
    @Expose
    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public class MetaInfo {

        @SerializedName("timestamp")
        @Expose
        private String timestamp;
        @SerializedName("mapVersion")
        @Expose
        private String mapVersion;
        @SerializedName("moduleVersion")
        @Expose
        private String moduleVersion;
        @SerializedName("interfaceVersion")
        @Expose
        private String interfaceVersion;
        @SerializedName("availableMapVersion")
        @Expose
        private List<String> availableMapVersion = null;

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public String getMapVersion() {
            return mapVersion;
        }

        public void setMapVersion(String mapVersion) {
            this.mapVersion = mapVersion;
        }

        public String getModuleVersion() {
            return moduleVersion;
        }

        public void setModuleVersion(String moduleVersion) {
            this.moduleVersion = moduleVersion;
        }

        public String getInterfaceVersion() {
            return interfaceVersion;
        }

        public void setInterfaceVersion(String interfaceVersion) {
            this.interfaceVersion = interfaceVersion;
        }

        public List<String> getAvailableMapVersion() {
            return availableMapVersion;
        }

        public void setAvailableMapVersion(List<String> availableMapVersion) {
            this.availableMapVersion = availableMapVersion;
        }

    }

    public class Response {

        @SerializedName("metaInfo")
        @Expose
        private MetaInfo metaInfo;
        @SerializedName("route")
        @Expose
        private List<Route> route = null;
        @SerializedName("language")
        @Expose
        private String language;

        public MetaInfo getMetaInfo() {
            return metaInfo;
        }

        public void setMetaInfo(MetaInfo metaInfo) {
            this.metaInfo = metaInfo;
        }

        public List<Route> getRoute() {
            return route;
        }

        public void setRoute(List<Route> route) {
            this.route = route;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

    }

    public class Route {

        @SerializedName("summary")
        @Expose
        private Summary summary;

    public Summary getSummary() {
            return summary;
        }

        public void setSummary(Summary summary) {
            this.summary = summary;
        }

    }

    public class Summary {

        @SerializedName("distance")
        @Expose
        private Integer distance;
        @SerializedName("baseTime")
        @Expose
        private Integer baseTime;
        @SerializedName("flags")
        @Expose
        private List<String> flags = null;
        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("travelTime")
        @Expose
        private Integer travelTime;
        @SerializedName("_type")
        @Expose
        private String type;

        public Integer getDistance() {
            return distance;
        }

        public void setDistance(Integer distance) {
            this.distance = distance;
        }

        public Integer getBaseTime() {
            return baseTime;
        }

        public void setBaseTime(Integer baseTime) {
            this.baseTime = baseTime;
        }

        public List<String> getFlags() {
            return flags;
        }

        public void setFlags(List<String> flags) {
            this.flags = flags;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Integer getTravelTime() {
            return travelTime;
        }

        public void setTravelTime(Integer travelTime) {
            this.travelTime = travelTime;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

    }



}
