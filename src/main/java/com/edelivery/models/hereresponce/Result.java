package com.edelivery.models.hereresponce;

import android.os.Parcel;
import android.os.Parcelable;


import com.edelivery.models.datamodels.CartProducts;
import com.edelivery.models.datamodels.OrderData;

import java.io.Serializable;

public class Result implements Parcelable {


    private String title;
    private String adress;
    private String lat;
    private String lng;

    private String post_code;
    private String country;
    private String address_1;
    private String address_2;
    private String city;

    public Result() {

    }


    public Result(Parcel in) {
        title = in.readString();
        adress = in.readString();
        lat = in.readString();
        lng = in.readString();

        post_code = in.readString();
        country = in.readString();
        address_1 = in.readString();
        address_2 = in.readString();
        city = in.readString();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

  /*  public static Creator<Result> getCREATOR() {
        return CREATOR;
    }*/

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress_1() {
        return address_1;
    }

    public void setAddress_1(String address_1) {
        this.address_1 = address_1;
    }

    public String getAddress_2() {
        return address_2;
    }

    public void setAddress_2(String address_2) {
        this.address_2 = address_2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(title);
        parcel.writeString(adress);
        parcel.writeString(lat);
        parcel.writeString(lng);
        parcel.writeString(post_code);
        parcel.writeString(country);
        parcel.writeString(address_1);
        parcel.writeString(address_2);
        parcel.writeString(city);


    }

    public static final Parcelable.Creator<Result> CREATOR = new Parcelable.Creator<Result>() {
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        public Result[] newArray(int size) {
            return new Result[size];

        }
    };


    // all get , set method

}
