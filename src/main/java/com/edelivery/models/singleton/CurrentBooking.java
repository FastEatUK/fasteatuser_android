package com.edelivery.models.singleton;

import android.os.Parcelable;

import com.edelivery.models.datamodels.Addresses;
import com.edelivery.models.datamodels.Ads;
import com.edelivery.models.datamodels.CartProducts;
import com.edelivery.models.datamodels.Deliveries;
import com.edelivery.models.datamodels.ProductItem;
import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by elluminati on 13-Feb-17.
 *
 * @see "Please modify carfully b'coz is effect in wholl app"
 */

public class CurrentBooking {

    private static CurrentBooking currentBooking = new CurrentBooking();

    private ArrayList<Addresses> destinationAddresses = new ArrayList<>();
    private ArrayList<Addresses> pickupAddresses = new ArrayList<>();
    private ArrayList<ProductItem> cartProductItemWithAllSpecificationList = new ArrayList<>();
    private ArrayList<CartProducts> cartProductWithSelectedSpecificationList = new ArrayList<>();
    private ArrayList<Deliveries> deliveryStoreList = new ArrayList<>();
    private List<Ads> ads = new ArrayList<>();
    private List<String> favourite = new ArrayList<>();
    private String bookCityId;
    private String bookCountryId;
    private boolean isCashPaymentMode;
    private boolean isOtherPaymentMode;
    private boolean isPromoApplyForCash;
    private boolean isPromoApplyForOther;
    private String currentCity;
    private String cartCurrency;
    private String currencyCode = "";
    private String selectedStoreId;
    private String deliveryAddress;
    private LatLng deliveryLatLng;
    private double totalCartAmount;
    private String orderPaymentId;
    private String cartId;
    private boolean isApplication = true;
    private boolean isStoreClosed;
    private boolean isHaveOrders;
    private String currency;
    private String currentAddress;
    private LatLng currentLatLng;
    private String country = "";
    private String countryCode = "";
    private String countryCode2 = "";
    private String city1 = "";
    private String city2 = "";
    private String city3 = "";
    private String cityCode = "";

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    private String storename;
    private double paymentLatitude = 0.0;
    private double paymentLongitude = 0.0;
    private double totalInvoiceAmount;
    private String cartCityId;
    private String timeZone;
    private String futureOrderDate;
    private String futureOrderTime;
    private long futureOrderSelectedTimeZoneMillis;
    private long futureOrderSelectedTimeUTCMillis;
    private boolean isFutureOrder;
    private String storeIdBranchIO;
    private List<Double> storeLocation = new ArrayList<>();
    private List<Double> cityLocation = new ArrayList<>();
    private double cityRadius;
    private boolean isDeliveryAvailable;
    private List<Addresses> favAddressList = new ArrayList<>();

    private CurrentBooking() {
        cartProductItemWithAllSpecificationList = new ArrayList<>();
        cartProductWithSelectedSpecificationList = new ArrayList<>();
        deliveryStoreList = new ArrayList<>();
        storeLocation = new ArrayList<>();
        cityLocation = new ArrayList<>();
        ads = new ArrayList<>();
        favourite = new ArrayList<>();
        destinationAddresses = new ArrayList<>();
        pickupAddresses = new ArrayList<>();
        favAddressList = new ArrayList<>();
    }

    public static CurrentBooking getInstance() {
        return currentBooking;
    }

    public String getStoreIdBranchIO() {
        return storeIdBranchIO;
    }

    public void setStoreIdBranchIO(String storeIdBranchIO) {
        this.storeIdBranchIO = storeIdBranchIO;
    }

    public ArrayList<Addresses> getDestinationAddresses() {
        return destinationAddresses;
    }

    public void setDestinationAddresses(Addresses destinationAddresses) {
        this.destinationAddresses.add(destinationAddresses);
    }

    public ArrayList<Addresses> getPickupAddresses() {
        return pickupAddresses;
    }

    public void setPickupAddresses(Addresses pickupAddresses) {
        this.pickupAddresses.add(pickupAddresses);
    }


    public List<String> getFavourite() {
        return favourite;
    }

    public void setFavourite(List<String> favourite) {
        this.favourite.addAll(favourite);
    }

    public double getTotalCartAmount() {
        return totalCartAmount;
    }

    public void setTotalCartAmount(double totalCartAmount) {
        this.totalCartAmount = totalCartAmount;
    }


    public ArrayList<CartProducts> getCartProductWithSelectedSpecificationList() {
        return cartProductWithSelectedSpecificationList;
    }

    public void setCartProduct(CartProducts cartProducts) {
        this.cartProductWithSelectedSpecificationList.add(cartProducts);
    }

    public String getCartCurrency() {
        return cartCurrency;
    }

    public void setCartCurrency(String cartCurrency) {
        this.cartCurrency = cartCurrency;
    }

    public String getSelectedStoreId() {
        return selectedStoreId;
    }

    public void setSelectedStoreId(String selectedStoreId) {
        this.selectedStoreId = selectedStoreId;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public LatLng getDeliveryLatLng() {
        return deliveryLatLng;
    }

    public void setDeliveryLatLng(LatLng deliveryLatLng) {
        this.deliveryLatLng = deliveryLatLng;
    }

    public ArrayList<Deliveries> getDeliveryStoreList() {
        return deliveryStoreList;
    }

    public void setDeliveryStoreList(ArrayList<Deliveries> deliveryStoreList) {
        this.deliveryStoreList.addAll(deliveryStoreList);
    }

    public String getCurrentCity() {
        return currentCity;
    }

    public void setCurrentCity(String currentCity) {
        this.currentCity = currentCity;
    }

    public boolean isCashPaymentMode() {
        return isCashPaymentMode;
    }

    public void setCashPaymentMode(boolean cashPaymentMode) {
        isCashPaymentMode = cashPaymentMode;
    }

    public boolean isOtherPaymentMode() {
        return isOtherPaymentMode;
    }

    public void setOtherPaymentMode(boolean otherPaymentMode) {
        isOtherPaymentMode = otherPaymentMode;
    }

    public boolean isPromoApplyForCash() {
        return isPromoApplyForCash;
    }

    public void setPromoApplyForCash(boolean promoApplyForCash) {
        isPromoApplyForCash = promoApplyForCash;
    }

    public boolean isPromoApplyForOther() {
        return isPromoApplyForOther;
    }

    public void setPromoApplyForOther(boolean promoApplyForOther) {
        isPromoApplyForOther = promoApplyForOther;
    }

    public String getBookCityId() {
        return bookCityId;
    }

    public void setBookCityId(String bookCityId) {
        this.bookCityId = bookCityId;
    }

    public String getBookCountryId() {
        return bookCountryId;
    }

    public void setBookCountryId(String bookCountryId) {
        this.bookCountryId = bookCountryId;
    }

    public void clearCart() {
        cartProductItemWithAllSpecificationList.clear();
        cartProductWithSelectedSpecificationList.clear();
        destinationAddresses.clear();
        pickupAddresses.clear();
        setSelectedStoreId("");
        setTotalCartAmount(0);
        currencyCode = "";
        deliveryAddress = currentAddress;
        deliveryLatLng = currentLatLng;
        totalInvoiceAmount = 0.0;

    }

    public String getOrderPaymentId() {
        return orderPaymentId;
    }

    public void setOrderPaymentId(String orderPaymentId) {
        this.orderPaymentId = orderPaymentId;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public boolean isApplication() {
        return isApplication;
    }

    public void setApplication(boolean application) {
        isApplication = application;
    }

    public boolean isStoreClosed() {
        return isStoreClosed;
    }

    public void setStoreClosed(boolean storeClosed) {
        isStoreClosed = storeClosed;
    }

    public boolean isHaveOrders() {
        return isHaveOrders;
    }

    public void setHaveOrders(boolean haveOrders) {
        isHaveOrders = haveOrders;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public LatLng getCurrentLatLng() {
        return currentLatLng;
    }

    public void setCurrentLatLng(LatLng currentLatLng) {
        this.currentLatLng = currentLatLng;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCity1() {
        return city1;
    }

    public void setCity1(String city1) {
        this.city1 = city1;
    }

    public String getCity2() {
        return city2;
    }

    public void setCity2(String city2) {
        this.city2 = city2;
    }

    public String getCity3() {
        return city3;
    }

    public void setCity3(String city3) {
        this.city3 = city3;
    }

    public double getPaymentLatitude() {
        return paymentLatitude;
    }

    public void setPaymentLatitude(double paymentLatitude) {
        this.paymentLatitude = paymentLatitude;
    }

    public double getPaymentLongitude() {
        return paymentLongitude;
    }

    public void setPaymentLongitude(double paymentLongitude) {
        this.paymentLongitude = paymentLongitude;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCountryCode2() {
        return countryCode2;
    }

    public void setCountryCode2(String countryCode2) {
        this.countryCode2 = countryCode2;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public double getTotalInvoiceAmount() {
        return totalInvoiceAmount;
    }

    public void setTotalInvoiceAmount(double totalInvoiceAmount) {
        this.totalInvoiceAmount = totalInvoiceAmount;
    }

    public String getCartCityId() {
        return cartCityId;
    }

    public void setCartCityId(String cartCityId) {
        this.cartCityId = cartCityId;
    }

    public ArrayList<ProductItem> getCartProductItemWithAllSpecificationList() {
        return cartProductItemWithAllSpecificationList;
    }

    public void setCartProductItemWithAllSpecificationList(ProductItem
                                                                   productItem) {
        this.cartProductItemWithAllSpecificationList.add(productItem);
    }

    public List<Double> getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(List<Double> storeLocation) {
        this.storeLocation = storeLocation;
    }

    public void clearCurrentBookingModel() {
        pickupAddresses.clear();
        destinationAddresses.clear();
        cartProductItemWithAllSpecificationList.clear();

        cartProductWithSelectedSpecificationList.clear();
        deliveryStoreList.clear();
        bookCityId = "";
        bookCountryId = "";
        isCashPaymentMode = false;
        isOtherPaymentMode = false;
        isPromoApplyForCash = false;
        isPromoApplyForOther = false;
        currentCity = "";
        cartCurrency = "";
        selectedStoreId = "";
        deliveryAddress = "";
        deliveryLatLng = null;
        totalCartAmount = 0.0;
        orderPaymentId = "";
        cartId = "";
        isApplication = true;
        isStoreClosed = false;
        isHaveOrders = false;
        currency = "";
        currentAddress = "";
        currentLatLng = null;
        country = "";
        countryCode = "";
        countryCode2 = "";
        city1 = "";
        city2 = "";
        city3 = "";
        cityCode = "";
        paymentLatitude = 0.0;
        paymentLongitude = 0.0;
        currencyCode = "";
        totalInvoiceAmount = 0.0;
        cartCityId = "";
        ads.clear();
        futureOrderDate = "";
        futureOrderTime = "";
        futureOrderSelectedTimeUTCMillis = 0;
        futureOrderSelectedTimeZoneMillis = 0;
        isFutureOrder = false;
        storeIdBranchIO="";
        storeLocation.clear();
        cityLocation.clear();
        cityRadius = 0;
        isDeliveryAvailable = false;
        favAddressList.clear();
    }


    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;


    }

    public List<Ads> getAds() {
        return ads;
    }

    public void setAds(List<Ads> ads) {
        this.ads.addAll(ads);
    }

    public String getFutureOrderDate() {
        return futureOrderDate;
    }

    public void setFutureOrderDate(String futureOrderDate) {
        this.futureOrderDate = futureOrderDate;
    }

    public String getFutureOrderTime() {
        return futureOrderTime;
    }

    public void setFutureOrderTime(String futureOrderTime) {
        this.futureOrderTime = futureOrderTime;
    }

    public long getFutureOrderSelectedTimeZoneMillis() {
        return futureOrderSelectedTimeZoneMillis;
    }

    public void setFutureOrderSelectedTimeZoneMillis(long futureOrderSelectedTimeZoneMillis) {
        this.futureOrderSelectedTimeZoneMillis = futureOrderSelectedTimeZoneMillis;
    }

    public long getFutureOrderSelectedTimeUTCMillis() {
        return futureOrderSelectedTimeUTCMillis;
    }

    public void setFutureOrderSelectedTimeUTCMillis(long futureOrderSelectedTimeUTCMillis) {
        this.futureOrderSelectedTimeUTCMillis = futureOrderSelectedTimeUTCMillis;
    }

    public boolean isFutureOrder() {
        return isFutureOrder;
    }

    public void setFutureOrder(boolean futureOrder) {
        isFutureOrder = futureOrder;
    }

    public List<Double> getCityLocation() {
        return cityLocation;
    }

    public void setCityLocation(List<Double> cityLocation) {
        this.cityLocation = cityLocation;
    }

    public void setCityRadius(double cityRadius) {
        this.cityRadius = cityRadius;
    }

    public double getCityRadius() {
        return cityRadius;
    }

    public boolean isDeliveryAvailable() {
        return isDeliveryAvailable;
    }

    public void setDeliveryAvailable(boolean deliveryAvailable) {
        isDeliveryAvailable = deliveryAvailable;
    }

    public void setFavAddressList(List<Addresses> favAddressList) {
        this.favAddressList = favAddressList;
    }

    public List<Addresses> getFavAddressList() {
        return favAddressList;
    }

    public void setDeliveryLatLng(com.mapbox.mapboxsdk.geometry.LatLng currentLatLng)
    {
        this.deliveryLatLng = deliveryLatLng;

    }
}
