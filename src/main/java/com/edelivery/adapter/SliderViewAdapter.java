package com.edelivery.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edelivery.R;
import com.edelivery.models.datamodels.Pager;

import java.util.ArrayList;

/**
 * Created by elluminati on 09-Jun-17.
 */

public class SliderViewAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private ArrayList<Pager> pagerArrayList;
    private Context context;

    public SliderViewAdapter(Context context, ArrayList<Pager> pagerArrayList) {
        this.context = context;
        this.pagerArrayList = pagerArrayList;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(pagerArrayList.get
                        (position).getLayoutId(),
                container, false);
        //TextView titleTextView = (TextView) view.findViewById(R.id.tvOffer);
       // titleTextView.setText(pagerArrayList.get(position).getData());
        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return pagerArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
