package com.edelivery.adapter;

import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.fragments.ReviewFragment;
import com.edelivery.models.datamodels.RemainingReview;
import com.edelivery.models.datamodels.StoreReview;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.SectionedRecyclerViewAdapter;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

/**
 * Created by elluminati on 21-Nov-17.
 */

public abstract class PublicReviewAdapter extends SectionedRecyclerViewAdapter<RecyclerView
        .ViewHolder> {
    private List<StoreReview> storeReview;
    private ReviewFragment reviewFragment;
    private List<RemainingReview> remainStoreReview;

    public PublicReviewAdapter(List<RemainingReview> remainStoreReview,
                               List<StoreReview> storeReview, ReviewFragment
                                       reviewFragment) {
        this.storeReview = storeReview;
        this.reviewFragment = reviewFragment;
        this.remainStoreReview = remainStoreReview;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (VIEW_TYPE_ITEM == viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review_2,
                    parent, false);
            return new PublicReviewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                            .item_review_2_header,
                    parent, false);
            return new PublicReviewHolderHeader(view);
        }


    }

    @Override
    public int getSectionCount() {
        return 1;
    }

    @Override
    public int getItemCount(int section) {
        return storeReview.size();
    }


    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {
        PublicReviewHolderHeader holderHeader = (PublicReviewHolderHeader) holder;
        if (remainStoreReview.isEmpty()) {
            holderHeader.itemView.setVisibility(View.GONE);
            holderHeader.itemView.getLayoutParams().height = 0;
        } else {
            holderHeader.itemView.setVisibility(View.VISIBLE);
            holderHeader.itemView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            holderHeader.rcvOrderReview.setLayoutManager(new LinearLayoutManager(reviewFragment
                    .getContext(),
                    LinearLayoutManager.HORIZONTAL, false));
            ReviewAdapter reviewAdapter = new ReviewAdapter(remainStoreReview, reviewFragment) {
                @Override
                public void onSelect(int position, float rating) {
                    reviewFragment.goToFeedbackActivity(remainStoreReview
                            .get(position)
                            .getOrderId(), rating);
                }

            };
            holderHeader.rcvOrderReview.setAdapter(reviewAdapter);

        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int section, final int
            relativePosition, int absolutePosition) {

        PublicReviewHolder reviewHolder = (PublicReviewHolder) holder;
        final StoreReview reviewListItem = storeReview.get(relativePosition);
        Glide.with(reviewFragment.getContext()).load(PreferenceHelper.getInstance(reviewFragment.getContext()).getIMAGE_BASE_URL() + reviewListItem
                .getUserDetail().getImageUrl())
                .dontAnimate()
                .placeholder
                        (ResourcesCompat.getDrawable(reviewFragment.getContext()
                                .getResources(), R.drawable.placeholder, null)).fallback
                (ResourcesCompat
                        .getDrawable
                                (reviewFragment.getContext()
                                        .getResources(), R.drawable.placeholder, null)).into
                (reviewHolder.ivUserImage);
        reviewHolder.tvUserName.setText(reviewListItem.getUserDetail().getFirstName() + " " +
                "" + reviewListItem.getUserDetail().getLastName());
        reviewHolder.tvRate.setText(String.valueOf(reviewListItem.getUserRatingToStore()));
        if (TextUtils.isEmpty(reviewListItem.getUserReviewToStore())) {
            reviewHolder.llLike.setVisibility(View.GONE);
            reviewHolder.tvUserComment.setVisibility(View.GONE);
        } else {
            reviewHolder.tvUserComment.setText(reviewListItem.getUserReviewToStore());
            reviewHolder.tvUserComment.setVisibility(View.VISIBLE);
            reviewHolder.llLike.setVisibility(View.VISIBLE);
            reviewHolder.tvLike.setCompoundDrawablesRelativeWithIntrinsicBounds(AppCompatResources
                            .getDrawable(reviewFragment.getContext(), R.drawable
                                    .ic_thumbs_up_01_unselect),
                    null, null, null);
            reviewHolder.tvDisLike.setCompoundDrawablesRelativeWithIntrinsicBounds
                    (AppCompatResources
                                    .getDrawable(reviewFragment.getContext(), R.drawable
                                            .ic_thumbs_down_01_unselect),
                            null, null, null);
            reviewHolder.tvLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reviewListItem.setDislike(false);
                    reviewListItem.setLike(!reviewListItem.isLike());
                    onLike(relativePosition);
                }
            });
            reviewHolder.tvDisLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reviewListItem.setLike(false);
                    reviewListItem.setDislike(!reviewListItem.isDislike());
                    onDislike(relativePosition);
                }
            });
            reviewListItem.setDislike(false);
            reviewListItem.setLike(false);
            if (reviewListItem.getIdOfUsersLikeStoreComment().contains(PreferenceHelper
                    .getInstance(reviewFragment.getContext()).getUserId())) {
                reviewHolder.tvLike.setCompoundDrawablesRelativeWithIntrinsicBounds
                        (AppCompatResources
                                .getDrawable(reviewFragment.getContext(), R.drawable
                                        .ic_thumbs_up_01), null, null, null);
                reviewListItem.setLike(true);

            } else if (reviewListItem.getIdOfUsersDislikeStoreComment().contains(PreferenceHelper
                    .getInstance(reviewFragment.getContext()).getUserId())) {
                reviewHolder.tvDisLike.setCompoundDrawablesRelativeWithIntrinsicBounds
                        (AppCompatResources
                                        .getDrawable(reviewFragment.getContext(), R.drawable
                                                .ic_thumbs_down_01),
                                null, null, null);
                reviewListItem.setDislike(true);
            }


        }
        reviewHolder.tvLike.setText(String.valueOf(reviewListItem.getIdOfUsersLikeStoreComment()
                .size()));
        reviewHolder.tvDisLike.setText(String.valueOf(reviewListItem
                .getIdOfUsersDislikeStoreComment()
                .size()));
        try {
            Date date = ParseContent.getInstance().webFormat.parse(reviewListItem.getCreatedAt());
            reviewHolder.tvDate.setText(ParseContent.getInstance().dateFormat3.format(date));
        } catch (ParseException e) {
            AppLog.handleException(PublicReviewAdapter.class.getName(), e);
        }
    }


    public abstract void onLike(int position);

    public abstract void onDislike(int position);

    protected class PublicReviewHolder extends RecyclerView.ViewHolder {
        CustomFontTextViewTitle tvUserName;
        CustomFontTextView tvRate, tvDate, tvUserComment, tvLike, tvDisLike;
        ImageView ivUserImage;
        LinearLayout llLike;


        public PublicReviewHolder(View itemView) {
            super(itemView);
            tvUserName = (CustomFontTextViewTitle) itemView.findViewById(R.id.tvUserName);
            tvRate = (CustomFontTextView) itemView.findViewById(R.id.tvRate);
            tvDate = (CustomFontTextView) itemView.findViewById(R.id.tvDate);
            tvUserComment = (CustomFontTextView) itemView.findViewById(R.id.tvUserComment);
            tvLike = (CustomFontTextView) itemView.findViewById(R.id.tvLike);
            tvDisLike = (CustomFontTextView) itemView.findViewById(R.id.tvDisLike);
            ivUserImage = (ImageView) itemView.findViewById(R.id.ivUserImage);
            llLike = (LinearLayout) itemView.findViewById(R.id.llLike);


        }


    }

    protected class PublicReviewHolderHeader extends RecyclerView.ViewHolder {
        RecyclerView rcvOrderReview;

        public PublicReviewHolderHeader(View itemView) {
            super(itemView);
            rcvOrderReview = (RecyclerView) itemView.findViewById(R.id.rcvOrderReview);
            SnapHelper snapHelper = new LinearSnapHelper();
            snapHelper.attachToRecyclerView(rcvOrderReview);
        }
    }
}
