package com.edelivery.adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.edelivery.BaseAppCompatActivity;
import com.edelivery.R;

import com.edelivery.component.CustomFontCheckBox;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.WalletReferalHistoryModel.WalletReferalHistory;
import com.edelivery.parser.ParseContent;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;

import android.util.Log;

/**
 * Created by elluminati on 02-Oct-17.
 */

public class RegisterBonusAdapter extends RecyclerView.Adapter<RegisterBonusAdapter.StringFilterViewHolder> {
    private ArrayList<WalletReferalHistory> storeStringList;
    public String str_currency;
    public Context context;
    private ParseContent parseContent;

    public RegisterBonusAdapter(ArrayList<WalletReferalHistory> storeStringList, Context context) {
        parseContent = ParseContent.getInstance();
        this.storeStringList = storeStringList;
        this.context = context;

    }

    @Override
    public StringFilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new StringFilterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R
                .layout.item_register_bonus, parent, false));
    }

    @Override
    public void onBindViewHolder(StringFilterViewHolder holder, int position) {

        str_currency = "£";
        /*if (storeStringList.get(position).getFromCurrencyCode() != null && !storeStringList.get(position).getFromCurrencyCode().equals("")) {

            if (storeStringList.get(position).getFromCurrencyCode().toUpperCase().contains("NULL")) {
                str_currency = convertInSymbol(storeStringList.get(position).getFromCurrencyCode().toUpperCase().replace("NULL", ""));
            } else {
                str_currency = convertInSymbol(storeStringList.get(position).getFromCurrencyCode());
            }
        } else {
            str_currency = "";
        }*/


        holder.tvTitle.setText(storeStringList.get(position).getWalletDescription());
        // holder.tvDate.setText(ParseContent.getInstance().dateFormat.format(date) + " " + ParseContent.getInstance().timeFormat.format(date));


        Date date = null;
        try {
            date = ParseContent.getInstance().webFormat.parse(storeStringList.get(position).getCreatedAt());
            Log.e("date", date.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            String dateValue = getDayOfMonthSuffix(Integer.parseInt(ParseContent.getInstance().day.format(date)));
            holder.tvDate.setText(ParseContent.getInstance().day.format(date) + dateValue + " " + ParseContent.getInstance().dateFormatMonthFull.format(date) + " " + ParseContent.getInstance().timeFormat2.format(date));
        } catch (Exception e) {

        }

        //holder.tvPrice.setText("+" + str_currency + " " + parseContent.decimalTwoDigitFormat.format(storeStringList.get(position).getFromAmount()));

        if (!storeStringList.get(position).getNegative()) {
            holder.tvPrice.setText("+" + str_currency + " " + parseContent.decimalTwoDigitFormat.format(Double.parseDouble(storeStringList.get(position).getFromAmount())));
            holder.tvPrice.setTextColor(context.getResources().getColor(R.color.color_black));
        } else {
            holder.tvPrice.setText("-" + str_currency + " " + parseContent.decimalTwoDigitFormat.format(Double.parseDouble(storeStringList.get(position).getFromAmount())));
            holder.tvPrice.setTextColor(context.getResources().getColor(R.color.tw__composer_red));
        }


    }

    public String getDayOfMonthSuffix(final int n) {
        /* checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);*/
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    private String convertInSymbol(String value) {
        Currency cur = Currency.getInstance(value);
        // Get and print the symbol of the currency
        String symbol = cur.getSymbol();

        return symbol;
    }

    @Override
    public int getItemCount() {
        return storeStringList.size();
    }

    protected class StringFilterViewHolder extends RecyclerView.ViewHolder {
        private CustomFontTextView tvTitle, tvDate, tvPrice;


        public StringFilterViewHolder(View itemView) {
            super(itemView);
            tvTitle = (CustomFontTextView) itemView.findViewById(R.id.tvTitle);
            tvDate = (CustomFontTextView) itemView.findViewById(R.id.tvDate);
            tvPrice = (CustomFontTextView) itemView.findViewById(R.id.tvPrice);

           /* tvStringNameFilter = (CustomFontTextView) itemView.findViewById(R.id
                    .tvStringNameFilter);
            rbSelectStringFilter = (CustomFontCheckBox) itemView.findViewById(R.id
                    .rbSelectStringFilter);
            rbSelectStringFilter.setOnCheckedChangeListener(new CompoundButton
                    .OnCheckedChangeListener() {


                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    storeStringList.get(getAdapterPosition()).setStringFiltered(isChecked);
                }
            });*/
        }
    }
}
