package com.edelivery.adapter;

import android.content.Context;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.OrderHistory;
import com.edelivery.models.datamodels.Store;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PinnedHeaderItemDecoration;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeSet;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

/**
 * Created by elluminati on 02-May-17.
 */

public abstract class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        PinnedHeaderItemDecoration.PinnedHeaderAdapter {

    private static final int HEADER = 1;
    private static final int ITEM = 2;
    private TreeSet<Integer> separatorSet;
    private ArrayList<OrderHistory> orderHistoryArrayList;
    private ParseContent parseContent;
    private Context context;

    public HistoryAdapter(TreeSet<Integer> separatorSet, ArrayList<OrderHistory>
            orderHistoryArrayList) {
        parseContent = ParseContent.getInstance();
        this.orderHistoryArrayList = orderHistoryArrayList;
        this.separatorSet = separatorSet;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        if (viewType == HEADER) {
            return new OrderHistoryDateHolder(LayoutInflater.from(parent.getContext()).inflate(R
                    .layout
                    .item_store_product_header, parent, false));
        } else {
            return new OrderHistoryHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .item_order_hsitory, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {

            OrderHistory orderHistory = orderHistoryArrayList.get(position);

            String dateCreated = orderHistory
                    .getCreatedAt();
            String currentDate = parseContent.dateFormat.format(new Date());
            if (holder instanceof OrderHistoryDateHolder) {
                OrderHistoryDateHolder orderHistoryDateHolder = (OrderHistoryDateHolder) holder;

                if (dateCreated.equals(currentDate)) {
                    orderHistoryDateHolder.tvHistoryDate.setText(context
                            .getString(R.string.text_today));
                } else if (dateCreated
                        .equals(getYesterdayDateString())) {
                    orderHistoryDateHolder.tvHistoryDate.setText(context
                            .getString(R.string.text_yesterday));
                } else {
                    Date date = parseContent.dateFormat.parse(orderHistory
                            .getCreatedAt());
                    String dateString = Utils.getDayOfMonthSuffix(Integer.valueOf
                            (parseContent.day
                                    .format(date))) + " " + parseContent.dateFormatMonth.format
                            (date);
                    orderHistoryDateHolder.tvHistoryDate.setText(dateString);
                }


            } else {
                OrderHistoryHolder orderHistoryHolder = (OrderHistoryHolder) holder;
                Store store = orderHistory.getStoreDetail();
                Glide.with(context).load(PreferenceHelper.getInstance(context).getIMAGE_BASE_URL() + store
                        .getImageUrl())
                        .dontAnimate()
                        .placeholder
                                (ResourcesCompat.getDrawable(context
                                        .getResources(), R.drawable.placeholder, null)).fallback
                        (ResourcesCompat
                                .getDrawable
                                        (context
                                                .getResources(), R.drawable.placeholder, null)).into
                        (orderHistoryHolder.ivHistoryStoreImage);
                if (orderHistory.getOrderStatus() != Const.OrderStatus
                        .FINAL_ORDER_COMPLETED) {
                    orderHistoryHolder.ivCanceled.setVisibility(View.VISIBLE);
                    orderHistoryHolder.ivHistoryStoreImage.setColorFilter(ResourcesCompat
                            .getColor(context
                                    .getResources(), R.color.color_app_transparent_white, null));
                    if (orderHistory.getRefundAmount() > 0) {
                        orderHistoryHolder.tvRefundAmount.setVisibility(View.VISIBLE);
                        orderHistoryHolder.tvRefundAmount.setText(context
                                .getResources().getString(R.string.text_refund) + " " +
                                parseContent.decimalTwoDigitFormat.format(orderHistory
                                        .getRefundAmount()));
                    } else {
                        orderHistoryHolder.tvRefundAmount.setVisibility(View.GONE);
                    }


                } else {
                    orderHistoryHolder.ivCanceled.setVisibility(View.GONE);
                    orderHistoryHolder.tvRefundAmount.setVisibility(View.GONE);
                    orderHistoryHolder.ivHistoryStoreImage.setColorFilter(ResourcesCompat
                            .getColor(context
                                    .getResources(), android.R.color.transparent, null));
                }

                String str_currency;
                if (orderHistory
                        .getCurrency() !=null && !orderHistory
                        .getCurrency().equals(""))
                {
                    if(orderHistory
                            .getCurrency().toUpperCase().contains("NULL")){
                        str_currency=orderHistory
                                .getCurrency().toUpperCase().replace("NULL", "");
                    }else {
                        str_currency=orderHistory
                                .getCurrency();
                    }

                }
                else
                {
str_currency="";
                }
                orderHistoryHolder.tvHistoryOrderPrice.setText(str_currency + parseContent.decimalTwoDigitFormat.format
                        (orderHistoryArrayList
                                .get(position)
                                .getTotal()));
                orderHistoryHolder.tvHistoryStoreName.setText(store.getName());
                String orderNumber = context.getResources().getString(R.string.text_order_number)
                        + " " + "#" + orderHistory.getUniqueId();
                orderHistoryHolder.tvHistoryOrderNumber.setText(orderNumber);
                orderHistoryHolder.tvHistoryOrderTime.setText(parseContent.timeFormat_am.format
                        (parseContent.webFormat.parse(orderHistory.getCreatedAt())).toUpperCase());

                if (orderHistoryArrayList.size() - 1 == position || separatorSet.contains
                        (position + 1)) {
                    orderHistoryHolder.viewDivProductItem.setVisibility(View.GONE);
                } else {
                    orderHistoryHolder.viewDivProductItem.setVisibility(View.VISIBLE);
                }

            }

        } catch (ParseException e) {
            AppLog.handleException(HistoryAdapter.class.getName(), e);
        }
    }

    @Override
    public int getItemCount() {
        return orderHistoryArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return separatorSet.contains(position) ? HEADER : ITEM;
    }

    private String getYesterdayDateString() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return parseContent.dateFormat.format(cal.getTime());
    }

    public abstract void onOrderClick(int position);

    @Override
    public boolean isPinnedViewType(int viewType) {
        return viewType == HEADER;
    }

    protected class OrderHistoryDateHolder extends RecyclerView.ViewHolder {
        CustomFontTextView tvHistoryDate;

        public OrderHistoryDateHolder(View itemView) {
            super(itemView);
            tvHistoryDate = (CustomFontTextView) itemView.findViewById(R.id.tvStoreProductName);
            Utils.setTagBackgroundRtlView(context, tvHistoryDate);
        }
    }

    protected class OrderHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView ivHistoryStoreImage, ivCanceled;
        CustomFontTextView tvHistoryOrderTime, tvHistoryOrderNumber, tvRefundAmount;
        CustomFontTextViewTitle tvHistoryStoreName, tvHistoryOrderPrice;
        View viewDivProductItem;
        LinearLayout llProduct;

        public OrderHistoryHolder(View itemView) {
            super(itemView);
            ivHistoryStoreImage = (ImageView) itemView.findViewById(R.id.ivHistoryStoreImage);
            tvHistoryStoreName = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvHistoryStoreName);
            tvHistoryOrderTime = (CustomFontTextView) itemView.findViewById(R.id
                    .tvHistoryOrderTime);
            tvHistoryOrderPrice = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvHistoryOrderPrice);
            tvHistoryOrderNumber = (CustomFontTextView) itemView.findViewById(R.id
                    .tvHistoryOrderNumber);
            viewDivProductItem = itemView.findViewById(R.id.viewDivProductItem);
            ivCanceled = (ImageView) itemView.findViewById(R.id.ivCanceled);
            tvRefundAmount = (CustomFontTextView) itemView.findViewById(R.id.tvRefundAmount);
            llProduct = (LinearLayout) itemView.findViewById(R.id.llProduct);
            llProduct.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.llProduct:
                    onOrderClick(getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }
}
