package com.edelivery.adapter;

import android.widget.Toast;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.ProductSpecificationActivity;
import com.edelivery.R;
import com.edelivery.component.CustomFontCheckBox;
import com.edelivery.component.CustomFontRadioButton;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.SpecificationSubItem;
import com.edelivery.models.datamodels.Specifications;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.Const;
import com.edelivery.utils.SectionedRecyclerViewAdapter;

import java.util.ArrayList;

/**
 * Created by elluminati on 02-Mar-17.
 */

public class ProductSpecificationItemAdapter extends
        SectionedRecyclerViewAdapter<RecyclerView
                .ViewHolder> {


    private ArrayList<Specifications> specificationsArrayList;
    private ProductSpecificationActivity productSpecificationActivity;
    private ParseContent parseContent;
    public String str_currency;

    public ProductSpecificationItemAdapter(ProductSpecificationActivity
                                                   productSpecificationActivity,
                                           ArrayList<Specifications> specificationsArrayList) {
        this.productSpecificationActivity = productSpecificationActivity;
        this.specificationsArrayList = specificationsArrayList;
        parseContent = ParseContent.getInstance();
    }

    @Override
    public int getSectionCount() {
        return specificationsArrayList.size();
    }

    @Override
    public int getItemCount(int section) {
        return specificationsArrayList.get(section).getList().size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {
        SpecificationHeaderHolder specificationHeaderHolder = (SpecificationHeaderHolder) holder;
        specificationHeaderHolder.tvSpecificationName.setText(specificationsArrayList.get(section)
                .getName());
        Specifications specifications = specificationsArrayList.get(section);
        specificationHeaderHolder.tvRequired.setVisibility(specifications.isRequired() ? View
                .VISIBLE : View.GONE);
      /*  if (0 == section) {
            specificationHeaderHolder.divProductSpecification.setVisibility(View.GONE);
        } else {
            specificationHeaderHolder.divProductSpecification.setVisibility(View.VISIBLE);
        }*/
        specificationHeaderHolder.tvChooseUpTo.setText(specifications.getChooseMessage());


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int section, final int
            relativePosition, final int absolutePosition) {
        final SpecificationItemHolder specificationItemHolder = (SpecificationItemHolder) holder;
        final Specifications specification = specificationsArrayList.get(section);
        final SpecificationSubItem specificationSubItem = specification.getList().get
                (relativePosition);
        if (specificationSubItem
                .getPrice() > 0) {


            if( productSpecificationActivity.productItem.getCurrency()!=null && ! productSpecificationActivity.productItem.getCurrency().equals(""))
            {
                if(productSpecificationActivity.productItem.getCurrency().toUpperCase().contains("NULL")){
                    str_currency=productSpecificationActivity.productItem.getCurrency().toUpperCase().replace("NULL", "");
                }else {
                    str_currency=productSpecificationActivity.productItem.getCurrency();
                }
            }
            else {
                str_currency="";
            }


            final String price =  str_currency+
                    parseContent
                            .decimalTwoDigitFormat.format(specificationSubItem
                            .getPrice());
            specificationItemHolder.tvSpecificationItemPrice.setText(price);
            specificationItemHolder.tvSpecificationItemPrice.setVisibility(View.VISIBLE);
        } else {
            specificationItemHolder.tvSpecificationItemPrice.setVisibility(View.GONE);
        }

        specificationItemHolder.tvSpecificationItemDescription.setText(specificationSubItem
                .getName());


        int itemType = specificationsArrayList.get(section).getType();
        switch (itemType) {
            case Const.TYPE_SPECIFICATION_SINGLE:
                specificationItemHolder.rbSingleSpecification.setVisibility(View.VISIBLE);
                specificationItemHolder.rbMultipleSpecification.setVisibility(View.GONE);
                specificationItemHolder.rbSingleSpecification.setChecked(specificationSubItem
                        .isIsDefaultSelected());
                specificationItemHolder.tvSpecificationItemDescription.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        specificationItemHolder.rbSingleSpecification.performClick();
                    }
                });

                specificationItemHolder.rbSingleSpecification.setOnClickListener(new View
                        .OnClickListener() {

                    @Override
                    public void onClick(View view)
                    {
                        specificationSubItem.setIsDefaultSelected(true);
                        productSpecificationActivity.onSingleItemClick(
                                section, relativePosition, absolutePosition);
                    }
                });

                break;
            case Const.TYPE_SPECIFICATION_MULTIPLE:
                specificationItemHolder.rbSingleSpecification.setVisibility(View.GONE);
                specificationItemHolder.rbMultipleSpecification.setVisibility(View.VISIBLE);
                specificationItemHolder.rbMultipleSpecification.setChecked(specificationSubItem
                        .isIsDefaultSelected());

                specificationItemHolder.tvSpecificationItemDescription.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        specificationItemHolder.rbMultipleSpecification.performClick();
                    }
                });

                specificationItemHolder.rbMultipleSpecification.setOnClickListener(new View
                        .OnClickListener() {

                    @Override
                    public void onClick(View view)
                    {
                        boolean checked = isValidSelection(specification.getRange(), specification
                                        .getMaxRange(), specification.getSelectedCount(),
                                specificationSubItem.isIsDefaultSelected());

                        if (!specificationSubItem.isIsDefaultSelected() && checked) {
                            specification.setSelectedCount(specification.getSelectedCount() +
                                    1);
                        } else if (specificationSubItem.isIsDefaultSelected() && !checked) {
                            specification.setSelectedCount(specification.getSelectedCount() -
                                    1);
                        }
                        specificationSubItem.setIsDefaultSelected(checked);
                        specificationItemHolder
                                .rbMultipleSpecification.setChecked(checked);
                        productSpecificationActivity.modifyTotalItemAmount();

                    }
                });
                break;
            default:
                // do with default
                break;

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                return new SpecificationHeaderHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R
                                .layout.item_specification_header, parent, false));
            case VIEW_TYPE_ITEM:
                return new SpecificationItemHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R
                                .layout.item_specification_item, parent, false));
            default:
                // do with default
                break;
        }
        return null;
    }

    /**
     * this method return flag according to range selection
     *
     * @param range
     * @param maxRange
     * @param selectedCount
     * @param isSelected
     * @return
     */
    private boolean isValidSelection(int range, int maxRange, int selectedCount, boolean
            isSelected) {
        if (range == 0 && maxRange ==
                0) {
            return !isSelected;
        } else if (selectedCount <= range && maxRange == 0) {
            return range != selectedCount && !isSelected;
        } else if (range >= 0 && selectedCount <= maxRange) {
            return maxRange != selectedCount && !isSelected;
        } else {
            return isSelected;
        }
    }

    protected class SpecificationHeaderHolder extends RecyclerView.ViewHolder {

        CustomFontTextViewTitle tvSpecificationName;
        CustomFontTextView tvRequired, tvChooseUpTo;
        View divProductSpecification;

        public SpecificationHeaderHolder(View itemView) {
            super(itemView);
            tvSpecificationName = (CustomFontTextViewTitle) itemView.findViewById(
                    R.id.tvSpecificationName);
            tvRequired = (CustomFontTextView) itemView.findViewById(R.id.tvRequired);
            divProductSpecification = itemView.findViewById(R.id.divProductSpecification);
            tvChooseUpTo = itemView.findViewById(R.id.tvChooseUpTo);

        }
    }

    protected class SpecificationItemHolder extends RecyclerView.ViewHolder {
        CustomFontRadioButton rbSingleSpecification;
        CustomFontCheckBox rbMultipleSpecification;
        CustomFontTextView tvSpecificationItemDescription, tvSpecificationItemPrice;

        public SpecificationItemHolder(View itemView) {
            super(itemView);
            rbSingleSpecification = (CustomFontRadioButton) itemView.findViewById(R.id
                    .rbSingleSpecification);
            rbMultipleSpecification = (CustomFontCheckBox) itemView.findViewById(R.id
                    .rbMultipleSpecification);
            tvSpecificationItemDescription = (CustomFontTextView) itemView.findViewById(R.id
                    .tvSpecificationItemDescription);
            tvSpecificationItemPrice = (CustomFontTextView) itemView.findViewById(R.id
                    .tvSpecificationItemPrice);

        }


    }

}
