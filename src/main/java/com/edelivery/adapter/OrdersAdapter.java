package com.edelivery.adapter;

import android.content.Context;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.fragments.OrderFragment;
import com.edelivery.models.datamodels.Order;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;

import java.util.ArrayList;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

/**
 * Created by elluminati on 12-Apr-17.
 */

public abstract class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.OrderHolder> {

    private ArrayList<Order> orderArrayList;
    private OrderFragment orderFragment;
    private Context context;
    public String  str_currency;
    public OrdersAdapter(OrderFragment orderFragment, ArrayList<Order>
            orderArrayList) {
        this.orderArrayList = orderArrayList;
        this.orderFragment = orderFragment;
        this.context = orderFragment.getContext();
    }

    @Override
    public OrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ordres, parent,
                false);
        return new OrderHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderHolder holder, int position) {
        Order order = orderArrayList.get(position);
        holder.tvOrderStoreName.setText(order.getStoreName());

        if(order.getCurrency()!=null && !order.getCurrency().equals(""))
        {

            if(order.getCurrency().toUpperCase().contains("NULL")){
                str_currency=order.getCurrency().toUpperCase().replace("NULL", "");
            }else {
                str_currency=order.getCurrency();
            }

        }
        else {
            str_currency="";
        }
        holder.tvOrderPricing.setText(str_currency + ParseContent
                .getInstance().decimalTwoDigitFormat.format(order
                        .getTotal()));
        holder.tvOrderStatus.setText(getStringDeliveryStatus(order.getDeliveryStatus() < order
                .getOrderStatus() ? order.getOrderStatus() : order.getDeliveryStatus()));
        if(order.getDeliveryStatus() == Const.OrderStatus.DELIVERY_MAN_NOT_FOUND){
            holder.btnReassign.setVisibility(View.VISIBLE);
        }else{
            holder.btnReassign.setVisibility(View.GONE);
        }
        String orderNumber = context.getResources().getString(R.string.text_order_number)
                + " " + "#" + order.getUniqueId();
        holder.tvOrderNumber.setText(orderNumber);
        Glide.with(context).load(PreferenceHelper.getInstance(context).getIMAGE_BASE_URL() + order.getStoreImage())
                .dontAnimate()
                .placeholder(ResourcesCompat.getDrawable
                        (context
                                .getResources(), R.drawable.placeholder, null)).fallback
                (ResourcesCompat
                        .getDrawable
                                (context.getResources(), R.drawable.placeholder,
                                        null)).into
                (holder.ivOrderStoreImage);
    }

    @Override
    public int getItemCount() {
        return orderArrayList.size();
    }

    abstract public void assignDeliveryman(int position);

    private String getStringDeliveryStatus(int status) {
        String message = "";
        switch (status) {
            case Const.OrderStatus.WAITING_FOR_ACCEPT_STORE:
                message = context.getResources().getString(R.string
                        .msg_wait_for_accept_order_by_store);
                break;
            case Const.OrderStatus.STORE_ORDER_ACCEPTED:
                message = context.getResources().getString(R.string
                        .msg_store_accepted);
                break;
            case Const.OrderStatus.STORE_ORDER_REJECTED:
                message = context.getResources().getString(R.string
                        .msg_store_rejected);
                break;
            case Const.OrderStatus.STORE_ORDER_PREPARING:
                message = context.getResources().getString(R.string
                        .msg_store_prepare_order);
                break;
            case Const.OrderStatus.DELIVERY_MAN_NOT_FOUND:
                message = context.getResources().getString(R.string
                        .msg_deliveryman_not_found);
                break;
            case Const.OrderStatus.DELIVERY_MAN_REJECTED:
                message = context.getResources().getString(R.string
                        .msg_store_rejected);
                break;
            case Const.OrderStatus.DELIVERY_MAN_CANCELLED:
                message = context.getResources().getString(R.string
                        .msg_deliveryman_cancel);
                break;
            case Const.OrderStatus.WAITING_FOR_DELIVERY_MEN:
                message = context.getResources().getString(R.string
                        .msg_waiting_for_deliveryman);
                break;
            case Const.OrderStatus.STORE_CANCELLED_REQUEST:
                message = context.getResources().getString(R.string
                        .msg_store_cancel_requst);
                break;

            case Const.OrderStatus.STORE_ORDER_READY:
                message = context.getResources().getString(R.string
                        .msg_store_ready_order);
                break;
            case Const.OrderStatus.DELIVERY_MAN_ACCEPTED:
                message = context.getResources().getString(R.string
                        .msg_delivery_man_accepted_req);
                break;

            case Const.OrderStatus.DELIVERY_MAN_COMING:
            case Const.OrderStatus.DELIVERY_MAN_ARRIVED:
            case Const.OrderStatus.DELIVERY_MAN_PICKED_ORDER:
                message = context.getResources().getString(R.string
                        .msg_delivery_man_picked_order);
                break;
            case Const.OrderStatus.DELIVERY_MAN_STARTED_DELIVERY:
                message = context.getResources().getString(R.string
                        .msg_delivery_man_started_delivery);
                break;
            case Const.OrderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION:
                message = context.getResources().getString(R.string
                        .msg_delivery_man_arrived_at_destination);
                break;
            case Const.OrderStatus.FINAL_ORDER_COMPLETED:
                message = context.getResources().getString(R.string
                        .msg_delivery_man_complete_delivery);
                break;
            default:
                message = context.getResources().getString(R.string
                        .text_unknown);
                break;
        }
        return message;
    }

    protected class OrderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView ivOrderStoreImage;
        CustomFontTextViewTitle tvOrderStoreName, tvOrderPricing;
        CustomFontTextView tvOrderStatus, tvOrderNumber,btnReassign;

        public OrderHolder(View itemView) {
            super(itemView);
            tvOrderStoreName = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvOrderStoreName);
            tvOrderStatus = (CustomFontTextView) itemView.findViewById(R.id.tvOrderStatus);
            tvOrderPricing = (CustomFontTextViewTitle) itemView.findViewById(R.id.tvOrderPricing);
            ivOrderStoreImage = (ImageView) itemView.findViewById(R.id.ivOrderStoreImage);
            tvOrderNumber = (CustomFontTextView) itemView.findViewById(R.id.tvOrderNumber);
            btnReassign = (CustomFontTextView) itemView.findViewById(R.id.btnReassign);
            btnReassign.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnReassign:
                    assignDeliveryman(getAdapterPosition());
                    break;
            }
        }
    }
}
