package com.edelivery.adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Addresses;

import java.util.ArrayList;

public abstract class FavAddressAdapter extends RecyclerView.Adapter<FavAddressAdapter.CountryViewHolder> {

    private ArrayList<Addresses> AddressList;
    private boolean isDelete;
    private boolean isEdit;


    public FavAddressAdapter(ArrayList<Addresses> AddressList, boolean isDelete, boolean isEdit) {
        this.AddressList = AddressList;
        this.isDelete = isDelete;
        this.isEdit = isEdit;


    }

    @Override
    public FavAddressAdapter.CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fav_address,
                parent, false);
        FavAddressAdapter.CountryViewHolder countryViewHolder = new FavAddressAdapter.CountryViewHolder(view);

        return countryViewHolder;
    }

    @Override
    public void onBindViewHolder(FavAddressAdapter.CountryViewHolder holder, int position) {

        holder.tvCityName.setText(AddressList.get(position).getAddress());
        /* holder.tvStoreProductName.setText(AddressList.get(position).getAddresName());*/
    }

    @Override
    public int getItemCount() {
        return AddressList.size();
    }


    public abstract void onDelete(int position);

    public abstract void onEdit(int position);

    protected class CountryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CustomFontTextView tvCityName, tvStoreProductName;
        LinearLayout ivDeleteFav, ivEdit;

        public CountryViewHolder(View itemView) {
            super(itemView);

            tvCityName = (CustomFontTextView) itemView.findViewById(R.id.tvItemCityName);
            ivEdit = (LinearLayout) itemView.findViewById(R.id.ivEdit);
            /*  tvStoreProductName = (CustomFontTextView) itemView.findViewById(R.id.tvStoreProductName);*/
            ivDeleteFav = itemView.findViewById(R.id.ivDeleteFav);
            if (isDelete) {
                ivDeleteFav.setVisibility(View.VISIBLE);
                ivDeleteFav.setOnClickListener(this);

            } else {
                ivDeleteFav.setVisibility(View.GONE);
            }

            if (isEdit) {
                ivEdit.setVisibility(View.VISIBLE);
                ivEdit.setOnClickListener(this);
            } else {
                ivEdit.setVisibility(View.GONE);
            }


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivDeleteFav:
                    onDelete(getAdapterPosition());
                    break;
                case R.id.ivEdit:
                    onEdit(getAdapterPosition());
                    break;
            }
        }
    }

}