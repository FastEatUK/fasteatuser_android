package com.edelivery.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.fragments.ReviewFragment;
import com.edelivery.models.datamodels.RemainingReview;

import java.util.List;

/**
 * Created by elluminati on 21-Nov-17.
 */

public abstract class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ReviewHolder> {
    private List<RemainingReview> storeReview;
    private ReviewFragment reviewFragment;

    public ReviewAdapter(List<RemainingReview> storeReview, ReviewFragment reviewFragment) {
        this.storeReview = storeReview;
        this.reviewFragment = reviewFragment;
    }

    @Override
    public ReviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review_1,
                parent, false);
        return new ReviewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ReviewHolder holder, final int position) {

        holder.tvOrderId.setText(reviewFragment.getResources().getString(R.string.text_order_number)
                + " #" + storeReview.get
                (position)
                .getOrderUniqueId());
        holder.btnWriteReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelect(position, holder.ratingBarReview.getRating());
            }
        });

    }

    @Override
    public int getItemCount() {
        return storeReview.size();
    }

    public abstract void onSelect(int position, float rating);

    protected class ReviewHolder extends RecyclerView.ViewHolder {
        CustomFontTextView btnWriteReview, tvOrderId;
        RatingBar ratingBarReview;

        public ReviewHolder(View itemView) {
            super(itemView);
            btnWriteReview = (CustomFontTextView) itemView.findViewById(R.id.btnWriteReview);
            ratingBarReview = (RatingBar) itemView.findViewById(R.id.ratingBarReview);
            tvOrderId = (CustomFontTextView) itemView.findViewById(R.id.tvOrderId);
        }
    }
}
