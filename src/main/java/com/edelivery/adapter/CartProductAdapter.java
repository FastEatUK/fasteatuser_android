package com.edelivery.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.CartActivity;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.CartProductItems;
import com.edelivery.models.datamodels.CartProducts;
import com.edelivery.models.datamodels.ProductDetail;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.SectionedRecyclerViewAdapter;

import java.util.ArrayList;

/**
 * Created by elluminati on 16-Feb-17.
 */

public class CartProductAdapter extends SectionedRecyclerViewAdapter<RecyclerView
        .ViewHolder> {
    private ArrayList<CartProducts> cartProductList;
    private CartActivity cartActivity;
    private ParseContent parseContent;


    public CartProductAdapter(CartActivity cartActivity, ArrayList<CartProducts> cartProductList) {
        this.cartActivity = cartActivity;
        this.cartProductList = cartProductList;
        parseContent = ParseContent.getInstance();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_HEADER:
                return new CartHeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R
                                .layout
                                .layout_divider_horizontal,
                        parent, false));
            case VIEW_TYPE_ITEM:
                return new CartHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                                .item_cart_product_item,
                        parent, false));
            default:
                // do with default
                break;
        }

        return null;
    }

    @Override
    public int getSectionCount() {
        return cartProductList.size();
    }

    @Override
    public int getItemCount(int section) {
        return cartProductList.get(section).getItems().size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {
        CartHeaderHolder cartHeaderHolder = (CartHeaderHolder) holder;
        cartHeaderHolder.itemView.setVisibility(View.GONE);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int section, final int
            relativePosition, int absolutePosition) {

        CartHolder cartHolder = (CartHolder) holder;
        CartProducts cartProducts = cartProductList.get(section);
        CartProductItems cartProductItems = cartProducts.getItems().get(relativePosition);
        cartHolder.tvCartProductName.setText(cartProductItems.getItemName());
        cartHolder.tvCartProductDescription.setText(cartProductItems.getDetails());

        String str_currency;
        if(CurrentBooking.getInstance().getCartCurrency()!=null && !CurrentBooking.getInstance().getCartCurrency().equals(""))
        {
            if(CurrentBooking.getInstance().getCartCurrency().toUpperCase().contains("NULL")){
                str_currency=CurrentBooking.getInstance().getCartCurrency().toUpperCase().replace("NULL", "");
            }else {
                str_currency=CurrentBooking.getInstance().getCartCurrency();
            }
        }
        else
        {
            str_currency="";
        }
        final String price = str_currency+ parseContent
                .decimalTwoDigitFormat.format(cartProductItems
                        .getTotalItemAndSpecificationPrice());

        cartHolder.tvCartProductPricing.setText(price);
        cartHolder.tvItemQuantity.setText(String.valueOf(cartProductItems.getQuantity()));


        cartHolder.btnIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartActivity.increaseItemQuantity(cartProductList.get(section)
                        .getItems().get(relativePosition));
            }
        });

        cartHolder.btnDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartActivity.decreaseItemQuantity(cartProductList.get(section)
                        .getItems().get(relativePosition));

            }
        });
        cartHolder.tvRemoveCartItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartActivity.removeItem(section
                        , relativePosition);
            }
        });
        cartHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProductDetail productDetail = new ProductDetail();
                productDetail.setName(cartProductList.get(section).getProductName());
                productDetail.setUniqueId(cartProductList.get(section).getUniqueId());
                cartActivity.goToProductSpecificationActivity(section, relativePosition,
                        cartProductList.get(section)
                                .getItems().get(relativePosition), productDetail);
            }
        });
    }


    protected class CartHolder extends RecyclerView.ViewHolder {
        CustomFontTextViewTitle tvCartProductName, tvCartProductPricing;
        CustomFontTextView tvCartProductDescription,
                btnDecrease, tvItemQuantity, btnIncrease, tvRemoveCartItem;

        public CartHolder(View itemView) {
            super(itemView);
            tvCartProductName = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvCartProductName);
            tvCartProductDescription = (CustomFontTextView) itemView.findViewById(R.id
                    .tvCartProductDescription);
            tvCartProductPricing = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvCartProductPricing);
            btnDecrease = (CustomFontTextView) itemView.findViewById(R.id.btnDecrease);
            tvItemQuantity = (CustomFontTextView) itemView.findViewById(R.id.tvItemQuantity);
            btnIncrease = (CustomFontTextView) itemView.findViewById(R.id.btnIncrease);
            tvRemoveCartItem = (CustomFontTextView) itemView.findViewById(R.id.tvRemoveCartItem);


        }

    }

    protected class CartHeaderHolder extends RecyclerView.ViewHolder {
        CustomFontTextView tvStoreProductName;

        public CartHeaderHolder(View itemView) {
            super(itemView);

        }
    }
}
