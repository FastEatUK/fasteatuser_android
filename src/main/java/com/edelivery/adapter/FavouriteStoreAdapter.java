package com.edelivery.adapter;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.edelivery.FavouriteStoreActivity;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.Store;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.PreferenceHelper;

import java.util.List;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

/**
 * Created by elluminati on 11-Nov-17.
 */

public class FavouriteStoreAdapter extends RecyclerView.Adapter<FavouriteStoreAdapter
        .FavouriteViewHolder> {

    private List<Store> storeDetail;
    private FavouriteStoreActivity activity;

    public FavouriteStoreAdapter(FavouriteStoreActivity activity, List<Store> storeDetail) {
        this.storeDetail = storeDetail;
        this.activity = activity;
    }

    @Override
    public FavouriteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .item_favourite_store, parent, false);

        return new FavouriteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FavouriteViewHolder holder, int position) {

        final Store store = storeDetail.get(position);
        store.setFavourite(true);
        FavouriteViewHolder storeHolder = (FavouriteViewHolder) holder;
        if (PreferenceHelper.getInstance(activity).getIsLoadStoreImage()) {
            AppLog.Log("STORE_IMG", store.getImageUrl());
            Glide.with(activity
            ).load(PreferenceHelper.getInstance(activity.getApplicationContext()).getIMAGE_BASE_URL() + store.getImageUrl()).dontAnimate()
                    .placeholder
                            (ResourcesCompat.getDrawable(activity

                                    .getResources(), R.drawable.placeholder, null)).fallback
                    (ResourcesCompat
                            .getDrawable
                                    (activity.getResources(), R.drawable.placeholder, null)).into
                    (storeHolder.ivStoreImage);
        }
        storeHolder.tvStoreName.setText(store.getName());
        storeHolder.tvStorePricing.setText(store.getPriceRattingAndTag());
        storeHolder.checkboxFavourite.setChecked(store.isFavouriteRemove());

        if (store.getDeliveryTimeMax() > store.getDeliveryTime()) {
            storeHolder.tvStoreApproxTime.setText(String.valueOf(store.getDeliveryTime()) +
                    " - " + String.valueOf(store.getDeliveryTimeMax()) + " " +
                    activity.getResources().getString(R.string.unit_mins));
        } else {
            storeHolder.tvStoreApproxTime.setText(String.valueOf(store.getDeliveryTime()) +
                    " " +
                    activity.getResources().getString(R.string.unit_mins));
        }
        if(store.getNote()!=null) {
            storeHolder.tvStoreRatings.setVisibility(View.VISIBLE);
            storeHolder.tvStoreRatings.setText(String.valueOf(store.getNote()));
        }else {
            storeHolder.tvStoreRatings.setVisibility(View.INVISIBLE);
        }

        //  storeHolder.tvRatingsCount.setText("(" + String.valueOf(store.getRateCount()) + ")");
        if (store.isStoreClosed()) {
            storeHolder.llStoreClosed.setVisibility(View.VISIBLE);
            storeHolder.tvTag.setText(activity.getResources().getText(R.string
                    .text_store_closed));
            storeHolder.tvStoreReOpenTime.setVisibility(View.VISIBLE);
            storeHolder.tvStoreReOpenTime.setText(store.getReOpenTime());
        } else {
            if (store.isBusy()) {
                storeHolder.llStoreClosed.setVisibility(View.VISIBLE);
                storeHolder.tvTag.setText(activity.getResources().getText(R.string
                        .text_store_busy));
                storeHolder.tvStoreReOpenTime.setVisibility(View.GONE);
            } else {
                storeHolder.llStoreClosed.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public int getItemCount() {
        return storeDetail.size();
    }

    protected class FavouriteViewHolder extends RecyclerView.ViewHolder {
        ImageView ivStoreImage;
        CheckBox checkboxFavourite;
        LinearLayout llStoreClosed, llStoreCard;
        CustomFontTextViewTitle tvStoreName;
        CustomFontTextView tvStorePricing, tvStoreRatings, tvStoreReOpenTime,
                tvStoreApproxTime, tvRatingsCount, tvTag;

        public FavouriteViewHolder(View itemView) {
            super(itemView);
            llStoreClosed = (LinearLayout) itemView.findViewById(R.id.llStoreClosed);
            llStoreCard = (LinearLayout) itemView.findViewById(R.id.llStoreCard);
            ivStoreImage = (ImageView) itemView.findViewById(R.id.ivStoreImage);
            tvStoreName = (CustomFontTextViewTitle) itemView.findViewById(R.id.tvStoreName);
            tvStorePricing = (CustomFontTextView) itemView.findViewById(R.id.tvStorePricing);
            tvStoreRatings = (CustomFontTextView) itemView.findViewById(R.id.tvStoreRatings);
            tvStoreReOpenTime = (CustomFontTextView) itemView.findViewById(R.id.tvStoreReOpenTime);
            tvStoreApproxTime = (CustomFontTextView) itemView.findViewById(R.id.tvStoreApproxTime);
            checkboxFavourite = (CheckBox) itemView.findViewById(R.id.checkboxFavourite);
            checkboxFavourite.setOnCheckedChangeListener(new CompoundButton
                    .OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    storeDetail.get(getAdapterPosition()).setFavouriteRemove(isChecked);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.gotToStoreProductActivity(storeDetail.get(getAdapterPosition()));
                }
            });
            itemView.findViewById(R.id.ivFavourites).setVisibility(View.GONE);
            checkboxFavourite.setVisibility(View.VISIBLE);
            tvRatingsCount = (CustomFontTextView) itemView.findViewById(R.id.tvRatingsCount);
            tvTag = (CustomFontTextView) itemView.findViewById(R.id.tvTag);
        }
    }
}
