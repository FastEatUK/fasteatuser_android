package com.edelivery.adapter;

import androidx.core.content.res.ResourcesCompat;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.edelivery.ProductSpecificationActivity;
import com.edelivery.R;
import com.edelivery.utils.PreferenceHelper;

import java.util.List;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

/**
 * Created by elluminati on 18-Apr-17.
 */

public class ProductItemItemAdapter extends PagerAdapter {

    private List<String> stringList;
    private ProductSpecificationActivity activity;
    private int id;
    private boolean isItemClick;

    public ProductItemItemAdapter(ProductSpecificationActivity activity, List<String>
            stringList, int layoutId, boolean isItemClick) {
        this.stringList = stringList;
        this.activity = activity;
        this.id = layoutId;
        this.isItemClick = isItemClick;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(id, container, false);
        if (PreferenceHelper.getInstance(activity).getIsLoadProductImage()) {
            Glide.with(activity).load(PreferenceHelper.getInstance(activity).getIMAGE_BASE_URL() + stringList.get(position))
                    .dontAnimate()
                    .placeholder(ResourcesCompat.getDrawable
                            (activity
                                    .getResources(), R.drawable.placeholder, null)).fallback
                    (ResourcesCompat
                            .getDrawable
                                    (activity.getResources(), R.drawable.placeholder,
                                            null)).into
                    ((ImageView) view);
        }
        if (isItemClick) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (PreferenceHelper.getInstance(activity).getIsLoadProductImage()) {
                        activity.openDialogItemImage(position);
                    }
                }
            });
        }

        container.addView(view);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }

    @Override
    public int getCount() {
        return PreferenceHelper.getInstance(activity).getIsLoadProductImage() ? stringList.size()
                : 1;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


}
