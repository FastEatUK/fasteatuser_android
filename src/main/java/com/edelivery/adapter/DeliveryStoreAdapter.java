package com.edelivery.adapter;

import android.os.Handler;
import android.os.Message;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.fragments.HomeFragment;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.Ads;
import com.edelivery.models.datamodels.Deliveries;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

/**
 * Created by elluminati on 09-Feb-17.
 */

public class DeliveryStoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public static final String TAG = DeliveryStoreAdapter.class.getName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_HEADER = 2;
    public int header = 1;
    private Handler handler;
    private HomeFragment homeFragment;
    private ArrayList<Deliveries> deliveryStoreList;
    private List<Ads> ads;
    private boolean isScheduleStart;
    private ScheduledExecutorService tripStatusSchedule;
    private RecyclerView recyclerView;
    private int count = 0;
    private TextView[] dots;
    private AdsAdapter adsAdapter;
    private CustomFontTextView tvAdds;

    public DeliveryStoreAdapter(HomeFragment homeFragment, ArrayList<Deliveries>
            deliveryStoreList, List<Ads> ads) {
        this.homeFragment = homeFragment;
        this.deliveryStoreList = deliveryStoreList;
        this.ads = ads;
        initHandler();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                            .item_ads_contain, parent,
                    false);
            return new StoreViewHeader(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                            .item_delivery_store_list,
                    parent,
                    false);
            return new StoreViewHolder(view);
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof StoreViewHeader) {
            StoreViewHeader storeViewHeader = (StoreViewHeader) holder;
            addBottomDots(storeViewHeader.llDots, position);
            tvAdds = storeViewHeader.tvAdds;
            if (ads.isEmpty()) {
                storeViewHeader.rcvAds.setVisibility(View.GONE);
                storeViewHeader.llDots.setVisibility(View.GONE);
                storeViewHeader.tvAdds.setVisibility(View.GONE);
                storeViewHeader.darkView.setVisibility(View.GONE);
                storeViewHeader.rcvAds.getLayoutParams().height = 0;
            } else {
                adsAdapter = new AdsAdapter(homeFragment.getContext(),
                        ads,
                        true);
                storeViewHeader.rcvAds.setAdapter(adsAdapter);
                storeViewHeader.rcvAds.getLayoutParams().height = WindowManager.LayoutParams
                        .WRAP_CONTENT;
                storeViewHeader.rcvAds.setVisibility(View.VISIBLE);
                storeViewHeader.llDots.setVisibility(View.VISIBLE);
                storeViewHeader.tvAdds.setVisibility(View.VISIBLE);
                storeViewHeader.darkView.setVisibility(View.VISIBLE);
                recyclerView = storeViewHeader.rcvAds;
                if (!ads.isEmpty()) {
                    startAdsScheduled();
                }
                if (ads != null && ads.size() > 0 && ads.get(0) != null && ads.get(0).getAdsDetail() != null && !TextUtils.isEmpty(ads.get(0).getAdsDetail())) {
                    tvAdds.setText(ads.get(0).getAdsDetail());
                } else {
                    tvAdds.setText("");
                }
            }

        } else {
            position = position - header;
            StoreViewHolder storeViewHolder = (StoreViewHolder) holder;
            if (deliveryStoreList != null && deliveryStoreList.get(position) != null) {
                Deliveries deliveries = deliveryStoreList.get(position);
                if (deliveries.getIconUrl() != null) {
                    Glide.with(homeFragment).load(PreferenceHelper.getInstance(homeFragment.getContext()).getIMAGE_BASE_URL() + deliveries.getIconUrl())
                            .transition(new DrawableTransitionOptions().crossFade()).dontAnimate().placeholder(ResourcesCompat.getDrawable(homeFragment
                            .getResources(), R.drawable.placeholder, null)).fallback(ResourcesCompat
                            .getDrawable(homeFragment
                                    .getResources(), R.drawable.placeholder, null)).into
                            (storeViewHolder.ivDeliverySoreIcon);
                }

                if (deliveries.getImageUrl() != null) {
                    Glide.with(homeFragment).load(PreferenceHelper.getInstance(homeFragment.getContext()).getIMAGE_BASE_URL() + deliveries.getImageUrl())
                            .transition(new DrawableTransitionOptions().crossFade()).dontAnimate().placeholder(ResourcesCompat.getDrawable(homeFragment
                            .getResources(), R.drawable.placeholder, null)).fallback(ResourcesCompat
                            .getDrawable(homeFragment
                                    .getResources(), R.drawable.placeholder, null)).into
                            (storeViewHolder.ivDeliverySoreImage);
                }

                if (deliveries.getDeliveryName() != null && !TextUtils.isEmpty(deliveries.getDeliveryName())) {
                    storeViewHolder.tvDeliveryName.setText(deliveries
                            .getDeliveryName());
                } else {
                    storeViewHolder.tvDeliveryName.setText("");
                }
            }
        }


    }

    @Override
    public int getItemCount() {
        return deliveryStoreList.size() + header;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return VIEW_HEADER;
        } else {
            return VIEW_ITEM;
        }
    }

    public void startAdsScheduled() {
        if (!isScheduleStart) {
            tripStatusSchedule = Executors.newSingleThreadScheduledExecutor();
            tripStatusSchedule.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    Message message = handler.obtainMessage();
                    handler.sendMessage(message);
                }
            }, 0, Const.ADS_SCHEDULED_SECONDS, TimeUnit
                    .SECONDS);
            AppLog.Log(TAG, "Schedule Start");
            isScheduleStart = true;
        }
    }

    private void initHandler() {

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                if (count == ads.size() - 1) {
                    count = 0;
                } else {
                    count++;
                }
                recyclerView.smoothScrollToPosition(count);
            }
        };
    }

    public void stopAdsScheduled() {
        if (isScheduleStart) {
            AppLog.Log(TAG, "Schedule Stop");
            tripStatusSchedule.shutdown(); // Disable new tasks from being submitted
            // Wait a while for existing tasks to terminate
            try {
                if (!tripStatusSchedule.awaitTermination(60, TimeUnit.SECONDS)) {
                    tripStatusSchedule.shutdownNow(); // Cancel currently executing tasks
                    // Wait a while for tasks to respond to being cancelled
                    if (!tripStatusSchedule.awaitTermination(60, TimeUnit.SECONDS))
                        AppLog.Log(TAG, "Pool did not terminate");

                }
            } catch (InterruptedException e) {
                AppLog.handleException(TAG, e);
                // (Re-)Cancel if current thread also interrupted
                tripStatusSchedule.shutdownNow();
                // Preserve interrupt ProviderStatus
                Thread.currentThread().interrupt();
            }
            isScheduleStart = false;
        }
    }

    private void addBottomDots(LinearLayout llDots, int currentPage) {
        if (ads.size() > 0) {
            dots = new TextView[ads.size()];

            llDots.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(homeFragment.getContext());
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(35);
                dots[i].setTextColor(ResourcesCompat.getColor(homeFragment.getResources(), R
                                .color
                                .color_app_gray_trans
                        , null));
                llDots.addView(dots[i]);
            }

            if (dots.length > 0)
                dots[currentPage].setTextColor(ResourcesCompat.getColor(homeFragment
                                .getResources(), R.color
                                .color_white
                        , null));
        }


    }

    private void dotColorChange(LinearLayout llDots, int currentPage) {
        if (llDots.getChildCount() > 0) {
            for (int i = 0; i < llDots.getChildCount(); i++) {
                TextView textView = (TextView) llDots.getChildAt(i);
                textView.setTextColor(ResourcesCompat.getColor(homeFragment.getResources(), R.color
                                .color_app_gray_trans
                        , null));

            }
            TextView textView = (TextView) llDots.getChildAt(currentPage);
            if (textView != null) {
                textView.setTextColor(ResourcesCompat.getColor(homeFragment.getResources(), R.color
                                .color_white
                        , null));
            }


        }


    }

    protected class StoreViewHolder extends RecyclerView.ViewHolder {
        ImageView ivDeliverySoreImage, ivDeliverySoreIcon;
        CustomFontTextView tvDeliveryName;

        public StoreViewHolder(View itemView) {
            super(itemView);
            ivDeliverySoreImage = (ImageView) itemView.findViewById(R.id.ivDeliverySoreImage);
            ivDeliverySoreIcon = (ImageView) itemView.findViewById(R.id.ivDeliverySoreIcon);
            tvDeliveryName = (CustomFontTextView) itemView.findViewById(R.id.tvDeliveryName);

        }
    }

    protected class StoreViewHeader extends RecyclerView.ViewHolder {

        RecyclerView rcvAds;
        LinearLayout llDots;
        CustomFontTextView tvAdds;
        View darkView;

        public StoreViewHeader(View itemView) {
            super(itemView);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(homeFragment
                    .getContext(),
                    LinearLayoutManager
                            .HORIZONTAL, false);
            rcvAds = (RecyclerView) itemView.findViewById(R.id.rcvAds);
            rcvAds.setLayoutManager(layoutManager);
            rcvAds.addOnItemTouchListener(new RecyclerTouchListener(homeFragment.getContext(),
                    rcvAds, new
                    ClickListener() {


                        @Override
                        public void onClick(View view, int position) {
                            if (!TextUtils.isEmpty(ads.get(position)
                                    .getStoreId())) {
                                homeFragment.goToStoreProductActivity(ads.get(position)
                                        .getStoreId());
                            }


                        }

                        @Override
                        public void onLongClick(View view, int position) {

                        }
                    }));
            final SnapHelper snapHelper = new LinearSnapHelper();
            snapHelper.attachToRecyclerView(rcvAds);
            llDots = (LinearLayout) itemView.findViewById(R.id.llDots);
            tvAdds = (CustomFontTextView) itemView.findViewById(R.id.tvAdds);
            darkView = itemView.findViewById(R.id.darkView);
            rcvAds.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    dotColorChange(llDots, layoutManager.findLastCompletelyVisibleItemPosition());
                    if (tvAdds != null && !ads.isEmpty() && layoutManager
                            .findLastCompletelyVisibleItemPosition() >= 0) {
                        tvAdds.setText(ads.get(layoutManager
                                .findLastCompletelyVisibleItemPosition()).getAdsDetail());
                    }

                }
            });
        }

    }
}
