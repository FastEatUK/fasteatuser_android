package com.edelivery.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.edelivery.R;
import com.edelivery.StoreProductActivity;
import com.edelivery.component.CustomFontCheckBox;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Product;

import java.util.ArrayList;

/**
 * Created by elluminati on 02-Oct-17.
 */

public class ProductFilterAdapter extends RecyclerView.Adapter<ProductFilterAdapter
        .ProductFilterViewHolder> {
    private ArrayList<Product>
            storeProductList;
    private StoreProductActivity storeProductActivity;

    public ProductFilterAdapter(StoreProductActivity storeProductActivity, ArrayList<Product>
            storeProductList) {
        this.storeProductList = storeProductList;
        this.storeProductActivity = storeProductActivity;
    }

    @Override
    public ProductFilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ProductFilterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R
                .layout.item_product_filter, parent, false));
    }

    @Override
    public void onBindViewHolder(ProductFilterViewHolder holder, int position) {
        holder.tvProductNameFilter.setText(storeProductList.get(position).getProductDetail()
                .getName());
        holder.rbSelectProductFilter.setChecked(storeProductList.get(position).isProductFiltered());

    }

    @Override
    public int getItemCount() {
        return storeProductList.size();
    }

    protected class ProductFilterViewHolder extends RecyclerView.ViewHolder {
        private CustomFontTextView tvProductNameFilter;
        private CustomFontCheckBox rbSelectProductFilter;

        public ProductFilterViewHolder(View itemView) {
            super(itemView);
            tvProductNameFilter = (CustomFontTextView) itemView.findViewById(R.id
                    .tvProductNameFilter);
            rbSelectProductFilter = (CustomFontCheckBox) itemView.findViewById(R.id
                    .rbSelectProductFilter);
            rbSelectProductFilter.setOnCheckedChangeListener(new CompoundButton
                    .OnCheckedChangeListener() {


                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    storeProductList.get(getAdapterPosition()).setProductFiltered(isChecked);
                }
            });
        }
    }
}
