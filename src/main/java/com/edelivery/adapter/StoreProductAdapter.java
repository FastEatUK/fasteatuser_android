package com.edelivery.adapter;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.edelivery.R;
import com.edelivery.StoreProductActivity;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.Product;
import com.edelivery.models.datamodels.ProductItem;
import com.edelivery.models.datamodels.SpecificationSubItem;
import com.edelivery.models.datamodels.Specifications;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.PinnedHeaderItemDecoration;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.SectionedRecyclerViewAdapter;
import com.edelivery.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

/**
 * Created by elluminati on 16-Feb-17.
 */

public class StoreProductAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder>
        implements Filterable, PinnedHeaderItemDecoration.PinnedHeaderAdapter {
    private ArrayList<Product> filterList;
    private ArrayList<Product> storeProductList;
    private StoreProductActivity storeProductActivity;
    private ParseContent parseContent;
    private StoreItemFilter filter;
    private List<ProductItem> itemsItemFilter;

    public StoreProductAdapter(StoreProductActivity storeProductActivity, ArrayList<Product>
            storeProductList) {
        this.storeProductList = storeProductList;
        this.storeProductActivity = storeProductActivity;
        parseContent = ParseContent.getInstance();
        itemsItemFilter = new ArrayList<>();
        filterList = new ArrayList<>();
    }

    @Override
    public int getSectionCount() {
        return storeProductList.size();
    }

    @Override
    public int getItemCount(int section) {
        return storeProductList.get(section).getItems().size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, final int section) {
        StoreProductHeaderHolder storeProductHeaderHolder = (StoreProductHeaderHolder) holder;

        Collections.sort(storeProductList, new Comparator<Product>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(Product product, Product t1) {
                return Integer.compare(product.getProductDetail().getSequenceNumber(),t1.getProductDetail().getSequenceNumber());
            }
        });

        storeProductHeaderHolder.tvStoreProductName.setText(storeProductList.get(section)
                .getProductDetail().getName());
        if(storeProductList.get(section).getProductDetail().getNote()!=null && !storeProductList.get(section).getProductDetail().getNote().isEmpty()){
            storeProductHeaderHolder.tvProductNote.setVisibility(View.VISIBLE);
            storeProductHeaderHolder.tvProductNote.setText(storeProductList.get(section).getProductDetail().getNote());
        }else{
            storeProductHeaderHolder.tvProductNote.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int section, int
            relativePosition, int absolutePosition) {
        final StoreProductItemHolder storeProductItemHolder = (StoreProductItemHolder) holder;
        List<ProductItem> productItemList = storeProductList.get(section).getItems();

        Collections.sort(productItemList, new Comparator<ProductItem>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(ProductItem productItem, ProductItem t1) {
                return Integer.compare(productItem.getSequesnceNumber(),t1.getSequesnceNumber());
            }
        });


        final ProductItem productsItem = productItemList.get(relativePosition);
        AppLog.Log("productsItem_IMG", String.valueOf(productsItem.getImageUrl()));
        productsItem.setCurrency(storeProductActivity.store.getCurrency());

        if (productsItem.getImageUrl() != null && !productsItem.getImageUrl().isEmpty() &&
                PreferenceHelper.getInstance(storeProductActivity).getIsLoadProductImage()) {
            Glide.with(storeProductActivity).load(PreferenceHelper.getInstance(storeProductActivity.getApplicationContext()).getIMAGE_BASE_URL() + productsItem.getImageUrl().get
                    (0))
                    .dontAnimate().listener(new RequestListener<Drawable>() {


                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    storeProductItemHolder.ivProductImage.setVisibility(View.GONE);
                    return false;                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    storeProductItemHolder.ivProductImage.setVisibility(View.VISIBLE);
                    return false;                }


            }).fallback(AppCompatResources.getDrawable(storeProductActivity, R.drawable
                    .placeholder)).placeholder(AppCompatResources.getDrawable
                    (storeProductActivity, R.drawable.placeholder)).into
                    (storeProductItemHolder.ivProductImage);
        } else {
            storeProductItemHolder.ivProductImage.setImageDrawable(AppCompatResources.getDrawable
                    (storeProductActivity, R.drawable.placeholder));
        }
        storeProductItemHolder.tvProductName.setText(productsItem.getName());
        storeProductItemHolder.tvProductDescription.setText(String.valueOf(productsItem
                .getDetails()));
        if (productsItem.getPrice() > 0) {

            String str_currency;
            if(storeProductActivity.store.getCurrency()!=null && !storeProductActivity.store.getCurrency().equals(""))
            {
                if(storeProductActivity.store.getCurrency().toUpperCase().contains("NULL")){
                    str_currency=storeProductActivity.store.getCurrency().toUpperCase().replace("NULL", "");
                }else {
                    str_currency=storeProductActivity.store.getCurrency();
                }


            }
            else {
                str_currency="";
            }

            String price = str_currency+ parseContent
                    .decimalTwoDigitFormat.format(productsItem
                            .getPrice());
            storeProductItemHolder.tvProductPricing.setText(price);
            storeProductItemHolder.tvProductPricing.setVisibility(View.VISIBLE);
        } else {
            double price = 0;
            for (Specifications specifications : productsItem.getSpecifications()) {
                for (SpecificationSubItem listItem : specifications.getList()) {
                    if (listItem.isIsDefaultSelected()) {
                        price = price + listItem.getPrice();
                    }
                }
            }
            storeProductItemHolder.tvProductPricing.setText(storeProductActivity.store
                    .getCurrency() + parseContent
                    .decimalTwoDigitFormat.format(price));
            storeProductItemHolder.tvProductPricing.setVisibility(View.VISIBLE);
        }
        if (productItemList.size() - 1 == relativePosition) {
            storeProductItemHolder.viewDivProductItem.setVisibility(View.GONE);
        } else {
            storeProductItemHolder.viewDivProductItem.setVisibility(View.VISIBLE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (storeProductActivity.currentBooking.isApplication()) {
                    storeProductActivity.currentBooking.setApplication(false);
                    storeProductActivity.goToProductSpecificationActivity(storeProductList.get
                                    (section).getProductDetail(),
                            productsItem, holder.itemView, false);
                }
            }
        });

        if (productsItem.getItemPriceWithoutOffer() > 0) {





            String str_currencypricewithoffer;
            if(storeProductActivity.store.getCurrency()!=null && !storeProductActivity.store.getCurrency().equals(""))
            {
                str_currencypricewithoffer=storeProductActivity.store.getCurrency();
            }
            else {
                str_currencypricewithoffer="";
            }
            String priceWithOutOffer = str_currencypricewithoffer+ parseContent
                    .decimalTwoDigitFormat.format(productsItem
                            .getItemPriceWithoutOffer());
            storeProductItemHolder.tvProductPricingWithoutOffer.setText(priceWithOutOffer);
            storeProductItemHolder.tvProductPricingWithoutOffer.setVisibility(View.VISIBLE);
        } else {
            storeProductItemHolder.tvProductPricingWithoutOffer.setVisibility(View.GONE);
        }
        storeProductItemHolder.tvProductOffer.setVisibility(TextUtils.isEmpty(productsItem
                .getOfferMessageOrPercentage()) ? View.GONE : View.VISIBLE);
        storeProductItemHolder.tvProductOffer.setText(productsItem.getOfferMessageOrPercentage());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                return new StoreProductHeaderHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout
                                .item_store_product_header, parent, false));
            case VIEW_TYPE_ITEM:
                return new StoreProductItemHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_store_product_item, parent, false));
            default:
                // do somethings
                break;

        }
        return null;
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new StoreItemFilter(storeProductList);
        return filter;
    }

    @Override
    public boolean isPinnedViewType(int viewType) {
        return viewType == VIEW_TYPE_HEADER;
    }

    protected class StoreProductHeaderHolder extends RecyclerView.ViewHolder {

        CustomFontTextView tvStoreProductName,tvProductNote;

        public StoreProductHeaderHolder(View itemView) {
            super(itemView);
            tvStoreProductName = (CustomFontTextView) itemView.findViewById(R.id
                    .tvStoreProductName);
            tvProductNote = (CustomFontTextView) itemView.findViewById(R.id.tvProductNote);
            Utils.setTagBackgroundRtlView(storeProductActivity, tvStoreProductName);
        }
    }

    protected class StoreProductItemHolder extends RecyclerView.ViewHolder {
        ImageView ivProductImage;
        View viewDivProductItem;
        CustomFontTextViewTitle tvProductName, tvProductPricing, tvProductPricingWithoutOffer;
        CustomFontTextView tvProductDescription, tvProductOffer;

        public StoreProductItemHolder(View itemView) {
            super(itemView);
            ivProductImage = (ImageView) itemView.findViewById(R.id.ivProductImage);
            tvProductName = (CustomFontTextViewTitle) itemView.findViewById(R.id.tvProductName);
            tvProductDescription = (CustomFontTextView) itemView.findViewById(R.id
                    .tvProductDescription);
            tvProductPricing = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvProductPricing);
            tvProductPricingWithoutOffer = (CustomFontTextViewTitle) itemView.findViewById(R.id
                    .tvProductPricingWithoutOffer);
            tvProductPricingWithoutOffer.setPaintFlags(tvProductPricingWithoutOffer.getPaintFlags
                    () | Paint.STRIKE_THRU_TEXT_FLAG);

            viewDivProductItem = itemView.findViewById(R.id.viewDivProductItem);
            tvProductOffer = (CustomFontTextView) itemView.findViewById(R.id.tvProductOffer);
        }


    }

    private class StoreItemFilter extends Filter {
        private ArrayList<Product> sourceList;

        StoreItemFilter(ArrayList<Product> storesItemArrayList) {
            sourceList = new ArrayList<>();
            synchronized (this) {
                sourceList.addAll(storesItemArrayList);
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterSeq = constraint.toString();
            FilterResults result = new FilterResults();
            filterList.clear();
            for (int i = 0; i < sourceList.size(); i++) {
                itemsItemFilter.clear();
                if (sourceList.get(i).isProductFiltered()) {
                    if (TextUtils.isEmpty(constraint)) {
                        itemsItemFilter.addAll(sourceList.get(i).getItems());
                    } else {
                        for (ProductItem itemsItem : sourceList.get(i).getItems()) {
                            if (itemsItem.getName().toUpperCase().contains(filterSeq.toUpperCase
                                    ())) {
                                itemsItemFilter.add(itemsItem);
                            }
                        }
                    }
                    if (!itemsItemFilter.isEmpty()) {
                        Product product = sourceList.get(i).copy();
                        product.getItems().clear();
                        product.getItems().addAll(itemsItemFilter);
                        filterList.add(product);
                    }
                }
            }
            result.count = filterList.size();
            result.values = filterList;
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            storeProductList = (ArrayList<Product>) results.values;
            notifyDataSetChanged();
        }
    }

}
