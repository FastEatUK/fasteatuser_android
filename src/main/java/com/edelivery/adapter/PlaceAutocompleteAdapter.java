/*
 * Copyright (C) 2015 Google Inc. All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.edelivery.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;

import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.utils.AppLog;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Adapter that handles Autocomplete requests from the Places Geo Data API.
 * {@link AutocompletePrediction} results from the API are frozen and stored directly in this
 * adapter. (See {@link AutocompletePrediction#}.)
 */
public class PlaceAutocompleteAdapter
        extends BaseAdapter implements Filterable {

    private static final String TAG = "PlaceAutocompleteAdapter";
    private CharacterStyle styleBold = new StyleSpan(Typeface.BOLD);
    private Context context;
    private ViewHolder holder;
    private LayoutInflater inflater;
    /**
     * Current results returned by this adapter.
     */
    private List<AutocompletePrediction> mResultList;

    /**
     * Handles autocomplete requests.
     */
    private GoogleApiClient googleApiClient;


    private PlacesClient mPlacesClient;

    /**
     * The bounds used for Places Geo Data autocomplete API requests.
     */
    private LatLngBounds bounds;

    public RectangularBounds boundsnew;

    /**
     * The autocomplete filter used to restrict queries to a specific set of place types.
     */
//    private AutocompleteFilter placeFilter;

    /**
     * Initializes with a resource for text rows and autocomplete query bounds.
     *
     * @see ArrayAdapter#ArrayAdapter(Context, int)
     */
    public PlaceAutocompleteAdapter(Context context, GoogleApiClient googleApiClient) {
        this.context = context;
        this.googleApiClient = googleApiClient;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (this.context != null) {
            Places.initialize(this.context, this.context.getString(R.string.GOOGLE_ANDROID_API_KEY));
            mPlacesClient = Places.createClient(this.context);
        }

    }

//    public void setPlaceFilter(AutocompleteFilter placeFilter) {
//        this.placeFilter = placeFilter;
//    }

    /**
     * Sets the bounds for all subsequent queries.
     */
    public void setBounds(LatLngBounds bounds) {
        this.bounds = bounds;
        Log.e("setbounds", this.bounds.toString());

        boundsnew = RectangularBounds.newInstance(
                new LatLng(bounds.northeast.latitude,bounds.northeast.longitude),
                new LatLng(bounds.northeast.latitude,bounds.northeast.longitude));

        Log.e("boundsnew","" + bounds.northeast.latitude + bounds.northeast.longitude );
    }

    /**
     * Returns the number of results received in the last autocomplete query.
     */
    @Override
    public int getCount() {
        return mResultList.size();
    }

    /**
     * Returns an item from the last autocomplete query.
     */
    @Override
    public AutocompletePrediction getItem(int position) {
        return mResultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        // Sets the primary and secondary text for a row.
        // Note that getPrimaryText() and getSecondaryText() return a CharSequence that may contain
        // styling based on the given CharacterStyle.
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_autocomplete_list, parent, false);
            holder = new ViewHolder();
            holder.tvPlaceName = (CustomFontTextView) convertView.findViewById(R.id.tvPlaceName);
            holder.tvPlaceAddress = (CustomFontTextView) convertView.findViewById(R.id
                    .tvPlaceAddress);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        AutocompletePrediction item = getItem(position);
        holder.tvPlaceName.setText(item.getPrimaryText(styleBold));
        holder.tvPlaceAddress.setText(item.getSecondaryText(styleBold));

        return convertView;
    }

    /**
     * Returns the filter for the current set of autocomplete results.
     */
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(final CharSequence constraint) {
                final FilterResults results = new FilterResults();
                // Skip the autocomplete query if no constraints are given.
                if (constraint != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();

                    // Use the builder to create a FindAutocompletePredictionsRequest.
                    FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                            // Call either setLocationBias() OR setLocationRestriction().
//                            .setLocationBias(boundsnew)
//                            .setLocationBias(bounds)
                            .setCountry("UK")
//                            .setTypeFilter(TypeFilter.GEOCODE)
                            .setSessionToken(token)
                            .setQuery(constraint.toString())
                            .build();
                    Log.e("setnew boundsnew","" + bounds.northeast.latitude + bounds.northeast.longitude );

                    Task<FindAutocompletePredictionsResponse> autocompletePredictions = mPlacesClient.findAutocompletePredictions(request);

                    try {
                        Tasks.await(autocompletePredictions, 60, TimeUnit.SECONDS);
                    } catch (ExecutionException | InterruptedException | TimeoutException e) {
                        e.printStackTrace();
                    }

                    if (autocompletePredictions.isSuccessful()) {
                        FindAutocompletePredictionsResponse findAutocompletePredictionsResponse = autocompletePredictions.getResult();
                        if (findAutocompletePredictionsResponse != null)

                            mResultList = findAutocompletePredictionsResponse.getAutocompletePredictions();
                        results.values = mResultList;
                        results.count = mResultList.size();
                        return results;
                    } else {
                        return results;
                    }

                }

                Log.i("tagPlace", "Result********************************");

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    // The API returned at least one result, update the data.
                    notifyDataSetChanged();
                } else {
                    // The API did not return any results, invalidate the data set.
                    notifyDataSetInvalidated();
                }
            }

            @Override
            public CharSequence convertResultToString(Object resultValue) {
                // Override this method to display a readable result in the AutocompleteTextView
                // when clicked.
                if (resultValue instanceof AutocompletePrediction) {
                    return ((AutocompletePrediction) resultValue).getFullText(null);
                } else {
                    return super.convertResultToString(resultValue);
                }
            }
        };
    }

    /**
     * Submits an autocomplete query to the Places Geo Data Autocomplete API.
     * Results are returned as frozen AutocompletePrediction objects, ready to be cached.
     * objects to store the Place ID and description that the API returns.
     * Returns an empty list if no results were found.
     * Returns null if the API client is not available or the query did not complete
     * successfully.
     * This method MUST be called off the main UI thread, as it will block until data is returned
     * from the API, which may include a network request.
     */
    /*private ArrayList<AutocompletePrediction> getAutocomplete(CharSequence constraint) {
        if (googleApiClient.isConnected()) {
            AppLog.Log(TAG, "Starting autocomplete query for: " + constraint);

            // Submit the query to the autocomplete API and retrieve a PendingResult that will
            // contain the results when the query completes.
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi.getAutocompletePredictions(googleApiClient, constraint.toString(),
                                    bounds, placeFilter);




            // This method should have been called off the main UI thread. Block and wait for at
            // most 60s
            // for a result from the API.
            AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);

            // Confirm that the query completed successfully, otherwise return null
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
//                Toast.makeText(context, "Error contacting API: " + OrderStatus.toString(),
//                        Toast.LENGTH_SHORT).show();
                AppLog.Log(TAG, "Error getting autocomplete prediction API call: " + status
                        .toString());
                autocompletePredictions.release();
                return null;
            }

            AppLog.Log(TAG, "Query completed. Received " + autocompletePredictions.getCount()
                    + " predictions.");

            // Freeze the results immutable representation that can be stored safely.
            return DataBufferUtils.freezeAndClose(autocompletePredictions);
        }
        AppLog.Log(TAG, "Google API client is not connected for autocomplete query.");
        return null;
    }*/


    private class ViewHolder {
        CustomFontTextView tvPlaceName, tvPlaceAddress;

    }

}
