package com.edelivery.adapter;

import android.content.res.TypedArray;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.edelivery.HomeActivity;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;


/**
 * Created by elluminati on 06-06-2016.
 */
public class UserMenuAdapter extends RecyclerView.Adapter<UserMenuAdapter.UserViewHolder> {


    private HomeActivity activity;
    private TypedArray userItemTitle;
    private TypedArray userItemIcon;


    public UserMenuAdapter(HomeActivity activity) {
        // TODO Auto-generated constructor stub

        userItemTitle = activity.getResources().obtainTypedArray(R.array.user_menu_item);
        userItemIcon = activity.getResources().obtainTypedArray(R.array.user_menu_icons);

        this.activity = activity;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_menu,
                parent,
                false);
        return new UserViewHolder(view);


    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        // set data here
        holder.ivUserItemIcon.setImageResource(userItemIcon.getResourceId(position, 0));
        holder.tvUserItemTitle.setText(userItemTitle.getString(position));
    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        // item count here
        return userItemTitle.length();
    }

    /**
     * Holder for item
     */

    protected class UserViewHolder extends RecyclerView.ViewHolder {

        CustomFontTextView tvUserItemTitle;
        ImageView ivUserItemIcon;

        public UserViewHolder(View itemView) {
            super(itemView);

            tvUserItemTitle = (CustomFontTextView) itemView.findViewById(R.id.tvUserItemTitle);
            ivUserItemIcon = (ImageView) itemView.findViewById(R.id.ivUserItemIcon);

        }


    }


}
