package com.edelivery.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.edelivery.AddUserLocationActivity;
import com.edelivery.CheckoutDeliveryLocationActivity;
import com.edelivery.DeliveryLocationActivity;
import com.edelivery.R;
import com.edelivery.animation.ViewHelper;
import com.edelivery.models.hereresponce.Result;
import com.edelivery.utils.Utils;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.search.AutoSuggest;
import com.here.android.mpa.search.AutoSuggestPlace;
import com.here.android.mpa.search.AutoSuggestQuery;
import com.here.android.mpa.search.AutoSuggestSearch;
import com.mapbox.mapboxsdk.geometry.LatLng;

import java.util.ArrayList;
import java.util.List;

public class AutoSuggestAdapter extends RecyclerView.Adapter<AutoSuggestAdapter.MyViewHolder> {

    private List<Result> m_resultsList = new ArrayList<>();
    LatLng latLng;
    Context context;
    String adress;

    public AutoSuggestAdapter(Context context, List<Result> objects) {
        Log.i("autoSuggestPlace", "ok");
        // super(context, resource, objects);
        this.m_resultsList = objects;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.result_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        holder.m_description.setText(m_resultsList.get(position).getAdress());
      /*  holder.m_title.setText(m_resultsList.get(position).getTitle());*/

        /*if(holder.rbLoc.setImageResource(R.drawable.checkmark);)*/


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (context != null) {

                    if (context instanceof DeliveryLocationActivity) {

                        ((DeliveryLocationActivity) context).setListViewData(new LatLng(Double.valueOf(m_resultsList.get(position).getLat()),Double.valueOf( m_resultsList.get(position).getLng()))
                                , m_resultsList.get(position).getAdress());

                    } else if (context instanceof CheckoutDeliveryLocationActivity) {

                        // AutoSuggestPlace autoSuggestPlace = (AutoSuggestPlace) autoSuggest;
                        ((CheckoutDeliveryLocationActivity) context).setListViewData(new LatLng(Double.valueOf(m_resultsList.get(position).getLat()), Double.valueOf(m_resultsList.get(position).getLng()))
                                , m_resultsList.get(position).getAdress());

                    } else if (context instanceof AddUserLocationActivity) {
                        holder.rbLoc.setImageResource(R.drawable.checkmark);
                        //AutoSuggestPlace autoSuggestPlace = (AutoSuggestPlace) autoSuggest;
                        ((AddUserLocationActivity) context).setListViewDataGetAddress(position);

                    }

                }
            }
        });


    }


    @Override
    public int getItemCount() {
        Log.i("AutoSuggestsize", String.valueOf(m_resultsList.size()));
        return m_resultsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView m_title, m_description;
        RelativeLayout llitems;
        AppCompatImageView rbLoc;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            llitems = (RelativeLayout) itemView.findViewById(R.id.llitems);
            m_title = (TextView) itemView.findViewById(R.id.txt_title);
            m_description = (TextView) itemView.findViewById(R.id.txt_dec);
            rbLoc = (AppCompatImageView) itemView.findViewById(R.id.rbLoc);


        }
    }

    public void removeAt(int position) {
        m_resultsList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, m_resultsList.size());
    }
}
