package com.edelivery.adapter;

import android.content.Context;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.edelivery.R;
import com.edelivery.models.datamodels.Ads;
import com.edelivery.utils.PreferenceHelper;

import java.util.List;



/**
 * Created by elluminati on 18-Apr-17.
 */

public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.AdsViewHolder> {

    private List<Ads> ads;
    private Context context;
    private boolean isItemClick;

    public AdsAdapter(Context context, List<Ads>
            ads, boolean isItemClick) {
        this.ads = ads;
        this.context = context;
        this.isItemClick = isItemClick;
    }


    @Override
    public AdsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ads, parent, false);
        return new AdsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdsViewHolder holder, int position) {
        Glide.with(context).load(PreferenceHelper.getInstance(context).getIMAGE_BASE_URL() + ads.get(position).getImageUrl())
                .dontAnimate()
                .placeholder(ResourcesCompat.getDrawable
                        (context
                                .getResources(), R.drawable.placeholder, null)).fallback
                (ResourcesCompat
                        .getDrawable
                                (context.getResources(), R.drawable.placeholder,
                                        null)).into
                (holder.ivAddImage);

    }

    @Override
    public int getItemCount() {
        return ads.size();
    }



    public class AdsViewHolder extends RecyclerView.ViewHolder {
        ImageView ivAddImage;

        public AdsViewHolder(View itemView) {
            super(itemView);
            ivAddImage = (ImageView) itemView.findViewById(R.id.ivAddImage);

        }

    }



}
