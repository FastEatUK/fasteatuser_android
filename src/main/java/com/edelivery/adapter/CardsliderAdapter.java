package com.edelivery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.edelivery.R;
import com.edelivery.StripePaymentActivity;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Card;

import java.util.ArrayList;

public class CardsliderAdapter extends RecyclerView.Adapter<CardsliderAdapter.CardViewHolder>  {

    private ArrayList<Card> cardList;
    private StripePaymentActivity stripePaymentActivity;
    private Context context;

    public CardsliderAdapter(ArrayList<Card> cardList, StripePaymentActivity stripePaymentActivity, Context context) {
        this.cardList = cardList;
        this.stripePaymentActivity = stripePaymentActivity;
        this.context = context;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stripe_card_item, parent,
                false);

        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {

        String month="",year="";
        Card card=cardList.get(position);
        holder.cardholdername.setText(card.getCardHolderName());


        if (card.getCardExpiryDate().length()>=6)
        {
            month=card.getCardExpiryDate().substring(0,card.getCardExpiryDate().length()-5);
            year=card.getCardExpiryDate().substring(card.getCardExpiryDate().length()-2,card.getCardExpiryDate().length());

        }
        else if (card.getCardExpiryDate().length()>=4)
        {
            month=card.getCardExpiryDate().substring(0,card.getCardExpiryDate().length()-3);
            year=card.getCardExpiryDate().substring(card.getCardExpiryDate().length()-2,card.getCardExpiryDate().length());
        }

        if (card.getCardExpiryDate().length()==6 || card.getCardExpiryDate().length()==4)
        {
            month="0"+month;
        }
        holder.cardexpiry.setText(month+"/"+year);
        holder.lastfour.setText(card.getLastFour());


        if (card.getIsDefault())
        {
            holder.selectedcard.setVisibility(View.VISIBLE);
        }
        else {
            holder.selectedcard.setVisibility(View.GONE);
        }



    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }



    protected class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CustomFontTextView lastfour,cardholdername,cardexpiry,cardtype;
        ImageView cardtypeimage,selectedcard,removecard;
        LinearLayout cardlayout;

        public CardViewHolder(@NonNull View itemView) {
            super(itemView);

            lastfour=itemView.findViewById(R.id.tv_lastfourcardno);
            cardholdername=itemView.findViewById(R.id.tv_cardname);
            cardexpiry=itemView.findViewById(R.id.tv_carddate);
        /*    cardtype=itemView.findViewById(R.id.tv_cardtype);*/
            cardlayout=itemView.findViewById(R.id.rlCard);
            selectedcard=itemView.findViewById(R.id.ivSelected);
            removecard=itemView.findViewById(R.id.ivDeleteCard);
            cardtypeimage=itemView.findViewById(R.id.iv_cardtype);
            cardlayout.setOnClickListener(this);
            removecard.setOnClickListener(this);
            selectedcard.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.rlCard:
                    stripePaymentActivity.selectCreditCard(getAdapterPosition());
                    break;
                case R.id.ivDeleteCard:
                    stripePaymentActivity.openDeleteCard(getAdapterPosition());
                    break;
                default:
                    break;

                }
            }

        }
    }

