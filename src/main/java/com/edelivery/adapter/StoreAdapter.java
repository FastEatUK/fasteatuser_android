package com.edelivery.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.edelivery.R;
import com.edelivery.StoreProductActivity;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.Ads;
import com.edelivery.models.datamodels.Store;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

/**
 * Created by elluminati on 16-Feb-17.
 */

public abstract class StoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        Filterable {
    public static final String TAG = StoreAdapter.class.getName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_HEADER = 2;
    public int header = 1;
    private ArrayList<Store> storeList;
    private Context context;
    private long currentTime;
    private StoreFilter storeFilter;
    private ParseContent parseContent;
    private ArrayList<Store> filterList;
    private List<Ads> ads;
    private boolean isScheduleStart;
    private ScheduledExecutorService tripStatusSchedule;
    private RecyclerView recyclerView;
    private Handler handler;
    private int count = 0;
    private TextView[] dots;
    private AdsAdapter adsAdapter;
    private CustomFontTextView tvAdds;
    private String filterBy;

    public StoreAdapter(Context context, ArrayList<Store> storeList,
                        List<Ads> ads) {
        this.context = context;
        this.storeList = storeList;
        parseContent = ParseContent.getInstance();
        String dateString = parseContent.timeFormat2.format(System.currentTimeMillis());
        try {
            Date date = parseContent.timeFormat2.parse(dateString);
            currentTime = date.getTime();
            AppLog.Log(StoreAdapter.class.getName(), "currentTime=" + currentTime);
        } catch (ParseException e) {
            AppLog.handleException(StoreAdapter.class.getName(), e);
        }
        filterList = new ArrayList<Store>();
        this.ads = ads;
        initHandler();
    }

    public String getFilterBy() {
        return filterBy;
    }

    public void setFilterBy(String filterBy) {
        this.filterBy = filterBy;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_HEADER) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_ads_contain,
                    parent,
                    false);
            return new StoreViewHeader(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_store_list,
                    parent, false);

            return new StoreHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof StoreHolder) {
            position = position - header;
            StoreHolder storeHolder = (StoreHolder) holder;
            Store store = storeList.get(position);
            AppLog.Log("STORE_IMG", store.getImageUrl());
            if (PreferenceHelper.getInstance(context).getIsLoadStoreImage()) {
                Glide.with(context).load(PreferenceHelper.getInstance(context).getIMAGE_BASE_URL() + store.getImageUrl()).dontAnimate()
                        .placeholder
                                (ResourcesCompat.getDrawable(context
                                        .getResources(), R.drawable.placeholder, null)).fallback
                        (ResourcesCompat
                                .getDrawable
                                        (context
                                                .getResources(), R.drawable.placeholder, null)).into
                        (storeHolder.ivStoreImage);
            }


            storeHolder.tvStoreName.setText(store.getName());
            storeHolder.tvStorePricing.setText(store.getPriceRattingAndTag());
            if (store.getDeliveryTimeMax() > store.getDeliveryTime()) {
                storeHolder.tvStoreApproxTime.setText(String.valueOf(store.getDeliveryTime()) +
                        " - " + String.valueOf(store.getDeliveryTimeMax()) + " " +
                        context
                                .getResources().getString(R.string.unit_mins));
            } else {
                storeHolder.tvStoreApproxTime.setText(String.valueOf(store.getDeliveryTime()) +
                        " " +
                        context
                                .getResources().getString(R.string.unit_mins));
            }

            if(store.getNote()!=null) {
                storeHolder.tvStoreRatings.setVisibility(View.VISIBLE);
                storeHolder.tvStoreRatings.setText(String.valueOf(store.getNote()));
            }else {
                storeHolder.tvStoreRatings.setVisibility(View.INVISIBLE);
            }

            if (store.isStoreClosed()) {
                storeHolder.llStoreClosed.setVisibility(View.VISIBLE);
                storeHolder.tvTag.setText(context.getResources().getText(R.string
                        .text_store_closed));
                storeHolder.tvStoreReOpenTime.setVisibility(View.VISIBLE);
                storeHolder.tvStoreReOpenTime.setText(store.getReOpenTime());
            } else {
                if (store.isBusy()) {
                    storeHolder.llStoreClosed.setVisibility(View.VISIBLE);
                    storeHolder.tvTag.setText(context.getResources().getText(R.string
                            .text_store_busy));
                    storeHolder.tvStoreReOpenTime.setVisibility(View.GONE);
                } else {
                    storeHolder.llStoreClosed.setVisibility(View.GONE);
                }
            }

            if (TextUtils.isEmpty(PreferenceHelper.getInstance(context).getUserId())) {
                storeHolder.ivFavourites.setVisibility(View.GONE);
            } else {
                store.setFavourite(CurrentBooking.getInstance().getFavourite()
                        .contains(store.getId()));
                storeHolder.ivFavourites.setVisibility(View.VISIBLE);
                storeHolder.ivFavourites.setChecked(store.isFavourite());
            }
            storeHolder.tvRatingsCount.setText("(" + String.valueOf(store.getRateCount()) + ")");
        } else {
            StoreViewHeader storeViewHeader = (StoreViewHeader) holder;
            addBottomDots(storeViewHeader.llDots, position);
            tvAdds = storeViewHeader.tvAdds;

            if (ads.isEmpty()) {
                storeViewHeader.rcvAds.setVisibility(View.GONE);
                storeViewHeader.rcvAds.getLayoutParams().height = 0;
                storeViewHeader.darkView.setVisibility(View.GONE);
                tvAdds.setVisibility(View.GONE);
                storeViewHeader.llDots.setVisibility(View.GONE);
            } else {
                adsAdapter = new AdsAdapter(context, ads, true);
                storeViewHeader.rcvAds.setAdapter(adsAdapter);
                storeViewHeader.rcvAds.getLayoutParams().height = WindowManager.LayoutParams
                        .WRAP_CONTENT;
                recyclerView = storeViewHeader.rcvAds;
                storeViewHeader.rcvAds.setVisibility(View.VISIBLE);
                storeViewHeader.darkView.setVisibility(View.VISIBLE);
                tvAdds.setVisibility(View.VISIBLE);
                storeViewHeader.llDots.setVisibility(View.VISIBLE);
                if (!ads.isEmpty()) {
                    startAdsScheduled();
                }
                tvAdds.setText(ads.get(0).getAdsDetail());
            }

        }
    }

    public void startAdsScheduled() {
        if (!isScheduleStart) {
            tripStatusSchedule = Executors.newSingleThreadScheduledExecutor();
            tripStatusSchedule.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    Message message = handler.obtainMessage();
                    handler.sendMessage(message);
                }
            }, 0, Const.ADS_SCHEDULED_SECONDS, TimeUnit
                    .SECONDS);
            AppLog.Log(TAG, "Schedule Start");
            isScheduleStart = true;
        }
    }

    private void initHandler() {

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                if (count == ads.size() - 1) {
                    count = 0;
                } else {
                    count++;
                }
                recyclerView.smoothScrollToPosition(count);
            }
        };
    }

    public void stopAdsScheduled() {
        if (isScheduleStart) {
            AppLog.Log(TAG, "Schedule Stop");
            tripStatusSchedule.shutdown(); // Disable new tasks from being submitted
            // Wait a while for existing tasks to terminate
            try {
                if (!tripStatusSchedule.awaitTermination(60, TimeUnit.SECONDS)) {
                    tripStatusSchedule.shutdownNow(); // Cancel currently executing tasks
                    // Wait a while for tasks to respond to being cancelled
                    if (!tripStatusSchedule.awaitTermination(60, TimeUnit.SECONDS))
                        AppLog.Log(TAG, "Pool did not terminate");

                }
            } catch (InterruptedException e) {
                AppLog.handleException(TAG, e);
                // (Re-)Cancel if current thread also interrupted
                tripStatusSchedule.shutdownNow();
                // Preserve interrupt ProviderStatus
                Thread.currentThread().interrupt();
            }
            isScheduleStart = false;
        }
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return VIEW_HEADER;
        } else {
            return VIEW_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return storeList.size() + header;
    }

    @Override
    public Filter getFilter() {
        if (storeFilter == null)
            storeFilter = new StoreFilter(storeList);
        return storeFilter;
    }

    public ArrayList<Store> getStoreArrayList() {
        return storeList;
    }

    public void setStoreArrayList(ArrayList<Store> storeList) {
        this.storeList = storeList;
        storeFilter = new StoreFilter(this.storeList);
    }

    public abstract void onSelected(View view, int position);

    public abstract void setFavourites(int position, boolean isFavourite);

    private void addBottomDots(LinearLayout llDots, int currentPage) {
        if (ads.size() > 0) {
            dots = new TextView[ads.size()];

            llDots.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(context);
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(35);
                dots[i].setTextColor(ResourcesCompat.getColor(context.getResources(), R
                                .color
                                .color_app_gray_trans
                        , null));
                llDots.addView(dots[i]);
            }

            if (dots.length > 0)
                dots[currentPage].setTextColor(ResourcesCompat.getColor(context
                                .getResources(), R.color
                                .color_white
                        , null));
        }


    }

    private void dotColorChange(LinearLayout llDots, int currentPage) {
        if (llDots.getChildCount() > 0) {
            for (int i = 0; i < llDots.getChildCount(); i++) {
                TextView textView = (TextView) llDots.getChildAt(i);
                textView.setTextColor(ResourcesCompat.getColor(context.getResources(), R
                                .color
                                .color_app_gray_trans
                        , null));

            }
            TextView textView = (TextView) llDots.getChildAt(currentPage);
            if (textView != null) {
                textView.setTextColor(ResourcesCompat.getColor(context.getResources(), R
                                .color
                                .color_white
                        , null));
            }


        }


    }

    private void goToStoreProductActivity(String storeId) {
        Intent intent = new Intent(context, StoreProductActivity.class);
        intent.putExtra(Const.STORE_DETAIL, storeId);
        context.startActivity(intent);
    }

    protected class StoreHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView ivStoreImage;
        ToggleButton ivFavourites;
        LinearLayout llStoreClosed, llStoreCard;
        CustomFontTextView tvStorePricing, tvStoreRatings, tvStoreReOpenTime,
                tvStoreApproxTime, tvRatingsCount, tvTag;
        CustomFontTextViewTitle tvStoreName;

        public StoreHolder(View itemView) {
            super(itemView);
            llStoreClosed = (LinearLayout) itemView.findViewById(R.id.llStoreClosed);
            llStoreCard = (LinearLayout) itemView.findViewById(R.id.llStoreCard);
            ivStoreImage = (ImageView) itemView.findViewById(R.id.ivStoreImage);
            tvStoreName = (CustomFontTextViewTitle) itemView.findViewById(R.id.tvStoreName);
            tvStorePricing = (CustomFontTextView) itemView.findViewById(R.id.tvStorePricing);
            tvStoreRatings = (CustomFontTextView) itemView.findViewById(R.id.tvStoreRatings);
            tvStoreReOpenTime = (CustomFontTextView) itemView.findViewById(R.id.tvStoreReOpenTime);
            tvStoreApproxTime = (CustomFontTextView) itemView.findViewById(R.id.tvStoreApproxTime);
            ivFavourites = (ToggleButton) itemView.findViewById(R.id.ivFavourites);
            itemView.setOnClickListener(this);
            ivFavourites.setOnClickListener(this);
            tvRatingsCount = (CustomFontTextView) itemView.findViewById(R.id.tvRatingsCount);
            tvTag = (CustomFontTextView) itemView.findViewById(R.id.tvTag);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivFavourites:
                    setFavourites(getAdapterPosition() - header, storeList.get(getAdapterPosition
                            () -
                            header)
                            .isFavourite());
                    break;
                default:
                    onSelected(v, getAdapterPosition() - header);
                    break;
            }
        }
    }

    private class StoreFilter extends Filter {
        private ArrayList<Store> sourceList;

        StoreFilter(ArrayList<Store> storeArrayList) {
            sourceList = new ArrayList<>();
            synchronized (this) {
                sourceList.addAll(storeArrayList);
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterSeq = constraint.toString();
            FilterResults result = new FilterResults();
            filterSeq = filterSeq.trim();
            if (filterSeq.length() > 0) {
                filterList.clear();
                if (TextUtils.equals(filterBy, context.getResources().getString(R.string
                        .text_item))) {
                    for (Store store : sourceList) {
                        for (String itemName : store.getProductItemNameList()) {
                            if (itemName.toUpperCase().contains(filterSeq.toUpperCase())) {
                                if (!filterList.contains(store)) {
                                    filterList.add(store);
                                }
                            }
                        }
                    }
                } else if (TextUtils.equals(filterBy, context.getResources().getString(R
                        .string.text_tag))) {
                    for (Store store : sourceList) {
                        for (String tag : store.getFamousProductsTags()) {
                            if (tag.toUpperCase().contains(filterSeq.toUpperCase())) {
                                if (!filterList.contains(store)) {
                                    filterList.add(store);
                                }
                            }
                        }

                    }
                } else {
                    for (Store store : sourceList) {
                        if (store.getName().toUpperCase().contains(filterSeq.toUpperCase()))
                            filterList.add(store);
                    }
                }


                result.count = filterList.size();
                result.values = filterList;
            } else {
                // add all objects
                synchronized (this) {
                    result.values = sourceList;
                    result.count = sourceList.size();
                }
            }
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            storeList = (ArrayList<Store>) results.values;
            notifyDataSetChanged();
        }
    }

    protected class StoreViewHeader extends RecyclerView.ViewHolder {

        RecyclerView rcvAds;
        LinearLayout llDots;
        CustomFontTextView tvAdds;
        View darkView;

        public StoreViewHeader(View itemView) {
            super(itemView);
            rcvAds = (RecyclerView) itemView.findViewById(R.id.rcvAds);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(context,
                    LinearLayoutManager
                            .HORIZONTAL, false);
            rcvAds.setLayoutManager(layoutManager);
            SnapHelper snapHelper = new LinearSnapHelper();
            snapHelper.attachToRecyclerView(rcvAds);
            rcvAds.addOnItemTouchListener(new RecyclerTouchListener(context, rcvAds, new
                    ClickListener() {


                        @Override
                        public void onClick(View view, int position) {
                            if (!TextUtils.isEmpty(ads.get(position).getStoreId())) {
                                goToStoreProductActivity(ads.get(position)
                                        .getStoreId());


                            }

                        }

                        @Override
                        public void onLongClick(View view, int position) {

                        }
                    }));
            llDots = (LinearLayout) itemView.findViewById(R.id.llDots);
            tvAdds = (CustomFontTextView) itemView.findViewById(R.id.tvAdds);
            darkView = itemView.findViewById(R.id.darkView);
            rcvAds.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    dotColorChange(llDots, layoutManager.findLastCompletelyVisibleItemPosition());
                    if (tvAdds != null && !ads.isEmpty() && layoutManager
                            .findLastCompletelyVisibleItemPosition() >= 0) {
                        tvAdds.setText(ads.get(layoutManager
                                .findLastCompletelyVisibleItemPosition()).getAdsDetail());
                    }

                }
            });
        }
    }

}
