package com.edelivery;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import com.edelivery.component.CustomFontCheckBox;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.fragments.HistoryFragment;
import com.edelivery.models.datamodels.RegistartionData;
import com.edelivery.models.responsemodels.EmailCheck;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.OrderHistoryResponse;
import com.edelivery.models.responsemodels.PaymentIntentResponce;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.internal.ImageRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.OAuthProvider;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.edelivery.utils.Const.Params.ORDER_ID;
import static com.edelivery.utils.Const.google.GOOGLE_SIGN_IN;

public class RegistrationActivity extends BaseAppCompatActivity implements LocationHelper
        .OnLocationReceived {


    private CustomFontEditTextView etRegisterLastName, etRegisterFirstName, etRegisterEmail,
            etRegisterPassword, etRegisterPasswordRetype, etReferrelCode;

    private CustomFontTextView tvApplyReferrel;
    private ImageView btnFb, btnGoogle, btnTwitter, btnApple;
    private TextInputLayout tilPassword, tilRetypePassword;
    private LinearLayout llSocialLogin;
    private CheckBox cbTcPolicy;
    private CustomFontTextView tvPolicy, tvsigin;
    private Button SignupButoon;
    private String FBA_TYPE = "";

    public CallbackManager callbackManager;
    private TwitterAuthClient twitterAuthClient;
    private String referralCode = "", socialId = "";
    public LocationHelper locationHelper;
    private Bitmap bmSocialProfilePic;
    private Uri picUri;

    private RegistartionData registartionData;
    public boolean isReferral_applied = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        registartionData = new RegistartionData();
        findViewById();
        setViewListener();
        setClickableString();
        callbackManager = CallbackManager.Factory.create();

        etReferrelCode.setText(preferenceHelper.getREFERRAL_DEEPLINK_REGISTER()); //set referal if used deeplink
        locationHelper = new LocationHelper(this);
        locationHelper.setLocationReceivedLister(this);

        callDeepLinkFunctionGet();

    }

    private void callDeepLinkFunctionGet() {

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
// Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            Log.w("RegistrationActivity", "getDynamicLink:onSuccess" + deepLink.getQueryParameter("referral"));

                            if (!TextUtils.isEmpty(deepLink.getQueryParameter("referral"))) {

                                Log.e("deepLink", deepLink.getQueryParameter("referral").toString());

                                etReferrelCode.setText(deepLink.getQueryParameter("referral"));

                                //store referral in deeplink
                                preferenceHelper.putREFERRAL_DEEPLINK_REGISTER(etReferrelCode.getText().toString());

                                if (!etReferrelCode.getText().toString().isEmpty()) {
                                    getReferrelCode(etReferrelCode.getText().toString(), "deeplink");
                                }
                            }
                        }

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("RegistrationActivity", "getDynamicLink:onFailure", e);
                    }
                });

    }


    @Override
    protected boolean isValidate() {
        String msg = null;

        if (TextUtils.isEmpty(etRegisterFirstName.getText().toString().trim())) {
            msg = getString(R.string.msg_please_enter_valid_name);
            etRegisterFirstName.setError(msg);
            etRegisterFirstName.requestFocus();
        } else if (TextUtils.isEmpty(etRegisterLastName.getText().toString().trim())) {
            msg = getString(R.string.msg_please_enter_valid_name);
            etRegisterLastName.setError(msg);
            etRegisterLastName.requestFocus();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etRegisterEmail.getText().toString().trim())
                .matches()) {
            msg = getString(R.string.msg_please_enter_valid_email);
            etRegisterEmail.setError(msg);
            etRegisterEmail.requestFocus();
        } else if (TextUtils.isEmpty(etRegisterPassword.getText().toString().trim())) {
            msg = getString(R.string.msg_please_enter_password);
            etRegisterPassword.setError(msg);
            etRegisterPassword.requestFocus();
        } else if (etRegisterPassword.getText().toString().trim().length() < 6 || etRegisterPassword.getText().toString().trim().length() > 10) {
            msg = getString(R.string.msg_please_enter_valid_password_length);
            etRegisterPassword.setError(msg);
            etRegisterPassword.requestFocus();
        } else if (!TextUtils.equals(etRegisterPasswordRetype.getText().toString().trim(),
                etRegisterPassword.getText().toString().trim())) {
            msg = getString(R.string.msg_enter_password_not_match);
            etRegisterPasswordRetype.setError(msg);
            etRegisterPasswordRetype.requestFocus();

        } else if (!cbTcPolicy.isChecked()) {
            msg = getResources().getString(R.string
                    .msg_plz_accept_tc);
            Utils.showToast(msg, RegistrationActivity.this);
        }
        return TextUtils.isEmpty(msg);
    }

    @Override
    protected void findViewById() {
        etRegisterFirstName = findViewById(R.id.etRegisterFirstName);
        etRegisterLastName = findViewById(R.id.etRegisterLastName);
        etRegisterEmail = findViewById(R.id.etRegisterEmail);
        etRegisterPassword = findViewById(R.id.etRegisterPassword);
        etRegisterPasswordRetype = findViewById(R.id.etRegisterPasswordRetype);
        tvPolicy = findViewById(R.id.tvPolicy);
        tilPassword = findViewById(R.id.tilPassword);
        tilRetypePassword = findViewById(R.id.tilRetypePassword);
        llSocialLogin = findViewById(R.id.llSocialButton);
        etReferrelCode = findViewById(R.id.etReferrelCode);
        cbTcPolicy = findViewById(R.id.cbTcPolicy_registration);

        btnFb = findViewById(R.id.btnFb);
        btnGoogle = findViewById(R.id.btnGoogle);
        btnTwitter = findViewById(R.id.btnTwitter);
        btnApple = findViewById(R.id.btnApple);
        tvApplyReferrel = findViewById(R.id.tvApplyReferrel);
        tvsigin = findViewById(R.id.tv_signin);
        SignupButoon = findViewById(R.id.btn_signup);
    }

    @Override
    protected void setViewListener() {
        btnFb.setOnClickListener(this);
        btnGoogle.setOnClickListener(this);
        btnTwitter.setOnClickListener(this);
        btnApple.setOnClickListener(this);
        SignupButoon.setOnClickListener(this);
        tvsigin.setOnClickListener(this);
        tvApplyReferrel.setOnClickListener(this);
        etReferrelCode.setOnClickListener(this);

        etReferrelCode.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }

    @Override
    protected void onBackNavigation() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signup:
                if (isValidate()) {

                    if (!etReferrelCode.getText().toString().isEmpty() && !isReferral_applied) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(RegistrationActivity.this);
                        builder1.setMessage(getString(R.string.you_want_to_apply_referral));
                        builder1.setCancelable(false);
                        builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                if (!etReferrelCode.getText().toString().isEmpty()) {
                                    getReferrelCode(etReferrelCode.getText().toString(), "signup");
                                }
                            }
                        });
                        builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                callRegisterFlow();
                            }
                        });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    } else {
                        callRegisterFlow();
                    }

                }
                break;
            case R.id.btnFb:
                FBA_TYPE = Const.Params.Social_facebook;
                loginWithFacebook();
                break;
            case R.id.btnGoogle:
                FBA_TYPE = Const.Params.Social_Gmail;
                loginWithGoogle();
                break;
            case R.id.btnTwitter:
                twitterAuthClient = new TwitterAuthClient();
                FBA_TYPE = Const.Params.Social_twitter;
                loginWithTwitter();
                break;

            case R.id.btnApple:
                FBA_TYPE = Const.Params.apple;
                LoginWithApple();
                break;

            case R.id.tv_signin:
                finish();
                break;
            case R.id.tvApplyReferrel:
                if (!etReferrelCode.getText().toString().isEmpty()) {
                    getReferrelCode(etReferrelCode.getText().toString(), "apply");
                } else {
                    String msg = getString(R.string.msg_plz_enter_valid_referral);
                    etReferrelCode.setError(msg);
                    etReferrelCode.requestFocus();
                }

                break;

        }

    }

    private void callRegisterFlow() {
        if (TextUtils.isEmpty(socialId)) {
            GoForRegistration();
        } else {
            if (TextUtils.isEmpty(registartionData.getFirstname())) {
                registartionData.setFirstname(etRegisterFirstName.getText().toString());
            }
            if (TextUtils.isEmpty(registartionData.getLastname())) {
                registartionData.setLastname(etRegisterLastName.getText().toString());
            }
            checkEmailInserver(etRegisterEmail.getText().toString());
        }
    }

    private void getReferrelCode(String referral_code, String isFrom) {

        Utils.showCustomProgressDialog(RegistrationActivity.this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.REFERRAL_CODE, referral_code);
            jsonObject.put(Const.Params.TYPE, Const.Type.USER);

        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ORDER_TRACK_ACTIVITY, e);
        }

        Log.i("setShowInvoice", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.getCheckReferral(ApiClient.makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new retrofit2.Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                AppLog.Log("setShowInvoice", ApiClient.JSONResponse(response.body()));
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && response.body().isSuccess()) {

                    cbTcPolicy.setChecked(true);
                    isReferral_applied = true;
                    tvApplyReferrel.setClickable(false);
                    tvApplyReferrel.setEnabled(false);

                    etReferrelCode.setEnabled(false);
                    etReferrelCode.setKeyListener(null);

                    //Utils.showMessageToast(response.body().getMessage(), getActivity());

                    callDialogForMessage(response.body().getMessages());

                    tvApplyReferrel.setTextColor(getResources().getColor(R.color.background_tab_grey));

                    if (isFrom.equals("signup")) {
                        callRegisterFlow();
                    }

                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), RegistrationActivity.this);
                }
            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                callErrorFunction(t);
                AppLog.handleThrowable(Const.Tag.INVOICE_FRAGMENT, t);
            }
        });
    }

    private void callDialogForMessage(String message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(message);
        builder1.setCancelable(false);

        builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GOOGLE_SIGN_IN:
                // result from google
                getGoogleSignInResult(data);
                break;

            case Const.LOGIN_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    setResult(Activity.RESULT_OK);
                    finish();
                }
                break;
        }
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (twitterAuthClient != null) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }


    }

    public void loginWithGoogle() {
        Auth.GoogleSignInApi.revokeAccess(locationHelper.googleApiClient)
                .setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                AppLog.Log("REVOKE_ACCESS_GOOGLE", status.toString());
                                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent
                                        (locationHelper.googleApiClient);
                                startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
                            }
                        });
    }

    private void getGoogleSignInResult(Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent
                (data);
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            /*  if (isLoginTabSelect()) {
           // login(acct.getId());
            } else {*/
            if (acct != null) {
                String firstName;
                String lastName = "";
                if (acct.getDisplayName().contains(" ")) {
                    String[] strings = acct.getDisplayName().split(" ");
                    firstName = strings[0];
                    lastName = strings[1];
                } else {
                    firstName = acct.getDisplayName();
                }

                updateUiForSocialLogin(acct.getEmail(), acct.getId(),
                        firstName,
                        lastName, acct
                                .getPhotoUrl());
                // }
            }


        } else {
            // Signed out, show unauthenticated UI.
            AppLog.Log("GOOGLE_RESULT", "failed");
        }
    }

    public void updateUiForSocialLogin(String email, String socialId, String firstName, String
            lastName, Uri profileUri) {
        registartionData = new RegistartionData();
        if (!TextUtils.isEmpty(email)) {
            etRegisterEmail.setText(email);
            registartionData.setEmail(email);
            etRegisterEmail.setEnabled(false);
            etRegisterEmail.setFocusableInTouchMode(false);
            preferenceHelper.putIsEmailVerified(true);

        }
        this.socialId = socialId;
        registartionData.setSocialId(socialId);
        etRegisterFirstName.setText(firstName);
        etRegisterLastName.setText(lastName);
        picUri = profileUri;
        etRegisterPassword.setVisibility(View.GONE);
        etRegisterPasswordRetype.setVisibility(View.GONE);
        tilPassword.setVisibility(View.GONE);
        tilRetypePassword.setVisibility(View.GONE);

        registartionData.setFirstname(firstName);
        registartionData.setLastname(lastName);
        registartionData.setSocialType(FBA_TYPE);


        if (picUri != null) {
            Utils.showCustomProgressDialog(RegistrationActivity.this, false);
            Glide.with(RegistrationActivity.this)
                    .asBitmap()
                    .load(picUri.toString())
                    .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.man_user)
                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            AppLog.handleException(getClass().getSimpleName(), e);
                            Utils.showToast(e.getMessage(), RegistrationActivity.this);
                            Utils.hideCustomProgressDialog();
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                            bmSocialProfilePic = resource;
                            registartionData.setImage(bmSocialProfilePic);
                            Utils.hideCustomProgressDialog();
                            return true;
                        }

                    });
            Utils.hideCustomProgressDialog();
        }


    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (locationHelper != null) {
            locationHelper.onStart();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (locationHelper != null) {
            locationHelper.onStop();
        }
    }

    public void loginWithFacebook() {
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        getFacebookSignInResult(loginResult);

                    }

                    @Override
                    public void onCancel() {
                        // App code
                        AppLog.Log("FB_RESULT", "failed");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        AppLog.Log("FB_RESULT", exception.getMessage());
                    }
                });
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(Const.Facebook
                .PUBLIC_PROFILE, Const.Facebook.EMAIL));
    }

    private void getFacebookSignInResult(LoginResult loginResult) {
        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject json_object,
                            GraphResponse response) {
                        try {
                            Profile profile = Profile.getCurrentProfile();
                            if (profile != null) {
                                updateUiForSocialLogin(json_object.getString(Const
                                                .Facebook.EMAIL),
                                        profile.getId(), profile.getFirstName(), profile.getLastName(), ImageRequest.getProfilePictureUri
                                                (profile.getId(), 250,
                                                        250));

                            }
                        } catch (JSONException e) {
                            AppLog.handleException(Const.Tag.REGISTER_FRAGMENT, e);
                        }

                    }

                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }

    public void loginWithTwitter() {

        try {

            if (twitterAuthClient != null) {
                twitterAuthClient.authorize(RegistrationActivity.this, new com.twitter.sdk.android.core
                        .Callback<TwitterSession>
                        () {


                    @Override
                    public void success(final Result<TwitterSession> result1) {
                        AppLog.Log("TWITTER_LOGIN", "success");


                        final TwitterSession twitterSession = result1.data;
                        //  fetchTwitterEmail(twitterSession);

                        twitterAuthClient.requestEmail(twitterSession, new Callback<String>() {
                            @Override
                            public void success(Result<String> result) {
                                //here it will give u only email and rest of other information u can get from TwitterSession
                                Log.d("response_final", "User Id : " + twitterSession.getUserId() + "\nScreen Name : " + twitterSession.getUserName() + "\nEmail Id : " + result.data);
                            }

                            @Override
                            public void failure(TwitterException exception) {
                                Toast.makeText(RegistrationActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
                            }
                        });


                        Utils.showCustomProgressDialog(RegistrationActivity.this, false);
                        twitterAuthClient.requestEmail(result1.data, new com.twitter.sdk.android.core
                                .Callback<String>() {

                            @Override
                            public void success(Result<String> result2) {
                                Utils.hideCustomProgressDialog();


                                updateUiForSocialLogin(result2.data,
                                        String.valueOf(result1.data.getUserId()),
                                        result1.data.getUserName(), "", null);

                            }

                            @Override
                            public void failure(TwitterException exception) {
                                Utils.hideCustomProgressDialog();
                                AppLog.handleException("TWITTER_LOGIN", exception);
                            }
                        });
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        Utils.hideCustomProgressDialog();
                        AppLog.handleException("TWITTER_LOGIN", exception);
                    }
                });
            }
        } catch (Exception e) {
            Log.e("loginWithTwitter() " , e.getMessage());
        }
    }


    public void LoginWithApple() {

        String TAG = "apple";
        OAuthProvider.Builder provider = OAuthProvider.newBuilder("apple.com");
        List<String> scopes =
                new ArrayList<String>() {
                    {
                        add("email");
                        add("name");
                    }
                };
        provider.setScopes(scopes);
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        Task<AuthResult> pending = firebaseAuth.getPendingAuthResult();
        if (pending != null) {
            pending.addOnSuccessListener(authResult -> {
                Log.e(TAG, "checkPending:onSuccess:" + authResult.getUser().getDisplayName());
// Get the user profile with authResult.getUser() and
// authResult.getAdditionalUserInfo(), and the ID
// token from Apple with authResult.getCredential().
                updateUiForSocialLogin(authResult.getUser().getEmail(), authResult.getUser().getUid(), authResult.getUser().getDisplayName(), "", null);
            }).addOnFailureListener(e -> Log.e(TAG, "checkPending:onFailure", e));
        } else {
            Log.d(TAG, "pending: null");

            firebaseAuth.startActivityForSignInWithProvider(this, provider.build())
                    .addOnSuccessListener(
                            authResult -> {
// Sign-in successful!
                                Log.e(TAG, "activitySignIn:onSuccess:" + authResult.getUser());
                                FirebaseUser user = authResult.getUser();
                                Log.e(TAG, "user.getDisplayName()" + user.getDisplayName());
                                Log.e(TAG, "user.getEmail()" + user.getEmail());
                                Log.e(TAG, "user.getuserid()" + user.getUid());
                                updateUiForSocialLogin(user.getEmail(), user.getUid(), user.getDisplayName(), "", null);

                            })
                    .addOnFailureListener(
                            e -> Log.e(TAG, "activitySignIn:onFailure", e));
        }
    }


    private void checkEmailInserver(String email) {
        Utils.showCustomProgressDialog(this, false);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.Params.EMAIL, email);
            jsonObject.put(Const.Params.SOCIAL_ID, socialId);
            jsonObject.put(Const.Params.PHONE, "");

        } catch (JSONException e) {
            AppLog.handleException(PaymentActivity.class.getName(), e);
        }
        Log.i("checkEmailInserver", jsonObject.toString());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<EmailCheck> responseCall = apiInterface.checkemailonserver(ApiClient
                .makeJSONRequestBody(jsonObject));

        responseCall.enqueue(new retrofit2.Callback<EmailCheck>() {
            @Override
            public void onResponse(Call<EmailCheck> call, Response<EmailCheck> response) {
                Utils.hideCustomProgressDialog();
                AppLog.Log("checkEmailInserver_responce", ApiClient.JSONResponse(response.body()));
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
                        GotoGetMobilenumbur();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), RegistrationActivity.this);
                    }
                }


            }

            @Override
            public void onFailure(Call<EmailCheck> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
                Utils.hideCustomProgressDialog();
            }
        });

    }

    private void GoForRegistration() {
        FBA_TYPE = Const.Params.Normal_Email;
        registartionData = new RegistartionData();
        registartionData.setFirstname(etRegisterFirstName.getText().toString());
        registartionData.setLastname(etRegisterLastName.getText().toString());
        registartionData.setEmail(etRegisterEmail.getText().toString());
        registartionData.setPassword(etRegisterPasswordRetype.getText().toString());
        registartionData.setReferalCode(etReferrelCode.getText().toString());
        registartionData.setSocialType(FBA_TYPE);
        checkEmailInserver(etRegisterEmail.getText().toString());
    }

    private void GotoGetMobilenumbur() {

        if (getCallingActivity() != null) {
            Log.i("LoginActivityget", "from checkout going get mobilenumbuer screen");
            Intent intent = new Intent(getApplicationContext(), GetMobileNo.class);

            intent.putExtra(Const.Params.Registartion_data, registartionData);
            startActivityForResult(intent, Const.LOGIN_REQUEST);
        } else {
            Intent intent = new Intent(getApplicationContext(), GetMobileNo.class);

            intent.putExtra(Const.Params.Registartion_data, registartionData);
            startActivity(intent);
        }

    }

    public void setClickableString() {
        SpannableString SpanString = new SpannableString(
                getString(R.string.txt_sign_in_privacy));

        ClickableSpan teremsAndCondition = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent mIntent = new Intent(RegistrationActivity.this, WebViewActivity.class);
                mIntent.putExtra("web", "terms");
                startActivity(mIntent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.linkColor = ContextCompat.getColor(getApplicationContext(), R.color.color_white);
                ds.setUnderlineText(false);
            }

        };

// Character starting from 32 - 45 is Terms and condition.
// Character starting from 49 - 63 is privacy policy.

        ClickableSpan privacy = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

                Intent mIntent = new Intent(RegistrationActivity.this, WebViewActivity.class);
                mIntent.putExtra("web", "privacy");
                startActivity(mIntent);

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.linkColor = ContextCompat.getColor(getApplicationContext(), R.color.color_white);
                ds.setUnderlineText(false);
            }
        };

        int start_terms = getString(R.string.txt_sign_in_privacy).indexOf(getString(R.string.text_t_and_c));
        int end_terms = getString(R.string.txt_sign_in_privacy).indexOf(getString(R.string.text_t_and_c)) + getString(R.string.text_t_and_c).length();
        int start_privacy = getString(R.string.txt_sign_in_privacy).indexOf(getString(R.string.text_policy));
        int end_privacy = getString(R.string.txt_sign_in_privacy).indexOf(getString(R.string.text_policy)) + getString(R.string.text_policy).length();
        SpanString.setSpan(teremsAndCondition, start_terms, end_terms, 0);
        SpanString.setSpan(privacy, start_privacy, end_privacy, 0);
        SpanString.setSpan(new UnderlineSpan(), start_terms, end_terms, 0);
        SpanString.setSpan(new UnderlineSpan(), start_privacy, end_privacy, 0);
        SpanString.setSpan(new StyleSpan(Typeface.BOLD), start_terms, end_terms, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        SpanString.setSpan(new StyleSpan(Typeface.BOLD), start_privacy, end_privacy, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        tvPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        tvPolicy.setText(SpanString, TextView.BufferType.SPANNABLE);
        tvPolicy.setHighlightColor(Color.TRANSPARENT);
        tvPolicy.setSelected(true);
    }
}

