package com.edelivery;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import android.widget.*;

import com.edelivery.adapter.FavAddressAdapter;
import com.edelivery.models.ReferralResponse;
import com.edelivery.models.datamodels.Invoice;
import com.edelivery.models.hereresponce.HereDistanceResponce;
import com.edelivery.parser.ParseContent;
import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;

import com.edelivery.adapter.InvoiceAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomFavAddressDialog;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontCheckBox;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.Addresses;
import com.edelivery.models.datamodels.CartOrder;
import com.edelivery.models.datamodels.CartProductItems;
import com.edelivery.models.datamodels.CartProducts;
import com.edelivery.models.datamodels.CartUserDetail;
import com.edelivery.models.datamodels.OrderPayment;
import com.edelivery.models.datamodels.Specifications;
import com.edelivery.models.datamodels.Store;
import com.edelivery.models.datamodels.StoreClosedResult;
import com.edelivery.models.datamodels.StoreTime;
import com.edelivery.models.responsemodels.AddCartResponse;
import com.edelivery.models.responsemodels.AvailableProviderResponse;
import com.edelivery.models.responsemodels.FavouriteAddressResponse;
import com.edelivery.models.responsemodels.InvoiceResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.OtpResponse;
import com.edelivery.models.responsemodels.UserDataResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.google.android.gms.maps.model.LatLng;

import com.google.gson.Gson;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.component.CustomFavAddressDialog.rcvCountryCode;

//CheckoutActivity
public class CheckoutActivity extends BaseAppCompatActivity implements TextView
        .OnEditorActionListener {


    private CustomFontEditTextView etCustomerName, etCustomerMobile, etCustomerDeliveryAddress,
            etDeliveryAddressNote, etPromoCode, etCustomerCountryCode;
    private CustomFontButton btnPlaceOrder;
    private CustomFontTextViewTitle tvInvoiceOderTotal;
    private CustomFontTextView tvPromoCodeApply, tvReopenAt, tvEditDetail;
    private int totalItemCount = 0;
    private int totalSpecificationCount = 0;
    private double totalItemPriceWithQuantity = 0;
    private double totalSpecificationPriceWithQuantity = 0;
    private RecyclerView rcvInvoice;
    private CustomDialogVerification customDialogVerification;
    private Dialog dialogEmailOrPhoneVerification;
    private CustomFontEditTextView etDialogEditTextOne, etDialogEditTextTwo;
    private OtpResponse otpResponse;
    private String phone, email;
    private ImageView ivDeliveryLocation, ivFreeShipping, ivConfirmDetail;
    private CustomFontTextViewTitle tvScheduleDate, tvScheduleTime;
    private CustomFontTextView ivFavAddress, tvReferralBonusAmount, tvReferralApplyBtn;
    private LinearLayout llAsps, llScheduleDate, llScheduleOrder;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private List<StoreTime> storeTimes = new ArrayList<>();
    private String serverTime, timeZone;
    private CustomFontCheckBox cbSelfDelivery, cb_referral;
    private CustomFontCheckBox cbAsps, cbScheduleOrder;
    private LinearLayout llSelfPickupDelivery;
    private boolean isApplicationStart;
    private ArrayList<Addresses> favAddressList = new ArrayList<>();
    Boolean isPromoApplied = false;
    Boolean isReferralApplied = false;
    ArrayList<Invoice> invoices = new ArrayList<>();
    ;
    private CustomFavAddressDialog customFavAddressDialog;
    private boolean adfavoriteadress = false;

    //for here map
    private AndroidXMapFragment mapFragment = null;
    private Map map = null;
    InvoiceAdapter invoiceAdapter;

    // permissions request code
    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    /**
     * Permissions that need to be explicitly requested from end user.
     */
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();

        Log.i("currentBooking", "-------------oncreate-------Address is-----" + currentBooking.getDeliveryAddress() +
                "\n ----------Latlng is-------" + currentBooking.getDeliveryLatLng());
        setTitleOnToolBar(getResources().getString(R.string.text_checkout));
        getUserDetail();
        findViewById();
        setViewListener();
        /*checkPermissions();*/
        loadCheckOutData();


        favAddressList = new ArrayList<>();
        if (isCurrentLogin()) {
            // getFavAddressList();
            ivFavAddress.setVisibility(View.VISIBLE);
        } else {
            ivFavAddress.setVisibility(View.GONE);

        }


        //getExtraData();
    }

    @Override
    protected boolean isValidate() {
        String msg = null;
        if (TextUtils.isEmpty(etCustomerName.getText().toString().trim())) {
            msg = getString(R.string.msg_please_enter_valid_name);
            etCustomerName.setError(msg);
            etCustomerName.requestFocus();
        } else if (!Patterns.PHONE.matcher(etCustomerMobile.getText().toString().trim())
                .matches()) {
            msg = getString(R.string.msg_please_enter_valid_mobile_number);
            etCustomerMobile.setError(msg);
            etCustomerMobile.requestFocus();
        } else if (TextUtils.isEmpty(etCustomerDeliveryAddress.getText().toString())) {
            msg = getString(R.string.msg_plz_enter_valid_place_address);
            etCustomerDeliveryAddress.setError(msg);
            etCustomerDeliveryAddress.requestFocus();
        }

        return TextUtils.isEmpty(msg);
    }

    @Override
    protected void findViewById() {
        // do somethings
        etCustomerName = (CustomFontEditTextView) findViewById(R.id.etCustomerName);
        etCustomerMobile = (CustomFontEditTextView) findViewById(R.id.etCustomerMobile);
        etCustomerDeliveryAddress = (CustomFontEditTextView) findViewById(R.id
                .etCustomerDeliveryAddress);
        etDeliveryAddressNote = (CustomFontEditTextView) findViewById(R.id.etDeliveryAddressNote);
        btnPlaceOrder = (CustomFontButton) findViewById(R.id.btnPlaceOrder);
        tvInvoiceOderTotal = (CustomFontTextViewTitle) findViewById(R.id.tvInvoiceOderTotal);
        rcvInvoice = (RecyclerView) findViewById(R.id.rcvInvoice);
        etPromoCode = (CustomFontEditTextView) findViewById(R.id.etPromoCode);
        tvPromoCodeApply = (CustomFontTextView) findViewById(R.id.tvPromoCodeApply);
        ivDeliveryLocation = (ImageView) findViewById(R.id.ivDeliveryLocation);
        tvReopenAt = (CustomFontTextView) findViewById(R.id.tvReopenAt);
        ivFreeShipping = (ImageView) findViewById(R.id.ivFreeShipping);
        cbSelfDelivery = (CustomFontCheckBox) findViewById(R.id.cbSelfDelivery);
        cb_referral = (CustomFontCheckBox) findViewById(R.id.cb_referral);
        CustomFontTextView tag2 = (CustomFontTextView) findViewById(R.id.tag2);
        CustomFontTextView tag1 = (CustomFontTextView) findViewById(R.id.tag1);
        Utils.setTagBackgroundRtlView(this, tag1);
        Utils.setTagBackgroundRtlView(this, tag2);
        llAsps = (LinearLayout) findViewById(R.id.llAsps);
        llScheduleDate = (LinearLayout) findViewById(R.id.llScheduleDate);
        llScheduleOrder = (LinearLayout) findViewById(R.id.llScheduleOrder);
        tvScheduleDate = (CustomFontTextViewTitle) findViewById(R.id.tvScheduleDate);
        tvScheduleTime = (CustomFontTextViewTitle) findViewById(R.id.tvScheduleTime);
        etCustomerCountryCode = (CustomFontEditTextView) findViewById(R.id.etCustomerCountryCode);
        ivConfirmDetail = (ImageView) findViewById(R.id.ivConfirmDetail);
        cbScheduleOrder = (CustomFontCheckBox) findViewById(R.id.cbScheduleOrder);
        cbAsps = (CustomFontCheckBox) findViewById(R.id.cbAsps);
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(15);
        etCustomerMobile.setFilters(FilterArray);
        llSelfPickupDelivery = findViewById(R.id.llSelfPickupDelivery);
        tvEditDetail = (CustomFontTextView) findViewById(R.id.tvEditDetail);
        ivFavAddress = findViewById(R.id.ivFavAddress);
        tvReferralBonusAmount = findViewById(R.id.tvReferralBonusAmount);
        tvReferralApplyBtn = findViewById(R.id.tvReferralApplyBtn);

        setEnableFiled(true);

    }

    @Override
    protected void setViewListener() {
        // do somethings
        tvPromoCodeApply.setEnabled(isCurrentLogin());
        etPromoCode.setEnabled(isCurrentLogin());
        btnPlaceOrder.setOnClickListener(this);
        tvPromoCodeApply.setOnClickListener(this);
        tvReferralApplyBtn.setOnClickListener(this);
        etPromoCode.setOnEditorActionListener(this);
        ivDeliveryLocation.setOnClickListener(this);
        tvScheduleDate.setOnClickListener(this);
        tvScheduleTime.setOnClickListener(this);
        cbSelfDelivery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkIsPickUpDeliveryByUser(isChecked);
            }
        });
        ivConfirmDetail.setOnClickListener(this);
        cbAsps.setOnClickListener(this);
        cbScheduleOrder.setOnClickListener(this);
        etCustomerDeliveryAddress.setOnClickListener(this);
        etCustomerDeliveryAddress.setClickable(false);
        tvEditDetail.setOnClickListener(this);
        ivFavAddress.setOnClickListener(this);


       /* cb_referral.setOnCheckedChangeListener((buttonView, isChecked) ->
        {

            if (isCurrentLogin()) {
                if (preferenceHelper.getWALLET().equals("0.0") || preferenceHelper.getWALLET().equals("0")) {
                    Utils.showToast(getResources().getString(R.string.msg_not_usable_referral_balance), this);
                    cb_referral.setChecked(false);
                } else {
                    if (isChecked) {
                        if (isPromoApplied) {
                            Utils.showToast(getResources().getString(R.string.msg_plz_cannotapply_referral), this);
                            cb_referral.setChecked(false);
                        } else {

                            applyReferralAmount();
                        }
                    } else {

                        removeReferralAmount();
                    }
                }
            } else {
                cb_referral.setChecked(false);
                Utils.showToast(getResources().getString(R.string.msg_referral_bonus_can_only_applied_after_signing_in), this);

            }


        });*/


    }

    private void setEnableFiled(boolean isEnable) {
        etCustomerName.setFocusableInTouchMode(isEnable);
        etCustomerCountryCode.setFocusableInTouchMode(isEnable);
        etCustomerMobile.setFocusableInTouchMode(isEnable);
        etDeliveryAddressNote.setFocusableInTouchMode(isEnable);
        etCustomerName.setEnabled(isEnable);
        etCustomerCountryCode.setEnabled(isEnable);
        etCustomerMobile.setEnabled(isEnable);
        etDeliveryAddressNote.setEnabled(isEnable);
        ivDeliveryLocation.setEnabled(isEnable);
        ivDeliveryLocation.setEnabled(!cbSelfDelivery.isChecked() && isEnable);
        ivFavAddress.setEnabled(isEnable);
        ivFavAddress.setEnabled(!cbSelfDelivery.isChecked() && isEnable);
        etCustomerDeliveryAddress.setEnabled(!cbSelfDelivery.isChecked() && isEnable);
    }


    private void applyReferralAmount() {

        currentBooking.setBookCityId(preferenceHelper.getBookCityId());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.ORDER_PAYMENT_ID, currentBooking.getOrderPaymentId());
            jsonObject.put(Const.Params.WALLET_PAYMENT, preferenceHelper.getWALLET());

        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<InvoiceResponse> responseCall = apiInterface.applyReferralCode(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<InvoiceResponse>() {
            @Override
            public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse> response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response)) {

                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {


                        if (response.body().isSuccess()) {
                            setReferralApplyToRemoveButton();
                            AppLog.Log("CHECKOUT_INVOICE_REFRERAL", ApiClient.JSONResponse(response.body()));
                            setInvoiceData(response, "ApplyReferral");
                        } else {
                            Log.e("error_occur", getString(R.string.something_went_wrong));
                            //Utils.showErrorToast(response.body().getErrorCode(), CheckoutActivity.this);
                        }


                    }
                } else {
                    /* cb_referral.setChecked(false);*/
                    Utils.showErrorToast(response.body().getErrorCode(), CheckoutActivity.this);

                    //Utils.showToast(getResources().getString(R.string.msg_no_provider_found), CheckoutActivity.this);
                }
            }

            @Override
            public void onFailure(Call<InvoiceResponse> call, Throwable t) {
                /* cb_referral.setChecked(false);*/
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
            }
        });
    }

    public void setReferralApplyToRemoveButton() {


        isReferralApplied = true;

        tvReferralApplyBtn.setText(getString(R.string.remove_bonus));


    }

    private void removeReferralAmount() {

        currentBooking.setBookCityId(preferenceHelper.getBookCityId());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.ORDER_PAYMENT_ID, currentBooking.getOrderPaymentId());
            jsonObject.put(Const.Params.WALLET_PAYMENT, preferenceHelper.getWALLET());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<InvoiceResponse> responseCall = apiInterface.removeReferralCode(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<InvoiceResponse>() {
            @Override
            public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {


                    if (response.body().isSuccess()) {

                        for (int i = 0; i < invoices.size(); i++) {
                            if (invoices.get(i).getTitle().equals(getString(R.string.text_referral_discount))) {
                                invoices.remove(invoices.size() - 1);
                            }
                        }
                        invoiceAdapter.notifyDataSetChanged();

                        setRefferralApplyButton();

                        AppLog.Log("CHECKOUT_INVOICE_REFRERAL", ApiClient.JSONResponse(response.body()));
                        //cb_referral.setChecked(false);
                        setInvoiceData(response, "RemoveReferral");
                    } else {
                        Log.e("error_occur", getString(R.string.something_went_wrong));
                        //Utils.showErrorToast(response.body().getErrorCode(), CheckoutActivity.this);
                    }

                } else {
                    //cb_referral.setChecked(true);
                    Utils.showToast(getResources().getString(R.string.msg_no_provider_found), CheckoutActivity.this);
                }

            }

            @Override
            public void onFailure(Call<InvoiceResponse> call, Throwable t) {
                //cb_referral.setChecked(true);
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
            }
        });
    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();


    }

    public void setRefferralApplyButton() {
        tvReferralApplyBtn.setText(getString(R.string.apply_bonus));
        isReferralApplied = false;
    }


    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.cbAsps:
                updateUiForOrderSelect(false);
                break;
            case R.id.cbScheduleOrder:
                updateUiForOrderSelect(true);
                break;
            case R.id.btnPlaceOrder:
                if (isCurrentLogin()) {

                    if (!TextUtils.isEmpty(etDeliveryAddressNote.getText().toString())) {
                        addItemInServerCart();

                        Log.i("etDeliveryAddressNote", "ok");
                    }

                    checkAvailableProvider();
                } else {
                    goToLoginActivityForResult(this);
                }
                //goToPlaceOrder();
                break;
            case R.id.tvPromoCodeApply:
                callClickApplyPromoCode();

                break;

            case R.id.tvReferralApplyBtn:
                callApplyReferralBonus();

                break;
            case R.id.ivDeliveryLocation:
            case R.id.etCustomerDeliveryAddress:
                //  goToCheckoutDeliveryLocationActivity();
                break;
            case R.id.tvScheduleDate:
                openDatePickerDialog();
                break;
            case R.id.tvScheduleTime:
                openTimePicker();
                break;
            case R.id.tvEditDetail:

            case R.id.btnAddFavAddress:
                goToAddUserAddress();

            case R.id.ivConfirmDetail:
                if (etCustomerName.isEnabled()) {
                    if (isValidate()) {
                        setEnableFiled(true);
                        ivConfirmDetail.setImageDrawable(AppCompatResources.getDrawable(this, R
                                .drawable.ic_edit_black_24dp));
                        ivConfirmDetail.setVisibility(View.GONE);
                        if (!etCustomerDeliveryAddress.getText().toString().isEmpty()) {
                            currentBooking.setDeliveryAddress(etCustomerDeliveryAddress.getText().toString());
                        }
                        /*tvEditDetail.setVisibility(View.VISIBLE)*/
                        ;
                        etPromoCode.requestFocus();
                        // addItemInServerCart();
                    }
                } else {
                    setEnableFiled(true);
                    ivConfirmDetail.setImageDrawable(AppCompatResources.getDrawable(this, R
                            .drawable.ic_check_black_24dp));
                    ivConfirmDetail.setVisibility(View.VISIBLE);
                    tvEditDetail.setVisibility(View.GONE);
                }

                break;
            case R.id.ivFavAddress:
                if (!favAddressList.isEmpty()) {
                    openFavAddressDialog();
                } else {
                    Utils.showToast(getResources().getString(R.string.msg_no_fav_address), this);
                }
                break;
            default:
                // do with default
                break;
        }
    }

    private void callClickApplyPromoCode() {
        if (isCurrentLogin()) {

            if (!cb_referral.isChecked()) {
                if (TextUtils.isEmpty(etPromoCode.getText().toString().trim())) {
                    Utils.showToast(getResources().getString(R.string
                            .msg_plz_enter_valid_promo_code), this);
                } else {

                    if (tvPromoCodeApply.getText().equals(getString(R.string.text_apply))) {
                        if (isReferralApplied/*cb_referral.isChecked()*/) {
                            Utils.showToast(getResources().getString(R.string.msg_plz_cannotapply_promo_code), this);
                            etPromoCode.setText("");
                        } else {
                            Utils.showCustomProgressDialog(this, false);
                            promoApply(etPromoCode.getText().toString().trim());
                        }
                    } else {
                        Utils.showCustomProgressDialog(this, false);
                        promoRemove(etPromoCode.getText().toString().trim());
                    }

                }
            } else {
                Utils.showToast(getResources().getString(R.string.msg_plz_cannotapply_promo_code), this);
            }


        } else {
            Utils.showToast(getResources().getString(R.string
                    .msg_promo_can_apply_after_signin), this);
        }

    }

    private void callApplyReferralBonus() {
        if (isCurrentLogin()) {
            if (preferenceHelper.getWALLET().equals("0.0") || preferenceHelper.getWALLET().equals("0")) {
                Utils.showToast(getResources().getString(R.string.msg_not_usable_referral_balance), this);

            } else {
                if (tvReferralApplyBtn.getText().equals(getString(R.string.apply_bonus))) {
                    if (isPromoApplied) {
                        Utils.showToast(getResources().getString(R.string.msg_plz_cannotapply_referral), this);
                    } else {
                        Utils.showCustomProgressDialog(this, false);
                        applyReferralAmount();
                    }
                } else {
                    Utils.showCustomProgressDialog(this, false);
                    removeReferralAmount();
                }
            }
        } else {
            // cb_referral.setChecked(false);
            Utils.showToast(getResources().getString(R.string.msg_referral_bonus_can_only_applied_after_signing_in), this);

        }

    }


    private void loadCheckOutData() {
        if (isCurrentLogin()) {
            etCustomerName.setText(preferenceHelper.getFirstName() + " " + preferenceHelper
                    .getLastName());
            etCustomerMobile.setText(preferenceHelper
                    .getPhoneNumber());
            btnPlaceOrder.setText(getResources().getString(R.string.text_place_order));
        } else {
            btnPlaceOrder.setText(getResources().getString(R.string.text_login));
        }
        if (currentBooking.getPickupAddresses().size() > 0) {
            etCustomerCountryCode.setText(currentBooking.getPickupAddresses().get(0).getUserDetails()
                    .getCountryPhoneCode());
        }


        if (currentBooking.getDeliveryAddress() != null && !currentBooking.getDeliveryAddress().equals("")) {
            /* etCustomerDeliveryAddress.setText(currentBooking.getDeliveryAddress());*/

        }
        for (CartProducts cartProducts : currentBooking
                .getCartProductWithSelectedSpecificationList()) {

            for (CartProductItems cartProductItems : cartProducts.getItems()) {
                totalItemPriceWithQuantity = totalItemPriceWithQuantity + (cartProductItems
                        .getItemPrice() * cartProductItems
                        .getQuantity());
                totalSpecificationPriceWithQuantity = totalSpecificationPriceWithQuantity +
                        (cartProductItems
                                .getTotalSpecificationPrice() * cartProductItems
                                .getQuantity());
                totalItemCount = totalItemCount + cartProductItems.getQuantity();
                for (Specifications specifications : cartProductItems.getSpecifications()) {
                    totalSpecificationCount = totalSpecificationCount + specifications.getList()
                            .size();
                }
            }

        }

        checkIsPickUpDeliveryByUser(cbSelfDelivery.isChecked());
    }

    private void checkIsPickUpDeliveryByUser(boolean isChecked) {
        if (isChecked) {
            etDeliveryAddressNote.getText().clear();
        }
        etDeliveryAddressNote.setVisibility(isChecked ? View.GONE : View.VISIBLE);
        ivDeliveryLocation.setEnabled(!isChecked && etCustomerName.isEnabled());
        ivFavAddress.setEnabled(!isChecked && etCustomerName.isEnabled());
        if (!currentBooking.getDestinationAddresses().isEmpty() && !isChecked) {
            Log.v("first", "first1");
            getDistanceMatrix();
        } else {
            Log.v("first", "first2");
            getDeliveryInvoice(0, 0);
        }
    }

    /**
     * this method called a webservice for get distance and time witch is provided by Google
     */
    private void getDistanceMatrix() {
        Log.i("getDistanceMatrixC", "ok");

        GeoCoordinate destination, origin;

        destination = new GeoCoordinate(currentBooking.getDeliveryLatLng().latitude, currentBooking.getDeliveryLatLng().longitude);
        origin = new GeoCoordinate(currentBooking.getPickupAddresses().get(0).getLocation().get(0), currentBooking.getPickupAddresses().get(0).getLocation().get(1));

        Log.i("getDistanceMatrixC", "destination" + destination + "," + "origin" + origin);
        if (destination != null && origin != null) {


            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(Const.HERE_API_KEY, getResources().getString(R.string.here_api_key));
            hashMap.put(Const.HERE_WAYPOINT0, getResources().getString(R.string.here_waypoint) + currentBooking.getDeliveryLatLng().latitude + "," + currentBooking.getDeliveryLatLng().longitude);
            hashMap.put(Const.HERE_WAYPOINT1, getResources().getString(R.string.here_waypoint) + currentBooking.getPickupAddresses().get(0).getLocation().get(0) + "," + currentBooking.getPickupAddresses().get(0).getLocation().get(1));
            hashMap.put(Const.HERE_MODE, getResources().getString(R.string.here_mode));
            ApiInterface apiInterface = ApiClient.getClientMapBox().create(ApiInterface.class);

            Call<HereDistanceResponce> call = apiInterface.getHereDistanceMatrix(hashMap);


            Log.i("getDistanceMatrixC", "ok");
            call.enqueue(new Callback<HereDistanceResponce>() {
                @Override
                public void onResponse(Call<HereDistanceResponce> call, Response<HereDistanceResponce> response) {

                    Log.i("getDistanceMatrixC", response.toString());
                    if (response.body() != null) {
                        Log.i("getDistanceMatrixC", response.toString());
                        float timeSecond = 0;
                        double distance = 0;
                        if (response.body().getResponse().getRoute().get(0).getSummary() != null) {

                            if (response.body().getResponse().getRoute().get(0).getSummary().getDistance() != null) {
                                distance = response.body().getResponse().getRoute().get(0).getSummary().getDistance();
                            }
                            if (response.body().getResponse().getRoute().get(0).getSummary().getBaseTime() != null) {
                                timeSecond = response.body().getResponse().getRoute().get(0).getSummary().getBaseTime();
                            }

                            Log.i("currentBooking", "Distance=" + distance + " " + "Time=" +
                                    timeSecond);
                            getDeliveryInvoice((int) timeSecond, distance);


                        }


                    }
                }

                @Override
                public void onFailure(Call<HereDistanceResponce> call, Throwable t) {

                    AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, t);
                    Utils.hideCustomProgressDialog();

                }
            });

        }







       /* Utils.showCustomProgressDialog(CheckoutActivity.this, false);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("sources", "1");
        hashMap.put("annotations", "distance,duration");
        hashMap.put("access_token", getResources().getString(R.string
                .MAP_BOX_ACCESS_TOKEN));
        ApiInterface apiInterface = ApiClient.getClientMapBox().create(ApiInterface.class);

        //swipe latlong
        Log.i("currentBooking", "===== getDistanceMatrix ===== >" + currentBooking.getDeliveryLatLng().longitude + "," + currentBooking.getDeliveryLatLng().latitude);
        Log.i("currentBooking", "===== getDistanceMatrix ===== >" + currentBooking.getPickupAddresses().get(0).getLocation()
                .get(1) + "," + currentBooking.getPickupAddresses().get(0)
                .getLocation()
                .get(0));

        Call<DirectionMatrixResponse> call = apiInterface.getDistanceMatrixApi(currentBooking.getPickupAddresses().get(0).getLocation()
                .get(1) + "," + currentBooking.getPickupAddresses().get(0).getLocation().get(0) + ";" + currentBooking.getDeliveryLatLng().longitude + "," + currentBooking.getDeliveryLatLng().latitude, hashMap);

        //     Call<DirectionMatrixResponse> call = apiInterface.getDistanceMatrixApi("72.56891285768901,23.055019751435523;72.56474049999997,23.0359696", hashMap);

        call.enqueue(new Callback<DirectionMatrixResponse>() {
            @Override
            public void onResponse(Call<DirectionMatrixResponse> call, Response<DirectionMatrixResponse> response) {
                Utils.hideCustomProgressDialog();
                Log.i("getDistanceMatrix","ok");
                Log.i("getDistanceMatrix",response.toString());
                if (response.body() != null) {




                    if (response.message().equalsIgnoreCase("Ok")) {
                        float timeSecond = 0;
                        double distance = 0;

                        if (response.body().getDistances() != null) {
                            distance = Double.parseDouble(String.valueOf(response.body().getDistances().get(0).get(0)));
                        }

                        if (response.body().getDurations() != null) {
                            timeSecond = (float) Double.parseDouble(String.valueOf(response.body().getDurations().get(0).get(0)));
                        }

                        Log.i("currentBooking","Distance=" + distance + " " + "Time=" +
                                timeSecond);
                        getDeliveryInvoice((int) timeSecond, distance);
                    }
                }

            }

            @Override
            public void onFailure(Call<DirectionMatrixResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, t);
                Utils.hideCustomProgressDialog();
            }
        });*/


    }

    /**
     * this method called a webservice to get delivery invoice or bill
     */
    private void getDeliveryInvoice(int timeSeconds, double tripDistance) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.IS_USER_PICK_UP_ORDER, cbSelfDelivery.isChecked());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());

            jsonObject.put(Const.Params.ORDER_TYPE, Const.Type.USER);
            jsonObject.put(Const.Params.STORE_ID, currentBooking
                    .getSelectedStoreId());
            jsonObject.put(Const.Params.TOTAL_ITEM_COUNT, totalItemCount);
            jsonObject.put(Const.Params.TOTAL_CART_PRICE, currentBooking
                    .getTotalCartAmount());
            jsonObject.put(Const.Params.TOTAL_ITEM_PRICE, totalItemPriceWithQuantity);
            jsonObject.put(Const.Params.TOTAL_SPECIFICATION_PRICE,
                    totalSpecificationPriceWithQuantity);
            jsonObject.put(Const.Params.TOTAL_DISTANCE, tripDistance);
            jsonObject.put(Const.Params.TOTAL_TIME, timeSeconds);
            jsonObject.put(Const.Params.TOTAL_TIME_SECOND, timeSeconds);

            jsonObject.put(Const.Params.TOTAL_SPECIFICATION_COUNT, totalSpecificationCount);
            if (isCurrentLogin()) {
                jsonObject.put(Const.Params.USER_ID, preferenceHelper
                        .getUserId());
            } else {
                jsonObject.put(Const.Params.CART_UNIQUE_TOKEN, preferenceHelper.getAndroidId());
            }


        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, e);
        }


        Log.i("getDeliveryInvoice", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<InvoiceResponse> responseCall = apiInterface.getDeliveryInvoice(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<InvoiceResponse>() {
            @Override
            public void onResponse(Call<InvoiceResponse> call, final Response<InvoiceResponse>
                    response) {

                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    setInvoiceData(response, "MainAPI");
                   /* Log.i("getDeliveryInvoice_responce", new Gson().toJson(response.body()));*/
                    //Utils.showMessageToast(response.body().getMessage(), CheckoutActivity.this);
                    storeTimes.clear();
                    storeTimes.addAll(response.body().getStore().getStoreTime());
                    serverTime = response.body().getServerTime();
                    timeZone = response.body().getTimezone();
                    updateUIWhenStoreClosed(Utils.checkStoreOpenAndClosed(CheckoutActivity
                                    .this, storeTimes, serverTime, "" /*currentBooking
                                        .getTimeZone()*/, currentBooking
                                    .isFutureOrder(),
                            currentBooking.getFutureOrderSelectedTimeUTCMillis()));
                    if (response.body().getOrderPayment().isStorePayDeliveryFees()) {
                        ivFreeShipping.setVisibility(View.VISIBLE);
                    } else {
                        ivFreeShipping.setVisibility(View.GONE);
                    }

                    Store store = response.body().getStore();
                    currentBooking.setStoreLocation(store.getLocation());
                    llSelfPickupDelivery.setVisibility(store.isProvidePickupDelivery() ? View
                            .VISIBLE : View.GONE);
                    if (store.isTakingScheduleOrder()) {
                        updateUiForOrderSelect(currentBooking.isFutureOrder());
                    } else {
                        updateUiForOrderSelect(false);
                        llScheduleOrder.setVisibility(View.GONE);
                    }
                    if (store.isProvideDeliveryAnywhere()
                            || cbSelfDelivery.isChecked()) {

                    } else {
                        Log.i("currentBooking", "===== response unsuccessfully ==== " +
                                currentBooking.getDeliveryLatLng());

                        if (store.getDeliveryRadius() < Utils.getDistance(new LatLng(store
                                        .getLocation().get(0), store.getLocation().get(1)),
                                currentBooking.getDeliveryLatLng(), response.body()
                                        .getOrderPayment().isDistanceUnitMile())) {
                            //String message = getResources().getString(R.string.msg_delivery_within_radius, String.valueOf(store.getDeliveryRadius()));
                            String message = getString(R.string.error_code_966);
                            CustomDialogAlert customDialogAlert = new CustomDialogAlert
                                    (CheckoutActivity.this, getResources().getString(R.string
                                            .text_attention), message, getResources()
                                            .getString(R
                                                    .string
                                                    .text_change_address), getResources()
                                            .getString(R
                                                    .string.text_i_ll_pickup)) {
                                @Override
                                public void onClickLeftButton() {
                                    dismiss();
                                    setEnableFiled(true);
                                    tvEditDetail.setVisibility(View.GONE);
                                    ivConfirmDetail.setImageDrawable(AppCompatResources
                                            .getDrawable(CheckoutActivity.this, R
                                                    .drawable.ic_check_black_24dp));
                                }

                                @Override
                                public void onClickRightButton() {
                                    dismiss();
                                    cbSelfDelivery.setChecked(true);

                                }
                            };
                            customDialogAlert.show();
                            customDialogAlert.btnDialogEditTextRight.setVisibility(store
                                    .isProvidePickupDelivery()
                                    ? View
                                    .VISIBLE : View.GONE);
                        }
                    }

                } else
                    {
                    btnPlaceOrder.setVisibility(View.GONE);
                    DecimalFormat format = new DecimalFormat("##.00");
                    if (Const.MINIMUM_ORDER_AMOUNT == response.body().getErrorCode()) {
                        String message = getResources().getString(R.string
                                .msg_minimum_order_amount) + " " + currentBooking
                                .getCartCurrency()
                                + format.format(response.body().getMinOrderPrice()) + getResources().getString(R.string.msg_minimum_order_amount_two);
                        CustomDialogAlert customDialogAlert = new CustomDialogAlert
                                (CheckoutActivity.this, "", message, "", getResources().getString(R
                                        .string.text_add_more_item)) {
                            @Override
                            public void onClickLeftButton() {

                            }

                            @Override
                            public void onClickRightButton() {
                                dismiss();
                                CheckoutActivity.this.onBackPressed();

                            }
                        };
                        customDialogAlert.show();

                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), CheckoutActivity
                                .this);
                    }


                }
            }


            @Override
            public void onFailure(Call<InvoiceResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, t);
            }
        });

    }

    private void updateUIWhenStoreClosed(StoreClosedResult storeClosedResult) {
        if (storeClosedResult.isStoreClosed()) {
            btnPlaceOrder.setVisibility(View.GONE);
            tvReopenAt.setVisibility(View.VISIBLE);
            tvReopenAt.setText(storeClosedResult.getReOpenAt());
        } else {
            btnPlaceOrder.setVisibility(View.VISIBLE);
            tvReopenAt.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void setInvoiceData(Response<InvoiceResponse> response, String isFrom) {

        OrderPayment orderPayment = response.body().getOrderPayment();
        String currency = currentBooking.getCartCurrency();

        if (isFrom.equals("MainAPI")) {

            rcvInvoice.setLayoutManager(new LinearLayoutManager(this));
            rcvInvoice.setNestedScrollingEnabled(false);

            invoices = parseContent.parseInvoice(orderPayment, currentBooking.getCartCurrency(), true);
            invoiceAdapter = new InvoiceAdapter(invoices);
            rcvInvoice.setAdapter(invoiceAdapter);


            setPromoButtonForApply();
            setRefferralApplyButton();
        }

        if (isFrom.equals("ApplyReferral")) {
            for (int i = 0; i < invoices.size(); i++) {
                if (invoices.get(i).getTitle().equals(getString(R.string.text_referral_discount))) {
                    invoices.remove(invoices.size() - 1);
                }
            }
            invoices.add(parseContent.loadInvoiceData(getResources().getString(R.string.text_referral_discount), orderPayment.getWalletPayment(), currentBooking.getCartCurrency(), 0.0, "", 0.0, ""));
            invoiceAdapter.notifyDataSetChanged();
        }

        if (isFrom.equals("PromoApply")) {
            for (int i = 0; i < invoices.size(); i++) {
                if (invoices.get(i).getTitle().equals(getString(R.string.text_promo_discount))) {
                    invoices.remove(invoices.size() - 1);
                }
            }
            invoices.add(parseContent.loadInvoiceData(getResources().getString(R.string.text_promo_discount), orderPayment.getPromoPayment(), currentBooking.getCartCurrency(), 0.0, "", 0.0, ""));
            invoiceAdapter.notifyDataSetChanged();
        }

        //setReferalWalletAmount(response.body().getOrderPayment().getWalletPayment());

        /*rcvInvoice.setAdapter(new InvoiceAdapter(parseContent.parseInvoice(orderPayment,
                currentBooking.getCartCurrency(), true)));*/


        String str_total_orderTime = String.valueOf(orderPayment.getUserPayPayment());
        if (str_total_orderTime != null && !str_total_orderTime.equals("")) {
            String str_currencydata;
            if (currency != null && !currency.equals("")) {
                //  str_currencydata=currency;
                if (currency.toUpperCase().contains("NULL")) {
                    str_currencydata = currency.toUpperCase().replace("NULL", "");
                } else {
                    str_currencydata = currency;
                }
            } else {
                str_currencydata = "";
            }
            Log.i("currentBooking", "setInvoiceData-----------tvInvoiceOderTotal------" + parseContent.decimalTwoDigitFormat.format
                    (orderPayment.getUserPayPayment()));

            tvInvoiceOderTotal.setText(currentBooking.getCartCurrency() + " " + parseContent.decimalTwoDigitFormat.format
                    (orderPayment.getUserPayPayment()));


          /*  tvInvoiceOderTotal.setText(str_currencydata + parseContent.decimalTwoDigitFormat.format
                    (orderPayment.getTotal()));*/

            Log.i("currentBooking", "setInvoiceData-----------setTotalInvoiceAmount------" + orderPayment.getUserPayPayment());
            currentBooking.setTotalInvoiceAmount(orderPayment.getUserPayPayment());
            currentBooking.setCurrency(str_currencydata);
            currentBooking.setCurrencyCode(orderPayment.getOrderCurrencyCode());
        }
    }

    /**
     * this method called a webservice when promo code is apply
     *
     * @param promoCode set by user
     */
    private void promoApply(String promoCode) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper
                    .getUserId());
            jsonObject.put(Const.Params.ORDER_PAYMENT_ID, currentBooking.getOrderPaymentId());
            jsonObject.put(Const.Params.PROMO_CODE_NAME, promoCode);

        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, e);
        }
        Log.i("promoApply", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<InvoiceResponse> responseCall = apiInterface.applyPromoCode(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<InvoiceResponse>() {
            @Override
            public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse> response) {
                Utils.hideCustomProgressDialog();
                AppLog.Log("promoApply_responce", ApiClient.JSONResponse(response.body()));
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {


                    if (response.body().isSuccess()) {
                        AppLog.Log("CHECKOUT_INVOICE_PROMO", ApiClient.JSONResponse(response.body()));
                        setInvoiceData(response, "PromoApply");
                        isPromoApplied = true;
                        etPromoCode.setEnabled(false);
                        tvPromoCodeApply.setText(getString(R.string.text_remove));
                        Utils.showMessageToast(response.body().getMessage(), CheckoutActivity.this);
                    } else {
                        Log.e("error_occur", getString(R.string.something_went_wrong));
                        //Utils.showErrorToast(response.body().getErrorCode(), CheckoutActivity.this);
                    }


                } else {

                    Utils.showErrorToast(response.body().getErrorCode(), CheckoutActivity.this);
                }

            }

            @Override
            public void onFailure(Call<InvoiceResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, t);
            }
        });
    }

    private void promoRemove(String promoCode) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.ORDER_PAYMENT_ID, currentBooking.getOrderPaymentId());
            jsonObject.put(Const.Params.PROMO_CODE_NAME, promoCode);

        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<InvoiceResponse> responseCall = apiInterface.removePromoCode(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<InvoiceResponse>() {
            @Override
            public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse> response) {
                Utils.hideCustomProgressDialog();

                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {


                    if (response.body().isSuccess()) {

                        for (int i = 0; i < invoices.size(); i++) {
                            if (invoices.get(i).getTitle().equals(getString(R.string.text_promo_discount))) {
                                invoices.remove(invoices.size() - 1);
                            }
                        }

                        invoiceAdapter.notifyDataSetChanged();
                        AppLog.Log("CHECKOUT_INVOICE_PROMO", ApiClient.JSONResponse(response.body()));
                        setInvoiceData(response, "PromoRemove");
                        setPromoButtonForApply();
                        //Utils.showMessageToast(response.body().getMessage(), CheckoutActivity.this);
                    } else {

                    }

                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), CheckoutActivity.this);
                }
            }

            @Override
            public void onFailure(Call<InvoiceResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, t);
            }
        });
    }

    public void setPromoButtonForApply() {
        isPromoApplied = false;
        etPromoCode.setEnabled(true);
        etPromoCode.setText("");
        tvPromoCodeApply.setText(getString(R.string.text_apply));
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        switch (textView.getId()) {
            case R.id.etPromoCode:
                if (i == EditorInfo.IME_ACTION_DONE) {
                   /* if (TextUtils.isEmpty(etPromoCode.getText().toString().trim())) {
                        Utils.showToast(getResources().getString(R.string
                                .msg_plz_enter_valid_promo_code), this);
                    } else {
                        promoApply(etPromoCode.getText().toString().trim());
                    }*/
                    return true;
                }
                break;
            default:
                //Do som thing
                break;
        }

        return false;
    }

    private void goToCheckoutDeliveryLocationActivity() {
        Intent intent = new Intent(this, CheckoutDeliveryLocationActivity.class);
        startActivityForResult(intent, Const.DELIVERY_LIST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i("onActivityResult", "ok");
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Const.DELIVERY_LIST_CODE:

                    if (!currentBooking.getDestinationAddresses().isEmpty()) {
                        btnPlaceOrder.setEnabled(true);
                        btnPlaceOrder.setBackgroundColor(getResources().getColor(R.color.color_black));
                        etCustomerDeliveryAddress.setText(ParseContent.getInstance().address);
                        if (ParseContent.getInstance().latLng != null) {
                            Log.i("currentBooking", "--------onActivityResult-------" + ParseContent.getInstance().latLng);
                            currentBooking.setDeliveryLatLng(ParseContent.getInstance().latLng);
                        }
                        /*if (cbSelfDelivery.isChecked()) {
                           // getDeliveryInvoice(0, 0);
                            getDistanceMatrix();

                        } else {
                        }*/

                        setEnableFiled(true);
                        ivConfirmDetail.setImageDrawable(AppCompatResources.getDrawable(this, R
                                .drawable.ic_edit_black_24dp));
                        ivConfirmDetail.setVisibility(View.GONE);
                        if (!etCustomerDeliveryAddress.getText().toString().isEmpty()) {
                            currentBooking.setDeliveryAddress(etCustomerDeliveryAddress.getText().toString());
                        }
                        /*   tvEditDetail.setVisibility(View.VISIBLE);*/
                        etPromoCode.requestFocus();
                        // addItemInServerCart();
                    }
                    break;
                case Const.LOGIN_REQUEST:
                    Log.i("LOGIN_REQUEST", "ok");
                    loadCheckOutData();
                    ivFavAddress.setVisibility(View.VISIBLE);
                    getFavAddressList();
                    // addItemInServerCart();
                    //checkAvailableProvider();

                    btnPlaceOrder.setText(getResources().getString(R.string.text_place_order));


                    break;

                case Const.STORE_LOCATION_RESULT_CHECKOUT:
                    adfavoriteadress = true;
                    customFavAddressDialog.dismiss();
                    getFavAddressList();


//                   etCustomerDeliveryAddress.setText(favAddressList.get(0).getAddress());
                  /*   currentBooking.setDeliveryAddress(favAddressList.get(0).getAddress());
                    LatLng favLatLng = new LatLng(favAddressList.get(0).getLocation().get(0),
                            favAddressList.get(0).getLocation().get(1));
                    changeDeliveryAddressAvailable(favLatLng, favAddressList.get(0).getAddress());*/

                    break;

                case Const.STORE_LOCATION_RESULT_CHECKOUT_NEWUSER:
                    adfavoriteadress = true;
                    getFavAddressList();
                    break;
                case Const.DOCUMENT_REQUEST:
                    btnPlaceOrder.setText(getResources().getString(R.string.text_place_order));
                    tvPromoCodeApply.setEnabled(isCurrentLogin());
                    etPromoCode.setEnabled(isCurrentLogin());
                    if (isCurrentLogin()) {
                        getFavAddressList();
                        ivFavAddress.setVisibility(View.VISIBLE);
                    } else {
                        ivFavAddress.setVisibility(View.GONE);
                    }
                    getUserDetail();
                    break;

                default:
                    // do with default
                    break;
            }
        }


    }

    /**
     * this method called a webservice for get user detail
     */
    private void getUserDetail() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.USER_ID,
                    preferenceHelper.getUserId());
            jsonObject.put(Const.Params.SERVER_TOKEN,
                    preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.APP_VERSION, getAppVersion());
            jsonObject.put(Const.Params.CART_UNIQUE_TOKEN, preferenceHelper.getAndroidId());


        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserDataResponse> responseCall = apiInterface.getUserDetail(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<UserDataResponse>() {
            @Override
            public void onResponse(Call<UserDataResponse> call, Response<UserDataResponse
                    > response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && parseContent.parseUserStorageData(response)) {
                    etCustomerName.setText(preferenceHelper.getFirstName() + " " +
                            preferenceHelper.getLastName());
                    etCustomerMobile.setText(preferenceHelper.getPhoneCountyCodeCode() +
                            preferenceHelper
                                    .getPhoneNumber());
                   /* cb_referral.setText(getString(R.string.apply_bonus) + ":- " + currentBooking.getCurrency() + " "
                            + parseContent.decimalTwoDigitFormat.format(Double.parseDouble(response.body().getUser().getWallet())));
*/
                    tvReferralBonusAmount.setText(getString(R.string.referral_bonus_capital) + "- " + "£" + " "
                            + parseContent.decimalTwoDigitFormat.format(Double.parseDouble(response.body().getUser().getWallet())));
                    //checkDocumentUploadAndApproved();
                }

            }

            @Override
            public void onFailure(Call<UserDataResponse> call, Throwable t) {

                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(CheckoutActivity.class.getName(), t);
            }
        });
    }

    private void checkDocumentUploadAndApproved() {


        if (preferenceHelper.getIsApproved()) {
            closedAdminApprovedDialog();
            if (preferenceHelper.getIsAdminDocumentMandatory() && !preferenceHelper
                    .getIsUserAllDocumentsUpload()) {
                goToDocumentActivityForResult(this, true);
            } else {
                if (currentBooking.isHaveOrders()) {
                    openEmailOrPhoneConfirmationDialog(getResources().getString(R
                            .string.text_confirm_detail), getResources().getString(R
                            .string.msg_plz_confirm_your_detail), getResources()
                            .getString(R.string
                                    .text_log_out), getResources().getString(R.string
                            .text_ok));
                }

            }

        } else {
            if (preferenceHelper.getIsAdminDocumentMandatory() && !preferenceHelper
                    .getIsUserAllDocumentsUpload()) {
                goToDocumentActivityForResult(this, true);
            } else {
                openAdminApprovedDialog();
            }
        }


    }

    /**
     * this method open dialog which confirm user email or mobile detail
     *
     * @param titleDialog      set dialog title
     * @param messageDialog    set dialog message
     * @param titleLeftButton  set dialog left button text
     * @param titleRightButton set dialog right button text
     */

    private void openEmailOrPhoneConfirmationDialog(String titleDialog, String messageDialog,
                                                    String titleLeftButton, String
                                                            titleRightButton) {
        CustomFontTextView tvDialogEdiTextMessage, tvDialogEditTextTitle, btnDialogEditTextLeft,
                btnDialogEditTextRight;
        TextInputLayout dialogItlOne;
        LinearLayout llConfirmationPhone;
        CustomFontEditTextView etRegisterCountryCode;

        if (customDialogVerification != null && customDialogVerification.isShowing()
                || dialogEmailOrPhoneVerification != null && dialogEmailOrPhoneVerification
                .isShowing()) {
            return;
        }
        dialogEmailOrPhoneVerification = new Dialog(this);
        dialogEmailOrPhoneVerification.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogEmailOrPhoneVerification.setContentView(R.layout.dialog_confrimation_email_or_phone);

        tvDialogEdiTextMessage = (CustomFontTextView) dialogEmailOrPhoneVerification.findViewById
                (R.id
                        .tvDialogAlertMessage);
        tvDialogEditTextTitle = (CustomFontTextView) dialogEmailOrPhoneVerification.findViewById
                (R.id.tvDialogAlertTitle);
        btnDialogEditTextLeft = (CustomFontTextView) dialogEmailOrPhoneVerification.findViewById
                (R.id.btnDialogAlertLeft);
        btnDialogEditTextRight = (CustomFontTextView) dialogEmailOrPhoneVerification.findViewById
                (R.id.btnDialogAlertRight);
        etDialogEditTextOne = (CustomFontEditTextView) dialogEmailOrPhoneVerification
                .findViewById(
                        R.id.etDialogEditTextOne);
        etDialogEditTextTwo = (CustomFontEditTextView) dialogEmailOrPhoneVerification
                .findViewById(R.id
                        .etDialogEditTextTwo);
        etRegisterCountryCode = (CustomFontEditTextView) dialogEmailOrPhoneVerification
                .findViewById(R.id
                        .etRegisterCountryCode);
        etDialogEditTextOne.setText(preferenceHelper.getEmail());
        etDialogEditTextTwo.setText(preferenceHelper.getPhoneNumber());
        etRegisterCountryCode.setText(preferenceHelper.getPhoneCountyCodeCode());

        llConfirmationPhone = (LinearLayout) dialogEmailOrPhoneVerification.findViewById(R.id
                .llConfirmationPhone);
        dialogItlOne = (TextInputLayout) dialogEmailOrPhoneVerification.findViewById(R.id
                .dialogItlOne);

        btnDialogEditTextLeft.setOnClickListener(this);
        btnDialogEditTextRight.setOnClickListener(this);

        tvDialogEditTextTitle.setText(titleDialog);
        tvDialogEdiTextMessage.setText(messageDialog);
        btnDialogEditTextLeft.setText(titleLeftButton);
        btnDialogEditTextRight.setText(titleRightButton);


        btnDialogEditTextRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Const.Params.ID, preferenceHelper.getUserId());
                    jsonObject.put(Const.Params.TYPE, String.valueOf(Const.Type.USER));
                    switch (checkWitchOtpValidationON()) {
                        case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                            if (Patterns.EMAIL_ADDRESS.matcher(etDialogEditTextOne.getText
                                    ().toString()).matches()) {
                                if (etDialogEditTextTwo.getText().toString().trim().length()
                                        > preferenceHelper.getMaxPhoneNumberLength() ||
                                        etDialogEditTextTwo.getText()
                                                .toString().trim()
                                                .length
                                                        () < preferenceHelper
                                                .getMinPhoneNumberLength()) {

                                    etDialogEditTextTwo.setError(getResources().getString(R
                                            .string.msg_please_enter_valid_mobile_number) + " " +
                                            "" + preferenceHelper.getMinPhoneNumberLength() +
                                            getResources().getString(R
                                                    .string
                                                    .text_or)
                                            + preferenceHelper.getMaxPhoneNumberLength() + " " +
                                            getResources().getString
                                                    (R.string
                                                            .text_digits));
                                } else {
                                    jsonObject.put(Const.Params.EMAIL, etDialogEditTextOne
                                            .getText().toString());
                                    jsonObject.put(Const.Params.PHONE, etDialogEditTextTwo.getText
                                            ().toString());
                                    jsonObject.put(Const.Params.COUNTRY_PHONE_CODE, preferenceHelper
                                            .getPhoneCountyCodeCode());
                                    dialogEmailOrPhoneVerification.dismiss();
                                    email = etDialogEditTextOne.getText().toString();
                                    phone = etDialogEditTextTwo.getText().toString();
                                    getOtpVerify(jsonObject);
                                }

                            } else {
                                etDialogEditTextOne.setError(getResources().getString(R.string
                                        .msg_please_enter_valid_email));
                            }
                            break;
                        case Const.SMS_VERIFICATION_ON:
                            if (etDialogEditTextTwo.getText().toString().trim().length()
                                    > preferenceHelper.getMaxPhoneNumberLength() ||
                                    etDialogEditTextTwo.getText()
                                            .toString().trim()
                                            .length
                                                    () < preferenceHelper
                                            .getMinPhoneNumberLength()) {

                                etDialogEditTextTwo.setError(getResources().getString(R
                                        .string.msg_please_enter_valid_mobile_number) + " " +
                                        "" + preferenceHelper.getMinPhoneNumberLength() +
                                        getResources().getString(R
                                                .string
                                                .text_or)
                                        + preferenceHelper.getMaxPhoneNumberLength() + " " +
                                        getResources().getString
                                                (R.string
                                                        .text_digits));
                            } else {
                                jsonObject.put(Const.Params.PHONE, etDialogEditTextTwo.getText
                                        ().toString());
                                jsonObject.put(Const.Params.COUNTRY_PHONE_CODE, preferenceHelper
                                        .getPhoneCountyCodeCode());
                                dialogEmailOrPhoneVerification.dismiss();
                                phone = etDialogEditTextTwo.getText().toString();
                                getOtpVerify(jsonObject);
                            }
                            break;
                        case Const.EMAIL_VERIFICATION_ON:
                            if (Patterns.EMAIL_ADDRESS.matcher(etDialogEditTextOne.getText
                                    ().toString()).matches()) {
                                jsonObject.put(Const.Params.EMAIL, etDialogEditTextOne
                                        .getText().toString());
                                dialogEmailOrPhoneVerification.dismiss();
                                email = etDialogEditTextOne.getText().toString();
                                getOtpVerify(jsonObject);
                            } else {
                                etDialogEditTextOne.setError(getResources().getString(R.string
                                        .msg_please_enter_valid_email));
                            }
                            break;
                        default:
                            // do with default
                            break;
                    }

                } catch (JSONException e) {
                    AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, e);
                }
            }
        });
        btnDialogEditTextLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOut();
                dialogEmailOrPhoneVerification.dismiss();

            }
        });
        WindowManager.LayoutParams params = dialogEmailOrPhoneVerification.getWindow()
                .getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialogEmailOrPhoneVerification.setCancelable(false);

        switch (checkWitchOtpValidationON()) {
            case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                etDialogEditTextOne.setVisibility(View.VISIBLE);
                dialogItlOne.setVisibility(View.VISIBLE);
                llConfirmationPhone.setVisibility(View.VISIBLE);
                dialogEmailOrPhoneVerification.show();
                break;
            case Const.EMAIL_VERIFICATION_ON:
                etDialogEditTextOne.setVisibility(View.VISIBLE);
                dialogItlOne.setVisibility(View.VISIBLE);
                llConfirmationPhone.setVisibility(View.GONE);
                dialogEmailOrPhoneVerification.show();
                break;
            case Const.SMS_VERIFICATION_ON:
                etDialogEditTextOne.setVisibility(View.GONE);
                dialogItlOne.setVisibility(View.GONE);
                llConfirmationPhone.setVisibility(View.VISIBLE);
                dialogEmailOrPhoneVerification.show();
                break;
            default:
                etDialogEditTextOne.setVisibility(View.GONE);
                dialogItlOne.setVisibility(View.GONE);
                llConfirmationPhone.setVisibility(View.GONE);
                break;
        }
    }

    /**
     * this method open dialog which is help to verify OTP witch is send to email or mobile
     *
     * @param otpEmailVerification set email otp number
     * @param otpSmsVerification   set mobile otp number
     * @param editTextOneHint      set hint text in edittext one
     * @param ediTextTwoHint       set hint text in edittext two
     * @param isEditTextOneVisible set true edittext one visible
     */
    private void openEmailOrPhoneOTPVerifyDialog(final String otpEmailVerification, final String
            otpSmsVerification, String editTextOneHint, String ediTextTwoHint, boolean
                                                         isEditTextOneVisible) {

        if (customDialogVerification != null && customDialogVerification.isShowing()) {
            return;
        }

        customDialogVerification = new CustomDialogVerification
                (this,
                        getResources().getString(R.string.text_verify_detail), getResources()
                        .getString(R
                                .string.msg_verify_detail), getResources().getString(R.string
                        .text_log_out)
                        , getResources().getString(R.string.text_ok), editTextOneHint,
                        ediTextTwoHint,
                        isEditTextOneVisible,
                        InputType.TYPE_CLASS_NUMBER, InputType.TYPE_CLASS_NUMBER) {
            @Override
            public void onClickLeftButton() {
                customDialogVerification.dismiss();
                logOut();

            }

            @Override
            public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                           CustomFontEditTextView etDialogEditTextTwo) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Const.Params.USER_ID,
                            preferenceHelper.getUserId());
                    jsonObject.put(Const.Params.SERVER_TOKEN,
                            preferenceHelper.getSessionToken());

                    switch (checkWitchOtpValidationON()) {
                        case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                            if (TextUtils.equals(etDialogEditTextOne.getText().toString(),
                                    otpEmailVerification)) {
                                if (TextUtils.equals(etDialogEditTextTwo
                                                .getText().toString(),
                                        otpSmsVerification)) {
                                    jsonObject.put(Const.Params.IS_PHONE_NUMBER_VERIFIED,
                                            true);
                                    jsonObject.put(Const.Params.IS_EMAIL_VERIFIED,
                                            true);
                                    jsonObject.put(Const.Params.EMAIL, email);
                                    jsonObject.put(Const.Params.PHONE, phone);
                                    customDialogVerification.dismiss();
                                    setOTPVerification(jsonObject);
                                } else {
                                    etDialogEditTextTwo.setError(getResources().getString(R.string
                                            .msg_sms_otp_wrong));
                                }

                            } else {
                                etDialogEditTextOne.setError(getResources().getString(R.string
                                        .msg_email_otp_wrong));
                            }
                            break;
                        case Const.SMS_VERIFICATION_ON:
                            if (TextUtils.equals(etDialogEditTextTwo
                                            .getText().toString(),
                                    otpSmsVerification)) {
                                jsonObject.put(Const.Params.IS_PHONE_NUMBER_VERIFIED,
                                        true);
                                jsonObject.put(Const.Params.PHONE, phone);
                                customDialogVerification.dismiss();
                                setOTPVerification(jsonObject);
                            } else {
                                etDialogEditTextTwo.setError(getResources().getString(R.string
                                        .msg_sms_otp_wrong));
                            }
                            break;
                        case Const.EMAIL_VERIFICATION_ON:
                            if (TextUtils.equals(etDialogEditTextTwo.getText().toString(),
                                    otpEmailVerification)) {
                                jsonObject.put(Const.Params.IS_EMAIL_VERIFIED,
                                        true);
                                jsonObject.put(Const.Params.EMAIL, email);
                                customDialogVerification.dismiss();
                                setOTPVerification(jsonObject);
                            } else {
                                etDialogEditTextTwo.setError(getResources().getString(R.string
                                        .msg_email_otp_wrong));
                            }
                            break;
                        default:
                            // do with default
                            break;
                    }

                } catch (JSONException e) {
                    AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, e);
                }
            }

        };
        customDialogVerification.show();
    }

    /**
     * this method called a webservice for set otp verification result in web
     */
    private void setOTPVerification(JSONObject jsonObject) {
        Utils.showCustomProgressDialog(this, false);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.setOtpVerification(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    preferenceHelper.putIsEmailVerified(response.body().isSuccess());
                    preferenceHelper.putIsPhoneNumberVerified(response.body().isSuccess());
                    preferenceHelper.putEmail(email);
                    preferenceHelper.putPhoneNumber(phone);
                    Utils.showMessageToast(response.body().getMessage(), CheckoutActivity.this);
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), CheckoutActivity.this);
                }


            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(CheckoutActivity.class.getName(), t);
            }
        });
    }

    /**
     * this method called webservice for get OTP for mobile or email
     *
     * @param jsonObject
     */
    private void getOtpVerify(JSONObject jsonObject) {
        Utils.showCustomProgressDialog(this, false);
        if (customDialogVerification != null && customDialogVerification.isShowing()) {
            return;
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OtpResponse> otpResponseCall = apiInterface.getOtpVerify(ApiClient
                .makeJSONRequestBody(jsonObject));
        otpResponseCall.enqueue(new Callback<OtpResponse>() {
            @Override
            public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    otpResponse = response.body();
                    switch (checkWitchOtpValidationON()) {
                        case Const.SMS_AND_EMAIL_VERIFICATION_ON:
                            openEmailOrPhoneOTPVerifyDialog(otpResponse.getOtpForEmail(),
                                    otpResponse
                                            .getOtpForSms(), getResources().getString(R
                                            .string.text_email_otp), getResources().getString
                                            (R.string
                                                    .text_phone_otp), true);
                            break;
                        case Const.SMS_VERIFICATION_ON:
                            openEmailOrPhoneOTPVerifyDialog(otpResponse.getOtpForEmail(),
                                    otpResponse
                                            .getOtpForSms(), "", getResources().getString(R
                                            .string
                                            .text_phone_otp), false);
                            break;
                        case Const.EMAIL_VERIFICATION_ON:
                            openEmailOrPhoneOTPVerifyDialog(otpResponse.getOtpForEmail(),
                                    otpResponse
                                            .getOtpForSms(),
                                    "", getResources().getString(R
                                            .string
                                            .text_email_otp), false);
                            break;
                        default:
                            // do with default
                            break;
                    }


                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), CheckoutActivity.this);
                }


            }

            @Override
            public void onFailure(Call<OtpResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
            }
        });

    }

    private void updateUiForOrderSelect(boolean isUpdate) {
        if (isUpdate) {
            cbScheduleOrder.setChecked(true);
            llScheduleDate.setVisibility(View.VISIBLE);
            cbAsps.setChecked(false);
            if (!TextUtils.isEmpty(currentBooking.getFutureOrderDate())) {
                tvScheduleDate.setText(currentBooking.getFutureOrderDate());
            } else {
                tvScheduleDate.setText(getResources().getString(R.string.text_schedule_a_date));
            }
            if (!TextUtils.isEmpty(currentBooking.getFutureOrderTime())) {
                tvScheduleTime.setText(currentBooking.getFutureOrderTime());
            } else {
                tvScheduleTime.setText(getResources().getString(R.string.text_set_time));
            }

        } else {
            cbAsps.setChecked(true);
            cbScheduleOrder.setChecked(false);
            llScheduleDate.setVisibility(View.GONE);
            currentBooking.setFutureOrder(false);
            currentBooking.setFutureOrderSelectedTimeUTCMillis(0);
            currentBooking.setFutureOrderSelectedTimeZoneMillis(0);
            currentBooking.setFutureOrderTime("");
            currentBooking.setFutureOrderDate("");
        }

    }

    private void openDatePickerDialog() {

        final Calendar calendar = Calendar.getInstance();
        Date trialTime = new Date();
        calendar.setTime(trialTime);
        calendar.setTimeZone(TimeZone.getTimeZone(currentBooking.getTimeZone()));
        final int currentYear = calendar.get(Calendar.YEAR);
        final int currentMonth = calendar.get(Calendar.MONTH);
        final int currentDate = calendar.get(Calendar.DAY_OF_MONTH);

        if (datePickerDialog != null && datePickerDialog.isShowing()) {
            return;
        }

        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog
                .OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            }
        };
        datePickerDialog = new DatePickerDialog(this, onDateSetListener, currentYear,
                currentMonth,
                currentDate);
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, this
                .getResources()
                .getString(R.string.text_select), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                calendar.set(datePickerDialog.getDatePicker().getYear(), datePickerDialog
                        .getDatePicker().getMonth(), datePickerDialog.getDatePicker()
                        .getDayOfMonth());

                currentBooking.setFutureOrderDate(parseContent.dateFormat.format(calendar.getTime
                        ()));
                tvScheduleDate.setText(currentBooking.getFutureOrderDate());
                currentBooking.setFutureOrder(true);
                selectedDateTime();
                updateUIWhenStoreClosed(Utils.checkStoreOpenAndClosed(CheckoutActivity
                                .this, storeTimes, serverTime, currentBooking
                                .getTimeZone(), currentBooking
                                .isFutureOrder(),
                        currentBooking.getFutureOrderSelectedTimeUTCMillis()));
            }
        });
        long now = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMinDate(now - 10000);
        datePickerDialog.show();

    }


    private void openTimePicker() {
        Calendar calendar = Calendar.getInstance();
        Date trialTime = new Date();
        calendar.setTimeZone(TimeZone.getTimeZone(currentBooking.getTimeZone()));
        calendar.setTime(trialTime);
        final int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        final int currentMinute = calendar.get(Calendar.MINUTE);
//        AppLog.Log("hour", currentHour + "");
//        AppLog.Log("minute", currentMinute + "");

        if (timePickerDialog != null && timePickerDialog.isShowing()) {
            return;
        }
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog
                .OnTimeSetListener() {

            int count = 0;

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    //  AppLog.Log("onTimeSet", "onTimeSetCalled");
                    currentBooking.setFutureOrderTime(hourOfDay + ":" + minute);
                    tvScheduleTime.setText(currentBooking.getFutureOrderTime());
                    currentBooking.setFutureOrder(true);
                    selectedDateTime();
                    updateUIWhenStoreClosed(Utils.checkStoreOpenAndClosed(CheckoutActivity
                                    .this, storeTimes, serverTime, currentBooking
                                    .getTimeZone(), currentBooking
                                    .isFutureOrder(),
                            currentBooking.getFutureOrderSelectedTimeUTCMillis()));
                } else {
                    if (count == 0) {
                        //   AppLog.Log("onTimeSet", "onTimeSetCalled");
                        currentBooking.setFutureOrderTime(hourOfDay + ":" + minute);
                        tvScheduleTime.setText(currentBooking.getFutureOrderTime());
                        currentBooking.setFutureOrder(true);
                        selectedDateTime();
                        updateUIWhenStoreClosed(Utils.checkStoreOpenAndClosed(CheckoutActivity
                                        .this, storeTimes, serverTime, currentBooking
                                        .getTimeZone(), currentBooking
                                        .isFutureOrder(),
                                currentBooking.getFutureOrderSelectedTimeUTCMillis()));
                    }
                    count++;
                }


            }

        }, currentHour, currentMinute, true);
        timePickerDialog.show();
    }

    private void selectedDateTime() {

        Calendar calendar = Calendar.getInstance();

        if (!TextUtils.isEmpty(currentBooking.getFutureOrderDate())) {
            try {
                Date date = parseContent.dateFormat.parse(currentBooking.getFutureOrderDate());
                calendar.setTime(date);
            } catch (ParseException e) {
                AppLog.handleException(DeliveryLocationActivity.class.getSimpleName(), e);
            }
        }
        if (!TextUtils.isEmpty(currentBooking.getFutureOrderTime())) {
            String[] strings = currentBooking.getFutureOrderTime().split(":");
            calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(strings[0]));
            calendar.set(Calendar.MINUTE, Integer.valueOf(strings[1]));
        }

        TimeZone timeZone = TimeZone.getTimeZone(CurrentBooking.getInstance().getTimeZone());

        currentBooking.setFutureOrderSelectedTimeZoneMillis(calendar.getTimeInMillis() +
                (timeZone.getOffset
                        (calendar.getTimeInMillis())));

        TimeZone timeZoneUTC = TimeZone.getTimeZone("UTC");

        currentBooking.setFutureOrderSelectedTimeUTCMillis(calendar.getTimeInMillis() +
                (timeZoneUTC.getOffset
                        (calendar.getTimeInMillis())));

      /*  AppLog.Log("TIME_ZONE_MILLI_SECOND", CurrentBooking.getInstance().getTimeZone()
                + "=" + currentBooking.getFutureOrderSelectedTimeZoneMillis());
        AppLog.Log("TIME_ZONE_MILLI_SECOND", "UTC"
                + "=" + currentBooking.getFutureOrderSelectedTimeUTCMillis());*/
    }

    /**
     * this method called a webservice for add item in cart
     */
    private void addItemInServerCart() {


        Log.i("addItemInServerCart", "ok");
        Utils.showCustomProgressDialog(this, false);

        CartOrder cartOrder = new CartOrder();
        cartOrder.setUserType(Const.Type.USER);
        if (isCurrentLogin()) {
            cartOrder.setUserId(preferenceHelper.getUserId());
            cartOrder.setAndroidId("");
        } else {
            cartOrder.setAndroidId(preferenceHelper.getAndroidId());
            cartOrder.setUserId("");
        }
        cartOrder.setServerToken(preferenceHelper.getSessionToken());
        cartOrder.setStoreId(currentBooking.getSelectedStoreId());
        cartOrder.setProducts(currentBooking.getCartProductWithSelectedSpecificationList());

        ArrayList<Addresses> destinationAddresses = new ArrayList<>();
        Addresses addresses = new Addresses();
        addresses.setAddress(currentBooking.getDeliveryAddress());
        addresses.setCity(currentBooking.getCity1());
        addresses.setAddressType(Const.Type.DESTINATION);
        addresses.setNote(etDeliveryAddressNote.getText().toString());
        addresses.setUserType(Const.Type.USER);
        ArrayList<Double> location = new ArrayList<>();
        if (currentBooking.getDeliveryLatLng() != null) {
            location.add(currentBooking.getDeliveryLatLng().latitude);
            location.add(currentBooking.getDeliveryLatLng().longitude);
        }

        addresses.setLocation(location);
        CartUserDetail cartUserDetail = new CartUserDetail();
        cartUserDetail.setEmail(preferenceHelper.getEmail());
        cartUserDetail.setCountryPhoneCode(etCustomerCountryCode.getText().toString());
        cartUserDetail.setName(etCustomerName.getText().toString());
        cartUserDetail.setPhone(etCustomerMobile.getText().toString());
        addresses.setUserDetails(cartUserDetail);
        destinationAddresses.add(addresses);

        cartOrder.setDestinationAddresses(destinationAddresses);
        cartOrder.setPickupAddresses(currentBooking.getPickupAddresses());


        // add filed on 4_Aug_2018
        double cartOrderTotalPrice = 0, cartOrderTotalTaxPrice = 0;
        for (CartProducts products : currentBooking.getCartProductWithSelectedSpecificationList()) {
            cartOrderTotalPrice = cartOrderTotalPrice + products
                    .getTotalProductItemPrice();
            cartOrderTotalTaxPrice = cartOrderTotalTaxPrice + products.getTotalItemTax();
        }
        cartOrder.setCartOrderTotalPrice(cartOrderTotalPrice);
        cartOrder.setCartOrderTotalTaxPrice(cartOrderTotalTaxPrice);


        Log.d("add_item_in_cart", "==========> " + cartOrder.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddCartResponse> responseCall = apiInterface.addItemInCart(ApiClient
                .makeGSONRequestBody(cartOrder));
        responseCall.enqueue(new Callback<AddCartResponse>() {
            @Override
            public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    currentBooking.setCartId(response.body().getCartId());
                    currentBooking.setCartCityId(response.body().getCityId());
                    //  checkIsPickUpDeliveryByUser(cbSelfDelivery.isChecked());
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(),
                            CheckoutActivity.this);
                }
            }

            @Override
            public void onFailure(Call<AddCartResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PRODUCT_SPE_ACTIVITY, t);
                try {
                    callErrorFunction(t);
                } catch (Exception e) {
                    Log.e("error_failure_main", e.toString());
                }
            }
        });
    }


    private void goToPlaceOrder() {

        // if (isValidate()) {
        //   if (!etCustomerName.isEnabled()) {
        if (CurrentBooking.getInstance().isFutureOrder()) {
            if (CurrentBooking.getInstance()
                    .getFutureOrderSelectedTimeUTCMillis() >= System
                    .currentTimeMillis() && !TextUtils
                    .isEmpty(currentBooking.getFutureOrderDate())) {
                gotoStripepaymentActivity(true, invoices);

            } else {
                Utils.showToast(getResources().getString(R.string
                                .msg_plz_select_schedule_date_first),
                        CheckoutActivity.this);
            }

        } else {
            gotoStripepaymentActivity(true, invoices);

        }
        //  } else {
        //    Utils.showToast(getResources().getString(R.string
        //             .msg_plz_confirm_user_detail), this);
        //  }
        //  }

    }


    private void getFavAddressList() {
        favAddressList.clear();
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<FavouriteAddressResponse> responseCall = apiInterface.getFavouriteAddressList(ApiClient
                .makeJSONRequestBody(jsonObject));

        Log.i("getFavAddressList()", jsonObject.toString());
        responseCall.enqueue(new Callback<FavouriteAddressResponse>() {
            @Override
            public void onResponse(Call<FavouriteAddressResponse> call, Response<FavouriteAddressResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().getSuccess())
                {
                    favAddressList.addAll(response.body().getFavouriteAddresses());

                    if (favAddressList.size() == 0) {
                        OpenFavoriteDialog();
                    }


                    Log.i("getFavAddressList()", "ok");

                    if (adfavoriteadress) {
                        etCustomerDeliveryAddress.setText(favAddressList.get(favAddressList.size() - 1).getAddress());

                        etDeliveryAddressNote.setText(favAddressList.get(favAddressList.size() - 1).getNote());

                        currentBooking.setDeliveryAddress(favAddressList.get(favAddressList.size() - 1).getAddress());
                        LatLng favLatLng = new LatLng(favAddressList.get(favAddressList.size() - 1).getLocation().get(0),
                                favAddressList.get(favAddressList.size() - 1).getLocation().get(1));
                        currentBooking.setDeliveryLatLng(favLatLng);
                        changeDeliveryAddressAvailable(favLatLng, favAddressList.get(favAddressList.size() - 1).getAddress());
                        map.setCenter(new GeoCoordinate(favLatLng.latitude, favLatLng.longitude), Map.Animation.LINEAR);
                    } else {
                        if (favAddressList.isEmpty()) {
                            etCustomerDeliveryAddress.setText("");
                            etDeliveryAddressNote.setText("");


                            //goToAddFavoritefornewuser();
                        } else {
                            etCustomerDeliveryAddress.setText(favAddressList.get(0).getAddress());
                            etDeliveryAddressNote.setText(favAddressList.get(0).getNote());
                            currentBooking.setDeliveryAddress(favAddressList.get(0).getAddress());
                            LatLng favLatLng = new LatLng(favAddressList.get(0).getLocation().get(0),
                                    favAddressList.get(0).getLocation().get(1));
                            currentBooking.setDeliveryLatLng(favLatLng);
                            changeDeliveryAddressAvailable(favLatLng, favAddressList.get(0).getAddress());
                            /* map.setCenter(new GeoCoordinate(favLatLng.latitude, favLatLng.longitude), Map.Animation.LINEAR);
                             */

                            if (customFavAddressDialog != null && customFavAddressDialog.isShowing()) {
                                if (customFavAddressDialog.isShowing()) {
                                    rcvCountryCode.setLayoutManager(new LinearLayoutManager(CheckoutActivity.this));
                                    FavAddressAdapter FavAddressAdapter = new FavAddressAdapter(favAddressList, false, false) {
                                        @Override
                                        public void onDelete(int position) {

                                        }

                                        @Override
                                        public void onEdit(int position) {

                                        }
                                    };
                                    rcvCountryCode.setAdapter(FavAddressAdapter);
                                    rcvCountryCode.addItemDecoration(new DividerItemDecoration(CheckoutActivity.this, LinearLayoutManager.VERTICAL));

                                }
                            }


                        }
                    }
                   /* currentBooking.setDeliveryAddress(favAddressList.get(favAddressList.size()-1).getAddress());
                    currentBooking.setDeliveryLatLng(new LatLng(favAddressList.get(favAddressList.size()-1).getLocation().get(0),
                            favAddressList.get(favAddressList.size()-1).getLocation().get(1)));*/

                } else {
                   /* Utils.showErrorToast(response.body().getErrorCode(), CheckoutActivity.this);*/
                }

            }

            @Override
            public void onFailure(Call<FavouriteAddressResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);

            }
        });
    }

    private void OpenFavoriteDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(CheckoutActivity.this);
        builder1.setMessage(getString(R.string.you_must_add_address_checkout));
        builder1.setCancelable(false);

        builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                goToFavouriteAddressActivity(false, false);
            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void openFavAddressDialog() {

        if (customFavAddressDialog != null && customFavAddressDialog.isShowing()) {
            return;
        }

        customFavAddressDialog = new CustomFavAddressDialog(this, favAddressList, true) {
            @Override
            public void onSelect(int position) {
                Utils.hideSoftKeyboard(CheckoutActivity.this);
                etCustomerDeliveryAddress.setText(favAddressList.get(position).getAddress());
                etDeliveryAddressNote.setText(favAddressList.get(position).getNote());
                currentBooking.setDeliveryAddress(favAddressList.get(position).getAddress());
                LatLng favLatLng = new LatLng(favAddressList.get(position).getLocation().get(0),
                        favAddressList.get(position).getLocation().get(1));
                currentBooking.setDeliveryLatLng(favLatLng);
                changeDeliveryAddressAvailable(favLatLng, favAddressList.get(position).getAddress());
                /*  map.setCenter(new GeoCoordinate(favLatLng.latitude, favLatLng.longitude), Map.Animation.LINEAR);*/


                setEnableFiled(true);
                ivConfirmDetail.setImageDrawable(AppCompatResources.getDrawable(CheckoutActivity.this, R
                        .drawable.ic_edit_black_24dp));
                ivConfirmDetail.setVisibility(View.GONE);
                if (!etCustomerDeliveryAddress.getText().toString().isEmpty()) {
                    currentBooking.setDeliveryAddress(etCustomerDeliveryAddress.getText().toString());
                }
                /*   tvEditDetail.setVisibility(View.VISIBLE);*/
                etPromoCode.requestFocus();
                // addItemInServerCart();
                dismiss();
            }

            @Override
            public void onClose() {
                customFavAddressDialog.dismiss();
            }

            @Override
            public void onAddAddress() {
                goToAddUserAddress();
            }
        };
        customFavAddressDialog.show();
    }

    /**
     * this method used to called webservice for get Delivery type according to param
     *
     * @param cityLatLng location of city
     */
    private void changeDeliveryAddressAvailable(final LatLng cityLatLng, final String address) {
        final Addresses addresses = new Addresses();
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            addresses.setAddress(address);
            addresses.setCity(currentBooking.getCity1());
            addresses.setAddressType(Const.Type.DESTINATION);
            addresses.setNote("");
            addresses.setUserType(Const.Type.USER);
            ArrayList<Double> location = new

                    ArrayList<>();
            location.add(cityLatLng.latitude);
            location.add(cityLatLng.longitude);
            addresses.setLocation(location);
            CartUserDetail cartUserDetail = new CartUserDetail();
            cartUserDetail.setEmail(preferenceHelper.getEmail());
            cartUserDetail.setCountryPhoneCode(preferenceHelper.getPhoneCountyCodeCode());
            cartUserDetail.setName(preferenceHelper.getFirstName() + " " + preferenceHelper
                    .getLastName());
            cartUserDetail.setPhone(preferenceHelper.getPhoneNumber());
            addresses.setUserDetails(cartUserDetail);
            ArrayList<Addresses> addresses1 = new ArrayList<>();
            addresses1.add(addresses);
            jsonObject.put(Const.Params.DESTINATION_ADDRESSES, ApiClient.JSONArray(addresses1));
            jsonObject.put(Const.Params.CART_ID, currentBooking.getCartId());
            Log.i("changeDeliveryAddressAvailable", jsonObject.toString());


        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.DELIVERY_LOCATION_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.changeDeliveryAddress
                (ApiClient.makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @SuppressLint("ResourceType")
            @Override
            public void onResponse(Call<IsSuccessResponse> call,
                                   Response<IsSuccessResponse> response) {

                Utils.hideCustomProgressDialog();
                AppLog.Log("DELIVERY_CITY", ApiClient.JSONResponse(response.body()));

                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    // currentBooking.setDeliveryAddress(address);
                    currentBooking.setDeliveryLatLng(cityLatLng);
                    currentBooking.getDestinationAddresses().clear();
                    currentBooking.setDestinationAddresses(addresses);
                    btnPlaceOrder.setEnabled(true);
                    btnPlaceOrder.setBackgroundColor(getResources().getColor(R.color.color_black));
                    getDistanceMatrix();
                } else {
                    // disable placeroder button = code 966
                    btnPlaceOrder.setEnabled(false);
                    btnPlaceOrder.setBackgroundColor(getResources().getColor(R.color.color_app_light_bg));

                    Log.e("Error_line_2138", "Something went wrong + delivery not available in selected address");
                    // Utils.showErrorToast(response.body().getErrorCode(), CheckoutActivity.this);
                }


            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.DELIVERY_LOCATION_ACTIVITY, t);

            }
        });
    }

    private void checkAvailableProvider() {
        Utils.showCustomProgressDialog(this, false);
        currentBooking.setBookCityId(preferenceHelper.getBookCityId());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.LATITUDE, currentBooking.getStoreLocation().get(0));
            jsonObject.put(Const.Params.LONGITUDE, currentBooking.getStoreLocation().get(1));
            jsonObject.put(Const.Params.CITY_ID, currentBooking.getBookCityId());
            jsonObject.put(Const.Params.STORE_ID, currentBooking.getSelectedStoreId());

        } catch (JSONException e) {
            AppLog.handleException(PaymentActivity.class.getName(), e);
        }
        Log.i("checkAvailableProvider", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AvailableProviderResponse> responseCall = apiInterface.checkStoreProviderAvailable(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<AvailableProviderResponse>() {
            @Override
            public void onResponse(Call<AvailableProviderResponse> call, Response<AvailableProviderResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().getSuccess()) {
                    if (response.body().isTotalOrderGreaterThanThree()) {
                        openProviderNotFoundDialog();
                    } else {
                        goToPlaceOrder();
                    }
                } else {
                    Utils.showToast(getResources().getString(R.string
                            .msg_no_provider_found), CheckoutActivity.this);
                }

            }

            @Override
            public void onFailure(Call<AvailableProviderResponse> call, Throwable t) {
                try {
                    callErrorFunction(t);
                } catch (Exception e) {
                    Log.e("error_failure_main", e.toString());
                }
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
            }
        });
    }

    private void openProviderNotFoundDialog() {
        String msg = getResources().getString(R.string.msg_driver_busy);
        CustomDialogAlert customDialogAlert = new CustomDialogAlert(CheckoutActivity.this, getResources().getString(R.string
                .text_attention), msg, getResources().getString(R.string.text_ok), getResources().getString(R.string.text_cancel)) {
            @Override
            public void onClickLeftButton() {
                goToPlaceOrder();
                dismiss();
            }

            @Override
            public void onClickRightButton() {
                dismiss();
            }
        };
        customDialogAlert.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (ParseContent.getInstance().latLng != null && ParseContent.getInstance().address != null) {
            //currentBooking.setDeliveryLatLng(ParseContent.getInstance().latLng);

            Log.e("currentBooking", "currentBooking---------" + currentBooking.getDeliveryLatLng()
                    + "Address--------" + ParseContent.getInstance().address);

            // currentBooking.setDeliveryLatLng(new LatLng(ParseContent.getInstance().latLng.getLatitude(),ParseContent.getInstance().latLng.getLongitude()));
            currentBooking.setDeliveryAddress(ParseContent.getInstance().address);
            getDistanceMatrix();
        }

        //in this line first initialize list at line :2392
        if (!isCurrentLogin()) {

        } else {
            getFavAddressList();
        }
    }

    public void goToAddUserAddress() {
        goToFavouriteAddressActivity(false, false);
       /* // Intent intent = new Intent(this, UserAddAddress.class);
        Intent intent = new Intent(this, AddUserLocationActivity.class);

        // intent.putExtra(Const.Tag.HOME_FRAGMENT, isApplicationStart);
        startActivityForResult(intent, Const.STORE_LOCATION_RESULT_CHECKOUT);*/
    }

    public void goToAddFavoritefornewuser() {
        Utils.showToast("you must have to add one favorite adress whean you are register with new user", CheckoutActivity.this);
        Intent intent = new Intent(this, AddUserLocationActivity.class);

        // intent.putExtra(Const.Tag.HOME_FRAGMENT, isApplicationStart);
        startActivityForResult(intent, Const.STORE_LOCATION_RESULT_CHECKOUT_NEWUSER);
    }
    /*private void getExtraData() {
        if (getIntent().getExtras() != null) {
            isApplicationStart = getIntent().getExtras().getBoolean(Const.Tag.HOME_FRAGMENT);
            getFavAddressList();
        }

    }*/

    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS, grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0) {
            switch (requestCode) {

                case REQUEST_CODE_ASK_PERMISSIONS:
                    for (int index = permissions.length - 1; index >= 0; --index) {
                        if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                            // exit the app if one permission is not granted
                            Toast.makeText(this, "Required permission '" + permissions[index]
                                    + "' not granted, exiting", Toast.LENGTH_LONG).show();
                            finish();
                            return;
                        }
                    }
                    // all permissions were granted
                    // initialize();

                    setheremap();
                    break;


                default:
                    //do with default
                    break;

            }
        }

    }

    private void setheremap() {
        try{
            mapFragment = getMapFragment();
            Utils.showCustomProgressDialog(CheckoutActivity.this, false);
            mapFragment.init(new OnEngineInitListener() {
                @Override
                public void onEngineInitializationCompleted(Error error) {
                    if (error == Error.NONE) {
                        map = mapFragment.getMap();

                        // Set the zoom level to the average between min and max
                        map.setZoomLevel(15);

                        if (!isCurrentLogin()) {
                            map.setCenter(new GeoCoordinate(currentBooking.getDeliveryLatLng().latitude, currentBooking.getDeliveryLatLng().longitude), Map.Animation.LINEAR);
                        } else {
                            /*getFavAddressList();*/
                        }

                        mapFragment.getMapGesture().setPinchEnabled(false);
                        mapFragment.getMapGesture().setPanningEnabled(false);
                        mapFragment.getMapGesture().setKineticFlickEnabled(false);
                        Utils.hideCustomProgressDialog();


                    } else {
                        Utils.hideCustomProgressDialog();
                        System.out.println("ERROR: Cannot initialize Map Fragment");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new AlertDialog.Builder(CheckoutActivity.this).setMessage(
                                        "Error : " + error.name() + "\n\n" + error.getDetails())
                                        .setTitle(R.string.engine_init_error)
                                        .setNegativeButton(android.R.string.cancel,
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog,
                                                            int which) {
                                                        finish();
                                                    }
                                                }).create().show();
                            }
                        });
                    }
                }
            });
            //  Toast.makeText(getApplicationContext(),"ok",Toast.LENGTH_LONG).show();
        }catch (Exception e)
        {

        }

    }
}
