package com.edelivery;

import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.viewpager.widget.ViewPager;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edelivery.adapter.WelcomePagerAdapter;
import com.edelivery.component.CustomFontTextView;

public class WelcomeActivity extends BaseAppCompatActivity {
    private ViewPager viewPager;
    private WelcomePagerAdapter welcomePagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] image;
    private CustomFontTextView tvSkip;

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_welcome);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        findViewById();
        setViewListener();
        initWelcomePager();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        tvSkip = (CustomFontTextView) findViewById(R.id.tvSkip);
    }

    @Override
    protected void setViewListener() {
        tvSkip.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSkip:
                preferenceHelper.putIsHideWelcomeScreen(true);
                if (ContextCompat.checkSelfPermission(WelcomeActivity.this, android
                        .Manifest.permission
                        .ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED && ContextCompat
                        .checkSelfPermission(WelcomeActivity.this,
                                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
                    goToMandatoryDeliveryLocationActivity();
                } else {
                    goToHomeActivity();
                }

                break;

            default:
                // do with default
                break;
        }
    }

    private void initWelcomePager() {
        image = new int[]{
                R.drawable.img_1,
                R.drawable.img_2,
                R.drawable.img_3};

        addBottomDots(0);
        changeStatusBarColor();
        welcomePagerAdapter = new WelcomePagerAdapter(this, image);
        viewPager.setAdapter(welcomePagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int
                    positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[image.length];
        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setPaddingRelative(0, 0, 10, 0);
            dots[i].setTextColor(ResourcesCompat.getColor(getResources(), R.color
                    .color_app_label, null));
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(ResourcesCompat.getColor(getResources(), R.color
                    .color_app_headings, null));
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}
