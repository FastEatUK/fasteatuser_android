package com.edelivery;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.edelivery.adapter.ViewPagerAdapter;
import com.edelivery.fragments.OverviewFragment;
import com.edelivery.fragments.ReviewFragment;
import com.edelivery.models.datamodels.Store;
import com.edelivery.utils.Const;

public class ReviewActivity extends BaseAppCompatActivity {
    public Store store;
    private TabLayout reviewHistoryTabsLayout;
    private ViewPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_orders));
        findViewById();
        setViewListener();
        initTabLayout();
        loadExtraData();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings
        reviewHistoryTabsLayout = (TabLayout) findViewById(R.id
                .reviewTabsLayout);
        viewPager = (ViewPager) findViewById(R.id.reviewViewpager);
    }

    @Override
    protected void setViewListener() {
        // do somethings
        reviewHistoryTabsLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();
    }


    @Override
    public void onClick(View view) {
        // do somethings

    }

    private void initTabLayout() {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new OverviewFragment(), getResources()
                .getString(R.string.text_over_view));
        viewPagerAdapter.addFragment(new ReviewFragment(), getResources()
                .getString(R.string.text_review));
        viewPager.setAdapter(viewPagerAdapter);
        reviewHistoryTabsLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    private void loadExtraData() {
        if (getIntent().getExtras() != null) {
            store = getIntent().getExtras().getParcelable(Const.STORE_DETAIL);
        }

    }
}
