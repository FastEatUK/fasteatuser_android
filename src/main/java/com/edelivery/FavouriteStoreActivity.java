package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.LinearLayout;

import com.edelivery.adapter.FavouriteStoreAdapter;
import com.edelivery.models.datamodels.RemoveFavourite;
import com.edelivery.models.datamodels.Store;
import com.edelivery.models.datamodels.StoreClosedResult;
import com.edelivery.models.responsemodels.FavouriteStoreResponse;
import com.edelivery.models.responsemodels.SetFavouriteResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteStoreActivity extends BaseAppCompatActivity {

    private RecyclerView rcvFavoriteStore;
    private FavouriteStoreAdapter favouriteStoreAdapter;
    private List<Store> storeList;
    private LinearLayout ivEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_store);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_favourite));
        findViewById();
        setViewListener();
        setDeleteButton();
        initRcvFavourite();
        getFavoriteStores();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //   getFavoriteStores();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        rcvFavoriteStore = (RecyclerView) findViewById(R.id.rcvFavoriteStore);
        ivEmpty = (LinearLayout) findViewById(R.id.ivEmpty);
    }

    @Override
    protected void setViewListener() {

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {

    }

    private void initRcvFavourite() {
        rcvFavoriteStore.setLayoutManager(new LinearLayoutManager(this));
    }

    private void getFavoriteStores() {
        storeList = new ArrayList<>();
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());

            Log.d("call","======== Sucess ======= "+jsonObject.toString());

        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<FavouriteStoreResponse> call = apiInterface.getFavouriteStores(ApiClient
                .makeJSONRequestBody(jsonObject));
        call.enqueue(new Callback<FavouriteStoreResponse>() {
            @Override
            public void onResponse(Call<FavouriteStoreResponse> call,
                                   Response<FavouriteStoreResponse
                                           > response) {
                Utils.hideCustomProgressDialog();
                AppLog.Log("FAVOURITE_STORE", ApiClient.JSONResponse(response.body()));
                Log.d("onResponse","======== Sucess ======= "+new Gson().toJson(response.body()));

                //  storeList.clear();
                if (parseContent.isSuccessful(response))
                {
                    if (response.body().getSuccess())
                    {
                        storeList.addAll(response.body().getFavouriteStores());
                        for (Store store : storeList) {
                            StoreClosedResult storeClosedResult = Utils.checkStoreOpenAndClosed
                                    (FavouriteStoreActivity.this, store.getStoreTime(), response
                                                    .body().getServerTime(),
                                            store.getCityDetail().getTimezone(), false, 0);
                            store.setStoreClosed(storeClosedResult.isStoreClosed());
                            store.setReOpenTime(storeClosedResult.getReOpenAt());
                            store.setDistance(0);
                            store.setCurrency(store.getCountries().getCurrencySign());
                            store.setPriceRattingAndTag(Utils.getStringPriceAndTag(store
                                    .getFamousProductsTags
                                            (), store.getPriceRating(), store
                                    .getCountries().getCurrencySign()));
                        }
                        if (storeList.size() > 0)
                        {
                            ivEmpty.setVisibility(View.GONE);
                            tvToolbarRightBtn.setVisibility(View.VISIBLE);
                            rcvFavoriteStore.setVisibility(View.VISIBLE);
                            favouriteStoreAdapter=new FavouriteStoreAdapter(FavouriteStoreActivity.this,storeList);
                            rcvFavoriteStore.setAdapter(favouriteStoreAdapter);
                        }
                        else
                        {
                            rcvFavoriteStore.setVisibility(View.GONE);
                            ivEmpty.setVisibility(View.VISIBLE);
                            tvToolbarRightBtn.setVisibility(View.GONE);
                        }

                    } else {
                        /*Utils.showErrorToast(response.body().getMessage(),
                                FavouriteStoreActivity.this);*/
                        rcvFavoriteStore.setVisibility(View.GONE);
                        ivEmpty.setVisibility(View.VISIBLE);
                        tvToolbarRightBtn.setVisibility(View.GONE);
                    }
                }
                else {
                        /*Utils.showErrorToast(response.body().getMessage(),
                                FavouriteStoreActivity.this);*/
                    rcvFavoriteStore.setVisibility(View.GONE);
                    ivEmpty.setVisibility(View.VISIBLE);
                    tvToolbarRightBtn.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<FavouriteStoreResponse> call, Throwable t) {
                Log.d("onFailure","======== Failure ======= "+t.getLocalizedMessage());
                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
                Utils.hideCustomProgressDialog();
            }
        });
    }

    private void removeAsFavoriteStore(final ArrayList<String> store) {
        Utils.showCustomProgressDialog(this, false);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SetFavouriteResponse> call = apiInterface.removeAsFavouriteStore(ApiClient
                .makeGSONRequestBody(new RemoveFavourite(preferenceHelper.getSessionToken(),
                        preferenceHelper.getUserId(), store)));
        call.enqueue(new Callback<SetFavouriteResponse>() {
            @Override
            public void onResponse(Call<SetFavouriteResponse> call, Response<SetFavouriteResponse
                    > response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        CurrentBooking.getInstance().getFavourite().clear();
                        CurrentBooking.getInstance().setFavourite(response.body()
                                .getFavouriteStores());
                        getFavoriteStores();
                    }
                }
            }

            @Override
            public void onFailure(Call<SetFavouriteResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void setDeleteButton() {
        ivToolbarRightIcon3.setImageDrawable(null);
        tvToolbarRightBtn.setText(getResources().getString(R.string.text_delete));
        tvToolbarRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> store = new ArrayList<>();
                for (int i = 0; i < storeList.size(); i++) {
                    if (storeList.get(i).isFavouriteRemove()) {
                        store.add(storeList.get(i).getId());
                    }
                }
                if (store.isEmpty()) {
                    Utils.showToast(getResources().getString(R.string.msg_plz_check_one_ro_more),
                            FavouriteStoreActivity.this);
                } else {
                    removeAsFavoriteStore(store);
                }
            }
        });
    }

    public void gotToStoreProductActivity(Store store) {
        Intent intent = new Intent(this, StoreProductActivity.class);
        intent.putExtra(Const.SELECTED_STORE, store);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}