package com.edelivery;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PointF;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.edelivery.adapter.AutoSuggestAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontAutoCompleteView;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Addresses;
import com.edelivery.models.datamodels.CartUserDetail;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.tasks.OnSuccessListener;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.search.AddressFilter;
import com.here.android.mpa.search.AutoSuggest;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.ReverseGeocodeRequest;
import com.here.android.mpa.search.TextAutoSuggestionRequest;
import com.mapbox.android.core.location.*;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.plugins.places.picker.PlacePicker;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import mapboxutils.CustomEventMapBoxView;
import mapboxutils.MapBoxMapReady;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class CheckoutDeliveryLocationActivity extends BaseAppCompatActivity implements
            LocationHelper.OnLocationReceived, OnMapReadyCallback, MapBoxMapReady
,View.OnClickListener, PermissionsListener

{

    private LocationHelper locationHelper;
    private CustomFontTextView acDeliveryAddress;
    private ImageView ivTargetLocation, ivClearText;
    private CustomFontButton btnDone;
    public FrameLayout mapFrameLayout;
    private CustomDialogAlert customDialogEnable;
    // Developer s mapbox
    private static final int REQUEST_CHECK_SETTINGS =656 ;
    public MapboxMap mapboxMap;
    public LocationEngine locationEngine;
    public PermissionsManager permissionsManager;
    public long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    public long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
    LocationEngineRequest mLocationEngineRequest;
    public MainActivityLocationCallback callback = new MainActivityLocationCallback(this);
    public MapView mapbox;
    public boolean isCurrentLocationGet = false;
    public String geojsonSourceLayerId = "geojsonSourceLayerId";
    public static double str_Latitude = 0.0, str_Longitude = 0.0;
    private LatLng deliveryLatLng;
    public boolean isplaceAddress = false;


    private LatLng currentLatLng;
    private Location currentLocation;
    private boolean isCameraIdeal = true;
    private Location lastLocation;

    //for here maps
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    private AndroidXMapFragment mapFragment = null;
    private Map map = null;

    private boolean isfirstTime = false;

    private LinearLayout searchview;
    private AutoSuggestAdapter m_autoSuggestAdapter;
    private ListView listView;
    private EditText editsearch;
    private ImageView close;
    private List<AutoSuggest> m_autoSuggests = new ArrayList<>();



    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.heremapview);
    }





    public CheckoutDeliveryLocationActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      /*  Mapbox.getInstance(getApplicationContext(), getResources().getString(R.string
                .MAP_BOX_ACCESS_TOKEN));
      */
        setContentView(R.layout.activity_checkout_delivery_location);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);



        checkPermissions();
        initToolBar();
        setTitleOnToolBar(getString(R.string.text_delivery_location));
        findViewById();


        /*mapbox = findViewById(R.id.mapboxView);
        mapbox.onCreate(savedInstanceState);
*/

        setViewListener();
        locationHelper = new LocationHelper(this);
        locationHelper.setLocationReceivedLister(this);
        if(currentBooking.getDeliveryAddress()!=null && !currentBooking.getDeliveryAddress().equals(""))
        {
            acDeliveryAddress.setText(currentBooking.getDeliveryAddress());
        }
        //new CustomEventMapBoxView(getApplicationContext(), mapbox, this);

    }

   /* private void initPlaceAutoComplete() {
        acDeliveryAddress = (CustomFontAutoCompleteView) findViewById(R.id.acDeliveryAddress);

        autocompleteAdapter = new PlaceAutocompleteAdapter(this, locationHelper
                .googleApiClient);
//        autocompleteAdapter.setPlaceFilter();
        acDeliveryAddress.setAdapter(autocompleteAdapter);
        acDeliveryAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                getGeocodeDataFromAddress(acDeliveryAddress.getText().toString());
            }
        });


    }
*/
   /* private void setPlaceFilter(String countryCode) {
        if (autocompleteAdapter != null) {

//            autocompleteAdapter.setPlaceFilter();
            locationHelper.getLastLocation(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        LatLng latLng = new LatLng(location.getLatitude(),
                                location.getLongitude());
                        LatLngBounds latLngBounds = new LatLngBounds(
                                latLng, latLng);
                        autocompleteAdapter.setBounds(latLngBounds);
                    }
                }
            });
        }
    }*/

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {

      //  mapView = (CustomEventMapView) findViewById(R.id.mapView);
        acDeliveryAddress =  findViewById(R.id.acDeliveryAddress);
        ivTargetLocation = (ImageView) findViewById(R.id.ivTargetLocation);
        ivClearText = (ImageView) findViewById(R.id
                .ivClearDeliveryAddressTextMap);
        btnDone = (CustomFontButton) findViewById(R.id.btnDone);
        mapFrameLayout = (FrameLayout) findViewById(R.id.mapFrameLayout);

        editsearch = (EditText) findViewById(R.id.edit_query);
        listView = (ListView) findViewById(R.id.list_search);
        close = (ImageView) findViewById(R.id.imgclose);
        searchview = (LinearLayout) findViewById(R.id.atuplacepickerview);



    }

    @Override
    protected void setViewListener() {
     //   mapView.getMapAsync(this);
        ivTargetLocation.setOnClickListener(this);
        ivClearText.setOnClickListener(this);
        ivClearText.setVisibility(View.GONE);
        btnDone.setOnClickListener(this);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editsearch.getText().clear();
                listView.setAdapter(null);
            }
        });

        acDeliveryAddress.setOnClickListener(this);

        acDeliveryAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    ivClearText.setVisibility(View.VISIBLE);
                } else {
                    ivClearText.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }


    @Override
    public void onLocationChanged(Location location) {
        //do something
        AppLog.Log(Const.Tag.HOME_FRAGMENT, "GoogleClientLocationChanged");
        currentLocation = location;
        currentLatLng = new LatLng(location.getLatitude(),
                location.getLongitude());
        lastLocation = currentLocation;
//        deliveryLatLng= new LatLng(location.getLatitude(),
//                location.getLongitude());

    }

    @Override
    public void onConnected(Bundle bundlLe) {
        AppLog.Log(Const.Tag.HOME_FRAGMENT, "GoogleClientConnected");
        lastLocation = locationHelper.getLastLocation();
        locationHelper.startLocationUpdate();
        //checkLocationPermission(false, null);
    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        //do something
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivTargetLocation:
                /*if (!locationHelper.isOpenGpsDialog()) {
                    locationHelper.setOpenGpsDialog(true);
                    locationHelper.setLocationSettingRequest(this, REQUEST_CHECK_SETTINGS, new
                            OnSuccessListener() {
                                @Override
                                public void onSuccess(Object o) {
                                    checkLocationPermission(null);
                                }
                            }, new LocationHelper.NoGPSDeviceFoundListener() {
                        @Override
                        public void noFound() {
                            checkLocationPermission(null);
                        }
                    });
                }*/
                isfirstTime = true;
                moveCameraFirstMyLocationZoom();

                break;
            case R.id.ivClearDeliveryAddressTextMap:
                acDeliveryAddress.setText("");
                break;
            case R.id.btnDone:

                if (!TextUtils.equals(currentBooking.getDeliveryAddress(), acDeliveryAddress
                        .getText().toString()))
                {

                    if(!acDeliveryAddress.getText().toString().isEmpty())
                    {
                        changeDeliveryAddressAvailable(deliveryLatLng,  acDeliveryAddress
                                .getText().toString());
                    }
                    } else {
                    onBackPressed();
                }
                break;


            case R.id.acDeliveryAddress:
              /*  Intent intent = new PlaceAutocomplete.IntentBuilder()
                        .accessToken(getResources().getString(R.string
                                .MAP_BOX_ACCESS_TOKEN))
                                .placeOptions(PlaceOptions.builder().backgroundColor(Color.parseColor("#EEEEEE")).country("gb").limit(10).language("en")
                                        .build())
                        .build(this);
                startActivityForResult(intent, Const.REQUEST_CODE_AUTOCOMPLETE);*/


                searchview.setVisibility(View.VISIBLE);
                editsearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (!s.toString().isEmpty()) {
                            doSearch(s.toString());

                        } else {
                            setSearchMode(false);
                        }


                    }
                });


            default:
                // do with default
                break;
        }
    }
    private void setSearchMode(boolean b) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        checkLocationPermission(null);
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        locationHelper.setOpenGpsDialog(false);
                        break;
                }
            case Const.REQUEST_CODE_AUTOCOMPLETE:
                if (data != null) {
                    CarmenFeature feature = PlaceAutocomplete.getPlace(data);
                    acDeliveryAddress.setText(feature.placeName());

                    CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);
                    if (mapboxMap != null) {
                        Utils.hideSoftKeyboard(this);
                        Style style = mapboxMap.getStyle();
                        if (style != null) {
                            GeoJsonSource source = style.getSourceAs(geojsonSourceLayerId);
                            if (source != null) {
                                source.setGeoJson(FeatureCollection.fromFeatures(
                                        new Feature[]{Feature.fromJson(selectedCarmenFeature.toJson())}));
                            }
                            isplaceAddress = true;
                            acDeliveryAddress.setText(selectedCarmenFeature.placeName());

                            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                                    new CameraPosition.Builder()
                                            .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                                    ((Point) selectedCarmenFeature.geometry()).longitude()))
                                            .zoom(14)
                                            .build()), 4000);

                        }
                    }
                }
                break;
            case Const.REQUEST_CODE:
                CarmenFeature carmenFeature = PlacePicker.getPlace(data);
                acDeliveryAddress.setText(carmenFeature.placeName());

        }
    }

    /**
     * This method is used to setUpMap option which help to load map as per option
     */
 /*   private void setUpMap() {

        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            boolean doNotMoveCameraToCenterMarker = true;

            public boolean onMarkerClick(Marker marker) {
                return doNotMoveCameraToCenterMarker;
            }
        });

        this.googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                AppLog.Log("IS_MAP_TOUCHED", String.valueOf(isMapTouched));
                if (isMapTouched) {
                    deliveryLatLng = googleMap.getCameraPosition().target;
                    getGeocodeDataFromLocation(deliveryLatLng);
                }
                isMapTouched = true;

            }

        });
    }
*/
   /* @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setUpMap();
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(currentBooking.getDeliveryLatLng()).zoom(17).build();
        googleMap.moveCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
    }*/

    /***
     * this method is used to move camera on map at current position and a isCameraMove is
     * used to decide when is move or not
     */
  /*  public void moveCameraFirstMyLocation(final boolean isAnimate, LatLng latLng) {
        if (latLng == null) {
            locationHelper.getLastLocation(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        LatLng latLng = new LatLng(location
                                .getLatitude(),
                                location.getLongitude());
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(latLng).zoom(17).build();

//                        if (isAnimate) {
//                            googleMap.animateCamera(CameraUpdateFactory
//                                    .newCameraPosition(cameraPosition));
//                        } else {
//                            googleMap.moveCamera(CameraUpdateFactory
//                                    .newCameraPosition(cameraPosition));
//                        }
                        locationHelper.setOpenGpsDialog(false);
                    }
                }
            });

        } else {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng).zoom(17).build();

            *//*if (isAnimate) {
                googleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            } else {
                googleMap.moveCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }*//*
            locationHelper.setOpenGpsDialog(false);
        }

    }
*/

    private void moveCameraFirstMyLocationZoom() {
        try {
            if (locationHelper != null && locationHelper.getLastLocation() != null) {
                currentLocation = locationHelper.getLastLocation();
                if (currentLocation != null) {
                    currentLatLng = new LatLng(currentLocation.getLatitude(),
                            currentLocation.getLongitude());
//                    isCameraIdeal = false;

                    Log.i("Deliverylocation", "moveCameraFirstMyLocationZoom " + currentLocation.getLatitude() + currentLocation.getLongitude());
                    //zoomCamera(currentLatLng, "");
                    deliveryLatLng=currentLatLng;
                    map.setCenter(new GeoCoordinate(currentLatLng.getLatitude(), currentLatLng.getLongitude()), Map.Animation.NONE);
                    map.setZoomLevel(15);
                    Log.i("Deliverylocation", "zoomCamera");

                    // GeoCoordinate geoCoordinate=map.getCenter();
                    acDeliveryAddress.setText(currentBooking.getCurrentAddress());
                  //setListViewData(currentLatLng, currentBooking.getCurrentAddress());

                    isCameraIdeal = true;
                }

                locationHelper.setOpenGpsDialog(false);
            } else {
                if (currentBooking != null && currentBooking.getCurrentLatLng() != null &&
                        currentBooking.getCurrentLatLng().latitude != 0.0 && currentBooking.getCurrentLatLng().longitude != 0.0)
                {
                    deliveryLatLng=new LatLng(currentBooking.getCurrentLatLng().latitude, currentBooking.getCurrentLatLng().longitude);
                    map.setCenter(new GeoCoordinate(currentBooking.getCurrentLatLng().latitude, currentBooking.getCurrentLatLng().longitude), Map.Animation.NONE);
                    map.setZoomLevel(15);
                    acDeliveryAddress.setText(currentBooking.getCurrentAddress());
                  //  setListViewData(new LatLng(currentBooking.getCurrentLatLng().latitude,currentBooking.getCurrentLatLng().latitude), currentBooking.getCurrentAddress());
                    Log.i("Deliverylocation", "moveCameraFirstMyLocationZoom with else " + currentBooking.getCurrentLatLng());
                }else{
                    deliveryLatLng=new LatLng(ParseContent.getInstance().latLng.getLatitude(), ParseContent.getInstance().latLng.getLongitude());
                    map.setCenter(new GeoCoordinate(ParseContent.getInstance().latLng.getLatitude(), ParseContent.getInstance().latLng.getLongitude()), Map.Animation.NONE);
                    map.setZoomLevel(15);
                    acDeliveryAddress.setText(currentBooking.getCurrentAddress());
                   // setListViewData(ParseContent.getInstance().latLng, currentBooking.getCurrentAddress());
                }
            }
        } catch (Exception e) {
            Log.e("moveCameraFirstMyLocation() " , e.getMessage());
        }
    }






    private void setDeliveryAddress(String deliveryAddress) {
        acDeliveryAddress.setFocusable(false);
        acDeliveryAddress.setFocusableInTouchMode(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            acDeliveryAddress.setText(deliveryAddress);
        } else {
            acDeliveryAddress.setText(deliveryAddress);
        }
        acDeliveryAddress.setFocusable(true);
        acDeliveryAddress.setFocusableInTouchMode(true);
    }

    /*private void getGeocodeDataFromAddress(final String address) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Const.google.ADDRESS, address);
        hashMap.put(Const.google.KEY, preferenceHelper.getGoogleKey());
        Utils.showCustomProgressDialog(this, false);
        ApiInterface apiInterface = new ApiClient().changeApiBaseUrl(Const.GOOGLE_API_URL).create
                (ApiInterface
                        .class);
        Call<ResponseBody> bodyCall = apiInterface.getGoogleGeocode(hashMap);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Utils.hideCustomProgressDialog();
                HashMap<String, String> map = parseContent.parsGoogleGeocode(response);
                if (map != null) {
                    deliveryLatLng = new LatLng(Double.valueOf(map.get(Const
                            .google.LAT)), Double.valueOf(map.get(Const
                            .google.LNG)));
                   *//* if (deliveryLatLng != null) {
                        isMapTouched = false;
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(deliveryLatLng).zoom(17).build();
                        googleMap.animateCamera(CameraUpdateFactory
                                .newCameraPosition(cameraPosition));
                        locationHelper.setOpenGpsDialog(false);

                    }*//*
                    deliveryAddress = acDeliveryAddress.getText().toString();
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.DELIVERY_LOCATION_ACTIVITY, t);
            }
        });


    }

*/
   /* private void getGeocodeDataFromLocation(final LatLng latLng) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Const.google.LAT_LNG, latLng.getLatitude() + "," + latLng.getLongitude());
        hashMap.put(Const.google.KEY, preferenceHelper.getGoogleKey());
        Utils.showCustomProgressDialog(this, false);
        ApiInterface apiInterface = new ApiClient().changeApiBaseUrl(Const.GOOGLE_API_URL).create
                (ApiInterface
                        .class);
        Call<ResponseBody> bodyCall = apiInterface.getGoogleGeocode(hashMap);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Utils.hideCustomProgressDialog();
                HashMap<String, String> map = parseContent.parsGoogleGeocode(response);
                if (map != null) {
                    deliveryLatLng = latLng;
                    deliveryAddress = map.get(Const.google.FORMATTED_ADDRESS);
                    setDeliveryAddress(map.get(Const.google.FORMATTED_ADDRESS));
                    //    setPlaceFilter(map.get(Const.google.COUNTRY_CODE));
                    //setPlaceFilter("UK");
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.DELIVERY_LOCATION_ACTIVITY, t);
            }
        });


    }*/

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

    }


    /**
     * this method used to called webservice for get Delivery type according to param
     *
     * @param cityLatLng location of city
     */
    private void changeDeliveryAddressAvailable(final LatLng cityLatLng, final String address) {
        final Addresses addresses = new Addresses();
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            addresses.setAddress(address);
            addresses.setCity(currentBooking.getCity1());
            addresses.setAddressType(Const.Type.DESTINATION);
            addresses.setNote("");
            addresses.setUserType(Const.Type.USER);
            ArrayList<Double> location = new ArrayList<>();
            location.add(cityLatLng.getLatitude());
            location.add(cityLatLng.getLongitude());
            addresses.setLocation(location);
            CartUserDetail cartUserDetail = new CartUserDetail();
            cartUserDetail.setEmail(preferenceHelper.getEmail());
            cartUserDetail.setCountryPhoneCode(preferenceHelper.getPhoneCountyCodeCode());
            cartUserDetail.setName(preferenceHelper.getFirstName() + " " + preferenceHelper
                    .getLastName());
            cartUserDetail.setPhone(preferenceHelper.getPhoneNumber());
            addresses.setUserDetails(cartUserDetail);
            ArrayList<Addresses> addresses1 = new ArrayList<>();
            addresses1.add(addresses);
            jsonObject.put(Const.Params.DESTINATION_ADDRESSES, ApiClient.JSONArray(addresses1));
            jsonObject.put(Const.Params.CART_ID, currentBooking.getCartId());


        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.DELIVERY_LOCATION_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.changeDeliveryAddress
                (ApiClient.makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call,
                                   Response<IsSuccessResponse> response) {

                Utils.hideCustomProgressDialog();
                //  AppLog.Log("DELIVERY_CITY", ApiClient.JSONResponse(response.body()));

                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {

                   ParseContent.getInstance().latLng = deliveryLatLng;
                   ParseContent.getInstance().address = acDeliveryAddress.getText().toString();
                    Log.d("setDeliveryLatLng", "Checkout Delivery Activity" + ParseContent.getInstance().latLng);
                   // currentBooking.setDeliveryAddress(address);
                   // currentBooking.setDeliveryLatLng(deliveryLatLng);
                    currentBooking.getDestinationAddresses().clear();
                    currentBooking.setDestinationAddresses(addresses);
                    setResult(Activity.RESULT_OK);
                    finish();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), CheckoutDeliveryLocationActivity.this);
                }
            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.DELIVERY_LOCATION_ACTIVITY, t);
            }
        });
    }

 /*   private void checkLocationPermission(boolean isAnimateLocation, LatLng latLng) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission
                            .ACCESS_FINE_LOCATION, android.Manifest.permission
                            .ACCESS_COARSE_LOCATION},
                    Const.PERMISSION_FOR_LOCATION);
        } else {
            //Do the stuff that requires permission...
            moveCameraFirstMyLocation(isAnimateLocation, latLng);
        }
    }
*/



//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (grantResults.length > 0) {
//            switch (requestCode) {
//                case Const.PERMISSION_FOR_LOCATION:
//                    goWithLocationPermission(grantResults);
//                    break;
//                default:
//                    //do with default
//                    break;
//            }
//        }
//    }

    private void goWithLocationPermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            moveCameraFirstMyLocation(true, null);
        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest
                    .permission.ACCESS_COARSE_LOCATION) && ActivityCompat
                    .shouldShowRequestPermissionRationale(this, android.Manifest
                            .permission.ACCESS_FINE_LOCATION)) {
                openPermissionDialog();
            } else {
                closedPermissionDialog();
            }
        }
    }

    private void openPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogAlert(this, getResources().getString(R.string
                .text_attention), getResources().getString(R.string
                .msg_reason_for_permission_location), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                closedPermissionDialog();
            }

            @Override
            public void onClickRightButton() {
                ActivityCompat.requestPermissions(CheckoutDeliveryLocationActivity.this, new
                        String[]{android
                        .Manifest
                        .permission
                        .ACCESS_FINE_LOCATION, android.Manifest.permission
                        .ACCESS_COARSE_LOCATION}, Const
                        .PERMISSION_FOR_LOCATION);
                closedPermissionDialog();
            }

        };
        customDialogEnable.show();
    }

    private void closedPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            customDialogEnable.dismiss();
            customDialogEnable = null;

        }
    }








    /// Developer S
    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {

    }

    @Override
    public void mapBoxMapReady(final MapboxMap mapboxMap, Style style) {
       /* this.mapboxMap = mapboxMap;
        enableLocationComponent(style);
        mapboxMap.getUiSettings().setAttributionEnabled(false);
        mapboxMap.getUiSettings().setLogoEnabled(false);
        mapboxMap.getUiSettings().setCompassEnabled(false);

        mapboxMap.addOnCameraIdleListener(new MapboxMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng mLatLng = mapboxMap.getCameraPosition().target;
                if (mLatLng != null)
                {
                    Geocoder geocoder = new Geocoder(CheckoutDeliveryLocationActivity.this, Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(
                                mLatLng.getLatitude(),
                                mLatLng.getLongitude(),
                                1);


                            if (addresses != null && addresses.size() > 0 && addresses.get(0) != null)
                            {
                               // Log.e("tagMap", "onResponse: " + addresses.get(0).toString());
                                if (addresses.get(0).getCountryName() != null) {
                                   // Log.e("tagMap", "onResponse: " + addresses.get(0).getCountryName());
                                    if(addresses.get(0).getAddressLine(0)!=null && !addresses.get(0).getAddressLine(0).equals(""))
                                    {
                                        if (isplaceAddress)
                                        {
                                            isplaceAddress = false;

                                        }else{
                                            isplaceAddress = false;
                                            acDeliveryAddress.setText(addresses.get(0).getAddressLine(0));



                                        }

                                    }

                                    //Log.d("call", "========= Location Lat ===========" + addresses.get(0).getLatitude());
                                    //Log.d("call", "========= Location Lon ===========" + addresses.get(0).getLongitude());
                                    str_Latitude = addresses.get(0).getLatitude();
                                    str_Longitude = addresses.get(0).getLongitude();

                                    deliveryLatLng=new LatLng(str_Latitude,str_Longitude);
                                    Log.d("location","===  Changes ======>"+deliveryLatLng);
                                }
                            }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
*/
    }



    public class MainActivityLocationCallback
            implements LocationEngineCallback<LocationEngineResult> {

        private final WeakReference<CheckoutDeliveryLocationActivity> activityWeakReference;

        MainActivityLocationCallback(CheckoutDeliveryLocationActivity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(LocationEngineResult result) {
            CheckoutDeliveryLocationActivity activity = activityWeakReference.get();

            if (activity != null) {
                Location location = result.getLastLocation();

                if (location == null) {
                    return;
                }

                if (!isCurrentLocationGet) {
                    isCurrentLocationGet = true;
                    zoomCamera(new LatLng(location.getLatitude(), location.getLongitude()));
                }
            }
        }

        @Override
        public void onFailure(@NonNull Exception exception) {
          //  Log.d("LocationChangeActivity", exception.getLocalizedMessage());
            CheckoutDeliveryLocationActivity activity = activityWeakReference.get();
            if (activity != null) {
                Toast.makeText(activity, exception.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void zoomCamera(LatLng latLng) {
      /*  mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                new CameraPosition.Builder()
                        .target(latLng)
                        .zoom(14)
                        .build()), 4000);
   */

        deliveryLatLng=latLng;
        map.setCenter(new GeoCoordinate(latLng.getLatitude(), latLng.getLongitude()), Map.Animation.NONE);
        map.setZoomLevel(15);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        //mapView.onResume();
       // mapbox.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        locationHelper.onStart();
        //mapbox.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        locationHelper.onStop();
      //  mapbox.onStop();

    }

    @Override
    protected void onPause() {
        //  mapView.onPause();
        super.onPause();
       // mapbox.onPause();

    }

    @Override
    protected void onDestroy() {
        if (locationEngine != null) {
            locationEngine.removeLocationUpdates(callback);
        }
       // mapbox.onDestroy();
        super.onDestroy();
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            initLocationEngine();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }


    @SuppressLint("MissingPermission")
    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(this);

        mLocationEngineRequest = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        locationEngine.requestLocationUpdates(mLocationEngineRequest, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }


    public void moveCameraFirstMyLocation(boolean isAnimate, LatLng latLng) {
        LatLng latLngOfMyLocation = null;
        if (latLng == null) {
        } else {
            latLngOfMyLocation = latLng;
        }
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLngOfMyLocation).zoom(14).build();
    }

    private void checkLocationPermission(LatLng latLng) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission
                            .ACCESS_FINE_LOCATION, android.Manifest.permission
                            .ACCESS_COARSE_LOCATION},
                    Const.PERMISSION_FOR_LOCATION);
        } else {
         //   moveCameraFirstMyLocation(true, latLng);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
       // permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_LOCATION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        moveCameraFirstMyLocation(true, null);
                    }
                    break;

                case REQUEST_CODE_ASK_PERMISSIONS:
                    for (int index = permissions.length - 1; index >= 0; --index) {
                        if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                            // exit the app if one permission is not granted
                            Toast.makeText(this, "Required permission '" + permissions[index]
                                    + "' not granted, exiting", Toast.LENGTH_LONG).show();
                            finish();
                            return;
                        }
                    }
                    // all permissions were granted
                    // initialize();

                    setheremap();
                    break;


                default:
                    break;
            }
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        /*if (granted) {
            if (mapboxMap.getStyle() != null) {
                enableLocationComponent(mapboxMap.getStyle());
            }
        }*/
    }


    //for here map
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS, grantResults);
        }
    }


    private void setheremap() {

        mapFragment = getMapFragment();
        Utils.showCustomProgressDialog(CheckoutDeliveryLocationActivity.this, false);
        // Set up disk cache path for the map service for this application
        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(
                getApplicationContext().getExternalFilesDir(null) + File.separator + ".here-maps",
                "com.here.android.mpa.service.MapService.v3");
       /* if (!success) {
            Toast.makeText(getApplicationContext(), "Unable to set isolated disk cache path.", Toast.LENGTH_LONG);
        } else {*/
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(
                    final Error error) {
                if (error == Error.NONE) {
                    // retrieve a reference of the map from the map fragment


                    map = mapFragment.getMap();

                    // Set the zoom level to the average between min and max
                    map.setZoomLevel((map.getMaxZoomLevel() + map.getMinZoomLevel()) / 1);
                    currentLocation = locationHelper.getLastLocation();
                    Utils.hideCustomProgressDialog();

                   moveCameraFirstMyLocationZoom();

                    mapFragment.getMapGesture()
                            .addOnGestureListener(new MapGesture.OnGestureListener() {

                                @Override
                                public void onPanStart() {
                                    Log.e("tagMap", "onPanStart()");
                                    //  getCenterMapLocation();
                                }

                                @Override
                                public void onPanEnd() {
                                    Log.e("tagMap", "onPanEnd()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public void onMultiFingerManipulationStart() {
                                    Log.e("tagMap", "onMultiFingerManipulationStart()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public void onMultiFingerManipulationEnd() {
                                    Log.e("tagMap", "onMultiFingerManipulationEnd()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public boolean onMapObjectsSelected(List<ViewObject> list) {
                                    Log.e("tagMap", "onMapObjectsSelected()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public boolean onTapEvent(PointF pointF) {
                                    Log.e("tagMap", "onTapEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public boolean onDoubleTapEvent(PointF pointF) {
                                    Log.e("tagMap", "onDoubleTapEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public void onPinchLocked() {
                                    Log.e("tagMap", "onPinchLocked()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public boolean onPinchZoomEvent(float v, PointF pointF) {
                                    Log.e("tagMap", "onPinchZoomEvent()");
                                    //  getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public void onRotateLocked() {
                                    Log.e("tagMap", "onRotateLocked()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public boolean onRotateEvent(float v) {
                                    Log.e("tagMap", "onRotateEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public boolean onTiltEvent(float v) {
                                    Log.e("tagMap", "onTiltEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public boolean onLongPressEvent(PointF pointF) {
                                    Log.e("tagMap", "onLongPressEvent()");
                                    // getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public void onLongPressRelease() {
                                    Log.e("tagMap", "onLongPressRelease()");
                                    // getCenterMapLocation();
                                }

                                @Override
                                public boolean onTwoFingerTapEvent(PointF pointF) {
                                    Log.e("tagMap", "onTwoFingerTapEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }
                            }, 0, false);
                    // getCenterMapLocation();

                } else {
                    System.out.println("ERROR: Cannot initialize Map Fragment");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(CheckoutDeliveryLocationActivity.this).setMessage(
                                    "Error : " + error.name() + "\n\n" + error.getDetails())
                                    .setTitle(R.string.engine_init_error)
                                    .setNegativeButton(android.R.string.cancel,
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    finishAffinity();
                                                }
                                            }).create().show();
                        }
                    });
                }
            }
        });
        // }


    }

    private void getCenterMapLocation() {
        if (map != null) {

            //  map.setCenter(new GeoCoordinate(currentLatLng.getLatitude(),currentLatLng.getLongitude()),Map.Animation.LINEAR);
            GeoCoordinate coordinate;
            coordinate = map.getCenter();
            ReverseGeocodeRequest revGecodeRequest = new ReverseGeocodeRequest(coordinate);

            revGecodeRequest.execute(new ResultListener<com.here.android.mpa.search.Location>() {
                @Override
                public void onCompleted(com.here.android.mpa.search.Location location, ErrorCode errorCode) {
                    if (errorCode == ErrorCode.NONE) {

//                        etSearch.setText(location.getAddress().toString().replace("\n", " "));
                        String myPlace = "";
                        if (location != null && location.getAddress() != null) {
                            if (!TextUtils.isEmpty(location.getAddress().getText())) {
                                myPlace = location.getAddress().getText();
                            }
                        }
                        setDeliveryAddress(myPlace);

                        LatLng latLng = new LatLng(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude());
                       /* getGeocodeDataFromAddress(location.getAddress().getCountryName(), location.getAddress().getCountryCode(),
                                location.getAddress().getCity(), "", "", latLng,
                                location.getAddress().toString().replace("\\n", ","), location.getAddress().getCity());
                 */   } else {


                        currentBooking.setDeliveryAddress("error code:" + errorCode);
                        setDeliveryAddress("error code:" + errorCode);

                    }
                }
            });
        }
    }

    public void setListViewData(LatLng latLng, String adress) {
            deliveryLatLng=latLng;

        if (searchview.getVisibility() == View.VISIBLE) {
            Utils.hideSoftKeyboard(CheckoutDeliveryLocationActivity.this);
            editsearch.getText().clear();
            listView.setAdapter(null);
            searchview.setVisibility(View.GONE);

        }   //

        Log.i("Deliverylocation", "zoomCamera" + latLng.getLatitude() + latLng.getLongitude());
        map.setCenter(new GeoCoordinate(latLng.getLatitude(), latLng.getLongitude()), Map.Animation.NONE);
        map.setZoomLevel(15);
        Log.i("Deliverylocation", "zoomCamera");

        // GeoCoordinate geoCoordinate=map.getCenter();

        if (!adress.isEmpty()) {
            acDeliveryAddress.setText(adress);
            Log.i("adressfrom adapter", adress);
        }
        GeoCoordinate coordinate;
        coordinate = map.getCenter();
        ReverseGeocodeRequest revGecodeRequest = new ReverseGeocodeRequest(coordinate);

        revGecodeRequest.execute(new ResultListener<com.here.android.mpa.search.Location>() {
            @Override
            public void onCompleted(com.here.android.mpa.search.Location location, ErrorCode errorCode) {
                if (errorCode == ErrorCode.NONE) {

//                        etSearch.setText(location.getAddress().toString().replace("\n", " "));
                    String myPlace = "";
                    if (location != null && location.getAddress() != null) {
                        if (!TextUtils.isEmpty(location.getAddress().getText())) {
                            myPlace = location.getAddress().getText();
                        }
                    }
                    if(isfirstTime){
                        acDeliveryAddress.setText(myPlace);
                        isfirstTime = false;
                    }

                    LatLng latLng = new LatLng(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude());
                   /* getGeocodeDataFromAddress(location.getAddress().getCountryName(), location.getAddress().getCountryCode(),
                            location.getAddress().getCity(), "", "", latLng,
                            location.getAddress().toString().replace("\\n", ","), location.getAddress().getCity());
               */ } else {


                    currentBooking.setDeliveryAddress("error code:" + errorCode);
                    setDeliveryAddress("error code:" + errorCode);

                }
            }
        });
    }



    //heremap autosugestion
    private void doSearch(String query) {
        // setSearchMode(true);
        /*
         Creates new TextAutoSuggestionRequest with current map position as search center
         and selected collection size with applied filters and chosen locale.
         For more details how to use TextAutoSuggestionRequest
         please see documentation for HERE Mobile SDK for Android.
         */
        TextAutoSuggestionRequest textAutoSuggestionRequest = new TextAutoSuggestionRequest(query);

        if (map != null) {
            textAutoSuggestionRequest.setSearchCenter(map.getCenter());
        }
        textAutoSuggestionRequest.setCollectionSize(10);

        // SET UK BASED COUNTRY WISE
      //  textAutoSuggestionRequest.setAddressFilter(new AddressFilter().setCountryCode(Locale.UK.getCountry()));
        //textAutoSuggestionRequest.setAddressFilter(new AddressFilter().setCountryCode("IN"));


      //  textAutoSuggestionRequest.setLocale(Locale.UK);

        /*
           The textAutoSuggestionRequest returns its results to non-UI thread.
           So, we have to pass the UI update with returned results to UI thread.
         */
        textAutoSuggestionRequest.execute(new ResultListener<List<AutoSuggest>>() {
            @Override
            public void onCompleted(final List<AutoSuggest> autoSuggests, ErrorCode errorCode) {
                if (errorCode == errorCode.NONE) {
                    processSearchResults(autoSuggests);
                } else {
                    Log.i("error with doserch", errorCode.toString());
                }
            }
        });
    }

    private void processSearchResults(final List<AutoSuggest> autoSuggests) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                m_autoSuggests.clear();
                m_autoSuggests.addAll(autoSuggests);
             /*   m_autoSuggestAdapter = new AutoSuggestAdapter(CheckoutDeliveryLocationActivity.this, m_autoSuggests);
             //   m_autoSuggestAdapter.notifyDataSetChanged();
                listView.setAdapter(m_autoSuggestAdapter);*/


            }
        });
    }




}
