package com.edelivery;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;
import com.freshchat.consumer.sdk.Freshchat;
import com.livechatinc.inappchat.ChatWindowConfiguration;
import com.livechatinc.inappchat.ChatWindowErrorType;
import com.livechatinc.inappchat.ChatWindowView;
import com.livechatinc.inappchat.models.NewMessageModel;

public class HelpActivity extends BaseAppCompatActivity {

    private LinearLayout llMail, llCall, llchat;
    private CustomFontTextView tvTandC, tvPolicy;

    ChatWindowView emmbeddedChatWindow;
    com.livechatinc.inappchat.ChatWindowConfiguration configuration;
    private Object ChatWindowEventsListener;
    String paramName = "", paramEmail = "";
    Boolean chatStart = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        initLiveChatData();
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_help));
        findViewById();
        setViewListener();

    }

    public void initLiveChatData()
    {
        configureChatWindow();
        initializeAndImplementLiveChat();  //initialize chat
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
//        llCall = (LinearLayout) findViewById(R.id.llCall);
        llchat = (LinearLayout) findViewById(R.id.ll_chat);
        initFreshchat();
        llMail = (LinearLayout) findViewById(R.id.llMail);
        tvTandC = (CustomFontTextView) findViewById(R.id.tvTandC);
        tvPolicy = (CustomFontTextView) findViewById(R.id.tvPolicy);
        tvTandC.setText(Utils.fromHtml
                ("<a href=\"" + preferenceHelper.getTermsANdConditions() + "\"" + ">" +
                        getResources().getString(R.string.text_t_and_c) +
                        "</a>"));
        tvTandC.setMovementMethod(LinkMovementMethod.getInstance());
        tvPolicy.setText(Utils.fromHtml
                ("<a href=\"" + preferenceHelper.getPolicy() + "\"" + ">" +
                        getResources().getString(R.string.text_policy) +
                        "</a>"));
        tvPolicy.setMovementMethod(LinkMovementMethod.getInstance());

    }

    @Override
    protected void setViewListener() {
//        llCall.setOnClickListener(this);
        llchat.setOnClickListener(this);
        llMail.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /*case R.id.llCall:
                makePhoneCallToAdmin();
                break;
            */
            case R.id.ll_chat:
                if (!chatStart) {
                    initializeAndImplementLiveChat();  //initialize chat
                }
                emmbeddedChatWindow.showChatWindow();
                /*Intent intent = new Intent(getActivity(), LiveChatActivity.class);
                startActivity(intent);*/
              /*  Intent mIntent = new Intent(getActivity(), WebViewActivity.class);
                mIntent.putExtra("web", "chat");
                startActivity(mIntent);*/
                /*Freshchat.showConversations(getApplicationContext());*/
                break;
            case R.id.llMail:
                contactUsWithAdmin();
                break;
            default:
                // do with default
                break;
        }
    }

    public void makePhoneCallToAdmin() {
        /*if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest
                    .permission
                    .CALL_PHONE}, Const
                    .PERMISSION_FOR_CALL);
        } else {*/
        //Do the stuff that requires permission...
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + preferenceHelper.getAdminContact()));
        startActivity(intent);
        //  }

    }

    private void openCallPermissionDialog() {

        CustomDialogAlert customDialogAlert = new CustomDialogAlert(this, getResources().getString(R
                .string
                .text_attention), getResources().getString(R
                .string
                .msg_reason_for_call_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
            }

            @Override
            public void onClickRightButton() {
                ActivityCompat.requestPermissions(HelpActivity.this, new String[]{android.Manifest
                        .permission
                        .CALL_PHONE}, Const
                        .PERMISSION_FOR_CALL);
                dismiss();
            }

        };
        customDialogAlert.show();
    }


    private void goWithCallPermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            makePhoneCallToAdmin();

        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (HelpActivity.this, android.Manifest
                            .permission.CALL_PHONE)) {
                openCallPermissionDialog();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CALL:
                    goWithCallPermission(grantResults);
                    break;
                default:
                    //do with default
                    break;
            }
        }

    }


    @Override
    public void onBackPressed() {
        if (emmbeddedChatWindow.isShown()) {
            emmbeddedChatWindow.hideChatWindow();
        }else{
            setResult(Activity.RESULT_CANCELED);
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }



    }

    public void configureChatWindow() {
        emmbeddedChatWindow = findViewById(R.id.embedded_chat_window);

        if (!PreferenceHelper.getInstance(getApplicationContext()).getUserId().equals("")) {
            paramName = PreferenceHelper.getInstance(getApplicationContext()).getFirstName() + " " + PreferenceHelper.getInstance(getApplicationContext()).getLastName();
            if (!PreferenceHelper.getInstance(getApplicationContext()).getEmail().equals("")) {
                paramEmail = PreferenceHelper.getInstance(getApplicationContext()).getEmail();
            }
        } else {
            paramName = "guest";
            paramEmail = "";
        }
        Log.e("name", paramName + paramEmail);


       /* HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("KEY_LICENCE_NUMBER", getString(R.string.livechat_lisense_key));
        *//*hashMap.put("KEY_GROUP_ID", group_id);*//*
        hashMap.put("KEY_VISITOR_NAME", paramName);
        hashMap.put("KEY_VISITOR_EMAIL", paramEmail);
        Log.e("parameter", hashMap.toString());*/
        /* params.put(CUSTOM_PARAM_PREFIX + key, customVariables.get(key))*/

        configuration = new ChatWindowConfiguration(getString(R.string.livechat_lisense_key), "", paramName, paramEmail, null);
    }


    public void initializeAndImplementLiveChat() {
        if (!chatStart) {
            if (!emmbeddedChatWindow.isInitialized()) {
                emmbeddedChatWindow = ChatWindowView.createAndAttachChatWindowInstance(getActivity());
                emmbeddedChatWindow.setUpWindow(configuration);
                emmbeddedChatWindow.setUpListener(new ChatWindowView.ChatWindowEventsListener() {
                    @Override
                    public void onChatWindowVisibilityChanged(boolean visible) {
                        chatStart = true;
                    }

                    @Override
                    public void onNewMessage(NewMessageModel message, boolean windowVisible) {
                        Log.e("chatVisible2", "new_message");
                    }

                    @Override
                    public void onStartFilePickerActivity(Intent intent, int requestCode) {
                        //for upload media in chat
                        intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("*/*");
                        startActivityForResult(intent, requestCode);
                        Log.e("chatVisible3", "pick_file");
                    }

                    @Override
                    public boolean onError(ChatWindowErrorType errorType, int errorCode, String errorDescription) {
                        Log.e("chatVisibleError4", errorDescription);
                        return false;
                    }

                    @Override
                    public boolean handleUri(Uri uri) {
                        Log.e("chatVisible5", uri.toString());
                        return false;
                    }
                });
                emmbeddedChatWindow.initialize();
            }
        }


    }

}
