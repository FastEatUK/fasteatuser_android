package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

import com.edelivery.component.CustomFontButton;
import com.edelivery.utils.Const;

public class ThankYouActivity extends BaseAppCompatActivity {

    private CustomFontButton btnTrackYourOrder;
    private String orderId,storeName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_order_placed));
        findViewById();
        setViewListener();
        if(getIntent()!= null){
            orderId = getIntent().getExtras().getString(Const.Params.ORDER_ID);
            storeName = getIntent().getExtras().getString(Const.Params.STORE_ID);
        }
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {

        btnTrackYourOrder = (CustomFontButton) findViewById(R.id.btnTrackYourOrder);
    }

    @Override
    protected void setViewListener() {
        btnTrackYourOrder.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTrackYourOrder:
                goToOrdersActivity();
                break;

            default:
                // do with default
                break;
        }
    }

    @Override
    public void onBackPressed() {
        goToHomeActivity();
    }

    private void goToOrdersActivity() {
        Intent intent = new Intent(this, OrderTrackActivity.class);
        intent.putExtra(Const.Params.ORDER_ID,orderId);
        intent.putExtra(Const.Params.STORE_ID,storeName);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
