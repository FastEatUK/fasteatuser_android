package com.edelivery;

/*import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.edelivery.adapter.PlaceAutocompleteAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.interfaces.LatLngInterpolator;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.responsemodels.ActiveOrderResponse;
import com.edelivery.models.responsemodels.ProviderLocationResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.mapbox.android.core.location.*;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdate;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import mapboxutils.CustomEventMapBoxView;
import mapboxutils.MapBoxMapReady;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.edelivery.BuildConfig.BASE_URL;
import static com.edelivery.utils.Const.REQUEST_CHECK_SETTINGS;

public class ProviderTrackActivity extends BaseAppCompatActivity implements PermissionsListener, OnMapReadyCallback, MapBoxMapReady {


    public boolean isScheduleStart;
    private ImageView ivRegisterProfileImage, ivCallProvider, ivTargetLocation;
    private CustomFontTextViewTitle tvProviderName;
    private CustomFontTextView tvOrderEstTime, tvProviderRatings;
    private ActiveOrderResponse activeOrderResponse;
    private Order order;
    private CustomFontTextView tvOrderNumber;
    private ScheduledExecutorService schedule;
    private Handler handler;
    private ArrayList<LatLng> markerList;
    private LatLng destinationLatLng;
    private Marker providerMarker, destinationMarker;
    private CustomDialogAlert customDialogAlert;
    private boolean isCameraIdeal = true;
    private MapboxMap mapboxMap;
    private LocationEngine locationEngine;
    private PermissionsManager permissionsManager;
    private long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    private long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
    LocationEngineRequest mLocationEngineRequest;
    private ProviderTrackActivity.MainActivityLocationCallback callback = new ProviderTrackActivity.MainActivityLocationCallback(this);
    private MapView mapbox;
    public com.mapbox.mapboxsdk.geometry.LatLng storeLatLng;
    private boolean isCurrentLocationGet = false;
    private String geojsonSourceLayerId = "geojsonSourceLayerId";
    public static double str_Latitude = 0.0, str_Longitude = 0.0;
    String str_subAdminarea11 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(getApplicationContext(), getResources().getString(R.string
                .MAP_BOX_ACCESS_TOKEN));
        setContentView(R.layout.activity_provider_track);
        MapsInitializer.initialize(getApplicationContext());
        initToolBar();
        findViewById();

        mapbox = findViewById(R.id.mapBoxView);
        mapbox.onCreate(savedInstanceState);
        new CustomEventMapBoxView(getApplicationContext(), mapbox, this);


        setViewListener();
        initHandler();
        getExtraData();
        loadData();
        markerList = new ArrayList<>();

    }

    @Override
    protected void onStart() {
        super.onStart();
        startOderScheduled();
        mapbox.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        stopOrderScheduled();
        mapbox.onStop();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mapbox.onResume();

    }


    @Override
    protected void onPause() {
        super.onPause();
        mapbox.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationEngine != null) {
            locationEngine.removeLocationUpdates(callback);
        }
        mapbox.onDestroy();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapbox.onSaveInstanceState(outState);
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapbox.onLowMemory();
    }


    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        ivRegisterProfileImage = (ImageView) findViewById(R.id.ivRegisterProfileImage);
        ivCallProvider = (ImageView) findViewById(R.id.ivCallProvider);
        tvProviderName = (CustomFontTextViewTitle) findViewById(R.id.tvProviderName);
        tvOrderEstTime = (CustomFontTextView) findViewById(R.id.tvOrderEstTime);
        tvProviderRatings = (CustomFontTextView) findViewById(R.id.tvProviderRatings);
        tvOrderNumber = (CustomFontTextView) findViewById(R.id.tvOrderNumber);
        ivTargetLocation = (ImageView) findViewById(R.id.ivTargetLocation);


    }

    @Override
    protected void setViewListener() {
        ivCallProvider.setOnClickListener(this);
        ivTargetLocation.setOnClickListener(this);

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivCallProvider:
                makePhoneCallToProvider();
                break;
            case R.id.ivTargetLocation:
                if (!markerList.isEmpty()) {
                    setLocationBounds(false, markerList);
                }

                break;
            default:
                break;
        }

    }

    private void getExtraData() {
        if (getIntent().getExtras() != null) {
            activeOrderResponse = getIntent().getParcelableExtra(Const.Params.ORDER);
            order = getIntent().getParcelableExtra(Const.Params.ORDER_ID);
        }

    }

    public void startOderScheduled() {
        if (!isScheduleStart) {
            schedule = Executors.newSingleThreadScheduledExecutor();
            schedule.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    Message message = handler.obtainMessage();
                    handler.sendMessage(message);
                }
            }, 0, Const.ADS_SCHEDULED_SECONDS, TimeUnit
                    .SECONDS);
            AppLog.Log(Const.Tag.PROVIDER_TRACK_ACTIVITY, "Schedule Start");
            isScheduleStart = true;
        }
    }

    public void stopOrderScheduled() {
        if (isScheduleStart) {
            AppLog.Log(Const.Tag.PROVIDER_TRACK_ACTIVITY, "Schedule Stop");
            schedule.shutdown();
            try {
                if (!schedule.awaitTermination(60, TimeUnit.SECONDS)) {
                    schedule.shutdownNow(); // Cancel currently executing tasks
                    if (!schedule.awaitTermination(60, TimeUnit.SECONDS))
                        AppLog.Log(Const.Tag.PROVIDER_TRACK_ACTIVITY, "Pool did not terminate");
                }
            } catch (InterruptedException e) {
                AppLog.handleException(Const.Tag.PROVIDER_TRACK_ACTIVITY, e);
                schedule.shutdownNow();
                Thread.currentThread().interrupt();
            }
            isScheduleStart = false;
        }
    }

    private void initHandler() {

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                getProviderLocation();
            }
        };
    }

    private void getProviderLocation() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, activeOrderResponse.getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.ORDER_ID, order.getId());
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.PROVIDER_TRACK_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProviderLocationResponse> responseCall = apiInterface.getProviderLocation(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<ProviderLocationResponse>() {
            @Override
            public void onResponse(Call<ProviderLocationResponse> call,
                                   Response<ProviderLocationResponse> response) {
                AppLog.Log("PROVIDER_LOCATION", "" + ApiClient.JSONResponse(response.body()));
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        float bearing = (float) response.body().getBearing();
                        LatLng providerLatLng = new LatLng(response.body().getProviderLocation().get
                                (0), response.body().getProviderLocation().get
                                (1));
                        setMarkerOnLocation(providerLatLng,
                                destinationLatLng, bearing, response.body().getMapPinImageUrl());
                        updateCamera(bearing, providerLatLng);
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                ProviderTrackActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderLocationResponse> call, Throwable t) {

            }
        });
    }

    private void setMarkerOnLocation(LatLng providerLatLang, LatLng
            destLatLng, float bearing, String pinUrl) {
        boolean isBounce = false;
        if (providerLatLang != null)
        {
            if  (providerMarker == null) {
                    providerMarker = mapboxMap.addMarker(new MarkerOptions().position(providerLatLang)
                            .title(getResources().getString(R.string
                                    .text_provider_location)
                            ));
                // providerMarker.seta(0.5f, 0.5f);
                if(pinUrl!=null && !pinUrl.equals(""))
                {
                    downloadVehiclePin(pinUrl);
                }
                isBounce = true;
            } else {

                animateMarkerToGB(providerMarker, providerLatLang, new LatLngInterpolator.Linear(),
                        bearing);
            }
            markerList.add(providerLatLang);
        }

        if (destLatLng != null)
        {
            if (destinationMarker == null)
            {
                  *//*  bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(Utils
                            .drawableToBitmap
                                    (AppCompatResources.getDrawable(this, R
                                            .drawable.ic_pin_delivery)));
                    destinationMarker = mapboxMap.addMarker(new MarkerOptions().position(destLatLng)
                            .title(getResources().getString(R.string
                                    .text_drop_location))
                            .icon(bitmapDescriptor)));*//*

                IconFactory iconFactory = IconFactory.getInstance(ProviderTrackActivity.this);
                Icon icon = iconFactory.fromResource(R.drawable.ic_pin_delivery);
                if (destLatLng != null) {
                    destinationMarker = mapboxMap.addMarker(new MarkerOptions()
                            .position(new LatLng(destLatLng))
                        *//*    .title(getResources().getString(R.string
                                    .text_delivery_location_your))*//*
                            .icon(icon));

                }

            } else {

                destinationMarker.setPosition(destLatLng);
            }
            markerList.add(destLatLng);
        }
        if (isBounce) {
            try {
                setLocationBounds(false, markerList);
            } catch (Exception e) {
                AppLog.handleException(Const.Tag.PROVIDER_TRACK_ACTIVITY, e);
            }
        }
    }

    private void animateMarkerToGB(final Marker marker, final LatLng finalPosition, final
    LatLngInterpolator latLngInterpolator, final float bearing)
    {

        if (marker != null) {

            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(finalPosition.getLatitude(), finalPosition
                    .getLongitude());

            //final float startRotation = marker.getRotation();     //commmented   not use so
            final LatLngInterpolator interpolator = new LatLngInterpolator.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(3000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                      *//*  float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition,
                                endPosition));
                        marker.setPosition(newPosition);
                        marker.setAnchor(0.5f, 0.5f);*//*
                    } catch (Exception ex) {
                    }
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    if (marker != null) {
                        marker.remove();
                    }

                    IconFactory iconFactory = IconFactory.getInstance(ProviderTrackActivity.this);
                    Icon icon = iconFactory.fromResource(R.drawable.driver_car);
                    // Add the custom icon marker to the map

                    if (endPosition != null) {
                        mapboxMap.addMarker(new MarkerOptions()
                                .position(new LatLng(endPosition))
                                *//*.title(getResources().getString(R.string
                                        .text_drop_location))*//*
                                .icon(icon));
                    }
                }
            });
            valueAnimator.start();
        }
    }

    private void updateCamera(float bearing, LatLng positionLatLng) {
        if (isCameraIdeal) {
            isCameraIdeal = false;
            CameraPosition oldPos = mapboxMap.getCameraPosition();
            CameraPosition pos = new CameraPosition.Builder(oldPos).bearing(bearing).target
                    (positionLatLng).build();
            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(pos), 3000);
                   *//* new mapboxMap.CancelableCallback() {

                        @Override
                        public void onFinish() {
                            AppLog.Log("CAMERA", "FINISH");
                            isCameraIdeal = true;

                        }

                        @Override
                        public void onCancel() {
                            isCameraIdeal = true;
                            AppLog.Log("CAMERA", "cancelling camera");

                        }
                    });*//*
        }

    }

    private void setLocationBounds(boolean isCameraAnim, ArrayList<LatLng> markerList) {
        LatLngBounds.Builder bounds = new LatLngBounds.Builder();
        int driverListSize = markerList.size();
        for (int i = 0; i < driverListSize; i++)
        {
            bounds.include(markerList.get(i));
            zoomCamera(new LatLng(markerList.get(i).getLatitude(),markerList.get(i).getLongitude()));
        }


       *//* CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds.build(), getResources()
                .getDimensionPixelSize(R.dimen.dimen_map_bounce));
        if (isCameraAnim) {
            mapboxMap.animateCamera(cu);
        } else {
            mapboxMap.moveCamera(cu);
        }*//*
    }

    @SuppressLint("SetTextI18n")
    private void loadData() {
        setTitleOnToolBar(order.getStoreName());
        String orderNumber = getResources().getString(R.string.text_order_number)
                + " " + "#" + activeOrderResponse.getUniqueId();
        tvOrderNumber.setText(orderNumber);
        Glide.with(this).load(BASE_URL + activeOrderResponse.getProviderImage())
                .dontAnimate()
                .placeholder(ResourcesCompat.getDrawable
                        (this
                                .getResources(), R.drawable.placeholder, null)).fallback
                (ResourcesCompat
                        .getDrawable
                                (this.getResources(), R.drawable.placeholder,
                                        null)).into
                (ivRegisterProfileImage);
        tvProviderName.setText(activeOrderResponse.getProviderFirstName() + " " +
                activeOrderResponse.getProviderLastName());
        tvProviderRatings.setText(String.valueOf(activeOrderResponse.getProviderRate()));
        destinationLatLng = new LatLng(activeOrderResponse.getDestinationAddresses().get(0)
                .getLocation().get(0),
                activeOrderResponse.getDestinationAddresses().get(0)
                        .getLocation().get(1));
        tvOrderEstTime.setText(getResources().getString(R.string.text_est_time) + " " + Utils
                .minuteToHoursMinutesSeconds(order
                        .getTotalTime()));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

    }

    public void makePhoneCallToProvider()
    {
       *//* if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest
                    .permission
                    .CALL_PHONE}, Const
                    .PERMISSION_FOR_CALL);
        } else {*//*
        //Do the stuff that requires permission...
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + activeOrderResponse.getProviderCountryPhoneCode()
                + activeOrderResponse.getProviderPhone()));
        startActivity(intent);
        //   }

    }

    private void openCallPermissionDialog() {
        if (customDialogAlert != null && customDialogAlert.isShowing()) {
            return;
        }
        customDialogAlert = new CustomDialogAlert(this, getResources().getString(R
                .string
                .text_attention), getResources().getString(R
                .string
                .msg_reason_for_call_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                closedPermissionDialog();
            }

            @Override
            public void onClickRightButton() {
                ActivityCompat.requestPermissions(ProviderTrackActivity.this, new String[]{Manifest
                        .permission
                        .CALL_PHONE}, Const
                        .PERMISSION_FOR_CALL);
                closedPermissionDialog();
            }

        };
        customDialogAlert.show();
    }

    private void closedPermissionDialog() {
        if (customDialogAlert != null && customDialogAlert.isShowing()) {
            customDialogAlert.dismiss();
            customDialogAlert = null;

        }
    }

    private void goWithCallPermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            makePhoneCallToProvider();

        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (ProviderTrackActivity.this, Manifest
                            .permission.CALL_PHONE)) {
                openCallPermissionDialog();
            } else {
                closedPermissionDialog();

            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.PERMISSION_FOR_CALL:
                makePhoneCallToProvider();
                break;

            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:

                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                }
                break;
            case Const.REQUEST_CODE_AUTOCOMPLETE:
                if (data != null) {
                    //Log.d("call", "========= PlaceAutocomplete Result ==========" + feature.geometry().toString());
                    CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);
                    if(selectedCarmenFeature!=null) {
                        if (mapboxMap != null)
                        {
                            Style style = mapboxMap.getStyle();
                            if (style != null) {
                                GeoJsonSource source = style.getSourceAs(geojsonSourceLayerId);
                                if (source != null) {
                                    source.setGeoJson(FeatureCollection.fromFeatures(
                                            new Feature[]{Feature.fromJson(selectedCarmenFeature.toJson())}));
                                }
                                if(selectedCarmenFeature.geometry()!=null)
                                {
                                    mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                                            new CameraPosition.Builder()
                                                    .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                                            ((Point) selectedCarmenFeature.geometry()).longitude()))
                                                    .zoom(14)
                                                    .build()), 4000);
                                }
                            }
                        }
                    }
                }
                break;
            case Const.REQUEST_CODE:
                break;
            default:
                //do with default
                break;
        }
    }

    public void downloadVehiclePin(String pinUrl) {
        if (!TextUtils.isEmpty(pinUrl))
        {
            Glide.with(this)
                    .asBitmap()
                    .load(BASE_URL + pinUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable
                    .driver_car)
                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            AppLog.handleException(getClass().getSimpleName(), e);
                            AppLog.Log("VEHICLE_PIN", "Download failed");
                            if (providerMarker != null)
                            {

                              *//*  providerMarker.setIcon(BitmapDescriptorFactory.fromBitmap
                                        (Utils.drawableToBitmap(AppCompatResources
                                                .getDrawable(ProviderTrackActivity.this, R.drawable
                                                        .driver_car));*//*

                                IconFactory iconFactory = IconFactory.getInstance(ProviderTrackActivity.this);
                                Icon icon = iconFactory.fromResource(R.drawable.driver_car);
                                providerMarker.setIcon(icon);
                            }
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                            *//*
                            commetd

                            if (providerMarker != null) {
                                providerMarker.setIcon(BitmapDescriptorFactory.fromBitmap
                                        (resource));
                            }*//*

                            if (providerMarker != null)
                            {
                                IconFactory iconFactory = IconFactory.getInstance(ProviderTrackActivity.this);
                                Icon icon = iconFactory.fromResource(R.drawable.driver_car);
                                providerMarker.setIcon(icon);
                            }
                            AppLog.Log("VEHICLE_PIN", "Download Successfully");
                            return true;
                        }


                    }).dontAnimate().override(getResources().getDimensionPixelSize(R
                            .dimen.vehicle_pin_width)
                    , getResources().getDimensionPixelSize(R
                            .dimen.vehicle_pin_height)).preload(getResources()
                            .getDimensionPixelSize(R
                                    .dimen.vehicle_pin_width)
                    , getResources().getDimensionPixelSize(R
                            .dimen.vehicle_pin_height));
        } else {
            if (providerMarker != null) {
           *//*
                commented use

                providerMarker.setIcon(BitmapDescriptorFactory.fromBitmap
                        (Utils.drawableToBitmap(AppCompatResources
                                .getDrawable(ProviderTrackActivity.this, R.drawable
                                        .driver_car))));
           *//*


                IconFactory iconFactory = IconFactory.getInstance(ProviderTrackActivity.this);
                Icon icon = iconFactory.fromResource( R.drawable.driver_car);
                providerMarker.setIcon(icon);
            }
        }


    }


     // developer s
    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            if (mapboxMap.getStyle() != null) {
                enableLocationComponent(mapboxMap.getStyle());
            }
        }
    }

    @Override
    public void mapBoxMapReady(final MapboxMap mapboxMap, Style style) {
        this.mapboxMap = mapboxMap;
        enableLocationComponent(style);
        mapboxMap.getUiSettings().setAttributionEnabled(false);
        mapboxMap.getUiSettings().setLogoEnabled(false);
        mapboxMap.getUiSettings().setCompassEnabled(false);

        mapboxMap.addOnCameraIdleListener(new MapboxMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng mLatLng = mapboxMap.getCameraPosition().target;
                if (mLatLng != null) {
                    Geocoder geocoder = new Geocoder(ProviderTrackActivity.this, Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(
                                mLatLng.getLatitude(),
                                mLatLng.getLongitude(),
                                1);
                        if (addresses != null && addresses.size() > 0 && addresses.get(0) != null) {
                            //Log.e("tagMap", "onResponse: " + addresses.get(0).toString());

                            str_Latitude = addresses.get(0).getLatitude();
                            str_Longitude = addresses.get(0).getLongitude();
                            storeLatLng = new LatLng(str_Latitude, str_Longitude);
                            if (addresses.get(0).getSubAdminArea() != null) {
                                str_subAdminarea11 = addresses.get(0).getSubAdminArea();
                            } else {
                                str_subAdminarea11 = "";
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            initLocationEngine();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }


    @SuppressLint("MissingPermission")
    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(this);

        mLocationEngineRequest = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        locationEngine.requestLocationUpdates(mLocationEngineRequest, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {

    }


    public class MainActivityLocationCallback
            implements LocationEngineCallback<LocationEngineResult> {

        private final WeakReference<ProviderTrackActivity> activityWeakReference;

        MainActivityLocationCallback(ProviderTrackActivity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(LocationEngineResult result) {
            ProviderTrackActivity activity = activityWeakReference.get();

            if (activity != null) {
                Location location = result.getLastLocation();

                if (location == null) {
                    return;
                }

                if (!isCurrentLocationGet) {
                    isCurrentLocationGet = true;
                    // Move map camera to the selected location
                    zoomCamera(new com.mapbox.mapboxsdk.geometry.LatLng(location.getLatitude(), location.getLongitude()));
                }

            }
        }

        @Override
        public void onFailure(@NonNull Exception exception) {
            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
            ProviderTrackActivity activity = activityWeakReference.get();
            if (activity != null) {
                Toast.makeText(activity, exception.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void zoomCamera(com.mapbox.mapboxsdk.geometry.LatLng latLng) {
        if (latLng != null) {
            mapboxMap.animateCamera(com.mapbox.mapboxsdk.camera.CameraUpdateFactory.newCameraPosition(
                    new com.mapbox.mapboxsdk.camera.CameraPosition.Builder()
                            .target(latLng)
                            .zoom(16)
                            .build()), 4000);
        }
    }


    *//* public void moveCameraFirstMyLocation(final boolean isAnimate, LatLng latLng) {

        LatLng latLngOfMyLocation = null;
        if (latLng == null)
        {

        } else {
            latLngOfMyLocation = latLng;
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLngOfMyLocation).zoom(17).build();
        }

    }
*//*
    private void checkLocationPermission(boolean isAnimateLocation, LatLng latLng) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission
                            .ACCESS_FINE_LOCATION, android.Manifest.permission
                            .ACCESS_COARSE_LOCATION},
                    Const.PERMISSION_FOR_LOCATION);
        } else {
            //moveCameraFirstMyLocation(isAnimateLocation, latLng);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode)
            {
                case Const.PERMISSION_FOR_CALL:
                    goWithCallPermission(grantResults);
                    break;
                case Const.PERMISSION_FOR_LOCATION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                       // moveCameraFirstMyLocation(true, null);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
*/

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomEventMapView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.interfaces.LatLngInterpolator;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.responsemodels.ActiveOrderResponse;
import com.edelivery.models.responsemodels.ProviderLocationResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.common.ViewRect;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.Router;
import com.here.android.mpa.routing.RoutingError;
import com.mapbox.android.core.location.*;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdate;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;

import mapboxutils.CustomEventMapBoxView;
import mapboxutils.MapBoxMapReady;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/*import static com.edelivery.BuildConfig.BASE_URL;*/


public class ProviderTrackActivity extends BaseAppCompatActivity implements PermissionsListener, OnMapReadyCallback, MapBoxMapReady {


    public boolean isScheduleStart;
    private ImageView ivRegisterProfileImage, ivCallProvider, ivTargetLocation;
    private CustomFontTextViewTitle tvProviderName;
    private CustomFontTextView tvOrderEstTime, tvProviderRatings;
    private ActiveOrderResponse activeOrderResponse;
    private Order order;
    private CustomFontTextView tvOrderNumber;
    private ScheduledExecutorService schedule;
    private Handler handler;
    private ArrayList<LatLng> markerList = new ArrayList<>();
    private LatLng destinationLatLng;
    private Marker providerMarker, destinationMarker;
    private CustomDialogAlert customDialogAlert;
    private boolean isCameraIdeal = true;
    private MapboxMap mapboxMap;
    private LocationEngine locationEngine;
    private PermissionsManager permissionsManager;
    private long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    private long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
    LocationEngineRequest mLocationEngineRequest;
    private ProviderTrackActivity.MainActivityLocationCallback callback = new ProviderTrackActivity.MainActivityLocationCallback(this);
    private MapView mapbox;
    public com.mapbox.mapboxsdk.geometry.LatLng storeLatLng;
    private boolean isCurrentLocationGet = false;
    private String geojsonSourceLayerId = "geojsonSourceLayerId";
    public static double str_Latitude = 0.0, str_Longitude = 0.0;
    String str_subAdminarea11 = null;

    //for here map
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    private AndroidXMapFragment mapFragment = null;
    private Map map = null;
    MapMarker providermarkerh,destinationmarkerh;
    private GeoCoordinate providercordinates,destinationcordinates;
    MapRoute mapRoute;

    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.heremapview);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* Mapbox.getInstance(getApplicationContext(), getResources().getString(R.string
                .MAP_BOX_ACCESS_TOKEN));*/
        setContentView(R.layout.activity_provider_track);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        checkPermissions();
        initToolBar();
        findViewById();



        /*mapbox = findViewById(R.id.mapBoxView);
        mapbox.onCreate(savedInstanceState);
        new CustomEventMapBoxView(getApplicationContext(), mapbox, this);
*/

        setViewListener();
        initHandler();
        getExtraData();
        loadData();
        markerList = new ArrayList<>();

       /* locationHelper.getLastLocation(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(final Location location) {
                if (location != null) {
                    final LatLng latLng = new LatLng(location.getLatitude(),
                            location.getLongitude());
                    LatLngBounds latLngBounds = new LatLngBounds(
                            latLng, latLng);



                }
            }
        });*/

    }

    @Override
    protected boolean isValidate() {
        // do somethings
        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings
        ivRegisterProfileImage = (ImageView) findViewById(R.id.ivRegisterProfileImage);
        ivCallProvider = (ImageView) findViewById(R.id.ivCallProvider);
        tvProviderName = (CustomFontTextViewTitle) findViewById(R.id.tvProviderName);
        tvOrderEstTime = (CustomFontTextView) findViewById(R.id.tvOrderEstTime);
        tvProviderRatings = (CustomFontTextView) findViewById(R.id.tvProviderRatings);
        tvOrderNumber = (CustomFontTextView) findViewById(R.id.tvOrderNumber);
        ivTargetLocation = (ImageView) findViewById(R.id.ivTargetLocation);


    }

    @Override
    protected void setViewListener() {
        // do somethings
        ivCallProvider.setOnClickListener(this);
        ivTargetLocation.setOnClickListener(this);

    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.ivCallProvider:
                makePhoneCallToProvider();
                break;
            case R.id.ivTargetLocation:
                if (!markerList.isEmpty()) {
                    Log.i("setbound",markerList.get(0).toString());
                    Log.i("setbound",markerList.get(1).toString());
                    setLocationBounds(false, markerList);
                    getProviderLocation();
                }

                break;
            default:
                // do with default
                break;
        }

    }

    private void getExtraData() {
        if (getIntent().getExtras() != null) {
            activeOrderResponse = getIntent().getParcelableExtra(Const.Params.ORDER);
            order = getIntent().getParcelableExtra(Const.Params.ORDER_ID);
        }

    }

    public void startOderScheduled() {
        if (!isScheduleStart) {
            schedule = Executors.newSingleThreadScheduledExecutor();
            schedule.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    Message message = handler.obtainMessage();
                    handler.sendMessage(message);
                }
            }, 0, Const.ADS_SCHEDULED_SECONDS, TimeUnit
                    .SECONDS);
            AppLog.Log(Const.Tag.PROVIDER_TRACK_ACTIVITY, "Schedule Start");
            isScheduleStart = true;
        }
    }

    public void stopOrderScheduled() {
        if (isScheduleStart) {
            AppLog.Log(Const.Tag.PROVIDER_TRACK_ACTIVITY, "Schedule Stop");
            schedule.shutdown(); // Disable new tasks from being submitted
            // Wait a while for existing tasks to terminate
            try {
                if (!schedule.awaitTermination(60, TimeUnit.SECONDS)) {
                    schedule.shutdownNow(); // Cancel currently executing tasks
                    // Wait a while for tasks to respond to being cancelled
                    if (!schedule.awaitTermination(60, TimeUnit.SECONDS))
                        AppLog.Log(Const.Tag.PROVIDER_TRACK_ACTIVITY, "Pool did not terminate");

                }
            } catch (InterruptedException e) {
                AppLog.handleException(Const.Tag.PROVIDER_TRACK_ACTIVITY, e);
                // (Re-)Cancel if current thread also interrupted
                schedule.shutdownNow();
                // Preserve interrupt ProviderStatus
                Thread.currentThread().interrupt();
            }
            isScheduleStart = false;
        }
    }

    private void initHandler() {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                getProviderLocation();


            }
        };
    }

    private void getProviderLocation() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, activeOrderResponse.getProviderId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.ORDER_ID, order.getId());
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.PROVIDER_TRACK_ACTIVITY, e);
        }

        Log.i("getProviderLocation","call");
       // Log.i("getProviderLocation",jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProviderLocationResponse> responseCall = apiInterface.getProviderLocation(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<ProviderLocationResponse>() {
            @Override
            public void onResponse(Call<ProviderLocationResponse> call,
                                   Response<ProviderLocationResponse> response) {
                AppLog.Log("PROVIDER_LOCATION", "" + ApiClient.JSONResponse(response.body()));
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && parseContent.isSuccessful(response)) {
                    String str_mark;
                    if (response.body().isSuccess()) {
                        float bearing = (float) response.body().getBearing();
                        LatLng providerLatLng = new LatLng(response.body().getProviderLocation().get
                                (0), response.body().getProviderLocation().get(1));


                        if (providerLatLng != null) {
                            if (response.body().getMapPinImageUrl() != null) {

                            } else {

                            }
                           setMarkerOnLocation(providerLatLng,
                                    destinationLatLng, bearing, response.body().getMapPinImageUrl());

                            Log.i("beraing", String.valueOf(bearing));
                           //updateCamera(bearing, providerLatLng);
                        }
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                ProviderTrackActivity.this);
                    }


                }


            }

            @Override
            public void onFailure(Call<ProviderLocationResponse> call, Throwable t) {

            }
        });
    }


    private void setMarkerOnLocation(LatLng providerLatLang, LatLng
            destLatLng, float bearing, String pinUrl) {
        Log.i("getProviderLocation","setmarker");
        boolean isBounce = false;
        if (providerLatLang != null) {
            if (providermarkerh == null) {

               /* IconFactory iconFactory = IconFactory.getInstance(ProviderTrackActivity.this);
                Icon icon = iconFactory.fromResource(R.drawable.driver_car);*/
                Image image = new Image();
                try {
                    image.setImageResource(R.drawable.driver_car);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (map != null) {
                   /* providerMarker = mapboxMap.addMarker(new MarkerOptions().position(providerLatLang).icon(icon)
                            .title(getResources().getString(R.string
                                    .text_provider_location)
                            ));*/

                   providermarkerh=new MapMarker();
                   providercordinates=new GeoCoordinate(providerLatLang.getLatitude(),providerLatLang.getLongitude());

                   Log.i("setMarkerOnLocation",providercordinates.toString());
                   providermarkerh.setIcon(image);
                   providermarkerh.setCoordinate(providercordinates);
                   map.addMapObject(providermarkerh);

                   Log.i("setMarkerOnLocation","providermarkerh");



                }
                if (pinUrl != null && !pinUrl.equals("")) {
                    downloadVehiclePin(pinUrl);
                }
                isBounce = true;
            } else {

                animateMarkerToGB(providermarkerh, providerLatLang, new LatLngInterpolator.Linear(),
                        bearing);
            }
            markerList.add(providerLatLang);

            if (destLatLng != null) {
                if (destinationmarkerh == null) {

                    IconFactory iconFactory = IconFactory.getInstance(ProviderTrackActivity.this);
                    Icon icon = iconFactory.fromResource(R.drawable.ic_pin_delivery);
                    Image image = new Image();
                    try {
                        image.setImageResource(R.drawable.ic_pin_delivery);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (map != null) {
                        /*destinationMarker = mapboxMap.addMarker(new MarkerOptions().position(destLatLng)
                                .title(getResources().getString(R.string
                                        .text_drop_location))
                                .icon(icon));*/

                        destinationmarkerh=new MapMarker();
                        destinationcordinates=new GeoCoordinate(destLatLng.getLatitude(),destLatLng.getLongitude());
                        destinationmarkerh.setIcon(image);
                        destinationmarkerh.setCoordinate(destinationcordinates);
                        map.addMapObject(destinationmarkerh);
                        Log.i("setMarkerOnLocation",destinationcordinates.toString());
                        Log.i("setMarkerOnLocation","destinationmarkerh");
                    }
                } else {
                    destinationmarkerh.setCoordinate(destinationcordinates);
                }
                markerList.add(destLatLng);
                //setLocationBounds(false, markerList);
               // Log.i("setbound",markerList.toString());

            }
            if (isBounce) {
                try {
                    setLocationBounds(false, markerList);
                    Log.i("setbound",markerList.get(0).toString());
                    Log.i("setbound",markerList.get(1).toString());
                } catch (Exception e) {
                    AppLog.handleException(Const.Tag.PROVIDER_TRACK_ACTIVITY, e);

                }
            }



        }

    }

    private void animateMarkerToGB(final MapMarker marker, final LatLng finalPosition, final
    LatLngInterpolator latLngInterpolator, final float bearing) {
        Log.i("getProviderLocation","animateMarkerToGB");

        if (marker != null) {

            final GeoCoordinate startPosition = marker.getCoordinate();
            final LatLng endPosition = new LatLng(finalPosition.getLatitude(), finalPosition
                    .getLongitude());

            final LatLngInterpolator interpolator = new LatLngInterpolator.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(3000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {


                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, new LatLng(startPosition.getLatitude(),startPosition.getLongitude()),
                                endPosition);

                        marker.setCoordinate(new GeoCoordinate(newPosition.getLatitude(),newPosition.getLongitude()));
                        Image image = new Image();
                        try {
                            image.setImageResource(R.drawable.driver_car);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        marker.setIcon(image);
                        map.addMapObject(marker);
                        //  marker.setAnchor(0.5f, 0.5f);
                    } catch (Exception ex) {
                        //I don't care atm..
                    }


                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);


                }
            });
            valueAnimator.start();
        }
    }

    private void updateCamera(float bearing, LatLng positionLatLng) {
        if (isCameraIdeal) {
            isCameraIdeal = false;

            if (positionLatLng != null) {

                if (mapboxMap != null) {
                    CameraPosition oldPos = mapboxMap.getCameraPosition();
                    CameraPosition pos = new CameraPosition.Builder(oldPos).bearing(bearing).target
                            (positionLatLng).build();
                    mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(pos), 3000,
                            new MapboxMap.CancelableCallback() {

                                @Override
                                public void onFinish() {
                                    AppLog.Log("CAMERA", "FINISH");
                                    isCameraIdeal = true;

                                }

                                @Override
                                public void onCancel() {
                                    isCameraIdeal = true;
                                    AppLog.Log("CAMERA", "cancelling camera");

                                }
                            });
                }
            }
        }

    }

    private void setLocationBounds(boolean isCameraAnim, ArrayList<LatLng> markerList) {
      //  LatLngBounds.Builder bounds = new LatLngBounds.Builder();
        GeoCoordinate start,end;
        List<GeoCoordinate> geoCoordinateList=new ArrayList<>();
        for (int i=0;i<markerList.size();i++)
        {   try {
                geoCoordinateList.add(new GeoCoordinate(markerList.get(i).getLatitude(),markerList.get(i).getLongitude()));
                Log.i("geoCoordinateList.add","ok");
            }
            catch (Exception e)
            {
                Log.i("geoCoordinateList.add",e.toString());
            }
        }
        try {
            start=new GeoCoordinate(geoCoordinateList.get(0));
            end=new GeoCoordinate(geoCoordinateList.get(1));
            GeoBoundingBox geoBoundingBox=GeoBoundingBox.getBoundingBoxContainingGeoCoordinates(geoCoordinateList);
            map.zoomTo(geoBoundingBox,map.getWidth()-150,map.getHeight()-150,Map.Animation.LINEAR,0);
            map.setZoomLevel(14);
          //  showrout(start,end);

            Log.i("map.setCenter","done");
        }
        catch (Exception e)
        {
            Log.i("map.setCenter",e.toString());
        }
    }

    private void loadData() {
        setTitleOnToolBar(order.getStoreName());
        String orderNumber = getResources().getString(R.string.text_order_number)
                + " " + "#" + activeOrderResponse.getUniqueId();
        tvOrderNumber.setText(orderNumber);
        Glide.with(this).load(PreferenceHelper.getInstance(getActivity()).getIMAGE_BASE_URL() + activeOrderResponse.getProviderImage())
                .dontAnimate()
                .placeholder(ResourcesCompat.getDrawable
                        (this
                                .getResources(), R.drawable.placeholder, null)).fallback
                (ResourcesCompat
                        .getDrawable
                                (this.getResources(), R.drawable.placeholder,
                                        null)).into
                (ivRegisterProfileImage);
        tvProviderName.setText(activeOrderResponse.getProviderFirstName() + " " +
                activeOrderResponse.getProviderLastName());


        String str_total_second = String.valueOf(activeOrderResponse.getTotal_sec());
        //  String str_total_second= String.valueOf(order.getTotal_sec());
        Log.d("str_final", "====Final Result ===> " + str_total_second);
        if (str_total_second != null && !str_total_second.equals("")) {
            tvOrderEstTime.setText(getResources().getString(R.string.text_est_time) + " " + Utils
                    .convertToHHMMSSFormate(Double.parseDouble(str_total_second)));
        }


        tvProviderRatings.setText(String.valueOf(activeOrderResponse.getProviderRate()));
        destinationLatLng = new LatLng(activeOrderResponse.getDestinationAddresses().get(0)
                .getLocation().get(0),
                activeOrderResponse.getDestinationAddresses().get(0)
                        .getLocation().get(1));


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

    }

    public void makePhoneCallToProvider() {
       /* if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest
                    .permission
                    .CALL_PHONE}, Const
                    .PERMISSION_FOR_CALL);
        } else {*/
        //Do the stuff that requires permission...
        String phoneno=activeOrderResponse.getProviderPhone();
        String[] prefix={"+440","+44","+91","+910","+","00","0"};
        for (int i=0;i<prefix.length;i++)
        {
            if (phoneno.startsWith(prefix[i]))
            {
                Log.i("startsWith(prefix[i])", prefix[i]);

                 phoneno= phoneno.substring(prefix[i].length());
                 i=0;

            }
        }


        Log.i("startsWith(prefix[i])", phoneno);

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + "0"
                + phoneno));
        startActivity(intent);
        //   }

    }

    private void openCallPermissionDialog() {
        if (customDialogAlert != null && customDialogAlert.isShowing()) {
            return;
        }
        customDialogAlert = new CustomDialogAlert(this, getResources().getString(R
                .string
                .text_attention), getResources().getString(R
                .string
                .msg_reason_for_call_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                closedPermissionDialog();
            }

            @Override
            public void onClickRightButton() {
                ActivityCompat.requestPermissions(ProviderTrackActivity.this, new String[]{Manifest
                        .permission
                        .CALL_PHONE}, Const
                        .PERMISSION_FOR_CALL);
                closedPermissionDialog();
            }

        };
        customDialogAlert.show();
    }

    private void closedPermissionDialog() {
        if (customDialogAlert != null && customDialogAlert.isShowing()) {
            customDialogAlert.dismiss();
            customDialogAlert = null;

        }
    }

    private void goWithCallPermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            makePhoneCallToProvider();

        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (ProviderTrackActivity.this, Manifest
                            .permission.CALL_PHONE)) {
                openCallPermissionDialog();
            } else {
                closedPermissionDialog();

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.PERMISSION_FOR_CALL:
                makePhoneCallToProvider();
                break;
            default:
                //do with default
                break;
        }
    }

    public void downloadVehiclePin(String pinUrl) {
        if (!TextUtils.isEmpty(pinUrl)) {
            Glide.with(this)
                    .asBitmap()
                    .load(PreferenceHelper.getInstance(getApplicationContext()).getIMAGE_BASE_URL() + pinUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable
                    .driver_car)
                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            AppLog.handleException(getClass().getSimpleName(), e);
                            AppLog.Log("VEHICLE_PIN", "Download failed");
                            if (providermarkerh != null) {

                               /* IconFactory iconFactory = IconFactory.getInstance(ProviderTrackActivity.this);
                                Icon icon = iconFactory.fromResource(R.drawable.driver_car);*/

                                Image image = new Image();

                                try {
                                    image.setImageResource(R.drawable.driver_car);
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                                providermarkerh.setIcon(image);


                            }
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {

                            if (providermarkerh != null) {
                                Image image = new Image();

                                try {
                                    image.setImageResource(R.drawable.driver_car);
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                                providermarkerh.setIcon(image);
                            }

                            AppLog.Log("VEHICLE_PIN", "Download Successfully");
                            return true;
                        }


                    }).dontAnimate().override(getResources().getDimensionPixelSize(R
                            .dimen.vehicle_pin_width)
                    , getResources().getDimensionPixelSize(R
                            .dimen.vehicle_pin_height)).preload(getResources()
                            .getDimensionPixelSize(R
                                    .dimen.vehicle_pin_width)
                    , getResources().getDimensionPixelSize(R
                            .dimen.vehicle_pin_height));
        } else {
            if (providermarkerh != null) {
                Image image = new Image();

                try {
                    image.setImageResource(R.drawable.driver_car);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                providermarkerh.setIcon(image);
            }
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        startOderScheduled();
       // mapbox.onStart();
       // mapFragment.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        stopOrderScheduled();
       // mapbox.onStop();
      //  mapFragment.onStop();

    }

    @Override
    protected void onResume() {
        super.onResume();
       // mapbox.onResume();
      //  mapFragment.onResume();

    }


    @Override
    protected void onPause() {
        super.onPause();
       // mapbox.onPause();
       // mapFragment.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationEngine != null) {
            locationEngine.removeLocationUpdates(callback);
        }
      // mapbox.onDestroy();

       // mapFragment.onDestroy();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
       // mapbox.onSaveInstanceState(outState);
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
       // mapbox.onLowMemory();
    }


    // developer s
    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
       /* if (granted) {
            if (mapboxMap.getStyle() != null) {
                enableLocationComponent(mapboxMap.getStyle());
            }
        }*/
    }

    @Override
    public void mapBoxMapReady(final MapboxMap mapboxMap, Style style) {
      /*  this.mapboxMap = mapboxMap;
        enableLocationComponent(style);
        mapboxMap.getUiSettings().setAttributionEnabled(false);
        mapboxMap.getUiSettings().setLogoEnabled(false);
        mapboxMap.getUiSettings().setCompassEnabled(false);

        mapboxMap.addOnCameraIdleListener(new MapboxMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng mLatLng = mapboxMap.getCameraPosition().target;
                if (mLatLng != null) {
                    Geocoder geocoder = new Geocoder(ProviderTrackActivity.this, Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(
                                mLatLng.getLatitude(),
                                mLatLng.getLongitude(),
                                1);
                        if (addresses != null && addresses.size() > 0 && addresses.get(0) != null) {
                            //Log.e("tagMap", "onResponse: " + addresses.get(0).toString());

                            str_Latitude = addresses.get(0).getLatitude();
                            str_Longitude = addresses.get(0).getLongitude();
                            storeLatLng = new LatLng(str_Latitude, str_Longitude);
                            if (addresses.get(0).getSubAdminArea() != null) {
                                str_subAdminarea11 = addresses.get(0).getSubAdminArea();
                            } else {
                                str_subAdminarea11 = "";
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });*/
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            initLocationEngine();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }


    @SuppressLint("MissingPermission")
    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(this);

        mLocationEngineRequest = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        locationEngine.requestLocationUpdates(mLocationEngineRequest, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {

    }


    public class MainActivityLocationCallback
            implements LocationEngineCallback<LocationEngineResult> {

        private final WeakReference<ProviderTrackActivity> activityWeakReference;

        MainActivityLocationCallback(ProviderTrackActivity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(LocationEngineResult result) {
            ProviderTrackActivity activity = activityWeakReference.get();

            if (activity != null) {
                Location location = result.getLastLocation();

                if (location == null) {
                    return;
                }

                if (!isCurrentLocationGet) {
                    isCurrentLocationGet = true;
                    // Move map camera to the selected location
                    zoomCamera(new LatLng(location.getLatitude(), location.getLongitude()));
                }

            }
        }

        @Override
        public void onFailure(@NonNull Exception exception) {
            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
            ProviderTrackActivity activity = activityWeakReference.get();
            if (activity != null) {
                Toast.makeText(activity, exception.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void zoomCamera(LatLng latLng) {
       /* if (latLng != null) {
            mapboxMap.animateCamera(com.mapbox.mapboxsdk.camera.CameraUpdateFactory.newCameraPosition(
                    new com.mapbox.mapboxsdk.camera.CameraPosition.Builder()
                            .target(latLng)
                            .zoom(14)
                            .build()), 4000);
        }*/
        Log.i("zoomCamera(LatLng)",latLng.toString());
        if (latLng != null) {



            map.setCenter(new GeoCoordinate(latLng.getLatitude(), latLng.getLongitude()), Map.Animation.LINEAR);
            map.setZoomLevel(14.5);


        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
       // permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CALL:
                    goWithCallPermission(grantResults);
                    break;
                case Const.PERMISSION_FOR_LOCATION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    }
                    break;


                case REQUEST_CODE_ASK_PERMISSIONS:
                    for (int index = permissions.length - 1; index >= 0; --index) {
                        if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                            // exit the app if one permission is not granted
                            Toast.makeText(this, "Required permission '" + permissions[index]
                                    + "' not granted, exiting", Toast.LENGTH_LONG).show();
                            finish();
                            return;
                        }
                    }
                    // all permissions were granted
                    // initialize();

                    setheremap();
                    break;
                default:
                    break;
            }
        }
    }

    //for here map
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS, grantResults);
        }
    }


    private void setheremap() {

        mapFragment = getMapFragment();
        Utils.showCustomProgressDialog(ProviderTrackActivity.this, false);
        // Set up disk cache path for the map service for this application
        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(
                getApplicationContext().getExternalFilesDir(null) + File.separator + ".here-maps",
                "com.here.android.mpa.service.MapService.v3");
        /*if (!success) {
            Toast.makeText(getApplicationContext(), "Unable to set isolated disk cache path.", Toast.LENGTH_LONG);
        } else {*/
           // map.setProjectionMode(Map.Projection.MERCATOR);

            mapFragment.init(new OnEngineInitListener() {
                @Override
                public void onEngineInitializationCompleted(
                        final Error error) {
                    if (error == Error.NONE) {
                        // retrieve a reference of the map from the map fragment


                        map = mapFragment.getMap();

                        // Set the zoom level to the average between min and max
                        map.setZoomLevel((map.getMaxZoomLevel() + map.getMinZoomLevel()) / 2);
                       // map.setCenter(new GeoCoordinate(currentBooking.getDeliveryLatLng().latitude,currentBooking.getDeliveryLatLng().longitude),Map.Animation.LINEAR);
                        //getProviderLocation();
                        Utils.hideCustomProgressDialog();


                    } else {
                        Utils.hideCustomProgressDialog();
                        System.out.println("ERROR: Cannot initialize Map Fragment");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new AlertDialog.Builder(ProviderTrackActivity.this).setMessage(
                                        "Error : " + error.name() + "\n\n" + error.getDetails())
                                        .setTitle(R.string.engine_init_error)
                                        .setNegativeButton(android.R.string.cancel,
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(
                                                            DialogInterface dialog,
                                                            int which) {
                                                        finishAffinity();
                                                    }
                                                }).create().show();
                            }
                        });
                    }
                }
            });
            // }


        }


    private void showrout(GeoCoordinate start, GeoCoordinate end)
    {
        // Declare the variable (the CoreRouter)

        CoreRouter router = new CoreRouter();


        // Create the RoutePlan and add two waypoints
        RoutePlan routePlan = new RoutePlan();
        routePlan.addWaypoint(new RouteWaypoint(start));
        routePlan.addWaypoint(new RouteWaypoint(end));


        // Create the RouteOptions and set its transport mode & routing type
        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
        routeOptions.setRouteType(RouteOptions.Type.FASTEST);

        routePlan.setRouteOptions(routeOptions);

        router.calculateRoute(routePlan, new Router.Listener<List<RouteResult>, RoutingError>() {
            @Override
            public void onProgress(int i) {
                    Utils.showCustomProgressDialog(ProviderTrackActivity.this,false);
            }

            @Override
            public void onCalculateRouteFinished(List<RouteResult> routeResults, RoutingError routingError) {
                if (routingError == RoutingError.NONE) {
                    Utils.hideCustomProgressDialog();
                    // Render the route on the map
                    mapRoute = new MapRoute(routeResults.get(0).getRoute());
                    map.addMapObject(mapRoute);
                    mapRoute.setRenderType(MapRoute.RenderType.SECONDARY);
                    mapRoute.setManeuverNumberVisible(true);
                }
                else {
                    Utils.hideCustomProgressDialog();
                    // Display a message indicating route calculation failure
                }
            }
        });
    }


}

