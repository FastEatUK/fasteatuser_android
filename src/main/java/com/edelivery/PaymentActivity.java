package com.edelivery;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import android.os.Handler;
import android.util.Log;

import com.edelivery.models.datamodels.Card;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.SwitchCompat;

import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.edelivery.adapter.ViewPagerAdapter;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.fragments.CashFragment;
import com.edelivery.fragments.PayPalFragment;
import com.edelivery.fragments.StripeFragment;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.datamodels.PaymentGateway;
import com.edelivery.models.datamodels.Provider;
import com.edelivery.models.responsemodels.AvailableProviderResponse;
import com.edelivery.models.responsemodels.CreateOrederResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.PaymentGatewayResponse;
import com.edelivery.models.responsemodels.WalletResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.jakewharton.rxbinding2.view.RxView;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentMethodCreateParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends BaseAppCompatActivity {

    public CustomFontEditTextView etWalletAmount;
    public List<PaymentGateway> paymentGateways = new ArrayList<>();
    ;
    public PaymentGateway gatewayItem;
    private SwitchCompat switchIsWalletUse;
    private CustomFontTextView tvWalletAmount, tvAddWalletAmount;
    private ViewPagerAdapter viewPagerAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private CustomFontButton btnPayNow;
    private CustomFontTextView tvTagPay;
    private CustomFontEditTextView etAddCVV;
    private boolean isComingFromCheckout;
    private ArrayList<Provider> availableProviderList = new ArrayList<>();
    public String clientSecret;
    private Card selectCard;
    public ArrayList<Card> cardList;
    private Stripe mStripe;
    private Dialog addCardDialog, addCVVDialog;

    private Order order;
    StripeFragment stripeFragment=new StripeFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_payments));

        /*FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplicationContext());*/
        logger = (AppEventsLogger) AppEventsLogger.newLogger(PaymentActivity.this);

        //  setToolbarRightIcon3(R.drawable.ic_history, this);
        findViewById();
        getExtraData();
        setViewListener();
        getPaymentGateWays();
        availableProviderList = new ArrayList<>();
        order = new Order();
    }

    @Override
    protected boolean isValidate() {
        // do somethings
        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings
        switchIsWalletUse = (SwitchCompat) findViewById(R.id.switchIsWalletUse);
        tvWalletAmount = (CustomFontTextView) findViewById(R.id.tvWalletAmount);
        tvAddWalletAmount = (CustomFontTextView) findViewById(R.id.tvAddWalletAmount);
        etWalletAmount = (CustomFontEditTextView) findViewById(R.id.etWalletAmount);
        tabLayout = (TabLayout) findViewById(R.id.paymentTabsLayout);
        viewPager = (ViewPager) findViewById(R.id.paymentViewpager);
        initTabLayout(viewPager);
        btnPayNow = (CustomFontButton) findViewById(R.id.btnPayNow);
        tvTagPay = (CustomFontTextView) findViewById(R.id.tvTagPay);
        CustomFontTextView tag2 = (CustomFontTextView) findViewById(R.id.tag2);
        CustomFontTextView tag1 = (CustomFontTextView) findViewById(R.id.tag1);
        Utils.setTagBackgroundRtlView(this, tag1);
        Utils.setTagBackgroundRtlView(this, tag2);
    }

    @Override
    protected void  setViewListener() {
        // do somethings
        tvAddWalletAmount.setOnClickListener(this);
        switchIsWalletUse.setOnClickListener(this);
        RxView.clicks(btnPayNow)
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(Object o) throws Exception {
                        new Handler(PaymentActivity.this.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                checkAvailableProvider();
                            }
                        });

                    }
                });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setSelectedPaymentGateway(tab.getText().toString());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                // do somethings
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // do somethings

            }
        });
        etWalletAmount.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() ==
                        KeyEvent.ACTION_UP) {
                    updateUiForWallet(false);
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();

    }


    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.tvAddWalletAmount:
                selectPaymentGatewayForAddWalletAmount();
                break;
            case R.id.switchIsWalletUse:
                toggleWalletUse();
                break;
            case R.id.btnPayNow:
                //checkAvailableProvider();
                //  checkPaymentGatewayForPayOrder();
                break;
            case R.id.ivToolbarRightIcon3:
                goToWalletHistoryActivity();
                break;
            default:
                // do with default
                break;
        }
    }

    private void updateUiForWallet(boolean isUpdate) {
        if (isUpdate) {
            tvWalletAmount.setVisibility(View.GONE);
            etWalletAmount.setVisibility(View.VISIBLE);
            etWalletAmount.requestFocus();
            tvAddWalletAmount.setText(getResources().getString(R.string.text_submit));
        } else {
            etWalletAmount.getText().clear();
            etWalletAmount.setVisibility(View.GONE);
            tvWalletAmount.setVisibility(View.VISIBLE);
            tvAddWalletAmount.setText(getResources().getString(R.string.text_add));
        }

    }

    /**
     * this method called a webservice for toggleWalletUse
     */
    private void toggleWalletUse() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper
                    .getUserId());
            jsonObject.put(Const.Params.IS_USE_WALLET, switchIsWalletUse.isChecked());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.toggleWalletUse(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    switchIsWalletUse.setChecked(response.body().isWalletUse());
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), PaymentActivity
                            .this);
                    switchIsWalletUse.setChecked(!switchIsWalletUse.isChecked());
                }

            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                switchIsWalletUse.setChecked(!switchIsWalletUse.isChecked());

            }
        });

    }

    /**
     * this method called a webservice for  add wallet amount
     */
    public void addWalletAmount(String cardId, String paymentId) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper
                    .getUserId());
            jsonObject.put(Const.Params.TYPE, Const.Type.USER);
            jsonObject.put(Const.Params.CARD_ID, cardId);
            jsonObject.put(Const.Params.PAYMENT_ID, paymentId);
            jsonObject.put(Const.Params.WALLET, Double.valueOf(etWalletAmount.getText().toString
                    ()));

            Utils.showCustomProgressDialog(this, false);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<WalletResponse> responseCall = apiInterface.getAddWalletAmount(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<WalletResponse>() {
                @Override
                public void onResponse(Call<WalletResponse> call, Response<WalletResponse>
                        response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        updateUiForWallet(false);
                        setWalletAmount(response.body().getWallet(), response.body()
                                .getWalletCurrencyCode());
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), PaymentActivity
                                .this);
                    }

                }

                @Override
                public void onFailure(Call<WalletResponse> call, Throwable t) {
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                }
            });

        } catch (NumberFormatException e) {

            etWalletAmount.setError(getResources().getString(R.string.msg_enter_valid_amount));
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

    }


    /**
     * this method called a webservice to get All Payment Gateway witch is available from admin
     * panel
     */
    private void getPaymentGateWays() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper
                    .getUserId());
            jsonObject.put(Const.Params.COUNTRY, currentBooking.getCountry());
            jsonObject.put(Const.Params.COUNTRY_CODE, currentBooking.getCountryCode());
            jsonObject.put(Const.Params.COUNTRY_CODE_2, currentBooking.getCountryCode2());
            jsonObject.put(Const.Params.LATITUDE, currentBooking.getPaymentLatitude());

            jsonObject.put(Const.Params.LONGITUDE, currentBooking.getPaymentLongitude());
            jsonObject.put(Const.Params.CITY1, currentBooking.getCity1());
            jsonObject.put(Const.Params.CITY2, currentBooking.getCity2());
            jsonObject.put(Const.Params.CITY3, currentBooking.getCity3());
            jsonObject.put(Const.Params.CITY_CODE, currentBooking.getCityCode());
            jsonObject.put(Const.Params.TYPE, Const.Type.USER);
            if (btnPayNow.getVisibility() == View.VISIBLE) {
                jsonObject.put(Const.Params.CITY_ID, currentBooking.getCartCityId());
            } else {
                jsonObject.put(Const.Params.CITY_ID, "");
            }

            jsonObject.put(Const.Params.ORDER_PAYMENT_ID, currentBooking.getOrderPaymentId());
            jsonObject.put(Const.Params.AMOUNT_ID, currentBooking.getTotalInvoiceAmount());
            jsonObject.put(Const.Params.COUNTRY_CURRENCY, currentBooking.getCurrency());
            jsonObject.put(Const.Params.COUNTRY_CURRENCY_CODE, currentBooking.getCurrencyCode());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

        Log.i("getPaymentGateWays",jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentGatewayResponse> responseCall = apiInterface.getPaymentGateway(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<PaymentGatewayResponse>() {
            @Override
            public void onResponse(Call<PaymentGatewayResponse> call,
                                   Response<PaymentGatewayResponse> response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {

                    AppLog.Log("PAYMENT_GATEWAY", ApiClient.JSONResponse(response.body()));
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        paymentGateways = new ArrayList<>();
                        switchIsWalletUse.setChecked(response.body().isUseWallet());
                        currentBooking.setCurrencyCode(response.body().getWalletCurrencyCode());
                        setWalletAmount(response.body().getWalletAmount(), response.body()
                                .getWalletCurrencyCode());

                        currentBooking.setCashPaymentMode(response.body().isCashPaymentMode());
                        if (currentBooking.isCashPaymentMode()) {
                            PaymentGateway paymentGateway = new PaymentGateway();
                            paymentGateway.setName(getResources().getString(R.string
                                    .text_cash));
                            paymentGateway.setId(Const.Payment.CASH);
                            paymentGateway.setPaymentModeCash(true);
                            paymentGateway.setPaymentKey("");
                            paymentGateway.setPaymentKeyId("");
                            paymentGateways.add(paymentGateway);
                        }
                        if (response.body().getPaymentGateway() != null) {
                            paymentGateways.addAll(response.body().getPaymentGateway());
                        }
                       /* if (response.body().getPaymentIntent() != null
                                && !TextUtils.isEmpty(response.body().getPaymentIntent().getClient_secret())) {
                            clientSecret = response.body().getPaymentIntent().getClient_secret();
                        }*/
                        initTabLayout(viewPager);
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), PaymentActivity
                                .this);
                    }
                }
            }

            @Override
            public void onFailure(Call<PaymentGatewayResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    private void initTabLayout(ViewPager viewPager) {
        if (!paymentGateways.isEmpty()) {
            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

            for (PaymentGateway paymentGateway : paymentGateways) {
                if (TextUtils.equals(paymentGateway.getId(), Const.Payment.CASH) &&
                        isComingFromCheckout) {
                    viewPagerAdapter.addFragment(new CashFragment(), getResources().getString(R
                            .string
                            .text_cash));
                }
                if (TextUtils.equals(paymentGateway.getId(), Const.Payment.STRIPE)) {
                    viewPagerAdapter.addFragment(new StripeFragment(), paymentGateway.getName
                            ());
                }
                if (TextUtils.equals(paymentGateway.getId(), Const.Payment.PAY_PAL)) {
                    viewPagerAdapter.addFragment(new PayPalFragment(), paymentGateway.getName
                            ());
                }
            }
            viewPager.setAdapter(viewPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    private void getExtraData() {
        if (getIntent() != null) {
            isComingFromCheckout = getIntent().getExtras().getBoolean(Const.Tag.PAYMENT_ACTIVITY);
            if (isComingFromCheckout) {
                btnPayNow.setVisibility(View.VISIBLE);
                tvTagPay.setVisibility(View.VISIBLE);
            } else {
                btnPayNow.setVisibility(View.GONE);
                tvTagPay.setVisibility(View.GONE);
            }

        }

    }

    /**
     * this method called a webservice fro make payment for delivery order
     */
    private void payOrderPayment() {
        if (gatewayItem != null) {
            Utils.showCustomProgressDialog(this, false);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
                jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
                jsonObject.put(Const.Params.IS_PAYMENT_MODE_CASH, gatewayItem.isPaymentModeCash());
                jsonObject.put(Const.Params.PAYMENT_ID, gatewayItem.getId());
                jsonObject.put(Const.Params.ORDER_PAYMENT_ID, currentBooking
                        .getOrderPaymentId());
            } catch (JSONException e) {
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
            }

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<IsSuccessResponse> responseCall = apiInterface.payOrderPayment(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        if (response.body().isPaymentPaid()) {
                            Utils.showMessageToast(response.body().getMessage(), PaymentActivity
                                    .this);

                            logAddedPaymentInfoEvent(true);

                            createOrder();
                        } else {
                            Utils.showToast(getResources().getString(R.string
                                    .msg_payment_flied), PaymentActivity.this);
                        }

                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), PaymentActivity.this);
                    }

                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
                }
            });
        }
    }

    public void logAddedPaymentInfoEvent(boolean success) {
        Bundle params = new Bundle();
        params.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, success ? 1 : 0);
        logger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_PAYMENT_INFO, params);
    }

    private void checkAvailableProvider() {
        Utils.showCustomProgressDialog(this, false);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.LATITUDE, currentBooking.getStoreLocation().get(0));
            jsonObject.put(Const.Params.LONGITUDE, currentBooking.getStoreLocation().get(1));
            jsonObject.put(Const.Params.CITY_ID, currentBooking.getBookCityId());
            jsonObject.put(Const.Params.IS_PAYMENT_MODE_CASH, gatewayItem.isPaymentModeCash());
            jsonObject.put(Const.Params.STORE_ID,currentBooking.getSelectedStoreId());
        } catch (JSONException e) {
            AppLog.handleException(PaymentActivity.class.getName(), e);
        }
        Log.i("checkAvailableProviderpayment",jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AvailableProviderResponse> responseCall = apiInterface.checkStoreProviderAvailable(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<AvailableProviderResponse>() {
            @Override
            public void onResponse(Call<AvailableProviderResponse> call, Response<AvailableProviderResponse>
                    response) {
                AppLog.Log("CHECK_PROVIDER_AVAILABLEpayment", ApiClient.JSONResponse(response.body()));

                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().getSuccess()) {
                    if (!response.body().getProviders().isEmpty()) {
                        // createOrder();
                        checkPaymentGatewayForPayOrder();
                    } else {
                        Utils.showToast(getResources().getString(R.string
                                .msg_no_provider_found), PaymentActivity.this);
                    }
                } else {
                    Utils.showToast(getResources().getString(R.string
                            .msg_no_provider_found), PaymentActivity.this);
                }

            }

            @Override
            public void onFailure(Call<AvailableProviderResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
            }
        });
    }


    /**
     * this method called a webservice for create delivery order
     */
    private void createOrder() {
        Utils.showCustomProgressDialog(this, false);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.CART_ID, currentBooking.getCartId());

            if (currentBooking.isFutureOrder()) {
                jsonObject.put(Const.Params.IS_SCHEDULE_ORDER, currentBooking.isFutureOrder());
                jsonObject.put(Const.Params.ORDER_START_AT, currentBooking
                        .getFutureOrderSelectedTimeUTCMillis());
            }

        } catch (JSONException e) {
            AppLog.handleException(PaymentActivity.class.getName(), e);
        }

        AppLog.Log("CREATE_ORDER", jsonObject.toString());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CreateOrederResponse> responseCall = apiInterface.createOrder(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<CreateOrederResponse>() {
            @Override
            public void onResponse(Call<CreateOrederResponse> call, Response<CreateOrederResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().getSuccess()) {

                    logPurchasedEvent(currentBooking.getCartProductItemWithAllSpecificationList().size(), "", "", Const.Facebook.FB_CURRENCY_CODE, currentBooking.getTotalInvoiceAmount());

                    Utils.showMessageToast(response.body().getMessage(), PaymentActivity
                            .this);
                    preferenceHelper.putAndroidId(Utils.generateRandomString());
                    currentBooking.clearCart();
                    currentBooking.setFutureOrder(false);
                    String orderId = response.body().getOrderId();
                    String storeName = response.body().getStoreName();

                    goToThankYouActivity(orderId, storeName);
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), PaymentActivity
                            .this);
                }

            }

            @Override
            public void onFailure(Call<CreateOrederResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
            }
        });
    }

    // After Purchased Order add facebook event
    public void logPurchasedEvent(int numItems, String contentType, String contentId, String currency, double price) {
        Bundle params = new Bundle();
        params.putInt(AppEventsConstants.EVENT_PARAM_NUM_ITEMS, numItems);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);

        if (currency != null && !currency.equals("")) {
            Log.d("currency", "--------- currency ----------" + currency);
            logger.logPurchase(BigDecimal.valueOf(price), Currency.getInstance(currency), params);
        }

    }

    private void setSelectedPaymentGateway(String paymentName) {
        for (PaymentGateway gatewayItem : paymentGateways) {
            if (TextUtils.equals(gatewayItem.getName(), paymentName)) {
                this.gatewayItem = gatewayItem;
                if (this.gatewayItem != null && this.gatewayItem.getName().equalsIgnoreCase(getString(R.string.text_stripe))) {
                    String mStripe_Pk = this.gatewayItem.getPaymentKeyId();
                    try {
                        PaymentConfiguration.init(PaymentActivity.this, mStripe_Pk);
                        mStripe = new Stripe(PaymentActivity.this, PaymentConfiguration.getInstance(PaymentActivity.this).getPublishableKey());
                    } catch (Exception e) {
                    }
                }
                return;
            }

        }
    }

    private void selectPaymentGatewayForAddWalletAmount() {


        if (gatewayItem != null) {
            if (etWalletAmount.getVisibility() == View.GONE) {
                updateUiForWallet(true);
            } else {

                if (!Utils.isDecimalAndGraterThenZero(etWalletAmount.getText().toString())) {
                    Utils.showToast(getResources().getString(R.string
                                    .msg_plz_enter_valid_amount)
                            , this);
                    return;
                }
                switch (gatewayItem.getId()) {
                    case Const.Payment.STRIPE:
                        StripeFragment stripeFragment = (StripeFragment) viewPagerAdapter.getItem
                                (tabLayout.getSelectedTabPosition());
                            stripeFragment.addWalletAmount();

                        Log.i("StripeFragment","ok");
                        break;
                    case Const.Payment.PAY_PAL:
                       /* PayPalFragment payPalFragment = (PayPalFragment) viewPagerAdapter.getItem
                                (tabLayout.getSelectedTabPosition());
                        payPalFragment.payWithPayPal(Const.PayPal.REQUEST_CODE_WALLET_PAYMENT,
                                Double.valueOf(etWalletAmount.getText().toString()));*/
                        break;
                    case Const.Payment.PAY_U_MONEY:
                        break;
                    case Const.Payment.CASH:
                        Utils.showToast(getResources().getString(R.string
                                .msg_plz_select_other_payment_gateway), this);
                        break;
                    default:
                        // do with default
                        break;
                }
            }
        }
    }


    private void setWalletAmount(double amount, String currency) {
        preferenceHelper.putWalletAmount((float) amount);
        preferenceHelper.putWalletCurrencyCode(currency);
        tvWalletAmount.setText(parseContent.decimalTwoDigitFormat.format(amount) + " " + currency);
    }

    private void checkPaymentGatewayForPayOrder() {
        if (gatewayItem != null) {
            switch (gatewayItem.getId()) {
                case Const.Payment.CASH:
                    payOrderPayment();
                    break;
                case Const.Payment.STRIPE:
                   /* StripeFragment stripeFragment = (StripeFragment) viewPagerAdapter.getItem
                            (tabLayout.getSelectedTabPosition());
                    if (stripeFragment.cardList.isEmpty()) {
                        Utils.showToast(getResources().getString(R.string
                                .msg_plz_add_card_first), this);
                    } else {
                        payOrderPayment();
                    }*/
                    /*stripeFragment=new StripeFragment();*/
                 //   stripeFragment.setSelectCreditCart();
                    Log.i("Const.Payment.STRIPE","clientSecret :"+clientSecret+"\n"+"stripeFragment.selectCard"+stripeFragment.selectCard.getCardNumber());
                    if (!TextUtils.isEmpty(clientSecret) && StripeFragment.selectCard != null && !TextUtils.isEmpty(StripeFragment.selectCard.getCardNumber())
                            && !TextUtils.isEmpty(StripeFragment.selectCard.getCardExpiryDate())) {

                        String cardMonth = "", cardYear = "";
                        if (StripeFragment.selectCard.getCardExpiryDate().contains(",")) {
                            String cardArray[] = StripeFragment.selectCard.getCardExpiryDate().split(",");
                            if (cardArray.length == 2) {
                                cardMonth = cardArray[0];
                                if (!TextUtils.isEmpty(cardArray[1])) {
                                    if (cardArray[1].length() == 4) {
                                        cardYear = cardArray[1].substring(2, 4);
                                    } else {
                                        cardYear = cardArray[1];
                                    }
                                }
                            }
                        }

                        if (!TextUtils.isEmpty(cardMonth) && !TextUtils.isEmpty(cardYear)) {
                            openAddCVVDialog(cardMonth, cardYear);
                        } else {
                            Toast.makeText(PaymentActivity.this, getString(R.string.msg_card_alert), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(PaymentActivity.this, "Stripe Error: clientSecret is empty!", Toast.LENGTH_SHORT).show();
                    }

                    break;
                case Const.Payment.PAY_PAL:
                   /* PayPalFragment payPalFragment = (PayPalFragment) viewPagerAdapter.getItem
                            (tabLayout.getSelectedTabPosition());
                    payPalFragment.payWithPayPal(Const.PayPal.REQUEST_CODE_ORDER_PAYMENT,
                            currentBooking.getTotalInvoiceAmount());*/
                    break;
                case Const.Payment.PAY_U_MONEY:
                    break;
                default:
                    // do with default
                    break;
            }
        }
    }

    private void goToWalletHistoryActivity() {
        Intent intent = new Intent(this, WalletHistoryActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToThankYouActivity(String orderId, String storeName) {
        Intent intent = new Intent(this, ThankYouActivity.class);
        intent.putExtra(Const.Params.ORDER_ID, orderId);
        intent.putExtra(Const.Params.STORE_ID, storeName);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void openAddCVVDialog(final String cardMonth, final String cardYear) {
        if (addCVVDialog != null && addCVVDialog.isShowing()) {
            return;
        }
        addCVVDialog = new Dialog(PaymentActivity.this);
        addCVVDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addCVVDialog.setContentView(R.layout.dialog_opencvv);
        etAddCVV = (CustomFontEditTextView) addCVVDialog.findViewById(R.id.etCVV);
        addCVVDialog.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etAddCVV.getText().toString().trim())) {
                    Utils.showToast(getString(R.string.msg_enter_cvv), PaymentActivity.this);
                    etAddCVV.requestFocus();
                } else {
                    addCVVDialog.dismiss();
                    hideCVVdialogkeyboard();
                    hideAllViewkeyboard();
                    Utils.hideSoftKeyboard(PaymentActivity.this);

                    com.stripe.android.model.Card mCard = new com.stripe.android.model.Card
                            .Builder(StripeFragment.selectCard.getCardNumber(),
                            Integer.parseInt(cardMonth),
                            Integer.parseInt(cardYear),
                            etAddCVV.getText().toString())
                            .build();

                    /*Log.e("tagCard", "selectCard.getCard_number() is " + selectCard.getCard_number());
                    Log.e("tagCard", "selectCard.cardMonth() is " + cardMonth);
                    Log.e("tagCard", "selectCard.cardYear() is " + cardYear);
                    Log.e("tagCard", "etAddCVV is " + etAddCVV.getText().toString());*/

                    if (mCard.validateCard()) {
                        Log.i("tagCard", "card is validated");
                        Utils.showCustomProgressDialog(PaymentActivity.this, false);

                        mStripe.confirmPayment(PaymentActivity.this,
                                ConfirmPaymentIntentParams
                                        .createWithPaymentMethodCreateParams(
                                                PaymentMethodCreateParams.create(mCard.toPaymentMethodParamsCard(), null),
                                                clientSecret, Const.STRIPE_RETURN_URL));

                        Log.i("tagCard", "payment done");
                        Utils.hideCustomProgressDialog();

                      //  payOrderPayment();
                    }

                }
            }
        });
        addCVVDialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCVVDialog.dismiss();
                hideCVVdialogkeyboard();
                hideAllViewkeyboard();
                Utils.hideSoftKeyboard(PaymentActivity.this);
            }
        });
        WindowManager.LayoutParams params = addCVVDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        addCVVDialog.getWindow().setAttributes(params);
        addCVVDialog.setCancelable(false);
        addCVVDialog.show();
    }

    private void hideCVVdialogkeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            if (etAddCVV != null) {
                imm.hideSoftInputFromWindow(etAddCVV.getWindowToken(), 0);
            }
        }
    }

    private void hideAllViewkeyboard() {
        View view = PaymentActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
        if (getWindow() != null) {
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
        }
        if (addCVVDialog != null && addCVVDialog.getWindow() != null) {
            addCVVDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (mStripe != null) {
            mStripe.onPaymentResult(requestCode, data, new ApiResultCallback<PaymentIntentResult>() {
                @Override
                public void onSuccess(@NonNull PaymentIntentResult result) {
                    if (!Utils.isInternetConnected(PaymentActivity.this)) {
                        Utils.hideCustomProgressDialog();
                        openInternetDialog(PaymentActivity.this);
                    } else {
                    Log.e("tagCard", "transaction id is " + result.getIntent().getId());
                     //   payOrderPayment(result.getIntent().getId());
                        payOrderPayment();
                    }
                }

                @Override
                public void onError(@NonNull Exception e) {
                    Utils.hideCustomProgressDialog();
//                    Log.e("tagCard", "error while doing payment " + e.getMessage());
                }
            });
        } else {
            Utils.hideCustomProgressDialog();
        }

    }
}
