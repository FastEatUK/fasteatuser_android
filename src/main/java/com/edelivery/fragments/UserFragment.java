package com.edelivery.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.FavouriteStoreActivity;
import com.edelivery.HelpActivity;
import com.edelivery.ProfileActivity;
import com.edelivery.R;
import com.edelivery.ReferralShareActivity;
import com.edelivery.SettingActivity;
import com.edelivery.adapter.UserMenuAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.utils.AppLog;

/**
 * Created by elluminati on 14-Feb-17.
 */

public class UserFragment extends BaseMainFragments {

    private RecyclerView rcvUserMenu;
    private CustomFontTextView tvAppVersion;
    private CustomDialogAlert logoutDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //homeActivity.ivToolbarRightIcon3.setVisibility(View.GONE);
        //homeActivity.tvTitleToolbar.setText("Account");




         //   homeActivity.setToolbarRightIcon3(R.drawable.ic_location_on_black_24dp, null);

      //  homeActivity.ivToolbarRightIcon3.setImageDrawable(null);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View userFragView = inflater.inflate(R.layout.fragment_user, container, false);
        rcvUserMenu = (RecyclerView) userFragView.findViewById(R.id.rcvUserMenu);
        tvAppVersion = (CustomFontTextView) userFragView.findViewById(R.id.tvAppVersion);

        return userFragView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRcvUserMenu();
        setAppVersion();

    }

    @Override
    public void onResume() {
        super.onResume();
        homeActivity.toolbar.setVisibility(View.VISIBLE);
        homeActivity.setTitleOnToolBar("Account");
    }

    @Override
    protected void onClickToolbarTitle() {
    }

    @Override
    public void onClick(View view) {

    }

    private void initRcvUserMenu() {
        UserMenuAdapter userMenuAdapter = new UserMenuAdapter(homeActivity);
        rcvUserMenu.setLayoutManager(new LinearLayoutManager(homeActivity));
        rcvUserMenu.setAdapter(userMenuAdapter);
        rcvUserMenu.addOnItemTouchListener(new RecyclerTouchListener(homeActivity, rcvUserMenu, new
                ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        AppLog.Log("POSITION", position + "");
                        switch (position) {
                            case 0:
                                goToProfileActivity();
                                break;
                            case 1:
                                homeActivity.goToPaymentActivity(false);
                                break;
                            case 2:
                                homeActivity.goToWalletActivity();
                                //homeActivity.goToDocumentActivity(false);
                                break;
                            case 3:
                                goToReferralShareActivity();
                                break;
                            case 4:
                                goToSettingActivity();
                                break;
                            case 5:
                                goToFavouriteActivity();
                                break;
                            case 6:
                                homeActivity.goToFavouriteAddressActivity(false,true);
                                break;
                            case 7:
                                goToHelpActivity();
                                break;
                            case 8:
                                openLogoutDialog();
                                break;
                            default:
                                // do with default
                                break;
                        }
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    private void openLogoutDialog() {
        if (logoutDialog != null && logoutDialog.isShowing()) {
            return;
        }
        logoutDialog = new CustomDialogAlert(homeActivity, homeActivity
                .getResources()
                .getString(R.string.text_log_out), homeActivity.getResources()
                .getString(R.string.msg_are_you_sure), homeActivity.getResources()
                .getString(R.string.text_cancel), homeActivity.getResources()
                .getString(R.string.text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                homeActivity.logOut();
                dismiss();
            }
        };
        logoutDialog.show();
    }


    private void setAppVersion() {

        tvAppVersion.setText(homeActivity.getResources().getString(R.string.text_app_version)
                + " " + homeActivity.getAppVersion());
    }

    private void goToProfileActivity() {
        Intent intent = new Intent(homeActivity, ProfileActivity.class);
        startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToSettingActivity() {
        Intent intent = new Intent(homeActivity, SettingActivity.class);
        startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToReferralShareActivity() {
        Intent intent = new Intent(homeActivity, ReferralShareActivity.class);
        startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToFavouriteActivity() {
        Intent intent = new Intent(homeActivity, FavouriteStoreActivity.class);
        startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToHelpActivity() {
        Intent intent = new Intent(homeActivity, HelpActivity.class);
        startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
