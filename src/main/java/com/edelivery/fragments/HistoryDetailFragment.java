package com.edelivery.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.edelivery.HistoryDetailActivity;
import com.edelivery.OrderCompleteActivity;
import com.edelivery.R;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.OrderPayment;
import com.edelivery.models.datamodels.Store;
import com.edelivery.models.datamodels.User;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

/**
 * Created by elluminati on 26-Apr-17.
 */

public class HistoryDetailFragment extends BaseHistoryFragments {


    private ImageView ivHistoryStoreImage, ivProviderImage;
    private CustomFontTextView tvHistoryOderTotal,
            tvAddressOne, tvAddressTwo, tvDeliveryDate;
    private CustomFontTextView tvEstTime, tvEstDistance;
    private CustomFontTextView tvStoreRatings, tvRatings;
    private CustomFontTextViewTitle tvHistoryOrderName, tvProviderName, tvOrderReceiverName;
    private CustomFontTextView tag2, tag1;
    private LinearLayout llDriverDetail, llDeliveryTime, llOrderReceiveBy;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (HistoryDetailActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View invoiceFrag = inflater.inflate(R.layout.fragment_detail_history, container, false);
        tvHistoryOderTotal =  invoiceFrag.findViewById(R.id.tvHistoryOderTotal);
        tvHistoryOrderName =  invoiceFrag.findViewById(R.id
                .tvHistoryOrderName);
        tvProviderName =  invoiceFrag.findViewById(R.id.tvProviderName);
        tvAddressOne =  invoiceFrag.findViewById(R.id.tvAddressOne);
        tvAddressTwo =  invoiceFrag.findViewById(R.id.tvAddressTwo);
        tvDeliveryDate =  invoiceFrag.findViewById(R.id.tvDetailDate);
        ivHistoryStoreImage =  invoiceFrag.findViewById(R.id.ivHistoryStoreImage);
        ivProviderImage =  invoiceFrag.findViewById(R.id.ivProviderImage);
        tvEstDistance =  invoiceFrag.findViewById(R.id.tvEstDistance);
        tvEstTime =  invoiceFrag.findViewById(R.id.tvEstTime);

        tvStoreRatings =  invoiceFrag.findViewById(R.id.tvStoreRatings);
        tvRatings =  invoiceFrag.findViewById(R.id.tvRatings);
        tag2 =  invoiceFrag.findViewById(R.id.tag2);
        tag1 =  invoiceFrag.findViewById(R.id.tag1);

        llDriverDetail =  invoiceFrag.findViewById(R.id.llDriverDetail);
        llDeliveryTime =  invoiceFrag.findViewById(R.id.llDeliveryTime);
        tvOrderReceiverName =  invoiceFrag.findViewById(R.id
                .tvOrderReceiverName);
        llOrderReceiveBy =  invoiceFrag.findViewById(R.id.llOrderReceiveBy);
        return invoiceFrag;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvDeliveryDate.setVisibility(View.VISIBLE);
        Utils.setTagBackgroundRtlView(activity, tag1);
        Utils.setTagBackgroundRtlView(activity, tag2);
        tvRatings.setOnClickListener(this);
        tvStoreRatings.setOnClickListener(this);


        String str_currency;
        if(activity.detailResponse.getCurrency()!=null && !activity.detailResponse.getCurrency().equals(""))
        {
            if(activity.detailResponse.getCurrency().toUpperCase().contains("NULL")){
                str_currency=activity.detailResponse.getCurrency().toUpperCase().replace("NULL", "");
            }else {
                str_currency=activity.detailResponse.getCurrency();
            }
        }
        else
        {
            str_currency="";
        }
        loadOrderData(str_currency);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Const.REQUEST_PROVIDER_RATING:
                    tvRatings.setVisibility(View.GONE);
                    activity.detailResponse.getOrderHistoryDetail().setUserRatedToProvider(true);
                    break;
                case Const.REQUEST_STORE_RATING:
                    tvStoreRatings.setVisibility(View.GONE);
                    activity.detailResponse.getOrderHistoryDetail().setUserRatedToStore(true);
                    break;
                default:
                    // do with default
                    break;
            }
        }

    }

    @Override
    public void onClick(View v) {
        //do something
        switch (v.getId()) {
            case R.id.tvStoreRatings:
                goToOrderCompleteActivity(activity.detailResponse.getOrderHistoryDetail()
                                .getOrderPaymentDetail(),
                        activity.detailResponse.getPayment(),
                        null,
                        activity.detailResponse.getStore(),
                        false,
                        false, Const.REQUEST_STORE_RATING);
                break;
            case R.id.tvRatings:
                goToOrderCompleteActivity(activity.detailResponse.getOrderHistoryDetail()
                                .getOrderPaymentDetail(), activity.detailResponse.getPayment(),
                        activity.detailResponse.getUser(),
                        null,
                        false,
                        false, Const.REQUEST_PROVIDER_RATING);
                break;
            default:
                // do with default
                break;
        }
    }

    /**
     * this method open Complete order activity witch is contain two fragment
     * Invoice Fragment and Feedback Fragment
     *
     * @param goToInvoiceFirst set true when you want to open invoice fragment when activity open
     * @param goToHome         set true when you want got back to home activity when press back
     *                         button
     */
    private void goToOrderCompleteActivity(OrderPayment orderPayment, String payment, User user, Store store, boolean goToInvoiceFirst,boolean goToHome, int requestCode) {
        Intent intent = new Intent(activity, OrderCompleteActivity.class);
        intent.putExtra(Const.Params.ORDER, orderPayment);
        intent.putExtra(Const.Params.PAYMENT_ID, payment);
        intent.putExtra(Const.PROVIDER_DETAIL, user);
        intent.putExtra(Const.STORE_DETAIL, store);
        intent.putExtra(Const.GO_TO_INVOICE, goToInvoiceFirst);
        intent.putExtra(Const.Params.ORDER_ID, activity.detailResponse.getOrderHistoryDetail()
                .getId());
        intent.putExtra(Const.GO_TO_HOME, false);
        startActivityForResult(intent, requestCode);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * this method load order data in view
     */

    private void loadOrderData(String currency) {
        tvHistoryOderTotal.setText(currency + activity.parseContent.decimalTwoDigitFormat.format(activity.detailResponse.getOrderHistoryDetail().getTotalOrderPrice()));
        tvAddressOne.setText(activity.detailResponse.getOrderHistoryDetail().getCartDetail()
                .getPickupAddresses().get(0).getAddress());
        tvAddressTwo.setText(activity.detailResponse.getOrderHistoryDetail().getCartDetail()
                .getDestinationAddresses().get(0).getAddress());


        try {
            Date date = activity.parseContent.webFormat.parse(activity.detailResponse
                    .getOrderHistoryDetail().getCreatedAt());
            if (date != null){
                String dateCreated = activity.parseContent.dateFormat.format(date);
                String currentDate = activity.parseContent.dateFormat.format(new Date());

                if (dateCreated.equals(currentDate)) {
                    tvDeliveryDate.setText(this
                            .getString(R.string.text_today));
                } else if (dateCreated
                        .equals(getYesterdayDateString())) {
                    tvDeliveryDate.setText(this
                            .getString(R.string.text_yesterday));
                } else {
                    tvDeliveryDate.setText(Utils.getDayOfMonthSuffix(Integer.valueOf(activity.parseContent.day.format(date))) +
                             " " + activity.parseContent.dateFormatMonth.format(date));
                }
            }

        } catch (ParseException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }
        tvHistoryOrderName.setText(activity.detailResponse.getStore().getName());
        tvProviderName.setText(activity.detailResponse.getUser().getFirstName() + " " + activity
                .detailResponse.getUser().getLastName());
        Glide.with(this).load(PreferenceHelper.getInstance(getContext()).getIMAGE_BASE_URL() + activity.detailResponse.getStore().getImageUrl())
                .dontAnimate().placeholder
                (ResourcesCompat.getDrawable(this
                        .getResources(), R.drawable.placeholder, null)).fallback(ResourcesCompat
                .getDrawable
                        (this.getResources(), R.drawable.placeholder, null)).into
                (ivHistoryStoreImage);
        Glide.with(this).load(PreferenceHelper.getInstance(getContext()).getIMAGE_BASE_URL() + activity.detailResponse.getUser().getImageUrl())
                .dontAnimate().placeholder
                (ResourcesCompat.getDrawable(this
                        .getResources(), R.drawable.placeholder, null)).fallback(ResourcesCompat
                .getDrawable
                        (this
                                .getResources(), R.drawable.placeholder, null)).into
                (ivProviderImage);



//       tvEstTime.setText(Utils.minuteToHoursMinutesSeconds(activity.detailResponse
//                .getOrderHistoryDetail
//                        ().getOrderPaymentDetail()
//                .getTotalItem()));


        /*String str_TotalTime= String.valueOf(activity.detailResponse
                .getOrderHistoryDetail
                        ().getOrderPaymentDetail().getTotal_sec());
        if(str_TotalTime!=null && !str_TotalTime.equals(""))
        {
            Log.d("formate","=========== Formate ========="+str_TotalTime);
            tvEstTime.setText(Utils.secondToHoursMinutesSeconds(Double.parseDouble(str_TotalTime)));
        }*/


        String str_TotalTime= String.valueOf(activity.detailResponse
                .getOrderHistoryDetail
                        ().getOrderPaymentDetail().getTotal_sec());
        Log.d("formate","=========== Total Sec  ========="+str_TotalTime);

            if(str_TotalTime!=null && !str_TotalTime.equals("") && !str_TotalTime.equals("null"))
            {
                Log.d("formate","=========== Formate ========="+str_TotalTime);
              //  tvEstTime.setText(Utils.secondToHoursMinutesSeconds(Double.parseDouble(str_TotalTime)));
                tvEstTime.setText(Utils.convertToHHMMSSFormate(Double.parseDouble(str_TotalTime)));

            }
            else {
                String str_TotalTime1= String.valueOf(activity.detailResponse
                        .getOrderHistoryDetail
                                ().getOrderPaymentDetail().getTotalTime()*60);
                tvEstTime.setText(Utils.convertToHHMMSSFormate(Double.parseDouble(str_TotalTime1)));
            }




        String unit;
        if(activity
                .detailResponse.getOrderHistoryDetail
                        ().getOrderPaymentDetail().isDistanceUnitMile())
        {
            unit="mile";
        }
        else {
            unit = "km";
        }
        tvEstDistance.setText(activity.parseContent.decimalTwoDigitFormat.format(activity
                .detailResponse.getOrderHistoryDetail
                        ().getOrderPaymentDetail()
                .getTotalDistance()) + unit);

        // is update detail UI when provider detail not available
        if (activity.detailResponse.getOrderHistoryDetail()
                .getOrderPaymentDetail().isUserPickUpOrder() || activity.detailResponse
                .getOrderHistoryDetail().getOrderStatus() == Const.OrderStatus
                .ORDER_CANCELED_BY_USER || activity.detailResponse
                .getOrderHistoryDetail().getOrderStatus() == Const.OrderStatus
                .STORE_ORDER_CANCELLED || activity.detailResponse
                .getOrderHistoryDetail().getOrderStatus() == Const.OrderStatus
                .STORE_ORDER_REJECTED) {
            llDriverDetail.setVisibility(View.GONE);
            llDeliveryTime.setVisibility(View.GONE);
            tag2.setVisibility(View.GONE);
        } else {
            llDriverDetail.setVisibility(View.VISIBLE);
            llDeliveryTime.setVisibility(View.VISIBLE);
            tag2.setVisibility(View.VISIBLE);
        }
        if (activity.detailResponse.getOrderHistoryDetail().isUserRatedToStore() || activity
                .detailResponse.getOrderHistoryDetail().getOrderStatus() != Const.OrderStatus
                .FINAL_ORDER_COMPLETED) {
            tvStoreRatings.setVisibility(View.GONE);
        } else {
            //tvStoreRatings.setVisibility(View.VISIBLE);
        }

        if (activity.detailResponse.getOrderHistoryDetail().isUserRatedToProvider() || activity
                .detailResponse.getOrderHistoryDetail().getOrderStatus() != Const.OrderStatus
                .FINAL_ORDER_COMPLETED || activity.detailResponse.getOrderHistoryDetail()
                .getOrderPaymentDetail().isUserPickUpOrder()) {
            tvRatings.setVisibility(View.GONE);
        } else {
            tvRatings.setVisibility(View.VISIBLE);
        }

        if (TextUtils.isEmpty(activity.detailResponse.getOrderHistoryDetail()
                .getCartDetail().getDestinationAddresses().get(0).getUserDetails().getName())) {
            llOrderReceiveBy.setVisibility(View.GONE);
        } else {
          //  llOrderReceiveBy.setVisibility(View.VISIBLE);
            tvOrderReceiverName.setText(activity.detailResponse.getOrderHistoryDetail()
                    .getCartDetail().getDestinationAddresses().get(0).getUserDetails().getName());
        }


    }

    private String getYesterdayDateString() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return activity.parseContent.dateFormat.format(cal.getTime());
    }

}
