package com.edelivery.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.edelivery.HistoryDetailActivity;
import com.edelivery.OrdersActivity;
import com.edelivery.R;
import com.edelivery.adapter.HistoryAdapter;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.OrderHistory;
import com.edelivery.models.responsemodels.OrderHistoryResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PinnedHeaderItemDecoration;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.TreeSet;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.R.id.ivToolbarRightIcon3;


/**
 * Created by elluminati on 01-Apr-17.
 */

public class HistoryFragment extends Fragment implements View.OnClickListener {


    public LinearLayout llDateFilter;
    private CustomFontTextView btnApplyFilter, btnResetFilter;
    private CustomFontTextView tvFromDate, tvToDate;
    private RecyclerView rcvOrderHistory;
    private HistoryAdapter ordersHistoryAdapter;
    private ArrayList<OrderHistory> orderHistoryShortList = new ArrayList<>(), orderHistoryOriznalList = new ArrayList<>();
    private TreeSet<Integer> separatorSet;
    private ArrayList<Date> dateList = new ArrayList<>();;
    private SwipeRefreshLayout srlOrdersHistory;
    private DatePickerDialog.OnDateSetListener fromDateSet, toDateSet;
    private Calendar calendar;
    private int day;
    private int month;
    private int year;
    private boolean isFromDateSet, isToDateSet;
    private long fromDateSetTime, toDateSetTime;
    private LinearLayout ivEmpty;
    private OrdersActivity ordersActivity;
    private PinnedHeaderItemDecoration pinnedHeaderItemDecoration;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ordersActivity = (OrdersActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View historyFragView = inflater.inflate(R.layout.fragment_history, container, false);
        llDateFilter = (LinearLayout) historyFragView.findViewById(R.id.llDateFilter);
        btnApplyFilter = (CustomFontTextView) historyFragView.findViewById(R.id.tvApply);
        btnResetFilter = (CustomFontTextView) historyFragView.findViewById(R.id.tvReset);
        tvFromDate = (CustomFontTextView) historyFragView.findViewById(R.id.tvFromDate);
        tvToDate = (CustomFontTextView) historyFragView.findViewById(R.id.tvToDate);
        rcvOrderHistory = (RecyclerView) historyFragView.findViewById(R.id.rcvOrderHistory);
        ivEmpty = (LinearLayout) historyFragView.findViewById(R.id.ivEmpty);
        srlOrdersHistory = (SwipeRefreshLayout) historyFragView.findViewById(R.id.srlOrdersHistory);

        return historyFragView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvFromDate.setOnClickListener(this);
        tvToDate.setOnClickListener(this);
        btnResetFilter.setOnClickListener(this);
        btnApplyFilter.setOnClickListener(this);
        dateList = new ArrayList<>();
        separatorSet = new TreeSet<>();
        orderHistoryShortList = new ArrayList<>();
        orderHistoryOriznalList = new ArrayList<OrderHistory>();

        getOrderHistory("", "");

        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);


        fromDateSet = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                calendar.clear();
                calendar.set(year, monthOfYear, dayOfMonth);
                fromDateSetTime = calendar.getTimeInMillis();
                isFromDateSet = true;
                tvFromDate.setText(ordersActivity.parseContent.dateFormat.format(calendar.getTime
                        ()));


            }
        };
        toDateSet = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.clear();
                calendar.set(year, monthOfYear, dayOfMonth);
                toDateSetTime = calendar.getTimeInMillis();
                isToDateSet = true;
                tvToDate.setText(ordersActivity.parseContent.dateFormat.format(calendar.getTime
                        ()));
            }
        };

        srlOrdersHistory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrderHistory("", "");
            }
        });

    }


    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case ivToolbarRightIcon3:
                updateUiFilter();
                break;
            case R.id.tvApply:
                getOrderHistory(ordersActivity.parseContent.dateFormat2.format(new Date
                        (fromDateSetTime)), ordersActivity.parseContent.dateFormat2.format(new
                        Date(toDateSetTime)));
                break;
            case R.id.tvReset:
                clearData();
                getOrderHistory("", "");
                break;
            case R.id.tvFromDate:
                openFromDatePicker();
                break;
            case R.id.tvToDate:
                openToDatePicker();
                break;
            default:
                // do with default
                break;
        }
    }


    private void updateUiFilter() {
        if (llDateFilter.getVisibility() == View.VISIBLE) {
            filterVisibleGone();
        } else {
            llDateFilter.setVisibility(View.VISIBLE);
            ordersActivity.ivToolbarRightIcon3.setImageDrawable(AppCompatResources.getDrawable
                    (ordersActivity,
                            R.drawable.ic_cancel));
        }
    }

    private void filterVisibleGone() {
        llDateFilter.setVisibility(View.GONE);
        ordersActivity.ivToolbarRightIcon3.setImageDrawable(AppCompatResources.getDrawable
                (ordersActivity,
                        R.drawable.filter_store));
    }

    /**
     * this method call webservice for get order history
     *
     * @param fromDate date in format (mm/dd/yyyy)
     * @param toDate   date in format (mm/dd/yyyy)
     */
    private void getOrderHistory(String fromDate, String toDate) {
        srlOrdersHistory.setRefreshing(true);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.START_DATE, fromDate);
            jsonObject.put(Const.Params.END_DATE, toDate);
            jsonObject.put(Const.Params.USER_ID, ordersActivity.preferenceHelper.getUserId());
            jsonObject.put(Const.Params.SERVER_TOKEN, ordersActivity.preferenceHelper
                    .getSessionToken());
        } catch (JSONException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderHistoryResponse> responseCall = apiInterface.getOrdersHistory(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<OrderHistoryResponse>() {
            @Override
            public void onResponse(Call<OrderHistoryResponse> call, Response<OrderHistoryResponse
                    > response) {
                filterVisibleGone();
                if (ordersActivity.parseContent.isSuccessful(response)) {
                    srlOrdersHistory.setRefreshing(false);
                    if (response != null && response.body() != null && ordersActivity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        orderHistoryOriznalList.clear();
                        orderHistoryOriznalList.addAll(response.body().getOrderList());
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(), ordersActivity);
                    }
                    updateUiHistoryList();
                    initRcvHistoryList();

                }

            }

            @Override
            public void onFailure(Call<OrderHistoryResponse> call, Throwable t) {
                AppLog.handleThrowable(HistoryFragment.class.getName(), t);
            }
        });
    }


    /**
     * This method is give arrayList which have unique date arrayList and is also add Date list
     * in treeSet
     */
    private void makeShortHistoryList() {
        orderHistoryShortList.clear();
        dateList.clear();
        separatorSet.clear();
        try {
            SimpleDateFormat sdf = ordersActivity.parseContent.dateFormat;
            final Calendar cal = Calendar.getInstance();

            Collections.sort(orderHistoryOriznalList, new Comparator<OrderHistory>() {
                @Override
                public int compare(OrderHistory lhs, OrderHistory rhs) {
                    return compareTwoDate(lhs.getCreatedAt(), rhs.getCreatedAt());
                }
            });

            HashSet<Date> listToSet = new HashSet<Date>();
            for (int i = 0; i < orderHistoryOriznalList.size(); i++) {
                AppLog.Log(HistoryFragment.class.getName(), orderHistoryOriznalList.get(i)
                        .getCreatedAt() + "");

                if (listToSet.add(sdf.parse(orderHistoryOriznalList.get(i).getCreatedAt()))) {
                    dateList.add(sdf.parse(orderHistoryOriznalList.get(i).getCreatedAt()));
                }

            }

            for (int i = 0; i < dateList.size(); i++) {

                cal.setTime(dateList.get(i));
                OrderHistory item = new OrderHistory();
                item.setCreatedAt(sdf.format(dateList.get(i)));
                orderHistoryShortList.add(item);

                separatorSet.add(orderHistoryShortList.size() - 1);
                for (int j = 0; j < orderHistoryOriznalList.size(); j++) {
                    Calendar messageTime = Calendar.getInstance();
                    messageTime.setTime(sdf.parse(orderHistoryOriznalList.get(j)
                            .getCreatedAt()));
                    if (cal.getTime().compareTo(messageTime.getTime()) == 0) {
                        orderHistoryShortList.add(orderHistoryOriznalList.get(j));
                    }
                }
            }
        } catch (ParseException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }
    }

    private int compareTwoDate(String firstStrDate, String secondStrDate) {
        try {
            SimpleDateFormat webFormat = ordersActivity.parseContent.webFormat;
            SimpleDateFormat dateFormat = ordersActivity.parseContent.dateTimeFormat;
            String date2 = dateFormat.format(webFormat.parse(secondStrDate));
            String date1 = dateFormat.format(webFormat.parse(firstStrDate));
            return date2.compareTo(date1);
        } catch (ParseException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }
        return 0;
    }

    private void initRcvHistoryList() {
        makeShortHistoryList();
        if (ordersHistoryAdapter != null) {
            pinnedHeaderItemDecoration.disableCache();
            ordersHistoryAdapter.notifyDataSetChanged();
        } else {
            rcvOrderHistory.setLayoutManager(new LinearLayoutManager(ordersActivity));
            ordersHistoryAdapter = new HistoryAdapter(separatorSet, orderHistoryShortList) {
                @Override
                public void onOrderClick(int position) {
                    goToHistoryOderDetailActivity(orderHistoryShortList.get(position).getId());

                }
            };
            rcvOrderHistory.setAdapter(ordersHistoryAdapter);
            pinnedHeaderItemDecoration = new PinnedHeaderItemDecoration();
            rcvOrderHistory.addItemDecoration(pinnedHeaderItemDecoration);
            /*rcvOrderHistory.addOnItemTouchListener(new RecyclerTouchListener(ordersActivity,
                    rcvOrderHistory, new ClickListener() {
                @Override
                public void onClick(View view, int position) {
                        goToHistoryOderDetailActivity(orderHistoryShortList.get(position).getId());
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));*/
        }
    }

    private void goToHistoryOderDetailActivity(String orderId) {
        Intent intent = new Intent(ordersActivity, HistoryDetailActivity.class);
        intent.putExtra(Const.Params.ORDER_ID, orderId);
        ordersActivity.startActivity(intent);
        ordersActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    private void openFromDatePicker() {
        DatePickerDialog fromPiker = new DatePickerDialog(ordersActivity,
                fromDateSet, year,
                month, day);
        fromPiker.setTitle(getResources().getString(R.string.text_select_from_date));
        fromPiker.getDatePicker();
        if (isToDateSet) {
            fromPiker.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        } else {
            fromPiker.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        }
        fromPiker.show();
    }

    private void openToDatePicker() {

        DatePickerDialog toPiker = new DatePickerDialog(ordersActivity, toDateSet,
                year,
                month,
                day);
        toPiker.setTitle(getResources().getString(R.string.text_select_to_date));
        if (isFromDateSet) {
            toPiker.getDatePicker().setMaxDate(System.currentTimeMillis());
            toPiker.getDatePicker().setMinDate(fromDateSetTime);
        } else {
            toPiker.getDatePicker().setMaxDate(System.currentTimeMillis());
        }
        toPiker.show();
    }

    private void clearData() {
        isFromDateSet = false;
        isToDateSet = false;
        tvToDate.setText(getResources().getString(R.string.text_to));
        tvFromDate.setText(getResources().getString(R.string.text_from));
    }

    private void updateUiHistoryList() {
        if (orderHistoryOriznalList.isEmpty()) {
            ivEmpty.setVisibility(View.VISIBLE);
            rcvOrderHistory.setVisibility(View.GONE);
        } else {
            ivEmpty.setVisibility(View.GONE);
            rcvOrderHistory.setVisibility(View.VISIBLE);
        }

    }


}
