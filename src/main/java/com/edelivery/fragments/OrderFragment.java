package com.edelivery.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.edelivery.BaseAppCompatActivity;
import com.edelivery.OrderCompleteActivity;
import com.edelivery.OrderTrackActivity;
import com.edelivery.OrdersActivity;
import com.edelivery.R;
import com.edelivery.adapter.OrdersAdapter;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.responsemodels.OrdersResponse;
import com.edelivery.models.responsemodels.ReassignDeliveryManResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by elluminati on 01-Apr-17.
 */

public class OrderFragment extends Fragment implements View.OnClickListener,
        BaseAppCompatActivity.OrderStatusListener {

    private SwipeRefreshLayout srlCurrentOrders;
    private RecyclerView rcvCurrentOrder;
    private OrdersAdapter ordersAdapter;
    private ArrayList<Order> orderArrayList = new ArrayList<>();
    ;
    private LinearLayout ivEmpty;
    private OrdersActivity ordersActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ordersActivity = (OrdersActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View currentOrderFragView = inflater.inflate(R.layout.fragment_current_order, container,
                false);
        srlCurrentOrders = (SwipeRefreshLayout) currentOrderFragView.findViewById(R.id
                .srlCurrentOrders);
        rcvCurrentOrder = (RecyclerView) currentOrderFragView.findViewById(R.id.rcvCurrentOrder);
        ivEmpty = (LinearLayout) currentOrderFragView.findViewById(R.id.ivEmpty);
        return currentOrderFragView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        orderArrayList = new ArrayList<>();
        srlCurrentOrders.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrders();
            }
        });

    }

    @Override
    public void onClick(View view) {
        // do somethings

    }

    @Override
    public void onResume() {
        super.onResume();
        ordersActivity.setOrderStatusListener(this);
        srlCurrentOrders.setRefreshing(true);
        getOrders();
    }


    private void initRcvOrders(final ArrayList<Order> orderArrayList) {
        if (ordersAdapter != null) {
            ordersAdapter.notifyDataSetChanged();
        } else {
            ordersAdapter = new OrdersAdapter(this, orderArrayList) {
                @Override
                public void assignDeliveryman(int position) {
                    assignDeliveryMan(position);
                }
            };
            rcvCurrentOrder.setLayoutManager(new LinearLayoutManager(ordersActivity));
            rcvCurrentOrder.setAdapter(ordersAdapter);
            rcvCurrentOrder.addOnItemTouchListener(new RecyclerTouchListener(ordersActivity,
                    rcvCurrentOrder, new ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    Order order = orderArrayList.get(position);
                    if (order.getId() != null) {
                        if (order.getOrderStatus() == Const.OrderStatus.FINAL_ORDER_COMPLETED) {
                            goToOrderCompleteActivity(order, true, true);
                        } else if (order.getDeliveryStatus() == Const.OrderStatus.DELIVERY_MAN_NOT_FOUND || order.getDeliveryStatus() ==
                                Const.OrderStatus.DELIVERY_MAN_CANCELLED || order.getDeliveryStatus() == Const.OrderStatus.DELIVERY_MAN_REJECTED) {

                        } else {
                            goToOrderStatusActivity(order);
                        }
                    } else {
                        Utils.showToast(getString(R.string.something_went_wrong), getActivity());
                    }


                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));
        }


    }

    /**
     * this method call webservice for create order pickup request to delivery man
     *
     * @param position
     * @param order
     */
    public void assignDeliveryMan(final int position) {
        Utils.showCustomProgressDialog(ordersActivity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.STORE_ID, orderArrayList.get(position).getStoreId());
            jsonObject.put(Const.Params.USER_ID, ordersActivity.preferenceHelper.getUserId());
            jsonObject.put(Const.Params.ORDER_ID, orderArrayList.get(position).getId());
            jsonObject.put(Const.Params.SERVER_TOKEN, ordersActivity.preferenceHelper.getSessionToken());

        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.ORDER_FRAGMENT, e);
        }

        AppLog.Log("ASSIGN_DELIVERYMAN", jsonObject.toString());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ReassignDeliveryManResponse> responseCall = apiInterface.ReassignDeliveryMan(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<ReassignDeliveryManResponse>() {
            @Override
            public void onResponse(Call<ReassignDeliveryManResponse> call, Response<ReassignDeliveryManResponse> response) {
                Utils.hideCustomProgressDialog();
                AppLog.Log("Assign_deliveryMan_response", ApiClient.JSONResponse(response.body()));
                if (response != null && response.body() != null && ordersActivity.parseContent.isSuccessful(response) && response.body().getSuccess()) {
                    getOrders();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), ordersActivity);
                }

            }

            @Override
            public void onFailure(Call<ReassignDeliveryManResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.CURRENT_ORDER_FRAGMENT, t);

            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        ordersActivity.setOrderStatusListener(null);
        srlCurrentOrders.setRefreshing(false);
    }

    /**
     * this method call a webservice for get ruining order of user
     */
    private void getOrders() {


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, ordersActivity.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.USER_ID, ordersActivity
                    .preferenceHelper
                    .getUserId());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.CURRENT_ORDER_FRAGMENT, e);
        }

        Log.i("getOrdersgetOrders", jsonObject.toString());
        AppLog.Log("getOrdersgetOrders", jsonObject.toString());


        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrdersResponse> responseCall = apiInterface.getOrders(ApiClient.makeJSONRequestBody
                (jsonObject));
        responseCall.enqueue(new Callback<OrdersResponse>() {
            @Override
            public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                if (ordersActivity.parseContent.isSuccessful(response)) {
                    orderArrayList.clear();
                    if (response != null && response.body() != null && ordersActivity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        orderArrayList.addAll(response.body().getOrderList());
                        Collections.sort(orderArrayList, new Comparator<Order>() {
                            @Override
                            public int compare(Order order1, Order order2) {
                                return compareTwoDate(order1.getCreatedAt(), order2.getCreatedAt());
                            }
                        });
                    }
                    updateUiOrderList();
                    initRcvOrders(orderArrayList);
                    srlCurrentOrders.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<OrdersResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.CURRENT_ORDER_FRAGMENT, t);
            }
        });

    }

    private void goToOrderStatusActivity(Order order) {
        Intent intent = new Intent(ordersActivity, OrderTrackActivity.class);
        intent.putExtra(Const.Params.ORDER, order);
        startActivity(intent);
        ordersActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * this method open Complete order activity witch is contain two fragment
     * Invoice Fragment and Feedback Fragment
     *
     * @param isGotoInvoice set true when you want to open invoice fragment when activity open
     * @param isGotoHome    set true when you want got back to home activity when press back button
     */
    private void goToOrderCompleteActivity(Order order, boolean isGotoInvoice, boolean isGotoHome) {
        Intent intent = new Intent(ordersActivity, OrderCompleteActivity.class);
        intent.putExtra(Const.Params.ORDER, order);
        intent.putExtra(Const.Params.ORDER_ID, order.getId());
        intent.putExtra(Const.GO_TO_INVOICE, true);
        intent.putExtra(Const.GO_TO_HOME, isGotoHome);
        startActivity(intent);
        ordersActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void updateUiOrderList() {
        if (orderArrayList.isEmpty()) {
            ivEmpty.setVisibility(View.VISIBLE);
            rcvCurrentOrder.setVisibility(View.GONE);
        } else {
            ivEmpty.setVisibility(View.GONE);
            rcvCurrentOrder.setVisibility(View.VISIBLE);
        }

    }

    private int compareTwoDate(String firstStrDate, String secondStrDate) {
        try {

            SimpleDateFormat dateFormat = ordersActivity.parseContent.webFormat;
            Date date2 = dateFormat.parse(secondStrDate);
            Date date1 = dateFormat.parse(firstStrDate);
            return date2.compareTo(date1);
        } catch (ParseException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }
        return 0;
    }


    @Override
    public void onOrderStatus() {
        getOrders();
    }
}
