package com.edelivery.fragments;

import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.edelivery.OrderCompleteActivity;
import com.edelivery.R;
import com.edelivery.adapter.InvoiceAdapter;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.Invoice;
import com.edelivery.models.datamodels.InvoicePayment;
import com.edelivery.models.datamodels.OrderPayment;
import com.edelivery.models.responsemodels.InvoiceResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.utils.Const.Params.ORDER_ID;

/**
 * Created by elluminati on 26-Apr-17.
 */

public class InvoiceFragment extends Fragment implements View.OnClickListener {

    private LinearLayout llInvoiceDistance, llInvoicePayment;
    private CustomFontTextViewTitle tvOderTotal;
    private CustomFontButton btnInvoiceSubmit;
    private RecyclerView rcvInvoice;
    private OrderCompleteActivity activity;
    private ArrayList<Invoice> invoiceArrayList = new ArrayList<>();;
    private OrderPayment orderPayment;
    private String payment, currency;
    private CustomFontTextView tvInvoiceMsg;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (OrderCompleteActivity) getActivity();
        activity.setTitleOnToolBar(activity.getResources().getString(R.string.text_invoice));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View invoiceFrag = inflater.inflate(R.layout.fragment_order_invoice, container, false);
        llInvoiceDistance = (LinearLayout) invoiceFrag.findViewById(R.id.llInvoiceDistance);
        llInvoicePayment = (LinearLayout) invoiceFrag.findViewById(R.id.llInvoicePayment);
        btnInvoiceSubmit = (CustomFontButton) invoiceFrag.findViewById(R.id.btnInvoiceSubmit);
        tvOderTotal = (CustomFontTextViewTitle) invoiceFrag.findViewById(R.id.tvOderTotal);
        rcvInvoice = (RecyclerView) invoiceFrag.findViewById(R.id.rcvInvoice);
        tvInvoiceMsg = (CustomFontTextView) invoiceFrag.findViewById(R.id.tvInvoiceMsg);
        return invoiceFrag;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnInvoiceSubmit.setOnClickListener(this);
        getOrderInvoice(activity.orderId);

    }

    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.btnInvoiceSubmit:
                setShowInvoice(activity.orderId);
                break;

            default:
                // do with default
                break;
        }
    }

    private void setInvoiceData() {

        rcvInvoice.setLayoutManager(new LinearLayoutManager(activity));
        rcvInvoice.setAdapter(new InvoiceAdapter(invoiceArrayList));
    }

    /**
     * this method will help to load a image in invoice screen
     *
     * @param title    set tile text
     * @param subTitle set sub title text
     * @param id       sett image id in image resource
     * @return invoicePayment object
     */
    private InvoicePayment loadInvoiceImage(String title, String subTitle, int id) {

        InvoicePayment invoicePayment = new InvoicePayment();
        invoicePayment.setTitle(title);
        invoicePayment.setValue(subTitle);
        invoicePayment.setImageId(id);
        return invoicePayment;

    }

    /**
     * this method used to set invoice distance and time view
     */
    private void setInvoiceDistanceAndTime() {

        String unit;
        if(orderPayment.isDistanceUnitMile())
        {
            unit="mile";
        }
        else {
            unit = "km";
        }

        ArrayList<InvoicePayment> invoicePayments = new ArrayList<>();

        String str_TotalTime= String.valueOf(orderPayment.getTotal_sec());
        if(str_TotalTime!=null && !str_TotalTime.equals(""))
        {

            Log.d("formate","=========== Total Sec  ========="+str_TotalTime);
            invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_time),
                    Utils.convertToHHMMSSFormate(Double.parseDouble(str_TotalTime))
                    , R
                            .drawable.ic_wall_clock));
        }

        invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_distance),
                appendString(orderPayment.getTotalDistance(), unit), R.drawable.ic_route));

        invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_payment),
                payment, orderPayment.isPaymentModeCash() ? R.drawable
                        .ic_cash : R.drawable.ic_credit_card_2));


        int size = invoicePayments.size();
        for (int i = 0; i < size; i++) {
            LinearLayout currentLayout = (LinearLayout) llInvoiceDistance.getChildAt(i);
            ImageView imageView = (ImageView) currentLayout.getChildAt(0);
            imageView.setImageDrawable
                    (AppCompatResources.getDrawable
                            (activity, invoicePayments.get(i).getImageId()));
            LinearLayout currentSubSubLayout = (LinearLayout) currentLayout.getChildAt(1);
            ((CustomFontTextView) currentSubSubLayout.getChildAt(0)).setText(invoicePayments.get
                    (i).getTitle());
            ((CustomFontTextView) currentSubSubLayout.getChildAt(1)).setText(invoicePayments.get
                    (i).getValue());
        }


    }

    /**
     * this method used to set invoice payment view
     */
    private void setInvoicePayments() {


        ArrayList<InvoicePayment> invoicePayments = new ArrayList<>();
        /*invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_wallet),
                appendString(currency, orderPayment.getWalletPayment()), R.drawable.ic_wallet));*/

        if (orderPayment.isPaymentModeCash())
        {


            invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_cash),
                    appendString(currency, orderPayment.getCashPayment()), R.drawable
                            .ic_cash));
        } else {
            invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_card),
                    appendString(currency, orderPayment.getCardPayment()), R.drawable
                            .ic_credit_card_2));
        }

        if (orderPayment.getPromoPayment() > 0) {
            llInvoicePayment.addView(LayoutInflater.from(activity).inflate(R.layout
                            .layout_invoice_item,
                    null));
            invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_promo),
                    appendString(currency, orderPayment.getPromoPayment()), R.drawable
                            .ic_promo_code));
        }
        int size = invoicePayments.size();
        for (int i = 0; i < size; i++) {
            LinearLayout currentLayout = (LinearLayout) llInvoicePayment.getChildAt(i);
            ImageView imageView = (ImageView) currentLayout.getChildAt(0);
            imageView.setImageDrawable
                    (AppCompatResources.getDrawable
                            (activity, invoicePayments.get(i).getImageId()));
            LinearLayout currentSubSubLayout = (LinearLayout) currentLayout.getChildAt(1);
            ((CustomFontTextView) currentSubSubLayout.getChildAt(0)).setText(invoicePayments.get
                    (i).getTitle());
            ((CustomFontTextView) currentSubSubLayout.getChildAt(1)).setText(invoicePayments.get
                    (i).getValue());
        }

        tvOderTotal.setText(currency + activity.parseContent.decimalTwoDigitFormat
                .format(orderPayment.getTotal
                        ()));
    }


    private String appendString(String string, Double value) {
        return string + activity.parseContent.decimalTwoDigitFormat.format(value);
    }

    private String appendString(Double value, String unit) {
        return activity.parseContent.decimalTwoDigitFormat.format(value) +
                unit;
    }

    private void getOrderInvoice(String orderId)
    {
        Utils.showCustomProgressDialog(activity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, activity.preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, activity.preferenceHelper.getUserId());
            jsonObject.put(ORDER_ID, orderId);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ORDER_TRACK_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<InvoiceResponse> invoiceResponseCall = apiInterface.getInvoice(ApiClient
                .makeJSONRequestBody(jsonObject));
        invoiceResponseCall.enqueue(new Callback<InvoiceResponse>() {
            @Override
            public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse> response) {

                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && response.body().isSuccess()) {
                    payment = response.body().getPayment();
                    orderPayment = response.body().getOrderPayment();



                    if(response.body().getCurrency()!=null && !response.body().getCurrency().equals(""))
                    {
                        if(response.body().getCurrency().toUpperCase().contains("NULL")){
                            currency=response.body().getCurrency().toUpperCase().replace("NULL", "");
                        }else {
                            currency=response.body().getCurrency();
                        }
                    }
                    else
                    {
                        currency="";
                    }

                    invoiceArrayList = activity.parseContent.parseInvoice(response.body()
                            .getOrderPayment(), currency, false);
                    setInvoiceData();
                    setInvoiceDistanceAndTime();
                    setInvoicePayments();
                    tvInvoiceMsg.setText(setInvoiceMessage());

                } else {

                    Utils.showErrorToast(response.body().getErrorCode(), activity);
                }

            }

            @Override
            public void onFailure(Call<InvoiceResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.INVOICE_FRAGMENT, t);
            }
        });
    }

    private void setShowInvoice(String orderId) {
        Utils.showCustomProgressDialog(activity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, activity.preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, activity.preferenceHelper.getUserId());
            jsonObject.put(ORDER_ID, orderId);
            jsonObject.put(Const.Params.IS_USER_SHOW_INVOICE, true);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ORDER_TRACK_ACTIVITY, e);
        }

        Log.i("setShowInvoice",jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.setShowInvoice(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                AppLog.Log("setShowInvoice", ApiClient.JSONResponse(response.body()));
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && activity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    if (activity.activeOrderResponse != null) {
                        if (TextUtils.isEmpty(activity.activeOrderResponse.getProviderId())) {
                            activity.goToHomeActivity();
                            return;
                        }
                    }
                    activity.goToFeedbackFragment();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), activity);
                }

            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.INVOICE_FRAGMENT, t);
            }
        });
    }

    private String setInvoiceMessage() {
        String message = "";
        if (orderPayment.isStorePayDeliveryFees() && orderPayment.isPaymentModeCash()) {
            message = getResources().getString(R.string
                    .msg_delivery_fee_free_and_cash_pay);
        } else if (orderPayment.isStorePayDeliveryFees() && !orderPayment.isPaymentModeCash()) {
            message = getResources().getString(R.string
                    .msg_delivery_fee_free_and_other_pay);
        } else if (orderPayment.isPaymentModeCash()) {
            message = getResources().getString(R.string
                    .msg_pay_cash);
        } else {
            message = getResources().getString(R.string
                    .msg_pay_other);
        }

        if (orderPayment.getPromoPayment() > 0) {
            String promo = orderPayment.isPromoForDeliveryService() ? getResources()
                    .getString(R.string
                            .msg_delivery_promo) : getResources()
                    .getString(R.string
                            .msg_order_promo);
            message = message + "\n" + promo;
        }

        return message;
    }
}
