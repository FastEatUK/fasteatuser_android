package com.edelivery.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.View;

import com.edelivery.HistoryDetailActivity;

/**
 * Created by elluminati on 23-12-2016.
 */
public abstract class BaseHistoryFragments extends Fragment implements View.OnClickListener{

    protected HistoryDetailActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (HistoryDetailActivity) getActivity();
    }
}
