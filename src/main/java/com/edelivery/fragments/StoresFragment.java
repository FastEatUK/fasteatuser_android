package com.edelivery.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.*;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import com.edelivery.*;
import com.edelivery.adapter.StoreAdapter;
import com.edelivery.animation.AlphaInAnimationAdapter;
import com.edelivery.animation.ResizeAnimation;
import com.edelivery.animation.ScaleInAnimationAdapter;
import com.edelivery.component.*;
import com.edelivery.models.datamodels.*;
import com.edelivery.models.responsemodels.AvailableProviderResponse;
import com.edelivery.models.responsemodels.SetFavouriteResponse;
import com.edelivery.models.responsemodels.StoreResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.tasks.OnSuccessListener;
import com.here.android.mpa.cluster.ClusterLayer;
import com.here.android.mpa.cluster.ClusterViewObject;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.MapOverlay;
import com.here.android.mpa.mapping.MapProxyObject;
import com.mapbox.android.core.location.*;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.picker.PlacePicker;
import com.mapbox.mapboxsdk.style.layers.Property;

import mapboxutils.CustomEventMapBoxView;
import mapboxutils.MapBoxMapReady;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static android.os.Looper.getMainLooper;
/*import static com.edelivery.BuildConfig.BASE_URL;*/

 public class StoresFragment extends BaseMainFragments implements PermissionsListener, OnMapReadyCallback, MapBoxMapReady
       /* MapboxMap.OnInfoWindowCloseListener, MapboxMap.OnInfoWindowClickListener,
        MapboxMap.OnInfoWindowLongClickListener*/ {
    private LocationHelper locationHelper;
    private MapOverlay mapOverlay;
    private RecyclerView rcvStore;
    private StoreAdapter storeAdapter;
    private ArrayList<Store> storeListOriginal = new ArrayList<>();
    private ArrayList<Store> storeListFiltered = new ArrayList<>();
    private ArrayList<Ads> ads = new ArrayList<>();
    private SwipeRefreshLayout srlSelectedStore;
    private LinearLayout llStoreFilter;
    private CustomFontTextView btnPriceOne, btnPriceTwo, btnPriceThree, btnPriceFour,
            selectedPrice, selectedTime, selectedDistance, selectCategory, btnTimeThree, btnTimeOne,
            btnTimeTwo,
            btnDistanceOne,
            btnDistanceTwo, btnDistanceThree, btnStore, btnItem, btnTag;
    private CustomFontButton btnResetFilter, btnApplyFilter;
    private ImageView ivClearText;
    private CustomFontEditTextView etStoreSearch;
    private LinearLayout ivEmpty, llCategoryFilter;
    private ArrayList<Integer> storePrices = new ArrayList<>();
    private int storeTime = 0;
    private double storeDistance = 0;
    private Deliveries deliveries;
    public Bitmap storeBitmap;
    private ArrayList<String> selectedTagList = new ArrayList<>();
    private TagView tagView;
    private ImageView ivCategoryFilter, ivStoreFilter;
    /* private LinearLayout imageView;*/
    private FrameLayout flStore;
    private static final int REQUEST_CHECK_SETTINGS = 656;
    public TextView acDeliveryAddress;
    public MapboxMap mapboxMap;
    public LocationEngine locationEngine;
    public PermissionsManager permissionsManager;
    public long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    public long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
    LocationEngineRequest mLocationEngineRequest;
    public MainActivityLocationCallback callback = new MainActivityLocationCallback(getActivity());
    public MapView mapbox;

    RelativeLayout relativeLayout;
    public boolean isCurrentLocationGet = false;
    //for here map
    public String store_id;
    private AndroidXMapFragment mapFragment = null;
    MapGesture.OnGestureListener listener;
    private Map map = null;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) getChildFragmentManager().findFragmentById(R.id.mapfragment);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        locationHelper = new LocationHelper(homeActivity);

        if (homeActivity.currentBooking != null && homeActivity.currentBooking.getCurrentAddress() != null) {
            if (homeActivity.currentBooking.getCurrentAddress() != null && !TextUtils.isEmpty(homeActivity.currentBooking.getCurrentAddress())) {
                // homeActivity.setTitleOnToolBar(getResources().getString(R.string.text_ASAP) + " " + homeActivity.currentBooking.getCurrentAddress());
            } else {
                // homeActivity.setTitleOnToolBar(getResources().getString(R.string.text_ASAP));
            }
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Mapbox.getInstance(getActivity(), getResources().getString(R.string.MAP_BOX_ACCESS_TOKEN));

        View view = inflater.inflate(R.layout.fragment_stores, container, false);
        Log.d("activity", "======= Store Fragement ============");

      /* mapbox = view.findViewById(R.id.mapBoxView);
        mapbox.onCreate(savedInstanceState);
        new CustomEventMapBoxView(getActivity(), mapbox, this);
*/


        relativeLayout = (RelativeLayout) view.findViewById(R.id.rl_map_view);
        rcvStore = (RecyclerView) view.findViewById(R.id.rcvStore);
        srlSelectedStore = (SwipeRefreshLayout) view.findViewById(R.id.srlSelectedStore);
        llStoreFilter = (LinearLayout) view.findViewById(R.id.llSoreFilter);
        btnPriceOne = (CustomFontTextView) view.findViewById(R.id.btnPriceOne);
        btnPriceTwo = (CustomFontTextView) view.findViewById(R.id.btnPriceTwo);
        btnPriceThree = (CustomFontTextView) view.findViewById(R.id.btnPriceThree);
        btnPriceFour = (CustomFontTextView) view.findViewById(R.id.btnPriceFour);
        btnApplyFilter = (CustomFontButton) view.findViewById(R.id.btnApplyFilter);
        btnResetFilter = (CustomFontButton) view.findViewById(R.id.btnResetFilter);
        btnTimeThree = (CustomFontTextView) view.findViewById(R.id.btnTimeThree);
        btnTimeOne = (CustomFontTextView) view.findViewById(R.id.btnTimeOne);
        btnTimeTwo = (CustomFontTextView) view.findViewById(R.id.btnTimeTwo);
        ivEmpty = (LinearLayout) view.findViewById(R.id.ivEmpty);
        etStoreSearch = (CustomFontEditTextView) view.findViewById(R.id.etStoreSearch);
        ivClearText = (ImageView) view.findViewById(R.id
                .ivClearDeliveryAddressTextMap);
        btnDistanceOne = (CustomFontTextView) view.findViewById(R.id.btnDistanceOne);
        btnDistanceTwo = (CustomFontTextView) view.findViewById(R.id.btnDistanceTwo);
        btnDistanceThree = (CustomFontTextView) view.findViewById(R.id.btnDistanceThree);

        tagView = (TagView) view.findViewById(R.id.tag_group);
        ivCategoryFilter = (ImageView) view.findViewById(R.id.ivCategoryFilter);
        llCategoryFilter = (LinearLayout) view.findViewById(R.id.llCategoryFilter);

        btnStore = (CustomFontTextView) view.findViewById(R.id.btnStore);
        btnItem = (CustomFontTextView) view.findViewById(R.id.btnItem);
        btnTag = (CustomFontTextView) view.findViewById(R.id.btnTag);
        ivStoreFilter = view.findViewById(R.id.ivStoreFilter);
        /*imageView = (LinearLayout) view.findViewById(R.id.ivEmpty);*/
        flStore = (FrameLayout) view.findViewById(R.id.flStore);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            llStoreFilter.setVisibility(View.GONE);
            llCategoryFilter.setVisibility(View.GONE);
            loadExtraData();
            downloadStorePin();
            setViewListener();
            storeListOriginal = new ArrayList<>();
            storeListFiltered = new ArrayList<>();
            storePrices = new ArrayList<>();
            ads = new ArrayList<>();
            selectedTagList = new ArrayList<>();
            //   homeActivity.setToolbarRightIcon3(R.drawable.ic_location_on_black_24dp, this);
            srlSelectedStore.setRefreshing(true);

            loadStoreList();
            initRcvStore();
            setFilterPriceData();
            initTagView();
        } catch (Exception e) {
            Log.e("onActivityCreated() " , e.getMessage());
        }
    }


    @Override
    protected void onClickToolbarTitle() {
        goToDeliveryLocationActivity();
    }

    private void goToDeliveryLocationActivity() {
        if (homeActivity != null) {
            Intent intent = new Intent(homeActivity, DeliveryLocationActivity.class);
            startActivityForResult(intent, Const.DELIVERY_LIST_CODE);
            homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    protected void setViewListener() {
        ivCategoryFilter.setOnClickListener(this);
        srlSelectedStore.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadStoreList();
            }
        });
        btnPriceOne.setOnClickListener(this);
        btnPriceTwo.setOnClickListener(this);
        btnPriceThree.setOnClickListener(this);
        btnPriceFour.setOnClickListener(this);
        btnTimeOne.setOnClickListener(this);
        btnTimeTwo.setOnClickListener(this);
        btnTimeThree.setOnClickListener(this);
        btnApplyFilter.setOnClickListener(this);
        btnResetFilter.setOnClickListener(this);
        ivClearText.setOnClickListener(this);
        ivClearText.setVisibility(View.GONE);
        btnStore.setOnClickListener(this);
        btnItem.setOnClickListener(this);
        btnTag.setOnClickListener(this);
        etStoreSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                storeFilter(s);
                if (s.length() > 0) {
                    ivClearText.setVisibility(View.VISIBLE);
                } else {
                    ivClearText.setVisibility(View.GONE);
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btnDistanceOne.setOnClickListener(this);
        btnDistanceTwo.setOnClickListener(this);
        btnDistanceThree.setOnClickListener(this);
        ivStoreFilter.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        //do something
        switch (view.getId()) {
            case R.id.ivToolbarRightIcon3:

                // checkPermissions();
                updateUiMap();
                break;
            case R.id.ivToolbarRightIcon2:
            case R.id.ivStoreFilter:
                updateUiStoreFilter();
                break;
            case R.id.btnResetFilter:
                loadStoreList();
                updateUiStoreFilter();
                break;
            case R.id.btnApplyFilter:
                applyFiltered(!storePrices.isEmpty(), storeTime > 0, storeDistance > 0,
                        !selectedTagList.isEmpty());
                updateUiStoreFilter();
                etStoreSearch.getText().clear();
                break;
            case R.id.ivClearDeliveryAddressTextMap:
                etStoreSearch.getText().clear();
                storeFilter(etStoreSearch.getText().toString());
                break;
            case R.id.ivCategoryFilter:
                slidStoreSearchFilterView();
                break;
            default:
                // do with default
                break;
        }
        checkSelectedPrice(view);
        checkSelectedTime(view);
        checkSelectedDistance(view);
        checkSelectedCategory(view);
    }

    private void initRcvStore() {

        Log.i("storeListOriginal", storeListOriginal.toString());
        try {
            if (storeAdapter != null && storeListOriginal != null) {
                storeAdapter.setStoreArrayList(storeListOriginal);
                storeAdapter.notifyDataSetChanged();
                //  Log.i("storeListOriginal",storeListOriginal.toString());
            } else {
                rcvStore.setLayoutManager(new LinearLayoutManager(homeActivity));
                if (storeListOriginal != null) {
                    storeListOriginal = new ArrayList<>();


                }
                if (ads != null) {
                    ads = new ArrayList<>();
                }
                storeAdapter = new StoreAdapter(homeActivity, storeListOriginal, ads) {
                    @Override
                    public void onSelected(View view, int position) {
                        try {
                            if (homeActivity.currentBooking != null && storeAdapter.getStoreArrayList() != null) {
                                Store store = storeAdapter.getStoreArrayList().get(position);
                                homeActivity.currentBooking.setStoreClosed(store.isStoreClosed());
                                checkAvailableProvider(store, view);
                            }
                        } catch (Exception e) {
                            Log.e("storeAdapter onSelected() " , e.getMessage());
                        }
                    }

                    @Override
                    public void setFavourites(int position, boolean isFavourite) {
                        try {
                            if (storeAdapter != null && storeAdapter.getStoreArrayList() != null) {
                                if (isFavourite) {
                                    removeAsFavoriteStore(storeAdapter.getStoreArrayList().get(position));
                                } else {
                                    setFavoriteStore(storeAdapter.getStoreArrayList().get(position));
                                }
                            }
                        } catch (Exception e) {
                            Log.e("initRcvStore() setFavourites onSelected() " , e.getMessage());
                        }
                    }
                };
                AlphaInAnimationAdapter animationAdapter = new AlphaInAnimationAdapter(storeAdapter);
                rcvStore.setAdapter(new ScaleInAnimationAdapter(animationAdapter));
                rcvStore.setDrawingCacheEnabled(true);
                rcvStore.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

            }
        } catch (Exception e) {
            Log.e("initRcvStore() " , e.getMessage());
        }
    }

    private void getStoreList(String cityId, String deliveryStoreId) {


        //Utils.showCustomProgressDialog(homeActivity, false);
        try {
            if (homeActivity.preferenceHelper != null) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(Const.Params.CITY_ID, cityId);
                    jsonObject.put(Const.Params.STORE_DELIVERY_ID, deliveryStoreId);
                    if (homeActivity.preferenceHelper.getSessionToken() != null) {
                        jsonObject.put(Const.Params.SERVER_TOKEN, homeActivity.preferenceHelper
                                .getSessionToken());
                    }
                    if (homeActivity.preferenceHelper.getUserId() != null) {
                        jsonObject.put(Const.Params.USER_ID, homeActivity.preferenceHelper
                                .getUserId());
                    }
                    if (homeActivity.preferenceHelper.getAndroidId() != null) {
                        jsonObject.put(Const.Params.CART_UNIQUE_TOKEN, homeActivity.preferenceHelper
                                .getAndroidId());
                    }
                    if (homeActivity.currentBooking.getCurrentLatLng() != null) {
                        jsonObject.put(Const.Params.LATITUDE, homeActivity.currentBooking.getCurrentLatLng().latitude);
                        jsonObject.put(Const.Params.LONGITUDE, homeActivity.currentBooking.getCurrentLatLng()
                                .longitude);
                    }
                } catch (JSONException e) {
                    Log.e("getStoreList() JSONException " , e.getMessage());
                    AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, e);
                }
                Log.i("getStoreList", jsonObject.toString());
                ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<StoreResponse> responseCall = apiInterface.getSelectedStoreList(ApiClient
                        .makeJSONRequestBody(jsonObject));
                responseCall.enqueue(new Callback<StoreResponse>() {
                    @Override
                    public void onResponse(Call<StoreResponse> call, Response<StoreResponse> response) {
                        try {
                            if (getActivity() != null && !getActivity().isFinishing()) {
                                AppLog.Log("STORE_RESPONSE", ApiClient.JSONResponse(response.body()));
                                storeListOriginal.clear();
                                if (response.body() != null && homeActivity.parseContent != null && homeActivity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                                    if (response.body().getStores() != null) {

                                        srlSelectedStore.setVisibility(View.VISIBLE);
                                        Utils.hideCustomProgressDialogForHome();
                                        ivEmpty.setVisibility(View.GONE);


                                        for (Store store : response.body().getStores()) {
                                            StoreClosedResult storeClosedResult = Utils.checkStoreOpenAndClosed
                                                    (homeActivity, store.getStoreTime(), response.body
                                                            ().getServerTime(), CurrentBooking.getInstance()
                                                            .getTimeZone(), false, 0);
                                            store.setStoreClosed(storeClosedResult.isStoreClosed());
                                            store.setReOpenTime(storeClosedResult.getReOpenAt());

                                            if (homeActivity.currentBooking.getCurrentLatLng() != null) {
                                                if (homeActivity.currentBooking != null && homeActivity.currentBooking.getCurrentAddress() != null) {
                                                    if (homeActivity.currentBooking.getCurrentAddress() != null && !TextUtils.isEmpty(homeActivity.currentBooking.getCurrentAddress())) {
                                                        //   homeActivity.setTitleOnToolBar(getResources().getString(R.string.text_ASAP) + " " + homeActivity.currentBooking.getCurrentAddress());
                                                    } else {
                                                        //  homeActivity.setTitleOnToolBar(getResources().getString(R.string.text_ASAP));
                                                    }
                                                }

                                                LatLng lt = new LatLng(homeActivity.currentBooking
                                                        .getCurrentLatLng().latitude, homeActivity.currentBooking
                                                        .getCurrentLatLng().longitude);
                                                store.setDistance(distanceTo(lt,
                                                        new LatLng(store
                                                                .getLocation().get(0), store
                                                                .getLocation().get(1)), true));
                                            }


                                            String str_currency;
                                            if (CurrentBooking.getInstance().getCurrency() != null && !CurrentBooking.getInstance().getCurrency().equals("")) {
                                                if (CurrentBooking.getInstance().getCurrency().toUpperCase().contains("NULL")) {
                                                    str_currency = CurrentBooking.getInstance().getCurrency().toUpperCase().replace("NULL", "");
                                                } else {
                                                    str_currency = CurrentBooking.getInstance().getCurrency();
                                                }
                                            } else {
                                                str_currency = "";
                                            }
                                            store.setCurrency(str_currency);
                                            store.setPriceRattingAndTag(Utils.getStringPriceAndTag(store
                                                    .getFamousProductsTags
                                                            (), store.getPriceRating(), store
                                                    .getCurrency()));
                                            if (storeListOriginal != null) {
                                                if (homeActivity.currentBooking.isFutureOrder()) {
                                                    if (store.isTakingScheduleOrder()) {
                                                        storeListOriginal.add(store);
                                                    }
                                                } else {
                                                    storeListOriginal.add(store);
                                                }
                                            }
                                            Log.i("initRcvStore();", storeListOriginal.toString());
                                        }
                                        if (ads != null) {
                                            ads.clear();
                                            if (response.body().getAds() != null) {
                                                ads.addAll(response.body().getAds());
                                            }
                                        }

                                        initRcvStore();
                                        Log.i("initRcvStore();", "done");
                                        resetFilter();
                                        setStoreMarkerOnMap();
                                        if (storeAdapter != null) {
                                            storeAdapter.setFilterBy(homeActivity.getResources().getString(R.string.text_store));
                                        }
                                        etStoreSearch.setHint(homeActivity.getResources().getString(R.string
                                                .text_search_by) + " " +
                                                homeActivity.getResources().getString(R.string.text_store));
                                        resetSelectedCategory();
                                        setSelectedCategory(btnStore);
                                        updateUiStoreList();
                                    }
                                } else {

                                    if (response.body() != null) {
                                        Utils.showErrorToast(response.body().getErrorCode(), homeActivity);
                                    }
                                }

                                if (srlSelectedStore != null) {
                                    srlSelectedStore.setRefreshing(false);
                                }
                                if (etStoreSearch != null && etStoreSearch.getText() != null) {
                                    etStoreSearch.getText().clear();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Utils.hideCustomProgressDialog();
                    }

                    @Override
                    public void onFailure(Call<StoreResponse> call, Throwable t) {
                        Utils.hideCustomProgressDialog();
                        AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
                    }
                });
            }
        } catch (Exception e) {
            Utils.hideCustomProgressDialog();
            Log.e("getStoreList() ", e.getMessage());
        }

        Utils.hideCustomProgressDialogForHome();
        Utils.hideCustomProgressDialogForHome();
    }

    private void checkAvailableProvider(final Store store, final View view) {
        Utils.showCustomProgressDialog(homeActivity, false);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.LATITUDE, store.getLocation().get(0));
            jsonObject.put(Const.Params.LONGITUDE, store.getLocation().get(1));
            jsonObject.put(Const.Params.CITY_ID, homeActivity.currentBooking.getBookCityId());
            jsonObject.put(Const.Params.STORE_ID, store.getId());

        } catch (JSONException e) {
            Log.e("checkAvailableProvider " , e.getMessage());
            AppLog.handleException(PaymentActivity.class.getName(), e);
        }

        AppLog.Log("CHECK_PROVIDER_AVAILABLE", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AvailableProviderResponse> responseCall = apiInterface.checkStoreProviderAvailable(ApiClient
                .makeJSONRequestBody(jsonObject));

        responseCall.enqueue(new Callback<AvailableProviderResponse>() {
            @Override
            public void onResponse(Call<AvailableProviderResponse> call, Response<AvailableProviderResponse>
                    response) {

                AppLog.Log("CHECK_PROVIDER_AVAILABLE", ApiClient.JSONResponse(response.body()));

                /* Utils.hideCustomProgressDialog();*/
                if (response != null && response.body() != null && homeActivity.parseContent.isSuccessful(response) && response.body().getSuccess())
                {
                    if (!response.body().getProviders().isEmpty())
                    {
                        if (TextUtils.equals(storeAdapter.getFilterBy(), getResources().getString(R
                                .string.text_item)) && !TextUtils.isEmpty(etStoreSearch.getText()
                                .toString())) {
                            goToStoreProductActivity(store, view, etStoreSearch.getText().toString()
                                    , true);

                        } else {
                            goToStoreProductActivity(store, view, null, true);
                        }
                    } else {
                        openProviderNotFoundDialog(store, view);
                    }
                } else {
                    openProviderNotFoundDialog(store, view);
                }
                Utils.hideCustomProgressDialog();
            }

            @Override
            public void onFailure(Call<AvailableProviderResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.STORE_FRAGMENT, t);
            }
        });
    }

    private void openProviderNotFoundDialog(final Store store, final View view) {
        String msg = getResources().getString(R.string.msg_provider_not_available);
        CustomDialogAlert customDialogAlert = new CustomDialogAlert(homeActivity, getResources().getString(R.string
                .text_attention), msg, getResources().getString(R.string.text_ok), getResources().getString(R.string.text_cancel)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
                if (TextUtils.equals(storeAdapter.getFilterBy(), getResources().getString(R
                        .string.text_item)) && !TextUtils.isEmpty(etStoreSearch.getText()
                        .toString())) {
                    goToStoreProductActivity(store, view, etStoreSearch.getText().toString()
                            , true);
                } else {
                    goToStoreProductActivity(store, view, null, true);
                }
            }

            @Override
            public void onClickRightButton() {
                dismiss();
            }
        };
        customDialogAlert.show();
    }

    private void goToStoreProductActivity(Store store, View view, String filter, boolean
            isAnimated) {
        Intent intent = new Intent(homeActivity, StoreProductActivity.class);
        intent.putExtra(Const.SELECTED_STORE, store);
        intent.putExtra(Const.FILTER, filter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && isAnimated) {

            Pair<View, String> p1 = Pair.create(view.findViewById(R.id.llStoreCard),
                    getResources().getString(R.string
                            .transition_string_store_card));
            Pair<View, String> p2 = Pair.create(view.findViewById(R.id.ivStoreImage),
                    getResources().getString(R.string
                            .transition_string_store_image));

            ActivityOptionsCompat options =
                    ActivityOptionsCompat.makeSceneTransitionAnimation(homeActivity, p1,
                            p2);
            ActivityCompat.startActivity(homeActivity, intent, options.toBundle());
        } else {
            startActivity(intent);
            homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

    }

    public void goToStoreProductActivity(String storeId) {
        Log.i("goToStoreProductActivity", storeId);
        Intent intent = new Intent(homeActivity, StoreProductActivity.class);
        intent.putExtra(Const.STORE_DETAIL, storeId);
        startActivity(intent);
        homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void gotToStoreProductActivity(Store mStore) {

        if (mStore != null) {
            Intent intent = new Intent(homeActivity, StoreProductActivity.class);
            intent.putExtra(Const.SELECTED_STORE, mStore);
            startActivity(intent);
            homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    private void loadStoreList() {
        try {
            srlSelectedStore.setRefreshing(true);
            if (homeActivity.currentBooking.isDeliveryAvailable()) {
                getStoreList(CurrentBooking.getInstance().getBookCityId(), deliveries.getId());
                ivEmpty.setVisibility(View.GONE);
                Log.i("loadStoreList()", "true");
                flStore.setVisibility(View.VISIBLE);
            } else {
                flStore.setVisibility(View.GONE);
                if (storeListOriginal.size() == 0) {
                    Utils.hideCustomProgressDialogForHome();
                    ivEmpty.setVisibility(View.VISIBLE);
                }

            }
        } catch (Exception e) {
            Log.e("loadStoreList() " , e.getMessage());
        }
    }

    private void loadExtraData() {
        if (getArguments() != null) {
            deliveries = getArguments().getParcelable(Const
                    .DELIVERY_STORE);
        }

    }

    private void setSelectedPrice(CustomFontTextView view) {
        selectedPrice = view;
        view.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                .selector_round_shape_black_solid, null));
        view.setTextColor(ResourcesCompat.getColor(getResources(), R.color.color_white, null));
    }

    private void setSelectedTime(CustomFontTextView view) {
        selectedTime = view;
        view.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                .selector_round_shape_black_solid, null));
        view.setTextColor(ResourcesCompat.getColor(getResources(), R.color.color_white, null));
    }

    private void setSelectedDistance(CustomFontTextView view) {
        selectedDistance = view;
        view.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                .selector_round_shape_black_solid, null));
        view.setTextColor(ResourcesCompat.getColor(getResources(), R.color.color_white, null));
    }

    private void setSelectedCategory(CustomFontTextView view) {
        selectCategory = view;
        view.setBackground(ResourcesCompat.getDrawable(homeActivity.getResources(), R.drawable
                .selector_round_shape_black_solid, null));
        view.setTextColor(ResourcesCompat.getColor(homeActivity.getResources(), R.color.color_white, null));
    }

    private void checkSelectedPrice(View view) {
        switch (view.getId()) {
            case R.id.btnPriceOne:
                if (storePrices.contains(Const.Store.STORE_PRICE_ONE)) {
                    storePrices.remove((Object) Const.Store.STORE_PRICE_ONE);
                    resetSelectedPrice((CustomFontTextView) view);
                } else {
                    storePrices.add(Const.Store.STORE_PRICE_ONE);
                    setSelectedPrice((CustomFontTextView) view);

                }
                break;
            case R.id.btnPriceTwo:
                if (storePrices.contains(Const.Store.STORE_PRICE_TWO)) {

                    storePrices.remove((Object) Const.Store.STORE_PRICE_TWO);
                    resetSelectedPrice((CustomFontTextView) view);
                } else {
                    storePrices.add(Const.Store.STORE_PRICE_TWO);
                    setSelectedPrice((CustomFontTextView) view);
                }
                break;
            case R.id.btnPriceThree:
                if (storePrices.contains(Const.Store.STORE_PRICE_THREE)) {
                    storePrices.remove((Object) Const.Store.STORE_PRICE_THREE);
                    resetSelectedPrice((CustomFontTextView) view);
                } else {
                    storePrices.add(Const.Store.STORE_PRICE_THREE);
                    setSelectedPrice((CustomFontTextView) view);
                }
                break;
            case R.id.btnPriceFour:
                if (storePrices.contains(Const.Store.STORE_PRICE_FOUR)) {
                    storePrices.remove((Object) Const.Store.STORE_PRICE_FOUR);
                    resetSelectedPrice((CustomFontTextView) view);
                } else {
                    storePrices.add(Const.Store.STORE_PRICE_FOUR);
                    setSelectedPrice((CustomFontTextView) view);
                }
                break;

            default:
                // do something
                break;
        }
    }

    private void checkSelectedTime(View view) {

        switch (view.getId()) {
            case R.id.btnTimeOne:
                storeTime = Const.Store.STORE_TIME_20;
                resetSelectedTime();
                setSelectedTime((CustomFontTextView) view);
                break;
            case R.id.btnTimeTwo:
                storeTime = Const.Store.STORE_TIME_60;
                resetSelectedTime();
                setSelectedTime((CustomFontTextView) view);
                break;
            case R.id.btnTimeThree:
                storeTime = Const.Store.STORE_TIME_120;
                resetSelectedTime();
                setSelectedTime((CustomFontTextView) view);
                break;

            default:
                // do with default
                break;
        }
    }

    private void checkSelectedCategory(View view) {

        switch (view.getId()) {
            case R.id.btnStore:
                storeFilter(etStoreSearch.getText().toString(), getResources().getString(R.string
                        .text_store));

                etStoreSearch.setHint(getResources().getString(R.string.text_search_by) + " " +
                        getResources().getString(R.string.text_store));
                resetSelectedCategory();
                setSelectedCategory((CustomFontTextView) view);
                break;
            case R.id.btnItem:
                storeFilter(etStoreSearch.getText().toString(), getResources().getString(R.string
                        .text_item));
                etStoreSearch.setHint(getResources().getString(R.string.text_search_by) + " " +
                        getResources().getString(R.string.text_item));
                resetSelectedCategory();
                setSelectedCategory((CustomFontTextView) view);
                break;
            case R.id.btnTag:
                storeFilter(etStoreSearch.getText().toString(), getResources().getString(R.string
                        .text_tag));
                etStoreSearch.setHint(getResources().getString(R.string.text_search_by) + " " +
                        getResources().getString(R.string.text_tag));
                resetSelectedCategory();
                setSelectedCategory((CustomFontTextView) view);
                break;

            default:
                // do with default
                break;
        }
    }

    private void storeFilter(String filter, String filterBy) {
        try {
            if (storeAdapter != null) {
                storeAdapter.setFilterBy(filterBy);
                if (storeAdapter.getFilter() != null) {
                    storeAdapter.getFilter().filter(filter.trim());
                }
            }
        } catch (Exception e) {
            Log.e("storeFilter() " , e.getMessage());
        }
    }

    private void storeFilter(CharSequence filter) {
        try {
            if (storeAdapter != null && storeAdapter.getFilter() != null) {
                storeAdapter.getFilter().filter(filter.toString().trim());
            }
        } catch (Exception e) {
            Log.e("storeFilter() ", e.getMessage());
        }
    }

    private void checkSelectedDistance(View view) {

        switch (view.getId()) {
            case R.id.btnDistanceOne:
                storeDistance = Const.Store.STORE_DISTANCE_5;
                resetSelectedDistance();
                setSelectedDistance((CustomFontTextView) view);
                break;
            case R.id.btnDistanceTwo:
                storeDistance = Const.Store.STORE_DISTANCE_15;
                resetSelectedDistance();
                setSelectedDistance((CustomFontTextView) view);
                break;
            case R.id.btnDistanceThree:
                storeDistance = Const.Store.STORE_DISTANCE_25;
                resetSelectedDistance();
                setSelectedDistance((CustomFontTextView) view);
                break;

            default:
                // do with default
                break;
        }
    }

    private void resetAllSelectedPrice() {
        try {
            if (selectedPrice != null) {
                btnPriceOne.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                        .selector_round_shape_black, null));
                btnPriceOne.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                        .color_app_text, null));
                btnPriceTwo.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                        .selector_round_shape_black, null));
                btnPriceTwo.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                        .color_app_text, null));
                btnPriceThree.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                        .selector_round_shape_black, null));
                btnPriceThree.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                        .color_app_text, null));
                btnPriceFour.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                        .selector_round_shape_black, null));
                btnPriceFour.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                        .color_app_text, null));
            }
        } catch (Exception e) {
            Log.e("resetAllSelectedPrice() " , e.getMessage());
        }
    }

    private void resetSelectedPrice(CustomFontTextView view) {
        try {
            view.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                    .selector_round_shape_black, null));
            view.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                    .color_app_text, null));
        } catch (Exception e) {
            Log.e("resetSelectedPrice() " , e.getMessage());
        }
    }

    private void resetSelectedTime() {
        try {
            if (selectedTime != null) {
                selectedTime.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                        .selector_round_shape_black, null));
                selectedTime.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                        .color_app_text, null));
            }
        } catch (Exception e) {
            Log.e("resetSelectedTime() " , e.getMessage());
        }
    }

    private void resetSelectedDistance() {
        try {
            if (selectedDistance != null) {
                selectedDistance.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                        .selector_round_shape_black, null));
                selectedDistance.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                        .color_app_text, null));
            }
        } catch (Exception e) {
            Log.e("resetSelectedDistance() " , e.getMessage());
        }
    }

    private void resetSelectedCategory() {
        try {
            if (selectCategory != null) {
                if (etStoreSearch != null && etStoreSearch.getText() != null) {
                    etStoreSearch.getText().clear();
                }
                selectCategory.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable
                        .selector_round_shape_black, null));
                selectCategory.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                        .color_app_text, null));
            }
        } catch (Exception e) {
            Log.e("resetSelectedCategory() " , e.getMessage());
        }
    }

    private void resetFilter() {
        try {
            resetSelectedTime();
            resetAllSelectedPrice();
            resetSelectedDistance();
            storeDistance = 0;
            storeTime = 0;
            if (storePrices != null) {
                storePrices.clear();
            }
            if (storeListFiltered != null) {
                storeListFiltered.clear();
            }
            if (tagView != null) {
                tagView.clearSelected();
            }
            if (selectedTagList != null) {
                selectedTagList.clear();
            }
        } catch (Exception e) {
            Log.e("resetFilter() " ,e.getMessage());
        }
    }


    private void updateUiStoreList() {

        srlSelectedStore.setVisibility(View.VISIBLE);

        try {
            if (storeListOriginal != null) {
                if (storeListOriginal.isEmpty()) {
                    Utils.hideCustomProgressDialogForHome();
                    ivEmpty.setVisibility(View.VISIBLE);
                    rcvStore.setVisibility(View.GONE);
                    Utils.hideCustomProgressDialogForHome();
                } else {
                    Utils.hideCustomProgressDialogForHome();
                    ivEmpty.setVisibility(View.GONE);
                    rcvStore.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            Log.e("updateUiStoreList() " , e.getMessage());
        }
    }

    private void updateUiStoreFilter() {
        try {
            if (llStoreFilter.getVisibility() == View.VISIBLE) {
                llStoreFilter.setVisibility(View.GONE);
                relativeLayout.setVisibility(View.GONE);
                homeActivity.ivToolbarRightIcon2.setImageDrawable(null);
                //  homeActivity.setToolbarRightIcon3(R.drawable.ic_location_on_black_24dp, this);

            } else {
                llStoreFilter.setVisibility(View.VISIBLE);
                relativeLayout.setVisibility(View.GONE);
                homeActivity.ivToolbarRightIcon3.setImageDrawable(null);
                homeActivity.setToolbarRightIcon2(R.drawable.ic_cancel, this);
            }
        } catch (Exception e) {
            Log.e("updateUiStoreFilter() ", e.getMessage());
        }
    }

    private void updateUiMap() {
        try {


            if (relativeLayout.getVisibility() == View.VISIBLE) {
                relativeLayout.setVisibility(View.GONE);
                llStoreFilter.setVisibility(View.GONE);
                flStore.setVisibility(View.VISIBLE);
                homeActivity.ivToolbarRightIcon2.setImageDrawable(null);
                //    homeActivity.setToolbarRightIcon3(R.drawable.ic_location_on_black_24dp, this);
            } else {
                flStore.setVisibility(View.GONE);
                relativeLayout.setVisibility(View.VISIBLE);
                llStoreFilter.setVisibility(View.GONE);
                homeActivity.ivToolbarRightIcon2.setImageDrawable(null);
                homeActivity.setToolbarRightIcon3(R.drawable.ic_cancel, this);
                checkPermissions();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //  if (cu != null) {

                        if (homeActivity.currentBooking.getCurrentLatLng() != null && String.valueOf(homeActivity.currentBooking.getCurrentLatLng().latitude) != null && String.valueOf(homeActivity.currentBooking.getCurrentLatLng().longitude) != null) {
                            LatLng lt = new LatLng(homeActivity.currentBooking.getCurrentLatLng().latitude, homeActivity.currentBooking
                                    .getCurrentLatLng().longitude);
                            if (lt != null) {
                                zoomCamera(lt);
                            } else {
                                moveCameraFirstMyLocationNew(true, lt);
                            }
                        }
                    }
                }, 500);
            }
        } catch (Exception e) {
            Log.e("updateUiMap() ", e.getMessage());
        }
    }

    private void applyFiltered(boolean isAnySelectedStorePrice, boolean isTimeSelected, boolean
            isDistanceSelected, boolean isAnyTagSelected) {
        try {
            if (storeAdapter != null && storeListOriginal != null && storeListFiltered != null) {
                if (storeListOriginal.isEmpty()) {
                    return;
                }
                storeListFiltered.clear();
                if (!isTimeSelected) {
                    storeTime = Integer.MAX_VALUE;
                }
                if (!isDistanceSelected) {
                    storeDistance = Double.MAX_VALUE;
                }
                if (!isAnySelectedStorePrice && !isTimeSelected && !isDistanceSelected &&
                        !isAnyTagSelected) {
                    storeListFiltered.addAll(storeListOriginal);
                } else {
                    if (storePrices != null) {
                        for (Store store : storeListOriginal) {
                            if (store.getDeliveryTime() <= storeTime && store.getDistance() <=
                                    storeDistance) {
                                if (isAnySelectedStorePrice) {
                                    if (storePrices.contains(store.getPriceRating())) {
                                        storeListFiltered.add(store);
                                    }
                                } else {
                                    storeListFiltered.add(store);
                                }
                            }
                        }
                    }

                    if (isAnyTagSelected) {
                        if (storeListFiltered != null && selectedTagList != null) {
                            ArrayList<Store> arrayList = new ArrayList<>();
                            for (Store filterStore : storeListFiltered) {
                                boolean isAdded = false;
                                for (String selectedTag : selectedTagList) {
                                    if (filterStore.getFamousProductsTags().contains(selectedTag)) {
                                        isAdded = true;
                                    }
                                }
                                if (isAdded) {
                                    arrayList.add(filterStore);
                                }
                            }
                            storeListFiltered.clear();
                            storeListFiltered.addAll(arrayList);
                        }
                    }
                }
                if (storeAdapter != null && storeListFiltered != null) {
                    storeAdapter.setStoreArrayList(storeListFiltered);
                    storeAdapter.notifyDataSetChanged();
                }
            }
        } catch (Exception e) {
            Log.e("applyFiltered() " , e.getMessage());
        }
    }

    private void setFilterPriceData() {
        try {
            if (homeActivity.currentBooking != null && homeActivity.currentBooking.getCurrency() != null) {
                btnPriceOne.setText(homeActivity.currentBooking.getCurrency());
                btnPriceTwo.setText(homeActivity.currentBooking.getCurrency() + homeActivity.currentBooking
                        .getCurrency());
                btnPriceThree.setText(homeActivity.currentBooking.getCurrency() + homeActivity
                        .currentBooking
                        .getCurrency() +
                        homeActivity.currentBooking.getCurrency());
                btnPriceFour.setText(homeActivity.currentBooking.getCurrency() + homeActivity.currentBooking
                        .getCurrency() +
                        homeActivity.currentBooking.getCurrency() + homeActivity.currentBooking
                        .getCurrency());
            }
        } catch (Exception e) {
            Log.e("setFilterPriceData() " , e.getMessage());
        }
    }


    private void setStoreMarkerOnMap() {
        try {
            /*if (mapboxMap != null && storeListOriginal != null) {
                mapboxMap.clear();
                LatLngBounds.Builder bounds = new LatLngBounds.Builder();
                for (Store store : storeListOriginal) {
                    if (store.getLocation() != null && !store.getLocation().isEmpty()) {
                        LatLng latLng = new LatLng
                                (store.getLocation().get(0), store.getLocation().get
                                        (1));
                        AppLog.Log("LAT", latLng.toString());
                        MarkerOptions markerOptions = new MarkerOptions().position(latLng)
                                .title(store.getName()).snippet(String.valueOf(store
                                        .getDeliveryTime()) + " " +
                                        homeActivity.getResources().getString(R.string.unit_mins));
                        if (storeBitmap != null) {
                         *//*   markerOptions.icon(BitmapDescriptorFactory
                                    .fromBitmap(storeBitmap));*//*
                        }
                        //  mapboxMap.addMarker(markerOptions).setTag(store);
                        bounds.include(latLng);
                    }
                }
                if (homeActivity.currentBooking != null && homeActivity.currentBooking.getCurrentLatLng() != null) {
                   *//* mapboxMap.addMarker(new MarkerOptions().position(homeActivity.currentBooking
                            .getCurrentLatLng())
                            .title(homeActivity.getResources().getString(R.string.text_drop_location)).icon
                                    (BitmapDescriptorFactory
                                            .fromBitmap(Utils
                                                    .drawableToBitmap
                                                            (AppCompatResources.getDrawable
                                                                    (homeActivity, R
                                                                            .drawable
                                                                            .ic_pin_delivery)))))
                            .setSnippet(homeActivity.preferenceHelper.getFirstName() + " " +
                                    homeActivity.preferenceHelper
                                            .getLastName());*//*
                }
                if (!storeListOriginal.isEmpty()) {
                     cu= CameraUpdateFactory.newLatLngBounds(bounds.build(), getResources()
                            .getDimensionPixelSize(R.dimen.dimen_map_bounce));

                }
            }*/
        } catch (Exception e) {
            Log.e("setStoreMarkerOnMap() " , e.getMessage());
        }
    }


    @Override
    public void onStop() {
        if (storeAdapter != null) {
            storeAdapter.stopAdsScheduled();
        }
//        mapbox.onStop();

        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
//        mapbox.onStart();

    }

    @Override
    public void onPause() {

        super.onPause();
//        mapbox.onPause();

    }

    @Override
    public void onDestroy() {
        if (locationEngine != null) {
            locationEngine.removeLocationUpdates(callback);
        }
//        mapbox.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
//        mapbox.onResume();

        if (storeAdapter != null) {
            storeAdapter.notifyDataSetChanged();
        }
    }


    public void downloadStorePin() {
        try {
            Glide.with(this)
                    .asBitmap()
                    .load(PreferenceHelper.getInstance(getContext()).getIMAGE_BASE_URL() + deliveries.getMapPinUrl())
                    .diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable
                    .driver_car)
                    .listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            if (e != null) {
                                Log.e("downloadStorePin " , e.getMessage());
                            }
                            AppLog.handleException(getClass().getSimpleName(), e);
                            AppLog.Log("STORE_PIN", "Download failed");
                            storeBitmap = Utils.drawableToBitmap(AppCompatResources.getDrawable
                                    (homeActivity, R.drawable.ic_pin_pickup));
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                            storeBitmap = resource;
                            AppLog.Log("STORE_PIN", "Download Successfully");
                            return true;
                        }


                    }).dontAnimate().override(getResources().getDimensionPixelSize(R
                            .dimen.store_pin_width)
                    , getResources().getDimensionPixelSize(R
                            .dimen.store_pin_width)).preload(getResources()
                            .getDimensionPixelSize(R
                                    .dimen.store_pin_width)
                    , getResources().getDimensionPixelSize(R
                            .dimen.store_pin_width));
        } catch (Exception e) {
            Log.e("downloadStorePin() " , e.getMessage());
        }
    }

    private void setFavoriteStore(final Store storeData) {
        try {
            Utils.showCustomProgressDialog(homeActivity, false);
            JSONObject jsonObject = new JSONObject();
            try {
                if (homeActivity.preferenceHelper != null) {
                    if (homeActivity.preferenceHelper.getUserId() != null) {
                        jsonObject.put(Const.Params.USER_ID, homeActivity.preferenceHelper.getUserId());
                    }
                    if (homeActivity.preferenceHelper.getSessionToken() != null) {
                        jsonObject.put(Const.Params.SERVER_TOKEN, homeActivity.preferenceHelper
                                .getSessionToken());
                    }
                }
                if (storeData.getId() != null) {
                    jsonObject.put(Const.Params.STORE_ID, storeData.getId());
                }
            } catch (JSONException e) {
                Log.e("setFavoriteStore ", e.getMessage());
                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, e);
            }
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<SetFavouriteResponse> call = apiInterface.setFavouriteStore(ApiClient
                    .makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<SetFavouriteResponse>() {
                @Override
                public void onResponse(Call<SetFavouriteResponse> call, Response<SetFavouriteResponse
                        > response) {
                    Utils.hideCustomProgressDialog();
                    if (homeActivity.parseContent != null) {
                        if (homeActivity.parseContent.isSuccessful(response)) {
                            if (response.body() != null && response.body().isSuccess()) {
                                if (homeActivity.currentBooking != null) {
                                    if (homeActivity.currentBooking.getFavourite() != null) {
                                        homeActivity.currentBooking.getFavourite().clear();
                                    }
                                    if (response.body().getFavouriteStores() != null) {
                                        homeActivity.currentBooking.setFavourite(response.body()
                                                .getFavouriteStores());
                                    }
                                }
                            }
                            if (storeAdapter != null) {
                                storeAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<SetFavouriteResponse> call, Throwable t) {
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
                }
            });
        } catch (Exception e) {
            Log.e("setFavoriteStore() " , e.getMessage());
        }
    }

    private void removeAsFavoriteStore(final Store storeData) {
        try {
            if (homeActivity.preferenceHelper != null && homeActivity.preferenceHelper.getUserId() != null
                    && homeActivity.preferenceHelper.getSessionToken() != null) {
                Utils.showCustomProgressDialog(homeActivity, false);
                final ArrayList<String> store = new ArrayList<>();
                store.add(storeData.getId());
                ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<SetFavouriteResponse> call = apiInterface.removeAsFavouriteStore(ApiClient
                        .makeGSONRequestBody(new RemoveFavourite(homeActivity.preferenceHelper
                                .getSessionToken(),
                                homeActivity.preferenceHelper.getUserId(), store)));
                call.enqueue(new Callback<SetFavouriteResponse>() {
                    @Override
                    public void onResponse(Call<SetFavouriteResponse> call, Response<SetFavouriteResponse> response) {
                        Utils.hideCustomProgressDialog();
                        if (homeActivity.parseContent != null && homeActivity.currentBooking != null
                                && storeAdapter != null) {
                            if (homeActivity.parseContent.isSuccessful(response)) {
                                if (response.body() != null && response.body().isSuccess()) {
                                    if (homeActivity.currentBooking.getFavourite() != null) {
                                        homeActivity.currentBooking.getFavourite().clear();
                                    }
                                    if (response.body().getFavouriteStores() != null) {
                                        homeActivity.currentBooking.setFavourite(response.body()
                                                .getFavouriteStores());
                                    }
                                }
                                storeAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SetFavouriteResponse> call, Throwable t) {
                        Utils.hideCustomProgressDialog();
                        AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
                    }
                });
            }
        } catch (Exception e) {
            Log.e("removeAsFavoriteStore() " , e.getMessage());
        }
    }


    private double distanceTo(LatLng start, LatLng stop, boolean isUnitKM) {
        try {
            if (start != null & stop != null) {
                Location locationFrom = new Location("");
                locationFrom.setLatitude(start.getLatitude());
                locationFrom.setLongitude(start.getLongitude());

                Location locationTo = new Location("");
                locationTo.setLatitude(stop.getLatitude());
                locationTo.setLongitude(stop.getLongitude());
                if (isUnitKM) {
                    return locationFrom.distanceTo(locationTo) * 0.001; // Km
                } else {
                    return locationFrom.distanceTo(locationTo) * 0.000621371; // mile
                }

            } else {
                return 0;
            }
        } catch (Exception e) {
            Log.e("distanceTo() " ,e.getMessage());
            return 0;
        }
    }

    private void initTagView() {
        try {
            if (deliveries.getFamousProductsTags() != null) {
                tagView.addTags(deliveries.getFamousProductsTags());
            }
            tagView.setOnTagClickListener(new TagView.OnTagClickListener() {
                @Override
                public void onTagClick(View v, int position) {

                    if (selectedTagList.contains(deliveries.getFamousProductsTags().get(position)
                    )) {
                        selectedTagList.remove(deliveries.getFamousProductsTags().get(position));
                        tagView.setSelect(v, false);
                    } else {
                        selectedTagList.add(deliveries.getFamousProductsTags().get(position));
                        tagView.setSelect(v, true);

                    }


                }
            });
        } catch (Exception e) {
            Log.e("initTagView() " ,e.getMessage());
        }
    }

    public void slidStoreSearchFilterView() {
        try {
            if (llCategoryFilter.getVisibility() == View.GONE) {
                ivCategoryFilter.setImageDrawable(AppCompatResources.getDrawable(homeActivity, R
                        .drawable
                        .ic_arrow_drop_up_black_24dp));
                ResizeAnimation resizeAnimation = new ResizeAnimation(llCategoryFilter, getResources()
                        .getDimensionPixelSize(R.dimen.dimen_filter_height), llCategoryFilter.getHeight
                        ());
                resizeAnimation.setInterpolator(new LinearInterpolator());
                resizeAnimation.setDuration(300);
                llCategoryFilter.startAnimation(resizeAnimation);
                llCategoryFilter.setVisibility(View.VISIBLE);

            } else {
                ivCategoryFilter.setImageDrawable(AppCompatResources.getDrawable(homeActivity, R
                        .drawable
                        .ic_arrow_drop_down_black_24dp));
                ResizeAnimation resizeAnimation = new ResizeAnimation(llCategoryFilter, 1,
                        llCategoryFilter.getHeight());
                resizeAnimation.setInterpolator(new LinearInterpolator());
                resizeAnimation.setDuration(300);
                llCategoryFilter.startAnimation(resizeAnimation);
                llCategoryFilter.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        llCategoryFilter.setVisibility(View.GONE);

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

            }
        } catch (Exception e) {
            Log.e("slidStoreSearchFilterView() " , e.getMessage());
        }
    }


    // developer s
    public class MainActivityLocationCallback
            implements LocationEngineCallback<LocationEngineResult> {
        public WeakReference<Activity> activityWeakReference;

        MainActivityLocationCallback(Activity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(LocationEngineResult result) {
            Activity activity = activityWeakReference.get();

            if (activity != null) {
                Location location = result.getLastLocation();

                if (location == null) {
                    return;
                }

                if (!isCurrentLocationGet) {
                    isCurrentLocationGet = true;
                    // zoomCamera(new LatLng(location.getLatitude(), location.getLongitude()));
                    //zoomCamera(new LatLng(location.getLatitude(), location.getLongitude()));

                }
            }
        }

        @Override
        public void onFailure(@NonNull Exception exception) {
            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
            Activity activity = activityWeakReference.get();
            if (activity != null) {

            }
        }
    }

    private void zoomCamera(LatLng latLng) {

        /*if (mapboxMap != null) {
            mapboxMap.animateCamera(com.mapbox.mapboxsdk.camera.CameraUpdateFactory.newCameraPosition(
                    new com.mapbox.mapboxsdk.camera.CameraPosition.Builder()
                            .target(latLng)
                            .zoom(10.5)
                            .build()), 4000);
        }*/
        if (map != null) {
            map.setCenter(new GeoCoordinate(latLng.getLatitude(), latLng.getLongitude()), Map.Animation.LINEAR);
            map.setZoomLevel(12);
        }
    }


    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        /*if (granted) {
            if (mapboxMap.getStyle() != null) {
                enableLocationComponent(mapboxMap.getStyle());
            }
        }*/
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        /*LatLng mLatLng = mapboxMap.getCameraPosition().target;

        zoomCamera(mLatLng);*/


        //  moveCameraFirstMyLocation(true,);
    }

    @Override
    public void mapBoxMapReady(final MapboxMap mapboxMap, Style style) {
      /*  this.mapboxMap = mapboxMap;
        enableLocationComponent(style);
        mapboxMap.getUiSettings().setAttributionEnabled(false);
        mapboxMap.getUiSettings().setLogoEnabled(false);
        mapboxMap.getUiSettings().setCompassEnabled(false);

        mapboxMap.addOnCameraIdleListener(new MapboxMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                LatLng mLatLng = mapboxMap.getCameraPosition().target;
                if (mLatLng != null) {
                    Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    //   zoomCamera(mLatLng);
                    try {
                        List<Address> addresses = geocoder.getFromLocation(
                                mLatLng.getLatitude(),
                                mLatLng.getLongitude(),
                                1);
                        if (addresses != null && addresses.size() > 0 && addresses.get(0) != null) {
                            Log.e("tagMap", "onResponse: " + addresses.get(0).toString());
                            if (addresses.get(0).getCountryName() != null) {
                                Log.e("tagMap", "onResponse: " + addresses.get(0).getCountryName());


                                acDeliveryAddress.setText(addresses.get(0).getAddressLine(0));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        this.addMarkers();
        this.addInfoWindowListeners();

*/
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {

            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(getActivity(), loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            initLocationEngine();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    @SuppressLint("MissingPermission")
    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(getActivity());

        mLocationEngineRequest = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();
        locationEngine.requestLocationUpdates(mLocationEngineRequest, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }

    public void moveCameraFirstMyLocation(boolean isAnimate, LatLng latLng) {


        LatLng latLngOfMyLocation = null;
        if (latLng == null) {
        } else {
            latLngOfMyLocation = latLng;
        }
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLngOfMyLocation).zoom(10).build();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // mapbox.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        // mapbox.onLowMemory();
    }


    private void checkLocationPermission(LatLng latLng) {
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission
                .ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission
                            .ACCESS_FINE_LOCATION, android.Manifest.permission
                            .ACCESS_COARSE_LOCATION},
                    Const.PERMISSION_FOR_LOCATION);
        } else {
            moveCameraFirstMyLocation(true, latLng);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_LOCATION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        moveCameraFirstMyLocation(true, null);
                    }
                    break;

                case REQUEST_CODE_ASK_PERMISSIONS:
                    for (int index = permissions.length - 1; index >= 0; --index) {
                        if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                            // exit the app if one permission is not granted
                            Toast.makeText(getContext(), "Required permission '" + permissions[index]
                                    + "' not granted, exiting", Toast.LENGTH_LONG).show();
                            // finish();
                            homeActivity.finish();
                            return;
                        }
                    }
                    // all permissions were granted
                    // initialize();

                    setheremap();
                    break;
                default:
                    break;
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        checkLocationPermission(null);
                        break;
                    case Activity.RESULT_CANCELED:
                        break;

                    case Const.REQUEST_CODE_AUTOCOMPLETE:
                        if (data != null) {
                            CarmenFeature feature = PlaceAutocomplete.getPlace(data);
                            Log.d("call", "========= PlaceAutocomplete Result ==========" + feature.geometry().toString());

                            CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);


                            if (mapboxMap != null) {
                                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                                        new CameraPosition.Builder()
                                                .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                                        ((Point) selectedCarmenFeature.geometry()).longitude()))
                                                .zoom(10.5)
                                                .build()), 4000);
                            }
                        }

                }
                break;
            case Const.REQUEST_CODE:
                CarmenFeature carmenFeature = PlacePicker.getPlace(data);
                acDeliveryAddress.setText(carmenFeature.placeName());
                break;
            default:
                break;

        }
    }


    private void addMarkers() {
      /*  final List<Feature> symbolLayerIconFeatureList = new ArrayList<>();
        if (storeListOriginal.size() > 0) {
            for (final Store store : storeListOriginal) {
                if (store.getLocation() != null && !store.getLocation().isEmpty()) {
                    symbolLayerIconFeatureList.add(Feature.fromGeometry(
                            Point.fromLngLat(store.getLocation().get(1), store.getLocation().get
                                    (0))));

                    IconFactory iconFactory = IconFactory.getInstance(getActivity());
                    Icon icon = iconFactory.fromResource(R.drawable.ic_pin_pickup);
                    MarkerOptions markerOptions = new MarkerOptions()
                            .position(new LatLng(store.getLocation().get
                                    (0), store.getLocation().get(1))).icon(icon);
                    markerOptions.title(String.valueOf(Html.fromHtml("<b>" + store.getName() + "</b> ")));
                    markerOptions.snippet(String.valueOf(Html.fromHtml("<font color=red>" + store.getReOpenTime() + "</font>")) + "\n" + "Time " + String.valueOf(store.getDeliveryTime() + " " +
                            getResources().getString(R.string.unit_mins)));
                    mapboxMap.addMarker(markerOptions);

                }
            }
        }

        if (homeActivity.currentBooking != null && homeActivity.currentBooking.getCurrentLatLng() != null) {

            IconFactory iconFactory = IconFactory.getInstance(getActivity());
            Icon icon = iconFactory.fromResource(R.drawable.ic_pin_delivery);
            mapboxMap.addMarker(new MarkerOptions()
                    .position(new LatLng(homeActivity.currentBooking
                            .getCurrentLatLng().latitude, homeActivity.currentBooking
                            .getCurrentLatLng().longitude))
                    //   .title("Current Location")
                    .icon(icon));


        }*/
    }

    //for here map
    private void setheremap() {


        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View myView = inflater.inflate(R.layout.infow_windowlayout, null);
        TextView title = (TextView) myView.findViewById(R.id.txt_title);
        TextView status = (TextView) myView.findViewById(R.id.txt_status);
        TextView time = (TextView) myView.findViewById(R.id.txt_time);
        LinearLayout main = (LinearLayout) myView.findViewById(R.id.llmain);
        mapFragment = getMapFragment();


        if (mapFragment != null) {

            Utils.showCustomProgressDialog(getContext(), false);
            mapFragment.init(new OnEngineInitListener() {
                @Override
                public void onEngineInitializationCompleted(Error error) {


                    if (error == Error.NONE) {
                        // retrieve a reference of the map from the map fragment
                        map = mapFragment.getMap();
                        // Set the zoom level to the average between min and max
                        map.setZoomLevel((map.getMaxZoomLevel() + map.getMinZoomLevel()) / 1);
                        //               currentLocation = locationHelper.getLastLocation();
                        Utils.hideCustomProgressDialog();
                        //   moveCameraFirstMyLocationNew(true, null);

                        addheremapmarkers();
                        if (mapOverlay != null) {
                            map.removeMapOverlay(mapOverlay);
                        }
                        mapOverlay = new MapOverlay(myView, new GeoCoordinate(49.163, -123.137766, 10));

                        mapFragment.getMapGesture().addOnGestureListener(new MapGesture.OnGestureListener.OnGestureListenerAdapter() {
                            @Override
                            public boolean onMapObjectsSelected(List<ViewObject> list) {
                                for (ViewObject viewObject : list) {
                                    if (viewObject.getBaseType() == ViewObject.Type.USER_OBJECT) {
                                        MapObject mapObject = (MapObject) viewObject;

                                        if (mapObject.getType() == MapObject.Type.MARKER) {

                                            MapMarker window_marker = ((MapMarker) mapObject);

                                            System.out.println("Title is................." + window_marker.getTitle());
                                            title.setText(window_marker.getTitle());
                                            if (mapOverlay != null) {

                                                if (window_marker.getDescription() != null) {
                                                    String[] strings = window_marker.getDescription().split(",");
                                                    if (strings != null) {
                                                        status.setText(strings[0]);
                                                        time.setText(strings[1] + " minuts");
                                                        store_id = strings[2];
                                                        map.setCenter(window_marker.getCoordinate(), Map.Animation.LINEAR);

                                                        main.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                if (!TextUtils.isEmpty(store_id)) {
                                                                    Log.i("store_id", store_id);
                                                                    goToStoreProductActivity(store_id);
                                                                }
                                                            }
                                                        });

                                                    }
                                                }

                                                mapOverlay = new MapOverlay(myView, window_marker.getCoordinate());
                                                mapOverlay.setAnchorPoint(new PointF(200, 200));
                                                map.addMapOverlay(mapOverlay);


                                                map.addMapOverlay(mapOverlay);
                                            }


                                            return true;
                                        }
                                    }
                                }
                                myView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                });
                                return false;
                            }

                            @Override
                            public boolean onTapEvent(PointF pointF) {
                                if (mapOverlay != null) {
                                    map.removeMapOverlay(mapOverlay);
                                }

                                Log.e("tagMap", "onTapEvent()");

                                return false;
                            }

                        }, 0, false);


                    } else {
                        Utils.hideCustomProgressDialog();
                        System.out.println("ERROR: Cannot initialize Map Fragment");

                    }
                }
            });

        }
    }

    public void moveCameraFirstMyLocationNew(final boolean isAnimate, LatLng latLng) {
        if (latLng == null) {
            locationHelper.getLastLocation(homeActivity, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        LatLng latLng = new LatLng(location
                                .getLatitude(),
                                location.getLongitude());
                      /*  CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(latLng).zoom(17).build();*/

                        if (isAnimate) {
                            zoomCamera(latLng);
                        } else {
                         /*   mapboxMap.moveCamera(CameraUpdateFactory
                                    .newCameraPosition(cameraPosition));*/
                        }
                        locationHelper.setOpenGpsDialog(false);
                    }
                }
            });


        } else {
          /*  CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng).zoom(17).build();

            if (isAnimate) {
                mapboxMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            } else {
                mapboxMap.moveCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }
            locationHelper.setOpenGpsDialog(false);*/
        }

        LatLng latLngOfMyLocation = null;
        if (latLng == null) {

        } else {
            latLngOfMyLocation = latLng;
        }
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLngOfMyLocation).zoom(9).build();

        locationHelper.setOpenGpsDialog(false);


    }

    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(getContext(), permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(getActivity(), permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS, grantResults);
        }
    }

    private void addheremapmarkers() {
        int i;

        Image image = new Image();
        try {
            image.setImageResource(R.drawable.ic_pin_pickup);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ClusterLayer cl = new ClusterLayer();

        for (i = 0; i < storeListOriginal.size(); i++) {
            MapMarker mm = new MapMarker();
            GeoCoordinate mycordinates = new GeoCoordinate(storeListOriginal.get(i).getLocation().get(0), storeListOriginal.get(i).getLocation().get(1));
            // mm.setTitle(store.getName());
            mm.setIcon(image);
            mm.setTitle(storeListOriginal.get(i).getName());
            mm.setDescription(storeListOriginal.get(i).getReOpenTime() + "," + storeListOriginal.get(i).getDeliveryTimeMax() + "," + storeListOriginal.get(i).getId());

            mm.setCoordinate(mycordinates);
            map.addMapObject(mm);

            //if you want to set cluster
            //  map.addClusterLayer(cl);
        }
    }

}

