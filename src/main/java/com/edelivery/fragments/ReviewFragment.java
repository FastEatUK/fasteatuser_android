package com.edelivery.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RatingBar;

import com.edelivery.FeedbackActivity;
import com.edelivery.R;
import com.edelivery.ReviewActivity;
import com.edelivery.adapter.PublicReviewAdapter;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.RemainingReview;
import com.edelivery.models.datamodels.StoreReview;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.ReviewResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elluminati on 21-Nov-17.
 */

public class ReviewFragment extends Fragment {


    private RecyclerView rcvPublicReview;
    private ReviewActivity reviewActivity;
    private List<RemainingReview> remainStoreReview = new ArrayList<>();;
    private List<StoreReview> storePublicReview = new ArrayList<>();;
    private PublicReviewAdapter publicReviewAdapter;
    private CustomFontTextView tvReviewAverage, tv5StarCount, tv4StarCount, tv3StarCount,
            tv2StarCount, tv1StarCount;
    private RatingBar ratingBar;
    private ProgressBar bar5Star, bar4Star, bar3Star, bar2Star, bar1Star;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reviewActivity = (ReviewActivity) getActivity();
        synchronized (this) {
            storePublicReview = new ArrayList<>();
        }
        remainStoreReview = new ArrayList<>();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review, container, false);
        rcvPublicReview = (RecyclerView) view.findViewById(R.id.rcvPublicReview);
        tvReviewAverage = (CustomFontTextView) view.findViewById(R.id.tvReviewAverage);
        tv5StarCount = (CustomFontTextView) view.findViewById(R.id.tv5StarCount);
        tv4StarCount = (CustomFontTextView) view.findViewById(R.id.tv4StarCount);
        tv3StarCount = (CustomFontTextView) view.findViewById(R.id.tv3StarCount);
        tv2StarCount = (CustomFontTextView) view.findViewById(R.id.tv2StarCount);
        tv1StarCount = (CustomFontTextView) view.findViewById(R.id.tv1StarCount);
        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        bar5Star = (ProgressBar) view.findViewById(R.id.bar5Star);
        bar4Star = (ProgressBar) view.findViewById(R.id.bar4Star);
        bar3Star = (ProgressBar) view.findViewById(R.id.bar3Star);
        bar2Star = (ProgressBar) view.findViewById(R.id.bar2Star);
        bar1Star = (ProgressBar) view.findViewById(R.id.bar1Star);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRcvStorePublicReview();
        getStoreReview(reviewActivity.store.getId());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == Const.FEEDBACK_REQUEST) {
            getStoreReview(reviewActivity.store.getId());
        }

    }


    private void initRcvStorePublicReview() {
        rcvPublicReview.setLayoutManager(new LinearLayoutManager(reviewActivity));
        publicReviewAdapter = new PublicReviewAdapter(remainStoreReview, storePublicReview, this) {
            @Override
            public void onLike(int position) {
                setReviewLikeOrDislike(position, storePublicReview.get(position).isLike(), false);

            }

            @Override
            public void onDislike(int position) {
                setReviewLikeOrDislike(position, false, storePublicReview.get(position).isDislike
                        ());
            }
        };
        publicReviewAdapter.shouldShowHeadersForEmptySections(true);
        rcvPublicReview.setAdapter(publicReviewAdapter);
        rcvPublicReview.addItemDecoration(new DividerItemDecoration(reviewActivity,DividerItemDecoration
                .VERTICAL));

    }

    private void getStoreReview(String storeId) {
        Utils.showCustomProgressDialog(reviewActivity, false);
        JSONObject jsonObject = new JSONObject();
        try {
            if (reviewActivity.isCurrentLogin()) {
                jsonObject.put(Const.Params.USER_ID, reviewActivity.preferenceHelper.getUserId());
                jsonObject.put(Const.Params.SERVER_TOKEN, reviewActivity.preferenceHelper
                        .getSessionToken());
            }
            jsonObject.put(Const.Params.STORE_ID, storeId);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ReviewResponse> call = apiInterface.getStoreReview(ApiClient
                .makeJSONRequestBody(jsonObject));
        call.enqueue(new Callback<ReviewResponse>() {
            @Override
            public void onResponse(Call<ReviewResponse> call,
                                   Response<ReviewResponse
                                           > response) {
                Utils.hideCustomProgressDialog();
                AppLog.Log("REVIEW_STORE", ApiClient.JSONResponse(response.body()));
                if (reviewActivity.parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess())
                    {
                        remainStoreReview.clear();
                        storePublicReview.clear();
                        remainStoreReview.addAll(response.body().getRemainingReviewList());
                        storePublicReview.addAll(response.body().getStoreReviewList());
                        publicReviewAdapter.notifyDataSetChanged();
                        setReviewData(storePublicReview);


                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                reviewActivity);
                    }
                }

            }

            @Override
            public void onFailure(Call<ReviewResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
            }
        });
    }

    private void setReviewData(List<StoreReview> reviewData) {
        int oneStar = 0, twoStat = 0, threeStar = 0, fourStar = 0, fiveStar = 0;
        float rate = 0;
        for (StoreReview review : reviewData) {
            int rateRound = (int) Math.round(review.getUserRatingToStore());
            rate += review.getUserRatingToStore();
            switch (rateRound) {
                case 1:
                    oneStar++;
                    break;
                case 2:
                    twoStat++;
                    break;
                case 3:
                    threeStar++;
                    break;
                case 4:
                    fourStar++;
                    break;
                case 5:
                    fiveStar++;
                    break;
                default:
                    // do with default
                    break;
            }
        }
        tv1StarCount.setText(String.valueOf(oneStar));
        tv2StarCount.setText(String.valueOf(twoStat));
        tv3StarCount.setText(String.valueOf(threeStar));
        tv4StarCount.setText(String.valueOf(fourStar));
        tv5StarCount.setText(String.valueOf(fiveStar));
        int totalRating = reviewData.size();
        if (totalRating > 0) {
            twoStat = twoStat * 100 / totalRating;
            threeStar = threeStar * 100 / totalRating;
            fourStar = fourStar * 100 / totalRating;
            fiveStar = fiveStar * 100 / totalRating;
            bar5Star.setProgress(fiveStar);
            bar4Star.setProgress(fourStar);
            bar3Star.setProgress(threeStar);
            bar2Star.setProgress(twoStat);
            bar1Star.setProgress(oneStar);
            rate = (rate / totalRating);
        }
        tvReviewAverage.setText(String.valueOf((float) Math.round(rate)));
        ratingBar.setRating((float) Math.round(rate));
    }

    private void setReviewLikeOrDislike(final int position, final boolean like, final boolean
            dislike) {
        if (reviewActivity.isCurrentLogin()) {
            Utils.showCustomProgressDialog(reviewActivity, false);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Const.Params.USER_ID, reviewActivity.preferenceHelper.getUserId());
                jsonObject.put(Const.Params.SERVER_TOKEN, reviewActivity.preferenceHelper
                        .getSessionToken());
                jsonObject.put(Const.Params.IS_USER_CLICKED_LIKE_STORE_REVIEW, like);
                jsonObject.put(Const.Params.IS_USER_CLICKED_DISLIKE_STORE_REVIEW, dislike);
                jsonObject.put(Const.Params.REVIEW_ID, storePublicReview.get(position).getId());
            } catch (JSONException e) {
                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, e);
            }
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<IsSuccessResponse> call = apiInterface.setUserReviewLikeAndDislike(ApiClient
                    .makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse> response) {

                    if (reviewActivity.parseContent.isSuccessful(response)) {
                        if (response.body().isSuccess()) {
                            StoreReview reviewListItem = storePublicReview.get(position);
                            if (reviewListItem.isLike()) {
                                if (reviewListItem.getIdOfUsersDislikeStoreComment().contains
                                        (reviewActivity.preferenceHelper.getUserId())) {
                                    reviewListItem.getIdOfUsersDislikeStoreComment().remove
                                            (reviewActivity.preferenceHelper.getUserId());

                                }

                                reviewListItem.getIdOfUsersLikeStoreComment().add(reviewActivity
                                        .preferenceHelper.getUserId());
                            } else if (reviewListItem.isDislike()) {
                                if (reviewListItem.getIdOfUsersLikeStoreComment().contains
                                        (reviewActivity.preferenceHelper.getUserId())) {
                                    reviewListItem.getIdOfUsersLikeStoreComment().remove
                                            (reviewActivity.preferenceHelper.getUserId());

                                }

                                reviewListItem.getIdOfUsersDislikeStoreComment().add(reviewActivity
                                        .preferenceHelper.getUserId());
                            } else {
                                if (reviewListItem.getIdOfUsersLikeStoreComment().contains
                                        (reviewActivity.preferenceHelper.getUserId())) {
                                    reviewListItem.getIdOfUsersLikeStoreComment().remove
                                            (reviewActivity.preferenceHelper.getUserId());

                                }
                                if (reviewListItem.getIdOfUsersDislikeStoreComment().contains
                                        (reviewActivity.preferenceHelper.getUserId())) {
                                    reviewListItem.getIdOfUsersDislikeStoreComment().remove
                                            (reviewActivity.preferenceHelper.getUserId());

                                }
                            }
                            publicReviewAdapter.notifyDataSetChanged();

                        } else {
                            Utils.showErrorToast(response.body().getErrorCode(),
                                    reviewActivity);
                        }
                    }
                    Utils.hideCustomProgressDialog();
                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
                }
            });
        }
    }

    public void goToFeedbackActivity(String orderId, float rating) {
        Intent intent = new Intent(reviewActivity, FeedbackActivity.class);
        intent.putExtra(Const.SELECTED_STORE, reviewActivity.store);
        intent.putExtra(Const.Params.ORDER_ID, orderId);
        intent.putExtra(Const.Params.RATING, rating);
        startActivityForResult(intent, Const.FEEDBACK_REQUEST);
        reviewActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
