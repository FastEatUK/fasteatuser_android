package com.edelivery.fragments;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.edelivery.R;
import com.edelivery.adapter.InvoiceAdapter;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.Invoice;
import com.edelivery.models.datamodels.InvoicePayment;
import com.edelivery.utils.Utils;

import java.util.ArrayList;

/**
 * Created by elluminati on 26-Apr-17.
 */

public class HistoryInvoiceFragment extends BaseHistoryFragments {

    private LinearLayout llInvoiceDistance, llInvoicePayment;
    private CustomFontTextViewTitle tvOderTotal;
    private CustomFontButton btnInvoiceSubmit;
    private RecyclerView rcvInvoice;
    private ArrayList<Invoice> invoiceArrayList = new ArrayList<>();
    ;
    private CustomFontTextView tvInvoiceMsg;
    public String str_currency;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View invoiceFrag = inflater.inflate(R.layout.fragment_order_invoice, container, false);
        llInvoiceDistance = (LinearLayout) invoiceFrag.findViewById(R.id.llInvoiceDistance);
        llInvoicePayment = (LinearLayout) invoiceFrag.findViewById(R.id.llInvoicePayment);
        btnInvoiceSubmit = (CustomFontButton) invoiceFrag.findViewById(R.id.btnInvoiceSubmit);
        tvOderTotal = (CustomFontTextViewTitle) invoiceFrag.findViewById(R.id.tvOderTotal);
        rcvInvoice = (RecyclerView) invoiceFrag.findViewById(R.id.rcvInvoice);
        tvInvoiceMsg = (CustomFontTextView) invoiceFrag.findViewById(R.id.tvInvoiceMsg);
        return invoiceFrag;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnInvoiceSubmit.setVisibility(View.GONE);


        str_currency = "£";
        /*if(activity.detailResponse.getCurrency()!=null && !activity.detailResponse.getCurrency().equals(""))
        {
            if(activity.detailResponse.getCurrency().toUpperCase().contains("NULL")){
                str_currency=activity.detailResponse.getCurrency().replace("NULL", "");
            }else {
                str_currency=activity.detailResponse.getCurrency();
            }
        }
        else
        {
            str_currency="";
        }*/


        invoiceArrayList = activity.parseContent.parseInvoice(activity.detailResponse
                .getOrderHistoryDetail
                        ().getOrderPaymentDetail(), str_currency, false);


        setInvoiceData();
        setInvoiceDistanceAndTime();
        setInvoicePayments();
        setInvoiceMessage();


    }


    private void setInvoiceData() {

        rcvInvoice.setLayoutManager(new LinearLayoutManager(activity));
        rcvInvoice.setAdapter(new InvoiceAdapter(invoiceArrayList));
    }

    /**
     * this method will help to load a image in invoice screen
     *
     * @param title    set tile text
     * @param subTitle set sub title text
     * @param id       sett image id in image resource
     * @return invoicePayment object
     */
    private InvoicePayment loadInvoiceImage(String title, String subTitle, int id) {

        InvoicePayment invoicePayment = new InvoicePayment();
        invoicePayment.setTitle(title);
        invoicePayment.setValue(subTitle);
        invoicePayment.setImageId(id);
        return invoicePayment;

    }

    /**
     * this method used to set invoice distance and time view
     */
    private void setInvoiceDistanceAndTime() {

        String unit;
        if (activity.detailResponse.getOrderHistoryDetail().getOrderPaymentDetail()
                .isDistanceUnitMile()) {
            unit = "mile";
        } else {
            unit = "km";
        }

        ArrayList<InvoicePayment> invoicePayments = new ArrayList<>();


        String str_totaltime = String.valueOf(activity.detailResponse.getOrderHistoryDetail().getOrderPaymentDetail().getTotal_sec());
        if (str_totaltime != null && !str_totaltime.equals("")) {
            invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_time), Utils
                    .convertToHHMMSSFormate
                            (Double.parseDouble(str_totaltime)), R
                    .drawable.ic_wall_clock));
        }

        invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_distance),
                appendString(activity.detailResponse.getOrderHistoryDetail()
                        .getOrderPaymentDetail().getTotalDistance(), unit), R.drawable.ic_route));

        invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_payment),
                activity.detailResponse.getPayment(), activity.detailResponse
                        .getOrderHistoryDetail()
                        .getOrderPaymentDetail()
                        .isPaymentModeCash() ? R.drawable
                        .ic_cash : R.drawable.ic_credit_card_2));


        int size = invoicePayments.size();
        for (int i = 0; i < size; i++) {
            LinearLayout currentLayout = (LinearLayout) llInvoiceDistance.getChildAt(i);
            ImageView imageView = (ImageView) currentLayout.getChildAt(0);
            imageView.setImageDrawable
                    (AppCompatResources.getDrawable
                            (activity, invoicePayments.get(i).getImageId()));
            LinearLayout currentSubSubLayout = (LinearLayout) currentLayout.getChildAt(1);
            ((CustomFontTextView) currentSubSubLayout.getChildAt(0)).setText(invoicePayments.get
                    (i).getTitle());
            ((CustomFontTextView) currentSubSubLayout.getChildAt(1)).setText(invoicePayments.get
                    (i).getValue());
        }


    }

    /**
     * this method used to set invoice payment view
     */
    private void setInvoicePayments() {


        ArrayList<InvoicePayment> invoicePayments = new ArrayList<>();
        /*invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_wallet),
                appendString(str_currency,
                        activity.detailResponse
                                .getOrderHistoryDetail()
                                .getOrderPaymentDetail()
                                .getWalletPayment()), R.drawable.ic_wallet));*/

        if (activity.detailResponse.getOrderHistoryDetail().getOrderPaymentDetail()
                .isPaymentModeCash()) {
            invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_cash),
                    appendString(str_currency,
                            activity.detailResponse.getOrderHistoryDetail().getOrderPaymentDetail
                                    ().getCashPayment()), R.drawable
                            .ic_cash));
        } else {
            invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_card),
                    appendString(str_currency,
                            activity.detailResponse.getOrderHistoryDetail().getOrderPaymentDetail
                                    ().getCardPayment()), R.drawable
                            .ic_credit_card_2));
        }
        if (activity.detailResponse.getOrderHistoryDetail()
                .getOrderPaymentDetail().getPromoPayment() > 0) {
            llInvoicePayment.addView(LayoutInflater.from(activity).inflate(R.layout
                            .layout_invoice_item,
                    null));
            invoicePayments.add(loadInvoiceImage(getResources().getString(R.string.text_promo),
                    appendString(str_currency, activity.detailResponse.getOrderHistoryDetail()
                            .getOrderPaymentDetail().getPromoPayment()), R.drawable
                            .ic_promo_code));
        }


        int size = invoicePayments.size();
        for (int i = 0; i < size; i++) {
            LinearLayout currentLayout = (LinearLayout) llInvoicePayment.getChildAt(i);
            ImageView imageView = (ImageView) currentLayout.getChildAt(0);
            imageView.setImageDrawable
                    (AppCompatResources.getDrawable
                            (activity, invoicePayments.get(i).getImageId()));
            LinearLayout currentSubSubLayout = (LinearLayout) currentLayout.getChildAt(1);
            ((CustomFontTextView) currentSubSubLayout.getChildAt(0)).setText(invoicePayments.get
                    (i).getTitle());
            ((CustomFontTextView) currentSubSubLayout.getChildAt(1)).setText(invoicePayments.get
                    (i).getValue());
        }

        tvOderTotal.setText(str_currency +
                activity.parseContent.decimalTwoDigitFormat
                        .format(activity.detailResponse.getOrderHistoryDetail()
                                .getOrderPaymentDetail()
                                .getTotal
                                        ()));
    }


    private String appendString(String string, Double value) {
        return string + activity.parseContent.decimalTwoDigitFormat.format(value);
    }

    private String appendString(Double value, String unit) {
        return activity.parseContent.decimalTwoDigitFormat.format(value) +
                unit;
    }


    private void setInvoiceMessage() {
        if (activity.detailResponse.getOrderHistoryDetail().getOrderPaymentDetail()
                .isStorePayDeliveryFees() && activity.detailResponse.getOrderHistoryDetail()
                .getOrderPaymentDetail().isPaymentModeCash()) {
            tvInvoiceMsg.setText(getResources().getString(R.string
                    .msg_delivery_fee_free_and_cash_pay));
        } else if (activity.detailResponse.getOrderHistoryDetail().getOrderPaymentDetail()
                .isStorePayDeliveryFees() && !activity.detailResponse.getOrderHistoryDetail()
                .getOrderPaymentDetail().isPaymentModeCash()) {
            tvInvoiceMsg.setText(getResources().getString(R.string
                    .msg_delivery_fee_free_and_other_pay));
        } else if (activity.detailResponse.getOrderHistoryDetail().getOrderPaymentDetail()
                .isPaymentModeCash()) {
            tvInvoiceMsg.setText(getResources().getString(R.string
                    .msg_pay_cash));
        } else {
            tvInvoiceMsg.setText(getResources().getString(R.string
                    .msg_pay_other));
        }
    }

    @Override
    public void onClick(View v) {

    }
}
