package com.edelivery.fragments;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.edelivery.R;
import com.edelivery.ReviewActivity;
import com.edelivery.adapter.StoreTimeAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomPhotoDialog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

/**
 * Created by elluminati on 21-Nov-17.
 */

public class OverviewFragment extends Fragment implements View.OnClickListener {

    private CustomFontTextView tvStoreAddress, tvSlogan, tvStoreTime, tvStoreWebsite,
            tvStorePhoneNumber;
    private LinearLayout btnShare, btnGetDirection;
    private ReviewActivity reviewActivity;
    private RecyclerView rcvStoreTime;
    private CustomDialogAlert customDialogAlert;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reviewActivity = (ReviewActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overview, container, false);
        tvStoreAddress = (CustomFontTextView) view.findViewById(R.id.tvStoreAddress);
        tvSlogan = (CustomFontTextView) view.findViewById(R.id.tvSlogan);
        tvStoreTime = (CustomFontTextView) view.findViewById(R.id.tvStoreTime);
        tvStoreWebsite = (CustomFontTextView) view.findViewById(R.id.tvStoreWebsite);
        tvStorePhoneNumber = (CustomFontTextView) view.findViewById(R.id.tvStorePhoneNumber);
        btnGetDirection = (LinearLayout) view.findViewById(R.id.btnGetDirection);
        btnShare = (LinearLayout) view.findViewById(R.id.btnShare);
        rcvStoreTime = (RecyclerView) view.findViewById(R.id.rcvStoreTime);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnGetDirection.setOnClickListener(this);
        btnShare.setOnClickListener(this);
        tvStoreAddress.setText(reviewActivity.store.getAddress());
        tvSlogan.setText(reviewActivity.store.getSlogan());
        tvStoreWebsite.setText(reviewActivity.store.getWebsiteUrl());
        tvStorePhoneNumber.setText(reviewActivity.store.getCountryPhoneCode() +
                reviewActivity.store
                        .getPhone());
        tvStoreAddress.setVisibility(TextUtils.isEmpty(reviewActivity.store.getAddress()) ? View
                .GONE : View.VISIBLE);
        tvSlogan.setVisibility(TextUtils.isEmpty(reviewActivity.store.getSlogan()) ? View
                .GONE : View.VISIBLE);
        tvStoreWebsite.setVisibility(TextUtils.isEmpty(reviewActivity.store.getWebsiteUrl()) ? View
                .GONE : View.VISIBLE);
        tvStorePhoneNumber.setVisibility(TextUtils.isEmpty(reviewActivity.store.getPhone()) ? View
                .GONE : View.VISIBLE);
        rcvStoreTime.setLayoutManager(new LinearLayoutManager(reviewActivity));
        rcvStoreTime.setAdapter(new StoreTimeAdapter(reviewActivity.store.getStoreTime()));
        rcvStoreTime.setNestedScrollingEnabled(false);
        rcvStoreTime.setVisibility(View.GONE);
        tvStoreTime.setOnClickListener(this);
        tvStorePhoneNumber.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvStoreWebsite:
                Utils.openWebPage(reviewActivity, reviewActivity.store.getWebsiteUrl());
                break;
            case R.id.btnGetDirection:
                openPhotoMapDialog();
                break;
            case R.id.btnShare:
                shareStoreDetail();
                break;
            case R.id.tvStorePhoneNumber:
                MakePhoneCallToStore();
                break;
            case R.id.tvStoreTime:
                if (rcvStoreTime.getVisibility() == View.GONE) {
                    rcvStoreTime.setVisibility(View.VISIBLE);
                    tvStoreTime.setCompoundDrawablesRelativeWithIntrinsicBounds
                            (AppCompatResources.getDrawable(reviewActivity, R.drawable
                                    .ic_clock_01), null, AppCompatResources.getDrawable
                                    (reviewActivity, R.drawable
                                            .ic_arrow_drop_up_black_24dp), null);
                } else {
                    rcvStoreTime.setVisibility(View.GONE);
                    tvStoreTime.setCompoundDrawablesRelativeWithIntrinsicBounds
                            (AppCompatResources.getDrawable(reviewActivity, R.drawable
                                    .ic_clock_01), null, AppCompatResources.getDrawable
                                    (reviewActivity, R.drawable
                                            .ic_arrow_drop_down_black_24dp), null);
                }

                break;
            default:
                // do with default
                break;
        }
    }

    /**
     * this method is used to open Google Map app whit given LatLng
     */
    private void goToGoogleMapApp() {

        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + reviewActivity.store
                .getLocation().get
                        (0) + "," + reviewActivity.store.getLocation().get(1));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(reviewActivity.getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            Utils.showToast(getResources().getString(R.string
                    .msg_google_app_not_installed), reviewActivity);
        }


    }

    /**
     * this method is used to open Waze Map app whit given LatLng
     */
    private void goToWazeMapApp() {
        try {
            String url = "waze://?ll=" + reviewActivity.store.getLocation().get
                    (0) + "," + reviewActivity.store.getLocation().get(1) + "&navigate=yes";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Intent intent =
                    new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
            startActivity(intent);
            Utils.showToast(getResources().getString(R.string.waze_map_msg), reviewActivity);
        }
    }

    private void openPhotoMapDialog() {
        //Do the stuff that requires permission...
        CustomPhotoDialog customPhotoDialog = new CustomPhotoDialog(reviewActivity, getResources()
                .getString(R.string.text_choose_map), getResources()
                .getString(R.string.text_google_map), getResources()
                .getString(R.string.text_waze_map)) {
            @Override
            public void clickedOnCamera() {
                goToGoogleMapApp();
                dismiss();
            }

            @Override
            public void clickedOnGallery() {
                goToWazeMapApp();
                dismiss();
            }
        };
        customPhotoDialog.show();

    }

    public void MakePhoneCallToStore() {
        if (!TextUtils.isEmpty(reviewActivity.store.getPhone())) {
            /*if (ContextCompat.checkSelfPermission(reviewActivity, Manifest.permission
                    .CALL_PHONE) !=
                    PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest
                        .permission
                        .CALL_PHONE}, Const
                        .PERMISSION_FOR_CALL);
            } else {*/
                //Do the stuff that requires permission...

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + reviewActivity.store.getCountryPhoneCode()
                        + reviewActivity.store.getPhone()));
                startActivity(intent);
          //  }
        }
    }

    private void openCallPermissionDialog() {
        if (customDialogAlert != null && customDialogAlert.isShowing()) {
            return;
        }
        customDialogAlert = new CustomDialogAlert(reviewActivity, getResources().getString(R
                .string
                .text_attention), getResources().getString(R
                .string
                .msg_reason_for_call_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                closedPermissionDialog();
            }

            @Override
            public void onClickRightButton() {
                requestPermissions(new String[]{Manifest
                        .permission
                        .CALL_PHONE}, Const
                        .PERMISSION_FOR_CALL);
                closedPermissionDialog();
            }

        };
        customDialogAlert.show();
    }

    private void closedPermissionDialog() {
        if (customDialogAlert != null && customDialogAlert.isShowing()) {
            customDialogAlert.dismiss();
            customDialogAlert = null;

        }
    }

    private void goWithCallPermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            MakePhoneCallToStore();

        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (reviewActivity, Manifest
                            .permission.CALL_PHONE)) {
                openCallPermissionDialog();
            } else {
                closedPermissionDialog();

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CALL:
                    goWithCallPermission(grantResults);
                    break;
                default:
                    //do with default
                    break;
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.PERMISSION_FOR_CALL:
                MakePhoneCallToStore();
                break;
            default:
                //do with default
                break;
        }
    }

    private void shareStoreDetail() {
        String msg = getResources().getString(R.string.text_try) + " " + reviewActivity.store
                .getName()
                + "" +
                "(" + reviewActivity.store
                .getWebsiteUrl()
                + ")" + getResources().getString(R.string.text_share_text);
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, msg);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString
                (R.string.msg_share_referral)));
    }
}
