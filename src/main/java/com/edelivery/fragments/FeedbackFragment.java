package com.edelivery.fragments;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.res.ResourcesCompat;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.edelivery.OrderCompleteActivity;
import com.edelivery.R;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

/**
 * Created by elluminati on 04-Jul-17.
 */

public class FeedbackFragment extends Fragment implements View.OnClickListener, TextView
        .OnEditorActionListener {

    private ImageView ivProviderImageFeedback;
    private CustomFontTextView tvProviderNameFeedback;
    private RatingBar ratingBarFeedback;
    private CustomFontEditTextView etFeedbackReview;
    private CustomFontButton btnSubmitFeedback;
    private OrderCompleteActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (OrderCompleteActivity) getActivity();
        activity.setTitleOnToolBar(activity.getResources().getString(R.string.text_feedback));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View fragFeedback = inflater.inflate(R.layout.fragment_feedback, container, false);
        ivProviderImageFeedback = (ImageView) fragFeedback.findViewById(R.id
                .ivProviderImageFeedback);
        tvProviderNameFeedback = (CustomFontTextView) fragFeedback.findViewById(R.id
                .tvProviderNameFeedback);
        ratingBarFeedback = (RatingBar) fragFeedback.findViewById(R.id.ratingBarFeedback);
        etFeedbackReview = (CustomFontEditTextView) fragFeedback.findViewById(R.id
                .etFeedbackReview);
        btnSubmitFeedback = (CustomFontButton) fragFeedback.findViewById(R.id.btnSubmitFeedback);
        return fragFeedback;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnSubmitFeedback.setOnClickListener(this);
        etFeedbackReview.setOnEditorActionListener(this);
        ratingBarFeedback.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                ratingBarFeedback.setRating(v);
            }
        });
        loadData();

    }

    private void loadData() {
        if (activity.activeOrderResponse != null && activity.user == null && activity.store
                == null) {
            Glide.with(this).load(PreferenceHelper.getInstance(activity.getApplicationContext()).getIMAGE_BASE_URL() + activity.activeOrderResponse.getProviderImage
                    ()).dontAnimate()
                    .placeholder
                            (ResourcesCompat.getDrawable(this
                                    .getResources(), R.drawable.placeholder, null)).fallback
                    (ResourcesCompat
                            .getDrawable
                                    (this.getResources(), R.drawable.placeholder, null)).into
                    (ivProviderImageFeedback);
            tvProviderNameFeedback.setText(activity.activeOrderResponse.getProviderFirstName() +
                    " " +
                    "" + activity.activeOrderResponse.getProviderLastName());
        } else if (activity.user != null && activity.activeOrderResponse == null && activity
                .store
                == null) {
            Glide.with(this).load(PreferenceHelper.getInstance(activity.getApplicationContext()).getIMAGE_BASE_URL() + activity.user.getImageUrl()).dontAnimate()
                    .placeholder
                            (ResourcesCompat.getDrawable(this
                                    .getResources(), R.drawable.placeholder, null)).fallback
                    (ResourcesCompat
                            .getDrawable
                                    (this.getResources(), R.drawable.placeholder, null)).into
                    (ivProviderImageFeedback);
            tvProviderNameFeedback.setText(activity.user.getFirstName() +
                    " " +
                    "" + activity.user.getLastName());
        } else {
            Glide.with(this).load(PreferenceHelper.getInstance(activity.getApplicationContext()).getIMAGE_BASE_URL() + activity.store.getImageUrl()).dontAnimate()
                    .placeholder
                            (ResourcesCompat.getDrawable(this
                                    .getResources(), R.drawable.placeholder, null)).fallback
                    (ResourcesCompat
                            .getDrawable
                                    (this.getResources(), R.drawable.placeholder, null)).into
                    (ivProviderImageFeedback);
            tvProviderNameFeedback.setText(activity.store.getName());
        }


    }

    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.btnSubmitFeedback:
                submitRatting();
                break;

            default:
                // do with default
                break;
        }
    }

    private void submitRatting() {
        if (ratingBarFeedback.getRating() == 0) {
            Utils.showToast(getResources().getString(R.string.msg_plz_give_rating), activity);
        } else {
            giveFeedback();
        }
    }


    /**
     * this method call a webservice for give feedback to delivery man
     */

    private void giveFeedback() {
        Utils.showCustomProgressDialog(activity, false);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, activity.preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, activity.preferenceHelper
                    .getUserId());
            jsonObject.put(Const.Params.ORDER_ID, activity.orderId);
            if (activity.store == null) {
                jsonObject.put(Const.Params.USER_RATING_TO_PROVIDER, ratingBarFeedback
                        .getRating());
                jsonObject.put(Const.Params.USER_REVIEW_TO_PROVIDER, etFeedbackReview
                        .getText().toString());

                responseCall = apiInterface.setFeedbackProvider(ApiClient
                        .makeJSONRequestBody(jsonObject));
            } else {
                jsonObject.put(Const.Params.USER_RATING_TO_STORE, ratingBarFeedback
                        .getRating());
                jsonObject.put(Const.Params.USER_REVIEW_TO_STORE, etFeedbackReview
                        .getText().toString());

                responseCall = apiInterface.setFeedbackStore(ApiClient
                        .makeJSONRequestBody(jsonObject));
            }

            AppLog.Log("FEED_BACK_PARAM", jsonObject.toString());


            responseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && activity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        Utils.showMessageToast(response.body().getMessage(),
                                activity);
                        activity.setResult(Activity.RESULT_OK);
                        activity.onBackPressed();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                activity);
                    }
                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(OrderCompleteActivity.class.getName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(OrderCompleteActivity.class.getName(), e);
        }

    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        switch (textView.getId()) {
            case R.id.etFeedbackReview:
                if (i == EditorInfo.IME_ACTION_DONE) {
                    submitRatting();
                    return true;
                }
                break;
            default:
                //Do som thing
                break;
        }

        return false;
    }
}
