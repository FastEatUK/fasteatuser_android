package com.edelivery.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.View;

import com.edelivery.HomeActivity;

/**
 * Created by elluminati on 14-06-2016.
 */
public abstract class BaseMainFragments extends Fragment implements View.OnClickListener {

    protected HomeActivity homeActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeActivity = (HomeActivity) getActivity();
        if (homeActivity != null &&  homeActivity.tvTitleToolbar != null && !homeActivity.isFinishing()) {
            homeActivity.tvTitleToolbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickToolbarTitle();
                }
            });
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    protected abstract void onClickToolbarTitle();
}
