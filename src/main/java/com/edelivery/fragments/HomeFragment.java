/*
package com.edelivery.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.edelivery.DeliveryLocationActivity;
import com.edelivery.R;
import com.edelivery.StoreProductActivity;
import com.edelivery.StoresActivity;
import com.edelivery.adapter.DeliveryStoreAdapter;
import com.edelivery.animation.AlphaInAnimationAdapter;
import com.edelivery.animation.ScaleInAnimationAdapter;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.Deliveries;
import com.edelivery.utils.Const;

*/
/**
 * Created by elluminati on 09-Feb-17.
 */

package com.edelivery.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.edelivery.DeliveryLocationActivity;
import com.edelivery.HomeActivity;
import com.edelivery.R;
import com.edelivery.StoreProductActivity;
import com.edelivery.StoresActivity;
import com.edelivery.adapter.DeliveryStoreAdapter;
import com.edelivery.animation.AlphaInAnimationAdapter;
import com.edelivery.animation.ScaleInAnimationAdapter;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.Deliveries;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.mapbox.mapboxsdk.geometry.LatLng;

/**
 * Created by elluminati on 09-Feb-17.
 */

public class HomeFragment extends BaseMainFragments {

    private RecyclerView rcvStoreCategory;
    private DeliveryStoreAdapter deliveryStoreAdapter;
    private LinearLayout imageView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (homeActivity != null && homeActivity.currentBooking != null) {
            if (homeActivity.currentBooking.getCurrentAddress() != null && !TextUtils.isEmpty(homeActivity.currentBooking.getCurrentAddress())) {
                // homeActivity.setTitleOnToolBar(getResources().getString(R.string.text_ASAP) + " " + homeActivity.currentBooking.getCurrentAddress());
                //   homeActivity.currentBooking.setDeliveryAddress(homeActivity.currentBooking.getCurrentAddress());
            } else {
                //   homeActivity.setTitleOnToolBar(getResources().getString(R.string.text_ASAP));
            }
            //  homeActivity.setToolbarRightIcon3(R.drawable.ic_location_on_black_24dp, this);
        }
    }

    @Override
    protected void onClickToolbarTitle() {
        goToDeliveryLocationActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View homeFragView = inflater.inflate(R.layout.fragment_home, container, false);

        rcvStoreCategory = (RecyclerView) homeFragView.findViewById(R.id.rcvDeliveryStore);
        imageView = (LinearLayout) homeFragView.findViewById(R.id.ivEmpty);
        return homeFragView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utils.showCustomProgressDialog(getContext(), false);
        initRcvDeliveryStore();
        Utils.hideCustomProgressDialog();
    }


    @Override
    public void onStop() {
        if (deliveryStoreAdapter != null) {
            deliveryStoreAdapter.stopAdsScheduled();
        }
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivToolbarRightIcon3:
                goToDeliveryLocationActivity();
                break;
            default:
                // do with default
                break;
        }
    }


    private void initRcvDeliveryStore() {
        Log.i("initRcvDeliveryStore", "done");
        try {
            updateUiList();
            if (deliveryStoreAdapter != null) {
                deliveryStoreAdapter.notifyDataSetChanged();
            } else {
                if (homeActivity != null && homeActivity.currentBooking != null && homeActivity.currentBooking.getDeliveryStoreList() != null
                        && homeActivity.currentBooking.getAds() != null) {
                    deliveryStoreAdapter = new DeliveryStoreAdapter(this,
                            homeActivity.currentBooking.getDeliveryStoreList(), homeActivity
                            .currentBooking.getAds());
                    AlphaInAnimationAdapter animationAdapter = new AlphaInAnimationAdapter
                            (deliveryStoreAdapter);
                    rcvStoreCategory.setAdapter(new ScaleInAnimationAdapter(animationAdapter));
                    rcvStoreCategory.setLayoutManager(new LinearLayoutManager(homeActivity));
                    rcvStoreCategory.addOnItemTouchListener(new RecyclerTouchListener(homeActivity,
                            rcvStoreCategory, new ClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                            try {
                                if (position > 0) {
                                    updateUiList();
                                    goToStoreActivity(homeActivity.currentBooking.getDeliveryStoreList().get
                                            (position - deliveryStoreAdapter.header));
                                }
                            } catch (Exception e) {
                                Log.e("initRcvDeliveryStore() rcvStoreCategory.addOnItemTouchListener " , e.getMessage());
                            }
                        }

                        @Override
                        public void onLongClick(View view, int position) {

                        }
                    }));
                }
            }
        } catch (Exception e) {
            Log.e("initRcvDeliveryStore() " , e.getMessage());
        }
        Log.i("initRcvDeliveryStore", "complete");

        Utils.hideCustomProgressDialogForHome();
        Utils.hideCustomProgressDialogForHome();
    }


    private void goToDeliveryLocationActivity() {
        if (homeActivity != null) {
            Intent intent = new Intent(homeActivity, DeliveryLocationActivity.class);
            startActivityForResult(intent, Const.DELIVERY_LIST_CODE);
            homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    private void goToStoreActivity(Deliveries deliveries) {
        if (homeActivity != null) {
            Intent intent = new Intent(homeActivity, StoresActivity.class);
            if (deliveries != null) {
                intent.putExtra(Const.DELIVERY_STORE, deliveries);
            }
            startActivity(intent);
            homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    public void goToStoreProductActivity(String storeId) {
        if (homeActivity != null) {
            Intent intent = new Intent(homeActivity, StoreProductActivity.class);
            intent.putExtra(Const.STORE_DETAIL, storeId);
            startActivity(intent);
            homeActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    private void updateUiList() {
        try {
            Log.i("updateUiList", "updateUiList");
            if (homeActivity != null && homeActivity.currentBooking != null && !homeActivity.currentBooking.isDeliveryAvailable() && homeActivity.currentBooking.getDeliveryStoreList().isEmpty()) {
                Log.i("updateUiList", "updateUiListif");
                imageView.setVisibility(View.VISIBLE);
                rcvStoreCategory.setVisibility(View.GONE);
            } else {
                Log.i("updateUiList", "updateUiListelse");
                Utils.hideCustomProgressDialogForHome();
                imageView.setVisibility(View.GONE);
                rcvStoreCategory.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            Log.e("updateUiList() " , e.getMessage());
        }
        Log.i("updateUiList", "complete");
    }

}

