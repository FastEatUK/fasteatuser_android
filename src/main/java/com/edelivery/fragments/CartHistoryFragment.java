package com.edelivery.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.edelivery.CartActivity;
import com.edelivery.R;
import com.edelivery.adapter.OrderDetailsAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontButton;
import com.edelivery.models.datamodels.CartOrder;
import com.edelivery.models.datamodels.CartProducts;
import com.edelivery.models.responsemodels.AddCartResponse;
import com.edelivery.models.responsemodels.CartResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PinnedHeaderItemDecoration;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elluminati on 26-Apr-17.
 */

public class CartHistoryFragment extends BaseHistoryFragments {


    private RecyclerView rcvOrderProductItem;
    private CustomFontButton btnReorder;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View invoiceFrag = inflater.inflate(R.layout.fragment_cart_history, container, false);
        rcvOrderProductItem = (RecyclerView) invoiceFrag.findViewById(R.id.rcvOrderProductItem);
        btnReorder = (CustomFontButton) invoiceFrag.findViewById(R.id.btnReorder);
        return invoiceFrag;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rcvOrderProductItem.setLayoutManager(new LinearLayoutManager(activity));


        String str_currency;
        if (activity.detailResponse.getCurrency() != null && !activity.detailResponse.getCurrency().equals("")) {
            if (activity.detailResponse.getCurrency().toUpperCase().contains("NULL")) {
                str_currency = activity.detailResponse.getCurrency().toUpperCase().replace("NULL", "");
            } else {
                str_currency = activity.detailResponse.getCurrency();
            }
        } else {
            str_currency = "";
        }


        OrderDetailsAdapter itemAdapter = new OrderDetailsAdapter(activity, activity
                .detailResponse.getOrderHistoryDetail().getCartDetail().getOrderDetails(),
                str_currency, false);
        rcvOrderProductItem.setAdapter(itemAdapter);
        rcvOrderProductItem.addItemDecoration(new PinnedHeaderItemDecoration());
        btnReorder.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnReorder:
                if (activity.currentBooking.getCartProductWithSelectedSpecificationList().isEmpty
                        ()) {
                    addItemInServerCart();
                } else {
                    openClearCartDialog();
                }
                break;

            default:
                // do with default
                break;
        }
    }

    /**
     * this method called a webservice for add item in cart
     */
    private void addItemInServerCart() {
        Utils.showCustomProgressDialog(activity, false);

        CartOrder cartOrder = new CartOrder();
        cartOrder.setUserType(Const.Type.USER);
        if (activity.isCurrentLogin()) {
            cartOrder.setUserId(activity.preferenceHelper.getUserId());
            cartOrder.setAndroidId("");
        } else {
            cartOrder.setAndroidId(activity.preferenceHelper.getAndroidId());
            cartOrder.setUserId("");
        }
        cartOrder.setServerToken(activity.preferenceHelper.getSessionToken());
        cartOrder.setStoreId(activity.detailResponse.getOrderHistoryDetail().getStoreId());
        cartOrder.setProducts(activity.detailResponse.getOrderHistoryDetail().getCartDetail()
                .getOrderDetails());
        cartOrder.setPickupAddresses(activity.detailResponse.getOrderHistoryDetail()
                .getCartDetail().getPickupAddresses());
        cartOrder.setDestinationAddresses(activity.detailResponse.getOrderHistoryDetail()
                .getCartDetail().getDestinationAddresses());

        // add filed on 4_Aug_2018
        double cartOrderTotalPrice = 0, cartOrderTotalTaxPrice = 0;
        for (CartProducts products : activity.detailResponse.getOrderHistoryDetail().getCartDetail()
                .getOrderDetails()) {
            cartOrderTotalPrice = cartOrderTotalPrice + products
                    .getTotalProductItemPrice();
            cartOrderTotalTaxPrice = cartOrderTotalTaxPrice + products.getTotalItemTax();
        }

        cartOrder.setCartOrderTotalPrice(cartOrderTotalPrice);
        cartOrder.setCartOrderTotalTaxPrice(cartOrderTotalTaxPrice);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddCartResponse> responseCall = apiInterface.addItemInCart(ApiClient
                .makeGSONRequestBody(cartOrder));
        responseCall.enqueue(new Callback<AddCartResponse>() {
            @Override
            public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse>
                    response) {

                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && activity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    getCart();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), activity);
                }
            }

            @Override
            public void onFailure(Call<AddCartResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PRODUCT_SPE_ACTIVITY, t);
            }
        });
    }

    private void getCart() {
        Utils.showCustomProgressDialog(activity, false);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CartResponse> orderCall = apiInterface.getCart(ApiClient.makeJSONRequestBody
                (activity.getCommonParam()));
        orderCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                Utils.hideCustomProgressDialog();
                activity.parseContent.parseCart(response);
                goToCartActivity();
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, t);
            }
        });

    }

    private void goToCartActivity() {
        Intent intent = new Intent(activity, CartActivity.class);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    private void openClearCartDialog() {
        final CustomDialogAlert dialogAlert = new CustomDialogAlert(activity, getResources()
                .getString(R
                        .string
                        .text_attention), getResources().getString(R.string
                .msg_other_store_item_in_cart),
                getResources().getString(R.string.text_cancel), getResources().getString(R.string
                .text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                clearCart();
                dismiss();
            }
        };
        dialogAlert.show();
    }

    /**
     * this method called webservice for clear user cart
     */
    protected void clearCart() {
        Utils.showCustomProgressDialog(activity, false);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject jsonObject = activity.getCommonParam();
        try {
            jsonObject.put(Const.Params.CART_ID, activity.currentBooking.getCartId());
            Call<IsSuccessResponse> responseCall = apiInterface.clearCart(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse> response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && activity.parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        activity.currentBooking.clearCart();
                        addItemInServerCart();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                activity);
                    }
                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(Const.Tag.PRODUCT_SPE_ACTIVITY, t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(CartActivity.class.getSimpleName(), e);
        }
    }
}
