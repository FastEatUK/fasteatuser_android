package com.edelivery;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import com.edelivery.adapter.WalletHistoryAdapter;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.WalletHistory;
import com.edelivery.models.responsemodels.WalletHistoryResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletHistoryActivity extends BaseAppCompatActivity {
    public static final String TAG = WalletHistoryActivity.class.getName();
    private RecyclerView rcvWalletData;
    private ArrayList<WalletHistory> walletHistory = new ArrayList<>();
    ;
    private WalletHistoryAdapter walletHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_history);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_wallet_history));
        findViewById();
        setViewListener();
        initRcvWalletHistory();
        getWalletHistory();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        rcvWalletData = (RecyclerView) findViewById(R.id.rcvWalletData);
    }

    @Override
    protected void setViewListener() {

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {

    }

    private void getWalletHistory() {

        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.TYPE, Const.Type.USER);
        } catch (JSONException e) {
            AppLog.handleException(TAG, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletHistoryResponse> call = apiInterface.getWalletHistory(ApiClient
                .makeJSONRequestBody(jsonObject));
        call.enqueue(new Callback<WalletHistoryResponse>() {
            @Override
            public void onResponse(Call<WalletHistoryResponse> call,
                                   Response<WalletHistoryResponse> response) {
                AppLog.Log("WALLET_HISTORY", ApiClient.JSONResponse(response.body()));
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    walletHistory.clear();
                    walletHistory.addAll(response.body().getWalletHistory());
                    Collections.sort(walletHistory, new Comparator<WalletHistory>() {
                        @Override
                        public int compare(WalletHistory lhs, WalletHistory rhs) {
                            return compareTwoDate(lhs.getCreatedAt(), rhs.getCreatedAt());
                        }
                    });
                    walletHistoryAdapter.notifyDataSetChanged();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(),
                            WalletHistoryActivity.this);
                }
            }

            @Override
            public void onFailure(Call<WalletHistoryResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(TAG, t);

            }
        });
    }

    private void initRcvWalletHistory() {
        walletHistory = new ArrayList<>();
        rcvWalletData.setLayoutManager(new LinearLayoutManager(this));
        walletHistoryAdapter = new WalletHistoryAdapter(this, walletHistory);
        rcvWalletData.setAdapter(walletHistoryAdapter);
        rcvWalletData.addOnItemTouchListener(new RecyclerTouchListener(this, rcvWalletData, new
                ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        goToWalletDetailActivity(walletHistory.get(position));
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    private void goToWalletDetailActivity(WalletHistory walletHistory) {
        Intent intent = new Intent(this, WalletDetailActivity.class);
        intent.putExtra(Const.BUNDLE, walletHistory);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private int compareTwoDate(String firstStrDate, String secondStrDate) {
        try {
            SimpleDateFormat webFormat = parseContent.webFormat;
            SimpleDateFormat dateFormat = parseContent.dateTimeFormat;
            String date2 = dateFormat.format(webFormat.parse(secondStrDate));
            String date1 = dateFormat.format(webFormat.parse(firstStrDate));
            return date2.compareTo(date1);
        } catch (ParseException e) {
            AppLog.handleException(WalletHistoryResponse.class.getName(), e);
        }
        return 0;
    }
}
