package com.edelivery;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.util.Pair;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.DisplayMetrics;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.edelivery.adapter.ProductFilterAdapter;
import com.edelivery.adapter.StoreProductAdapter;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.CartProducts;
import com.edelivery.models.datamodels.Product;
import com.edelivery.models.datamodels.ProductDetail;
import com.edelivery.models.datamodels.ProductItem;
import com.edelivery.models.datamodels.RemoveFavourite;
import com.edelivery.models.datamodels.Store;
import com.edelivery.models.responsemodels.SetFavouriteResponse;
import com.edelivery.models.responsemodels.StoreProductResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PinnedHeaderItemDecoration;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

public class StoreProductActivity extends BaseAppCompatActivity {

    public Store store;
    int animConnerUp, animCornerDown;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private RecyclerView rcvStoreProduct, rcvFilterList;
    private StoreProductAdapter storeProductAdapter;
    private ProductFilterAdapter productFilterAdapter;
    private ArrayList<Product> storeProductList = new ArrayList<>();;
    private CustomFontTextViewTitle tvSelectedStoreName;
    private CustomFontTextView tvSelectedStoreRatings,
            tvSelectedStoreApproxTime, tvSelectedStorePricing, tvRatingsCount;
    private ImageView ivStoreImage, ivClearText;
    private String storeId, filter;
    private CustomFontTextView tvStoreReOpenTime;
    private FrameLayout flStoreClosed;
    private Dialog storeInfoDialog;
    private CustomFontEditTextView etProductSearch;
    private CardView cvProductFilter;
    private ImageView btnFbFilter;
    private LinearLayout llProductFilter;
    private CustomFontButton btnApplyProductFilter;
    private ToggleButton ivFavourites;
    private CustomFontButton btnGotoCart;
    private CustomFontTextView tvTag;
    private PinnedHeaderItemDecoration pinnedHeaderItemDecoration;

    public static void setMargins(View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupWindowAnimations();
        setContentView(R.layout.activity_store_product);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        loadExtraData();
        initToolBar();
        tvTitleToolbar.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
        tvTitleToolbar.setTextColor(ResourcesCompat.getColor(getResources(),
                android.R.color.transparent, null));
        toolbar.setBackground(AppCompatResources.getDrawable(this, R.drawable
                .toolbar_transparent_2));
        setToolbarRightIcon3(R.drawable.ic_shopping_bag_white, this);
        // setToolbarRightIcon2(R.drawable.ic_thumb_up_white_24dp, this);
        ivToolbarBack.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable
                .ic_left_arrow_white));
        tvTitleToolbar.setTextColor(ResourcesCompat.getColor(getResources(),
                android.R.color.transparent, null));
        findViewById();
        setViewListener();
        storeProductList = new ArrayList<>();
        initAppBar();
        initCollapsingToolbar();
        loadAnimation();

        if (store != null)
        {
            getStoreProductList(store.getId());
            loadStoreProductData(store);
        } else {
            getStoreProductList(storeId);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        currentBooking.setApplication(true);
        setCartItem();
        setHeightOfFilter();
    }

    private void setHeightOfFilter() {
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        cvProductFilter.getLayoutParams().height = (metrics.heightPixels / 2) + (metrics
                .heightPixels / 6);
    }

    @Override
    protected boolean isValidate() {
        // do somethings
        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings
        pinnedHeaderItemDecoration = new PinnedHeaderItemDecoration();
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id
                .productCollapsingToolBar);
        rcvStoreProduct = (RecyclerView) findViewById(R.id.rcvStoreProduct);
        tvSelectedStorePricing = (CustomFontTextView) findViewById(R.id.tvStorePricing);
        tvSelectedStoreName = (CustomFontTextViewTitle) findViewById(R.id.tvStoreName);
        tvSelectedStoreRatings = (CustomFontTextView) findViewById(R.id.tvStoreRatings);
        tvSelectedStoreApproxTime = (CustomFontTextView) findViewById(R.id
                .tvStoreApproxTime);
        ivStoreImage = (ImageView) findViewById(R.id.ivStoreImage);
        flStoreClosed = (FrameLayout) findViewById(R.id.flStoreClosed);
        tvStoreReOpenTime = (CustomFontTextView) findViewById(R.id.tvStoreReOpenTime);
        etProductSearch = (CustomFontEditTextView) findViewById(R.id.etProductSearch);
        rcvFilterList = (RecyclerView) findViewById(R.id.rcvFilterList);
        cvProductFilter = (CardView) findViewById(R.id.cvProductFilter);
        cvProductFilter.setVisibility(View.GONE);
        llProductFilter = (LinearLayout) findViewById(R.id.llProductFilter);
        llProductFilter.setVisibility(View.GONE);
        btnFbFilter = (ImageView) findViewById(R.id.btnFbFilter);
        btnApplyProductFilter = (CustomFontButton) findViewById(R.id.btnApplyProductFilter);
        ivClearText = (ImageView) findViewById(R.id
                .ivClearDeliveryAddressTextMap);
        ivClearText.setVisibility(View.GONE);
        ivFavourites = (ToggleButton) findViewById(R.id.ivFavourites);
        btnGotoCart = (CustomFontButton) findViewById(R.id.btnGotoCart);
        if (TextUtils.isEmpty(preferenceHelper.getUserId())) {
            ivFavourites.setVisibility(View.GONE);
        } else {
            ivFavourites.setVisibility(View.VISIBLE);
        }
        tvRatingsCount = (CustomFontTextView) findViewById(R.id.tvRatingsCount);
        tvTag = (CustomFontTextView) findViewById(R.id.tvTag);

    }

    @Override
    protected void setViewListener() {
        btnFbFilter.setOnClickListener(this);
        llProductFilter.setOnClickListener(this);
        btnApplyProductFilter.setOnClickListener(this);
        ivClearText.setOnClickListener(this);
        ivFavourites.setOnClickListener(this);
        btnGotoCart.setOnClickListener(this);
        etProductSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    ivClearText.setVisibility(View.VISIBLE);
                } else {
                    ivClearText.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.ivToolbarRightIcon3:
                goToCartActivity();
                break;
            case R.id.ivToolbarRightIcon2:
                goToStoreReviewActivity();
                break;
            case R.id.btnFbFilter:
                slidFilterView();
                break;
            case R.id.llProductFilter:
                slidFilterView();
                break;
            case R.id.btnApplyProductFilter:
                try{
                    pinnedHeaderItemDecoration.disableCache();
                    storeProductAdapter.getFilter().filter(etProductSearch.getText().toString());
                    etProductSearch.getText().clear();
                    slidFilterView();
                }catch (Exception e)
                {

                }

                break;
            case R.id.ivClearDeliveryAddressTextMap:
                etProductSearch.getText().clear();
                break;
            case R.id.ivFavourites:
                if (store.isFavourite()) {
                    removeAsFavoriteStore(store.getId());
                } else {
                    setFavoriteStore(store.getId());
                }
                break;
            case R.id.btnGotoCart:
                goToCartActivity();
                break;
            default:
                // do with default
                break;
        }
    }

    private void initCollapsingToolbar() {
        collapsingToolbarLayout.setTitleEnabled(false);
        collapsingToolbarLayout.setContentScrimColor(ResourcesCompat.getColor(getResources(), R
                .color.color_app_theme, null));
        collapsingToolbarLayout.setStatusBarScrimColor(ResourcesCompat.getColor(getResources(), R
                .color.color_app_theme, null));
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
    }

    private void initAppBar() {

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        tvTitleToolbar.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                                .color_black, null));
                        setToolbarRightIcon3(R.drawable.ic_shopping_bag, StoreProductActivity.this);
                        //   setToolbarRightIcon2(R.drawable.ic_thumb_up_black_24dp,StoreProductActivity.this);
                        ivToolbarBack.setImageDrawable(AppCompatResources.getDrawable
                                (StoreProductActivity.this, R
                                        .drawable.ic_left_arrow));
                        toolbar.setBackground(null);
                        break;
                    case 2:
                        tvTitleToolbar.setTextColor(ResourcesCompat.getColor(getResources(),
                                android.R.color.transparent, null));
                        setToolbarRightIcon3(R.drawable.ic_shopping_bag_white, StoreProductActivity
                                .this);
                        //  setToolbarRightIcon2(R.drawable.ic_thumb_up_white_24dp, StoreProductActivity.this);
                        ivToolbarBack.setImageDrawable(AppCompatResources.getDrawable
                                (StoreProductActivity.this, R
                                        .drawable.ic_left_arrow_white));
                        tvTitleToolbar.setTextColor(ResourcesCompat.getColor(getResources(),
                                android.R.color.transparent, null));
                        toolbar.setBackground(AppCompatResources.getDrawable(StoreProductActivity
                                .this, R.drawable
                                .toolbar_transparent_2));

                        break;
                    default:
                        // do with default
                        break;
                }
            }
        };
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.productAppBarLayout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    handler.sendEmptyMessage(1);
                    isShow = true;
                } else if (isShow) {
                    handler.sendEmptyMessage(2);
                    isShow = false;
                }
            }
        });


    }

    private void initRevStoreProductList() {
        if (storeProductAdapter != null) {
            storeProductAdapter.notifyDataSetChanged();
            productFilterAdapter.notifyDataSetChanged();
        } else {

            storeProductAdapter = new StoreProductAdapter(this, storeProductList);
          /*  AlphaInAnimationAdapter animationAdapter = new AlphaInAnimationAdapter
                    (storeProductAdapter);*/
            rcvStoreProduct.setAdapter(storeProductAdapter);
            rcvStoreProduct.setLayoutManager(new LinearLayoutManager(this));

            rcvStoreProduct.addItemDecoration(pinnedHeaderItemDecoration);
            rcvFilterList.setLayoutManager(new LinearLayoutManager(this));
            rcvFilterList.addItemDecoration(new DividerItemDecoration(this,
                    LinearLayoutManager.VERTICAL));
            productFilterAdapter = new ProductFilterAdapter(this, storeProductList);
            rcvFilterList.setAdapter(productFilterAdapter);
            if (!TextUtils.isEmpty(filter)) {
                pinnedHeaderItemDecoration.disableCache();
                storeProductAdapter.getFilter().filter(filter);
                etProductSearch.setText(filter);
            }


        }
    }

    /**
     * this method call a webservice for get particular store product list according to store
     *
     * @param storeId store in string
     */
    private void getStoreProductList(String storeId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.STORE_ID, storeId);
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper
                    .getUserId());
            jsonObject.put(Const.Params.CART_UNIQUE_TOKEN, preferenceHelper.getAndroidId());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.STORES_PRODUCT_ACTIVITY, e);
        }
        Log.i("getStoreProductList",jsonObject.toString());

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<StoreProductResponse> responseCall = apiInterface.getStoreProductList(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<StoreProductResponse>() {
            @Override
            public void onResponse(Call<StoreProductResponse> call, Response<StoreProductResponse
                    > response) {
                Utils.hideCustomProgressDialog();
                AppLog.Log("getStoreProductList_responce", ApiClient.JSONResponse(response.body()));
                storeProductList.clear();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    if (response.body().getProducts() != null) {
                        storeProductList.addAll(response.body().getProducts());
                        if (store == null) {
                            store = response.body().getStore();
                            store.setCurrency(response.body().getCurrency());
                            store.setPriceRattingAndTag(Utils.getStringPriceAndTag(store
                                    .getFamousProductsTags
                                            (), store.getPriceRating(), store
                                    .getCurrency()));
                            loadStoreProductData(store);
                        }
                        initRevStoreProductList();

                    }
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), StoreProductActivity
                            .this);
                }
                if (storeProductList.isEmpty()) {
                    btnFbFilter.setVisibility(View.GONE);
                } else {
                    btnFbFilter.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onFailure(Call<StoreProductResponse> call, Throwable t) {
                AppLog.handleThrowable(Const.Tag.STORES_PRODUCT_ACTIVITY, t);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (llProductFilter.getVisibility() == View.VISIBLE) {
            slidFilterView();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }


    }

    private void loadStoreProductData(Store store) {
        if (store != null) {
            tvSelectedStoreName.setText(store.getName());
            tvSelectedStorePricing.setVisibility(View.GONE);
            tvSelectedStoreRatings.setVisibility(View.INVISIBLE);

            if (store.getNote() != null) {
                tvSelectedStoreRatings.setVisibility(View.VISIBLE);
                tvSelectedStoreRatings.setText(String.valueOf(store.getNote()));
            } else {
                tvSelectedStoreRatings.setVisibility(View.INVISIBLE);
            }
            //   tvSelectedStorePricing.setText(store.getPriceRattingAndTag());

            if (store.getDeliveryTimeMax() > store.getDeliveryTime()) {
                tvSelectedStoreApproxTime.setText(String.valueOf(store.getDeliveryTime()) +
                        " - " + String.valueOf(store.getDeliveryTimeMax()) + " " +
                        getResources().getString(R.string.unit_mins));
            } else {
                tvSelectedStoreApproxTime.setText(String.valueOf(store.getDeliveryTime()) +
                        " " + getResources().getString(R.string.unit_mins));
            }

            ivFavourites.setChecked(store.isFavourite());
            if (store.isStoreClosed()) {
                flStoreClosed.setVisibility(View.VISIBLE);
                tvStoreReOpenTime.setVisibility(View.VISIBLE);
                tvStoreReOpenTime.setText(store.getReOpenTime());
            } else {
                if (store.isBusy()) {
                    flStoreClosed.setVisibility(View.VISIBLE);
                    tvTag.setText(getResources().getText(R.string
                            .text_store_busy));
                    tvStoreReOpenTime.setVisibility(View.GONE);
                } else {
                    flStoreClosed.setVisibility(View.GONE);
                }
            }
            setTitleOnToolBar(store.getName());
            if (PreferenceHelper.getInstance(this).getIsLoadStoreImage()) {
                Glide.with(StoreProductActivity.this).load(preferenceHelper.getIMAGE_BASE_URL() + store.getImageUrl())
                        .placeholder(ResourcesCompat.getDrawable(
                                getResources(), R.drawable.placeholder, null))
                        .fallback(ResourcesCompat.getDrawable(
                                getResources(), R.drawable.placeholder, null))
                        .dontAnimate().into
                        (ivStoreImage);
            }
            supportPostponeEnterTransition();
            ivStoreImage.getViewTreeObserver().addOnPreDrawListener(
                    new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            ivStoreImage.getViewTreeObserver().removeOnPreDrawListener(this);
                            supportStartPostponedEnterTransition();
                            return true;
                        }
                    }
            );
            tvRatingsCount.setVisibility(View.GONE);
            // tvRatingsCount.setText("(" + String.valueOf(store.getRateCount()) + ")");
        }
    }

    public void goToProductSpecificationActivity(ProductDetail productDetail,
                                                 ProductItem
                                                         productItem, View view, boolean
                                                         isAnimated) {
        Intent intent = new Intent(this, ProductSpecificationActivity.class);
        intent.putExtra(Const.UPDATE_ITEM_INDEX, -1);
        intent.putExtra(Const.UPDATE_ITEM_INDEX_SECTION, -1);
        intent.putExtra(Const.PRODUCT_DETAIL, productDetail);
        intent.putExtra(Const.PRODUCT_ITEM, productItem);
        intent.putExtra(Const.SELECTED_STORE, store);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && isAnimated) {
            Pair<View, String> p1 = Pair.create(view.findViewById(R.id.ivProductImage),
                    getResources().getString(R.string
                            .transition_string_store_product));

            ActivityOptionsCompat options =
                    ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
            ActivityCompat.startActivity(this, intent, options.toBundle());
        } else {
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

    }

    private void setCartItem() {
        int cartCount = 0;
        for (CartProducts cartProducts : currentBooking
                .getCartProductWithSelectedSpecificationList()) {
            cartCount = cartCount + cartProducts.getItems().size();
        }
        if (cartCount > 0) {
            btnGotoCart.setVisibility(View.VISIBLE);

            setMargins(cvProductFilter, getResources().getDimensionPixelSize(R.dimen
                    .activity_horizontal_margin), 0, getResources().getDimensionPixelSize(R.dimen
                    .activity_horizontal_margin), getResources().getDimensionPixelSize(R.dimen
                    .filter_margin));
            setMargins(btnFbFilter, getResources().getDimensionPixelSize(R.dimen
                    .activity_horizontal_margin), 0, getResources().getDimensionPixelSize(R.dimen
                    .activity_horizontal_margin), getResources().getDimensionPixelSize(R.dimen
                    .filter_margin));
        } else {
            btnGotoCart.setVisibility(View.GONE);
            setMargins(cvProductFilter, getResources().getDimensionPixelSize(R.dimen
                    .activity_horizontal_margin), 0, getResources().getDimensionPixelSize(R.dimen
                    .activity_horizontal_margin), getResources().getDimensionPixelSize(R.dimen
                    .activity_horizontal_margin));
            setMargins(btnFbFilter, getResources().getDimensionPixelSize(R.dimen
                    .activity_horizontal_margin), 0, getResources().getDimensionPixelSize(R.dimen
                    .activity_horizontal_margin), getResources().getDimensionPixelSize(R.dimen
                    .activity_horizontal_margin));
        }
        setToolbarCartCount(cartCount);
    }

    private void loadExtraData() {
        if (getIntent() != null)
        {
            store = getIntent().getExtras().getParcelable(Const.SELECTED_STORE);
            storeId = getIntent().getExtras().getString(Const.STORE_DETAIL);
            filter = getIntent().getExtras().getString(Const.FILTER);
        }
    }

    private void setupWindowAnimations() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            Transition transition;
            transition = TransitionInflater.from(this).inflateTransition(R.transition
                    .slide_and_changebounds);
            getWindow().setSharedElementEnterTransition(transition);
        }
    }

    private void slidFilterView() {


        if (cvProductFilter.getVisibility() == View.GONE && btnFbFilter.getVisibility() == View
                .VISIBLE) {

            llProductFilter.setVisibility(View.VISIBLE);
            Animation fbAnim = AnimationUtils.loadAnimation(this, R.anim
                    .scale_down);
            btnFbFilter.startAnimation(fbAnim);

            btnFbFilter.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    btnFbFilter.setVisibility(View.GONE);
                    Animation logoMoveAnimation = AnimationUtils.loadAnimation
                            (StoreProductActivity.this, animConnerUp);
                    cvProductFilter.startAnimation(logoMoveAnimation);
                    cvProductFilter.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });


        } else {

            Animation logoMoveAnimation = AnimationUtils.loadAnimation(this, animCornerDown);
            cvProductFilter.startAnimation(logoMoveAnimation);
            cvProductFilter.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    cvProductFilter.setVisibility(View.GONE);
                    llProductFilter.setVisibility(View.GONE);
                    Animation fbAnim = AnimationUtils.loadAnimation(StoreProductActivity.this, R
                            .anim
                            .scale_up);
                    btnFbFilter.startAnimation(fbAnim);
                    btnFbFilter.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });


        }

    }

    private void setFavoriteStore(String storeId) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.STORE_ID, storeId);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SetFavouriteResponse> call = apiInterface.setFavouriteStore(ApiClient
                .makeJSONRequestBody(jsonObject));
        call.enqueue(new Callback<SetFavouriteResponse>() {
            @Override
            public void onResponse(Call<SetFavouriteResponse> call, Response<SetFavouriteResponse> response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        currentBooking.getFavourite().clear();
                        currentBooking.setFavourite(response.body().getFavouriteStores());
                        store.setFavourite(true);
                        ivFavourites.setChecked(true);
                    }
                }
            }
            @Override
            public void onFailure(Call<SetFavouriteResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
            }
        });
    }

    private void removeAsFavoriteStore(String storeId) {
        Utils.showCustomProgressDialog(this, false);
        ArrayList<String> storeList = new ArrayList<>();
        storeList.add(storeId);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SetFavouriteResponse> call = apiInterface.removeAsFavouriteStore(ApiClient
                .makeGSONRequestBody(new RemoveFavourite(preferenceHelper.getSessionToken(),
                        preferenceHelper.getUserId(), storeList)));
        call.enqueue(new Callback<SetFavouriteResponse>() {
            @Override
            public void onResponse(Call<SetFavouriteResponse> call, Response<SetFavouriteResponse
                    > response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        currentBooking.getFavourite().clear();
                        currentBooking.setFavourite(response.body().getFavouriteStores());
                        store.setFavourite(false);
                        ivFavourites.setChecked(false);
                    }
                }

            }

            @Override
            public void onFailure(Call<SetFavouriteResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
            }
        });
    }

    private void loadAnimation() {
        if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
            animConnerUp = R.anim.scale_left_corner_up;
            animCornerDown = R.anim.scale_left_corner_down;
        } else {
            animConnerUp = R.anim.scale_right_corner_up;
            animCornerDown = R.anim.scale_right_corner_down;
        }
    }

    public void goToStoreReviewActivity()
    {
        Intent intent = new Intent(this, ReviewActivity.class);
        intent.putExtra(Const.STORE_DETAIL, store);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
