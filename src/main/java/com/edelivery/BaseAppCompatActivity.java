package com.edelivery;

import android.accounts.AuthenticatorException;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.Massdialgue;
import com.edelivery.models.datamodels.ConfirmationData;
import com.edelivery.models.datamodels.Invoice;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.FontsOverride;
import com.edelivery.utils.LanguageHelper;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;
import com.facebook.appevents.AppEventsLogger;
import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class BaseAppCompatActivity extends AppCompatActivity implements View
        .OnClickListener {

    public Toolbar toolbar;
    public PreferenceHelper preferenceHelper;
    public ParseContent parseContent;
    public ImageView ivToolbarBack, ivToolbarRightIcon3, ivToolbarRightIcon1, ivToolbarRightIcon2;
    public CustomFontTextView tvCartCount, tvToolbarRightBtn,tvSkip;
    public AppCompatImageView ivToolbarPlus;
    public FrameLayout flCart;
    public CustomFontTextView tvTitleToolbar;
    public CurrentBooking currentBooking;
    private ActionBar actionBar;
    private CustomDialogAlert customDialogEnableInternet;
    private CustomDialogAlert customDialogAdmin, customDialogEnable;
    private AppReceiver appReceiver = new AppReceiver();
    private NetworkListener networkListener;
    private OrderStatusListener orderStatusListener;
    AppEventsLogger logger;
    public String intedntdata;
    ConfirmationData confirmationData;
    Massdialgue massdialgue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/ClanPro-News.otf");
        preferenceHelper = PreferenceHelper.getInstance(this);
        parseContent = ParseContent.getInstance();
        parseContent.setContext(this);


        FreshchatConfig freshchatConfig = new FreshchatConfig(getResources().getString(R.string.FRESHCHAT_APP_ID), getResources().getString(R.string.FRESHCHAT_APP_KEY));
        Freshchat.getInstance(this).init(freshchatConfig);


        currentBooking = CurrentBooking.getInstance();
        setNetworkListener(new NetworkListener() {
            @Override
            public void onNetworkChange(boolean isEnable) {
                if (isEnable) {
                    closedEnableDialogInternet();
                } else {
                    openInternetDialog(BaseAppCompatActivity.this);
                    Utils.hideCustomProgressDialog();
                }
            }
        });
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.Action.NETWORK_ACTION);
        intentFilter.addAction(Const.Action.ACTION_ADMIN_APPROVED);
        intentFilter.addAction(Const.Action.ACTION_ADMIN_DECLINE);

        intentFilter.addAction(Const.Action.ACTION_MASS_NOTIFICATION);
        intentFilter.addAction(Const.Action.SEND_NOTIFICATION);
        intentFilter.addAction(Const.Action.ACTION_ORDER_STATUS);
        intentFilter.addAction(Const.Action.ACTION_DELIVERYMEN_ACCEPTED);
        registerReceiver(appReceiver, intentFilter);


    }


    @Override
    protected void onDestroy() {
        unregisterReceiver(appReceiver);
        super.onDestroy();
    }

    public void CheckverifiedUser() {
        Log.i("CheckverifiedUser", "call");
        if (isCurrentLogin()) {
            preferenceHelper.putREFERRAL_DEEPLINK_REGISTER("");
            if (!preferenceHelper.getIsPhoneNumberVerified()) {

                confirmationData = new ConfirmationData();
                confirmationData.setMobileNo(preferenceHelper.getPhoneNumber());
                confirmationData.setUser_id(preferenceHelper.getUserId());
                confirmationData.setContrycode(preferenceHelper.getPhoneCountyCodeCode());
                GoforConfirmation(confirmationData);
            }
        }
    }

    public Activity getActivity() {
        return this;
    }

    protected void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.appToolbar);
        tvTitleToolbar = (CustomFontTextView) toolbar.findViewById(R.id.tvToolbarTitle);
        ivToolbarRightIcon1 = (ImageView) toolbar.findViewById(R.id.ivToolbarRightIcon1);
        ivToolbarRightIcon2 = (ImageView) toolbar.findViewById(R.id.ivToolbarRightIcon2);
        ivToolbarRightIcon3 = (ImageView) toolbar.findViewById(R.id.ivToolbarRightIcon3);
        tvToolbarRightBtn = (CustomFontTextView) toolbar.findViewById(R.id.tvToolbarRightBtn);
        tvSkip =  (CustomFontTextView)toolbar.findViewById(R.id.tvSkip);

        flCart = (FrameLayout) toolbar.findViewById(R.id.flCart);

        ivToolbarBack = (ImageView) findViewById(R.id.ivToolbarBack);
        tvCartCount = (CustomFontTextView) findViewById(R.id.tvCartCount);
        ivToolbarPlus = (AppCompatImageView) findViewById(R.id.ivToolbarPlus);
        tvCartCount.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        ivToolbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackNavigation();
            }
        });
    }

    public void initFreshchat() {

        FreshchatConfig freshConfig = new FreshchatConfig(getResources().getString(R.string.FRESHCHAT_APP_ID), getResources().getString(R.string.FRESHCHAT_APP_KEY));

        freshConfig.setCameraCaptureEnabled(true);
        freshConfig.setGallerySelectionEnabled(true);

        Freshchat.getInstance(this).init(freshConfig);
    }


    public void setTitleOnToolBar(String title) {
        if (tvTitleToolbar != null) {
            tvTitleToolbar.setText(title);
        }
    }

    public void callErrorFunction(Throwable t) {
        try {
            if (t instanceof SocketTimeoutException) {
                Utils.showToast(getResources().getString(R.string.connection_timeout), getActivity());
            } else if (t instanceof NetworkErrorException) {
                Utils.showToast(getResources().getString(R.string.network_error), getActivity());
            } else if (t instanceof AuthenticatorException) {
                Utils.showToast(getResources().getString(R.string.authentiation_error), getActivity());
            } else {
                Utils.showToast(getResources().getString(R.string.something_went_wrong), getActivity());
            }
            String error1  =  t.getMessage().replace("For input string:","");
            Utils.showToast(error1, getActivity());
        } catch (Exception e) {
            Log.e("error_failure", e.toString());
        }

    }

    protected void IsToolbarNavigationVisible(boolean isVisible) {
        if (ivToolbarBack != null) {
            if (isVisible) {
                ivToolbarBack.setVisibility(View.VISIBLE);
            } else {
                ivToolbarBack.setVisibility(View.GONE);
            }
        }
    }

    public void setToolbarCartCount(int count) {

        tvCartCount.setText(String.valueOf(count));
        tvCartCount.setVisibility(View.VISIBLE);
    }

    public void setToolbarRightIcon3(int drawableResourcesId, View.OnClickListener
            onClickListener) {
        if (ivToolbarRightIcon3 != null) {
            ivToolbarRightIcon3.setImageDrawable(AppCompatResources.getDrawable(this,
                    drawableResourcesId));
            ivToolbarRightIcon3.setOnClickListener(onClickListener);
        }
    }

    protected abstract boolean isValidate();

    public void setToolbarRightIcon1(int drawableResourcesId, View.OnClickListener
            onClickListener) {
        ivToolbarRightIcon1.setImageDrawable(AppCompatResources.getDrawable(this,
                drawableResourcesId));
        ivToolbarRightIcon1.setOnClickListener(onClickListener);

    }

    public void setToolbarRightIcon2(int drawableResourcesId, View.OnClickListener
            onClickListener) {
        ivToolbarRightIcon2.setImageDrawable(AppCompatResources.getDrawable(this,
                drawableResourcesId));
        ivToolbarRightIcon2.setOnClickListener(onClickListener);

    }


    /**
     * this method will help you to check all data filed is valid or not
     *
     * @return true if is valid otherwise false
     */


    /**
     * method used to manage all view address
     */
    protected abstract void findViewById();

    /**
     * method used to manage all interface or listener
     */
    protected abstract void setViewListener();

    /**
     * method used to manage toolbar beck navigation button
     */
    protected abstract void onBackNavigation();


    public void goToLoginActivityForResult(AppCompatActivity activity) {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        activity.startActivityForResult(loginIntent, Const.LOGIN_REQUEST);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goToLoginActivity() {
        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        startActivity(loginIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goToHomeActivity() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        homeIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(homeIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goToMandatoryDeliveryLocationActivity() {
        Intent intent = new Intent(this, DeliveryLocationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goToWalletActivity() {

        Intent homeIntent = new Intent(this, WalletReferalActivity.class);
        startActivity(homeIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goToPaymentActivity(boolean isPayNowInvisible) {
   /*     Intent homeIntent = new Intent(this, PaymentActivity.class);
        homeIntent.putExtra(Const.Tag.PAYMENT_ACTIVITY, isPayNowInvisible);
        homeIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(homeIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);*/

        Intent homeIntent = new Intent(this, StripePaymentActivity.class);
        homeIntent.putExtra(Const.Tag.PAYMENT_ACTIVITY,isPayNowInvisible);

        homeIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(homeIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    public void gotoStripepaymentActivity(boolean isPayNowInvisible, ArrayList<Invoice> invoices) {
        Intent homeIntent = new Intent(this, StripePaymentActivity.class);
        homeIntent.putExtra(Const.Tag.PAYMENT_ACTIVITY, isPayNowInvisible);
        homeIntent.putExtra(Const.Tag.PAYMENT_ACTIVITY_Invoice, invoices);
        homeIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(homeIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goToDocumentActivity(boolean isApplicationStart) {
        Intent intent = new Intent(this, DocumentActivity.class);
        intent.putExtra(Const.Tag.DOCUMENT_ACTIVITY, isApplicationStart);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goToDocumentActivityForResult(AppCompatActivity activity, boolean
            isApplicationStart) {
        Intent intent = new Intent(this, DocumentActivity.class);
        intent.putExtra(Const.Tag.DOCUMENT_ACTIVITY, isApplicationStart);
        activity.startActivityForResult(intent, Const.DOCUMENT_REQUEST);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void goToFavouriteAddressActivity(boolean isApplicationStart,boolean isDelete) {
        Intent intent = new Intent(this, FavouriteAddressActivity.class);
        intent.putExtra(Const.Tag.HOME_FRAGMENT, isApplicationStart);
        intent.putExtra(Const.Tag.HIDE_DELETE, isDelete);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    protected void openInternetDialog(final Activity activity) {
        if (!this.isFinishing()) {
            if (customDialogEnableInternet != null && customDialogEnableInternet.isShowing()) {
                return;
            }
            customDialogEnableInternet = new CustomDialogAlert(this, getString(R.string
                    .text_internet), getString(R.string
                    .msg_internet_enable), getString(R.string.text_cancel), getString(R.string
                    .text_ok)) {

                @Override
                public void onClickLeftButton() {
                    closedEnableDialogInternet();
                    activity.finishAffinity();
                }

                @Override
                public void onClickRightButton() {
                    activity.startActivityForResult(new Intent(Settings
                            .ACTION_SETTINGS), Const.ACTION_SETTINGS);
                }

            };
            customDialogEnableInternet.show();
        }

    }


    protected void closedEnableDialogInternet() {
        if (customDialogEnableInternet != null && customDialogEnableInternet.isShowing()) {
            customDialogEnableInternet.dismiss();
            customDialogEnableInternet = null;

        }
    }


    void gotoordertracking(String string) {
        //  Log.i("ACTION_DELIVERYMEN_receiving",intent.getExtras().getString(Const.Params.PUSH_DATA1));
        Intent intentorder = new Intent(this, OrderTrackActivity.class);
        intentorder.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intentorder.putExtra(Const.Params.PUSH_DATA1, string);
        startActivity(intentorder);
    }

    protected void goToCartActivity() {
        CheckverifiedUser();
        toolbar.setVisibility(View.VISIBLE);
        Intent intent = new Intent(this, CartActivity.class);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);
        toolbar.setVisibility(View.VISIBLE);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    protected void openAdminApprovedDialog() {
        if (!this.isFinishing()) {
            if (customDialogAdmin != null && customDialogAdmin.isShowing()) {
                return;
            }
            customDialogAdmin = new CustomDialogAlert(this, getResources().getString(R.string
                    .text_alert), getResources().getString(R.string.msg_not_approved_by_admin),
                    getResources()
                            .getString(R.string.text_log_out), getResources().getString(R.string
                    .text_email)) {
                @Override
                public void onClickLeftButton() {
                    dismiss();
                    goToLoginActivity();

                }

                @Override
                public void onClickRightButton() {
                    contactUsWithAdmin();
                }
            };
            customDialogAdmin.show();
        }
    }


    protected void closedAdminApprovedDialog() {
        if (customDialogAdmin != null && customDialogAdmin.isShowing()) {
            customDialogAdmin.dismiss();
        }
    }

    public void setOrderStatusListener(OrderStatusListener orderStatusListener) {
        this.orderStatusListener = orderStatusListener;
    }

    public void setNetworkListener(NetworkListener networkListener) {
        this.networkListener = networkListener;
    }


    public String getAppVersion() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            AppLog.handleException(BaseAppCompatActivity.class.getName(), e);
        }
        return null;
    }

    public void openPermissionNotifyDialog(Activity activity, final int code) {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogAlert(this, getResources().getString(R
                .string
                .text_attention), getResources()
                .getString(R.string
                        .msg_permission_notification), getResources()
                .getString(R
                        .string.text_exit), getResources().getString(R
                .string
                .text_settings)) {
            @Override
            public void onClickLeftButton() {
                closedPermissionDialog();
                finishAffinity();
            }

            @Override
            public void onClickRightButton() {
                closedPermissionDialog();
                startActivityForResult(getIntentForPermission(), code);
            }

        };
        customDialogEnable.show();

    }

    private void closedPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            customDialogEnable.dismiss();
            customDialogEnable = null;

        }
    }

    private Intent getIntentForPermission() {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter" +
                ".autostart.AutoStartManagementActivity"));
        return intent;
    }

    public int checkWitchOtpValidationON() {
        if (checkEmailVerification() && checkPhoneNumberVerification()) {

            return Const.SMS_AND_EMAIL_VERIFICATION_ON;

        } else if (checkPhoneNumberVerification()) {
            return Const.SMS_VERIFICATION_ON;
        } else if (checkEmailVerification()) {
            return Const.EMAIL_VERIFICATION_ON;
        }
        return 0;

    }

    private boolean checkEmailVerification() {
        return preferenceHelper.getIsMailVerification() && !preferenceHelper.getIsEmailVerified();
    }

    private boolean checkPhoneNumberVerification() {
        return preferenceHelper.getIsSmsVerification() && !preferenceHelper
                .getIsPhoneNumberVerified();

    }

    public void contactUsWithAdmin() {
        Uri gmmIntentUri = Uri.parse("mailto:" + preferenceHelper
                .getAdminContactEmail()/*"customers@fasteat.biz" */ +
                "?subject=" + "Request to Admin " +
                "&body=" + "Hello sir");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.gm");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            mapIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(mapIntent);
        } else {
            Utils.showToast(getResources().getString(R.string
                    .msg_google_mail_app_not_installed), this);
        }
    }

    /**
     * this method will help to logout in our app it will called logout service from web
     */
    public void logOut() {

        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.USER_FRAGMENT, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.logOut(ApiClient.makeJSONRequestBody
                (jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (response != null && response.body() != null && response.body().isSuccess()) {
                        preferenceHelper.logout();
                        preferenceHelper.putAndroidId(Utils.generateRandomString());
                        ParseContent.getInstance().latLng = null;
                        ParseContent.getInstance().address = "";
                        goToLoginActivity();

                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                BaseAppCompatActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(BaseAppCompatActivity.class.getName(), t);
            }
        });
    }

    public boolean isCurrentLogin() {
        Log.e("tagLogin ", "IS LOGIN " + String.valueOf(preferenceHelper != null && preferenceHelper.getUserId() != null && !TextUtils.isEmpty(preferenceHelper.getUserId())));
        return preferenceHelper != null && preferenceHelper.getUserId() != null && !TextUtils.isEmpty(preferenceHelper.getUserId());
    }

    public JSONObject getCommonParam() {
        JSONObject jsonObject = new JSONObject();
        try {
            if (isCurrentLogin()) {
                jsonObject.put(Const.Params.USER_ID, preferenceHelper
                        .getUserId());
                jsonObject.put(Const.Params.CART_UNIQUE_TOKEN, "");
            } else {
                jsonObject.put(Const.Params.CART_UNIQUE_TOKEN, preferenceHelper
                        .getAndroidId());
                jsonObject.put(Const.Params.USER_ID, "");
            }
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
        } catch (JSONException e) {
            AppLog.handleThrowable(BaseAppCompatActivity.class.getName(), e);
        }

        Log.i("getCommonParam", jsonObject.toString());
        return jsonObject;
    }


    public void restartApp() {
        startActivity(new Intent(this, SplashScreenActivity.class));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LanguageHelper.wrapper(newBase, PreferenceHelper.getInstance
                (newBase).getLanguageCode()));
    }


    public interface NetworkListener {
        void onNetworkChange(boolean isEnable);

    }

    public interface OrderStatusListener {
        void onOrderStatus();

    }

    public class AppReceiver extends BroadcastReceiver {


        @Override
        public void onReceive(final Context context, final Intent intent) {

            if (intent != null) {
                switch (intent.getAction()) {
                    case Const.Action.NETWORK_ACTION:
                        if (networkListener != null) {
                            networkListener.onNetworkChange(Utils.isInternetConnected(context));
                        }
                        break;
                    case Const.Action.ACTION_ADMIN_APPROVED:
                        closedAdminApprovedDialog();
                        goToHomeActivity();
                        break;
                    case Const.Action.ACTION_ADMIN_DECLINE:
                        openAdminApprovedDialog();
                        break;
                    case Const.Action.ACTION_ORDER_STATUS:
                        if (orderStatusListener != null) {
                            orderStatusListener.onOrderStatus();
                        }

                        break;

                    case Const.Action.ACTION_DELIVERYMEN_ACCEPTED:

                        //   Log.i("ACTION_DELIVERYMEN_ACCEPTED","ok");
                        if (!intent.getExtras().getString(Const.Params.PUSH_DATA1).isEmpty()) {

                            Log.i("ACTION_DELIVERYMEN_receiving", intent.getExtras().getString(Const.Params.PUSH_DATA1));

                            gotoordertracking(intent.getExtras().getString(Const.Params.PUSH_DATA1));
                        }

                        break;

                    case Const.Action.ACTION_MASS_NOTIFICATION:

                        if (!intent.getExtras().getString(Const.Params.message).isEmpty()) {


                            if (massdialgue != null && massdialgue.isShowing()) {
                                massdialgue.dismiss();
                            }
                            Log.i("ACTION_MASS", intent.getExtras().getString(Const.Params.message));
                            massdialgue = new Massdialgue(context, intent.getExtras().getString(Const.Params.message)) {

                                @Override

                                public void onClick(View v) {
                                    super.onClick(v);

                                    switch (v.getId()) {
                                        case R.id.iv_close:
                                            preferenceHelper.putpopupmassage("");
                                            dismiss();
                                            break;
                                    }
                                }
                            };

                            massdialgue.show();

                            preferenceHelper.putisshowpopup(false);

                        }

                        break;


                    case Const.Action.SEND_NOTIFICATION:
                        if (!intent.getExtras().getString(Const.Params.message).isEmpty()) {


                            if (massdialgue != null && massdialgue.isShowing()) {
                                massdialgue.dismiss();
                            }
                            Log.i("ACTION_MASS", intent.getExtras().getString(Const.Params.message));
                            massdialgue = new Massdialgue(context, intent.getExtras().getString(Const.Params.message)) {

                                @Override

                                public void onClick(View v) {
                                    super.onClick(v);

                                    switch (v.getId()) {
                                        case R.id.iv_close:
                                            preferenceHelper.putpopupmassage("");
                                            dismiss();
                                            break;
                                    }
                                }
                            };

                            massdialgue.show();

                            preferenceHelper.putisshowpopup(false);

                        }
                        break;
                }
            }


        }


    }

    public void GoforConfirmation(ConfirmationData confirmationData) {
        Intent intent = new Intent(this, ConfirmOnetimePassword.class);
        // intent.putExtra(Const.Params.Otp_data,otp);
        intent.putExtra(Const.Params.Otp_data, confirmationData);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
