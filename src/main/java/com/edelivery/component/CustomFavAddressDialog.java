package com.edelivery.component;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.edelivery.R;
import com.edelivery.adapter.FavAddressAdapter;
import com.edelivery.interfaces.ClickListener;
import com.edelivery.interfaces.RecyclerTouchListener;
import com.edelivery.models.datamodels.Addresses;

import java.util.ArrayList;

/**
 * Created by elluminati on 04-08-2016.
 */
public abstract class CustomFavAddressDialog extends Dialog {

    public static RecyclerView rcvCountryCode;
    private CustomFontTextView tvCountryDialogTitle;
    private FavAddressAdapter FavAddressAdapter;
    private Context context;
    private ImageView ivCloseFavAddress;
    private CustomFontButton btnAddFavAddress;

    public CustomFavAddressDialog(Context context, ArrayList<Addresses> addressList,boolean isAddAddress) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_fav_address);
        rcvCountryCode = (RecyclerView) findViewById(R.id.rcvCountryCode);
        ivCloseFavAddress = (ImageView) findViewById(R.id.ivCloseFavAddress);
        btnAddFavAddress = (CustomFontButton) findViewById(R.id.btnAddFavAddress);
        tvCountryDialogTitle = (CustomFontTextView) findViewById(R.id.tvCountryDialogTitle);
        tvCountryDialogTitle.setText(context.getResources().getString(R.string
                .text_favourite_address));
        if(isAddAddress){
            btnAddFavAddress.setVisibility(View.VISIBLE);
        }else{
            btnAddFavAddress.setVisibility(View.GONE);
        }
        rcvCountryCode.setLayoutManager(new LinearLayoutManager(context));
        FavAddressAdapter = new FavAddressAdapter(addressList, false,false) {
            @Override
            public void onDelete(int position) {

            }

            @Override
            public void onEdit(int position) {

            }
        };
        rcvCountryCode.setAdapter(FavAddressAdapter);
        rcvCountryCode.addItemDecoration(new DividerItemDecoration(context,
                LinearLayoutManager.VERTICAL));
        this.context = context;
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rcvCountryCode.addOnItemTouchListener(new RecyclerTouchListener(context, rcvCountryCode,
                new ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        onSelect(position);

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

        ivCloseFavAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClose();
            }
        });

        btnAddFavAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddAddress();
            }
        });
    }

    public abstract void onSelect(int position);
    public abstract void onClose();
    public abstract void onAddAddress();

}
