package com.edelivery.component;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.edelivery.R;

/**
 * Created by elluminati on 08-Feb-17.
 */

public abstract class CustomDialogAlert extends Dialog implements View.OnClickListener {
    private CustomFontTextView tvDialogEdiTextMessage, tvDialogEditTextTitle, btnDialogEditTextLeft;
    public CustomFontTextView btnDialogEditTextRight;

    public CustomDialogAlert(Context context, String title, String message,
                             String titleLeftButton, String titleRightButton) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom_alert);
        tvDialogEdiTextMessage = (CustomFontTextView) findViewById(R.id.tvDialogAlertMessage);
        tvDialogEditTextTitle = (CustomFontTextView) findViewById(R.id.tvDialogAlertTitle);
        btnDialogEditTextLeft = (CustomFontTextView) findViewById(R.id.btnDialogAlertLeft);
        btnDialogEditTextRight = (CustomFontTextView) findViewById(R.id.btnDialogAlertRight);


        btnDialogEditTextLeft.setOnClickListener(this);
        btnDialogEditTextRight.setOnClickListener(this);

        if (TextUtils.isEmpty(title))
        {

        tvDialogEditTextTitle.setVisibility(View.GONE);
        }
        else
        {
            tvDialogEditTextTitle.setText(title);
        }

        tvDialogEdiTextMessage.setText(message);
        btnDialogEditTextLeft.setText(titleLeftButton);
        btnDialogEditTextRight.setText(titleRightButton);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        setCancelable(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDialogAlertLeft:
                onClickLeftButton();
                break;
            case R.id.btnDialogAlertRight:
                onClickRightButton();
                break;
            default:
                // do with default
                break;
        }

    }

    public abstract void onClickLeftButton();

    public abstract void onClickRightButton();


}
