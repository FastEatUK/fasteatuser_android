package com.edelivery.component;

import android.app.Dialog;
import android.content.Context;
import com.google.android.material.textfield.TextInputLayout;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.edelivery.R;

/**
 * Created by elluminati on 08-Feb-17.
 */

public abstract class CustomDialogVerification extends Dialog implements View.OnClickListener,
        TextView
        .OnEditorActionListener {
    private CustomFontEditTextView etDialogEditTextOne, etDialogEditTextTwo;
    private CustomFontTextView tvDialogEdiTextMessage, tvDialogEditTextTitle, btnDialogEditTextLeft,
            btnDialogEditTextRight;
    private TextInputLayout dialogItlOne, dialogItlTwo;

    public CustomDialogVerification(Context context, String titleDialog, String messageDialog,
                                    String titleLeftButton, String titleRightButton, String
                                            editTextOneHint, String editTextTwoHint, boolean
                                            isEdiTextOneIsVisible, int editTextOneInputType, int
                                            editTextTwoInputType) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom_verification);
        tvDialogEdiTextMessage = (CustomFontTextView) findViewById(R.id.tvDialogAlertMessage);
        tvDialogEditTextTitle = (CustomFontTextView) findViewById(R.id.tvDialogAlertTitle);
        btnDialogEditTextLeft = (CustomFontTextView) findViewById(R.id.btnDialogAlertLeft);
        btnDialogEditTextRight = (CustomFontTextView) findViewById(R.id.btnDialogAlertRight);
        etDialogEditTextOne = (CustomFontEditTextView) findViewById(R.id.etDialogEditTextOne);
        etDialogEditTextTwo = (CustomFontEditTextView) findViewById(R.id.etDialogEditTextTwo);
        dialogItlOne = (TextInputLayout) findViewById(R.id.dialogItlOne);
        dialogItlTwo = (TextInputLayout) findViewById(R.id.dialogItlTwo);

        btnDialogEditTextLeft.setOnClickListener(this);
        btnDialogEditTextRight.setOnClickListener(this);

        tvDialogEditTextTitle.setText(titleDialog);
        tvDialogEdiTextMessage.setText(messageDialog);
        btnDialogEditTextLeft.setText(titleLeftButton);
        btnDialogEditTextRight.setText(titleRightButton);
        etDialogEditTextTwo.setInputType(editTextTwoInputType);
        etDialogEditTextOne.setInputType(editTextOneInputType);
        etDialogEditTextTwo.setOnEditorActionListener(this);
        dialogItlOne.setHint(editTextOneHint);
        dialogItlTwo.setHint(editTextTwoHint);

        if (isEdiTextOneIsVisible) {
            etDialogEditTextOne.setVisibility(View.VISIBLE);
            dialogItlOne.setVisibility(View.VISIBLE);

        }

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        setCancelable(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDialogAlertLeft:
                onClickLeftButton();
                break;
            case R.id.btnDialogAlertRight:
                onClickRightButton(etDialogEditTextOne, etDialogEditTextTwo);
                break;
            default:
                // do with default
                break;
        }

    }

    public abstract void onClickLeftButton();

    public abstract void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                            CustomFontEditTextView etDialogEditTextTwo);


    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        switch (textView.getId()) {
            case R.id.etDialogEditTextTwo:
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    onClickRightButton(etDialogEditTextOne, etDialogEditTextTwo);
                    return true;
                }
                break;
            default:
                //Do som thing
                break;
        }

        return false;
    }
}
