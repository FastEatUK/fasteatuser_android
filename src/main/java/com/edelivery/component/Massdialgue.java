package com.edelivery.component;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.edelivery.R;
import com.edelivery.utils.PreferenceHelper;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
public abstract class Massdialgue  extends Dialog implements View.OnClickListener {

CustomFontTextView massage;

ImageView ivclose;


    public Massdialgue(@NonNull Context context,String title) {
        super(context ,R.style.mythemeDialog);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialogue_mass_notification);


        massage=findViewById(R.id.custome_mssage);
        ivclose=findViewById(R.id.iv_close);
        ivclose.setOnClickListener(this);
        if (!TextUtils.isEmpty(title))
        {
            massage.setText(title);
        }
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        setCancelable(false);


    }

    @Override
    public void onClick(View v) {

    }
}
