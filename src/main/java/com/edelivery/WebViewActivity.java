package com.edelivery;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;

public class WebViewActivity extends BaseAppCompatActivity {
    WebView webview;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        preferenceHelper = PreferenceHelper.getInstance(getApplicationContext());

        Utils.showCustomProgressDialog(WebViewActivity.this, false);
        initToolBar();
        findViewById();
        setViewListener();
        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getStringExtra("web").equals("terms")) {
                setTitleOnToolBar(getResources().getString(R.string.text_t_and_c));
                url = preferenceHelper.getTermsANdConditions();
            } else if (getIntent().getStringExtra("web").equals("chat")) {
                setTitleOnToolBar(getResources().getString(R.string.fasteat));
                url = "https://chats.landbot.io/v3/H-856786-ZR4MIJ2CLPRNYGGB/index.html";
            } else {
                setTitleOnToolBar(getResources().getString(R.string.text_policy));
                url = preferenceHelper.getPolicy();
            }
        }
        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        webview.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);

        webview.setWebViewClient(new AppWebViewClients());
        webview.loadUrl(url);

        /*webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            }

            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {

                    //do your task

                }
            }

        });*/


        webview.loadUrl(url);
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        webview = findViewById(R.id.webview);
    }

    @Override
    protected void setViewListener() {

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {

    }

    public class AppWebViewClients extends WebViewClient {


        public AppWebViewClients() {

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            Utils.hideCustomProgressDialog();
        }
    }
}