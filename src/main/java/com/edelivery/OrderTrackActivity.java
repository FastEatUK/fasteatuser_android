package com.edelivery;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.*;

import androidx.annotation.IdRes;
import androidx.core.content.res.ResourcesCompat;

import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.datamodels.Status;
import com.edelivery.models.responsemodels.ActiveOrderResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.PushDataResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.utils.Const.Params.ORDER_ID;

public class OrderTrackActivity extends BaseAppCompatActivity implements BaseAppCompatActivity
        .OrderStatusListener {


    public Order order;
    private CustomFontTextView tvOrderNumber, tvEstTime, tvOrderAcceptedDate, tvOrderAcceptedTime,
            tvOrderReadyDate, tvOrderReadyTime, tvOrderOnTheWayDate, tvOrderOnTheWayTime,
            tvOrderReceiveDate, tvOrderReceiveTime, tvCancelOrder;
    private ImageView ivOrderAccepted, ivOrderPrepared, ivOrderOnWay, ivOrderOnDoorstep;
    private LinearLayout llOrderPrepared, llOrderOnWay, llOrderOnDoorstep, llOrderAccepted, llOrderTrack;
    private String cancelReason;
    private ActiveOrderResponse activeOrderResponse;
    private Dialog orderCancelDialog;
    private CustomDialogAlert confirmDialog, cancelDialog;

    String strEstimateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_track);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        findViewById();
        setViewListener();
        getExtraData();


    }

    @Override
    protected void onStop() {
        super.onStop();
        setOrderStatusListener(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setOrderStatusListener(this);
        if (!TextUtils.isEmpty(order.getId())) {
            getOrderStatus(order.getId());
        }


    }

    @Override
    protected boolean isValidate() {
        // do somethings
        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings
        llOrderTrack = (LinearLayout) findViewById(R.id.llOrderTrack);
        tvOrderNumber = (CustomFontTextView) findViewById(R.id.tvOrderNumber);
        ivOrderAccepted = (ImageView) findViewById(R.id.ivOrderAccepted);
        ivOrderPrepared = (ImageView) findViewById(R.id.ivOrderPrepared);
        ivOrderOnWay = (ImageView) findViewById(R.id.ivOrderOnWay);
        ivOrderOnDoorstep = (ImageView) findViewById(R.id.ivOrderOnDoorstep);

        llOrderOnDoorstep = (LinearLayout) findViewById(R.id.llOrderOnDoorstep);
        llOrderPrepared = (LinearLayout) findViewById(R.id.llOrderPrepared);
        llOrderOnWay = (LinearLayout) findViewById(R.id.llOrderOnWay);
        tvEstTime = (CustomFontTextView) findViewById(R.id.tvEstTime);
        llOrderAccepted = (LinearLayout) findViewById(R.id.llOrderAccepted);
        tvOrderAcceptedDate = findViewById(R.id.tvOrderAcceptedDate);
        tvOrderAcceptedTime = findViewById(R.id.tvOrderAcceptedTime);
        tvOrderReadyDate = findViewById(R.id.tvOrderReadyDate);
        tvOrderReadyTime = findViewById(R.id.tvOrderReadyTime);
        tvOrderOnTheWayDate = findViewById(R.id.tvOrderOnTheWayDate);
        tvOrderOnTheWayTime = findViewById(R.id.tvOrderOnTheWayTime);
        tvOrderReceiveDate = findViewById(R.id.tvOrderReceiveDate);
        tvOrderReceiveTime = findViewById(R.id.tvOrderReceiveTime);
        tvCancelOrder = (CustomFontTextView) findViewById(R.id.tvCancelOrder);
    }

    @Override
    protected void setViewListener() {
        // do somethings
        llOrderOnDoorstep.setOnClickListener(this);
        llOrderOnWay.setOnClickListener(this);
        llOrderPrepared.setOnClickListener(this);
        llOrderAccepted.setOnClickListener(this);
        tvCancelOrder.setClickable(false);
        tvCancelOrder.setText(R.string.push_message_2001);
    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();
    }

    private void gotToOrderActivity() {
        Intent intent = new Intent(this, OrdersActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    @Override
    public void onClick(View view) {
        // do somethings
        switch (view.getId()) {
            case R.id.llOrderPrepared:
                goToOrderPrepareActivity();
                break;
            case R.id.llOrderOnWay:
                int orderStatus = activeOrderResponse.getDeliveryStatus() <
                        activeOrderResponse
                                .getOrderStatus() ? activeOrderResponse.getOrderStatus()
                        : activeOrderResponse.getDeliveryStatus();
                if (orderStatus == Const.OrderStatus
                        .DELIVERY_MAN_PICKED_ORDER || orderStatus ==
                        Const.OrderStatus
                                .DELIVERY_MAN_STARTED_DELIVERY ||
                        orderStatus == Const.OrderStatus
                                .DELIVERY_MAN_ARRIVED_AT_DESTINATION || orderStatus == Const
                        .OrderStatus.FINAL_ORDER_COMPLETED) {
                    gotToProviderTrackActivity();
                } else {
                    Utils.showToast(getResources().getString(R.string
                            .msg_order_not_pickup_at), this);
                }

                break;
            case R.id.llOrderAccepted:
                // openCancelDialog();
                // openCancelOrderDialog(activeOrderResponse.getOrderCancellationCharge() > 0);
                break;
            case R.id.llOrderOnDoorstep:
                openConfirmCodeDialog();
                break;
            default:
                // do with default
                break;
        }
    }


    private void getExtraData() {

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getParcelable(Const.Params.ORDER) != null) {
                order = getIntent().getExtras().getParcelable(Const.Params.ORDER);

            } else if (getIntent().getExtras().getString(Const.Params.PUSH_DATA1) != null) {
                String pusData = getIntent().getExtras().getString(Const.Params.PUSH_DATA1);
                PushDataResponse pushDataResponse = new Gson().fromJson(pusData, PushDataResponse
                        .class);
                order = new Order();
                order.setId(pushDataResponse.getOrderId());
                order.setStoreName(pushDataResponse.getStoreName());
            } else {
                order = new Order();
                order.setId(getIntent().getExtras().getString(Const.Params.ORDER_ID));
                order.setStoreName(getIntent().getExtras().getString(Const.Params.STORE_ID));
            }
            setTitleOnToolBar(order.getStoreName());

        }


    }

    /**
     * this method called webservice for get order status
     *
     * @param orderId oderId in string
     */
    private void getOrderStatus(final String orderId) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper
                    .getUserId());
            jsonObject.put(ORDER_ID, orderId);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ORDER_TRACK_ACTIVITY, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ActiveOrderResponse> responseCall = apiInterface.getActiveOrderStatus(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<ActiveOrderResponse>() {
            @Override
            public void onResponse(Call<ActiveOrderResponse> call, Response<ActiveOrderResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    AppLog.Log("ORDER_STATUS", ApiClient.JSONResponse(response.body()));
                    activeOrderResponse = response.body();
                    String orderNumber = getResources().getString(R.string.text_order_number)
                            + " " + "#" + activeOrderResponse.getUniqueId();
                    tvOrderNumber.setText(orderNumber);
                    int orderStatus = activeOrderResponse.getDeliveryStatus() <
                            activeOrderResponse
                                    .getOrderStatus() ? activeOrderResponse.getOrderStatus()
                            : activeOrderResponse.getDeliveryStatus();
                    checkOrderStatus(orderStatus);
                    updateUIAsPerDelivery(activeOrderResponse.isUserPickUpOrder(),
                            activeOrderResponse.isConfirmationCodeRequiredAtCompleteDelivery());
                    getDateAndTimeOnStatus(activeOrderResponse);

                    llOrderTrack.setVisibility(View.VISIBLE);
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), OrderTrackActivity
                            .this);
                }

            }

            @Override
            public void onFailure(Call<ActiveOrderResponse> call, Throwable t) {

                AppLog.handleThrowable(Const.Tag.ORDER_TRACK_ACTIVITY, t);
                Utils.hideCustomProgressDialog();

            }
        });

    }

    /**
     * this method will help tp menage a view according to status code
     *
     * @param orderStatus
     */
    private void checkOrderStatus(int orderStatus) {
        // tvCancelOrder.setText(R.string.push_message_2001);
        Log.d("call_status", "========== Check Estimated Deliver time " +
                activeOrderResponse
                        .getEstimatedTimeForDeliveryInMin());
        Log.d("call_status", "========== Check Total time " +
                activeOrderResponse
                        .getTotalTime());

        Log.e("call_status", "==========Order Status ====> " + orderStatus);

        switch (orderStatus) {


            case Const.OrderStatus.WAITING_FOR_ACCEPT_STORE:
                ivOrderAccepted.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.red_dot, null));
                llOrderOnDoorstep.setEnabled(false);
                break;


            case Const.OrderStatus.STORE_ORDER_ACCEPTED:
                Log.i("STORE_ORDER_ACCEPTED", "ok");
                break;

            case Const.OrderStatus.STORE_ORDER_PREPARING:
                ivOrderAccepted.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderPrepared.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.red_dot, null));
                llOrderOnDoorstep.setEnabled(false);
                break;
            case Const.OrderStatus.DELIVERY_MAN_CANCELLED:
                onBackPressed();
                break;
            case Const.OrderStatus.WAITING_FOR_DELIVERY_MEN:
                ivOrderAccepted.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.red_dot, null));
                llOrderOnDoorstep.setEnabled(false);


               /* tvEstTime.setText(Utils.minuteToHoursMinutesSeconds(activeOrderResponse
                        .getTotalTime() + activeOrderResponse
                        .getEstimatedTimeForDeliveryInMin()));*/
                break;
            case Const.OrderStatus.DELIVERY_MAN_ACCEPTED:
                Log.i("DELIVERY_MAN_ACCEPTED", "ok");
                tvCancelOrder.setText(R.string.push_message_2081);
                ivOrderAccepted.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderPrepared.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.red_dot, null));
                llOrderOnDoorstep.setEnabled(false);
                /*tvEstTime.setText(Utils.minuteToHoursMinutesSeconds(activeOrderResponse
                        .getTotalTime() + activeOrderResponse
                        .getEstimatedTimeForDeliveryInMin()));*/

                break;
            case Const.OrderStatus.STORE_ORDER_READY:
            case Const.OrderStatus.DELIVERY_MAN_COMING:
            case Const.OrderStatus.DELIVERY_MAN_REJECTED:
            case Const.OrderStatus.DELIVERY_MAN_PICKED_ORDER:
                ivOrderAccepted.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderPrepared.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderOnWay.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.red_dot, null));
                //  tvCancelOrder.setVisibility(View.GONE);
                llOrderAccepted.setEnabled(false);
                llOrderOnDoorstep.setEnabled(false);
               /* tvEstTime.setText(Utils.minuteToHoursMinutesSeconds(activeOrderResponse
                        .getTotalTime() + activeOrderResponse
                        .getEstimatedTimeForDeliveryInMin()));*/
                break;
            case Const.OrderStatus.DELIVERY_MAN_NOT_FOUND:
                Utils.showToast(getResources().getString(R.string
                        .txt_no_deliver_man_found), OrderTrackActivity.this);
                break;

            case Const.OrderStatus.DELIVERY_MAN_ARRIVED:
                ivOrderAccepted.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderPrepared.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderOnWay.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.red_dot, null));
                //   tvCancelOrder.setVisibility(View.GONE);
                llOrderAccepted.setEnabled(false);
                llOrderOnDoorstep.setEnabled(false);
               /* tvEstTime.setText(Utils.minuteToHoursMinutesSeconds(activeOrderResponse
                        .getTotalTime() + activeOrderResponse
                        .getEstimatedTimeForDeliveryInMin()));*/
//                tvEstTime.setText("00:00");

                break;


            case Const.OrderStatus.DELIVERY_MAN_STARTED_DELIVERY:
                ivOrderAccepted.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderPrepared.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderOnWay.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderOnDoorstep.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.red_dot, null));
                //  tvCancelOrder.setVisibility(View.GONE);
                llOrderAccepted.setEnabled(false);
                /*tvEstTime.setText(Utils.minuteToHoursMinutesSeconds(
                        activeOrderResponse
                                .getEstimatedTimeForDeliveryInMin()));*/
                break;
            case Const.OrderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION:
                ivOrderAccepted.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderPrepared.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderOnWay.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderOnDoorstep.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                //   tvCancelOrder.setVisibility(View.GONE);
                llOrderAccepted.setEnabled(false);
                break;
            case Const.OrderStatus.FINAL_ORDER_COMPLETED:
                ivOrderAccepted.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderPrepared.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderOnWay.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                ivOrderOnDoorstep.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R
                        .drawable.checkmark, null));
                //    tvCancelOrder.setVisibility(View.GONE);
                llOrderAccepted.setEnabled(false);
                goToOrderCompleteActivity(true, true);
//                tvEstTime.setText("00:00");
                break;
            case Const.OrderStatus.STORE_ORDER_REJECTED:
            case Const.OrderStatus.STORE_ORDER_CANCELLED:
                onBackPressed();
                break;
            default:
                // do with default
                break;
        }

        if (orderStatus == Const.OrderStatus
                .DELIVERY_MAN_PICKED_ORDER || orderStatus == Const.OrderStatus
                .DELIVERY_MAN_STARTED_DELIVERY || orderStatus == Const.OrderStatus
                .DELIVERY_MAN_ARRIVED_AT_DESTINATION || orderStatus == Const
                .OrderStatus
                .DELIVERY_MAN_COMPLETE_DELIVERY) {


             /*strEstimateTime= String.valueOf(activeOrderResponse
                    .getEstimatedTimeForDeliveryInMin());
            if (strEstimateTime != null)
            {
                tvEstTime.setText(Utils.minuteToHoursMinutesSeconds(
                        Double.parseDouble(strEstimateTime)));
                Log.e("estimatetime","estimate time ==> "+ Double.parseDouble(strEstimateTime));
            }*/

            /*tvEstTime.setText(Utils.minuteToHoursMinutesSeconds(
                    activeOrderResponse
                            .getEstimatedTimeForDeliveryInMin()));*/


        } else {

            /*tvEstTime.setText(Utils.minuteToHoursMinutesSeconds(
                    activeOrderResponse
                            .getEstimatedTimeForDeliveryInMin()));*/

        }

        try {
            if (activeOrderResponse != null) {
                tvEstTime.setText(Utils.minuteToHoursMinutesSeconds(activeOrderResponse
                        .getTotalTime() + activeOrderResponse
                        .getEstimatedTimeForDeliveryInMin()));
            } else {
                tvEstTime.setText("00:00");
            }
        } catch (Exception e) {
            tvEstTime.setText("00:00");
        }

    }

    @Override
    public void onBackPressed() {

        if (order != null) {
            gotToOrderActivity();
        } else {
            goToHomeActivity();
        }


    }


    private void goToOrderPrepareActivity() {
        Intent intent = new Intent(this, OrderPreparedActivity.class);
        intent.putExtra(Const.Params.ORDER, order);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void openConfirmCodeDialog() {
        if (confirmDialog != null && confirmDialog.isShowing()) {
            return;
        }
        confirmDialog = new CustomDialogAlert(this, getResources().getString
                (R.string.text_confirmation_code), activeOrderResponse.getConfirmationCode(),
                getResources().getString
                        (R.string.text_cancel), getResources().getString
                (R.string.text_share)) {
            @Override
            public void onClickLeftButton() {
                dismiss();
            }

            @Override
            public void onClickRightButton() {
                dismiss();
                shareConfirmationCode();
            }
        };
        confirmDialog.show();

    }

    /**
     * this method will help to share your delivery conformation code to other user
     */
    private void shareConfirmationCode() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString
                (R.string.msg_delivery_confirm_code_is) + " " + activeOrderResponse
                .getConfirmationCode
                        ());
        startActivity(Intent.createChooser(sharingIntent, getResources().getString
                (R.string.msg_share_confirmation_code)));
    }


   /* protected void openCancelDialog() {
        if (cancelDialog != null && cancelDialog.isShowing()) {
            return;
        }
        cancelDialog = new CustomDialogAlert(this, this
                .getResources()
                .getString(R.string.text_cancel_order), this.getResources()
                .getString(R.string.msg_cancel), this.getResources()
                .getString(R.string.text_yes), this.getResources()
                .getString(R.string.text_no)) {
            @Override
            public void onClickLeftButton() {
                cancelOrder("");
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                dismiss();
            }
        };
        cancelDialog.show();
    }*/

    private void openCancelOrderDialog(boolean isCancelChargeApply) {
        if (orderCancelDialog != null && orderCancelDialog.isShowing()) {
            return;
        }
        RadioGroup radioGroup;
        final RadioButton rbReasonOne, rbReasonTwo, rbReasonOther;
        final CustomFontEditTextView etOtherReason;
        final CustomFontTextView tvCancelMessage, tvCharge;
        orderCancelDialog = new Dialog(this);
        orderCancelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        orderCancelDialog.setContentView(R.layout.dialog_cancel_order);
        tvCharge = (CustomFontTextView) orderCancelDialog.findViewById(R.id.tvCharge);
        tvCancelMessage = (CustomFontTextView) orderCancelDialog.findViewById(R.id.tvCancelMessage);
        rbReasonOne = (RadioButton) orderCancelDialog.findViewById(R.id.rbReasonOne);
        rbReasonTwo = (RadioButton) orderCancelDialog.findViewById(R.id.rbReasonTwo);
        rbReasonOther = (RadioButton) orderCancelDialog.findViewById(R.id.rbReasonOthers);
        radioGroup = (RadioGroup) orderCancelDialog.findViewById(R.id.radioGroup);
        etOtherReason = (CustomFontEditTextView) orderCancelDialog.findViewById(R.id
                .etOthersReason);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.rbReasonOne:
                        etOtherReason.setVisibility(View.GONE);
                        cancelReason = rbReasonOne.getText().toString();
                        break;
                    case R.id.rbReasonTwo:
                        etOtherReason.setVisibility(View.GONE);
                        cancelReason = rbReasonTwo.getText().toString();
                        break;
                    case R.id.rbReasonOthers:
                        etOtherReason.setVisibility(View.VISIBLE);
                        break;
                    default:
                        // do with default
                        break;
                }
            }
        });
        orderCancelDialog.findViewById(R.id.btnDialogAlertRight).setOnClickListener(new View
                .OnClickListener
                () {
            @Override
            public void onClick(View view) {
                if (rbReasonOther.isChecked()) {
                    cancelReason = etOtherReason.getText().toString();
                }
                if (!TextUtils.isEmpty(cancelReason)) {

                    cancelOrder(cancelReason);
                    orderCancelDialog.dismiss();
                } else {
                    Utils.showToast(getResources().getString(R
                            .string.msg_plz_give_valid_reason), OrderTrackActivity.this);
                }
            }
        });
        orderCancelDialog.findViewById(R.id.btnDialogAlertLeft).setOnClickListener(new View
                .OnClickListener() {
            @Override
            public void onClick(View view) {
                orderCancelDialog.dismiss();
                cancelReason = "";
            }
        });
        WindowManager.LayoutParams params = orderCancelDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        orderCancelDialog.setCancelable(false);
        orderCancelDialog.show();
        if (isCancelChargeApply) {
            tvCharge.setText(activeOrderResponse.getCurrency() + activeOrderResponse
                    .getOrderCancellationCharge());
            tvCharge.setVisibility(View.VISIBLE);
            tvCancelMessage.setVisibility(View.VISIBLE);
        } else {
            tvCancelMessage.setVisibility(View.GONE);
            tvCharge.setVisibility(View.GONE);
        }

    }

    /**
     * this method called a webservice for cancel a current delivery order
     *
     * @param cancelReason string reason
     */

    private void cancelOrder(String cancelReason) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper
                    .getUserId());
            jsonObject.put(Const.Params.ORDER_ID, order.getId());
            jsonObject.put(Const.Params.ORDER_STATUS, Const.OrderStatus
                    .ORDER_CANCELED_BY_USER);
            jsonObject.put(Const.Params.CANCEL_REASON, cancelReason);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.ORDER_TRACK_ACTIVITY, e);
        }


        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.cancelOrder(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    openCancellationInfoDialog();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), OrderTrackActivity
                            .this);
                }

            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.ORDER_TRACK_ACTIVITY, t);
            }
        });

    }

    private void openCancellationInfoDialog() {
        String msg = getResources().getString(R.string.msg_cancellation);
        CustomDialogAlert customDialogAlert = new CustomDialogAlert(OrderTrackActivity.this, getResources().getString(R.string
                .text_attention), msg, getResources().getString(R.string.text_ok), getResources().getString(R.string.text_cancel)) {
            @Override
            public void onClickLeftButton() {
                onBackNavigation();
                dismiss();
            }

            @Override
            public void onClickRightButton() {
                dismiss();
            }
        };
        customDialogAlert.show();
        customDialogAlert.btnDialogEditTextRight.setVisibility(View.GONE);
    }

    /**
     * this method open Complete order activity witch is contain two fragment
     * Invoice Fragment and Feedback Fragment
     *
     * @param isGoToInvoice set true when you want to open invoice fragment when activity open
     * @param isGoToHome    set true when you want got back to home activity when press back button
     */
    private void goToOrderCompleteActivity(boolean isGoToInvoice, boolean isGoToHome) {


        Intent intent = new Intent(this, OrderCompleteActivity.class);
        intent.putExtra(Const.Params.ORDER, activeOrderResponse);
        intent.putExtra(Const.Params.ORDER_ID, order.getId());
        intent.putExtra(Const.GO_TO_INVOICE, isGoToInvoice);
        intent.putExtra(Const.GO_TO_HOME, isGoToHome);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void gotToProviderTrackActivity() {
        Intent intent = new Intent(this, ProviderTrackActivity.class);
        intent.putExtra(Const.Params.ORDER, activeOrderResponse);
        intent.putExtra(Const.Params.ORDER_ID, order);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    private void updateUIAsPerDelivery(boolean isPickupByUser, boolean isConfirmCodeRequried) {

        if (isConfirmCodeRequried) {
            findViewById(R.id.tvGetCode).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.tvGetCode).setVisibility(View.GONE);
        }

        if (isPickupByUser) {
            llOrderOnWay.setVisibility(View.GONE);
            findViewById(R.id.llDeliveryTime).setVisibility(View.GONE);
        } else {
            llOrderOnWay.setVisibility(View.VISIBLE);
            findViewById(R.id.llDeliveryTime).setVisibility(View.VISIBLE);
        }


    }


    @Override
    public void onOrderStatus() {
        if (!TextUtils.isEmpty(order.getId())) {
            if (order.getId() != null) {
                getOrderStatus(order.getId());
            } else {
                Utils.showToast(getString(R.string.something_went_wrong), getActivity());
            }

        }
    }


    private void getDateAndTimeOnStatus(ActiveOrderResponse activeOrderResponse) {
        List<Status> statusList = new ArrayList<>();
        statusList.addAll(activeOrderResponse.getDeliveryStatusDetails());
        statusList.addAll(activeOrderResponse.getOrderStatusDetails());
        for (Status status : statusList) {
            if (Const.OrderStatus.DELIVERY_MAN_ACCEPTED == status.getStatus()) {
                setDateAnTime(tvOrderAcceptedDate, tvOrderAcceptedTime, status.getDate());
                tvCancelOrder.setText(R.string.push_message_2081);
            } else if (Const.OrderStatus.DELIVERY_MAN_PICKED_ORDER == status.getStatus()) {
                setDateAnTime(tvOrderReadyDate, tvOrderReadyTime, status.getDate());
            } else if (Const.OrderStatus.DELIVERY_MAN_STARTED_DELIVERY == status.getStatus()) {
                setDateAnTime(tvOrderOnTheWayDate, tvOrderOnTheWayTime, status.getDate());
            } else if (Const.OrderStatus.DELIVERY_MAN_ARRIVED_AT_DESTINATION == status.getStatus()) {
                setDateAnTime(tvOrderReceiveDate, tvOrderReceiveTime, status.getDate());
            } else if (Const.OrderStatus.DELIVERY_MAN_COMPLETE_DELIVERY == status.getStatus()) {
                setDateAnTime(tvOrderReceiveDate, tvOrderReceiveTime, status.getDate());
            }
        }
    }

    private void setDateAnTime(CustomFontTextView dateView, CustomFontTextView timeView, String
            date) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd", Locale.US);
            Date statusDate = parseContent.webFormat.parse(date);
            dateView.setText(simpleDateFormat.format(statusDate));
            timeView.setText(parseContent.timeFormat_am.format(statusDate));
            dateView.setVisibility(View.VISIBLE);
            timeView.setVisibility(View.VISIBLE);

        } catch (ParseException e) {
            AppLog.handleException(OrderTrackActivity.class.getSimpleName(), e);
        }
    }
}
