package com.edelivery;

import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;


import com.edelivery.models.datamodels.Product;
import com.edelivery.utils.PreferenceHelper;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.core.content.res.ResourcesCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.text.TextUtils;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.edelivery.adapter.ProductItemItemAdapter;
import com.edelivery.adapter.ProductSpecificationItemAdapter;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Addresses;
import com.edelivery.models.datamodels.CartOrder;
import com.edelivery.models.datamodels.CartProductItems;
import com.edelivery.models.datamodels.CartProducts;
import com.edelivery.models.datamodels.CartUserDetail;
import com.edelivery.models.datamodels.ProductDetail;
import com.edelivery.models.datamodels.ProductItem;
import com.edelivery.models.datamodels.SpecificationSubItem;
import com.edelivery.models.datamodels.Specifications;
import com.edelivery.models.datamodels.Store;
import com.edelivery.models.responsemodels.AddCartResponse;
import com.edelivery.models.responsemodels.AvailableProviderResponse;
import com.edelivery.models.responsemodels.CartResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

public class ProductSpecificationActivity extends BaseAppCompatActivity {

    public ProductItem productItem;
    private int requiredCount = 0;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ImageView ivProductImage;
    private CustomFontEditTextView etAddNote;
    private CustomFontTextView tvProductName, tvProductDescription, btnDecrease, btnIncrease,
            tvItemQuantity, tvItemAmount, tvAddToCart;
    private LinearLayout llAddToCart;
    private RecyclerView rcvSpecificationItem;
    private ProductSpecificationItemAdapter productSpecificationItemAdapter;
    private int itemQuantity = 1;
    public String storeid = "";
    private double itemPriceAndSpecificationPriceTotal = 0;
    private int cartCount = 0;
    private List<Specifications> specifications = new ArrayList<>();
    private ProductDetail productDetail;
    private ViewPager imageViewPager, imageViewPagerDialog;
    private LinearLayout llDots, llDotsDialog;
    private TextView[] dots, dots1;
    private NestedScrollView scrollView;
    private CustomFontTextView tvMaxQuantity;
    private boolean isScheduleStart;
    private ScheduledExecutorService tripStatusSchedule;
    private Handler handler;
    private int updateItemIndex = -1;
    private int updateItemSectionIndex = -1;
    private Store store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupWindowAnimations();
        setContentView(R.layout.activity_product_specification);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /*FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplicationContext());*/
        try {
            logger = (AppEventsLogger) AppEventsLogger.newLogger(getApplicationContext());
        } catch (Exception e) {
        }
        initToolBar();
        setToolbarRightIcon3(R.drawable.ic_shopping_bag, this);
        toolbar.setBackground(AppCompatResources.getDrawable(this, R.drawable
                .toolbar_transparent_2));
        setToolbarRightIcon3(R.drawable.ic_shopping_bag_white, this);
        ivToolbarBack.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable
                .ic_left_arrow_white));
        tvTitleToolbar.setTextColor(ResourcesCompat.getColor(getResources(),
                android.R.color.transparent, null));
        findViewById();
        setViewListener();
        loadExtraData();
        initImagePager();
        initAppBar();
        initCollapsingToolbar();
        setData(itemQuantity);
        loadProductSpecification();
        setTitleOnToolBar(productItem.getName());
        initHandler();
        setCartButtonTitle();


        String str_currency;
        if (productItem.getCurrency() != null && !productItem.getCurrency().equals("")) {
            if (productItem.getCurrency().toUpperCase().contains("NULL")) {
                str_currency = productItem.getCurrency().toUpperCase().replace("NULL", "");
            } else {
                str_currency = productItem.getCurrency();
            }
        } else {
            str_currency = "";
        }
        logViewedContentEvent(productDetail.getName(), "", currentBooking.getCartId(), Const.Facebook.FB_CURRENCY_CODE, productItem.getTotalPrice());

    }

    public void setCartButtonTitle() {
        if (!isFromCartActivity()) {
            tvAddToCart.setText(getString(R.string.text_add_to_cart));
        } else {
            tvAddToCart.setText(getString(R.string.text_update_cart));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCartItem();
        startAdsScheduled();
    }

    @Override
    protected void onPause() {
        stopAdsScheduled();
        super.onPause();
    }

    private void initImagePager() {
        addBottomDots(0);
        ProductItemItemAdapter itemItemAdapter = new ProductItemItemAdapter(this, productItem
                .getImageUrl(), R.layout.item_image, true);
        imageViewPager.setAdapter(itemItemAdapter);
        imageViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int
                    positionOffsetPixels) {
                dotColorChange(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void addBottomDots(int currentPage) {
        if (productItem.getImageUrl().size() > 1) {
            dots = new TextView[productItem
                    .getImageUrl().size()];

            llDots.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(this);
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(35);
                dots[i].setTextColor(ResourcesCompat.getColor(getResources(), R.color
                                .color_app_label
                        , null));
                llDots.addView(dots[i]);
            }

            if (dots.length > 0)
                dots[currentPage].setTextColor(ResourcesCompat.getColor(getResources(), R.color
                                .color_app_text
                        , null));
        }


    }

    private void dotColorChange(int currentPage) {
        if (llDots.getChildCount() > 0) {
            for (int i = 0; i < llDots.getChildCount(); i++) {
                TextView textView = (TextView) llDots.getChildAt(i);
                textView.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                                .color_app_label
                        , null));

            }
            TextView textView = (TextView) llDots.getChildAt(currentPage);
            textView.setTextColor(ResourcesCompat.getColor(getResources(), R.color.color_app_text
                    , null));
        }


    }

    private void addBottomDotsDialog(int currentPage) {
        if (productItem.getImageUrl().size() > 1) {
            dots1 = new TextView[productItem
                    .getImageUrl().size()];

            llDotsDialog.removeAllViews();
            for (int i = 0; i < dots1.length; i++) {
                dots1[i] = new TextView(this);
                dots1[i].setText(Html.fromHtml("&#8226;"));
                dots1[i].setTextSize(35);
                dots1[i].setTextColor(ResourcesCompat.getColor(getResources(), R.color
                                .color_app_label
                        , null));
                llDotsDialog.addView(dots1[i]);
            }

            if (dots1.length > 0)
                dots1[currentPage].setTextColor(ResourcesCompat.getColor(getResources(), R.color
                                .color_app_text
                        , null));
        }


    }

    private void dotColorChangeDialog(int currentPage) {
        if (llDotsDialog.getChildCount() > 0) {
            for (int i = 0; i < llDotsDialog.getChildCount(); i++) {
                TextView textView = (TextView) llDotsDialog.getChildAt(i);
                textView.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                                .color_app_label
                        , null));

            }
            TextView textView = (TextView) llDotsDialog.getChildAt(currentPage);
            textView.setTextColor(ResourcesCompat.getColor(getResources(), R.color.color_app_text
                    , null));
        }


    }

    private int getItem(int i) {
        return imageViewPager.getCurrentItem() + i;
    }

    @Override
    protected boolean isValidate() {
        //do somethings
        return false;
    }

    @Override
    protected void findViewById() {
        //do somethings
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id
                .specificationCollapsingToolBar);
//        ivProductImage = (ImageView) findViewById(R.id.ivProductImage);
        tvProductDescription = (CustomFontTextView) findViewById(R.id
                .tvCollapsingProductDescription);
        tvProductName = (CustomFontTextView) findViewById(R.id.tvCollapsingProductName);
        rcvSpecificationItem = (RecyclerView) findViewById(R.id.rcvSpecificationItem);
        btnIncrease = (CustomFontTextView) findViewById(R.id.btnIncrease);
        btnDecrease = (CustomFontTextView) findViewById(R.id.btnDecrease);
        tvItemQuantity = (CustomFontTextView) findViewById(R.id.tvItemQuantity);
        llAddToCart = (LinearLayout) findViewById(R.id.llAddToCart);
        tvItemAmount = (CustomFontTextView) findViewById(R.id.tvItemAmount);
        tvAddToCart = (CustomFontTextView) findViewById(R.id.tvAddToCart);

        imageViewPager = (ViewPager) findViewById(R.id.imageViewPager);
        llDots = (LinearLayout) findViewById(R.id.llDots);
        scrollView = (NestedScrollView) findViewById(R.id.scrollView);
        etAddNote = (CustomFontEditTextView) findViewById(R.id.etAddNote);
        tvMaxQuantity = (CustomFontTextView) findViewById(R.id.tvMaxQuantity);


    }

    @Override
    protected void setViewListener() {
        //do somethings
        btnDecrease.setOnClickListener(this);
        btnIncrease.setOnClickListener(this);

    }

    private void setData(int itemQuantity) {
        tvItemQuantity.setText(String.valueOf(itemQuantity));
    }

    @Override
    protected void onBackNavigation() {
        //do somethings
        onBackPressed();
    }


    @Override
    public void onClick(View view) {
        //do somethings

        switch (view.getId()) {
            case R.id.llAddToCart:
                AppLog.Log("STORE_ID", "selected_store_id = " + productItem.getStoreId() + "" +
                        " " +
                        "" + "save_store_id = " + currentBooking.getSelectedStoreId());
                checkAvailableProvider();
                //checkValidCartItem();
                break;
            case R.id.btnIncrease:
                increaseItemQuality();
                break;
            case R.id.btnDecrease:
                decreaseItemQuantity();
                break;
            case R.id.ivToolbarRightIcon3:
                if (isFromCartActivity()) {
                    onBackPressed();
                } else {
                    goToCartActivity();
                }
                break;
            default:
                // do with default
                break;
        }
    }

    private void checkAvailableProvider() {
        Utils.showCustomProgressDialog(this, false);

        JSONObject jsonObject = new JSONObject();
        try {
            if (store != null && currentBooking.getPickupAddresses().isEmpty()) {
                jsonObject.put(Const.Params.LATITUDE, store.getLocation().get(0));
                jsonObject.put(Const.Params.LONGITUDE, store.getLocation().get(1));
            } else {
                jsonObject.put(Const.Params.LATITUDE, currentBooking.getPickupAddresses().get(0).getLocation().get(0));
                jsonObject.put(Const.Params.LONGITUDE, currentBooking.getPickupAddresses().get(0).getLocation().get(1));
            }
            jsonObject.put(Const.Params.CITY_ID, currentBooking.getBookCityId());
            if (!isFromCartActivity()) {
                jsonObject.put(Const.Params.STORE_ID, store.getId());
            } else {
                jsonObject.put(Const.Params.STORE_ID, storeid);
            }


        } catch (JSONException e) {
            AppLog.handleException(PaymentActivity.class.getName(), e);
        }

        Log.i("checkAvailableProvider", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AvailableProviderResponse> responseCall = apiInterface.checkStoreProviderAvailable(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<AvailableProviderResponse>() {
            @Override
            public void onResponse(Call<AvailableProviderResponse> call, Response<AvailableProviderResponse>
                    response) {

                Log.i("checkAvailableProvider", ApiClient.JSONResponse(response.body()));
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().getSuccess()) {
                    if (!response.body().getProviders().isEmpty()) {
                        Log.i("checkAvailableProvider", "jsonObject.toString()");
                        AppLog.Log("CHECK_PROVIDER_AVAILABLE", ApiClient.JSONResponse(response.body()));
                        checkValidCartItem();
                    } else {
                        Utils.showToast(getResources().getString(R.string
                                .text_no_provider_found), ProductSpecificationActivity.this);
                    }
                } else {
                    Utils.showToast(getResources().getString(R.string
                            .text_no_provider_found), ProductSpecificationActivity.this);
                }

            }

            @Override
            public void onFailure(Call<AvailableProviderResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
            }
        });
    }


    private void initCollapsingToolbar() {
        collapsingToolbarLayout.setTitleEnabled(false);
        collapsingToolbarLayout.setContentScrimColor(ResourcesCompat.getColor(getResources(), R
                .color.color_app_theme, null));
        collapsingToolbarLayout.setStatusBarScrimColor(ResourcesCompat.getColor(getResources(), R
                .color.color_app_theme, null));
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);

    }

    private void initAppBar() {

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        tvTitleToolbar.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                                .color_black, null));
                        setToolbarRightIcon3(R.drawable.ic_shopping_bag,
                                ProductSpecificationActivity.this);
                        ivToolbarBack.setImageDrawable(AppCompatResources.getDrawable
                                (ProductSpecificationActivity.this, R
                                        .drawable.ic_left_arrow));
                        toolbar.setBackground(null);
                        break;
                    case 2:
                        tvTitleToolbar.setTextColor(ResourcesCompat.getColor(getResources(),
                                android.R.color.transparent, null));
                        setToolbarRightIcon3(R.drawable.ic_shopping_bag_white,
                                ProductSpecificationActivity
                                        .this);
                        ivToolbarBack.setImageDrawable(AppCompatResources.getDrawable
                                (ProductSpecificationActivity.this, R
                                        .drawable.ic_left_arrow_white));
                        tvTitleToolbar.setTextColor(ResourcesCompat.getColor(getResources(),
                                android.R.color.transparent, null));
                        toolbar.setBackground(AppCompatResources.getDrawable
                                (ProductSpecificationActivity.this, R.drawable
                                        .toolbar_transparent_2));
                        break;
                    default:
                        // do with default
                        break;
                }
            }
        };
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.specificationAppBarLayout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    handler.sendEmptyMessage(1);
                    isShow = true;
                } else if (isShow) {
                    handler.sendEmptyMessage(2);
                    isShow = false;
                }
            }
        });
    }

    private void loadProductSpecification() {
        tvProductName.setText(productItem.getName());
        tvProductDescription.setText(productItem.getDetails());
        specifications = productItem.getSpecifications();
        reloadAmountData(productItem.getPrice());
        countIsRequiredAndDefaultSelected();
        modifyTotalItemAmount();
        productSpecificationItemAdapter = new ProductSpecificationItemAdapter(this, (ArrayList
                <Specifications>) specifications);
        rcvSpecificationItem.setLayoutManager(new LinearLayoutManager(this));
        rcvSpecificationItem.setNestedScrollingEnabled(false);
        rcvSpecificationItem.setAdapter(productSpecificationItemAdapter);
        scrollView.getParent().requestChildFocus(scrollView, scrollView);
    }


    /**
     * this method manag single type specification click event
     *
     * @param section
     * @param position
     * @param absolutePosition
     */
    public void onSingleItemClick(int section, int position, int
            absolutePosition) {
        try {
            List<SpecificationSubItem> specificationSubItems = specifications.get(section)
                    .getList();
            for (SpecificationSubItem specificationSubItem : specificationSubItems) {
                specificationSubItem.setIsDefaultSelected(false);
            }
            specificationSubItems.get(position).setIsDefaultSelected(true);
            specifications.get(section).setSelectedCount(1);
            productSpecificationItemAdapter.notifyItemRangeChanged(section,
                    productSpecificationItemAdapter.getItemCount());
            modifyTotalItemAmount();
        } catch (Exception e) {
            Log.e(" " , e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        try {
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } catch (Exception e) {
            Log.e("onBackPressed() ", e.getMessage());
        }
    }


    /**
     * this method to create cart structure witch is help to add item in cart
     */
    private void addToCart() {
        AppLog.Log("addToCart", "ok");

        double specificationPriceTotal = 0;
        double specificationPrice = 0;
        currentBooking.setDeliveryLatLng(currentBooking.getCurrentLatLng());
        currentBooking.setDeliveryAddress(currentBooking.getCurrentAddress());
        ArrayList<Specifications> specificationList = new ArrayList<>();
        Utils.showCustomProgressDialog(this, false);
        for (Specifications specificationListItem : specifications) {
            ArrayList<SpecificationSubItem> specificationItemCartList = new ArrayList<>();
            for (SpecificationSubItem listItem : specificationListItem.getList()) {
                if (listItem.isIsDefaultSelected()) {
                    specificationPrice = specificationPrice + listItem.getPrice();
                    specificationPriceTotal = specificationPriceTotal + listItem.getPrice();
                    specificationItemCartList.add(listItem);
                }
            }

            Specifications specifications = new Specifications();
            specifications.setList(specificationItemCartList);
            specifications.setName(specificationListItem.getName());
            specifications.setPrice(specificationPrice);
            specifications.setType(specificationListItem.getType());
            specifications.setUniqueId(specificationListItem.getUniqueId());
            specificationList.add(specifications);
            specificationPrice = 0;
        }


        CartProductItems
                cartProductItems = new CartProductItems();
        cartProductItems.setItemId(productItem.getId());
        cartProductItems.setUniqueId(productItem.getUniqueId());
        cartProductItems.setItemName(productItem.getName());
        cartProductItems.setQuantity(itemQuantity);
        cartProductItems.setImageUrl(productItem.getImageUrl());
        cartProductItems.setDetails(productItem.getDetails());
        cartProductItems.setSpecifications(specificationList);
        cartProductItems.setTotalSpecificationPrice(specificationPriceTotal);
        cartProductItems.setItemPrice(productItem.getPrice());
        cartProductItems.setItemNote(etAddNote.getText().toString());
        cartProductItems.setTotalItemAndSpecificationPrice(itemPriceAndSpecificationPriceTotal);

        // add new filed 0n 4_Aug_2018

        if (store != null) {
            cartProductItems.setTax(store.isUseItemTax() ? productItem.getTax() : store
                    .getTax());
        } else {
            cartProductItems.setTax(productItem.getTax());
        }
        cartProductItems.setTotalPrice(cartProductItems.getItemPrice() + cartProductItems
                .getTotalSpecificationPrice());

        cartProductItems.setItemTax(getTaxableAmount(productItem.getPrice(), cartProductItems
                .getTax()));
        cartProductItems.setTotalSpecificationTax(getTaxableAmount(specificationPriceTotal,
                cartProductItems.getTax()));
        cartProductItems.setTotalTax(cartProductItems.getItemTax() + cartProductItems
                .getTotalSpecificationTax());
        cartProductItems.setTotalItemTax(cartProductItems.getTotalTax() * cartProductItems
                .getQuantity());

        if (isFromCartActivity()) {
            currentBooking.getCartProductWithSelectedSpecificationList().get
                    (updateItemSectionIndex).getItems().set(updateItemIndex, cartProductItems);
            addItemInServerCart(currentBooking.getCartProductWithSelectedSpecificationList().get
                    (updateItemSectionIndex));
        } else {
            if (!currentBooking.getCartProductItemWithAllSpecificationList().contains
                    (productItem)) {
                currentBooking.setCartProductItemWithAllSpecificationList(productItem);
            }
            if (isProductExistInLocalCart(cartProductItems)) {

            } else {

                ArrayList<CartProductItems> cartProductItemsList = new ArrayList<>();
                cartProductItemsList.add(cartProductItems);
                CartProducts cartProducts = new CartProducts();
                cartProducts.setItems(cartProductItemsList);
                cartProducts.setProductId(productItem.getProductId());
                cartProducts.setProductName(productDetail.getName());
                cartProducts.setUniqueId(productDetail.getUniqueId());
                cartProducts.setTotalProductItemPrice(itemPriceAndSpecificationPriceTotal);
                cartProducts.setTotalItemTax(cartProductItems.getTotalItemTax());
                currentBooking.setCartProduct(cartProducts);
                currentBooking.setSelectedStoreId(productItem.getStoreId());
                addItemInServerCart(cartProducts);
            }
            Utils.hideCustomProgressDialog();
        }

    }


    /**
     * this method check product is exist in local cart
     *
     * @param cartProductItems
     * @return true if product exist otherwise false
     */
    private boolean isProductExistInLocalCart(CartProductItems cartProductItems) {
        for (CartProducts cartProducts : currentBooking
                .getCartProductWithSelectedSpecificationList()) {
            if (TextUtils.equals(cartProducts.getProductId(), productItem.getProductId())) {
                cartProducts.getItems().add(cartProductItems);
                itemPriceAndSpecificationPriceTotal = cartProducts
                        .getTotalProductItemPrice() +
                        itemPriceAndSpecificationPriceTotal;
                cartProducts.setTotalProductItemPrice
                        (itemPriceAndSpecificationPriceTotal);
                cartProducts.setTotalItemTax(cartProducts.getTotalItemTax() + cartProductItems
                        .getTotalItemTax());
                addItemInServerCart(cartProducts);
                return true;
            }
        }
        return false;
    }

    /**
     * this method called webservice for add product in cart
     *
     * @param cartProducts CartProducts object
     */
    private void addItemInServerCart(final CartProducts cartProducts) {

        AppLog.Log("addItemInServerCart", "ok");
        Utils.showCustomProgressDialog(this, false);

        CartOrder cartOrder = new CartOrder();
        cartOrder.setUserType(Const.Type.USER);
        if (isCurrentLogin()) {
            cartOrder.setUserId(preferenceHelper.getUserId());
            cartOrder.setAndroidId("");
        } else {
            cartOrder.setAndroidId(preferenceHelper.getAndroidId());
            cartOrder.setUserId("");
        }
        cartOrder.setServerToken(preferenceHelper.getSessionToken());
        cartOrder.setStoreId(currentBooking.getSelectedStoreId());
        cartOrder.setProducts(currentBooking.getCartProductWithSelectedSpecificationList());

        if (currentBooking.getDestinationAddresses().isEmpty() || !TextUtils.equals
                (currentBooking.getDestinationAddresses().get(0).getAddress(),
                        currentBooking.getDeliveryAddress())) {
            currentBooking.getDestinationAddresses().clear();
            Addresses addresses = new Addresses();
            addresses.setAddress(currentBooking.getDeliveryAddress());
            addresses.setCity(currentBooking.getCity1());
            addresses.setAddressType(Const.Type.DESTINATION);
            addresses.setNote("");
            addresses.setUserType(Const.Type.USER);
            ArrayList<Double> location = new ArrayList<>();
            location.add(currentBooking.getDeliveryLatLng().latitude);
            location.add(currentBooking.getDeliveryLatLng().longitude);
            addresses.setLocation(location);
            CartUserDetail cartUserDetail = new CartUserDetail();
            cartUserDetail.setEmail(preferenceHelper.getEmail());
            cartUserDetail.setCountryPhoneCode(preferenceHelper.getPhoneCountyCodeCode());
            cartUserDetail.setName(preferenceHelper.getFirstName() + " " + preferenceHelper
                    .getLastName());
            cartUserDetail.setPhone(preferenceHelper.getPhoneNumber());
            addresses.setUserDetails(cartUserDetail);
            currentBooking.setDestinationAddresses(addresses);
        }


        if (currentBooking.getPickupAddresses().isEmpty() && store != null) {
            Addresses addresses = new Addresses();
            addresses.setAddress(store.getAddress());
            addresses.setCity("");
            addresses.setAddressType(Const.Type.PICKUP);
            addresses.setNote("");
            addresses.setUserType(Const.Type.STORE);
            ArrayList<Double> location = new ArrayList<>();
            location.add(store.getLocation().get(0));
            location.add(store.getLocation().get(1));
            addresses.setLocation(location);
            CartUserDetail cartUserDetail = new CartUserDetail();
            cartUserDetail.setEmail(store.getEmail());
            cartUserDetail.setCountryPhoneCode(store.getCountryPhoneCode());
            cartUserDetail.setName(store.getName());
            cartUserDetail.setPhone(store.getPhone());
            addresses.setUserDetails(cartUserDetail);
            currentBooking.setPickupAddresses(addresses);
        }
        cartOrder.setDestinationAddresses(currentBooking.getDestinationAddresses());
        cartOrder.setPickupAddresses(currentBooking.getPickupAddresses());

        // add filed on 4_Aug_2018
        double cartOrderTotalPrice = 0, cartOrderTotalTaxPrice = 0;
        for (CartProducts products : currentBooking.getCartProductWithSelectedSpecificationList()) {
            cartOrderTotalPrice = cartOrderTotalPrice + products
                    .getTotalProductItemPrice();
            cartOrderTotalTaxPrice = cartOrderTotalTaxPrice + products.getTotalItemTax();
        }
        cartOrder.setCartOrderTotalPrice(cartOrderTotalPrice);
        cartOrder.setCartOrderTotalTaxPrice(cartOrderTotalTaxPrice);

        AppLog.Log("ADD_ITEM_CART", ApiClient.JSONResponse(cartOrder));
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddCartResponse> responseCall = apiInterface.addItemInCart(ApiClient
                .makeGSONRequestBody(cartOrder));
        responseCall.enqueue(new Callback<AddCartResponse>() {
            @Override
            public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {

                    if (!isFromCartActivity()) {
                        currentBooking.setStorename(store.getName());
                    }


                    currentBooking.setCartId(response.body().getCartId());
                    currentBooking.setCartCityId(response.body().getCityId());

                    String str_currency;
                    if (productItem.getCurrency() != null && !productItem.getCurrency().equals("")) {
                        if (productItem.getCurrency().toUpperCase().contains("NULL")) {
                            str_currency = productItem.getCurrency().toUpperCase().replace("NULL", "");
                        } else {
                            str_currency = productItem.getCurrency();
                        }
                    } else {
                        str_currency = "";
                    }

                    currentBooking.setCartCurrency(str_currency);

                    logAddedToCartEvent(currentBooking.getCartId(), "", Const.Facebook.FB_CURRENCY_CODE, currentBooking.getTotalCartAmount());

                    Utils.showMessageToast(response.body().getMessage(),
                            ProductSpecificationActivity.this);
                    onBackPressed();
                } else {

                    Utils.showErrorToast(response.body().getErrorCode(),
                            ProductSpecificationActivity.this);
                    getCart();
                }


            }

            @Override
            public void onFailure(Call<AddCartResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                currentBooking.getCartProductWithSelectedSpecificationList().remove(cartProducts);
                AppLog.handleThrowable(Const.Tag.PRODUCT_SPE_ACTIVITY, t);
            }
        });
    }

    // Add to Cart Facebook Event
    public void logAddedToCartEvent(String contentId, String contentType, String currency, double price) {
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent(AppEventsConstants.EVENT_NAME_ADDED_TO_CART, price, params);
    }

    private void openClearCartDialog() {
        final CustomDialogAlert dialogAlert = new CustomDialogAlert(this, getResources().getString(R
                .string
                .text_attention), getResources().getString(R.string.msg_other_store_item_in_cart),
                getResources().getString(R.string.text_cancel), getResources().getString(R.string
                .text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                clearCart();
                dismiss();
            }
        };
        dialogAlert.show();
    }

    private void checkValidCartItem() {
        //Log.i("checkAvailableProvider","jsonObject.toString()");
        AppLog.Log("checkValidCartItem", "ok");
        if (cartCount == 0) {

            addToCart();
        } else {

            if (TextUtils.equals(currentBooking.getSelectedStoreId(), productItem.getStoreId
                    ())) {

                addToCart();
            } else {
                openClearCartDialog();
            }
        }
    }


    private void increaseItemQuality() {
        itemQuantity++;
        setData(itemQuantity);
        modifyTotalItemAmount();
    }

    private void decreaseItemQuantity() {
        if (itemQuantity > 1) {
            itemQuantity--;
            setData(itemQuantity);
            modifyTotalItemAmount();
        }

    }

    /**
     * this method will manage total amount after change or modify
     */
    public void modifyTotalItemAmount() {
        itemPriceAndSpecificationPriceTotal = productItem.getPrice();
        int requiredCountTemp = 0;
        for (Specifications specifications : this.specifications) {
            for (SpecificationSubItem listItem : specifications.getList()) {
                if (listItem.isIsDefaultSelected()) {
                    itemPriceAndSpecificationPriceTotal = itemPriceAndSpecificationPriceTotal +
                            listItem.getPrice();
                }
            }

            if (specifications.isRequired() && specifications.getSelectedCount() >= specifications
                    .getRange() && (specifications.getMaxRange() == 0 || specifications
                    .getSelectedCount() <= specifications
                    .getMaxRange())) {
                requiredCountTemp++;
            }


        }
        if (requiredCountTemp == requiredCount) {
            llAddToCart.setOnClickListener(this);
            llAddToCart.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color
                    .color_app_button, null));
        } else {
            llAddToCart.setOnClickListener(null);
            llAddToCart.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color
                    .color_app_gray_trans, null));
        }

        itemPriceAndSpecificationPriceTotal = itemPriceAndSpecificationPriceTotal * itemQuantity;
        reloadAmountData(itemPriceAndSpecificationPriceTotal);
    }


    private void countIsRequiredAndDefaultSelected() {
        for (Specifications specifications : this.specifications) {
            if (specifications.isRequired()) {
                requiredCount++;
            }
            for (SpecificationSubItem specificationSubItem : specifications.getList()) {
                if (specificationSubItem.isIsDefaultSelected()) {
                    specifications.setSelectedCount(specifications.getSelectedCount() + 1);
                }
            }
            specifications.setChooseMessage(getChooseMessage(specifications.getRange(),
                    specifications.getMaxRange()));
        }
    }

    private void reloadAmountData(Double itemAmount) {
        String str_curency;
        if (productItem.getCurrency() != null && !productItem.getCurrency().equals("")) {
            //str_curency=productItem.getCurrency();
            if (productItem.getCurrency().toUpperCase().contains("NULL")) {
                str_curency = productItem.getCurrency().toUpperCase().replace("NULL", "");
            } else {
                str_curency = productItem.getCurrency();
            }
        } else {
            str_curency = "";
        }

        String amount = str_curency + parseContent.decimalTwoDigitFormat
                .format(itemAmount);
        tvItemAmount.setText(amount);
    }

    private void setCartItem() {
        cartCount = 0;
        for (CartProducts cartProducts : currentBooking
                .getCartProductWithSelectedSpecificationList()) {
            cartCount = cartCount + cartProducts.getItems().size();
        }
        setToolbarCartCount(cartCount);
    }

    private void loadImage() {
        Glide.with(this).load(PreferenceHelper.getInstance(getActivity()).getIMAGE_BASE_URL() + productItem.getImageUrl().get(0)).placeholder
                (ResourcesCompat
                        .getDrawable(this
                                .getResources(), R.drawable.placeholder, null))
                .dontAnimate()
                .fallback(ResourcesCompat.getDrawable(this
                        .getResources(), R.drawable.placeholder, null)).into
                (ivProductImage);
        supportPostponeEnterTransition();
        ivProductImage.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        ivProductImage.getViewTreeObserver().removeOnPreDrawListener(this);
                        supportStartPostponedEnterTransition();
                        return true;
                    }
                }
        );
    }

    private void loadExtraData() {
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            updateItemIndex = bundle.getInt(Const.UPDATE_ITEM_INDEX);
            updateItemSectionIndex = bundle.getInt(Const.UPDATE_ITEM_INDEX_SECTION);
            productItem = bundle.getParcelable(Const
                    .PRODUCT_ITEM);
            productDetail = bundle.getParcelable(Const.PRODUCT_DETAIL);
            store = bundle.getParcelable(Const.SELECTED_STORE);
            if (isFromCartActivity()) {
                storeid = productItem.getStoreId();
                /*  Log.i("productItemstorename",currentBooking.getStorename());*/
                itemQuantity = productItem.getQuantity();
                etAddNote.setText(productItem.getInstruction());
            }
        }
    }

    /**
     * this method called webservice for clear user cart
     */
    protected void clearCart() {
        Utils.showCustomProgressDialog(this, false);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject jsonObject = getCommonParam();
        try {
            jsonObject.put(Const.Params.CART_ID, currentBooking.getCartId());
            Call<IsSuccessResponse> responseCall = apiInterface.clearCart(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        currentBooking.clearCart();
                        setCartItem();
                        addToCart();
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                ProductSpecificationActivity.this);
                    }

                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(Const.Tag.PRODUCT_SPE_ACTIVITY, t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(ProductSpecificationActivity.class.getSimpleName(), e);
        }
    }

    private void setupWindowAnimations() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            Transition transition;
            transition = TransitionInflater.from(this).inflateTransition(R.transition
                    .slide_and_changebounds);
            getWindow().setSharedElementEnterTransition(transition);
        }
    }

    public void openDialogItemImage(int position) {


        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_item_image);
        llDotsDialog = (LinearLayout) dialog.findViewById(R.id.llDotsDialog);
        imageViewPagerDialog = (ViewPager) dialog.findViewById(R.id.dialogImageViewPager);
        addBottomDotsDialog(position);
        ProductItemItemAdapter itemItemAdapter = new ProductItemItemAdapter(this, productItem
                .getImageUrl(), R.layout.item_image_full, false);
        imageViewPagerDialog.setAdapter(itemItemAdapter);
        imageViewPagerDialog.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int
                    positionOffsetPixels) {
                dotColorChangeDialog(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        imageViewPagerDialog.setCurrentItem(position);
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(params);
        dialog.show();

    }

    public void startAdsScheduled() {
        if (!isScheduleStart) {
            tripStatusSchedule = Executors.newSingleThreadScheduledExecutor();
            tripStatusSchedule.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    Message message = handler.obtainMessage();
                    handler.sendMessage(message);
                }
            }, 0, Const.ADS_SCHEDULED_SECONDS, TimeUnit
                    .SECONDS);
            AppLog.Log(ProductSpecificationActivity.class.getName(), "Schedule Start");
            isScheduleStart = true;
        }
    }

    private void initHandler() {

        handler = new Handler() {
            int position = 0;

            @Override
            public void handleMessage(Message msg) {
                if (imageViewPager.getChildCount() > 1) {
                    imageViewPager.setCurrentItem(position, true);
                    if (imageViewPager.getChildCount() == imageViewPager.getCurrentItem() + 1) {
                        position = 0;
                    } else {
                        position = imageViewPager.getCurrentItem() + 1;
                    }

                } else {
                    stopAdsScheduled();
                }
            }
        };
    }

    public void logViewedContentEvent(String contentType, String contentData, String contentId, String currency, double price) {
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT, contentData);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, price, params);
    }

    public void stopAdsScheduled() {
        if (isScheduleStart) {
            AppLog.Log(ProductSpecificationActivity.class.getName(), "Schedule Stop");
            tripStatusSchedule.shutdown(); // Disable new tasks from being submitted
            // Wait a while for existing tasks to terminate
            try {
                if (!tripStatusSchedule.awaitTermination(60, TimeUnit.SECONDS)) {
                    tripStatusSchedule.shutdownNow(); // Cancel currently executing tasks
                    // Wait a while for tasks to respond to being cancelled
                    if (!tripStatusSchedule.awaitTermination(60, TimeUnit.SECONDS))
                        AppLog.Log(ProductSpecificationActivity.class.getName(), "Pool did not " +
                                "terminate");

                }
            } catch (InterruptedException e) {
                AppLog.handleException(ProductSpecificationActivity.class.getName(), e);
                // (Re-)Cancel if current thread also interrupted
                tripStatusSchedule.shutdownNow();
                // Preserve interrupt ProviderStatus
                Thread.currentThread().interrupt();
            }
            isScheduleStart = false;
        }
    }

    private boolean isFromCartActivity() {
        return updateItemIndex > -1;
    }

    private void getCart() {
        Utils.showCustomProgressDialog(this, false);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CartResponse> orderCall = apiInterface.getCart(ApiClient.makeJSONRequestBody
                (getCommonParam()));
        orderCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                Utils.hideCustomProgressDialog();
                parseContent.parseCart(response);
                onBackPressed();
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.HOME_FRAGMENT, t);
            }
        });

    }


    /*  private Integer setTypeAsPerRange(Specifications specifications) {
          int startRange = specifications.getRange();
          int endRange = specifications.getMaxRange();
          if (startRange == 0 && endRange == 0) {
              return specifications.getType();
          } else if (startRange == 0 && endRange == 1) {
              return Const.TYPE_SPECIFICATION_SINGLE;
          } else if (startRange == 1 && endRange == 0) {
              return Const.TYPE_SPECIFICATION_SINGLE;
          } else {
              return Const.TYPE_SPECIFICATION_MULTIPLE;
          }

      }*/
    private String getChooseMessage(int startRange, int maxRange) {
        if (maxRange == 0 && startRange > 0) {
            return getResources().getString(R.string.text_choose,
                    startRange);
        } else if (startRange > 0 && maxRange > 0) {
            return getResources().getString(R.string.text_choose_to,
                    startRange, maxRange);
        } else if (startRange == 0 && maxRange > 0) {
            return getResources().getString(R.string.text_choose_up_to,
                    maxRange);
        } else {
            return "";
        }
    }

    private double getTaxableAmount(double amount, double taxValue) {
        return amount * taxValue * 0.01;
    }
}
