package com.edelivery;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.edelivery.adapter.AutoSuggestAdapter;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.models.AddressModel;
import com.edelivery.models.PostcoderModel;
import com.edelivery.models.getAddressModel;
import com.edelivery.models.hereresponce.Autosuggetionhere;
import com.edelivery.models.hereresponce.Result;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.service.LocationTrack;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.search.AutoSuggest;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.ReverseGeocodeRequest;
import com.mapbox.android.core.location.*;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.picker.PlacePicker;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import mapboxutils.MapBoxMapReady;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.edelivery.utils.Const.REQUEST_CHECK_SETTINGS;

public class AddUserLocationActivity extends BaseAppCompatActivity implements
        LocationHelper.OnLocationReceived, /*PermissionsListener,*/ OnMapReadyCallback, MapBoxMapReady {
    private LocationHelper locationHelper;
    private AutoCompleteTextView acDeliveryAddress;
    private LinearLayout llAddresslist;
    private MapboxMap mapboxMap;
    private LocationEngine locationEngine;
    private PermissionsManager permissionsManager;
    private long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    private long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
    LocationEngineRequest mLocationEngineRequest;
    private MainActivityLocationCallback callback = new MainActivityLocationCallback(this);
    private MapView mapbox;
    private boolean isCurrentLocationGet = false;
    private String geojsonSourceLayerId = "geojsonSourceLayerId";
    public static double str_Latitude = 0.0, str_Longitude = 0.0;
    private CustomFontEditTextView etAddressName, etPostCode, etAddressLine1, etAddressLine2, etCity, etInstruction;
    private AppCompatButton btnSave;
    private AppCompatEditText etMainAddress;
    private LinearLayout llAddress;

    public boolean isplaceAddress = false;

    private LatLng currentLatLng;
    private Location currentLocation;

    private boolean isCameraIdeal = true;
    private Location lastLocation;
    ArrayList<Result> results = new ArrayList<>();
    Double lattitude, longitude;
    LocationTrack locationTrack;
    String isFrom, mainAddress, postcode, line1, line2, city, instruction, addressId, param_lat, param_lng, country;


    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();

    private final static int ALL_PERMISSIONS_RESULT = 101;
    int selectedPostion = -1;

    //for here map
    //for here maps
    private LatLng deliveryLatLng;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    private AndroidXMapFragment mapFragment = null;
    private Map map = null;

    private boolean isfirstTime = false;

    private LinearLayout searchview, llMainRoot;
    private AutoSuggestAdapter m_autoSuggestAdapter;
    private RecyclerView listView;
    private EditText editsearch;
    private ImageView close;
    private List<AutoSuggest> m_autoSuggests = new ArrayList<>();
    private List<PostcoderModel> dataArrayList;
    private List<AddressModel> addressArrayList = new ArrayList<>();


    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
    }



   /* private static final LatLng BOUND_CORNER_NW = new LatLng( 55.957459,-3.206530 );
    private static final LatLng BOUND_CORNER_SE = new LatLng(51.312089, -0.561721 );
    private static final LatLngBounds RESTRICTED_BOUNDS_AREA = new LatLngBounds.Builder()
            .include(BOUND_CORNER_NW)
            .include(BOUND_CORNER_SE)
            .build();*/

    public AddUserLocationActivity() {
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    public class MainActivityLocationCallback
            implements LocationEngineCallback<LocationEngineResult> {

        private final WeakReference<AddUserLocationActivity> activityWeakReference;

        MainActivityLocationCallback(AddUserLocationActivity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(LocationEngineResult result) {
            AddUserLocationActivity activity = activityWeakReference.get();
            if (activity != null) {
                Location location = result.getLastLocation();

                if (location == null) {
                    return;
                }

                if (!isCurrentLocationGet) {
                    isCurrentLocationGet = true;
                    zoomCamera(new LatLng(location.getLatitude(), location.getLongitude()));
                }
            }
        }

        @Override
        public void onFailure(@NonNull Exception exception) {
            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
            AddUserLocationActivity activity = activityWeakReference.get();
            if (activity != null) {
                Toast.makeText(activity, exception.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void zoomCamera(LatLng latLng) {
     /*   mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                new CameraPosition.Builder()
                        .target(latLng)
                        .zoom(14)
                        .build()), 4000);*/
        deliveryLatLng = latLng;
        map.setCenter(new GeoCoordinate(latLng.getLatitude(), latLng.getLongitude()), Map.Animation.NONE);
        map.setZoomLevel(15);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*Mapbox.getInstance(getApplicationContext(),getResources().getString(R.string
                .MAP_BOX_ACCESS_TOKEN));*/
        setContentView(R.layout.activity_adduserlocation);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        /* setTitleOnToolBar(getResources().getString(R.string.text_address));*/
        setTitleOnToolBar(getResources().getString(R.string.text_add_address));
        //mapbox = findViewById(R.id.mapBoxView);
        //mapbox.onCreate(savedInstanceState);
        //checkPermissions();
        locationHelper = new LocationHelper(this);
        locationHelper.setLocationReceivedLister(this);


        acDeliveryAddress = (AutoCompleteTextView) findViewById(R.id.acDeliveryAddress);
        llAddresslist = (LinearLayout) findViewById(R.id.llAddresslist);

        ImageView ivTargetLocation = (ImageView) findViewById(R.id.ivTargetLocation);
        etAddressName = (CustomFontEditTextView) findViewById(R.id.etAddressName);

        etPostCode = (CustomFontEditTextView) findViewById(R.id.etPostCode);
        etMainAddress = (AppCompatEditText) findViewById(R.id.etMainAddress);
        etAddressLine1 = (CustomFontEditTextView) findViewById(R.id.etAddressLine1);
        etAddressLine2 = (CustomFontEditTextView) findViewById(R.id.etAddressLine2);
        etCity = (CustomFontEditTextView) findViewById(R.id.etCity);
        etInstruction = (CustomFontEditTextView) findViewById(R.id.etInstruction);
        btnSave = (AppCompatButton) findViewById(R.id.btnSave);


        llAddress = (LinearLayout) findViewById(R.id.llAddress);
        llAddress.setOnClickListener(this);

        editsearch = (EditText) findViewById(R.id.edit_query);
        listView = (RecyclerView) findViewById(R.id.list_search);
        close = (ImageView) findViewById(R.id.imgclose);
        searchview = (LinearLayout) findViewById(R.id.atuplacepickerview);
        llMainRoot = (LinearLayout) findViewById(R.id.llMainRoot);


        Bundle bundle = getIntent().getExtras();
        results = bundle.getParcelableArrayList("AddressIntentList");
        isFrom = bundle.getString("isFrom");
        Log.e("size", String.valueOf(results.size()));
        processSearchResults(results);

        if (isFrom.equals("Edit_Address")) {

            setTitleOnToolBar(getResources().getString(R.string.text_edit_address));

            llAddresslist.setVisibility(View.GONE);
            llMainRoot.setVisibility(View.VISIBLE);
            btnSave.setText(getString(R.string.update));


            mainAddress = bundle.getString("mainAddrees");
            postcode = bundle.getString("postcode");
            line1 = bundle.getString("line1");
            line2 = bundle.getString("line2");
            city = bundle.getString("city");
            instruction = bundle.getString("instruction");
            addressId = bundle.getString("addressId");
            param_lat = bundle.getString("latitude");
            param_lng = bundle.getString("longitude");
            country = bundle.getString("country");
            setAllDataAndClick(mainAddress, postcode, line1, line2, city, instruction);


        }




       /* if (i != null) {
            results = i.getParcelableArrayListExtra("AddressIntentList");

            Log.e("size", String.valueOf(results.size()));
            // processSearchResults(results);
            // callAPIgetAddress(postcode);
        }*/


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editsearch.getText().clear();
                listView.setAdapter(null);
            }
        });


        acDeliveryAddress.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        /*acDeliveryAddress.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (!acDeliveryAddress.getText().toString().isEmpty()) {
                    //doSearch(s.toString());
                    Log.e("Click_call","Click_call");
                    doSearchPushcoder(acDeliveryAddress.getText().toString());
                    searchview.setVisibility(View.VISIBLE);

                } else {
                    setSearchMode(false);
                }     // you can do anything
                return true;
            }
            return false;
        });*/

      /*  acDeliveryAddress.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                results.clear();
                acDeliveryAddress.getText().clear();
                acDeliveryAddress.requestFocus();

                searchview.setVisibility(View.VISIBLE);

                acDeliveryAddress.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        int i = s.toString().getBytes().length;
                        if (!s.toString().isEmpty()) {
                            //doSearch(s.toString());
                            doSearchPushcoder(s.toString());

                        } else {
                            setSearchMode(false);
                        }
                    }
                });
                return false;
            }
        });*/


        ImageView ivClearDeliveryAddressTextMap = (ImageView) findViewById(R.id
                .ivClearDeliveryAddressTextMap);
        CustomFontButton btnDone = (CustomFontButton) findViewById(R.id.btnDone);
        // new CustomEventMapBoxView(getApplicationContext(), mapbox, this);

        ivTargetLocation.setOnClickListener(this);
        ivClearDeliveryAddressTextMap.setOnClickListener(this);
        btnDone.setOnClickListener(this);

        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
        locationTrack = new LocationTrack(AddUserLocationActivity.this);
        if (locationTrack.canGetLocation()) {


            longitude = locationTrack.getLongitude();
            lattitude = locationTrack.getLatitude();


            // Toast.makeText(getApplicationContext(), "Longitude:" + Double.toString(longitude) + "\nLatitude:" + Double.toString(lattitude), Toast.LENGTH_SHORT).show();
        } else {

            locationTrack.showSettingsAlert(AddUserLocationActivity.this);
        }

    }


    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    public void callAPIgetAddress(String keyword) {


        Utils.showCustomProgressDialog(this, false);
        dataArrayList = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("api-key", getString(R.string.address_key));
        hashMap.put("expand", "true");


        results.clear();
        ApiInterface apiinterface = ApiClient.getaddresssuggetion().create(ApiInterface.class);
        Call<getAddressModel> call = apiinterface.getAddressAPIForLocation(keyword, hashMap);
        call.enqueue(new Callback<getAddressModel>() {
            @Override
            public void onResponse(Call<getAddressModel> call, Response<getAddressModel> response) {
                Log.i("dosearch", "ok");
                //    Toast.makeText(getApplicationContext(),String.valueOf(currentLatLng.getLatitude())+","+String.valueOf(currentLatLng.getLongitude()),Toast.LENGTH_LONG).show();
                results.clear();

                if (response.body() != null) {

                    if (response != null) {
                        if (response.body().getAddresses().size() > 0) {
                            Log.e("dosearch", ApiClient.JSONResponse(response.body().toString()));

                            addressArrayList.clear();
                            addressArrayList = response.body().getAddresses();

                            Log.i("dosearch", ApiClient.JSONResponse(response.body()));

                            for (int i = 0; i < addressArrayList.size(); i++) {

                                Result result = new Result();

                                //add in result list

                                String desc = "";

                                if (!addressArrayList.get(i).getLine1().equals("")) {


                                    ArrayList<String> listNew = new ArrayList<>();
                                    listNew.clear();

                                    listNew.add(addressArrayList.get(i).getLine1());
                                    listNew.add(addressArrayList.get(i).getLine2());
                                    listNew.add(addressArrayList.get(i).getLine3());
                                    listNew.add(addressArrayList.get(i).getLine4());
                                    listNew.add(addressArrayList.get(i).getLocality());
                                    listNew.add(addressArrayList.get(i).getTownOrCity());
                                    listNew.add(response.body().getPostcode());
                                    listNew.add(addressArrayList.get(i).getCounty());
                                    listNew.add(addressArrayList.get(i).getDistrict());
                                    listNew.add(addressArrayList.get(i).getCountry());


                                   /* ArrayList<String> filter = new ArrayList();
                                    filter.clear();*/

                                    for (int j = 0; j < listNew.size(); j++) {
                                        if (!listNew.get(j).equals("")) {
                                            if (j == 0) {
                                                desc = listNew.get(j);
                                            } else {
                                                desc = desc + "," + listNew.get(j);
                                            }
                                        }

                                    }

                                   /* desc = addressArrayList.get(i).getLine1() + ("," + addressArrayList.get(i).getLine2().trim() + ",").replace(",,", ",") + ("," + addressArrayList.get(i).getLine3() + ",").replace(",,", ",")
                                            + (addressArrayList.get(i).getLine4() + ",").replace(",,", ",") + (addressArrayList.get(i).getLocality() + ",").replace(",,", ",") + (addressArrayList.get(i).getTownOrCity() + ",").replace(",,", ",") + (response.body().getPostcode() + ",").replace(",,", ",") + (addressArrayList.get(i).getCounty() + ",").replace(",,", ",") + (addressArrayList.get(i).getDistrict() + ",").replace(",,", ",") + addressArrayList.get(i).getCountry();
*/
                                    /* desc = desc.replaceAll(",,", ",");*/
                                }

                                result.setTitle("");
                                result.setCountry(addressArrayList.get(i).getCountry());
                                result.setCity(addressArrayList.get(i).getTownOrCity());
                                result.setPost_code(response.body().getPostcode());
                                result.setAddress_1(addressArrayList.get(i).getLine1());
                                result.setAddress_2(addressArrayList.get(i).getLine2());
                                result.setLat(response.body().getLatitude());
                                result.setLng(response.body().getLongitude());
                                result.setAdress(desc);

                                results.add(result);


                                //Toast.makeText(getApplicationContext(),finaladress,Toast.LENGTH_LONG).show();


                            }
                            processSearchResults(results);
                            Utils.hideCustomProgressDialog();

                        } else {
                            Utils.hideCustomProgressDialog();
                            Utils.showToast(getString(R.string.no_data_found), AddUserLocationActivity.this);
                        }
                    } else {
                        Utils.hideCustomProgressDialog();
                        Utils.showToast(getString(R.string.server_error), AddUserLocationActivity.this);
                        // Utils.showToast(response.message(), AddUserLocationActivity.this);
                    }


                } else {
                    Utils.hideCustomProgressDialog();
                    Utils.showToast(getString(R.string.server_error), AddUserLocationActivity.this);
                    //Utils.showToast(response.message(), AddUserLocationActivity.this);
                }

            }

            @Override
            public void onFailure(Call<getAddressModel> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                Utils.showToast(getString(R.string.server_error), AddUserLocationActivity.this);

                /* Utils.showErrorToast(response.body().getErrorCode(), AddUserLocationActivity.this);*/
                // Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_LONG).show();
                Log.i("processSearchResults", t.toString());
            }
        });

    }


    /* @TargetApi(Build.VERSION_CODES.M)
     @Override
     public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

         switch (requestCode) {

             case ALL_PERMISSIONS_RESULT:
                 for (String perms : permissionsToRequest) {
                     if (!hasPermission(perms)) {
                         permissionsRejected.add(perms);
                     }
                 }

                 if (permissionsRejected.size() > 0) {


                     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                         if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                             showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                     new DialogInterface.OnClickListener() {
                                         @Override
                                         public void onClick(DialogInterface dialog, int which) {
                                             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                 requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                             }
                                         }
                                     });
                             return;
                         }
                     }

                 }

                 break;
         }

     }
 */
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new androidx.appcompat.app.AlertDialog.Builder(AddUserLocationActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();
        locationHelper.onStart();
        //mapbox.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        locationHelper.onStop();
        //mapbox.onStop();

    }

    @Override
    protected void onPause() {
        super.onPause();
        //mapbox.onPause();


    }

    @Override
    protected void onDestroy() {

        if (locationEngine != null) {
            locationEngine.removeLocationUpdates(callback);
        }
        //mapbox.onDestroy();
        super.onDestroy();
        locationTrack.stopListener();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {

    }

    @Override
    protected void setViewListener() {

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();

    }


 /*   @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnected(Bundle bundle) {
        checkLocationPermission(null);
    }
*/

    @Override
    public void onLocationChanged(Location location) {
        //do something
        AppLog.Log(Const.Tag.HOME_FRAGMENT, "GoogleClientLocationChanged");
        currentLocation = location;
        currentLatLng = new LatLng(location.getLatitude(),
                location.getLongitude());
        lastLocation = currentLocation;

    }

    @Override
    public void onConnected(Bundle bundlLe) {
        AppLog.Log(Const.Tag.HOME_FRAGMENT, "GoogleClientConnected");
        lastLocation = locationHelper.getLastLocation();
        locationHelper.startLocationUpdate();
        //checkLocationPermission(false, null);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //do something
    }

    @Override
    public void onConnectionSuspended(int i) {
        //do something
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.etMainAddress:
                ShowAddressListView();

                break;

            case R.id.llAddress:
               /* if (!acDeliveryAddress.getText().toString().isEmpty()) {
                    //doSearch(s.toString());
                    Log.e("Click_call", "Click_call");
                    doSearchPushcoder(acDeliveryAddress.getText().toString());
                    searchview.setVisibility(View.VISIBLE);

                } else {
                    setSearchMode(false);
                }     // you can do anything
*/
                /*doSearchPushcoder(acDeliveryAddress.getText().toString());*/
                break;
            case R.id.ivTargetLocation:
                /*isCurrentLocationGet = false;
                if (!locationHelper.isOpenGpsDialog())
                {
                    locationHelper.setOpenGpsDialog(true);
                    locationHelper.setLocationSettingRequest(this, REQUEST_CHECK_SETTINGS, new
                            OnSuccessListener() {
                                @Override
                                public void onSuccess(Object o) {
                                    checkLocationPermission(null);
                                }
                            }, new LocationHelper.NoGPSDeviceFoundListener() {
                        @Override
                        public void noFound() {
                            checkLocationPermission(null);
                        }
                    });
                }*/

                moveCameraFirstMyLocationZoom();


                break;
            case R.id.ivClearDeliveryAddressTextMap:
                listView.setAdapter(null);
                acDeliveryAddress.setText("");
                searchview.setVisibility(View.GONE);
                Utils.hideSoftKeyboard(AddUserLocationActivity.this);
                break;
            case R.id.btnDone:
                    /*Intent intent = new Intent();
                    intent.putExtra(Const.LATITUDE, storeLatLng.getLatitude());
                    intent.putExtra(Const.LONGITUDE, storeLatLng.getLongitude());
                    intent.putExtra(Const.ADDRESS_MAPBOX, acDeliveryAddress.getText().toString());
                    setResult(Activity.RESULT_OK, intent);
                    finish();*/
                if (acDeliveryAddress.getText().toString().isEmpty()) {
                    Utils.showToast(getResources().getString(R.string.text_select_location), this);
                } else if (etAddressName.getText().toString().isEmpty()) {
                    //  Utils.showToast(getResources().getString(R.string.text_enter_location), this);
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.custom_toast,
                            (ViewGroup) findViewById(R.id.custom_toast_container));

                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setWidth(2500);
                    text.setText(getResources().getString(R.string.text_enter_location));
                    //  text.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);

                    toast.show();

                } else {
                    // addFavouriteAddress();
                }
                break;
            case R.id.btnSave:
                if (isFrom.equals("Add_Address")) {
                    addFavouriteAddressUsingGetAddrress();
                } else {
                    // for isFrom = "Edit_Address
                    EditFavouriteAddressUsingGetAddrress();
                }

                break;


            case R.id.acDeliveryAddress:
                  /*  results.clear();
                    acDeliveryAddress.getText().clear();
                    searchview.setVisibility(View.VISIBLE);
                    acDeliveryAddress.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            int i=s.toString().getBytes().length;
                            if (!s.toString().isEmpty()) {
                                doSearch(s.toString());

                            } else {
                                setSearchMode(false);
                            }
                        }
                    });
*/

              /*  Intent intent = new PlaceAutocomplete.IntentBuilder()

                        .accessToken(getResources().getString(R.string
                                .MAP_BOX_ACCESS_TOKEN))
                        .placeOptions(PlaceOptions.builder().backgroundColor(Color.parseColor("#EEEEEE")).country("gb").limit(10).language("en")
                                .build())
                        .build(this);
                startActivityForResult(intent, Const.REQUEST_CODE_AUTOCOMPLETE);*/
/*

                searchview.setVisibility(View.VISIBLE);
                editsearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (!s.toString().isEmpty()) {
                            doSearch(s.toString());

                        } else {
                           setSearchMode(false);
                        }


                    }
                });


*/


            default:
                break;
        }
    }

    private void EditFavouriteAddressUsingGetAddrress() {

        Utils.showCustomProgressDialog(this, false);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.LATITUDE, param_lat);
            jsonObject.put(Const.Params.LONGITUDE, param_lng);
            jsonObject.put(Const.Params.ADDRESS, etMainAddress.getText().toString());
            jsonObject.put(Const.Params.ADDRESS_NAME, "");
            jsonObject.put(Const.Params.CITY, etCity.getText().toString());
            jsonObject.put(Const.Params.POSTCODE, postcode);
            jsonObject.put(Const.Params.COUNTRY, country);
            jsonObject.put(Const.Params.ADDRESS_1, etAddressLine1.getText().toString());
            jsonObject.put(Const.Params.ADDRESS_2, etAddressLine2.getText().toString());
            jsonObject.put(Const.Params.PARAM_ADDRESS_ID, addressId);
            jsonObject.put(Const.Params.NOTE, etInstruction.getText().toString());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }


        Log.i("addFavouriteAddress", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.EditFavouriteAddress(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {

                    setResult(Activity.RESULT_OK);
                    finish();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), AddUserLocationActivity.this);
                }
            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
            }
        });

    }

    private void ShowAddressListView() {
        llMainRoot.setVisibility(View.GONE);
        llAddresslist.setVisibility(View.VISIBLE);
    }

    private void setSearchMode(boolean b) {

    }

    public void moveCameraFirstMyLocationZoom() {
        try {
            if (locationHelper != null && locationHelper.getLastLocation() != null) {
                currentLocation = locationHelper.getLastLocation();
                if (currentLocation != null) {
                    currentLatLng = new LatLng(currentLocation.getLatitude(),
                            currentLocation.getLongitude());
//                    isCameraIdeal = false;

                    Log.i("Deliverylocation", "moveCameraFirstMyLocationZoom " + currentLocation.getLatitude() + currentLocation.getLongitude());
                    //zoomCamera(currentLatLng, "");
                    deliveryLatLng = currentLatLng;
                    map.setCenter(new GeoCoordinate(currentLatLng.getLatitude(), currentLatLng.getLongitude()), Map.Animation.NONE);
                    map.setZoomLevel(15);
                    Log.i("Deliverylocation", "zoomCamera");

                    GeoCoordinate geoCoordinate = map.getCenter();
                    str_Latitude = geoCoordinate.getLatitude();
                    str_Longitude = geoCoordinate.getLongitude();
                    /*acDeliveryAddress.setText(currentBooking.getCurrentAddress());*/

                    //setListViewData(currentLatLng, currentBooking.getCurrentAddress());

                    isCameraIdeal = true;
                }

                locationHelper.setOpenGpsDialog(false);
            } else {
                if (currentBooking != null && currentBooking.getCurrentLatLng() != null &&
                        currentBooking.getCurrentLatLng().latitude != 0.0 && currentBooking.getCurrentLatLng().longitude != 0.0) {
                    deliveryLatLng = new LatLng(currentBooking.getCurrentLatLng().latitude, currentBooking.getCurrentLatLng().longitude);
                    map.setCenter(new GeoCoordinate(currentBooking.getCurrentLatLng().latitude, currentBooking.getCurrentLatLng().longitude), Map.Animation.NONE);
                    map.setZoomLevel(15);
                    GeoCoordinate geoCoordinate = map.getCenter();
                    str_Latitude = geoCoordinate.getLatitude();
                    str_Longitude = geoCoordinate.getLongitude();
                    /*acDeliveryAddress.setText(currentBooking.getCurrentAddress());*/
                    //  setListViewData(new LatLng(currentBooking.getCurrentLatLng().latitude,currentBooking.getCurrentLatLng().latitude), currentBooking.getCurrentAddress());
                    Log.i("Deliverylocation", "moveCameraFirstMyLocationZoom with if " + currentBooking.getCurrentLatLng());
                } else {
                    Log.i("Deliverylocation", "moveCameraFirstMyLocationZoom with else " + currentBooking.getCurrentLatLng());
                    deliveryLatLng = new LatLng(ParseContent.getInstance().latLng.getLatitude(), ParseContent.getInstance().latLng.getLongitude());
                    map.setCenter(new GeoCoordinate(ParseContent.getInstance().latLng.getLatitude(), ParseContent.getInstance().latLng.getLongitude()), Map.Animation.NONE);
                    map.setZoomLevel(15);
                    GeoCoordinate geoCoordinate = map.getCenter();
                    str_Latitude = geoCoordinate.getLatitude();
                    str_Longitude = geoCoordinate.getLongitude();
                    /*  acDeliveryAddress.setText(currentBooking.getCurrentAddress());*/
                    // setListViewData(ParseContent.getInstance().latLng, currentBooking.getCurrentAddress());
                }
            }
        } catch (Exception e) {
            Log.i("Deliverylocation", e.getMessage());
            Log.e("moveCameraFirstMyLocation() " , e.getMessage());
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        checkLocationPermission(null);
                        break;
                    case Activity.RESULT_CANCELED:
                        locationHelper.setOpenGpsDialog(false);
                        break;
                    default:
                        break;
                }
                break;
            case Const.REQUEST_CODE_AUTOCOMPLETE:
                if (data != null) {


                    CarmenFeature feature = PlaceAutocomplete.getPlace(data);
                    CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);

                    if (feature.placeName() != null && !feature.placeName().equals("")) {
                        /* acDeliveryAddress.setText(feature.placeName());*/
                    }
                    if (mapboxMap != null) {
                        Style style = mapboxMap.getStyle();

                        if (style != null) {
                            GeoJsonSource source = style.getSourceAs(geojsonSourceLayerId);
                            if (source != null) {
                                source.setGeoJson(FeatureCollection.fromFeatures(
                                        new Feature[]{Feature.fromJson(selectedCarmenFeature.toJson())}));
                            }
                            isplaceAddress = true;
                            /*  acDeliveryAddress.setText(selectedCarmenFeature.placeName());*/

                            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                                    new CameraPosition.Builder()
                                            .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                                    ((Point) selectedCarmenFeature.geometry()).longitude()))
                                            .zoom(14)
                                            .build()), 4000);

                        }
                    }

                }
                break;
            case Const.REQUEST_CODE:
                if (data != null) {
                    CarmenFeature carmenFeature = PlacePicker.getPlace(data);
                    if (carmenFeature.placeName() != null && !carmenFeature.placeName().equals("")) {
                        /* acDeliveryAddress.setText(carmenFeature.placeName());*/
                    }
                }
        }
    }


    @Override
    public void mapBoxMapReady(final MapboxMap mapboxMap, Style style) {
        /*this.mapboxMap = mapboxMap;

        enableLocationComponent(style);
        mapboxMap.getUiSettings().setAttributionEnabled(false);
        mapboxMap.getUiSettings().setLogoEnabled(false);
        mapboxMap.getUiSettings().setCompassEnabled(false);

        mapboxMap.addOnCameraIdleListener(new MapboxMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {



                LatLng mLatLng = mapboxMap.getCameraPosition().target;
               // mapboxMap.setLatLngBoundsForCameraTarget(RESTRICTED_BOUNDS_AREA);

                if (mLatLng != null)
                {
                    Geocoder geocoder = new Geocoder(AddUserLocationActivity.this, Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(
                                mLatLng.getLatitude(),
                                mLatLng.getLongitude(),
                                1);
                        if (addresses != null && addresses.size() > 0 && addresses.get(0) != null)
                        {
                            //Log.e("tagMap", "onResponse: " + addresses.get(0).toString());
                            if (addresses.get(0).getCountryName() != null) {
                                //Log.e("tagMap", "onResponse: " + addresses.get(0).getCountryName());
                                if((addresses.get(0).getAddressLine(0)!=null && !(addresses.get(0).getAddressLine(0).equals(""))))
                                {
                                    if (isplaceAddress)
                                    {
                                        isplaceAddress = false;

                                    }else{
                                        isplaceAddress = false;
                                        acDeliveryAddress.setText(addresses.get(0).getAddressLine(0));


                                    }


                                }
                                str_Latitude=addresses.get(0).getLatitude();
                                str_Longitude=addresses.get(0).getLongitude();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
*/
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            initLocationEngine();
        } else {
           /* permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);*/
        }
    }


    @SuppressLint("MissingPermission")
    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(this);

        mLocationEngineRequest = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();

        locationEngine.requestLocationUpdates(mLocationEngineRequest, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }

    public void moveCameraFirstMyLocation(boolean isAnimate, LatLng latLng) {
        LatLng latLngOfMyLocation = null;
        if (latLng == null) {


        } else {
            latLngOfMyLocation = latLng;
        }
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLngOfMyLocation).zoom(14).build();

        locationHelper.setOpenGpsDialog(false);
    }

   /* private void getGeocodeDataFromAddress(final String address) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Const.ADDRESS_MAPBOX, address);
        hashMap.put(Const.google.KEY, preferenceHelper.getGoogleKey());
        Utils.showCustomProgressDialog(this, false);
        ApiInterface apiInterface = new ApiClient().changeApiBaseUrl(Const
                .GOOGLE_API_URL).create
                (ApiInterface.class);
        Call<ResponseBody> bodyCall = apiInterface.getGoogleGeocode(hashMap);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                Utils.hideCustomProgressDialog();
                HashMap<String, String> map = parseContent.parsGoogleGeocode(response);
                if (map != null) {
                    storeLatLng = new LatLng(Double.valueOf(map.get(Const
                            .google.LAT)), Double.valueOf(map.get(Const
                            .google.LNG)));
                    moveCameraFirstMyLocation(true, storeLatLng);

                    city = TextUtils.isEmpty(map.get(Const.google.LOCALITY)) ? map.get(Const.google.ADMINISTRATIVE_AREA_LEVEL_1) : map.get(Const.google.LOCALITY);

                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });


    }*/

    @Override
    public void onBackPressed() {

        /*if (searchview.getVisibility() == View.VISIBLE) {
            searchview.setVisibility(View.GONE);
        } else {*/
        setResult(Activity.RESULT_CANCELED);
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        /*}*/

    }


    @Override
    protected void onResume() {
        super.onResume();
        //  mapbox.onResume();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //mapbox.onSaveInstanceState(outState);
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        // mapbox.onLowMemory();
    }


    private void checkLocationPermission(LatLng latLng) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission
                            .ACCESS_FINE_LOCATION, android.Manifest.permission
                            .ACCESS_COARSE_LOCATION},
                    Const.PERMISSION_FOR_LOCATION);
        } else {
            moveCameraFirstMyLocation(true, latLng);
        }
    }

   /* @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_LOCATION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        moveCameraFirstMyLocation(true, null);
                    }
                    break;

                case REQUEST_CODE_ASK_PERMISSIONS:
                    for (int index = permissions.length - 1; index >= 0; --index) {
                        if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                            // exit the app if one permission is not granted
                            Toast.makeText(this, "Required permission '" + permissions[index]
                                    + "' not granted, exiting", Toast.LENGTH_LONG).show();
                            finish();
                            return;
                        }
                    }
                    // all permissions were granted
                    // initialize();

                    setheremap();
                    break;
                case ALL_PERMISSIONS_RESULT:
                    for (String perms : permissionsToRequest) {
                        if (!hasPermission(perms)) {
                            permissionsRejected.add(perms);
                        }
                    }

                    if (permissionsRejected.size() > 0) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                                showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }

                    break;
                default:
                    break;
            }
        }

    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
       *//* if (granted) {
            if (mapboxMap.getStyle() != null) {
                enableLocationComponent(mapboxMap.getStyle());
            }
        }*//*
    }*/


    private void addFavouriteAddressUsingGetAddrress() {
        Utils.showCustomProgressDialog(this, false);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.LATITUDE, param_lat);
            jsonObject.put(Const.Params.LONGITUDE, param_lng);
            jsonObject.put(Const.Params.ADDRESS, etMainAddress.getText().toString());
            jsonObject.put(Const.Params.ADDRESS_NAME, "");
            jsonObject.put(Const.Params.CITY, etCity.getText().toString());
            jsonObject.put(Const.Params.POSTCODE, postcode);
            jsonObject.put(Const.Params.COUNTRY, country);
            jsonObject.put(Const.Params.ADDRESS_1, etAddressLine1.getText().toString());
            jsonObject.put(Const.Params.ADDRESS_2, etAddressLine2.getText().toString());
            jsonObject.put(Const.Params.NOTE, etInstruction.getText().toString());

        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }


        Log.i("addFavouriteAddress", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.AddFavouriteAddress(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    setResult(Activity.RESULT_OK);
                    finish();
                } else {
                    try {
                        if (null != response.body()) {
                            Utils.showErrorToast(response.body().getErrorCode(), AddUserLocationActivity.this);
                        }

                    } catch (Exception e) {

                    }

                }
            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
            }
        });
    }

    // Call API
    private void addFavouriteAddress() {
        Utils.showCustomProgressDialog(this, false);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.LATITUDE, str_Latitude);
            jsonObject.put(Const.Params.LONGITUDE, str_Longitude);
            jsonObject.put(Const.Params.ADDRESS, acDeliveryAddress.getText().toString());
            jsonObject.put(Const.Params.ADDRESS_NAME, etAddressName.getText().toString());
            jsonObject.put(Const.Params.CITY, city);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }


        Log.i("addFavouriteAddress", jsonObject.toString());
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.AddFavouriteAddress(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    setResult(Activity.RESULT_OK);
                    finish();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), AddUserLocationActivity.this);
                }
            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
            }
        });
    }


   /* ////for here map

    //for here map
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS, grantResults);
        }
    }*/


    public void setheremap() {

        mapFragment = getMapFragment();
        Utils.showCustomProgressDialog(AddUserLocationActivity.this, false);
        // Set up disk cache path for the map service for this application
        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(
                getApplicationContext().getExternalFilesDir(null) + File.separator + ".here-maps",
                "com.here.android.mpa.service.MapService.v3");
       /* if (!success) {
            Toast.makeText(getApplicationContext(), "Unable to set isolated disk cache path.", Toast.LENGTH_LONG);
        } else {*/
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(
                    final Error error) {
                if (error == Error.NONE) {
                    // retrieve a reference of the map from the map fragment


                    map = mapFragment.getMap();

                    // Set the zoom level to the average between min and max
                    map.setZoomLevel((map.getMaxZoomLevel() + map.getMinZoomLevel()) / 1);
                    currentLocation = locationHelper.getLastLocation();
                    Utils.hideCustomProgressDialog();

                    moveCameraFirstMyLocationZoom();

                    mapFragment.getMapGesture()
                            .addOnGestureListener(new MapGesture.OnGestureListener() {

                                @Override
                                public void onPanStart() {
                                    Log.e("tagMap", "onPanStart()");
                                    //  getCenterMapLocation();
                                }

                                @Override
                                public void onPanEnd() {
                                    Log.e("tagMap", "onPanEnd()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public void onMultiFingerManipulationStart() {
                                    Log.e("tagMap", "onMultiFingerManipulationStart()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public void onMultiFingerManipulationEnd() {
                                    Log.e("tagMap", "onMultiFingerManipulationEnd()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public boolean onMapObjectsSelected(List<ViewObject> list) {
                                    Log.e("tagMap", "onMapObjectsSelected()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public boolean onTapEvent(PointF pointF) {
                                    Log.e("tagMap", "onTapEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public boolean onDoubleTapEvent(PointF pointF) {
                                    Log.e("tagMap", "onDoubleTapEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public void onPinchLocked() {
                                    Log.e("tagMap", "onPinchLocked()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public boolean onPinchZoomEvent(float v, PointF pointF) {
                                    Log.e("tagMap", "onPinchZoomEvent()");
                                    //  getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public void onRotateLocked() {
                                    Log.e("tagMap", "onRotateLocked()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public boolean onRotateEvent(float v) {
                                    Log.e("tagMap", "onRotateEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public boolean onTiltEvent(float v) {
                                    Log.e("tagMap", "onTiltEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public boolean onLongPressEvent(PointF pointF) {
                                    Log.e("tagMap", "onLongPressEvent()");
                                    // getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public void onLongPressRelease() {
                                    Log.e("tagMap", "onLongPressRelease()");
                                    // getCenterMapLocation();
                                }

                                @Override
                                public boolean onTwoFingerTapEvent(PointF pointF) {
                                    Log.e("tagMap", "onTwoFingerTapEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }
                            }, 0, false);
                    // getCenterMapLocation();

                } else {
                    System.out.println("ERROR: Cannot initialize Map Fragment");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(AddUserLocationActivity.this).setMessage(
                                    "Error : " + error.name() + "\n\n" + error.getDetails())
                                    .setTitle(R.string.engine_init_error)
                                    .setNegativeButton(android.R.string.cancel,
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    finishAffinity();
                                                }
                                            }).create().show();
                        }
                    });
                }
            }
        });
        // }


    }

    private void getCenterMapLocation() {
        if (map != null) {

            //  map.setCenter(new GeoCoordinate(currentLatLng.getLatitude(),currentLatLng.getLongitude()),Map.Animation.LINEAR);
            GeoCoordinate coordinate;
            coordinate = map.getCenter();
            ReverseGeocodeRequest revGecodeRequest = new ReverseGeocodeRequest(coordinate);

            revGecodeRequest.execute(new ResultListener<com.here.android.mpa.search.Location>() {
                @Override
                public void onCompleted(com.here.android.mpa.search.Location location, ErrorCode errorCode) {
                    if (errorCode == ErrorCode.NONE) {

//                        etSearch.setText(location.getAddress().toString().replace("\n", " "));
                        String myPlace = "";
                        if (location != null && location.getAddress() != null) {
                            if (!TextUtils.isEmpty(location.getAddress().getText())) {
                                myPlace = location.getAddress().getText();
                                str_Latitude = location.getCoordinate().getLatitude();
                                str_Longitude = location.getCoordinate().getLongitude();
                            }
                        }
                        // GeoCoordinate geoCoordinate=map.getCenter();
                      /*  str_Latitude=location.getCoordinate().getLatitude();
                        str_Longitude=location.getCoordinate().getLongitude();*/
                        /* acDeliveryAddress.setText(myPlace);*/

                        LatLng latLng = new LatLng(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude());
                       /* getGeocodeDataFromAddress(location.getAddress().getCountryName(), location.getAddress().getCountryCode(),
                                location.getAddress().getCity(), "", "", latLng,
                                location.getAddress().toString().replace("\\n", ","), location.getAddress().getCity());
                 */
                    } else {


                        currentBooking.setDeliveryAddress("error code:" + errorCode);
                        //  setDeliveryAddress("error code:" + errorCode);

                    }
                }
            });
        }
    }

    public void setListViewData(LatLng latLng, String adress, int pos) {
        //  deliveryLatLng=latLng;

        if (searchview.getVisibility() == View.VISIBLE) {
            Utils.hideSoftKeyboard(AddUserLocationActivity.this);
            editsearch.getText().clear();
            listView.setAdapter(null);
            searchview.setVisibility(View.GONE);

        }   //

        Log.i("Deliverylocation", "zoomCamera" + latLng.getLatitude() + latLng.getLongitude());
        map.setCenter(new GeoCoordinate(latLng.getLatitude(), latLng.getLongitude()), Map.Animation.NONE);
        map.setZoomLevel(15);
        Log.i("Deliverylocation", "zoomCamera");

        GeoCoordinate geoCoordinate = map.getCenter();

        if (!adress.isEmpty()) {
            acDeliveryAddress.setText(adress);
            str_Latitude = geoCoordinate.getLatitude();
            str_Longitude = geoCoordinate.getLongitude();
            Log.i("adressfrom adapter", adress);
        }
        GeoCoordinate coordinate;
        coordinate = map.getCenter();
        ReverseGeocodeRequest revGecodeRequest = new ReverseGeocodeRequest(coordinate);

        revGecodeRequest.execute(new ResultListener<com.here.android.mpa.search.Location>() {
            @Override
            public void onCompleted(com.here.android.mpa.search.Location location, ErrorCode errorCode) {
                if (errorCode == ErrorCode.NONE) {

//                        etSearch.setText(location.getAddress().toString().replace("\n", " "));
                    String myPlace = "";
                    if (location != null && location.getAddress() != null) {
                        if (!TextUtils.isEmpty(location.getAddress().getText())) {
                            myPlace = location.getAddress().getText();
                            str_Latitude = location.getCoordinate().getLatitude();
                            str_Longitude = location.getCoordinate().getLongitude();
                        }
                    }
                    if (isfirstTime) {
                        /* acDeliveryAddress.setText(myPlace);*/
                        isfirstTime = false;
                    }

                    LatLng latLng = new LatLng(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude());
                   /* getGeocodeDataFromAddress(location.getAddress().getCountryName(), location.getAddress().getCountryCode(),
                            location.getAddress().getCity(), "", "", latLng,
                            location.getAddress().toString().replace("\\n", ","), location.getAddress().getCity());
               */
                } else {


                    currentBooking.setDeliveryAddress("error code:" + errorCode);
                    // setDeliveryAddress("error code:" + errorCode);

                }
            }
        });
    }


    public void setListViewDataGetAddress(int pos) {

        selectedPostion = pos;

        postcode = results.get(pos).getPost_code();
        param_lat = results.get(pos).getLat();
        param_lng = results.get(pos).getLng();
        country = results.get(pos).getCountry();


        llMainRoot.setVisibility(View.VISIBLE);
        llAddresslist.setVisibility(View.GONE);

        setAllDataAndClick(results.get(pos).getAdress(), results.get(pos).getPost_code(), results.get(pos).getAddress_1(), results.get(pos).getAddress_2(), results.get(pos).getCity(), "");

    }

    private void setAllDataAndClick(String mainAddress, String postcode, String line1, String line2, String city, String instruction) {
        etPostCode.setEnabled(false);
        etPostCode.setKeyListener(null);

        etMainAddress.setClickable(true);
        etMainAddress.setKeyListener(null);
        etMainAddress.setOnClickListener(this);

        etAddressLine1.setEnabled(false);
        etAddressLine1.setKeyListener(null);

        etCity.setEnabled(false);
        etCity.setKeyListener(null);

        etMainAddress.setClickable(true);
        etMainAddress.setKeyListener(null);


        etMainAddress.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ShowAddressListView();
                return false;
            }
        });

        etPostCode.setText(getString(R.string.your_postcode_is) + "" + postcode);
        etMainAddress.setText(mainAddress);
        etAddressLine1.setText(line1);
        etAddressLine2.setText(line2);
        etCity.setText(city);
        etInstruction.setText(instruction);
    }


    private void doSearchPushcoder(String query) {
        Utils.showCustomProgressDialog(this, false);
        dataArrayList = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("lines", "3");
        hashMap.put("page", "0");
        hashMap.put("addtags", "latitude,longitude");
        hashMap.put("identifier", "iOS_Swift_Example_Code");

        results.clear();
        ApiInterface apiinterface = ApiClient.getautosuggetion().create(ApiInterface.class);
        Call<List<PostcoderModel>> call = apiinterface.getautosuggetionsPushCode(query, hashMap);
        call.enqueue(new Callback<List<PostcoderModel>>() {
            @Override
            public void onResponse(Call<List<PostcoderModel>> call, Response<List<PostcoderModel>> response) {
                Log.i("dosearch", "ok");
                //    Toast.makeText(getApplicationContext(),String.valueOf(currentLatLng.getLatitude())+","+String.valueOf(currentLatLng.getLongitude()),Toast.LENGTH_LONG).show();
                results.clear();

                if (response.body() != null) {

                    if (response != null) {
                        if (response.body().size() > 0) {
                            Log.e("dosearch", String.valueOf(dataArrayList.size()) + String.valueOf(dataArrayList));
                            Log.e("dosearch", ApiClient.JSONResponse(response.body().toString()));


                            dataArrayList = response.body();
                            String finaladress = "", housenumbur = "", street = "", district = "", city = "", postalCode = "", countryName = "", district2 = "";

                            Log.i("dosearch", ApiClient.JSONResponse(response.body()));

                            for (int i = 0; i < dataArrayList.size(); i++) {
                                finaladress = "";
                                Result result = new Result();
                                if (dataArrayList.get(i).getAddressline1() != null) {
                                    //add in result list

                                    result.setTitle(dataArrayList.get(i).getAddressline1());
                                    result.setAdress(dataArrayList.get(i).getSummaryline());

                                    results.add(result);


/*

                            //*housenumbur = response.body().getItems().get(i).getAddress().getHouseNumber();
                            street = dataArrayList.get(i).getStreet();
                            district = dataArrayList.get(i).getPosttown();
                            //city = response.body().getItems().get(i).getAddress().getCity();
                            postalCode = dataArrayList.get(i).getPostcode();//*
                            countryName = dataArrayList.get(i).getCounty();


                            if (housenumbur != null) {
                                finaladress = housenumbur + " ";
                            }
                            if (street != null) {
                                if (street.matches("[0-9]+")) {
                                    finaladress = finaladress + street + " ";
                                } else {
                                    finaladress = finaladress + street + ", ";
                                }

                            }
                            if (district != null) {
                                district2 = district;
                                finaladress = finaladress + district + ", ";
                            }
                            if (city != null && !city.matches(district2)) {
                                finaladress = finaladress + city + ", ";
                            }
                            if (postalCode != null) {
                                finaladress = finaladress + postalCode + ", ";
                            }
                            if (countryName != null) {
                                finaladress = finaladress + countryName;
                            }
*/


                                }

                                Log.i("adresssshow", finaladress);
                                //Toast.makeText(getApplicationContext(),finaladress,Toast.LENGTH_LONG).show();


                            }
                            processSearchResults(results);

                        } else {
                            Utils.showToast(getString(R.string.no_data_found), AddUserLocationActivity.this);
                        }
                    } else {
                        Utils.showToast(getString(R.string.server_error), AddUserLocationActivity.this);
                        // Utils.showToast(response.message(), AddUserLocationActivity.this);
                    }


                } else {
                    Utils.showToast(getString(R.string.server_error), AddUserLocationActivity.this);
                    //Utils.showToast(response.message(), AddUserLocationActivity.this);
                }
                Utils.hideCustomProgressDialog();
            }

            @Override
            public void onFailure(Call<List<PostcoderModel>> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                Utils.showToast(getString(R.string.server_error), AddUserLocationActivity.this);

                /* Utils.showErrorToast(response.body().getErrorCode(), AddUserLocationActivity.this);*/
                // Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_LONG).show();
                Log.i("processSearchResults", t.toString());
            }
        });


    }


    //heremap autosugestion
    private void doSearch(String query) {
        // Toast.makeText(getApplicationContext(),"ok",Toast.LENGTH_LONG).show();
        // Toast.makeText(getApplicationContext(),String.valueOf(currentLatLng.getLatitude())+","+String.valueOf(currentLatLng.getLongitude()),Toast.LENGTH_LONG).show();
        results.clear();
        ApiInterface apiinterface = ApiClient.getautosuggetion().create(ApiInterface.class);
        Call<Autosuggetionhere> call = apiinterface.getautosuggetions(String.valueOf(lattitude) + "," + String.valueOf(longitude), Const.Hereapi.size, query,/*Const.resultTypes,*/Const.Hereapi.apikey);
        call.enqueue(new Callback<Autosuggetionhere>() {
            @Override
            public void onResponse(Call<Autosuggetionhere> call, Response<Autosuggetionhere> response) {

                //    Toast.makeText(getApplicationContext(),String.valueOf(currentLatLng.getLatitude())+","+String.valueOf(currentLatLng.getLongitude()),Toast.LENGTH_LONG).show();
                results.clear();

                if (response.body().getItems() != null) {
                    String finaladress = "", housenumbur = "", street = "", district = "", city = "", postalCode = "", countryName = "", district2 = "";

                    Log.i("dosearch", ApiClient.JSONResponse(response.body()));
                    //  Toast.makeText(getApplicationContext(),"ok",Toast.LENGTH_LONG).show();


                    for (int i = 0; i < response.body().getItems().size(); i++) {
                        finaladress = "";
                        Result result = new Result();
                        if (response.body().getItems().get(i).getAddress() != null) {
                            result.setTitle(response.body().getItems().get(i).getTitle());
                            housenumbur = response.body().getItems().get(i).getAddress().getHouseNumber();
                            street = response.body().getItems().get(i).getAddress().getStreet();
                            district = response.body().getItems().get(i).getAddress().getDistrict();
                            city = response.body().getItems().get(i).getAddress().getCity();
                            postalCode = response.body().getItems().get(i).getAddress().getPostalCode();
                            countryName = response.body().getItems().get(i).getAddress().getCountryName();


                            if (housenumbur != null) {
                                finaladress = housenumbur + " ";
                            }
                            if (street != null) {
                                if (street.matches("[0-9]+")) {
                                    finaladress = finaladress + street + " ";
                                } else {
                                    finaladress = finaladress + street + ", ";
                                }

                            }
                            if (district != null) {
                                district2 = district;
                                finaladress = finaladress + district + ", ";
                            }
                            if (city != null && !city.matches(district2)) {
                                finaladress = finaladress + city + ", ";
                            }
                            if (postalCode != null) {
                                finaladress = finaladress + postalCode + ", ";
                            }
                            if (countryName != null) {
                                finaladress = finaladress + countryName;
                            }
                          /*  if(finaladress=="")
                            {
                                finaladress = response.body().getItems().get(i).getAddress().getLabel();
                            }*/
                            if (response.body().getItems().get(i).getAddress().getLabel() != null) {
                                result.setAdress(response.body().getItems().get(i).getAddress().getLabel());
                            }

                            result.setLat(String.valueOf(response.body().getItems().get(i).getPosition().getLat()));
                            result.setLng(String.valueOf(response.body().getItems().get(i).getPosition().getLng()));
                            results.add(result);

                        }

                        Log.i("adresssshow", finaladress);
                        //Toast.makeText(getApplicationContext(),finaladress,Toast.LENGTH_LONG).show();


                    }
                    processSearchResults(results);

                }
            }

            @Override
            public void onFailure(Call<Autosuggetionhere> call, Throwable t) {
                // Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_LONG).show();
                Log.i("processSearchResults", t.toString());
            }
        });


        //  Toast.makeText(getApplicationContext(),String.valueOf(currentLatLng.getLatitude())+","+String.valueOf(currentLatLng.getLongitude()),Toast.LENGTH_LONG).show();
        // setSearchMode(true);
        /*
         Creates new TextAutoSuggestionRequest with current map position as search center
         and selected collection size with applied filters and chosen locale.
         For more details how to use TextAutoSuggestionRequest
         please see documentation for HERE Mobile SDK for Android.
         */
    /*    TextAutoSuggestionRequest textAutoSuggestionRequest = new TextAutoSuggestionRequest(query);

        if (map != null) {
            textAutoSuggestionRequest.setSearchCenter(map.getCenter());
        }
        textAutoSuggestionRequest.setCollectionSize(6);

        Log.i("textAutoSuggestionRequest",currentLatLng.toString());

        // SET UK BASED COUNTRY WISE
        textAutoSuggestionRequest.setAddressFilter(new AddressFilter().setCountryCode(Locale.UK.getCountry()));
        //textAutoSuggestionRequest.setAddressFilter(new AddressFilter().setCountryCode("IN"));


        textAutoSuggestionRequest.setLocale(Locale.UK);

        *//*
           The textAutoSuggestionRequest returns its results to non-UI thread.
           So, we have to pass the UI update with returned results to UI thread.
         *//*
        textAutoSuggestionRequest.execute(new ResultListener<List<AutoSuggest>>() {
            @Override
            public void onCompleted(final List<AutoSuggest> autoSuggests, ErrorCode errorCode) {
                if (errorCode == errorCode.NONE) {
                    AutoSuggest autoSuggest;
                    for (int i=0;i<autoSuggests.size();i++)
                    {
                        if (!autoSuggests.get(i).getTitle().isEmpty())
                        {
                            Log.i("processSearchResults","ok"+autoSuggests.size());
                        }
                        else
                        {
                            Log.i("processSearchResults","not ok");
                        }
                    }
                    processSearchResults(autoSuggests);
                } else {
                    Log.i("error with doserch", errorCode.toString());
                }
            }
        });*/


    }

    private void processSearchResults(final List<Result> autoSuggests) {


        m_autoSuggestAdapter = new AutoSuggestAdapter(AddUserLocationActivity.this, autoSuggests);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(mLayoutManager);
        listView.setAdapter(m_autoSuggestAdapter);




        /* *//*   this.runOnUiThread(new Runnable() {
            @Override
            public void run() {*//*
                m_autoSuggests.clear();
                m_autoSuggests.addAll(autoSuggests);

                m_autoSuggestAdapter = new AutoSuggestAdapter(AddUserLocationActivity.this,  m_autoSuggests);

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                listView.setLayoutManager(mLayoutManager);
                listView.setAdapter(m_autoSuggestAdapter);
                m_autoSuggestAdapter.notifyDataSetChanged();


     *//*       }
        });*/
    }


}