package com.edelivery;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.edelivery.adapter.OrderDetailsAdapter;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.Order;
import com.edelivery.models.responsemodels.OrderResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PinnedHeaderItemDecoration;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderPreparedActivity extends BaseAppCompatActivity {

    private CustomFontTextView tvOrderNumber;
    private RecyclerView rcvOrderProductItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_prepared);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        findViewById();
        setViewListener();
        getExtraData();
    }

    @Override
    protected boolean isValidate() {
        // do somethings
        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings
        tvOrderNumber = (CustomFontTextView) findViewById(R.id.tvOrderNumber);
        rcvOrderProductItem = (RecyclerView) findViewById(R.id.rcvOrderProductItem);
    }

    @Override
    protected void setViewListener() {
        // do somethings

    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();

    }


    @Override
    public void onClick(View view) {
        // do somethings

    }

    private void getExtraData() {
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getParcelable(Const.Params.ORDER) != null) {
                Order order = getIntent().getExtras().getParcelable(Const.Params.ORDER);
                if (order.getProducts() != null && !order.getProducts().isEmpty()) {
                    setOrderData(order);
                } else {
                    getOrderDetail(order);
                }
            }
        }

    }

    private void setOrderData(Order order) {
        setTitleOnToolBar(order.getStoreName());
        String orderNumber = getResources().getString(R.string.text_order_number)
                + " " + "#" + order.getUniqueId();
        tvOrderNumber.setText(orderNumber);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rcvOrderProductItem.setLayoutManager(layoutManager);
        OrderDetailsAdapter itemAdapter = new OrderDetailsAdapter(this, order.getProducts(),
                order.getCurrency(), true);
        rcvOrderProductItem.setAdapter(itemAdapter);
        rcvOrderProductItem.addItemDecoration(new PinnedHeaderItemDecoration());
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void getOrderDetail(final Order pushDataResponse) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.ORDER_ID, pushDataResponse.getId());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderResponse> call = apiInterface.getOrderDetail(ApiClient
                .makeJSONRequestBody(jsonObject));
        call.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call,
                                   Response<OrderResponse
                                           > response) {
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {
                    if (response.body().isSuccess()) {
                        Order order = response.body().getOrder();
                        order.setStoreName(pushDataResponse.getStoreName());
                        order.setProducts(order.getOrderData().getOrderDetails());
                        order.setCurrency(order.getCountries().getCurrencySign());
                        setOrderData(order);
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                OrderPreparedActivity.this);
                    }
                }

            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();

                AppLog.handleThrowable(Const.Tag.STORES_ACTIVITY, t);
            }
        });
    }
}
