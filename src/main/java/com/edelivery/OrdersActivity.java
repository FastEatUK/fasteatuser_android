package com.edelivery;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.edelivery.adapter.ViewPagerAdapter;
import com.edelivery.fragments.OrderFragment;
import com.edelivery.fragments.HistoryFragment;

public class OrdersActivity extends BaseAppCompatActivity {
    private TabLayout orderHistoryTabsLayout;
    private ViewPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_orders));
        findViewById();
        setViewListener();
        initTabLayout();
    }

    @Override
    protected boolean isValidate() {
        // do somethings
        return false;
    }

    @Override
    protected void findViewById() {
        // do somethings
        orderHistoryTabsLayout = (TabLayout) findViewById(R.id
                .ordersTabsLayout);
        viewPager = (ViewPager) findViewById(R.id.ordersViewpager);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(getResources().getDimensionPixelSize(R.dimen
                    .dimen_app_tab_elevation));
        }
    }

    @Override
    protected void setViewListener() {
        // do somethings
        orderHistoryTabsLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                updateUiHistory(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onBackNavigation() {
        // do somethings
        onBackPressed();
    }


    @Override
    public void onClick(View view) {
        // do somethings

    }

    private void initTabLayout() {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new OrderFragment(), getResources()
                .getString(R.string.text_current_orders));
        viewPagerAdapter.addFragment(new HistoryFragment(), getResources()
                .getString(R.string.text_order_history));
        viewPager.setAdapter(viewPagerAdapter);
        orderHistoryTabsLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onBackPressed() {
        goToHomeActivity();
    }

    private void updateUiHistory(int position) {
        switch (position) {
            case 0:
                setToolbarRightIcon3(R.drawable.filter_store, null);
                ivToolbarRightIcon3.setImageDrawable(null);
                break;
            case 1:
                HistoryFragment orderHistoryFragment = (HistoryFragment)
                        viewPagerAdapter.getItem(1);

               if(orderHistoryFragment!=null) {
                   if (orderHistoryFragment.llDateFilter.getVisibility() == View.VISIBLE) {
                       setToolbarRightIcon3(R.drawable.ic_cancel, orderHistoryFragment);
                   } else {
                       setToolbarRightIcon3(R.drawable.filter_store, orderHistoryFragment);
                   }
               }
                break;
            default:
                // do with default
                break;
        }
    }

}
