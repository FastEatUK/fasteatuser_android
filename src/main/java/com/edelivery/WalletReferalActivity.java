package com.edelivery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.edelivery.adapter.OrderDetailsAdapter;
import com.edelivery.adapter.RegisterBonusAdapter;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.WalletReferalHistoryModel.WalletReferalHistory;
import com.edelivery.models.WalletReferalHistoryModel.WalletReferalHistoryResponse;
import com.edelivery.models.datamodels.WalletHistory;

import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;

import com.edelivery.utils.AppLog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletReferalActivity extends BaseAppCompatActivity {

    RecyclerView rvRegistrationBonus;

    ArrayList<WalletReferalHistory> bonuslist = new ArrayList<>();
    CustomFontTextView tvTotalBalance, tvReferalBonus, tvRegisterBonus;
    CardView cvList;
    LinearLayout llMainPart, llNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        initToolBar();
        findViewById();
        setTitleOnToolBar(getResources().getString(R.string.text_wallet));
        setViewListener();

        callWalletHistoryAPI();


    }

    public void callWalletHistoryAPI() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.TYPE, Const.Type.USER);
        } catch (JSONException e) {
            AppLog.handleException("WalletActivity", e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<WalletReferalHistoryResponse> call = apiInterface.getReferralHistory(ApiClient
                .makeJSONRequestBody(jsonObject));
        call.enqueue(new Callback<WalletReferalHistoryResponse>() {
            @Override
            public void onResponse(Call<WalletReferalHistoryResponse> call,
                                   Response<WalletReferalHistoryResponse> response) {
                AppLog.Log("WALLET_HISTORY", ApiClient.JSONResponse(response.body()));
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.isSuccessful()) {

                    if (response.body().getSuccess()) {

                        try {

                            String str_currency = "£";
                        /*if (response.body().getCurrencyCode() != null && !response.body().getCurrencyCode().equals("")) {

                            if (response.body().getCurrencyCode().toUpperCase().contains("NULL")) {
                                str_currency = convertInSymbol(response.body().getCurrencyCode().toUpperCase().replace("NULL", ""));
                            } else {
                                str_currency = convertInSymbol(response.body().getCurrencyCode());
                            }
                        } else {
                            str_currency = "";
                        }*/


                            tvTotalBalance.setText(" " + str_currency + parseContent.decimalTwoDigitFormat.format(Double.parseDouble(response.body().getTotalWallets())));
                            tvRegisterBonus.setText(str_currency + "" + parseContent.decimalTwoDigitFormat.format(Double.parseDouble(response.body().getRegisterReferalWallet())));
                            tvReferalBonus.setText(str_currency + "" + parseContent.decimalTwoDigitFormat.format(Double.parseDouble(response.body().getFriendReferalWallet())));

                            bonuslist.clear();
                            bonuslist.addAll(response.body().getWalletHistory());

                            if (bonuslist.size() > 0) {
                                cvList.setVisibility(View.VISIBLE);
                            } else {
                                cvList.setVisibility(View.GONE);
                            }

                            setRecycleAdapter();
                        } catch (Exception e) {
                            Log.e("Error", e.getMessage().toString());
                        }
                        llMainPart.setVisibility(View.VISIBLE);
                        llNoData.setVisibility(View.GONE);
                    } else {
                        if (bonuslist.size() > 0) {
                            cvList.setVisibility(View.VISIBLE);
                        } else {
                            cvList.setVisibility(View.GONE);
                        }
                        setRecycleAdapter();

                        llMainPart.setVisibility(View.VISIBLE);
                        llNoData.setVisibility(View.GONE);
                    }


                } else {
                    callNoData();
                    Utils.showErrorToast(response.body().getErrorCode(), WalletReferalActivity.this);
                }
            }

            @Override
            public void onFailure(Call<WalletReferalHistoryResponse> call, Throwable t) {
                callNoData();
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable("WalletReferal", t);

            }
        });
    }

    public void callNoData() {
        llMainPart.setVisibility(View.GONE);
        llNoData.setVisibility(View.VISIBLE);
    }

    @Override
    protected boolean isValidate() {
        return false;
    }


    @Override
    protected void findViewById() {
        rvRegistrationBonus = findViewById(R.id.rvRegistrationBonus);
        tvTotalBalance = findViewById(R.id.tvTotalBalance);
        tvReferalBonus = findViewById(R.id.tvReferalBonus);
        tvRegisterBonus = findViewById(R.id.tvRegisterBonus);

        llMainPart = findViewById(R.id.llMainPart);
        llNoData = findViewById(R.id.llNoData);
        cvList = findViewById(R.id.cvList);
    }

    @Override
    protected void setViewListener() {

    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private String convertInSymbol(String value) {
        Currency cur = Currency.getInstance(value);
        // Get and print the symbol of the currency
        String symbol = cur.getSymbol();

        return symbol;
    }

    private void setRecycleAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvRegistrationBonus.setLayoutManager(layoutManager);
        RegisterBonusAdapter itemAdapter = new RegisterBonusAdapter(bonuslist, this);
        rvRegistrationBonus.setAdapter(itemAdapter);
    }


}