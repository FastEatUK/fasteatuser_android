package com.edelivery;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.edelivery.adapter.FavAddressAdapter;
import com.edelivery.component.CustomDialogPostCode;
import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.AddressModel;
import com.edelivery.models.datamodels.Addresses;
import com.edelivery.models.getAddressModel;
import com.edelivery.models.hereresponce.Position;
import com.edelivery.models.hereresponce.Result;
import com.edelivery.models.responsemodels.FavouriteAddressResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouriteAddressActivity extends BaseAppCompatActivity {

    public static final String TAG = FavouriteAddressActivity.class.getName();
    private RecyclerView rcvAddress;
    private CustomFontButton floatingBtnAddFavAddress;
    private boolean isApplicationStart;
    private boolean isHideDelete = false;
    private ArrayList<Addresses> favAddressList = new ArrayList<>();
    ;
    private FavAddressAdapter favAddressAdapter;
    private CustomFontTextView tvNoFavAddress;
    private LinearLayout ivEmpty;
    private ImageView ivEmptyImage;
    private CustomDialogPostCode customDialogPostCode;
    ArrayList<Result> results = new ArrayList<>();
    private List<AddressModel> addressArrayList = new ArrayList<>();
    RelativeLayout rlClickHereView;
    String isFrom = "";
    int clickPosition = -1;

    public CurrentBooking currentBooking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_address);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_favourite_address));
        currentBooking = CurrentBooking.getInstance();
        ivToolbarPlus.setVisibility(View.VISIBLE);
        findViewById();
        setViewListener();
        favAddressList = new ArrayList<>();
        getExtraData();


    }

    private void initRcvAddress() {
        if (favAddressAdapter != null) {
            favAddressAdapter.notifyDataSetChanged();
        } else {
            rcvAddress.setLayoutManager(new LinearLayoutManager(this));
            favAddressAdapter = new FavAddressAdapter(favAddressList, isHideDelete, true) {
                @Override
                public void onDelete(int position) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(FavouriteAddressActivity.this);
                    builder1.setMessage(getString(R.string.you_want_to_delete_address));
                    builder1.setCancelable(false);

                    builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            deleteAddress(favAddressList.get(position).getId());
                        }
                    });
                    builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();


                }

                @Override
                public void onEdit(int position) {
                    clickPosition = position;
                    try {
                        if (favAddressList.get(position).getPostCode() != null) {
                            editAddress(favAddressList.get(position).getId(), favAddressList.get(position).getPostCode());
                        }
                    } catch (Exception e) {

                    }

                }
            };
            rcvAddress.setAdapter(favAddressAdapter);
        }
    }

    private void editAddress(String id, String postCode) {
        try {
            if (postCode != null) {
                if (postCode.equals("")) {
                    Utils.showToast(getString(R.string.no_data_found), this);
                } else {
                    callAPIgetAddress(postCode, "Edit_Address");
                }
            }

        } catch (Exception e) {

        }


    }

    private void getExtraData() {
        if (getIntent().getExtras() != null) {
            isApplicationStart = getIntent().getExtras().getBoolean(Const.Tag.HOME_FRAGMENT);
            isHideDelete = getIntent().getExtras().getBoolean(Const.Tag.HIDE_DELETE);

            getFavAddressList();
        }

    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {
        rcvAddress = (RecyclerView) findViewById(R.id.rcvAddress);
        floatingBtnAddFavAddress = (CustomFontButton) findViewById(R.id
                .floatingBtnAddFavAddress);
        tvNoFavAddress = (CustomFontTextView) findViewById(R.id.tvNoFavAddress);
        ivEmpty = (LinearLayout) findViewById(R.id.ivEmpty);
        ivEmptyImage = (ImageView) findViewById(R.id.ivEmptyImage);
        rlClickHereView = (RelativeLayout) findViewById(R.id.rlClickHereView);
    }

    @Override
    protected void setViewListener() {
        floatingBtnAddFavAddress.setOnClickListener(this);
        ivToolbarPlus.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.floatingBtnAddFavAddress:
                /* goToAddUserAddress("", results);*/
                break;
            case R.id.ivToolbarPlus:
                initOpenPostCodeDialog();
                customDialogPostCode.show();

                //goToAddUserAddress();
                break;
            default:
                // do with default
                break;
        }
    }

    public void initOpenPostCodeDialog() {
        customDialogPostCode = new CustomDialogPostCode(this) {
            @Override
            public void onClickLeftButton() {
                customDialogPostCode.dismiss();
            }

            @Override
            public void onClickRightButton(CustomFontEditTextView etDialogEditTextTwo) {

                if (etDialogEditTextTwo.getText().toString().isEmpty()) {
                    Utils.showToast(getString(R.string.msg_enter_postcode), getContext());
                } else {
                    callAPIgetAddress(etDialogEditTextTwo.getText().toString(), "Add_Address");
                }
            }
        };
    }


    public void callAPIgetAddress(String postcode_keyword, String isFrom) {
        Utils.showCustomProgressDialog(this, false);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("api-key", getString(R.string.address_key));
        hashMap.put("expand", "true");
        hashMap.put("format", "false");
        hashMap.put("fuzzy", "true");
        hashMap.put("sort", "true");
        Log.e("param", hashMap.toString());
        results.clear();
        ApiInterface apiinterface = ApiClient.getaddresssuggetion().create(ApiInterface.class);
        Call<getAddressModel> call = apiinterface.getAddressAPIForLocation(postcode_keyword, hashMap);
        call.enqueue(new Callback<getAddressModel>() {
            @Override
            public void onResponse(Call<getAddressModel> call, Response<getAddressModel> response) {
                Log.i("dosearch", "ok");
                //    Toast.makeText(getApplicationContext(),String.valueOf(currentLatLng.getLatitude())+","+String.valueOf(currentLatLng.getLongitude()),Toast.LENGTH_LONG).show();
                results.clear();

                if (response.isSuccessful()) {


                    if (response.body().getAddresses().size() > 0) {
                        Log.e("dosearch", ApiClient.JSONResponse(response.body().toString()));

                        addressArrayList.clear();
                        addressArrayList = response.body().getAddresses();

                        Log.i("dosearch", ApiClient.JSONResponse(response.body()));

                        for (int i = 0; i < addressArrayList.size(); i++) {

                            Result result = new Result();
                            //add in result list
                            String desc = "";

                            ArrayList<String> listNew = new ArrayList();
                            listNew.clear();

                            listNew.add(addressArrayList.get(i).getLine1());
                            listNew.add(addressArrayList.get(i).getLine2());
                           /* listNew.add(addressArrayList.get(i).getLine2());
                            listNew.add(addressArrayList.get(i).getLine3());
                            listNew.add(addressArrayList.get(i).getLine4());
                            listNew.add(addressArrayList.get(i).getLocality());*/
                            listNew.add(addressArrayList.get(i).getTownOrCity());
                            /* listNew.add(addressArrayList.get(i).getCounty());
                            listNew.add(addressArrayList.get(i).getCountry());
                            listNew.add(addressArrayList.get(i).getDistrict());*/
                            listNew.add(response.body().getPostcode());

                            for (int j = 0; j < listNew.size(); j++) {
                                if (!listNew.get(j).equals("")) {
                                    if (j == 0) {
                                        desc = listNew.get(j);
                                    } else {
                                        desc = desc + ", " + listNew.get(j);
                                    }
                                }

                            }

                            result.setTitle("");
                            result.setAdress(desc);
                            result.setCountry(addressArrayList.get(i).getCountry());
                            result.setCity(addressArrayList.get(i).getTownOrCity());
                            result.setPost_code(response.body().getPostcode());
                            result.setAddress_1(addressArrayList.get(i).getLine1());
                            result.setAddress_2(addressArrayList.get(i).getLine2());
                            result.setLat(response.body().getLatitude());
                            result.setLng(response.body().getLongitude());
                            results.add(result);
                        }

                        if (isFrom.equals("Add_Address")) {
                            customDialogPostCode.dismiss();
                        }
                        goToAddUserAddress(postcode_keyword, results, isFrom);
                        Utils.hideCustomProgressDialog();

                    } else {
                        if (isFrom.equals("Add_Address")) {
                            customDialogPostCode.dismiss();
                        }
                        Utils.hideCustomProgressDialog();
                        Utils.showToast(getString(R.string.check_postcode_try_again), FavouriteAddressActivity.this);
                    }

                } else {

                    if (isFrom.equals("Add_Address")) {
                        customDialogPostCode.dismiss();
                    }
                    Utils.hideCustomProgressDialog();
                    Utils.showToast(getString(R.string.check_postcode_try_again), FavouriteAddressActivity.this);
                }

            }

            @Override
            public void onFailure(Call<getAddressModel> call, Throwable t) {
                if (isFrom.equals("Add_Address")) {
                    customDialogPostCode.dismiss();
                }
                Utils.hideCustomProgressDialog();
                Log.i("processSearchResults", t.toString());
            }
        });

    }

    public void goToAddUserAddress(String postcode, ArrayList<Result> addresslist, String isFrom) {
        // Intent intent = new Intent(this, UserAddAddress.class);
        Intent intent = new Intent(this, AddUserLocationActivity.class);

        Bundle bundle = new Bundle();
        bundle.putBoolean(Const.Tag.HOME_FRAGMENT, isApplicationStart);
        bundle.putParcelableArrayList("AddressIntentList", results);
        bundle.putString("isFrom", isFrom);
        if (isFrom.equals("Edit_Address")) {
            bundle.putString("mainAddrees", favAddressList.get(clickPosition).getAddress());
            bundle.putString("postcode", favAddressList.get(clickPosition).getPostCode());
            bundle.putString("line1", favAddressList.get(clickPosition).getAddress_1());
            bundle.putString("line2", favAddressList.get(clickPosition).getAddress_2());
            bundle.putString("city", favAddressList.get(clickPosition).getCity());
            bundle.putString("instruction", favAddressList.get(clickPosition).getNote());
            bundle.putString("addressId", favAddressList.get(clickPosition).getId());
            bundle.putString("latitude", String.valueOf(favAddressList.get(clickPosition).getLocation().get(0)));
            bundle.putString("longitude", String.valueOf(favAddressList.get(clickPosition).getLocation().get(1)));
            bundle.putString("country", favAddressList.get(clickPosition).getCountry());

        }
        intent.putExtras(bundle);

        /*intent.putExtra(Const.Tag.HOME_FRAGMENT, isApplicationStart);
        intent.putParcelableArrayListExtra("AddressIntentList", results);*/
        startActivityForResult(intent, Const.STORE_LOCATION_RESULT);
        Utils.overridePendingTransitionEnterDown(this);

    }


    private void deleteAddress(String id) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.ADDRESS_ID, id);

        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<IsSuccessResponse> responseCall = apiInterface.DeleteFavouriteAddress(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    getFavAddressList();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), FavouriteAddressActivity.this);
                }

            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);

            }
        });
    }

    @Override
    public void onBackPressed() {
        /*if (favAddressList.isEmpty()) {
            Utils.showToast(getResources().getString(R.string.text_add_address_or_location), this);
        } else {*/
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        /*  }*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Const.STORE_LOCATION_RESULT:
                    getFavAddressList();
                    break;

                default:
                    // do with default
                    break;
            }

        }

    }

    private void getFavAddressList() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<FavouriteAddressResponse> responseCall = apiInterface.getFavouriteAddressList(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<FavouriteAddressResponse>() {
            @Override
            public void onResponse(Call<FavouriteAddressResponse> call, Response<FavouriteAddressResponse>
                    response) {
                favAddressList.clear();
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().getSuccess()) {
                    favAddressList.addAll(response.body().getFavouriteAddresses());

                    currentBooking.getInstance().getFavAddressList().clear();
                    currentBooking.getInstance().setFavAddressList(response.body().getFavouriteAddresses());

                    updateUiFavAddress(!favAddressList.isEmpty());
                    initRcvAddress();
                    Log.d("aaa", "" + favAddressList.size());
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), FavouriteAddressActivity.this);
                }

            }

            @Override
            public void onFailure(Call<FavouriteAddressResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);

            }
        });
    }

    public void updateUiFavAddress(boolean isUpdate) {
        if (isUpdate) {
            rcvAddress.setVisibility(View.VISIBLE);
            ivEmpty.setVisibility(View.GONE);
        } else {
            rcvAddress.setVisibility(View.GONE);
            ivEmpty.setVisibility(View.VISIBLE);
            ivEmptyImage.setVisibility(View.GONE);
            tvNoFavAddress.setText(getResources().getString(R.string.msg_fav_address_not_added));
            tvNoFavAddress.setVisibility(View.VISIBLE);
            rlClickHereView.setVisibility(View.VISIBLE);
        }
    }
}
