package com.edelivery;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.chaos.view.PinView;
import com.edelivery.component.CustomDialogVerification;
import com.edelivery.component.CustomFontEditTextView;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.interfaces.LatLngInterpolator;
import com.edelivery.models.datamodels.ConfirmationData;
import com.edelivery.models.datamodels.OtpConfirmation;
import com.edelivery.models.datamodels.RegistartionData;
import com.edelivery.models.responsemodels.UserDataResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmOnetimePassword extends BaseAppCompatActivity {
    EditText etotp1,etotp2,etotp3,etotp4;
    PinView pinView;
    CustomFontTextView mobileno,timer,resend,changeno;
    GetMobileNo getMobileNo=new GetMobileNo();
    Button verify;
    String OTP="";
    AppEventsLogger logger;
    static ConfirmationData confirmationData;
    boolean gofav=false;
    android.os.CountDownTimer countDownTimer;
    private CustomDialogVerification customDialogVerification;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_onetime_password);

        findViewById();
        setViewListener();
        if (getIntent().getSerializableExtra(Const.Params.Otp_data)!=null)
        {
            confirmationData= (ConfirmationData) getIntent().getSerializableExtra(Const.Params.Otp_data);
            Log.i("confirmationData","otp:"+confirmationData.getOtp()+"\n"
                    +"mobileno:"+confirmationData.getMobileNo());
        }

        if (!TextUtils.isEmpty(confirmationData.getMobileNo())){

           setphoneno(confirmationData.getMobileNo());
        }
        if (!TextUtils.isEmpty(confirmationData.getOtp())){
            OTP=confirmationData.getOtp();
            countdown();
        }
        if (TextUtils.isEmpty(confirmationData.getOtp()))
        {
            Resendotp(confirmationData.getUser_id());
        }

        logger = (AppEventsLogger) AppEventsLogger.newLogger(getApplicationContext());

    }

    @Override
    protected boolean isValidate() {

        String msg = null;
        String enteredotp=pinView.getText().toString();


        if (enteredotp.length()< 4) {
            msg = getString(R.string.msd_please_enter_valid_otp);
            //pinView.requestFocus();
        }
        /*else if (TextUtils.isEmpty(etotp2.getText().toString().trim()))
        {
            msg = getString(R.string.msd_please_enter_valid_otp);
            etotp2.setError(msg);
            etotp2.requestFocus();
        }
        else if (TextUtils.isEmpty(etotp3.getText().toString().trim()))
        {
            msg = getString(R.string.msd_please_enter_valid_otp);
            etotp3.setError(msg);
            etotp3.requestFocus();
        }
        else if (TextUtils.isEmpty(etotp4.getText().toString().trim()))
        {
            msg = getString(R.string.msd_please_enter_valid_otp);
            etotp4.setError(msg);
            etotp4.requestFocus();
        }*/
        if (!TextUtils.isEmpty(msg)){
                Utils.showToast(msg,ConfirmOnetimePassword.this);
                pinView.requestFocus();


        }
        return TextUtils.isEmpty(msg);
    }

    @Override
    protected void findViewById() {
        /*etotp1=findViewById(R.id.otpET1);
        etotp2=findViewById(R.id.otpET2);
        etotp3=findViewById(R.id.otpET3);
        etotp4=findViewById(R.id.otpET4);*/
        pinView=findViewById(R.id.firstPinView);
        mobileno=findViewById(R.id.tv_mobileno);
        timer=findViewById(R.id.tv_timer);
        resend=findViewById(R.id.tv_resend);
        verify=findViewById(R.id.btn_verify);
        changeno=findViewById(R.id.tv_changemobile);
    }

    @Override
    protected void setViewListener() {
        verify.setOnClickListener(this);
        resend.setOnClickListener(this);
        changeno.setOnClickListener(this);
        /*etotp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etotp1.getText().toString().length() == 1)     //size as per your requirement
                {
                    etotp2.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etotp1.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (etotp1.getText().toString().length() == 0 && keyCode == KeyEvent.KEYCODE_DEL)
                {
                    etotp1.clearFocus();
                }
                return false;
            }
        });
        etotp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etotp2.getText().toString().length() == 1)     //size as per your requirement
                {
                    etotp3.requestFocus();
                }
                else if (etotp2.getText().toString().length() == 0)     //size as per your requirement
                {
                    etotp1.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etotp2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (etotp2.getText().toString().length() == 0 && keyCode == KeyEvent.KEYCODE_DEL)
                {
                    etotp1.requestFocus();
                }
                return false;
            }
        });
        etotp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etotp3.getText().toString().length() == 1)     //size as per your requirement
                {
                    etotp4.requestFocus();
                }
                else if(etotp3.getText().toString().length() == 0)     //size as per your requirement
                {
                    etotp2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etotp3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (etotp3.getText().toString().length() == 0 && keyCode == KeyEvent.KEYCODE_DEL)
                {
                    etotp2.requestFocus();
                }
                return false;
            }
        });
        etotp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etotp4.getText().toString().length() == 1)     //size as per your requirement
                {
                    etotp4.clearFocus();
                    Utils.hideSoftKeyboard(ConfirmOnetimePassword.this);
                }
                if (etotp4.getText().toString().length() == 0)     //size as per your requirement
                {
                    etotp3.requestFocus();
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etotp4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (etotp4.getText().toString().length() == 0 && keyCode == KeyEvent.KEYCODE_DEL)
                {
                    etotp3.requestFocus();
                }
                return false;
            }
        });*/
    }

    @Override
    protected void onBackNavigation() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_verify:
               /* setResult(Activity.RESULT_OK);
                finish();*/
               if (isValidate())
               {
                   if (CheckOtp())
                   {

                      // Utils.showToast("Otp macthed"+confirmationData.getOtp(),ConfirmOnetimePassword.this);
                       Log.i("OTP_matched_user",confirmationData.getUser_id());
                     Confirmotp(confirmationData.getUser_id());
                   }

               }
                break;

            case R.id.tv_resend:
                Resendotp(confirmationData.getUser_id());
                break;

            case R.id.tv_changemobile:
                openchangemobilenodialog();
                break;
            default:
                break;
        }

    }
    protected boolean CheckOtp() {

        boolean valid=true;
        String enteredotp=null;
        enteredotp=pinView.getText().toString();
        if (!enteredotp.matches(OTP))
        {
            valid=false;
            Utils.showToast("You have entered Wrong Otp",ConfirmOnetimePassword.this);
        }
        else if (TextUtils.isEmpty(OTP))
        {
            valid=false;
            Utils.showToast("You have entered Wrong Otp expired",ConfirmOnetimePassword.this);
        }

        return valid;
    }


    void Confirmotp(String id)
    {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.USER_ID, id);

            if(confirmationData.isUpdateuser())
            {
                jsonObject.put("update_user", 1);
            }
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<UserDataResponse> otpConfirmationCall=apiInterface.otpconfirmation(ApiClient
                .makeJSONRequestBody(jsonObject));
        Log.i("Confirmotp()",jsonObject.toString());

        otpConfirmationCall.enqueue(new Callback<UserDataResponse>() {
            @Override
            public void onResponse(Call<UserDataResponse> call, Response<UserDataResponse> response) {
                AppLog.Log("register_responce", ApiClient.JSONResponse(response.body()));
                if (parseContent.isSuccessful(response)) {


                    if (response != null && response.body() != null && response.body().getUser() != null && response.body().isSuccess() && preferenceHelper != null) {

                        if (parseContent.parseUserStorageData(response)) {
                            Utils.showMessageToast(response.body().getMessage(), ConfirmOnetimePassword.this);
                            CurrentBooking.getInstance().setBookCityId("");

                                goToHomeActivity();

                                if (!TextUtils.isEmpty(confirmationData.getSocialtype())) {
                                    Log.i("FBA_TYPE", confirmationData.getSocialtype());
                                    logCompleteRegistrationEvent(confirmationData.getSocialtype());
                                    gofav=true;
                                }

                                if (gofav)
                                {
                                    goToFavouriteAddressActivity(true,true);
                                }
                              //

                        } else {
                            preferenceHelper.clearVerification();
                        }
                    }
                    else
                    {
                        if (response != null && response.body() != null) {
                            Utils.showErrorToast(response.body().getErrorCode(), ConfirmOnetimePassword.this);
                        }

                    }


                } else {
                    preferenceHelper.clearVerification();
                }

            }

            @Override
            public void onFailure(Call<UserDataResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
            }
        });
    }
    public void logCompleteRegistrationEvent(String registrationMethod) {
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, registrationMethod);
        logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);
    }


    void Resendotp(String id)
    {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.USER_ID, id);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<UserDataResponse> otpConfirmationCall=apiInterface.resendpin(ApiClient
                .makeJSONRequestBody(jsonObject));
        Log.i("Confirmotp()",jsonObject.toString());

        otpConfirmationCall.enqueue(new Callback<UserDataResponse>() {
            @Override
            public void onResponse(Call<UserDataResponse> call, Response<UserDataResponse> response) {
                AppLog.Log("register_responce_resend", ApiClient.JSONResponse(response.body()));

                Utils.hideCustomProgressDialog();

                    if (response != null && response.body() != null && response.body().getUser() != null) {

                                OTP=response.body().getUser().getOtp();
                                pinView.setText("");

                                countdown();
                        }




                 else {
                    preferenceHelper.clearVerification();
                }

            }

            @Override
            public void onFailure(Call<UserDataResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
            }
        });
    }
    void countdown()
    {
       /* countDownTimer.cancel();
        countDownTimer=null;*/

        timer.setText("00 : 00");
        resend.setClickable(false);
        countDownTimer=new CountDownTimer(50000, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setText("00 : " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                timer.setText("00 : 00");
                resend.setClickable(true);
                countDownTimer.cancel();
                OTP="";
            }

        }.start();

    }
    private void openchangemobilenodialog() {


            customDialogVerification = new CustomDialogVerification(this,
                    getResources()
                            .getString(R.string
                                    .text_change_mobileno), getResources()
                    .getString(R.string.msg_enter_mobilenoforchange),
                    getResources().getString(R.string.text_cancel), getResources()
                    .getString(R.string.text_ok), "", getResources()
                    .getString(R.string
                            .text_mobile_no), false, InputType.TYPE_CLASS_NUMBER, InputType
                    .TYPE_TEXT_VARIATION_PHONETIC | InputType.TYPE_CLASS_NUMBER) {
                @Override
                public void onClickLeftButton() {
                    dismiss();

                }

                @Override
                public void onClickRightButton(CustomFontEditTextView etDialogEditTextOne,
                                               CustomFontEditTextView etDialogEditTextTwo) {
                    if (!etDialogEditTextTwo.getText().toString().isEmpty()) {
                        if (validatemobileno(etDialogEditTextTwo)){
                            changeMobileno(etDialogEditTextTwo.getText().toString());
                            dismiss();
                        }

                      //updateProfile(etDialogEditTextTwo.getText().toString());

                    } else {
                        etDialogEditTextTwo.setError(getString(R.string.msg_please_enter_valid_mobile_number));
                    }
                }

            };
            customDialogVerification.show();



    }

    boolean validatemobileno(CustomFontEditTextView etDialogEditTextTwo)
    {
        String msg = null;

        if (TextUtils.isEmpty(etDialogEditTextTwo.getText().toString().trim())) {
            msg = getString(R.string.msg_please_enter_valid_mobile_number);
            etDialogEditTextTwo.setError(msg);
            etDialogEditTextTwo.requestFocus();
        } else if (etDialogEditTextTwo.getText().toString().trim().length()
                > preferenceHelper.getMaxPhoneNumberLength() || etDialogEditTextTwo.getText().toString().trim().length
                () < preferenceHelper.getMinPhoneNumberLength()) {
            msg = getString(R.string.msg_please_enter_valid_mobile_number) + " " +
                    "" + preferenceHelper.getMinPhoneNumberLength() + getString(R.string.text_or)
                    + preferenceHelper.getMaxPhoneNumberLength() + " " + getString(R.string
                    .text_digits);
            etDialogEditTextTwo.setError(msg);
            etDialogEditTextTwo.requestFocus();
        }

        return TextUtils.isEmpty(msg);


    }

    private void changeMobileno(String phone)
    {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.USER_ID,confirmationData.getUser_id());
            jsonObject.put(Const.Params.PHONE,phone);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);


        Call<UserDataResponse> changemobilenocall=apiInterface.changenumbur(ApiClient
                .makeJSONRequestBody(jsonObject));
        Log.i("changeMobileno()",jsonObject.toString());

        changemobilenocall.enqueue(new Callback<UserDataResponse>() {
            @Override
            public void onResponse(Call<UserDataResponse> call, Response<UserDataResponse> response) {
                Utils.hideCustomProgressDialog();
                AppLog.Log("register_responce_changeno", ApiClient.JSONResponse(response.body()));
                if (response != null && response.body() != null && response.body().getUser() != null) {

                    if (!TextUtils.isEmpty(response.body().getUser().getPhone()))
                    {
                        OTP=response.body().getUser().getOtp();
                        //cancelTimer();
                        countDownTimer.onFinish();
                        countdown();

                        if (!TextUtils.isEmpty(response.body().getUser().getPhone()))
                        {

                            setphoneno(response.body().getUser().getPhone());
                        }

                    }

                }
                else {
                    preferenceHelper.clearVerification();
                }
            }

            @Override
            public void onFailure(Call<UserDataResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
            }
        });

    }
    void cancelTimer() {
        if(countDownTimer!=null)
            countDownTimer.cancel();

    }

    void setphoneno(String mobileNo)
    {
        String[] prefix={"+440","+44","+91","+910","+","00","0"};
        for (int i=0;i<prefix.length;i++)
        {
            if (mobileNo.startsWith(prefix[i]))
            {
                Log.i("startsWith(prefix[i])", prefix[i]);

                mobileNo= mobileNo.substring(prefix[i].length());
                i=0;

            }
        }
        mobileno.setText("0"+mobileNo);
    }
}
