package com.edelivery.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.LayoutDirection;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.edelivery.LoginActivity;
import com.edelivery.R;
import com.edelivery.adapter.StoreAdapter;
import com.edelivery.component.CustomCircularProgressView;
import com.edelivery.models.datamodels.DayTime;
import com.edelivery.models.datamodels.StoreClosedResult;
import com.edelivery.models.datamodels.StoreTime;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ParseContent;
import com.google.android.gms.maps.model.LatLng;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

/**
 * Created by elluminati on 02-Feb-2017.
 */
public class Utils {

    public static final String TAG = "Utils";
    private static Dialog dialog;
    private static CustomCircularProgressView ivProgressBar;
    private static Dialog dialogHome;
    public static void showToast(String message, Context context) {

        Log.i("showToast",message);

        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }

    public static void showErrorToast(int code, Context context) {
        Log.i("showErrorToast", String.valueOf(code));

        String msg;
        String errorCode = Const.ERROR_CODE_PREFIX + code;
        try {
            msg = context.getResources().getString(
                    context.getResources().getIdentifier(errorCode, Const.STRING,
                            context.getPackageName()));


            if (Const.INVALID_TOKEN == code || Const.USER_DATA_NOT_FOUND == code) {
                goToLoginActivity(context);
            }

        } catch (Resources.NotFoundException e) {
            msg = errorCode;
            AppLog.Log(TAG, msg);
            AppLog.handleException(TAG, e);
        }
        showToast(msg, context);

    }
    public static void overridePendingTransitionEnterDown(Context context) {
        ((Activity) context).overridePendingTransition(R.anim.slide_down, R.anim.slide_up);
    }
    public static void showMessageToast(int code, Context context) {
        String msg;
        String messageCode = Const.MESSAGE_CODE_PREFIX + code;
        try {
            msg = context.getResources().getString(
                    context.getResources().getIdentifier(messageCode, Const.STRING,
                            context.getPackageName()));

        } catch (Resources.NotFoundException e) {
            msg = messageCode;
            AppLog.Log(TAG, msg);
            AppLog.handleException(TAG, e);
        }
        showToast(msg, context);
    }

    public static void showCustomProgressDialog(Context context, boolean isCancel)
    {
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        if (isInternetConnected(context) && !((AppCompatActivity) context).isFinishing()) {
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.circuler_progerss_bar_two);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            ivProgressBar = (CustomCircularProgressView) dialog.findViewById(R.id.ivProgressBarTwo);
            ivProgressBar.startAnimation();
            dialog.setCancelable(isCancel);
            WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setAttributes(params);
            dialog.getWindow().setDimAmount(0);
            dialog.show();
        }


    }

    public static void showCustomProgressDialogForHome(Context context, boolean isCancel)
    {
        if (dialogHome != null && dialogHome.isShowing()) {
            return;
        }
        if (isInternetConnected(context) && !((AppCompatActivity) context).isFinishing()) {
            dialogHome = new Dialog(context);
            dialogHome.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogHome.setContentView(R.layout.circuler_progerss_bar_two);
            dialogHome.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            ivProgressBar = (CustomCircularProgressView) dialogHome.findViewById(R.id.ivProgressBarTwo);
            ivProgressBar.startAnimation();
            dialogHome.setCancelable(isCancel);
            WindowManager.LayoutParams params = dialogHome.getWindow().getAttributes();
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            dialogHome.getWindow().setAttributes(params);
            dialogHome.getWindow().setDimAmount(0);
            dialogHome.show();
        }


    }

    public static void hideCustomProgressDialogForHome() {
        try {
            if (dialogHome != null && ivProgressBar != null) {
                dialogHome.dismiss();
            }
        } catch (Exception e) {
            AppLog.handleException(TAG, e);
        }
    }



    public static void showHttpErrorToast(int code, Context context) {
        String msg;
        String errorCode = Const.HTTP_ERROR_CODE_PREFIX + code;
        try {
            msg = context.getResources().getString(
                    context.getResources().getIdentifier(errorCode, Const.STRING,
                            context.getPackageName()));
            showToast(msg, context);
        } catch (Resources.NotFoundException e) {
            msg = errorCode;
            AppLog.Log(TAG, msg);
            AppLog.handleException(TAG, e);
        }


    }

    public static void hideCustomProgressDialog() {
        try {
            if (dialog != null && ivProgressBar != null) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            AppLog.handleException(TAG, e);
        }
    }

    public static boolean isInternetConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService
                (Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();

    }

    public static boolean hasGpsHardware(Context context) {
        PackageManager pm = context.getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
    }

    public static boolean isGpsEnable(Context context) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context
                .LOCATION_SERVICE);

        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap
            // will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable
                    .getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static String trimString(String address) {
        boolean isInteger;
        String original = "";
        if (!address.isEmpty()) {
            String[] strings = address.split(",");
            String s = address.substring(0, 1);
            try {
                Integer.valueOf(s);
                isInteger = true;


            } catch (NumberFormatException e) {
                isInteger = false;
            }

            int stringLength = strings.length;
            AppLog.Log("StringLenth", stringLength + "");

            if (isInteger) {
                switch (stringLength) {
                    case 1:
                    case 2:
                        original = address;
                        break;
                    default:
                        original += strings[0] + "," + strings[1];
                        break;
                }


            } else {
                switch (stringLength) {
                    case 1:
                        original = address;
                        break;
                    case 2:
                        original = strings[0];
                        break;
                    default:
                        original = strings[0] + "," + strings[1];
                        break;
                }
            }

        }


        return original.trim();
    }

    public static boolean hasAnyPrefix(String number, String... prefixes) {
        if (number == null) {
            return false;
        }
        for (String prefix : prefixes) {
            if (number.startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }





   /* public static String secondToHoursMinutesSeconds(double second) {
        int p1 = (int) (second % 60);
        int p2 = (int) (second / 60);
        int p3 = p2 % 60;
        p2 = p2 / 60;
        String finalTime  = String.format("%02d:%02d:%02d", p2, p3, p1);
        return finalTime;
    }*/

    //Utils.convertToHHMMSSFormate(339.1);
    public static String convertToHHMMSSFormate(double second)
    {
        int total_second = (int) second;
        Log.d("","======== Total Second INT ========> "+total_second);
        int hours = (int) Math.floor(total_second / 3600);
        int minute = (int) Math.floor((total_second % 3600) / 60);
        int seconds = (int) Math.abs(Math.floor((minute * 60) - total_second));
        String finalTime  = String.format("%02d:%02d", hours, minute);
        Log.d("response","======== Final Formate ========> "+finalTime);
        return finalTime;
    }


    public static boolean isBlank(String value) {
        return value == null || value.trim().length() == 0;
    }

    private static void goToLoginActivity(Context context) {
        Intent loginIntent = new Intent(context, LoginActivity.class);
       /* loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);*/
        PreferenceHelper.getInstance(context).logout();
        PreferenceHelper.getInstance(context).putAndroidId(Utils.generateRandomString());
        context.startActivity(loginIntent);
    }

    public static String getDayOfMonthSuffix(final int n) {
        if (n >= 11 && n <= 13) {
            return n + "th";
        }
        switch (n % 10) {
            case 1:
                return n + "st";
            case 2:
                return n + "nd";
            case 3:
                return n + "rd";
            default:
                return n + "th";
        }
    }

    public static String getStringPriceAndTag(ArrayList<String> tags, int code, String currency) {
        StringBuilder msg = new StringBuilder();
        for (int i = 0; i < code; i++) {
            msg.append(currency);
        }
        for (String s : tags) {
            msg.append(" \u2022 ");
            msg.append(s);

        }
        return msg.toString();
    }

    public static long maxTime(String serverTime, String timeZone) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Const.DATE_TIME_FORMAT_WEB);
        Date date = null;
        try {
            date = simpleDateFormat.parse(serverTime);
            TimeZone timeZone1 = TimeZone.getTimeZone(timeZone);
            long millis = date.getTime() + (timeZone1.getOffset(date.getTime()));
            return millis > System.currentTimeMillis() ? millis : System.currentTimeMillis();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static String minuteToHoursMinutesSeconds(double second)
    {
       //long seconds = (long) (minute * 60);
       // return (seconds / 3600) + " : " + (seconds % 3600) / 60;
        Log.d("call_status","======== Total Estimated APi Value ========> "+second);
        int total_second = (int) second*60;
        Log.d("call_status","======== Total Second INT ========> "+total_second);
        int hours = (int) Math.floor(total_second / 3600);
        int minute = (int) Math.floor((total_second % 3600) / 60);
        int seconds = (int) Math.abs(Math.floor((minute * 60) - total_second));
        String finalTime  = String.format("%02d:%02d", hours, minute);
        Log.d("response","======== Final Formate ========> "+finalTime);
        return finalTime;
    }

    public static void hideSoftKeyboard(AppCompatActivity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void setTagBackgroundRtlView(Context context, View view) {
        if (context.getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
            view.setBackground(AppCompatResources.getDrawable(context, R.drawable
                    .selector_round_rect_shape_red_mirror));
        } else {
            view.setBackground(AppCompatResources.getDrawable(context, R.drawable
                    .selector_round_rect_shape_red));
        }

    }

    public static StoreClosedResult checkStoreOpenAndClosed(Context context, List<StoreTime>
            storeTime, String serverTime, String timeZoneString, boolean isFutureOrder, long
                                                                    futureTimeMillis) {
        StoreClosedResult storeClosedResult = new StoreClosedResult();
        storeClosedResult.setReOpenAt(context.getResources().getString(R.string.text_open));

        try {
            Calendar serverTimeCalendar = Calendar.getInstance();
            if (isFutureOrder) {
                AppLog.Log("FUTURE_TIME_MILLI", futureTimeMillis + "");
                serverTimeCalendar.setTimeInMillis(futureTimeMillis);
            } else {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Const
                        .DATE_TIME_FORMAT_WEB);
                Date date = simpleDateFormat.parse(serverTime);
                AppLog.Log("SERVER_DATE", date.toString());
                AppLog.Log("TIME_ZONE", CurrentBooking.getInstance()
                        .getTimeZone());
                TimeZone timeZone = TimeZone.getTimeZone(timeZoneString);
                serverTimeCalendar.setTimeInMillis(date.getTime() + (timeZone.getOffset(date
                        .getTime())));
            }

            AppLog.Log("DAY_OF_WEEK", serverTimeCalendar.get(Calendar.DAY_OF_WEEK) + "");
            AppLog.Log("SERVER_DATE_TIME_ZONE", serverTimeCalendar.getTime() + "");
            String nextOpenTime = "";
            boolean isStoreClosed = false;
            int dayOfWeek = serverTimeCalendar.get(Calendar.DAY_OF_WEEK) - 1;
            for (StoreTime timeItem : storeTime) {
                AppLog.Log("STORE_DAY", timeItem.getDay() + "");
                if (timeItem.getDay() == dayOfWeek) {
                    if (timeItem.isStoreOpenFullTime()) {
                        isStoreClosed = false;
                        break;
                    } else {
                        if (timeItem.isStoreOpen()) {
                            if (timeItem.getDayTime().isEmpty()) {
                                isStoreClosed = true;
                            } else {
                                for (DayTime dayTime : timeItem.getDayTime()) {

                                    String[] open = dayTime.getStoreOpenTime().split(":");
                                    String[] closed = dayTime.getStoreCloseTime().split(":");

                                    Calendar openCalendar = Calendar.getInstance();
                                    openCalendar.setTimeInMillis(serverTimeCalendar
                                            .getTimeInMillis());
                                    openCalendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf
                                            (open[0]));
                                    openCalendar.set(Calendar.MINUTE, Integer.valueOf(open[1]));
                                    openCalendar.set(Calendar.SECOND, 0);


                                    Calendar closedCalendar = Calendar.getInstance();
                                    closedCalendar.setTimeInMillis(serverTimeCalendar
                                            .getTimeInMillis());
                                    closedCalendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf
                                            (closed[0]));
                                    closedCalendar.set(Calendar.MINUTE, Integer.valueOf(closed[1]));
                                    closedCalendar.set(Calendar.SECOND, 0);


                                    AppLog.Log("OPEN_TIME", openCalendar.getTime() + "");
                                    AppLog.Log("CLOSED_TIME", closedCalendar.getTime() + "");

                                    if (serverTimeCalendar.after(openCalendar) &&
                                            serverTimeCalendar.before
                                                    (closedCalendar)) {
                                        isStoreClosed = false;
                                        break;
                                    } else if (serverTimeCalendar.before(openCalendar) &&
                                            TextUtils.isEmpty
                                                    (nextOpenTime)) {
                                        isStoreClosed = true;
                                        nextOpenTime = ParseContent.getInstance()
                                                .timeFormat2.format
                                                        (openCalendar.getTimeInMillis());
                                        break;
                                    } else {
                                        isStoreClosed = true;
                                    }

                                }

                            }

                        } else {
                            isStoreClosed = true;
                            break;
                        }

                    }
                    break;
                }

            }

            storeClosedResult.setStoreClosed(isStoreClosed);
            if (isStoreClosed) {
                if (TextUtils.isEmpty(nextOpenTime)) {
                    String currentDate = ParseContent.getInstance().dateFormat.format(new Date());
                    if (currentDate.equals(ParseContent.getInstance().dateFormat.format
                            (serverTimeCalendar.getTime()))) {
                        storeClosedResult.setReOpenAt(context.getResources().getString(R.string
                                .text_store_closed_on) + " " + context.getResources().getString(R
                                .string
                                .text_today));
                    } else {

                        storeClosedResult.setReOpenAt(context.getResources().getString(R.string
                                .text_store_closed_on) +
                                " " +
                                "" + serverTimeCalendar.getDisplayName(Calendar.DAY_OF_WEEK,
                                Calendar
                                        .LONG,
                                Locale.getDefault()));
                    }


                } else {
                    storeClosedResult.setReOpenAt(context.getResources().getString(R
                            .string
                            .text_reopen_at) + " "
                            + nextOpenTime);
                }
            }


        } catch (ParseException e) {
            AppLog.handleException(StoreAdapter.class.getName(), e);
        }


        return storeClosedResult;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public static boolean isDecimalAndGraterThenZero(String data) {
        try {
            if (Double.valueOf(data) <= 0) {

                return false;
            }
        } catch (NumberFormatException e) {

            return false;
        }
        return true;
    }

    public static int dipToPx(Context c, float dipValue) {
        DisplayMetrics metrics = c.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

    public static String generateRandomString() {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
                .toCharArray();
        StringBuilder sb = new StringBuilder(20);
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public static void openWebPage(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, URLUtil.isValidUrl(url) ? Uri.parse(url) :
                Uri.parse("http://" + url));
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }


    public static double getDistance(LatLng start, LatLng end, boolean isMile) {
        Location location1 = new Location("Start");
        Location location2 = new Location("End");
        location1.setLatitude(start.latitude);
        location1.setLongitude(start.longitude);
        location2.setLatitude(end.latitude);
        location2.setLongitude(end.longitude);
        float distance = location1.distanceTo(location2);
        if (isMile) {
            return distance * 0.000621371;
        } else {
            return distance * 0.001;
        }
    }


    public static String capitaliseName(String name) {
        String collect[] = name.split(" ");
        String returnName = "";
        for (int i = 0; i < collect.length; i++) {
            collect[i] = collect[i].trim().toUpperCase();
            if (collect[i].isEmpty() == false) {
                returnName = returnName + collect[i].substring(0, 1).toUpperCase() + collect[i].substring(1) + " ";
            }
        }
        return returnName.trim();
    }

}

