package com.edelivery.utils;

/**
 * Created by elluminati on 19-Jan-17.
 */

public class Const {

    //Main Base URL
    //public static String BASE_URL = "https://liveadmin.fasteat.biz/";
    public static String BASE_URL = "https://testadmin.fasteat.biz/";

    /***
     * Error Code
     */

    public static final int MINIMUM_ORDER_AMOUNT = 557;
    public static final boolean COMPLETED_ONBOARDING_PREF_NAME = false;
    public static String STRIPE_RETURN_URL = "stripe://payment_auth";


    /***
     * Google url
     */
    //  public static final String GOOGLE_API_URL = "https://maps.googleapis.com/maps/";


    public static final String GOOGLE_API_URL = " https://api.mapbox.com/";

    //for here map
    public static final String HERE_API_URL = "https://route.ls.hereapi.com/";
    public static final String HERE_API_KEY = "apikey";
    public static final String HERE_WAYPOINT0 = "waypoint0";
    public static final String HERE_WAYPOINT1 = "waypoint1";
    public static final String HERE_MODE = "mode";


    //public static final String MAP_BOX_ACCESS_TOKEN = "pk.eyJ1IjoiZmFzdGVhdCIsImEiOiJjano1MWE3anYwN2RiM2V0NHR1bmJ5cnZuIn0.w_FKK4oeN1Epqr-q4MEnUQ";


    /**
     * Default font scale for used when app font scale change
     */

    public static final float DEFAULT_FONT_SCALE = 1.0f;
    /**
     * Permission requestCode
     */

    public static final int PERMISSION_FOR_LOCATION = 2;
    public static final int PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE = 3;
    public static final int PERMISSION_FOR_CALL = 4;
    /**
     * App IntentId
     */

    public static final int HOME_ACTIVITY = 2;
    public static final int LOGIN_ACTIVITY = 4;
    public static final int ORDER_TRACK_ACTIVITY = 5;
    /**
     * App result
     */
    public static final int DELIVERY_LIST_CODE = 2;
    public static final int ACTION_SETTINGS = 4;
    public static final int LOGIN_REQUEST = 16;
    public static final int DOCUMENT_REQUEST = 17;
    public static final int FEEDBACK_REQUEST = 18;
    public static final int REQUEST_CHECK_SETTINGS = 32;
    public static final int REQUEST_STORE_RATING = 44;
    public static final int REQUEST_PROVIDER_RATING = 45;
    public static final int STORE_LOCATION_RESULT = 46;
    public static final int STORE_LOCATION_RESULT_CHECKOUT = 47;
    public static final int STORE_LOCATION_RESULT_CHECKOUT_NEWUSER = 48;
    public static final int ADD_STRIPE_CARD = 49;
    public static final int GOFOR_CONFIRM_OTP = 51;


    public static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 53;

    public static final int REQUEST_CODE_AUTOCOMPLETE = 1000;
    public static final String LATITUDE = "latitude";

    public static final String LONGITUDE = "longitude";
    public static final String ADDRESS_MAPBOX = "ADDRESS_MAPBOX";
    public static final int REQUEST_CODE = 5678;

    public static final String currentBooking = "currentBooking";

    /**
     * Timer Scheduled in Second for display ADS
     */

    public static final long ADS_SCHEDULED_SECONDS = 5;//seconds
    /**
     * App General
     */

    public static final String PROVIDER_DETAIL = "PROVIDER_DETAIL";
    public static final String STORE_DETAIL = "STORE_DETAIL";
    public static final String GO_TO_INVOICE = "GO_TO_INVOICE";
    public static final int SMS_VERIFICATION_ON = 1;
    public static final int EMAIL_VERIFICATION_ON = 2;
    public static final int SMS_AND_EMAIL_VERIFICATION_ON = 3;
    public static final String SELECTED_STORE = "selected_store";
    public static final String FILTER = "filter";
    public static final String PRODUCT_ITEM = "product_item";
    public static final String PRODUCT_DETAIL = "product_detail";
    public static final String DELIVERY_STORE = "delivery_store";

    public static final int TYPE_SPECIFICATION_MULTIPLE = 2;
    public static final int TYPE_SPECIFICATION_SINGLE = 1;
    public static final String ERROR_CODE_PREFIX = "error_code_";
    public static final String MESSAGE_CODE_PREFIX = "message_code_";
    public static final String PUSH_MESSAGE_PREFIX = "push_message_";
    public static final String HTTP_ERROR_CODE_PREFIX = "http_error_";
    public static final String STRING = "string";
    public static final String ANDROID = "android";
    public static final String MANUAL = "manual";
    public static final String SOCIAL = "social";
    public static final String DATE_TIME_FORMAT_WEB = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final String TIME_FORMAT_AM = "h:mm a";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_2 = "MM-dd-yyyy";
    public static final String DATE_FORMAT_3 = "dd MMM yy";
    public static final String TIME_FORMAT_2 = "HH:mm";
    public static final String DATE_FORMAT_MONTH = "MMM yyyy";
    public static final String DATE_FORMAT_MONTH_FULL = "MMMM yyyy";
    public static final String DATE_TIME_FORMAT_AM = "yyyy-MM-dd h:mm a";
    public static final String DAY = "d";
    public static final int INVALID_TOKEN = 999;
    public static final int USER_DATA_NOT_FOUND = 534;
    public static final String GO_TO_HOME = "GO_TO_HOME";
    public static final String BUNDLE = "bundle";
    public static final String UPDATE_ITEM_INDEX = "update_item_index";
    public static final String UPDATE_ITEM_INDEX_SECTION = "update_item_index_section";

    public class Params {
        public static final String message = "message";
        public static final String Otp_data = "otp";
        public static final String TEST = "test";
        public static final String Registartion_data = "Registartion_Data";
        public static final String Normal_Email = "Normal_Email";
        public static final String Social_facebook = "Social_facebook";
        public static final String Social_twitter = "Social_twitter";
        public static final String apple = "apple";
        public static final String Social_Gmail = "Social_Gmail";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String EMAIL = "email";
        public static final String PASS_WORD = "password";
        public static final String LOGIN_BY = "login_by";
        public static final String COUNTRY_PHONE_CODE = "country_phone_code";
        public static final String PHONE = "phone";
        public static final String REFERRAL_CODE = "referral_code";
        public static final String ADDRESS = "address";
        public static final String DESTINATION_ADDRESSES = "destination_addresses";
        public static final String ZIP_CODE = "zipcode";
        public static final String COUNTRY_ID = "country_id";
        public static final String CITY_ID = "city_id";
        public static final String CITY = "city";
        public static final String POSTCODE = "post_code";
        public static final String STORE_DELIVERY_ID = "store_delivery_id";
        public static final String SERVER_TOKEN = "server_token";
        public static final String DEVICE_TOKEN = "device_token";
        public static final String DEVICE_TYPE = "device_type";
        public static final String IMAGE_URL = "image_url";
        public static final String TYPE = "type";
        public static final String USER_ID = "user_id";
        public static final String COUNTRY = "country";
        public static final String ADDRESS_1 = "address_1";
        public static final String ADDRESS_2 = "address_2";
        public static final String PARAM_ADDRESS_ID = "_id";
        public static final String NOTE = "note";


        public static final String COUNTRY_name = "country_name";
        public static final String COUNTRY_CODE = "country_code";
        public static final String COUNTRY_CODE_2 = "country_code_2";
        public static final String CITY_CODE = "city_code";
        public static final String CITY1 = "city1"; // city
        public static final String CITY2 = "city2"; // subAdminArea
        public static final String CITY3 = "city3"; // adminArea
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String OLD_PASS_WORD = "old_password";
        public static final String NEW_PASS_WORD = "new_password";
        public static final String STORE_ID = "store_id";
        public static final String TOTAL_ITEM_COUNT = "total_item_count";
        public static final String TOTAL_DISTANCE = "total_distance";
        public static final String TOTAL_TIME = "total_time";
        public static final String TOTAL_TIME_SECOND = "total_sec";
        public static final String TOTAL_CART_PRICE = "total_cart_price";
        public static final String IS_PAYMENT_MODE_CASH = "is_payment_mode_cash";
        public static final String TOTAL_SPECIFICATION_COUNT = "total_specification_count";
        public static final String WALLET = "wallet";
        public static final String CARD_NUMBER = "card_number";
        public static final String PAYMENT_TOKEN = "payment_token";
        public static final String CARD_ID = "card_id";
        public static final String PAYMENT_ID = "payment_id";
        public static final String TRANSACTION_ID = "transaction_id";
        public static final String LAST_FOUR = "last_four";
        public static final String CARD_TYPE = "card_type";
        public static final String CARD_CVV = "card_cvv";
        public static final String IS_USE_WALLET = "is_use_wallet";
        public static final String ORDER_PAYMENT_ID = "order_payment_id";
        public static final String WALLET_PAYMENT = "wallet_payment";
        public static final String AMOUNT_ID = "amount";
        public static final String COUNTRY_CURRENCY = "country_currency";
        public static final String COUNTRY_CURRENCY_CODE = "country_currency_code";
        public static final String CART_ID = "cart_id";
        public static final String ORDER_ID = "order_id";
        public static final String ORDER = "order";
        public static final String ORDER_STATUS = "order_status";
        public static final String PUSH_DATA1 = "push_data1";
        public static final String PUSH_DATA2 = "push_data2";
        public static final String NEW_ORDER = "new_order";
        public static final String UNIQUE_CODE = "unique_code";
        public static final String CANCEL_REASON = "cancel_reason";
        public static final String ID = "id";
        public static final String DOCUMENT_ID = "document_id";
        public static final String EXPIRED_DATE = "expired_date";
        public static final String START_DATE = "start_date";
        public static final String END_DATE = "end_date";

        public static final String SOCIAL_ID = "social_id";
        public static final String RATING = "rating";
        public static final String REVIEW = "review";
        public static final String PROVIDER_ID = "provider_id";
        public static final String IS_PHONE_NUMBER_VERIFIED = "is_phone_number_verified";
        public static final String IS_EMAIL_VERIFIED = "is_email_verified";
        public static final String APP_VERSION = "app_version";
        public static final String TOTAL_ITEM_PRICE = "total_item_price";
        public static final String TOTAL_SPECIFICATION_PRICE = "total_specification_price";
        public static final String PROMO_CODE_NAME = "promo_code_name";
        public static final String IS_USER_SHOW_INVOICE = "is_user_show_invoice";
        public static final String USER_RATING_TO_PROVIDER = "user_rating_to_provider";
        public static final String USER_REVIEW_TO_PROVIDER = "user_review_to_provider";
        public static final String USER_RATING_TO_STORE = "user_rating_to_store";
        public static final String USER_REVIEW_TO_STORE = "user_review_to_store";
        public static final String CART_UNIQUE_TOKEN = "cart_unique_token";
        public static final String NOTE_FOR_DELIVERYMAN = "note_for_deliveryman";
        public static final String IS_SCHEDULE_ORDER = "is_schedule_order";
        public static final String ORDER_START_AT = "order_start_at";
        public static final String IS_USER_PICK_UP_ORDER = "is_user_pick_up_order";
        public static final String REVIEW_ID = "review_id";
        public static final String
                IS_USER_CLICKED_LIKE_STORE_REVIEW = "is_user_clicked_like_store_review";
        public static final String
                IS_USER_CLICKED_DISLIKE_STORE_REVIEW = "is_user_clicked_dislike_store_review";
        public static final String ORDER_TYPE = "order_type";
        public static final String CARD_EXPIRY_DATE = "card_expiry_date";
        public static final String CARD_HOLDER_NAME = "card_holder_name";

        public static final String ADDRESS_NAME = "address_name";
        public static final String ADDRESS_ID = "address_id";

        public static final String VERSION = "version";
        public static final int version_value = 1;
    }

    /**
     * all activity and fragment TAG for log
     */
    public class Tag {
        public static final String HOME_FRAGMENT = "HOME_FRAGMENT";
        public static final String HIDE_DELETE = "HIDE_DELETE";
        public static final String POSTCODE = "postcode";
        public static final String STORE_FRAGMENT = "STORE_FRAGMENT";
        public static final String REGISTER_FRAGMENT = "REGISTER_FRAGMENT";
        public static final String LOG_IN_FRAGMENT = "LOG_IN_FRAGMENT";
        public static final String SPLASH_SCREEN_ACTIVITY = "SPLASH_SCREEN_ACTIVITY";
        public static final String DELIVERY_LOCATION_ACTIVITY = "DELIVERY_LOCATION_ACTIVITY";
        public static final String USER_FRAGMENT = "USER_FRAGMENT";
        public static final String STORES_ACTIVITY = "STORES_ACTIVITY";
        public static final String STORES_PRODUCT_ACTIVITY = "STORES_PRODUCT_ACTIVITY";
        public static final String PROFILE_ACTIVITY = "PROFILE_ACTIVITY";
        public static final String CHECKOUT_ACTIVITY = "CHECKOUT_ACTIVITY";
        public static final String CART_ACTIVITY = "CART_ACTIVITY";
        public static final String PAYMENT_ACTIVITY = "PAYMENT_ACTIVITY";
        public static final String PAYMENT_ACTIVITY_Invoice = "PAYMENT_ACTIVITY_invoice";
        public static final String PRODUCT_SPE_ACTIVITY = "PRODUCT_SPE_ACTIVITY";
        public static final String ORDER_TRACK_ACTIVITY = "ORDER_TRACK_ACTIVITY";
        public static final String CURRENT_ORDER_FRAGMENT = "CURRENT_ORDER_FRAGMENT";
        public static final String DOCUMENT_ACTIVITY = "DOCUMENT_ACTIVITY";
        public static final String PROVIDER_TRACK_ACTIVITY = "PROVIDER_TRACK_ACTIVITY";
        public static final String INVOICE_FRAGMENT = "INVOICE_FRAGMENT";
        public static final String FEEDBACK_FRAGMENT = "FEEDBACK_FRAGMENT";
        public static final String ORDER_FRAGMENT = "ORDER_FRAGMENT";
    }

    public class OrderStatus {


        public static final int WAITING_FOR_ACCEPT_STORE = 1;
        public static final int STORE_ORDER_ACCEPTED = 3;
        public static final int STORE_ORDER_PREPARING = 5;
        public static final int STORE_ORDER_READY = 7;
        public static final int STORE_ORDER_REJECTED = 103;
        public static final int STORE_ORDER_CANCELLED = 104;
        public static final int STORE_CANCELLED_REQUEST = 105;
        public static final int WAITING_FOR_DELIVERY_MEN = 9;
        public static final int DELIVERY_MAN_ACCEPTED = 11;
        public static final int DELIVERY_MAN_COMING = 13;
        public static final int DELIVERY_MAN_ARRIVED = 15;
        public static final int DELIVERY_MAN_PICKED_ORDER = 17;
        public static final int DELIVERY_MAN_STARTED_DELIVERY = 19;
        public static final int DELIVERY_MAN_ARRIVED_AT_DESTINATION = 21;
        public static final int DELIVERY_MAN_COMPLETE_DELIVERY = 23;
        public static final int FINAL_ORDER_COMPLETED = 25;
        public static final int DELIVERY_MAN_NOT_FOUND = 109;
        public static final int DELIVERY_MAN_REJECTED = 111;
        public static final int DELIVERY_MAN_CANCELLED = 112;
        public static final int ORDER_CANCELED_BY_USER = 101;


    }

    public class Wallet {
        public static final int ADDED_BY_ADMIN = 1;
        public static final int ADDED_BY_CARD = 2;
        public static final int ADDED_BY_REFERRAL = 3;
        public static final int ORDER_CHARGED = 4;
        public static final int ORDER_REFUND = 5;
        public static final int ORDER_PROFIT = 6;
        public static final int ORDER_CANCELLATION_CHARGE = 7;
        public static final int WALLET_REQUEST_CHARGE = 8;

        public static final int WALLET_STATUS_CREATED = 1;
        public static final int WALLET_STATUS_ACCEPTED = 2;
        public static final int WALLET_STATUS_TRANSFERRED = 3;
        public static final int WALLET_STATUS_COMPLETED = 4;
        public static final int WALLET_STATUS_CANCELLED = 5;

        public static final int ADD_WALLET_AMOUNT = 1;
        public static final int ORDER_REFUND_AMOUNT = 3;
        public static final int ORDER_PROFIT_AMOUNT = 5;
        public static final int REMOVE_WALLET_AMOUNT = 2;
        public static final int ORDER_CHARGE_AMOUNT = 4;
        public static final int ORDER_CANCELLATION_CHARGE_AMOUNT = 6;
        public static final int REQUEST_CHARGE_AMOUNT = 8;
        public static final int ORDER_PROFIT_DEDUCT_AMOUNT = 10;
    }

    public class Store {
        public static final int STORE_PRICE_ONE = 1;
        public static final int STORE_PRICE_TWO = 2;
        public static final int STORE_PRICE_THREE = 3;
        public static final int STORE_PRICE_FOUR = 4;

        public static final int STORE_TIME_20 = 20;
        public static final int STORE_TIME_60 = 60;
        public static final int STORE_TIME_120 = 120;

        public static final int STORE_DISTANCE_5 = 5;
        public static final int STORE_DISTANCE_15 = 15;
        public static final int STORE_DISTANCE_25 = 25;
    }

    public class Payment {
        public static final String CASH = "0";
        public static final String STRIPE = "586f7db95847c8704f537bd5";
        public static final String PAY_PAL = "586f7db95847c8704f537bd6";
        public static final String PAY_U_MONEY = "586f7db95847c8704f537bd";
        public static final String G_PAY = "586f7db95847c8704hewjfhkjkljklj";
    }

    /**
     * App Receiver
     */

    public class Action {

        public static final String NETWORK_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
        public static final String GPS_ACTION = "android.location.PROVIDERS_CHANGED";


        public static final String ACTION_ORDER_STATUS = "edelivery" +
                ".ACTION_ORDER_STATUS";

        public static final String ACTION_MASS_NOTIFICATION = "edelivery" +
                ".ACTION_MASS_NOTIFICATION";

        public static final String SEND_NOTIFICATION = "edelivery" +
                ".SEND_NOTIFICATION";
        public static final String ACTION_ADMIN_DECLINE = "edelivery" +
                ".ACTION_ADMIN_DECLINE";
        public static final String ACTION_ADMIN_APPROVED = "edelivery" +
                ".ACTION_ADMIN_APPROVED";
        public static final String ACTION_DELIVERYMEN_ACCEPTED = "edelivery" +
                ".ACTION_DELIVERYMEN_ACCEPTED";
        public static final String ACTION_DELIVERYMAN_CANCEL = "edelivery" +
                ".ACTION_DELIVERYMAN_CANCEL";

    }

    /**
     * Google params
     */


    public class google {
        public static final String OK = "OK";
        public static final String STATUS = "status";
        public static final String RESULTS = "results";
        public static final String GEOMETRY = "geometry";
        public static final String LOCATION = "location";
        public static final String ADDRESS_COMPONENTS = "address_components";
        public static final String LONG_NAME = "long_name";
        public static final String ADMINISTRATIVE_AREA_LEVEL_2 = "administrative_area_level_2";
        public static final String ADMINISTRATIVE_AREA_LEVEL_1 = "administrative_area_level_1";

        public static final String COUNTRY = "country";
        public static final String COUNTRY_CODE = "country_code";
        public static final String SHORT_NAME = "short_name";
        public static final String TYPES = "types";
        public static final String LOCALITY = "locality";
        public static final String LAT = "lat";
        public static final String LNG = "lng";
        public static final String DESTINATION_ADDRESSES = "destination_addresses";
        public static final String ORIGIN_ADDRESSES = "origin_addresses";
        public static final String ROWS = "rows";
        public static final String ELEMENTS = "elements";
        public static final String DISTANCE = "distance";
        public static final String VALUE = "value";
        public static final String DURATION = "duration";
        public static final String ORIGINS = "origins";
        public static final String DESTINATIONS = "destinations";
        public static final String KEY = "key";
        public static final String LAT_LNG = "latlng";
        public static final String ADDRESS = "address";
        public static final String FORMATTED_ADDRESS = "formatted_address";
        public static final int GOOGLE_SIGN_IN = 21;
    }

    public class Facebook {
        public static final String EMAIL = "email";
        public static final String PUBLIC_PROFILE = "public_profile";
        public static final String FB_CURRENCY_CODE = "GBP";
    }

    public class PayPal {
        public static final int REQUEST_CODE_ORDER_PAYMENT = 11;
        public static final int REQUEST_CODE_WALLET_PAYMENT = 22;
        public static final String MERCHANT_PRIVACY_POLICY = "https://www.elluminatiinc.com";
        public static final String MERCHANT_AGREEMENT = "https://www.elluminatiinc.com";
    }


    public class Type {
        public static final int USER = 7;
        public static final int STORE = 2;
        public static final String DESTINATION = "destination";
        public static final String PICKUP = "pickup";
    }

    public class Hereapi {
        public static final int size = 10;
        public static final String apikey = "ZFh5cT9zRMSGP1sJoMn2CNJY2khTqmGi1LfXhK0szsU";
    }

    public class cardbrand {
        public static final String AMERICAN_EXPRESS = "American Express";
        public static final String DISCOVER = "Discover";
        public static final String JCB = "JCB";
        public static final String DINERS_CLUB = "Diners Club";
        public static final String VISA = "Visa";
        public static final String MASTERCARD = "MasterCard";
        public static final String UNIONPAY = "UnionPay";
        public static final String UNKNOWN = "Unknown";


    }


}
