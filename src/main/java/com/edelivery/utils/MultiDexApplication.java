package com.edelivery.utils;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

/**/

/*import io.fabric.sdk.android.Fabric;*/

public class MultiDexApplication extends Application
{
    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
       /* Fabric.with(this, new Crashlytics());*/
    }
}
