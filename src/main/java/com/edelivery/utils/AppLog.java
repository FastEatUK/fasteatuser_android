package com.edelivery.utils;


import android.util.Log;

import com.edelivery.BuildConfig;

/**
 * Created by elluminati on 30-01-2017.
 */
public class AppLog {

    public static final boolean isDebug = BuildConfig.DEBUG;

    public static final void Log(String tag, String message) {
        if (isDebug) {
            android.util.Log.i(tag, message + "");
        }else {
            if (message != null) {
                Log.e("Log( => ", message);
            }
        }
    }

    public static final void handleException(String tag, Exception e) {
        if (isDebug) {
            if (e != null) {
                android.util.Log.d(tag, e + "");
            }
        }else {
            if (e != null) {
                Log.e("handleException => ", e.getMessage());
            }
        }
    }

    public static final void handleThrowable(String tag, Throwable t) {
        if (isDebug) {
            if (t != null) {
                android.util.Log.d(tag, t + "");
                Log.e("handleThrowable => " , t.getMessage());
            }
        }else {
            if (t != null) {
                Log.e("handleThrowable => " ,t.getMessage());
            }
        }
    }
}
