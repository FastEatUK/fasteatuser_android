package com.edelivery;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import android.util.Log;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.LinearLayout;

import com.edelivery.adapter.CartProductAdapter;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.datamodels.CartOrder;
import com.edelivery.models.datamodels.CartProductItems;
import com.edelivery.models.datamodels.CartProducts;
import com.edelivery.models.datamodels.ProductDetail;
import com.edelivery.models.datamodels.ProductItem;
import com.edelivery.models.datamodels.SpecificationSubItem;
import com.edelivery.models.datamodels.Specifications;
import com.edelivery.models.responsemodels.AddCartResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends BaseAppCompatActivity {


    private RecyclerView rcvCart;
    private CustomFontTextView tvCartTotal, tvStoreName;
    private LinearLayout btnCheckOut;
    private CartProductAdapter cartProductAdapter;
    private LinearLayout ivEmpty;
    public String str_currency;
    HomeActivity homeActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //homeActivity.toolbar.setVisibility(View.VISIBLE);
       /* FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplicationContext());*/
        logger = (AppEventsLogger) AppEventsLogger.newLogger(getApplicationContext());

        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_basket));
        findViewById();
        setViewListener();
        initRcvCart();
        clearUiCheckoutButtonList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (cartProductAdapter != null) {
            modifyTotalAmount();
            cartProductAdapter.notifyDataSetChanged();
        }
        updateUiCartList();

    }

    @Override
    protected boolean isValidate() {
        // do something
        return false;
    }

    @Override
    protected void findViewById() {
        // do something
        rcvCart = (RecyclerView) findViewById(R.id.rcvCart);
        tvCartTotal = (CustomFontTextView) findViewById(R.id.tvCartTotal);
        tvStoreName = (CustomFontTextView) findViewById(R.id.tv_store_name);
        btnCheckOut = (LinearLayout) findViewById(R.id.btnCheckOut);
        ivEmpty = (LinearLayout) findViewById(R.id.ivEmpty);

        /*    if (currentBooking.getStorename()!= null && !currentBooking.getStorename().equals("")){
                tvStoreName.setText(currentBooking.getStorename());
        }*/
    }

    @Override
    protected void setViewListener() {
        // do something
        btnCheckOut.setOnClickListener(this);

    }

    @Override
    protected void onBackNavigation() {
        // do something
        onBackPressed();

    }

    @Override
    public void onClick(View view) {
        // do something
        switch (view.getId()) {
            case R.id.btnCheckOut:
                if (currentBooking.getCartProductWithSelectedSpecificationList().isEmpty()) {
                    Utils.showToast(getResources().getString(R.string.msg_no_in_cart), this);
                } else {
                    //addItemInServerCart(true);
                    if (isCurrentLogin()) {
                        if (currentBooking.getFavAddressList().size() == 0) {
                            OpenFavoriteDialog() ;
                        } else {
                            goToCheckoutActivity();
                        }

                    } else {
                        goToLoginActivityForResult(this);
                    }


                }
                break;

            default:
                // do with default
                break;
        }
    }
    private void OpenFavoriteDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(getString(R.string.you_must_add_address_checkout));
        builder1.setCancelable(false);

        builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                goToFavouriteAddressActivity(false,true);
            }
        });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    private void initRcvCart() {
        cartProductAdapter = new CartProductAdapter(this, currentBooking
                .getCartProductWithSelectedSpecificationList());
        rcvCart.setLayoutManager(new LinearLayoutManager(this));
        rcvCart.setAdapter(cartProductAdapter);
        modifyTotalAmount();
    }

    @Override
    public void onBackPressed() {
       /* super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);*/
        goToHomeActivity();
    }

    /**
     * this method id used to increase particular item quantity in cart
     *
     * @param cartProductItems
     */
    public void increaseItemQuantity(CartProductItems cartProductItems) {
        int quantity = cartProductItems.getQuantity();
        quantity++;
        cartProductItems.setQuantity(quantity);
        cartProductItems.setTotalItemAndSpecificationPrice((cartProductItems
                .getTotalSpecificationPrice()
                + cartProductItems.getItemPrice()) * quantity);
        cartProductItems.setTotalItemTax(cartProductItems.getTotalTax() * quantity);
        cartProductAdapter.notifyDataSetChanged();
        modifyTotalAmount();
        addItemInServerCart(false);
    }

    /**
     * this method id used to decrease particular item quantity in cart
     *
     * @param cartProductItems
     */
    public void decreaseItemQuantity(CartProductItems cartProductItems) {
        int quantity = cartProductItems.getQuantity();
        if (quantity > 1) {
            quantity--;
            cartProductItems.setQuantity(quantity);
            cartProductItems.setTotalItemAndSpecificationPrice((cartProductItems
                    .getTotalSpecificationPrice()
                    + cartProductItems.getItemPrice()) * quantity);
            cartProductItems.setTotalItemTax(cartProductItems.getTotalTax() * quantity);
            cartProductAdapter.notifyDataSetChanged();
            modifyTotalAmount();
            addItemInServerCart(false);
        }
    }

    /**
     * this method id used to remove particular item quantity in cart
     *
     * @param position
     * @param relativePosition
     */
    public void removeItem(int position, int relativePosition) {
        currentBooking.getCartProductWithSelectedSpecificationList().get(position).getItems()
                .remove(relativePosition);
        if (currentBooking.getCartProductWithSelectedSpecificationList().get(position).getItems()
                .isEmpty()) {
            currentBooking.getCartProductWithSelectedSpecificationList().remove(position);
        }
        cartProductAdapter.notifyDataSetChanged();
        modifyTotalAmount();
        if (getCartItemCount() == 0) {
            clearCart();
        } else {
            addItemInServerCart(false);
        }

    }

    private int getCartItemCount() {
        int cartCount = 0;
        for (CartProducts cartProducts : currentBooking
                .getCartProductWithSelectedSpecificationList()) {
            cartCount = cartCount + cartProducts.getItems().size();
        }
        return cartCount;
    }

    /**
     * this method id used to modify or change  total cart item  amount
     */
    @SuppressLint("SetTextI18n")
    public void modifyTotalAmount() {
        double totalAmount = 0;
        for (CartProducts cartProducts : currentBooking
                .getCartProductWithSelectedSpecificationList()) {
            for (CartProductItems cartProductItems : cartProducts.getItems()) {
                totalAmount = totalAmount + cartProductItems
                        .getTotalItemAndSpecificationPrice();
            }
        }
        currentBooking.setTotalCartAmount(totalAmount);
        /*tvCartTotal.setText(currentBooking.getCartCurrency() + parseContent.decimalTwoDigitFormat
                .format(totalAmount));*/

        if (currentBooking.getCartCurrency() != null && !currentBooking.getCartCurrency().equals("")) {

            if (currentBooking.getCartCurrency().toUpperCase().contains("NULL")) {
                str_currency = currentBooking.getCartCurrency().toUpperCase().replace("NULL", "");
                tvCartTotal.setText(str_currency + parseContent.decimalTwoDigitFormat
                        .format(totalAmount));
            } else {
                str_currency = currentBooking.getCartCurrency();
                tvCartTotal.setText(str_currency + parseContent.decimalTwoDigitFormat
                        .format(totalAmount));
            }
        } else {
            tvCartTotal.setText(parseContent.decimalTwoDigitFormat
                    .format(totalAmount));
        }

    }


    private void goToCheckoutActivity() {
        logInitiatedCheckoutEvent("", "", 0, false, Const.Facebook.FB_CURRENCY_CODE, currentBooking.getTotalCartAmount());
        Intent intent = new Intent(this, CheckoutActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public void logInitiatedCheckoutEvent(String contentId, String contentType, int numItems, boolean paymentInfoAvailable, String currency, double totalPrice) {
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId);
        params.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, contentType);
        params.putInt(AppEventsConstants.EVENT_PARAM_NUM_ITEMS, numItems);
        params.putInt(AppEventsConstants.EVENT_PARAM_PAYMENT_INFO_AVAILABLE, paymentInfoAvailable ? 1 : 0);
        params.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency);
        logger.logEvent(AppEventsConstants.EVENT_NAME_INITIATED_CHECKOUT, totalPrice, params);
    }


    /**
     * this method called a webservice for clear cart
     */
    protected void clearCart() {
        Utils.showCustomProgressDialog(this, false);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        JSONObject jsonObject = getCommonParam();

        try {
            jsonObject.put(Const.Params.CART_ID, currentBooking.getCartId());
            Call<IsSuccessResponse> responseCall = apiInterface.clearCart(ApiClient
                    .makeJSONRequestBody(jsonObject));
            responseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    Utils.hideCustomProgressDialog();
                    clearUiCheckoutButtonList();
                    if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                        currentBooking.clearCart();
                        cartProductAdapter.notifyDataSetChanged();
                        updateUiCartList();
                        Utils.showMessageToast(response.body().getMessage(),
                                CartActivity.this);
                    } else {
                        Utils.showErrorToast(response.body().getErrorCode(),
                                CartActivity.this);
                    }

                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(Const.Tag.CART_ACTIVITY, t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(CartActivity.class.getSimpleName(), e);
        }
    }

    /**
     * this method called a webservice for add item in cart
     */
    private void addItemInServerCart(final boolean goToCheckout) {
        Utils.showCustomProgressDialog(this, false);
        CartOrder cartOrder = new CartOrder();
        cartOrder.setUserType(Const.Type.USER);
        if (isCurrentLogin()) {
            cartOrder.setUserId(preferenceHelper.getUserId());
            cartOrder.setAndroidId("");
        } else {
            cartOrder.setAndroidId(preferenceHelper.getAndroidId());
            cartOrder.setUserId("");
        }
        cartOrder.setServerToken(preferenceHelper.getSessionToken());
        cartOrder.setStoreId(currentBooking.getSelectedStoreId());
        cartOrder.setProducts(currentBooking.getCartProductWithSelectedSpecificationList());
        cartOrder.setDestinationAddresses(currentBooking.getDestinationAddresses());
        cartOrder.setPickupAddresses(currentBooking.getPickupAddresses());
        // add filed on 4_Aug_2018
        double totalCartPrice = 0, totalCartTaxPrice = 0;
        for (CartProducts products : currentBooking.getCartProductWithSelectedSpecificationList()) {
            double totalItemPrice = 0, totalTaxPrice = 0;
            for (CartProductItems cartProductItems : products.getItems()) {
                totalTaxPrice = totalTaxPrice + cartProductItems.getTotalItemTax();
                totalItemPrice = totalItemPrice + cartProductItems
                        .getTotalItemAndSpecificationPrice();
            }
            products.setTotalItemTax(totalTaxPrice);
            products.setTotalProductItemPrice(totalItemPrice);
            totalCartPrice = totalCartPrice + totalItemPrice;
            totalCartTaxPrice = totalCartTaxPrice + totalTaxPrice;

        }
        cartOrder.setCartOrderTotalPrice(totalCartPrice);
        cartOrder.setCartOrderTotalTaxPrice(totalCartTaxPrice);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<AddCartResponse> responseCall = apiInterface.addItemInCart(ApiClient
                .makeGSONRequestBody(cartOrder));
        Log.d("json", "------- " + new Gson().toJson(cartOrder));

        responseCall.enqueue(new Callback<AddCartResponse>() {
            @Override
            public void onResponse(Call<AddCartResponse> call, Response<AddCartResponse>
                    response) {
                AppLog.Log("responce_cartapi", ApiClient.JSONResponse(response.body()));
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    currentBooking.setCartId(response.body().getCartId());
                    currentBooking.setCartCityId(response.body().getCityId());
                    if (goToCheckout) {
                        goToCheckoutActivity();
                    }

                } else {
                    Utils.showErrorToast(response.body().getErrorCode(),
                            CartActivity.this);
                }


            }

            @Override
            public void onFailure(Call<AddCartResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PRODUCT_SPE_ACTIVITY, t);
            }
        });
    }


    private void clearUiCheckoutButtonList() {

        if (currentBooking.getCartProductWithSelectedSpecificationList().isEmpty()) {
            btnCheckOut.setVisibility(View.GONE);
        } else {
            btnCheckOut.setVisibility(View.VISIBLE);
        }
    }

    private void updateUiCartList() {

        if (ivEmpty != null && rcvCart != null && currentBooking.getCartProductWithSelectedSpecificationList().isEmpty()) {
            ivEmpty.setVisibility(View.VISIBLE);
            rcvCart.setVisibility(View.GONE);
        } else {
            if (ivEmpty != null && rcvCart != null) {
                ivEmpty.setVisibility(View.GONE);
                rcvCart.setVisibility(View.VISIBLE);

            }
        }

    }

    public void goToProductSpecificationActivity(int section, int position,
                                                 CartProductItems
                                                         cartProductItems, ProductDetail
                                                         productDetail) {
        Intent intent = new Intent(this, ProductSpecificationActivity.class);
        intent.putExtra(Const.UPDATE_ITEM_INDEX, position);
        intent.putExtra(Const.UPDATE_ITEM_INDEX_SECTION, section);
        intent.putExtra(Const.PRODUCT_DETAIL, productDetail);
        intent.putExtra(Const.PRODUCT_ITEM, matchSelectedCartProductItemToProductItem
                (cartProductItems));
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    private ProductItem matchSelectedCartProductItemToProductItem(CartProductItems
                                                                          cartProductItems) {
        for (ProductItem saveProductItem : currentBooking
                .getCartProductItemWithAllSpecificationList()) {

            if (saveProductItem.getUniqueId() == cartProductItems.getUniqueId()) {
                saveProductItem.setTax(cartProductItems.getTax());
                saveProductItem.setInstruction(cartProductItems.getItemNote());
                saveProductItem.setQuantity(cartProductItems.getQuantity());
                for (Specifications saveSpecification : saveProductItem.getSpecifications()) {
                    for (Specifications specification : cartProductItems.getSpecifications()) {
                        if (saveSpecification.getUniqueId() == specification.getUniqueId()) {
                            for (SpecificationSubItem saveSubSpecification : saveSpecification
                                    .getList()) {
                                saveSubSpecification.setIsDefaultSelected(false);
                                for (SpecificationSubItem specificationSubItem : specification
                                        .getList()) {
                                    if (saveSubSpecification.getUniqueId() ==
                                            specificationSubItem.getUniqueId()) {
                                        saveSubSpecification.setIsDefaultSelected(true);
                                    }

                                }
                            }
                        }

                    }
                }
                return saveProductItem;
            }

        }
        return null;
    }
}
