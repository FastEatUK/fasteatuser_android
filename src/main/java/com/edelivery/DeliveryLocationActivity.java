package com.edelivery;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.text.HtmlCompat;


import com.edelivery.adapter.AutoSuggestAdapter;
import com.edelivery.animation.ResizeAnimation;
import com.edelivery.component.CustomDialogAlert;
import com.edelivery.component.CustomFavAddressDialog;
import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontCheckBox;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.component.CustomFontTextViewTitle;
import com.edelivery.models.datamodels.Addresses;
import com.edelivery.models.datamodels.Deliveries;
import com.edelivery.models.hereresponce.HereDistanceResponce;
import com.edelivery.models.mapboxdirectionmatrix.DirectionMatrixResponse;
import com.edelivery.models.responsemodels.DeliveryStoreResponse;
import com.edelivery.models.responsemodels.FavouriteAddressResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.parser.ParseContent;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.tasks.OnSuccessListener;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.search.AddressFilter;
import com.here.android.mpa.search.AutoSuggest;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.ReverseGeocodeRequest;
import com.here.android.mpa.search.TextAutoSuggestionRequest;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineCallback;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.location.LocationEngineRequest;
import com.mapbox.android.core.location.LocationEngineResult;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.picker.PlacePicker;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.here.android.mpa.search.AutoSuggestPlace;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.edelivery.utils.Const.REQUEST_CHECK_SETTINGS;
import static com.here.android.mpa.search.AutoSuggest.Type.PLACE;

public class DeliveryLocationActivity extends BaseAppCompatActivity implements LocationHelper
        .OnLocationReceived,
        /*PermissionsListener,*/ OnMapReadyCallback/*, MapBoxMapReady */ {

    //for here map
    // permissions request code
    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    /**
     * Permissions that need to be explicitly requested from end user.
     */
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};


    private LocationHelper locationHelper;
    HomeActivity homeActivity;

    public LinearLayout llAsps, llScheduleOrder, llScheduleDate;
    private ImageView ivTargetLocation, ivClearText,
            ivFullScreen, ivFavAddress;
    private CustomFontCheckBox cbAsps, cbScheduleOrder;
    private CustomFontTextView tvNoDeliveryFound;
    private CustomFontTextViewTitle tvScheduleDate, tvScheduleTime;
    private CustomFontButton btnDone;
    private FrameLayout mapFrameLayout;
    private LinearLayout llDeliveryOrder;
    private ArrayList<Deliveries> deliveriesArrayList = new ArrayList<>();
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private CustomDialogAlert customDialogEnable;
    private CustomDialogAlert exitDialog;
    private ArrayList<Addresses> favAddressList = new ArrayList<>();
    private CustomFavAddressDialog customFavAddressDialog;
    public Point origin;
    public Point destination;
    private CustomFontTextView acDeliveryAddress;
    private MapboxMap mapboxMap;
    private LocationEngine locationEngine;
    private PermissionsManager permissionsManager;
    private long DEFAULT_INTERVAL_IN_MILLISECONDS = 1000L;
    private long DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5;
    LocationEngineRequest mLocationEngineRequest;
    private DeliveryLocationActivity.MainActivityLocationCallback callback = new MainActivityLocationCallback(this);
    private MapView mapbox;
    public com.mapbox.mapboxsdk.geometry.LatLng storeLatLng;
    private boolean isCurrentLocationGet = false;
    private String geojsonSourceLayerId = "geojsonSourceLayerId";
    public static double str_Latitude = 0.0, str_Longitude = 0.0;
    String str_subAdminarea11 = null;
    private LatLng currentLatLng;
    private Location currentLocation;
    private ImageView close;
    private List<AutoSuggest> m_autoSuggests = new ArrayList<>();


    //for here map
    private AndroidXMapFragment mapFragment = null;
    private Map map = null;

    private boolean isCameraIdeal = true;
    private boolean isfirstTime = false;
    private boolean success;
    private LinearLayout searchview;
    private AutoSuggestAdapter m_autoSuggestAdapter;
    private ListView listView;
    private EditText editsearch;
    // map limitation
 /*   private static final LatLng BOUND_CORNER_NW = new LatLng( 55.957459,-3.206530 );
    private static final LatLng BOUND_CORNER_SE = new LatLng(51.312089, -0.561721 );
    private static final LatLngBounds RESTRICTED_BOUNDS_AREA = new LatLngBounds.Builder()
            .include(BOUND_CORNER_NW)
            .include(BOUND_CORNER_SE)
            .build();*/
    public boolean isplaceAddress = false;
    private Location lastLocation;

   /* @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            if (mapboxMap.getStyle() != null) {
                enableLocationComponent(mapboxMap.getStyle());
            }
        }
    }*/

   /* @Override
    public void mapBoxMapReady(final MapboxMap mapboxMap, Style style) {
        this.mapboxMap = mapboxMap;
        enableLocationComponent(style);
        mapboxMap.getUiSettings().setAttributionEnabled(false);
        mapboxMap.getUiSettings().setLogoEnabled(false);
        mapboxMap.getUiSettings().setCompassEnabled(false);

        mapboxMap.addOnCameraIdleListener(new MapboxMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng mLatLng = mapboxMap.getCameraPosition().target;

                // mapboxMap.setLatLngBoundsForCameraTarget(RESTRICTED_BOUNDS_AREA);

                if (mLatLng != null) {
                    Geocoder geocoder = new Geocoder(DeliveryLocationActivity.this, Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(
                                mLatLng.getLatitude(),
                                mLatLng.getLongitude(),
                                1);
                        if (addresses != null && addresses.size() > 0 && addresses.get(0) != null) {
                            //Log.e("tagMap", "onResponse: " + addresses.get(0).toString());
                            if (addresses.get(0).getCountryName() != null) {
                                if (isplaceAddress) {
                                    isplaceAddress = false;

                                } else {
                                    isplaceAddress = false;
                                    acDeliveryAddress.setText(addresses.get(0).getAddressLine(0));

                                }


                            }
                            str_Latitude = addresses.get(0).getLatitude();
                            str_Longitude = addresses.get(0).getLongitude();
                            storeLatLng = new LatLng(str_Latitude, str_Longitude);
                            if (addresses.get(0).getSubAdminArea() != null && !addresses.get(0).getSubAdminArea().equals("")) {
                                str_subAdminarea11 = addresses.get(0).getSubAdminArea();
                            } else {
                                str_subAdminarea11 = "";
                            }
                            getGeocodeDataFromAddress(addresses.get(0).getCountryName(), addresses.get(0).getCountryCode(), addresses.get(0).getLocality(), str_subAdminarea11, addresses.get(0).getAdminArea(), storeLatLng, acDeliveryAddress.getText().toString(), addresses.get(0).getLocality());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });


    }*/


   /* @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions =
                    LocationComponentActivationOptions.builder(this, loadedMapStyle)
                            .useDefaultLocationEngine(false)
                            .build();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            initLocationEngine();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }
*/

   /* @SuppressLint("MissingPermission")
    private void initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(this);

        mLocationEngineRequest = new LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build();
        locationEngine.requestLocationUpdates(mLocationEngineRequest, callback, getMainLooper());
        locationEngine.getLastLocation(callback);
    }
*/

    public class MainActivityLocationCallback
            implements LocationEngineCallback<LocationEngineResult> {

        private final WeakReference<DeliveryLocationActivity> activityWeakReference;

        MainActivityLocationCallback(DeliveryLocationActivity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(LocationEngineResult result) {
            DeliveryLocationActivity activity = activityWeakReference.get();

            if (activity != null) {
                Location location = result.getLastLocation();

                if (location == null) {
                    return;
                }

                if (!isCurrentLocationGet) {
                    isCurrentLocationGet = true;
                    Log.i("Deliverylocation", " MainActivityLocationCallback on Success".concat(String.valueOf(location.getLatitude())));
                    //for here map set mainactivity call back
                   // zoomCamera(new LatLng(location.getLatitude(), location.getLongitude()), "");
                }
            }
        }

        @Override
        public void onFailure(@NonNull Exception exception) {
            Log.d("LocationChangeActivity", exception.getLocalizedMessage());
            DeliveryLocationActivity activity = activityWeakReference.get();
            if (activity != null) {
                Toast.makeText(activity, exception.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setListViewData(LatLng latLng, String adress) {


        if (searchview.getVisibility() == View.VISIBLE) {
            Utils.hideSoftKeyboard(DeliveryLocationActivity.this);
            editsearch.getText().clear();
            listView.setAdapter(null);
            searchview.setVisibility(View.GONE);

        }   //

        Log.i("Deliverylocation", "zoomCamera" + latLng.getLatitude() + latLng.getLongitude());
        map.setCenter(new GeoCoordinate(latLng.getLatitude(), latLng.getLongitude()), Map.Animation.NONE);
        map.setZoomLevel(15);
        Log.i("Deliverylocation", "zoomCamera");

        // GeoCoordinate geoCoordinate=map.getCenter();

        if (!adress.isEmpty()) {
            acDeliveryAddress.setText(adress);
            Log.i("adressfrom adapter", adress);
        }
        GeoCoordinate coordinate;
        coordinate = map.getCenter();
        ReverseGeocodeRequest revGecodeRequest = new ReverseGeocodeRequest(coordinate);

        revGecodeRequest.execute(new ResultListener<com.here.android.mpa.search.Location>() {
            @Override
            public void onCompleted(com.here.android.mpa.search.Location location, ErrorCode errorCode) {
                if (errorCode == ErrorCode.NONE) {

//                        etSearch.setText(location.getAddress().toString().replace("\n", " "));
                    String myPlace = "";
                    if (location != null && location.getAddress() != null) {
                        if (!TextUtils.isEmpty(location.getAddress().getText())) {
                            myPlace = location.getAddress().getText();
                        }
                    }
                   if(isfirstTime){
                       acDeliveryAddress.setText(myPlace);
                       isfirstTime = false;
                   }

                    LatLng latLng = new LatLng(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude());
                    getGeocodeDataFromAddress(location.getAddress().getCountryName(), location.getAddress().getCountryCode(),
                            location.getAddress().getCity(), "", "", latLng,
                            location.getAddress().toString().replace("\\n", ","), location.getAddress().getCity());
                } else {


                    currentBooking.setDeliveryAddress("error code:" + errorCode);
                    setDeliveryAddress("error code:" + errorCode);

                }
            }
        });
    }

    public void zoomCamera(LatLng latLng, String adress) {


        if (searchview.getVisibility() == View.VISIBLE) {
            Utils.hideSoftKeyboard(DeliveryLocationActivity.this);
            editsearch.getText().clear();
            listView.setAdapter(null);
            searchview.setVisibility(View.GONE);

        }   //

        Log.i("Deliverylocation", "zoomCamera" + latLng.getLatitude() + latLng.getLongitude());
        map.setCenter(new GeoCoordinate(latLng.getLatitude(), latLng.getLongitude()), Map.Animation.NONE);
        map.setZoomLevel(15);
        Log.i("Deliverylocation", "zoomCamera");

        // GeoCoordinate geoCoordinate=map.getCenter();

        if (!adress.isEmpty()) {
            acDeliveryAddress.setText(adress);
            Log.i("adressfrom adapter", adress);
        } else {
            acDeliveryAddress.setText(currentBooking.getDeliveryAddress());

        }
        getCenterMapLocation();
    }

    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Mapbox.getInstance(getApplicationContext(), getResources().getString(R.string.MAP_BOX_ACCESS_TOKEN));
        setContentView(R.layout.activity_delivery_location);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //for here map
        // setheremap();
        checkPermissions();
        initToolBar();

        initLocationHelper();
        setTitleOnToolBar(getString(R.string.text_delivery_location));
        findViewById();
        setViewListener();

        //for here map


        //mapbox = findViewById(R.id.mapBoxView);
        //mapbox.onCreate(savedInstanceState);

        acDeliveryAddress = (CustomFontTextView) findViewById(R.id.acDeliveryAddress);
        ImageView ivTargetLocation = (ImageView) findViewById(R.id.ivTargetLocation);
        acDeliveryAddress.setOnClickListener(this);
        ImageView ivClearDeliveryAddressTextMap = (ImageView) findViewById(R.id
                .ivClearDeliveryAddressTextMap);
        CustomFontButton btnDone = (CustomFontButton) findViewById(R.id.btnDone);
        //new CustomEventMapBoxView(getApplicationContext(), mapbox, this);


        // mapView.onCreate(savedInstanceState);
        locationHelper = new LocationHelper(this);
        locationHelper.setLocationReceivedLister(this);
        updateUiForOrderSelect(currentBooking.isFutureOrder());
        favAddressList = new ArrayList<>();
        if (!preferenceHelper.getUserId().isEmpty()) {
            ivFavAddress.setVisibility(View.VISIBLE);
        } else {
            ivFavAddress.setVisibility(View.GONE);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (m_autoSuggests != null && m_autoSuggests.size() > 0 && m_autoSuggests.get(position) != null) {
                    switch (m_autoSuggests.get(position).getType()) {
                        case PLACE:

                            AutoSuggestPlace autoSuggestPlace = (AutoSuggestPlace) m_autoSuggests.get(position);
                            LatLng latLng = new LatLng(autoSuggestPlace.getPosition().getLatitude(), autoSuggestPlace.getPosition().getLongitude());
                            String adress = autoSuggestPlace.getVicinity();
                           // zoomCamera(latLng, adress);

                            break;
                        default:
                            break;
                    }

                }
            }
        });


    }


    private void initLocationHelper() {
        try {

            locationHelper = new LocationHelper(this);
            locationHelper.setLocationReceivedLister(this);
        } catch (Exception e) {
            Log.e("initLocationHelper() " , e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // mapbox.onResume();
      //  mapFragment.onResume();


    }

    @Override
    protected void onStart() {
        super.onStart();
        locationHelper.onStart();
        // mapbox.onStart();
      //  mapFragment.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationHelper.onStop();
        // mapbox.onStop();
     //   mapFragment.onStop();

    }

    @Override
    protected void onPause() {
        //mapView.onPause();
        super.onPause();
        // mapbox.onPause();
      //  mapFragment.onPause();
    }


    @Override
    protected void onDestroy() {
        //mapView.onDestroy();
        super.onDestroy();
      //  mapFragment.onDestroy();
//        if (locationEngine != null) {
//            locationEngine.removeLocationUpdates(
//
//          );
//        }
        // mapbox.onDestroy();

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // mapbox.onSaveInstanceState(outState);
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        // mapbox.onLowMemory();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    /*private void setPlaceFilter(String countryCode) {
        if (autocompleteAdapter != null) {
           *//* AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE).setCountry
                            (countryCode)
                    .build();
            autocompleteAdapter.setPlaceFilter(typeFilter);*//*

            locationHelper.getLastLocation(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        LatLng latLng = new LatLng(location.getLatitude(),
                                location.getLongitude());
                        LatLngBounds latLngBounds = new LatLngBounds(
                                latLng, latLng);
                        autocompleteAdapter.setBounds(latLngBounds);
                    }
                }
            });
        }
    }
*/
    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void findViewById() {

        //   mapView = (CustomEventMapView) findViewById(R.id.mapView);
        editsearch = (EditText) findViewById(R.id.edit_query);
        listView = (ListView) findViewById(R.id.list_search);
        close = (ImageView) findViewById(R.id.imgclose);
        searchview = (LinearLayout) findViewById(R.id.atuplacepickerview);
        acDeliveryAddress = findViewById(R.id.acDeliveryAddress);
        llAsps = (LinearLayout) findViewById(R.id.llAsps);
        llScheduleOrder = (LinearLayout) findViewById(R.id.llScheduleOrder);
        llScheduleDate = (LinearLayout) findViewById(R.id.llScheduleDate);
        cbScheduleOrder = (CustomFontCheckBox) findViewById(R.id.cbScheduleOrder);
        cbAsps = (CustomFontCheckBox) findViewById(R.id.cbAsps);
        tvScheduleDate = (CustomFontTextViewTitle) findViewById(R.id.tvScheduleDate);
        tvScheduleTime = (CustomFontTextViewTitle) findViewById(R.id.tvScheduleTime);
        ivTargetLocation = (ImageView) findViewById(R.id.ivTargetLocation);
        ivClearText = (ImageView) findViewById(R.id
                .ivClearDeliveryAddressTextMap);
        mapFrameLayout = (FrameLayout) findViewById(R.id.mapFrameLayout);
        llDeliveryOrder = (LinearLayout) findViewById(R.id.llDeliveryOrder);
        ivFullScreen = (ImageView) findViewById(R.id
                .ivFullScreen);
        btnDone = (CustomFontButton) findViewById(R.id.btnDone);
        tvNoDeliveryFound = (CustomFontTextView) findViewById(R.id.tvNoDeliveryFound);
        ivFavAddress = (ImageView) findViewById(R.id.ivFavAddress);

    }

    @Override
    protected void setViewListener() {
        cbAsps.setOnClickListener(this);
        cbScheduleOrder.setOnClickListener(this);
        tvScheduleDate.setOnClickListener(this);
        tvScheduleTime.setOnClickListener(this);
        ivTargetLocation.setOnClickListener(this);
        ivClearText.setOnClickListener(this);
        ivClearText.setVisibility(View.GONE);
        ivFullScreen.setOnClickListener(this);
        btnDone.setOnClickListener(this);
        ivFavAddress.setOnClickListener(this);
        acDeliveryAddress.setOnClickListener(this);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editsearch.getText().clear();
                listView.setAdapter(null);
                //searchview.setVisibility(View.GONE);
            }
        });

    }

    @Override
    protected void onBackNavigation() {

        onBackPressed();
    }

    private void updateUiForOrderSelect(boolean isUpdate) {
        if (isUpdate) {
            cbScheduleOrder.setChecked(true);
            llScheduleDate.setVisibility(View.VISIBLE);
            cbAsps.setChecked(false);
            if (!TextUtils.isEmpty(currentBooking.getFutureOrderDate())) {
                tvScheduleDate.setText(currentBooking.getFutureOrderDate());
            } else {
                tvScheduleDate.setText(getResources().getString(R.string.text_schedule_a_date));
            }
            if (!TextUtils.isEmpty(currentBooking.getFutureOrderTime())) {
                tvScheduleTime.setText(currentBooking.getFutureOrderTime());
            } else {
                tvScheduleTime.setText(getResources().getString(R.string.text_set_time));
            }

        } else {
            cbAsps.setChecked(true);
            cbScheduleOrder.setChecked(false);
            llScheduleDate.setVisibility(View.GONE);
            currentBooking.setFutureOrder(false);
            currentBooking.setFutureOrderSelectedTimeUTCMillis(0);
            currentBooking.setFutureOrderSelectedTimeZoneMillis(0);
            currentBooking.setFutureOrderTime("");
            currentBooking.setFutureOrderDate("");
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        //do something
        AppLog.Log(Const.Tag.HOME_FRAGMENT, "GoogleClientLocationChanged");
        currentLocation = location;
        currentLatLng = new LatLng(location.getLatitude(),
                location.getLongitude());
        lastLocation = currentLocation;

    }

    @Override
    public void onConnected(Bundle bundlLe) {
        AppLog.Log(Const.Tag.HOME_FRAGMENT, "GoogleClientConnected");
        lastLocation = locationHelper.getLastLocation();
        locationHelper.startLocationUpdate();
        checkLocationPermission(false, null);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //do something
    }

    @Override
    public void onConnectionSuspended(int i) {
        //do something
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cbAsps:
                updateUiForOrderSelect(false);
                break;
            case R.id.cbScheduleOrder:
                updateUiForOrderSelect(true);
                break;
            case R.id.ivTargetLocation:
               /* if (!locationHelper.isOpenGpsDialog())
                {
                    locationHelper.setOpenGpsDialog(true);

                    locationHelper.setLocationSettingRequest(this, REQUEST_CHECK_SETTINGS, new
                            OnSuccessListener() {
                                @Override
                                public void onSuccess(Object o)
                                {


                                    checkLocationPermission(true,null );
                                }
                            }, new LocationHelper.NoGPSDeviceFoundListener() {
                        @Override
                        public void noFound() {
                            checkLocationPermission(true, null);
                        }
                    });
                }


             /*   isCurrentLocationGet = false;
                if (!locationHelper.isOpenGpsDialog()) {
                    locationHelper.setOpenGpsDialog(true);
                    locationHelper.setLocationSettingRequest(this, REQUEST_CHECK_SETTINGS, new
                            OnSuccessListener() {
                                @Override
                                public void onSuccess(Object o) {
                                    checkLocationPermission(true, null);
                                }
                            }, new LocationHelper.NoGPSDeviceFoundListener() {
                        @Override
                        public void noFound() {
                            checkLocationPermission(true, null);
                        }
                    });
                }*/
                //for
                isfirstTime = true;
                moveCameraFirstMyLocationZoom();


                break;
            case R.id.ivClearDeliveryAddressTextMap:
                acDeliveryAddress.setText("");
                break;
            case R.id.ivFullScreen:
                expandMap();
                break;
            case R.id.btnDone:
                if (!CurrentBooking.getInstance().getDeliveryStoreList().isEmpty()) {
                    if (llScheduleDate.getVisibility() == View.VISIBLE) {
                        if (currentBooking.getFutureOrderSelectedTimeUTCMillis() > 0) {
                            currentBooking.setFutureOrder(true);
                            if (!acDeliveryAddress.getText().toString().isEmpty()) {
                                Log.i("currentBooking", "btnDone-------Address is" + acDeliveryAddress.getText().toString() +
                                        "\n ----------Latlng is-------" + storeLatLng.toString());
                                currentBooking.setDeliveryAddress(acDeliveryAddress.getText().toString());

                                currentBooking.setDeliveryLatLng(storeLatLng);
                            }
                            ParseContent.getInstance().address = acDeliveryAddress.getText().toString();
                            ParseContent.getInstance().latLng = storeLatLng;
                            goToHomeActivity();
                        } else {
                            Utils.showToast(getResources().getString(R.string
                                    .msg_plz_select_schedule_date_first), this);
                        }
                    } else {
                        if (!acDeliveryAddress.getText().toString().isEmpty()) {

                           /* Log.i("currentBooking", "btnDone-------Address is" + acDeliveryAddress.getText().toString() +
                                    "\n ----------Latlng is-------" + storeLatLng.toString());*/
                            currentBooking.setDeliveryAddress(acDeliveryAddress.getText().toString());

                            currentBooking.setDeliveryLatLng(storeLatLng);
                            if (!acDeliveryAddress.getText().toString().isEmpty()) {
                                currentBooking.setDeliveryAddress(acDeliveryAddress.getText().toString());
                                if (storeLatLng != null) {
                                    currentBooking.setDeliveryLatLng(storeLatLng);
                                }
                            }
                        }
                        ParseContent.getInstance().address = acDeliveryAddress.getText().toString();
                        if (storeLatLng != null) {
                            ParseContent.getInstance().latLng = storeLatLng;

                        }
                        goToHomeActivity();
                    }
                } else {
                    Utils.showToast(getResources().getString(R.string
                            .msg_plz_enter_valid_place_address), this);
                }

                break;
            case R.id.tvScheduleDate:
                openDatePickerDialog();
                break;
            case R.id.tvScheduleTime:
                openTimePicker();
                break;
            case R.id.ivFavAddress:
                getFavAddressList();
                break;


            case R.id.acDeliveryAddress:
                /*Intent intent = new PlaceAutocomplete.IntentBuilder()
                        .accessToken(getResources().getString(R.string
                                .MAP_BOX_ACCESS_TOKEN))
                        .placeOptions(PlaceOptions.builder().backgroundColor(Color.parseColor("#EEEEEE")).country("gb").limit(10).language("en")
                                .build())
                        .build(this);
                startActivityForResult(intent, Const.REQUEST_CODE_AUTOCOMPLETE);*/

                // Toast.makeText(getApplicationContext(),"ok",Toast.LENGTH_SHORT).show();
                searchview.setVisibility(View.VISIBLE);
                editsearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (!s.toString().isEmpty()) {

                            doSearch(s.toString());


                        } else {
                            setSearchMode(false);
                        }


                    }
                });
            default:
                // do with default
                break;
        }
    }

    private void setSearchMode(boolean b) {

    }

    private void moveCameraFirstMyLocationZoom() {
        try {
            if (locationHelper != null && locationHelper.getLastLocation() != null) {
                currentLocation = locationHelper.getLastLocation();
                if (currentLocation != null) {
                    currentLatLng = new LatLng(currentLocation.getLatitude(),
                            currentLocation.getLongitude());
//                    isCameraIdeal = false;

                    Log.i("Deliverylocation", "moveCameraFirstMyLocationZoom " + currentLocation.getLatitude() + currentLocation.getLongitude());
                    //zoomCamera(currentLatLng, "");
                    map.setCenter(new GeoCoordinate(currentLatLng.getLatitude(), currentLatLng.getLongitude()), Map.Animation.NONE);
                    map.setZoomLevel(15);
                    Log.i("Deliverylocation", "zoomCamera");

                    // GeoCoordinate geoCoordinate=map.getCenter();
                    acDeliveryAddress.setText(currentBooking.getCurrentAddress());
                    setListViewData(currentLatLng, currentBooking.getCurrentAddress());

                    isCameraIdeal = true;
                }

                locationHelper.setOpenGpsDialog(false);
            } else {
                if (currentBooking != null && currentBooking.getCurrentLatLng() != null &&
                currentBooking.getCurrentLatLng().latitude != 0.0 && currentBooking.getCurrentLatLng().longitude != 0.0) {
                    map.setCenter(new GeoCoordinate(currentBooking.getCurrentLatLng().latitude, currentBooking.getCurrentLatLng().longitude), Map.Animation.NONE);
                    map.setZoomLevel(15);
                    acDeliveryAddress.setText(currentBooking.getCurrentAddress());
                   // setListViewData(new LatLng(currentBooking.getCurrentLatLng().latitude,currentBooking.getCurrentLatLng().latitude), currentBooking.getCurrentAddress());
                    Log.i("Deliverylocation", "moveCameraFirstMyLocationZoom with else " + currentBooking.getCurrentLatLng());
                }else{
                    Log.i("Deliverylocation", "moveCameraFirstMyLocationZoom with else else " + currentBooking.getCurrentLatLng());
                    map.setCenter(new GeoCoordinate(ParseContent.getInstance().latLng.getLatitude(), ParseContent.getInstance().latLng.getLongitude()), Map.Animation.NONE);
                    map.setZoomLevel(15);
                    acDeliveryAddress.setText(currentBooking.getCurrentAddress());
                    setListViewData(ParseContent.getInstance().latLng, currentBooking.getCurrentAddress());
                }
            }
        } catch (Exception e) {
            Log.e("moveCameraFirstMyLocation() " , e.getMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        locationHelper.onStop();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                locationHelper.onStart();
                            }
                        }, 1000);
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        locationHelper.setOpenGpsDialog(false);
                        break;
                }
                break;
           /* case Const.REQUEST_CODE_AUTOCOMPLETE:
                Toast.makeText(getApplicationContext(),"ok REQUEST_CODE_AUTOCOMPLETE",Toast.LENGTH_LONG).show();
                if (data != null) {

                    // Log.d("call", "========= PlaceAutocomplete Result ==========" + feature.geometry().toString());
                    CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);
                    if (selectedCarmenFeature != null) {
                        if (mapboxMap != null) {
                            Style style = mapboxMap.getStyle();
                            if (style != null) {
                                GeoJsonSource source = style.getSourceAs(geojsonSourceLayerId);
                                if (source != null) {
                                    source.setGeoJson(FeatureCollection.fromFeatures(
                                            new Feature[]{Feature.fromJson(selectedCarmenFeature.toJson())}));
                                }
                                isplaceAddress = true;
                                acDeliveryAddress.setText(selectedCarmenFeature.placeName());
                                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                                        new CameraPosition.Builder()
                                                .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                                        ((Point) selectedCarmenFeature.geometry()).longitude()))
                                                .zoom(14)
                                                .build()), 4000);

                               zoomCamera(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                        ((Point) selectedCarmenFeature.geometry()).longitude()), "");
                            }
                        }
                    }
                }
                break;
            case Const.REQUEST_CODE:
                if (data != null) {
                    Toast.makeText(getApplicationContext(),"ok REQUEST_CODE_AUTOCOMPLETE",Toast.LENGTH_LONG).show();
                    CarmenFeature carmenFeature = PlacePicker.getPlace(data);
                    if (carmenFeature.placeName() != null && !carmenFeature.placeName().equals("")) {
                        acDeliveryAddress.setText(carmenFeature.placeName());
                    }
                }*/
            default:
                break;
        }
    }

    /**
     * This method is used to setUpMap option which help to load map as per option
     */
    /*private void setUpMap() {

        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            boolean doNotMoveCameraToCenterMarker = true;

            public boolean onMarkerClick(Marker marker) {
                return doNotMoveCameraToCenterMarker;
            }
        });

        this.googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                AppLog.Log("IS_MAP_TOUCHED", String.valueOf(isMapTouched));
                if (isMapTouched) {
                    AppLog.Log("CAMERA_CHANGE", "called");
                    deliveryLatLng = googleMap.getCameraPosition().target;
                    getGeocodeDataFromLocation(deliveryLatLng);

                }
                isMapTouched = true;

            }

        });
    }
*/
    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    /***
     * this method is used to move camera on map at current position and a isCameraMove is
     * used to decide when is move or not
     */
    public void moveCameraFirstMyLocation(final boolean isAnimate, LatLng latLng) {
        if (latLng == null)
        {
            locationHelper.getLastLocation(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        LatLng latLng = new LatLng(location
                                .getLatitude(),
                                location.getLongitude());
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(latLng).zoom(17).build();

                        if (isAnimate) {
                            mapboxMap.animateCamera(CameraUpdateFactory
                                    .newCameraPosition(cameraPosition));
                        } else {
                            mapboxMap.moveCamera(CameraUpdateFactory
                                    .newCameraPosition(cameraPosition));
                        }
                        locationHelper.setOpenGpsDialog(false);
                    }
                }
            });


        } else {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng).zoom(17).build();

            if (isAnimate) {
                mapboxMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            } else {
                mapboxMap.moveCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }
            locationHelper.setOpenGpsDialog(false);
        }

        LatLng latLngOfMyLocation = null;
        if (latLng == null) {

        } else {
            latLngOfMyLocation = latLng;
        }
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLngOfMyLocation).zoom(9).build();

        locationHelper.setOpenGpsDialog(false);


    }


    private void setDeliveryAddress(String deliveryAddress) {
        acDeliveryAddress.setFocusable(false);
        acDeliveryAddress.setFocusableInTouchMode(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {

            acDeliveryAddress.setText(deliveryAddress);

        } else {

            acDeliveryAddress.setText(deliveryAddress);

        }
        // acDeliveryAddress.setFocusable(true);
        // acDeliveryAddress.setFocusableInTouchMode(true);
    }

    private void checkLocationPermission(boolean isAnimateLocation, LatLng latLng) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission
                            .ACCESS_FINE_LOCATION, android.Manifest.permission
                            .ACCESS_COARSE_LOCATION},
                    Const.PERMISSION_FOR_LOCATION);
        } else {
            //Do the stuff that requires permission...

            //moveCameraFirstMyLocation(isAnimateLocation, latLng);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (grantResults.length > 0) {
            switch (requestCode) {
               /* case Const.PERMISSION_FOR_LOCATION:
                    goWithLocationPermission(grantResults);
                    moveCameraFirstMyLocation(true, null);
                    break;*/

                case Const.PERMISSION_FOR_LOCATION:
                    //goWithLocationPermission(grantResults);
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //Do the stuff that requires permission...
                        moveCameraFirstMyLocation(true, null);
                    }
                    break;

                case REQUEST_CODE_ASK_PERMISSIONS:
                    for (int index = permissions.length - 1; index >= 0; --index) {
                        if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                            // exit the app if one permission is not granted
                            Toast.makeText(this, "Required permission '" + permissions[index]
                                    + "' not granted, exiting", Toast.LENGTH_LONG).show();
                            finish();
                            return;
                        }
                    }
                    // all permissions were granted
                    // initialize();

                    setheremap();
                    break;


                default:
                    //do with default
                    break;
            }
        }

    }

    private void goWithLocationPermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            moveCameraFirstMyLocation(true, null);
        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest
                    .permission.ACCESS_COARSE_LOCATION) && ActivityCompat
                    .shouldShowRequestPermissionRationale(this, android.Manifest
                            .permission.ACCESS_FINE_LOCATION)) {
                openPermissionDialog();
            } else {
                closedPermissionDialog();
            }
        }
    }

    private void openPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogAlert(this, getResources().getString(R.string
                .text_attention), getResources().getString(R.string
                .msg_reason_for_permission_location), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void onClickLeftButton() {
                closedPermissionDialog();
            }

            @Override
            public void onClickRightButton() {
                ActivityCompat.requestPermissions(DeliveryLocationActivity.this, new
                        String[]{android
                        .Manifest
                        .permission
                        .ACCESS_FINE_LOCATION, android.Manifest.permission
                        .ACCESS_COARSE_LOCATION}, Const
                        .PERMISSION_FOR_LOCATION);
                closedPermissionDialog();
            }

        };
        customDialogEnable.show();
    }

    private void closedPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            customDialogEnable.dismiss();
            customDialogEnable = null;

        }
    }


    private void getGeocodeDataFromAddress(String countryName, String countryCode, String locality, String subAdminArea, String adminArea, LatLng storeLatLng, String str_Address, String cit_locality) {

        getDeliveryStoreInCity(countryName, countryCode, locality, subAdminArea,
                adminArea, storeLatLng, str_Address, cit_locality);

    }

    @Override
    public void onBackPressed() {

        setResult(Activity.RESULT_CANCELED);
       /* if (ContextCompat.checkSelfPermission(this, android.Manifest.permission
                .ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && CurrentBooking.getInstance()
                .getDeliveryStoreList().isEmpty()) {
            openExitDialog();

        } else {

            if (searchview.getVisibility() == View.VISIBLE) {
                searchview.setVisibility(View.GONE);
                editsearch.getText().clear();
                listView.setAdapter(null);
                Utils.hideSoftKeyboard(DeliveryLocationActivity.this);

            } else {
                editsearch.getText().clear();
                listView.setAdapter(null);
                goToHomeActivity();
            }
           *//* super.onBackPressed();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);*//*
        }*/
        finish();
    }

    protected void openExitDialog() {
        if (exitDialog != null && exitDialog.isShowing()) {
            return;
        }
        exitDialog = new CustomDialogAlert(this, this
                .getResources()
                .getString(R.string.text_exit), this.getResources()
                .getString(R.string.msg_are_you_sure), this.getResources()
                .getString(R.string.text_cancel), this.getResources()
                .getString(R.string.text_ok)) {
            @Override
            public void onClickLeftButton() {
                dismiss();

            }

            @Override
            public void onClickRightButton() {
                dismiss();
                CurrentBooking.getInstance().setBookCityId("");
                finishAffinity();
            }
        };
        exitDialog.show();
    }

    /**
     * this method expand and collapse  map view from particular size
     */
    private void expandMap() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int[] loc = new int[2];
        int[] loc2 = new int[2];
        btnDone.getLocationOnScreen(loc);
        mapbox.getLocationOnScreen(loc2);
        int viewDistance = loc[1] - loc2[1];
        ResizeAnimation resizeAnimation;
        if (mapFrameLayout.getLayoutParams().height == getResources().getDimensionPixelSize
                (R.dimen.dimen_map_size_large)) {
            resizeAnimation = new ResizeAnimation(mapFrameLayout, viewDistance, getResources()
                    .getDimensionPixelSize(R.dimen.dimen_map_size_large));
            resizeAnimation.setInterpolator(new LinearInterpolator());
            resizeAnimation.setDuration(300);
        } else {
            resizeAnimation = new ResizeAnimation(mapFrameLayout, getResources()
                    .getDimensionPixelSize(R.dimen.dimen_map_size_large), viewDistance);
            resizeAnimation.setInterpolator(new LinearInterpolator());
            resizeAnimation.setDuration(300);
        }
        mapFrameLayout.startAnimation(resizeAnimation);
    }

    private void getDeliveryStoreInCity(String country, String countryCode, String city, String
            subAdminArea, String adminArea, final LatLng cityLatLng, final String address, String
                                                cityCode) {

        final CurrentBooking currentBooking = CurrentBooking.getInstance();
        currentBooking.setCurrentAddress(address);
        storeLatLng = cityLatLng;
        currentBooking.setCurrentLatLng(new com.google.android.gms.maps.model.LatLng(cityLatLng.getLatitude(),cityLatLng.getLongitude()));
        if (!TextUtils.equals(currentBooking.getCurrentCity(), city) || TextUtils.isEmpty
                (currentBooking.getBookCityId())) {
            currentBooking.setCurrentCity(city);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Const.Params.USER_ID, preferenceHelper
                        .getUserId());
                jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                        .getSessionToken());

                /*jsonObject.put(Const.Params.COUNTRY, "United Kingdom");
                jsonObject.put(Const.Params.ADDRESS, "Thornbridge, Washington NE38 8TJ, UK");
                jsonObject.put(Const.Params.COUNTRY_CODE, "GB");
                jsonObject.put(Const.Params.COUNTRY_CODE_2, "GB");
                jsonObject.put(Const.Params.CITY_CODE, "England");
                jsonObject.put(Const.Params.LATITUDE, 54.89711219999999);
                jsonObject.put(Const.Params.LONGITUDE, -1.4964260000000422);*/

                jsonObject.put(Const.Params.COUNTRY, country);
                jsonObject.put(Const.Params.ADDRESS, address);
                jsonObject.put(Const.Params.COUNTRY_CODE, countryCode);
                jsonObject.put(Const.Params.COUNTRY_CODE_2, countryCode);
                jsonObject.put(Const.Params.CITY_CODE, cityCode);
                jsonObject.put(Const.Params.LATITUDE, cityLatLng.getLatitude());
                jsonObject.put(Const.Params.LONGITUDE, cityLatLng.getLongitude());

                int count = 0;
                if (TextUtils.isEmpty(city)) {
                } else {
                    jsonObject.put(Const.Params.CITY1, city);
                    count++;
                }
                if (TextUtils.isEmpty(subAdminArea)) {
                    jsonObject.put(Const.Params.CITY2, "");
                } else {
                    jsonObject.put(Const.Params.CITY2, /*"England"*/subAdminArea);
                    count++;
                }
                if (TextUtils.isEmpty(adminArea)) {
                    jsonObject.put(Const.Params.CITY3, "");
                } else {
                    jsonObject.put(Const.Params.CITY3, /*"Tyne and Wear"*/adminArea);
                    count++;
                }
                /*if (count == 0) {
                    Utils.showToast(getResources().getString(R.string.msg_not_get_proper_address),
                            this);
                }*/

            } catch (JSONException e) {
                AppLog.handleThrowable(Const.Tag.DELIVERY_LOCATION_ACTIVITY, e);
            }

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<DeliveryStoreResponse> responseCall = apiInterface.getDeliveryStoreList
                    (ApiClient.makeJSONRequestBody(jsonObject));
            AppLog.Log("DELIVERY_adress", jsonObject.toString());
            responseCall.enqueue(new Callback<DeliveryStoreResponse>() {
                @Override
                public void onResponse(Call<DeliveryStoreResponse> call,
                                       Response<DeliveryStoreResponse> response) {

                    AppLog.Log("Deliverylocation", ApiClient.JSONResponse(response.body()));
                    Utils.hideCustomProgressDialog();
                    if (response != null && response.body() != null && parseContent != null && parseContent.isSuccessful(response) && parseContent.parseDeliveryStore(response)) {
                        AppLog.Log("Deliverylocation", "AFTER PARSECONTENT ");
                        getDistanceMatrixnew();
                      /*  llDeliveryOrder.setVisibility(View.VISIBLE);
                        tvNoDeliveryFound.setVisibility(View.GONE);
                        btnDone.setVisibility(View.VISIBLE);*/
                    } else {
                        llDeliveryOrder.setVisibility(View.GONE);
                        tvNoDeliveryFound.setVisibility(View.VISIBLE);
                        btnDone.setVisibility(View.GONE);
                    }


                }

                @Override
                public void onFailure(Call<DeliveryStoreResponse> call, Throwable t) {
                    Utils.hideCustomProgressDialog();
                    AppLog.handleThrowable(Const.Tag.DELIVERY_LOCATION_ACTIVITY, t);

                }
            });
        } else {
            Utils.hideCustomProgressDialog();
            getDistanceMatrixnew();
            AppLog.Log(Const.Tag.HOME_FRAGMENT, "CurrentCity and selectedCity are same");
        }


    }

    // call here map direction metrix api
    private void getDistanceMatrixnew() {
        Utils.showCustomProgressDialog(DeliveryLocationActivity.this, false);
        if (currentBooking != null && currentBooking.getCityLocation() != null && currentBooking.getCityLocation().size() > 0 && currentBooking.getCityLocation().get(0) != null && currentBooking.getCityLocation().get(1) != null && String.valueOf(currentBooking.getPaymentLatitude()) != null && String.valueOf(currentBooking.getPaymentLongitude()) != null) {

            origin = Point.fromLngLat(currentBooking.getCityLocation().get(1), currentBooking.getCityLocation().get(0));
            destination = Point.fromLngLat(currentBooking.getPaymentLongitude(), currentBooking.getPaymentLatitude());



            /* Define waypoints for the route */
            /* START: 4350 Still Creek Dr */
            GeoCoordinate startGeoCoordinate = new GeoCoordinate(currentBooking.getCityLocation().get(0), currentBooking.getCityLocation().get(1));
            GeoCoordinate endGeoCoordinate = new GeoCoordinate(currentBooking.getPaymentLatitude(), currentBooking.getPaymentLongitude());
            Log.e("Deliverylocation", "startGeoCoordinate" + startGeoCoordinate.toString());
            Log.e("Deliverylocation", "endGeoCoordinate" + endGeoCoordinate.toString());
            // TO CALCULATE DISTANCE
            double totalKM = startGeoCoordinate.distanceTo(endGeoCoordinate);
            Log.e("Deliverylocation", "totalKM is " + totalKM);


            if(startGeoCoordinate!=null && endGeoCoordinate!=null)
            {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(Const.HERE_API_KEY, getResources().getString(R.string.here_api_key));
                hashMap.put(Const.HERE_WAYPOINT0, getResources().getString(R.string.here_waypoint)+currentBooking.getCityLocation().get(0)+","+ currentBooking.getCityLocation().get(1));
                hashMap.put(Const.HERE_WAYPOINT1, getResources().getString(R.string.here_waypoint)+currentBooking.getPaymentLatitude()+","+ currentBooking.getPaymentLongitude());
                hashMap.put(Const.HERE_MODE,getResources().getString(R.string.here_mode));
                ApiInterface apiInterface = ApiClient.getClientMapBox().create(ApiInterface.class);

                Call<HereDistanceResponce> call=apiInterface.getHereDistanceMatrix(hashMap);
                call.enqueue(new Callback<HereDistanceResponce>() {
                    @Override
                    public void onResponse(Call<HereDistanceResponce> call, Response<HereDistanceResponce> response) {

                        double timeSecond = 0;
                        double distance = 0;
                        Log.i("Call<ResponseBody>","ok");
                        Utils.hideCustomProgressDialog();

                        if (response!=null)
                        {


                                if (response.body().getResponse().getRoute().get(0).getSummary()!=null) {
                                  int destance =response.body().getResponse().getRoute().get(0).getSummary().getDistance();
                                    Log.i("Call<ResponseBody>", response.toString());
                                    Log.i("Call<ResponseBody>", "done");
                                    Log.i("Call<ResponseBody>", String.valueOf(destance));

                                    AppLog.Log("DISTANCE_MATRIX", "Distance=" + distance);
                                    Double radius = Double.valueOf(distance) / 1609.344;
                                    AppLog.Log("aaa", "radius=" + radius);
                                    if (radius != null && currentBooking != null && llDeliveryOrder != null && tvNoDeliveryFound != null && btnDone != null && radius <= currentBooking.getCityRadius()) {
                                        currentBooking.setDeliveryAvailable(true);
                                        llDeliveryOrder.setVisibility(View.VISIBLE);
                                        tvNoDeliveryFound.setVisibility(View.GONE);
                                        btnDone.setVisibility(View.VISIBLE);
                                    } else {
                                        if (currentBooking != null) {
                                            currentBooking.setDeliveryAvailable(false);
                                        }
                                        llDeliveryOrder.setVisibility(View.GONE);
                                        tvNoDeliveryFound.setVisibility(View.VISIBLE);
                                        btnDone.setVisibility(View.GONE);
                                    }


                                }

                        }
                        else
                        {
                            showNoDeliveryView();
                        }


                    }

                    @Override
                    public void onFailure(Call<HereDistanceResponce> call, Throwable t) {
                        AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, t);
                        Utils.hideCustomProgressDialog();
                        Log.d("call", "==== Failure =====" + t.getLocalizedMessage());
                    }
                });






            //AppLog.Log("DISTANCE_MATRIX", "Distance=" + distance);
            Double radius = Double.valueOf(totalKM) / 1609.344;
            AppLog.Log("Deliverylocation", "radius=" + radius);
          /*  if (radius != null && currentBooking != null && llDeliveryOrder != null && tvNoDeliveryFound != null && btnDone != null && radius <= currentBooking.getCityRadius()) {
                currentBooking.setDeliveryAvailable(true);
                llDeliveryOrder.setVisibility(View.VISIBLE);
                tvNoDeliveryFound.setVisibility(View.GONE);
                btnDone.setVisibility(View.VISIBLE);
            } else {
                if (currentBooking != null) {
                    currentBooking.setDeliveryAvailable(false);
                }
                llDeliveryOrder.setVisibility(View.GONE);
                tvNoDeliveryFound.setVisibility(View.VISIBLE);
                btnDone.setVisibility(View.GONE);
            }*/


          /*  if (origin != null && destination != null) {

                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("sources", "1");
                hashMap.put("annotations", "distance,duration");
                hashMap.put("access_token", getResources().getString(R.string
                        .MAP_BOX_ACCESS_TOKEN));
                ApiInterface apiInterface = ApiClient.getClientMapBox().create(ApiInterface.class);

                //swipe latlong
                Call<DirectionMatrixResponse> call = apiInterface.getDistanceMatrixApi(currentBooking.getCityLocation().get(1) + "," + currentBooking.getCityLocation().get(0) + ";" + currentBooking.getPaymentLongitude() + "," + currentBooking.getPaymentLatitude(), hashMap);

                // Call<DirectionMatrixResponse> call = apiInterface.getDistanceMatrixApi(currentBooking.getPaymentLongitude()+","+currentBooking.getPaymentLatitude()+";"+currentBooking.getCityLocation().get(1)+","+currentBooking.getCityLocation().get(0), hashMap);
                call.enqueue(new Callback<DirectionMatrixResponse>() {
                    @Override
                    public void onResponse(Call<DirectionMatrixResponse> call, Response<DirectionMatrixResponse> response) {
                        Utils.hideCustomProgressDialog();
                        double timeSecond = 0;
                        double distance = 0;
                        if (response != null) {

                            if (response.body().getCode().equalsIgnoreCase("Ok")) {

                                if (parseContent != null && response != null && parseContent.isSuccessful(response) && response.body() != null) {

                                    if (response.body().getDistances() != null) {
                                        distance = Double.parseDouble(String.valueOf(response.body().getDistances().get(0).get(0)));
                                        Log.d("call", "==== Response =====" + response.body().getDistances().get(0).get(0));
                                    }

                                    if (response.body().getDurations() != null) {
                                        timeSecond = Double.parseDouble(String.valueOf(response.body().getDurations().get(0).get(0)));
                                        Log.d("call", "==== Response =====" + response.body().getDurations().get(0).get(0));
                                    }

                                    AppLog.Log("DISTANCE_MATRIX", "Distance=" + distance);
                                    Double radius = Double.valueOf(distance) / 1609.344;
                                    AppLog.Log("aaa", "radius=" + radius);
                                    if (radius != null && currentBooking != null && llDeliveryOrder != null && tvNoDeliveryFound != null && btnDone != null && radius <= currentBooking.getCityRadius()) {
                                        currentBooking.setDeliveryAvailable(true);
                                        llDeliveryOrder.setVisibility(View.VISIBLE);
                                        tvNoDeliveryFound.setVisibility(View.GONE);
                                        btnDone.setVisibility(View.VISIBLE);
                                    } else {
                                        if (currentBooking != null) {
                                            currentBooking.setDeliveryAvailable(false);
                                        }
                                        llDeliveryOrder.setVisibility(View.GONE);
                                        tvNoDeliveryFound.setVisibility(View.VISIBLE);
                                        btnDone.setVisibility(View.GONE);
                                    }
                                } else {
                                    showNoDeliveryView();
                                }
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<DirectionMatrixResponse> call, Throwable t) {
                        AppLog.handleThrowable(Const.Tag.CHECKOUT_ACTIVITY, t);
                        Utils.hideCustomProgressDialog();
                        Log.d("call", "==== Failure =====" + t.getLocalizedMessage());
                    }
                });
*/

            } else {
                Utils.hideCustomProgressDialog();
                showNoDeliveryView();
            }

        } else {
            Utils.hideCustomProgressDialog();
            showNoDeliveryView();
        }
    }


    private void showNoDeliveryView() {
        Utils.hideCustomProgressDialog();
        if (currentBooking != null) {
            currentBooking.setDeliveryAvailable(false);
        }
        llDeliveryOrder.setVisibility(View.GONE);
        tvNoDeliveryFound.setVisibility(View.VISIBLE);
        btnDone.setVisibility(View.GONE);

    }

   /* private void updateUiWhenDeliveryAvailable(boolean isUpdate) {
        if (isUpdate) {
            llDeliveryOrder.setVisibility(View.VISIBLE);
            tvNoDeliveryFound.setVisibility(View.GONE);
        } else {
            llDeliveryOrder.setVisibility(View.GONE);
            tvNoDeliveryFound.setVisibility(View.VISIBLE);
        }


    }*/

    private void openDatePickerDialog() {

        final Calendar calendar = Calendar.getInstance();
        Date trialTime = new Date();
        calendar.setTime(trialTime);
        calendar.setTimeZone(TimeZone.getTimeZone(currentBooking.getTimeZone()));
        final int currentYear = calendar.get(Calendar.YEAR);
        final int currentMonth = calendar.get(Calendar.MONTH);
        final int currentDate = calendar.get(Calendar.DAY_OF_MONTH);

        if (datePickerDialog != null && datePickerDialog.isShowing()) {
            return;
        }

        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog
                .OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            }
        };
        datePickerDialog = new DatePickerDialog(this, onDateSetListener, currentYear,
                currentMonth,
                currentDate);
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, this
                .getResources()
                .getString(R.string.text_select), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                calendar.set(datePickerDialog.getDatePicker().getYear(), datePickerDialog
                        .getDatePicker().getMonth(), datePickerDialog.getDatePicker()
                        .getDayOfMonth());
                currentBooking.setFutureOrderDate(parseContent.dateFormat.format(calendar.getTime
                        ()));
                tvScheduleDate.setText(currentBooking.getFutureOrderDate());
                selectedDateTime();
            }
        });
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 10000);
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();

    }


    private void openTimePicker() {
        Calendar calendar = Calendar.getInstance();
        Date trialTime = new Date();
        calendar.setTimeZone(TimeZone.getTimeZone(currentBooking.getTimeZone()));
        calendar.setTime(trialTime);
        final int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        final int currentMinute = calendar.get(Calendar.MINUTE);
        AppLog.Log("hour", currentHour + "");
        AppLog.Log("minute", currentMinute + "");

        if (timePickerDialog != null && timePickerDialog.isShowing()) {
            return;
        }
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog
                .OnTimeSetListener() {

            int count = 0;

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    AppLog.Log("onTimeSet", "onTimeSetCalled");
                    currentBooking.setFutureOrderTime(hourOfDay + ":" + minute);
                    tvScheduleTime.setText(currentBooking.getFutureOrderTime());
                    selectedDateTime();
                } else {
                    if (count == 0) {
                        AppLog.Log("onTimeSet", "onTimeSetCalled");
                        currentBooking.setFutureOrderTime(hourOfDay + ":" + minute);
                        tvScheduleTime.setText(currentBooking.getFutureOrderTime());
                        selectedDateTime();
                    }

                    count++;
                }


            }
        }, currentHour, currentMinute, true);

        timePickerDialog.setCancelable(false);
        timePickerDialog.show();
    }

    private void selectedDateTime() {

        Calendar calendar = Calendar.getInstance();

        if (!TextUtils.isEmpty(currentBooking.getFutureOrderDate())) {
            try {
                Date date = parseContent.dateFormat.parse(currentBooking.getFutureOrderDate());
                calendar.setTime(date);
            } catch (ParseException e) {
                AppLog.handleException(DeliveryLocationActivity.class.getSimpleName(), e);
            }
        }
        if (!TextUtils.isEmpty(currentBooking.getFutureOrderTime())) {
            String[] strings = currentBooking.getFutureOrderTime().split(":");
            calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(strings[0]));
            calendar.set(Calendar.MINUTE, Integer.valueOf(strings[1]));
        }

        TimeZone timeZone = TimeZone.getTimeZone(CurrentBooking.getInstance().getTimeZone());

        currentBooking.setFutureOrderSelectedTimeZoneMillis(calendar.getTimeInMillis() +
                (timeZone.getOffset
                        (calendar.getTimeInMillis())));

        TimeZone timeZoneUTC = TimeZone.getTimeZone("UTC");

        currentBooking.setFutureOrderSelectedTimeUTCMillis(calendar.getTimeInMillis() +
                (timeZoneUTC.getOffset
                        (calendar.getTimeInMillis())));
    }


    private void getFavAddressList() {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<FavouriteAddressResponse> responseCall = apiInterface.getFavouriteAddressList(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<FavouriteAddressResponse>() {
            @Override
            public void onResponse(Call<FavouriteAddressResponse> call, Response<FavouriteAddressResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().getSuccess()) {
                    favAddressList.clear();
                    favAddressList.addAll(response.body().getFavouriteAddresses());
                    if (!favAddressList.isEmpty()) {
                        openFavAddressDialog();
                    } else {
                        goToFavouriteAddressActivity(false,true);
                        //  Utils.showToast(getResources().getString(R.string.msg_no_fav_address),this);
                    }
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), DeliveryLocationActivity.this);
                }

            }

            @Override
            public void onFailure(Call<FavouriteAddressResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);

            }
        });
    }

    private void openFavAddressDialog() {
        if (customFavAddressDialog != null && customFavAddressDialog.isShowing()) {
            return;
        }

        customFavAddressDialog = new CustomFavAddressDialog(this,
                favAddressList, true) {
            @Override
            public void onSelect(int position) {
                Utils.hideSoftKeyboard(DeliveryLocationActivity.this);
                setDeliveryAddress(favAddressList.get(position).getAddress());
                storeLatLng = new LatLng(favAddressList.get(position).getLocation().get(0), favAddressList.get(position).getLocation().get(1));
                //  getGeocodeDataFromAddress(acDeliveryAddress.getText().toString(), favAddressList.get(position).getCity(),favAddressList.get(position).getUserDetails().getCountryPhoneCode(),"","","");
                getGeocodeDataFromAddress("", "", favAddressList.get(position).getCity(), "", "", storeLatLng, favAddressList.get(position).getAddress(), favAddressList.get(position).getCity());
                isplaceAddress = true;
                zoomCamera(storeLatLng, "");
                dismiss();
            }

            @Override
            public void onClose() {
                customFavAddressDialog.dismiss();
            }

            @Override
            public void onAddAddress() {
                dismiss();
                goToFavouriteAddressActivity(false,true);
            }
        };
        customFavAddressDialog.show();
    }

    private void setheremap() {

        mapFragment = getMapFragment();
        Utils.showCustomProgressDialog(DeliveryLocationActivity.this, false);
        // Set up disk cache path for the map service for this application
      /*  boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(
                getApplicationContext().getExternalFilesDir(null) + File.separator + ".here-maps",
                "com.here.android.tutorial.MapService");*/
       /* if (!success) {
            Toast.makeText(getApplicationContext(), "Unable to set isolated disk cache path.", Toast.LENGTH_LONG);
        } else {*/
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(
                    final Error error) {
                if (error == Error.NONE) {
                    // retrieve a reference of the map from the map fragment


                    map = mapFragment.getMap();

                    // Set the zoom level to the average between min and max
                    map.setZoomLevel((map.getMaxZoomLevel() + map.getMinZoomLevel()) / 1);
                    currentLocation = locationHelper.getLastLocation();
                    Utils.hideCustomProgressDialog();

                    moveCameraFirstMyLocationZoom();

                    mapFragment.getMapGesture()
                            .addOnGestureListener(new MapGesture.OnGestureListener() {

                                @Override
                                public void onPanStart() {
                                    Log.e("tagMap", "onPanStart()");
                                    //  getCenterMapLocation();
                                }

                                @Override
                                public void onPanEnd() {
                                    Log.e("tagMap", "onPanEnd()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public void onMultiFingerManipulationStart() {
                                    Log.e("tagMap", "onMultiFingerManipulationStart()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public void onMultiFingerManipulationEnd() {
                                    Log.e("tagMap", "onMultiFingerManipulationEnd()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public boolean onMapObjectsSelected(List<ViewObject> list) {
                                    Log.e("tagMap", "onMapObjectsSelected()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public boolean onTapEvent(PointF pointF) {
                                    Log.e("tagMap", "onTapEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public boolean onDoubleTapEvent(PointF pointF) {
                                    Log.e("tagMap", "onDoubleTapEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public void onPinchLocked() {
                                    Log.e("tagMap", "onPinchLocked()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public boolean onPinchZoomEvent(float v, PointF pointF) {
                                    Log.e("tagMap", "onPinchZoomEvent()");
                                    //  getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public void onRotateLocked() {
                                    Log.e("tagMap", "onRotateLocked()");
                                    getCenterMapLocation();
                                }

                                @Override
                                public boolean onRotateEvent(float v) {
                                    Log.e("tagMap", "onRotateEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public boolean onTiltEvent(float v) {
                                    Log.e("tagMap", "onTiltEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public boolean onLongPressEvent(PointF pointF) {
                                    Log.e("tagMap", "onLongPressEvent()");
                                    // getCenterMapLocation();
                                    return false;
                                }

                                @Override
                                public void onLongPressRelease() {
                                    Log.e("tagMap", "onLongPressRelease()");
                                    // getCenterMapLocation();
                                }

                                @Override
                                public boolean onTwoFingerTapEvent(PointF pointF) {
                                    Log.e("tagMap", "onTwoFingerTapEvent()");
                                    getCenterMapLocation();
                                    return false;
                                }
                            }, 0, false);
                    // getCenterMapLocation();

                } else {
                    Utils.hideCustomProgressDialog();
                    System.out.println("ERROR: Cannot initialize Map Fragment");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(DeliveryLocationActivity.this).setMessage(
                                    "Error : " + error.name() + "\n\n" + error.getDetails())
                                    .setTitle(R.string.engine_init_error)
                                    .setNegativeButton(android.R.string.cancel,
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    finish();
                                                }
                                            }).create().show();
                        }
                    });
                }
            }
        });
        // }


    }

    //for here map
    private void getCenterMapLocation() {
        if (map != null) {

            Utils.showCustomProgressDialog(DeliveryLocationActivity.this,false);

            //  map.setCenter(new GeoCoordinate(currentLatLng.getLatitude(),currentLatLng.getLongitude()),Map.Animation.LINEAR);
            GeoCoordinate coordinate;
            coordinate = map.getCenter();
            Log.i("coordinate",coordinate.toString());
            ReverseGeocodeRequest revGecodeRequest = new ReverseGeocodeRequest(coordinate);

            revGecodeRequest.execute(new ResultListener<com.here.android.mpa.search.Location>() {
                @Override
                public void onCompleted(com.here.android.mpa.search.Location location, ErrorCode errorCode) {
                    if (errorCode == ErrorCode.NONE) {

//                        etSearch.setText(location.getAddress().toString().replace("\n", " "));
                        String myPlace = "";
                        if (location != null && location.getAddress() != null) {
                            if (!TextUtils.isEmpty(location.getAddress().getText())) {
                                myPlace = location.getAddress().getText();
                            }
                        }
                        setDeliveryAddress(myPlace);

                        LatLng latLng = new LatLng(location.getCoordinate().getLatitude(), location.getCoordinate().getLongitude());
                        getGeocodeDataFromAddress(location.getAddress().getCountryName(), location.getAddress().getCountryCode(),
                                location.getAddress().getCity(), "", "", latLng,
                                location.getAddress().toString().replace("\\n", ","), location.getAddress().getCity());

                    } else {

                        Utils.hideCustomProgressDialog();


                        currentBooking.setDeliveryAddress("error code:" + errorCode);
                        setDeliveryAddress("error code:" + errorCode);

                    }
                }
            });
        }
        Utils.hideCustomProgressDialog();
    }

    //for here map

    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS, grantResults);
        }
    }

    //heremap autosugestion
    private void doSearch(String query) {
        // setSearchMode(true);
        /*
         Creates new TextAutoSuggestionRequest with current map position as search center
         and selected collection size with applied filters and chosen locale.
         For more details how to use TextAutoSuggestionRequest
         please see documentation for HERE Mobile SDK for Android.
         */
        TextAutoSuggestionRequest textAutoSuggestionRequest = new TextAutoSuggestionRequest(query);

        if (map != null) {
            textAutoSuggestionRequest.setSearchCenter(map.getCenter());
        }
        textAutoSuggestionRequest.setCollectionSize(10);

        // SET UK BASED COUNTRY WISE
       // textAutoSuggestionRequest.setAddressFilter(new AddressFilter().setCountryCode(Locale.UK.getCountry()));
        //textAutoSuggestionRequest.setAddressFilter(new AddressFilter().setCountryCode("IN"));


       // textAutoSuggestionRequest.setLocale(Locale.UK);

        /*
           The textAutoSuggestionRequest returns its results to non-UI thread.
           So, we have to pass the UI update with returned results to UI thread.
         */

        textAutoSuggestionRequest.execute(new ResultListener<List<AutoSuggest>>() {
            @Override
            public void onCompleted(final List<AutoSuggest> autoSuggests, ErrorCode errorCode) {
                Utils.showCustomProgressDialog(DeliveryLocationActivity.this,false);
                if (errorCode == errorCode.NONE) {

                    processSearchResults(autoSuggests);
                    Log.i("processSearchResults", autoSuggests.toString());
                } else {
                    Utils.hideCustomProgressDialog();
                    Log.i("error with doserch", errorCode.toString());
                }
            }
        });
    }

    private void processSearchResults(final List<AutoSuggest> autoSuggests) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i("processSearchResults", autoSuggests.toString());
                m_autoSuggests.clear();
                m_autoSuggests.addAll(autoSuggests);
          /*      m_autoSuggestAdapter = new AutoSuggestAdapter(DeliveryLocationActivity.this, android.R.layout.simple_list_item_1, m_autoSuggests);
               *//* m_autoSuggestAdapter.notifyDataSetChanged();
                listView.setAdapter(m_autoSuggestAdapter);*/
                Utils.hideCustomProgressDialog();


            }
        });
    }


}
