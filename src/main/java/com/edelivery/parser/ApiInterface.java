package com.edelivery.parser;


import com.edelivery.models.PostcoderModel;
import com.edelivery.models.ReferralResponse;
import com.edelivery.models.WalletReferalHistoryModel.WalletReferalHistoryResponse;
import com.edelivery.models.datamodels.OtpConfirmation;
import com.edelivery.models.datamodels.Resendpin;
import com.edelivery.models.datamodels.UpdateCardResponce;
import com.edelivery.models.getAddressModel;
import com.edelivery.models.hereresponce.Autosuggetionhere;
import com.edelivery.models.hereresponce.HereDistanceResponce;
import com.edelivery.models.mapboxdirectionmatrix.DirectionMatrixResponse;
import com.edelivery.models.responsemodels.ActiveOrderResponse;
import com.edelivery.models.responsemodels.AddCartResponse;
import com.edelivery.models.responsemodels.AllDocumentsResponse;
import com.edelivery.models.responsemodels.AvailableProviderResponse;
import com.edelivery.models.responsemodels.CardsResponse;
import com.edelivery.models.responsemodels.CartResponse;
import com.edelivery.models.responsemodels.CityResponse;
import com.edelivery.models.responsemodels.CountriesResponse;
import com.edelivery.models.responsemodels.CreateOrederResponse;
import com.edelivery.models.responsemodels.DeliveryStoreResponse;
import com.edelivery.models.responsemodels.DocumentResponse;
import com.edelivery.models.responsemodels.EmailCheck;
import com.edelivery.models.responsemodels.FavouriteAddressResponse;
import com.edelivery.models.responsemodels.FavouriteStoreResponse;
import com.edelivery.models.responsemodels.InvoiceResponse;
import com.edelivery.models.responsemodels.IsSuccessResponse;
import com.edelivery.models.responsemodels.OrderHistoryDetailResponse;
import com.edelivery.models.responsemodels.OrderHistoryResponse;
import com.edelivery.models.responsemodels.OrderResponse;
import com.edelivery.models.responsemodels.OrdersResponse;
import com.edelivery.models.responsemodels.OtpResponse;
import com.edelivery.models.responsemodels.PaymentGatewayResponse;
import com.edelivery.models.responsemodels.PaymentIntent;
import com.edelivery.models.responsemodels.PaymentIntentResponce;
import com.edelivery.models.responsemodels.ProviderLocationResponse;
import com.edelivery.models.responsemodels.ReassignDeliveryManResponse;
import com.edelivery.models.responsemodels.ReviewResponse;
import com.edelivery.models.responsemodels.SetFavouriteResponse;
import com.edelivery.models.responsemodels.AppSettingDetailResponse;
import com.edelivery.models.responsemodels.StoreProductResponse;
import com.edelivery.models.responsemodels.StoreResponse;
import com.edelivery.models.responsemodels.UserDataResponse;
import com.edelivery.models.responsemodels.WalletHistoryResponse;
import com.edelivery.models.responsemodels.WalletResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface ApiInterface {

    @Multipart
    @POST("v2/api/user/register")
    Call<UserDataResponse> register(@Part MultipartBody.Part file, @PartMap() Map<String,
            RequestBody> partMap);

    @Headers("Content-Type:application/json;charset=UTF-8")
        @POST("v2/api/user/login")
    Call<UserDataResponse> login(@Body RequestBody requestBody);


    @GET("api/admin/get_country_list")
    Call<CountriesResponse> getCountries();

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/get_city_list")
    Call<CityResponse> getCities(@Body RequestBody requestBody);

    @Multipart
    @POST("api/user/update")
    Call<UserDataResponse> updateProfile(@Part MultipartBody.Part file, @PartMap() Map<String,
            RequestBody> partMap);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/check_app_keys")
    Call<AppSettingDetailResponse> getAppSettingDetail(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/forgot_password")
    Call<IsSuccessResponse> forgotPassword(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/update_device_token")
    Call<IsSuccessResponse> updateDeviceToken(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/logout")
    Call<IsSuccessResponse> logOut(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_store_list")
    Call<StoreResponse> getSelectedStoreList(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/user_get_store_product_item_list")
    Call<StoreProductResponse> getStoreProductList(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_delivery_list_for_nearest_city")
    Call<DeliveryStoreResponse> getDeliveryStoreList(@Body RequestBody requestBody);

    @GET("api/geocode/json")
    Call<ResponseBody> getGoogleGeocode(@QueryMap Map<String, String> stringMap);

    /*old
    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_order_cart_invoice")
    Call<InvoiceResponse> getDeliveryInvoice(@Body RequestBody requestBody);
*/

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("v1/api/user/get_order_cart_invoice")
    Call<InvoiceResponse> getDeliveryInvoice(@Body RequestBody requestBody);

    @GET("api/distancematrix/json?")
    Call<ResponseBody> getGoogleDistanceMatrix(@QueryMap Map<String, String> stringMap);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/otp_verification")
    Call<OtpResponse> getOtpVerify(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/create_order")
    Call<CreateOrederResponse> createOrder(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_payment_gateway")
    Call<PaymentGatewayResponse> getPaymentGateway(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/add_wallet_amount")
    Call<WalletResponse> getAddWalletAmount(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_card_list")
    Call<CardsResponse> getAllCreditCards(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/add_card")
    Call<CardsResponse> getAddCreditCard(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/select_card")
    Call<CardsResponse> selectCreditCard(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/delete_card")
    Call<IsSuccessResponse> deleteCreditCard(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/change_user_wallet_status")
    Call<IsSuccessResponse> toggleWalletUse(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/New_Payment_process_pay_order_payment")
    Call<IsSuccessResponse> payOrderPayment(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/wallet_payments")
    Call<IsSuccessResponse> payOrderUsingZeroPayment(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/add_item_in_cart")
    Call<AddCartResponse> addItemInCart(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_cart")
    Call<CartResponse> getCart(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/clear_cart")
    Call<IsSuccessResponse> clearCart(@Body RequestBody requestBody);

    //old
    /*@Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_orders")
    Call<OrdersResponse> getOrders(@Body RequestBody requestBody);*/

    //new
    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("v1/api/user/get_orders")
    Call<OrdersResponse> getOrders(@Body RequestBody requestBody);

/*   old
    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_order_status")
    Call<ActiveOrderResponse> getActiveOrderStatus(@Body RequestBody requestBody);*/

    //new
    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("v1/api/user/get_order_status")
    Call<ActiveOrderResponse> getActiveOrderStatus(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/user_cancel_order")
    Call<IsSuccessResponse> cancelOrder(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/get_document_list")
    Call<AllDocumentsResponse> getAllDocument(@Body RequestBody requestBody);


    @Multipart
    @POST("api/admin/upload_document")
    Call<DocumentResponse> uploadDocument(@Part MultipartBody.Part file, @PartMap() Map<String,
            RequestBody> partMap);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/order_history_detail")
    Call<OrderHistoryDetailResponse> getOrderHistoryDetail(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/order_history")
    Call<OrderHistoryResponse> getOrdersHistory(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/check_referral")
    Call<IsSuccessResponse> getCheckReferral(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_provider_location")
    Call<ProviderLocationResponse> getProviderLocation(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_detail")
    Call<UserDataResponse> getUserDetail(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/otp_verification")
    Call<IsSuccessResponse> setOtpVerification(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/apply_promo_code")
    Call<InvoiceResponse> applyPromoCode(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/remove_promo_code")
    Call<InvoiceResponse> removePromoCode(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("/api/user/apply_wallet_code")
    Call<InvoiceResponse> applyReferralCode(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("/api/user/remove_wallet_code")
    Call<InvoiceResponse> removeReferralCode(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/show_invoice")
    Call<IsSuccessResponse> setShowInvoice(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_invoice")
    Call<InvoiceResponse> getInvoice(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/rating_to_store")
    Call<IsSuccessResponse> setFeedbackStore(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/rating_to_provider")
    Call<IsSuccessResponse> setFeedbackProvider(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/change_delivery_address")
    Call<IsSuccessResponse> changeDeliveryAddress(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/get_wallet_history")
    Call<WalletHistoryResponse> getWalletHistory(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/admin/get_referal_histroy")
    Call<WalletReferalHistoryResponse>getReferralHistory(@Body RequestBody requestBody);



    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/add_favourite_store")
    Call<SetFavouriteResponse> setFavouriteStore(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/remove_favourite_store")
    Call<SetFavouriteResponse> removeAsFavouriteStore(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_favourite_store_list")
    Call<FavouriteStoreResponse> getFavouriteStores(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/user_get_store_review_list")
    Call<ReviewResponse> getStoreReview(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/user_like_dislike_store_review")
    Call<IsSuccessResponse> setUserReviewLikeAndDislike(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_order_detail")
    Call<OrderResponse> getOrderDetail(@Body RequestBody requestBody);

    /*@Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/check_provider_available")
    Call<AvailableProviderResponse>checkProviderAvailable(@Body RequestBody requestBody);*/

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/provider_near_store")
    Call<AvailableProviderResponse>checkStoreProviderAvailable(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/store/create_request")
    Call<ReassignDeliveryManResponse>ReassignDeliveryMan(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("v2/api/user/add_favourite_address")
    Call<IsSuccessResponse>AddFavouriteAddress(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("v2/api/user/update_favourite_address")
    Call<IsSuccessResponse>EditFavouriteAddress(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("/api/user/get_favourite_address_list")
    Call<FavouriteAddressResponse>getFavouriteAddressList(@Body RequestBody requestBody);

   /* @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("/api/user/update_favourite_address")
    Call<IsSuccessResponse>UpdateFavouriteAddress(@Body RequestBody requestBody);*/

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("/api/user/delete_favourite_address")
    Call<IsSuccessResponse>DeleteFavouriteAddress(@Body RequestBody requestBody);


    @GET("/directions-matrix/v1/mapbox/driving/{latlng}")
    Call<DirectionMatrixResponse>getDistanceMatrixApi(@Path("latlng") String latlng,  @QueryMap Map<String, String> stringMap);

    @GET("routing/7.2/calculateroute.json?")
    Call<HereDistanceResponce> getHereDistanceMatrix(@QueryMap Map<String, String> stringMap);

        @GET("v1/autosuggest")
    Call<Autosuggetionhere> getautosuggetions(@Query("at") String latlng, @Query("limit")int size, @Query("q")String query,
            /*@Query("resultTypes") String resultTypes,*/@Query("apiKey") String apikey);

    @GET("pcw/PCWH3-YXCLS-3MX8X-MPPCL/address/GB/{search}")
    Call<List<PostcoderModel>>getautosuggetionsPushCode(@Path("search") String search, @QueryMap Map<String, String> stringMap);


    @GET("find/{search}")
    Call<getAddressModel>getAddressAPIForLocation(@Path("search") String search, @QueryMap Map<String, String> stringMap);

    //api-key


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("v1/api/user/confirm_pin")
    Call<UserDataResponse> otpconfirmation(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("v1/api/user/resend_pin")
    Call<UserDataResponse> resendpin(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("v1/api/user/change_number")
    Call<UserDataResponse> changenumbur(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("api/user/get_payment_intent")
    Call<PaymentIntentResponce> getpaymentintent(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("/v1/api/user/check_user_exist")
    Call<EmailCheck> checkemailonserver(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("/api/user/update_Card")
    Call<UpdateCardResponce> Updateoldcard(@Body RequestBody requestBody);



}