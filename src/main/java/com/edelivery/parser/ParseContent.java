package com.edelivery.parser;

import android.content.Context;
import android.location.Address;
import android.location.Location;
import android.text.TextUtils;


import com.edelivery.R;
import com.edelivery.models.datamodels.CartProductItems;
import com.edelivery.models.datamodels.CartProducts;
import com.edelivery.models.datamodels.Cities;
import com.edelivery.models.datamodels.City;
import com.edelivery.models.datamodels.Countries;
import com.edelivery.models.datamodels.Deliveries;
import com.edelivery.models.datamodels.Addresses;
import com.edelivery.models.datamodels.Invoice;
import com.edelivery.models.datamodels.OrderPayment;
import com.edelivery.models.datamodels.SpecificationSubItem;
import com.edelivery.models.datamodels.Specifications;
import com.edelivery.models.datamodels.User;
import com.edelivery.models.responsemodels.AppSettingDetailResponse;
import com.edelivery.models.responsemodels.CartResponse;
import com.edelivery.models.responsemodels.CityResponse;
import com.edelivery.models.responsemodels.CountriesResponse;
import com.edelivery.models.responsemodels.DeliveryStoreResponse;
import com.edelivery.models.responsemodels.UserDataResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.PreferenceHelper;
import com.edelivery.utils.Utils;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Response;

/*import static com.edelivery.BuildConfig.BASE_URL;*/

/**
 * Created by elluminati on 02-Feb-2017.
 */
public class ParseContent {
    private static final String TAG = "ParseContent";
    private static ParseContent parseContent = new ParseContent();
    public SimpleDateFormat webFormat;
    public SimpleDateFormat timeFormat, timeFormat2;
    public SimpleDateFormat dateFormat, dateFormat2, dateFormat3, dateTimeFormat_am;
    public SimpleDateFormat dateFormatMonth, dateFormatMonthFull;
    public SimpleDateFormat day, dateTimeFormat, timeFormat_am;
    public DecimalFormat decimalTwoDigitFormat;
    private PreferenceHelper preferenceHelper;
    private Context context;

    public com.mapbox.mapboxsdk.geometry.LatLng latLng;
    public String address = "";

    private ParseContent() {

        webFormat = new SimpleDateFormat(Const.DATE_TIME_FORMAT_WEB, Locale.US);
        webFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateFormat = new SimpleDateFormat(Const.DATE_FORMAT, Locale.US);
        timeFormat = new SimpleDateFormat(Const.TIME_FORMAT, Locale.US);
        dateFormatMonth = new SimpleDateFormat(Const.DATE_FORMAT_MONTH, Locale.US);
        dateFormatMonthFull = new SimpleDateFormat(Const.DATE_FORMAT_MONTH_FULL, Locale.US);

        dateTimeFormat = new SimpleDateFormat(Const.DATE_TIME_FORMAT, Locale.US);
        day = new SimpleDateFormat(Const.DAY, Locale.US);
        timeFormat_am = new SimpleDateFormat(Const.TIME_FORMAT_AM, Locale.US);
        dateFormat2 = new SimpleDateFormat(Const.DATE_FORMAT_2, Locale.US);
        dateFormat3 = new SimpleDateFormat(Const.DATE_FORMAT_3, Locale.US);
        timeFormat2 = new SimpleDateFormat(Const.TIME_FORMAT_2, Locale.US);
        decimalTwoDigitFormat = new DecimalFormat("0.00");
        dateTimeFormat_am = new SimpleDateFormat(Const.DATE_TIME_FORMAT_AM, Locale.US);
        String latitude = "";
        String Longitude = "";
    }

    public static ParseContent getInstance() {
        return parseContent;
    }

    public void setContext(Context context) {
        preferenceHelper = PreferenceHelper.getInstance(context);
        this.context = context;
    }

    public boolean parseUserStorageData(Response<UserDataResponse> response) {
        AppLog.Log("USER_DETAIL", ApiClient.JSONResponse(response.body()));

        Utils.hideCustomProgressDialog();
        try {
            if (response != null && response.body() != null && isSuccessful(response) && response.body().getUser() != null && response.body().isSuccess() && preferenceHelper != null) {
                User user = response.body().getUser();
                preferenceHelper.putUserId(user.getId());
                preferenceHelper.putSessionToken(user.getServerToken());
                preferenceHelper.putFirstName(user.getFirstName());
                preferenceHelper.putLastName(user.getLastName());
                preferenceHelper.putAddress(user.getAddress());
                preferenceHelper.putZipCode(user.getZipcode());
                preferenceHelper.putPhoneNumber(user.getPhone());
                preferenceHelper.putPhoneCountyCodeCode(user.getCountryPhoneCode());
                preferenceHelper.putEmail(user.getEmail());
                preferenceHelper.putProfilePic(PreferenceHelper.getInstance(context).getIMAGE_BASE_URL() + user.getImageUrl());
                preferenceHelper.putReferral(user.getReferralCode());
                preferenceHelper.putIsPhoneNumberVerified(user.isIsPhoneNumberVerified());
                preferenceHelper.putIsEmailVerified(user.isIsEmailVerified());
                preferenceHelper.putIsUserAllDocumentsUpload(user.isIsDocumentUploaded());
                preferenceHelper.putIsApproved(user.isIsApproved());
                preferenceHelper.putIsPhoneNumberVerified(user.isIsPhoneNumberVerified());
                preferenceHelper.putIsEmailVerified(user.isIsEmailVerified());
                preferenceHelper.putCountryId(user.getCountryId());
                preferenceHelper.putWalletCurrencyCode(user.getWalletCurrencyCode());
                preferenceHelper.putFRIEND_REFERAL_WALLET(response.body().referralBonusToUserFriend);
                preferenceHelper.putWallet(user.getWallet());
                preferenceHelper.putREGISTER_REFERAL_WALLET(response.body().referralBonusToUser);

                if (user.getSocialId() != null && !user.getSocialId().isEmpty()) {
                    preferenceHelper.putSocialId(user.getSocialId().get(0));
                } else {
                    preferenceHelper.putSocialId("");
                }
                if (user.getOrders() != null) {
                    CurrentBooking.getInstance().setHaveOrders(user.getOrders().isEmpty());
                } else {
                    CurrentBooking.getInstance().setHaveOrders(false);
                }

                CurrentBooking.getInstance().getFavourite().clear();
                CurrentBooking.getInstance().setFavourite(user.getFavouriteStores());
                CurrentBooking.getInstance().getFavAddressList().clear();
                CurrentBooking.getInstance().setFavAddressList(user.getFavAddressList());
                preferenceHelper.putMaxPhoneNumberLength(response.body().getMaxPhoneNumberLength());
                preferenceHelper.putMinPhoneNumberLength(response.body().getMinPhoneNumberLength());


                return true;
            } else {
                if (response != null && response.body() != null) {
                    Utils.showErrorToast(response.body().getErrorCode(), context);
                }
            }
        } catch (Exception e) {

           /* Log.e("parseUserStorageData() " + e.getMessage());*/
            return false;
        }

        return false;
    }

    public ArrayList<Countries> parseCountries(Response<CountriesResponse>
                                                       response) {
        try {
            if (response != null && response.body() != null && isSuccessful(response)) {
                Utils.hideCustomProgressDialog();
                if (response.body().isSuccess()) {
                    ArrayList<Countries> countries = (ArrayList<Countries>) response.body
                            ().getCountries();
                    if (countries == null) {
                        return new ArrayList<>();
                    } else {
                        return countries;
                    }

                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), context);
                    return new ArrayList<>();
                }
            }
        } catch (Exception e) {
            /*Log.e("parseCountries() " + e.getMessage());*/
        }
        return null;
    }

    public ArrayList<Cities> parseCities(Response<CityResponse> response) {
        ArrayList<Cities> cities = new ArrayList<>();
        try {
            if (response != null && response.body() != null && isSuccessful(response)) {
                Utils.hideCustomProgressDialog();
                if (response.body().isSuccess()) {
                    if ((ArrayList<Cities>) response.body().getCities() != null) {
                        cities.addAll((ArrayList<Cities>) response.body().getCities());
                    }
                    if (cities == null) {
                        return new ArrayList<>();
                    } else {
                        return cities;
                    }
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), context);
                    return new ArrayList<>();
                }
            }
        } catch (Exception e) {
            /*Log.e("parseCities() " + e.getMessage());*/
        }
        return cities;
    }

    public ArrayList<Invoice> parseInvoice(OrderPayment orderPayment, String currency, boolean
            isShowPromo) {
        ArrayList<Invoice> invoices = new ArrayList<>();
        try {
            CurrentBooking.getInstance().setOrderPaymentId(orderPayment.getId());
            String unit = orderPayment.isDistanceUnitMile() ? context.getResources().getString(R.string
                    .unit_mile) : context.getResources().getString(R.string.unit_km);

            if (orderPayment != null && orderPayment.getTotalCartPrice() >
                    0) {
                invoices.add(loadInvoiceData(context.getResources().getString(R.string.text_item_price),
                        orderPayment.getTotalCartPrice(),
                        currency, 0.0, orderPayment.getTotalItem() + "" +
                                " " +
                                "" + context.getResources()
                                .getString(R.string.text_items), 0.0, ""));
            }

            if (orderPayment != null && orderPayment.getTotalStoreTaxPrice() > 0) {
                invoices.add(loadInvoiceData(context.getResources().getString(R.string.text_tax),
                        orderPayment.getTotalStoreTaxPrice(), currency, 0.0, "", 0.0,
                        ""));
            }

        /*if (orderPayment.getTotalOrderPrice() > 0) {
            invoices.add(loadInvoiceData(context.getResources().getString(R.string
                            .text_total_item_cost),
                    orderPayment.getTotalOrderPrice(), currency, 0.0, "", 0.0,
                    ""));
        }*/

            if (orderPayment.getTotalBasePrice() > 0) {
                invoices.add(loadInvoiceData(context.getResources().getString(R.string.text_base_price),
                        orderPayment.getTotalBasePrice(), currency, orderPayment.getBasePrice(),
                        currency, orderPayment
                                .getBasePriceDistance(), unit));
            }

            if (orderPayment.getDistancePrice() > 0) {
                invoices.add(loadInvoiceData(context.getResources().getString(R.string
                                .text_distance_price),
                        orderPayment.getDistancePrice(), currency, orderPayment
                                .getPricePerUnitDistance(), currency, 0.0,
                        unit));
            }

            if (orderPayment.getTotalTimePrice() > 0) {
                invoices.add(loadInvoiceData(context.getResources().getString(R.string.text_time_price),
                        orderPayment.getTotalTimePrice(), currency, orderPayment
                                .getPricePerUnitTime(), currency, 0.0,
                        context.getResources().getString(R.string.unit_mins)));
            }
            if (orderPayment.getTotalServicePrice() > 0) {
                invoices.add(loadInvoiceData(context.getResources().getString(R.string
                                .text_delivery_fee),
                        orderPayment.getTotalServicePrice(), currency, 0.0, "", 0.0,
                        ""));
            }

            if (orderPayment.getTotalAdminTaxPrice() > 0) {
                invoices.add(loadInvoiceData(context.getResources().getString(R.string.text_service_tax),
                        orderPayment.getTotalAdminTaxPrice(), currency, 0.0, orderPayment
                                .getServiceTax()
                                + "%", 0.0,
                        ""));
            }
            if (orderPayment.getTotalSurgePrice() > 0) {
                invoices.add(loadInvoiceData(context.getResources().getString(R.string
                                .text_surge_price),
                        orderPayment.getTotalSurgePrice(), currency, orderPayment.getSurgeCharges(),
                        "x", 0.0,
                        ""));
            }
            if (orderPayment.isPromoForDeliveryService() && orderPayment.getPromoPayment() > 0 &&
                    isShowPromo) {
                invoices.add(loadInvoiceData(context.getResources().getString(R.string
                                .text_promo),
                        orderPayment.getPromoPayment(), currency, 0.0, "", 0.0,
                        ""));
            }


        /*if (orderPayment.getTotalDeliveryPrice() > 0) {
            invoices.add(loadInvoiceData(context.getResources().getString(R.string
                            .text_total_service_cost),
                    orderPayment.getTotalDeliveryPrice(), currency, 0.0, "", 0.0,
                    ""));
        }*/
            if (orderPayment.getStoreServiceCharge() > 0) {
                invoices.add(loadInvoiceData(context.getResources().getString(R.string
                                .text_handling_charge),
                        orderPayment.getStoreServiceCharge(), currency, 0.0, "", 0.0,
                        ""));
            }

            if (!orderPayment.isPromoForDeliveryService() && orderPayment.getPromoPayment() > 0 &&
                    isShowPromo) {
                invoices.add(loadInvoiceData(context.getResources().getString(R.string
                                .text_promo_discount),
                        orderPayment.getPromoPayment(), "-" + currency, 0.0, "", 0.0,
                        ""));
            }

        } catch (Exception e) {
           /* Log.e("parseInvoice() " + e.getMessage());*/
        }
        return invoices;
    }

    /***
     *
     * @param title main title for show in invoice
     * @param mainPrice main price for show in invoice
     * @param currency currency append with main price
     * @param subPrice subPrice is price which is apply particular distance or time or other
     *                 things
     * @param subText subTex is append ahead with subPrice
     * @param unitValue unitValue is value to decide distance or time
     * @param unit unit km/mile
     * @return
     */
    public Invoice loadInvoiceData(String title, double mainPrice, String currency,
                                   double subPrice, String subText, double unitValue, String
                                           unit) {
        Invoice invoice = new Invoice();
        try {
            if (title.equals(context.getString(R.string.text_referral_discount))||title.equals(context.getString(R.string.text_promo_discount))) {
                invoice.setPrice("- " + currency + decimalTwoDigitFormat.format(mainPrice));
            } else {
                invoice.setPrice(currency + decimalTwoDigitFormat.format(mainPrice));
            }

            invoice.setSubTitle(appendString(subText, subPrice, unitValue, unit));
            invoice.setTitle(title);
        } catch (Exception e) {
            /*Log.e("loadInvoiceData() " + e.getMessage());*/
        }
        return invoice;
    }


    private String appendString(String text, Double price, Double value, String unit) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder.append(text);
            if (price > 0) {
                stringBuilder.append(decimalTwoDigitFormat.format(price));
            }
            if (!TextUtils.isEmpty(unit)) {
                stringBuilder.append("/");
                if (value > 1.0) {
                    stringBuilder.append(decimalTwoDigitFormat.format(value));
                }
                stringBuilder.append(unit);
            }
        } catch (Exception e) {
           /* Log.e("appendString() " + e.getMessage());*/
        }
        return stringBuilder.toString();
    }

    public boolean parseDeliveryStore(Response<DeliveryStoreResponse> response) {
        try {
            if (isSuccessful(response)) {
                if (response.body() != null && response.body().isSuccess() && response.body().getCity() != null) {
                    CurrentBooking currentBooking = CurrentBooking.getInstance();
                    if (currentBooking.getDeliveryStoreList() != null) {
                        currentBooking.getDeliveryStoreList().clear();
                    }
                    City deliveryCity = response.body().getCity();
                    if (deliveryCity != null) {
                        currentBooking.setBookCityId(deliveryCity.getId());
                        preferenceHelper.putBookCityId(deliveryCity.getId());
                        currentBooking.setBookCountryId(deliveryCity.getCountryId());
                        currentBooking.setCashPaymentMode(deliveryCity.isIsCashPaymentMode());
                        currentBooking.setOtherPaymentMode(deliveryCity.isIsOtherPaymentMode());
                        currentBooking.setPromoApplyForCash(deliveryCity.isIsPromoApplyForCash());
                        currentBooking.setPromoApplyForOther(deliveryCity.isIsPromoApplyForOther());
                        currentBooking.setCurrentCity(deliveryCity.getCityName());
                        currentBooking.setCityLocation(deliveryCity.getCityLatLong());
                        currentBooking.setCityRadius(deliveryCity.getCityRadius());
                        currentBooking.setDeliveryStoreList((ArrayList<Deliveries>)
                                response.body().getDeliveries());
                        currentBooking.setCurrency(response.body().getCurrencySign());
                        currentBooking.setCurrencyCode(response.body().getCurrencycode());


                    }

                    if (response.body().getCityData() != null) {
                        if (response.body().getCityData().getCity1() != null) {
                            currentBooking.setCity1(response.body().getCityData().getCity1());
                        } else {
                            currentBooking.setCity1("");
                        }

                        if (response.body().getCityData().getCity2() != null) {
                            currentBooking.setCity2(response.body().getCityData().getCity2());
                        } else {
                            currentBooking.setCity2("");
                        }


                        if (response.body().getCityData().getCity2() != null) {
                            currentBooking.setCity3(response.body().getCityData().getCity3());
                        } else {
                            currentBooking.setCity3("");
                        }

                    }
                    currentBooking.setCityCode(response.body().getCityData().getCityCode());
                    currentBooking.setCountry(response.body().getCityData().getCountry());
                    currentBooking.setCountryCode(response.body().getCityData().getCountryCode());
                    currentBooking.setCountryCode2(response.body().getCityData().getCountryCode2());
                    currentBooking.setPaymentLongitude(response.body().getCityData().getLongitude());
                    currentBooking.setPaymentLatitude(response.body().getCityData().getLatitude());
                    currentBooking.setTimeZone(response.body().getCity().getTimezone());
                    currentBooking.getAds().clear();
                    if (response.body().getAds() != null) {
                        currentBooking.setAds(response.body().getAds());
                    }
                    Utils.hideCustomProgressDialog();
                    return true;
                } else {
                    CurrentBooking.getInstance().setBookCityId("");
                    CurrentBooking.getInstance().getDeliveryStoreList().clear();
                    CurrentBooking.getInstance().getAds().clear();
                    //   Utils.showErrorToast(response.body().getErrorCode(), context);
                    Utils.hideCustomProgressDialog();
                }
            }

        } catch (Exception e) {
            /*Log.e("parseDeliveryStore() " + e.getMessage());*/
        }
        Utils.hideCustomProgressDialog();
        return false;
    }

    /**
     * This method sync a serverCart to localCart so please read whole response of server cart
     * and take care about all models of cart which is help to make sync serverCart to localCart.
     *
     * @param response
     * @see "check web service which have uniqueId for all produt,produtItem,
     * productSpecificationItem and productSpecificationItemItems"
     */
    public void parseCart(Response<CartResponse> response) {
        try {
            if (response != null && response.body() != null && isSuccessful(response) && response.body().isSuccess()) {
                Gson gson = new Gson();
                AppLog.Log("CART_RESPONSE", gson.toJson(response.body()));
                CurrentBooking currentBooking = CurrentBooking.getInstance();
                currentBooking.clearCart();
                currentBooking.setCartCityId(response.body().getCityId());
                currentBooking.setCartId(response.body().getCartId());
                currentBooking.setCartCurrency(response.body().getCurrency());
                currentBooking.setPickupAddresses(response.body().getPickupAddresses().get(0));
                Addresses addresses = response.body().getDestinationAddresses
                        ().get(0);
                currentBooking.setDeliveryAddress(addresses.getAddress());
                currentBooking.setDeliveryLatLng(new LatLng(addresses.getLocation()
                        .get(0),
                        addresses.getLocation().get(1)));
                currentBooking.setDestinationAddresses(response.body().getDestinationAddresses()
                        .get(0));
                List<CartProducts> cartProductsList = response.body().getCart().getProducts();
                for (CartProducts cartProducts : cartProductsList) {

                    double itemPriceAndSpecificationPriceTotal = 0;
                    CartProducts products = new CartProducts();
                    products.setProductName(cartProducts.getCartProductItemDetail().getName());
                    products.setUniqueId(cartProducts.getCartProductItemDetail().getUniqueId());
                    products.setProductId(cartProducts.getCartProductItemDetail().getId());
                    CartProductItems cartProductItemsNew = null;
                    ArrayList<CartProductItems> cartProductItemsListNew = new ArrayList<>();
                    for (CartProductItems cartProductItems : cartProducts.getItems()) {
                        cartProductItems.getProductItem().setCurrency(response.body().getCurrency
                                ());
                        currentBooking.setCartProductItemWithAllSpecificationList
                                (cartProductItems.getProductItem());
                        ArrayList<Specifications> specificationListNew = new ArrayList<>();
                        cartProductItemsNew = new CartProductItems();
                        cartProductItemsNew.setImageUrl(cartProductItems.getProductItem()
                                .getImageUrl());
                        cartProductItemsNew.setItemId(cartProductItems.getProductItem().getId
                                ());
                        cartProductItemsNew.setItemName(cartProductItems.getProductItem()
                                .getName());
                        cartProductItemsNew.setItemPrice(cartProductItems.getProductItem()
                                .getPrice());
                        cartProductItemsNew.setItemNote(cartProductItems.getItemNote());
                        cartProductItemsNew.setDetails(cartProductItems
                                .getProductItem().getDetails());
                        cartProductItemsNew.setQuantity(cartProductItems.getQuantity());
                        cartProductItemsNew.setUniqueId(cartProductItems.getUniqueId());
                        itemPriceAndSpecificationPriceTotal = cartProductItems.getProductItem()
                                .getPrice();
                        List<Specifications> cartSpecifications = cartProductItems
                                .getSpecifications();
                        List<Specifications> specifications = cartProductItems
                                .getProductItem()
                                .getSpecifications();

                        int specificationSize = cartSpecifications.size();
                        double specificationPriceTotal = 0;
                        double specificationPrice = 0;
                        ArrayList<SpecificationSubItem> specificationItemCartListNew = null;

                        for (int i = 0; i < specificationSize; i++) {
                            Specifications specificationsNew = null;
                            int size = specifications.size();
                            for (int a = 0; a < size; a++) {
                                if (cartSpecifications.get(i).getUniqueId() == specifications
                                        .get(a).getUniqueId()) {
                                    specificationItemCartListNew = new
                                            ArrayList<>();
                                    List<SpecificationSubItem> cartSpecificationListItemSub
                                            = cartSpecifications.get(i)
                                            .getList();
                                    List<SpecificationSubItem> specificationListItemSub =
                                            specifications.get(a).getList();
                                    int cartSpecificationItemListSize = cartSpecificationListItemSub
                                            .size();
                                    int specificationListItemListSize = specifications.get(a)
                                            .getList().size();
                                    for (int j = 0; j < cartSpecificationItemListSize; j++) {
                                        for (int k = 0; k < specificationListItemListSize; k++) {
                                            if (cartSpecificationListItemSub.get(j).getUniqueId()
                                                    == specificationListItemSub.get(k)
                                                    .getUniqueId()) {
                                                specificationPrice = specificationPrice +
                                                        specificationListItemSub.get(k).getPrice();
                                                specificationPriceTotal = specificationPriceTotal +
                                                        specificationListItemSub.get(k).getPrice();
                                                specificationItemCartListNew.add
                                                        (specificationListItemSub
                                                                .get(k));
                                                break;
                                            }
                                        }

                                    }
                                    if (!specificationItemCartListNew
                                            .isEmpty()) {
                                        specificationsNew = new Specifications();
                                        specificationsNew.setList(specificationItemCartListNew);
                                        specificationsNew.setName(specifications
                                                .get(a).getName());
                                        specificationsNew.setPrice(specificationPrice);
                                        specificationsNew.setType(specifications
                                                .get(a).getType());
                                        specificationsNew.setUniqueId(specifications
                                                .get(a).getUniqueId());
                                    }
                                    specificationPrice = 0;
                                    break;
                                }

                            }

                            if (specificationsNew != null) {
                                specificationListNew.add(specificationsNew);
                            }

                        }
                        cartProductItemsNew.setSpecifications(specificationListNew);
                        cartProductItemsNew.setTotalSpecificationPrice(specificationPriceTotal);
                        itemPriceAndSpecificationPriceTotal =
                                (itemPriceAndSpecificationPriceTotal + specificationPriceTotal) *
                                        cartProductItems.getQuantity();
                        cartProductItemsNew.setTotalItemAndSpecificationPrice
                                (itemPriceAndSpecificationPriceTotal);

// add new filed on 4_Aug_2018

                        cartProductItemsNew.setTotalPrice(cartProductItemsNew.getItemPrice() +
                                cartProductItemsNew
                                        .getTotalSpecificationPrice());
                        cartProductItemsNew.setTax(response.body().isUseItemTax() ?
                                cartProductItems.getProductItem().getTax() : response.body()
                                .getTax());
                        cartProductItemsNew.setItemTax(getTaxableAmount(cartProductItemsNew
                                .getItemPrice(), cartProductItemsNew.getTax()));
                        cartProductItemsNew.setTotalSpecificationTax(getTaxableAmount
                                (cartProductItemsNew.getTotalSpecificationPrice(),
                                        cartProductItemsNew.getTax()));
                        cartProductItemsNew.setTotalTax(cartProductItemsNew.getItemTax() +
                                cartProductItemsNew.getTotalSpecificationTax());
                        cartProductItemsNew.setTotalItemTax(cartProductItemsNew.getTotalTax() *
                                cartProductItemsNew
                                        .getQuantity());

                        cartProductItemsListNew.add(cartProductItemsNew);
                    }
                    products.setItems(cartProductItemsListNew);
                    products.setTotalProductItemPrice(itemPriceAndSpecificationPriceTotal);
                    currentBooking.setSelectedStoreId(cartProducts.getCartProductItemDetail()
                            .getStoreId());
                    currentBooking.setCartProduct(products);
                }
// Utils.showMessageToast(response.body().getMessage(), context);
            }
        } catch (Exception e) {
           /* Log.e("parseCart() " + e.getMessage());*/
        }
    }

    private double getTaxableAmount(double amount, double taxValue) {
        return amount * taxValue * 0.01;
    }

    public boolean parseAppSettingDetail(Response<AppSettingDetailResponse> response) {
        try {
            AppLog.Log("SETTING_DETAIL", ApiClient.JSONResponse(response.body()));
            if (response != null && response.body() != null && isSuccessful(response)) {
                if (response.body().isSuccess()) {
                    preferenceHelper.putIsShowOptionalFieldInRegister(response.body()
                            .isShowOptionalField());
                    preferenceHelper.putIsMailVerification(response.body()
                            .isVerifyEmail());
                    preferenceHelper.putIsSmsVerification(response.body()
                            .isVerifyPhone());
                    preferenceHelper.putIsProfilePictureRequired(response.body()
                            .isProfilePictureRequired());
                    preferenceHelper.putIsAdminDocumentMandatory(response.body()
                            .isUploadDocumentsMandatory());
                    preferenceHelper.putGoogleKey(response.body().getGoogleKey());

                    preferenceHelper.putIsLoginByEmail(response.body().isLoginByEmail());
                    preferenceHelper.putIsLoginByPhone(response.body().isLoginByPhone());

                    preferenceHelper.putAdminContactEmail(response.body().getAdminContactEmail());
                    preferenceHelper.putIsReferralOn(response.body().isUseReferral());
                    preferenceHelper.putIsLoginBySocial(response.body().isLoginBySocial());
                    preferenceHelper.putAdminContact(response.body().getAdminContactPhoneNumber());
                    preferenceHelper.putPolicy(response.body().getPrivacyPolicyUrl());
                    preferenceHelper.putTermsANdConditions(response.body().getTermsAndConditionUrl());
                    return true;
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), context);
                }
            }
        } catch (Exception e) {
            /*Log.e("parseAppSettingDetail() " + e.getMessage());*/
        }
        return false;
    }

    public HashMap<String, String> parsGoogleGeocode(Response<ResponseBody> response) {
        try {
            if (response != null && response.body() != null && isSuccessful(response)) {
                HashMap<String, String> map = new HashMap<>();
                try {
                    String responseGeocode = response.body().string();
                    AppLog.Log("GEOCODE", responseGeocode);
                    JSONObject jsonObject = new JSONObject(responseGeocode);
                    if (jsonObject.getString(Const.google.STATUS).equals(Const.google.OK)) {

                        JSONObject resultObject = jsonObject.getJSONArray(Const.google
                                .RESULTS).getJSONObject(0);

                        JSONArray addressComponent = resultObject.getJSONArray(Const.google
                                .ADDRESS_COMPONENTS);

                        JSONObject geometryObject = resultObject.getJSONObject(Const.google.GEOMETRY);
                        map.put(Const.google.LAT, geometryObject.getJSONObject(Const.google.LOCATION)
                                .getString(Const.google.LAT));
                        map.put(Const.google.LNG, geometryObject.getJSONObject(Const.google.LOCATION)
                                .getString(Const.google.LNG));
                        map.put(Const.google.FORMATTED_ADDRESS, resultObject.getString(Const.google
                                .FORMATTED_ADDRESS));

                        int addressSize = addressComponent.length();
                        for (int i = 0; i < addressSize; i++) {
                            JSONObject address = addressComponent.getJSONObject(i);
                            JSONArray typesArray = address.getJSONArray(Const.google.TYPES);
                            if (Const.google.LOCALITY.equals(typesArray.get(0).toString())) {
                                map.put(Const.google.LOCALITY, address.getString(Const.google
                                        .LONG_NAME));
                            } else if (Const.google.ADMINISTRATIVE_AREA_LEVEL_2.equals(typesArray.get(0)
                                    .toString())) {
                                map.put(Const.google.ADMINISTRATIVE_AREA_LEVEL_2, address.getString
                                        (Const.google.LONG_NAME));
                            } else if (Const.google.ADMINISTRATIVE_AREA_LEVEL_1.equals(typesArray
                                    .get(0).toString())) {
                                map.put(Const.google.ADMINISTRATIVE_AREA_LEVEL_1, address.getString
                                        (Const.google.LONG_NAME));
                                map.put(Const.Params.CITY_CODE, address.getString(Const.google
                                        .SHORT_NAME));
                            } else if (Const.google.COUNTRY.equals(typesArray.get(0).toString())) {
                                map.put(Const.google.COUNTRY, address.getString(Const
                                        .google.LONG_NAME));
                                map.put(Const.google.COUNTRY_CODE, address.getString(Const
                                        .google.SHORT_NAME));
                            }

                        }
                        return map;
                    } else {
                        Utils.hideCustomProgressDialog();
                    }

                } catch (JSONException | IOException e) {
                    AppLog.handleException(TAG, e);
                }
            }
        } catch (Exception e) {
            /*Log.e("parsGoogleGeocode() " + e.getMessage());*/
        }
        return null;
    }


    public HashMap<String, String> parseMapboxAutoPlace(Response<ResponseBody> response) {
        try {
            if (response != null && response.body() != null && isSuccessful(response)) {
                HashMap<String, String> map = new HashMap<>();
                try {
                    String responseGeocode = response.body().string();
                    AppLog.Log("AutoPlaceApi", responseGeocode);
                    JSONObject jsonObject = new JSONObject(responseGeocode);
                    if (jsonObject.getString(Const.google.STATUS).equals(Const.google.OK)) {

                        JSONObject resultObject = jsonObject.getJSONArray(Const.google
                                .RESULTS).getJSONObject(0);

                        JSONArray addressComponent = resultObject.getJSONArray(Const.google
                                .ADDRESS_COMPONENTS);

                        JSONObject geometryObject = resultObject.getJSONObject(Const.google.GEOMETRY);
                        map.put(Const.google.LAT, geometryObject.getJSONObject(Const.google.LOCATION)
                                .getString(Const.google.LAT));
                        map.put(Const.google.LNG, geometryObject.getJSONObject(Const.google.LOCATION)
                                .getString(Const.google.LNG));
                        map.put(Const.google.FORMATTED_ADDRESS, resultObject.getString(Const.google
                                .FORMATTED_ADDRESS));

                        int addressSize = addressComponent.length();
                        for (int i = 0; i < addressSize; i++) {
                            JSONObject address = addressComponent.getJSONObject(i);
                            JSONArray typesArray = address.getJSONArray(Const.google.TYPES);
                            if (Const.google.LOCALITY.equals(typesArray.get(0).toString())) {
                                map.put(Const.google.LOCALITY, address.getString(Const.google
                                        .LONG_NAME));
                            } else if (Const.google.ADMINISTRATIVE_AREA_LEVEL_2.equals(typesArray.get(0)
                                    .toString())) {
                                map.put(Const.google.ADMINISTRATIVE_AREA_LEVEL_2, address.getString
                                        (Const.google.LONG_NAME));
                            } else if (Const.google.ADMINISTRATIVE_AREA_LEVEL_1.equals(typesArray
                                    .get(0).toString())) {
                                map.put(Const.google.ADMINISTRATIVE_AREA_LEVEL_1, address.getString
                                        (Const.google.LONG_NAME));
                                map.put(Const.Params.CITY_CODE, address.getString(Const.google
                                        .SHORT_NAME));
                            } else if (Const.google.COUNTRY.equals(typesArray.get(0).toString())) {
                                map.put(Const.google.COUNTRY, address.getString(Const
                                        .google.LONG_NAME));
                                map.put(Const.google.COUNTRY_CODE, address.getString(Const
                                        .google.SHORT_NAME));
                            }

                        }
                        return map;
                    } else {
                        Utils.hideCustomProgressDialog();
                    }

                } catch (JSONException | IOException e) {
                    AppLog.handleException(TAG, e);
                }
            }
        } catch (Exception e) {
            /*Log.e("parsGoogleGeocode() " + e.getMessage());*/
        }
        return null;
    }


    public HashMap<String, String> parsDistanceMatrix(Response<ResponseBody> response) {
        HashMap<String, String> map = new HashMap<>();
        try {
            if (response != null && response.body() != null && isSuccessful(response)) {
                String destAddress, distance, time, originAddress, text;
                try {
                    String distanceMatrix = response.body().string();
                    AppLog.Log("DISTANCE_MATRIX", distanceMatrix);
                    JSONObject jsonObject = new JSONObject(distanceMatrix);
                    if (jsonObject.getString(Const.google.STATUS).equals(Const.google.OK)) {
                        destAddress = jsonObject.getJSONArray(Const.google
                                .DESTINATION_ADDRESSES).getString(0);
                        originAddress = jsonObject.getJSONArray(Const.google
                                .ORIGIN_ADDRESSES).getString(0);
                        JSONObject rowsJson = jsonObject.getJSONArray(Const.google.ROWS)
                                .getJSONObject(0);
                        JSONObject elementsJson = rowsJson.getJSONArray(Const.google.ELEMENTS)
                                .getJSONObject(0);
                        if (elementsJson.getString(Const.google.STATUS).equals(Const.google.OK)) {
                            distance = elementsJson.getJSONObject(Const.google.DISTANCE)
                                    .getString(Const.google.VALUE);
                            time = elementsJson.getJSONObject(Const.google.DURATION)
                                    .getString(Const.google.VALUE);
                        } else {
                            float distanceFloat = calculateManualDistance();
                            time = String.format("%.0f", calculateManualTime(distanceFloat));
                            distance = String.format("%.0f", distanceFloat);
                        }
                        if (destAddress != null && distance != null && time != null && originAddress != null) {
                            map.put(Const.google.DESTINATION_ADDRESSES, destAddress);
                            map.put(Const.google.DISTANCE, distance);
                            map.put(Const.google.DURATION, time);
                            map.put(Const.google.ORIGIN_ADDRESSES, originAddress);

                        }
                        return map;
                    }
                } catch (JSONException | IOException e) {
                    AppLog.handleException(TAG, e);
                }
            }
        } catch (Exception e) {
            /*Log.e("parsDistanceMatrix() " + e.getMessage());*/
        }
        return map;
    }

    private float calculateManualDistance() {
        CurrentBooking currentBooking = CurrentBooking.getInstance();
        float result[] = new float[1];
        try {
            Location.distanceBetween(currentBooking.getPickupAddresses().get(0).getLocation()
                            .get(0),
                    currentBooking.getPickupAddresses().get(0).getLocation()
                            .get(1), currentBooking.getDeliveryLatLng().latitude,
                    currentBooking.getDeliveryLatLng().longitude, result);
        } catch (Exception e) {
            /*Log.e("calculateManualDistance() " + e.getMessage());*/
        }
        return result[0];
    }

    private double calculateManualTime(float distance) {
        double time = (60 * distance) / 30000;
        time = time * 60;
        return time;
    }

    public boolean isSuccessful(Response<?> response) {
        if (response.isSuccessful()) {

            return true;

        } else {
            Utils.showHttpErrorToast(response.code(), context);
            Utils.hideCustomProgressDialog();
        }
        return false;
    }

}
