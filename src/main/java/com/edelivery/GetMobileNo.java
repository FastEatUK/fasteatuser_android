package com.edelivery;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import com.edelivery.component.CustomFontEditTextView;

import com.edelivery.models.datamodels.ConfirmationData;
import com.edelivery.models.datamodels.Countries;
import com.edelivery.models.datamodels.RegistartionData;
import com.edelivery.models.responsemodels.CountriesResponse;
import com.edelivery.models.responsemodels.UserDataResponse;
import com.edelivery.models.singleton.CurrentBooking;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.LocationHelper;
import com.edelivery.utils.Utils;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetMobileNo extends BaseAppCompatActivity implements LocationHelper
        .OnLocationReceived {

    static RegistartionData registartionData;
    public LocationHelper locationHelper;
    boolean gofav = false;
    public ArrayList<Countries> countryList = new ArrayList<>();
    private String countryId, countryphonecode, cityname, countryname;
    private CustomFontEditTextView etRegisterMobileNumber, etConfMobileNumber;
    private Button SubmitButton;
    private int maxPhoneNumberLength = 15, minPhoneNumberLength = 11;
    static Response<UserDataResponse> userDataResponseResponse;
    AppEventsLogger logger;
    ConfirmationData confirmationData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get);
        locationHelper = new LocationHelper(this);
        locationHelper.setLocationReceivedLister(this);
        confirmationData = new ConfirmationData();
        if (getIntent().getSerializableExtra(Const.Params.Registartion_data) != null) {
            registartionData = (RegistartionData) getIntent().getSerializableExtra(Const.Params.Registartion_data);
            Log.i("registartionData", "name:" + registartionData.getFirstname() + "\n"
                    + "lname:" + registartionData.getLastname());
        }
        findViewById();
        setViewListener();
        getCountries();
        logger = (AppEventsLogger) AppEventsLogger.newLogger(getApplicationContext());
    }

    @Override
    protected boolean isValidate() {
        String msg = null;

        if (TextUtils.isEmpty(etRegisterMobileNumber.getText().toString().trim())) {
            msg = getString(R.string.msg_please_enter_valid_mobile_number);
            etRegisterMobileNumber.setError(msg);
            etRegisterMobileNumber.requestFocus();
        } else if (etRegisterMobileNumber.getText().toString().trim().length()
                > maxPhoneNumberLength || etRegisterMobileNumber.getText().toString().trim().length
                () < minPhoneNumberLength) {
            msg = getString(R.string.msg_please_enter_valid_mobile_number) + " " +
                    "" + minPhoneNumberLength + getString(R.string.text_or)
                    + maxPhoneNumberLength + " " + getString(R.string
                    .text_digits);
            etRegisterMobileNumber.setError(msg);
            etRegisterMobileNumber.requestFocus();
        } else if (TextUtils.isEmpty(etConfMobileNumber.getText().toString().trim())) {
            msg = getString(R.string.msg_please_enter_valid_mobile_number);
            etConfMobileNumber.setError(msg);
            etConfMobileNumber.requestFocus();
        } else if (!TextUtils.equals
                (etRegisterMobileNumber.getText().toString().trim(),
                        etConfMobileNumber.getText().toString().trim())) {
            msg = getString(R.string.text_mobile_not_match);
            etConfMobileNumber.setError(msg);
            etConfMobileNumber.requestFocus();
        }

        return TextUtils.isEmpty(msg);


    }

    @Override
    protected void findViewById() {
        etRegisterMobileNumber = findViewById(R.id.etRegisterMobileNumber);
        etConfMobileNumber = findViewById(R.id.etConfRegisterMobileNumber);
        SubmitButton = findViewById(R.id.btn_submit);
    }

    @Override
    protected void setViewListener() {
        SubmitButton.setOnClickListener(this);
    }

    @Override
    protected void onBackNavigation() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:

                if (isValidate()) {
                    register();
                }
                break;
            default:
                break;
        }
    }

    private void register() {

        HashMap<String, RequestBody> registerMap = new HashMap<>();
        registerMap.put(Const.Params.FIRST_NAME, ApiClient.makeTextRequestBody
                (registartionData.getFirstname()));
        registerMap.put(Const.Params.LAST_NAME, ApiClient.makeTextRequestBody
                (registartionData.getLastname()));
        registerMap.put(Const.Params.EMAIL, ApiClient.makeTextRequestBody
                (registartionData.getEmail()));


        registerMap.put(Const.Params.PHONE, ApiClient.makeTextRequestBody
                (etRegisterMobileNumber.getText().toString()));
        registerMap.put(Const.Params.CITY, ApiClient.makeTextRequestBody
                (cityname));
        registerMap.put(Const.Params.COUNTRY_name, ApiClient.makeTextRequestBody
                (countryname));
        registerMap.put(Const.Params.COUNTRY_ID, ApiClient.makeTextRequestBody
                (countryId));
        registerMap.put(Const.Params.DEVICE_TOKEN, ApiClient.makeTextRequestBody
                (preferenceHelper.getDeviceToken()));
        registerMap.put(Const.Params.COUNTRY_PHONE_CODE, ApiClient.makeTextRequestBody
                (countryphonecode));
      /*  registerMap.put(Const.Params.REFERRAL_CODE, ApiClient.makeTextRequestBody
                (referralCode));*/
       /* registerMap.put(Const.Params.APP_VERSION, ApiClient.makeTextRequestBody
                (getAppVersion()));*/
       /* registerMap.put(Const.Params.IS_PHONE_NUMBER_VERIFIED, ApiClient.makeTextRequestBody
                (loginActivity.preferenceHelper.getIsPhoneNumberVerified()));
        registerMap.put(Const.Params.IS_EMAIL_VERIFIED, ApiClient.makeTextRequestBody
                (loginActivity.preferenceHelper.getIsEmailVerified()));*/
        registerMap.put(Const.Params.DEVICE_TYPE, ApiClient.makeTextRequestBody
                (Const.ANDROID));

        registerMap.put(Const.Params.REFERRAL_CODE, ApiClient.makeTextRequestBody(registartionData.getReferalCode()));

        registerMap.put(Const.Params.CART_UNIQUE_TOKEN, ApiClient.makeTextRequestBody(preferenceHelper.getAndroidId()));
        if (TextUtils.isEmpty(registartionData.getSocialId())) {
            registerMap.put(Const.Params.PASS_WORD, ApiClient.makeTextRequestBody
                    (registartionData.getPassword()));
            registerMap.put(Const.Params.LOGIN_BY, ApiClient.makeTextRequestBody(Const.MANUAL));
        } else {
            registerMap.put(Const.Params.SOCIAL_ID, ApiClient.makeTextRequestBody(registartionData.getSocialId()));
            registerMap.put(Const.Params.PASS_WORD, ApiClient.makeTextRequestBody
                    (""));
            registerMap.put(Const.Params.LOGIN_BY, ApiClient.makeTextRequestBody(Const.SOCIAL));
        }


        Utils.showCustomProgressDialog(GetMobileNo.this, false);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserDataResponse> register = null;
        if (TextUtils.isEmpty(registartionData.getSocialId()) && registartionData.getImage() != null) {
            register = apiInterface.register(ApiClient.makeMultipartRequestBodySocial(
                    GetMobileNo.this, getImageFile(registartionData.getImage()), Const.Params
                            .IMAGE_URL), registerMap);
        } else {
            register = apiInterface.register(null, registerMap);
        }


        Log.i("register_data", "first_name:" + registerMap.get(Const.Params.FIRST_NAME) + "last_name:" + registerMap.get(Const.Params.LAST_NAME) +
                "email:" + registerMap.get(Const.Params.EMAIL) + "country_phone_code:" + registerMap.get(Const.Params.COUNTRY_PHONE_CODE) +
                "phone:" + registerMap.get(Const.Params.PHONE) + "password:" + registerMap.get(Const.Params.PASS_WORD) + "country_id" +
                registerMap.get(Const.Params.COUNTRY_ID) + "city:" + registerMap.get(Const.Params.CITY));

        Log.i("Registration_devicetoke", preferenceHelper.getDeviceToken());


        register.enqueue(new Callback<UserDataResponse>() {
            @Override
            public void onResponse(Call<UserDataResponse> call, Response<UserDataResponse>
                    response) {

                AppLog.Log("register_responce", ApiClient.JSONResponse(response.body()));
                Utils.hideCustomProgressDialog();
                if (parseContent.isSuccessful(response)) {


                    if (response != null && response.body() != null && response.body().getUser() != null && response.body().isSuccess() && preferenceHelper != null) {

                        userDataResponseResponse = response;
                     /*   Utils.showMessageToast(response.body().getMessage(),GetMobileNo.this);
                        CurrentBooking.getInstance().setBookCityId("");
                        if (getCallingActivity() == null) {
                          //  goToHomeActivity();
                            Log.i("FBA_TYPE",FBA_TYPE);
                            if (!FBA_TYPE.isEmpty())
                            {
                                logCompleteRegistrationEvent(FBA_TYPE);
                            }
                           // goToFavouriteAddressActivity(true);
                        } else {
                            //setResult(Activity.RESULT_OK);
                           // finish();
                        }*/
                        if (response.body().getUser().isIsPhoneNumberVerified()) {
                            if (parseContent.parseUserStorageData(response)) {
                                Utils.showMessageToast(response.body().getMessage(), GetMobileNo.this);
                                CurrentBooking.getInstance().setBookCityId("");


                                if (getCallingActivity() == null) {
                                    goToHomeActivity();
                                    if (!TextUtils.isEmpty(confirmationData.getSocialtype())) {
                                        Log.i("FBA_TYPE", confirmationData.getSocialtype());
                                        logCompleteRegistrationEvent(confirmationData.getSocialtype());
                                        gofav = true;
                                    }
                                    if (gofav) {
                                        goToFavouriteAddressActivity(true,true);
                                    }

                                } else {
                                    Log.i("LoginActivityget", "from checkout going back");

                                    setResult(Activity.RESULT_OK);
                                    finish();
                                }


                                //

                            } else {
                                preferenceHelper.clearVerification();
                            }
                        } else if (!response.body().getUser().isIsPhoneNumberVerified()) {
                            GoforConfirmOtp(response.body().getUser().getOtp(), response.body().getUser().getId(), response.body().getUser().getPhone(), response.body().getUser().getCountryPhoneCode());
                        }


                    } else {
                        if (response != null && response.body() != null) {
                            Utils.showErrorToast(response.body().getErrorCode(), GetMobileNo.this);
                        }

                    }


                } else {
                    preferenceHelper.clearVerification();
                }

            }

            @Override
            public void onFailure(Call<UserDataResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
            }
        });


    }

    private File getImageFile(Bitmap bitmap) {

        File imageFile = new File(getFilesDir(), "name.jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            AppLog.handleException(getClass().getSimpleName(), e);
        }
        return imageFile;
    }

    private void getCountries() {
        Utils.showCustomProgressDialog(GetMobileNo.this, false);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CountriesResponse> countriesItemCall = apiInterface.getCountries();
        countriesItemCall.enqueue(new Callback<CountriesResponse>() {
            @Override
            public void onResponse(Call<CountriesResponse> call, Response<CountriesResponse>
                    response) {
                Utils.hideCustomProgressDialog();
                countryList = parseContent.parseCountries(response);
                locationHelper.getLastLocation(GetMobileNo.this, new
                        OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                if (location != null) {
                                    new GetCityAsyncTask(location.getLatitude(), location
                                            .getLongitude()).execute();
                                } else {
                                    setCountry(0);
                                }
                            }
                        });
            }

            @Override
            public void onFailure(Call<CountriesResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.REGISTER_FRAGMENT, t);
            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (locationHelper != null) {
            locationHelper.onStart();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (locationHelper != null) {
            locationHelper.onStop();
        }
    }

    private class GetCityAsyncTask extends AsyncTask<String, Void, Address> {

        double lat, lng;

        public GetCityAsyncTask(double lat, double lng) {
            this.lat = lat;
            this.lng = lng;
        }


        @Override
        protected Address doInBackground(String... params) {

            Geocoder geocoder = new Geocoder(GetMobileNo.this, new Locale("en_US"));
            try {
                List<Address> addressList = geocoder.getFromLocation(lat,
                        lng, 1);
                if (addressList != null && !addressList.isEmpty()) {

                    Address address = addressList.get(0);
                    return address;
                }

            } catch (IOException e) {
                AppLog.handleException("GetMobileNo", e);
                publishProgress();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            setCountry(0);
        }

        @Override
        protected void onPostExecute(Address address) {
            String countryName = "", cityName = "";
            if (address != null) {
                countryName = address.getCountryName();
                checkCountryCode(countryName);
                if (address.getLocality() != null) {
                    cityName = address.getLocality();
                } else if (address.getSubAdminArea() != null) {
                    cityName = address.getSubAdminArea();
                } else {
                    cityName = address.getAdminArea();
                }
                cityname = cityName;
                countryname = countryName;
                AppLog.Log("GetMobileNo", "countryName= " + countryName);
                AppLog.Log("GetMobileNo", "cityName= " + cityName);
            }


        }
    }

    private void checkCountryCode(String country) {
        int countryListSize = countryList.size();
        for (int i = 0; i < countryListSize; i++) {
            if (countryList.get(i).getCountryName().toUpperCase().startsWith(country.toUpperCase
                    ())) {
                setCountry(i);
                Log.i("checkCountryName", country);


                return;
            }
        }
        setCountry(0);

    }

    private void setCountry(int position) {
        if (!countryList.isEmpty() && !TextUtils.equals(countryId, countryList.get(position)
                .getId())) {
            countryId = countryList.get(position).getId();
//            getCities(countryId);
            countryphonecode = countryList.get(position).getCountryPhoneCode();
            maxPhoneNumberLength = countryList.get(position).getMaxPhoneNumberLength();
            minPhoneNumberLength = countryList.get(position).getMinPhoneNumberLength();
            setContactNoLength(maxPhoneNumberLength);

        }

    }

    private void setContactNoLength(int length) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        etRegisterMobileNumber.setFilters(FilterArray);
    }

    private void GoforConfirmOtp(String otp, String user_id, String phoneno, String countryPhoneCode) {
        confirmationData.setOtp(otp);
        confirmationData.setUser_id(user_id);
        confirmationData.setMobileNo(phoneno);
        confirmationData.setContrycode(countryPhoneCode);
        confirmationData.setSocialtype(registartionData.getSocialType());

        //  Utils.showToast(userDataResponseResponse.body().getUser().getOtp(),GetMobileNo.this);
        Intent intent = new Intent(GetMobileNo.this, ConfirmOnetimePassword.class);
        // intent.putExtra(Const.Params.Otp_data,otp);
        intent.putExtra(Const.Params.Otp_data, confirmationData);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Const.GOFOR_CONFIRM_OTP) {
            if (resultCode == Activity.RESULT_OK) {

              /*  if (parseContent.isSuccessful(userDataResponseResponse)) {
                    Log.i("GOFOR_CONFIRM_OTP", "ok" + "phone:" + userDataResponseResponse.body().getUser().getPhone());


                    if (parseContent.parseUserStorageData(userDataResponseResponse)) {
                        Utils.showMessageToast(userDataResponseResponse.body().getMessage(), GetMobileNo.this);
                        CurrentBooking.getInstance().setBookCityId("");
                        if (getCallingActivity() == null) {
                            goToHomeActivity();

                            if (TextUtils.isEmpty(registartionData.getSocialType())) {
                                Log.i("FBA_TYPE", registartionData.getSocialType());
                                logCompleteRegistrationEvent(registartionData.getSocialType());
                            }

                            goToFavouriteAddressActivity(true);
                        } else {
                            setResult(Activity.RESULT_OK);
                            finish();
                        }
                    } else {
                        preferenceHelper.clearVerification();
                    }
                }*/
            } else if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    public void logCompleteRegistrationEvent(String registrationMethod) {
        Bundle params = new Bundle();
        params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, registrationMethod);
        logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);
    }
}
