package com.edelivery;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.edelivery.component.CustomFontButton;
import com.edelivery.component.CustomFontTextView;
import com.edelivery.models.responsemodels.CardsResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.Stripe;
import com.stripe.android.model.Token;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.stripe.android.model.Card.CardBrand.DISCOVER;
import static com.stripe.android.model.Card.CardBrand.JCB;
import static com.stripe.android.model.Card.CardBrand.MASTERCARD;
import static com.stripe.android.model.Card.CardBrand.UNKNOWN;
import static com.stripe.android.model.Card.CardBrand.VISA;
import static com.stripe.android.model.PaymentMethod.Card.Brand.AMERICAN_EXPRESS;
import static com.stripe.android.model.PaymentMethod.Card.Brand.DINERS_CLUB;

public class AddStripeCardActivity extends BaseAppCompatActivity {
    EditText editText_cardnumbur,editText_expirydate,editText_cvv,editText_cardholdername;
    CustomFontButton btn_addcard;
    private static final Pattern CODE_PATTERN = Pattern
            .compile("([0-9]{0,4})|([0-9]{4}-)+|([0-9]{4}-[0-9]{0,4})+");
    private static final Pattern CODE_PATTERN_Date = Pattern
            .compile("(0[1-9]|1[0-2])[0-9]{2}");

    private String cardType;
    private String current_year;
    private CustomFontTextView card_name,card_expiry,card_lastfour;
    private ImageView card_image;
    //String stripeKey="pk_test_XwYfEHXj88Z7IhdTZSgUfOtx";
    String stripeKey="pk_live_LHfXVcvWm8klCp4dEjGJw1LT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stripe_card);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        setTitleOnToolBar(getResources().getString(R.string.text_add_card));
        findViewById();

        DateFormat df = new SimpleDateFormat("yy"); // Just the year, with 2 digits
        current_year = df.format(Calendar.getInstance().getTime());

        editText_cardnumbur.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                cardType = getCreditCardType(s.toString());


                if (editText_cardnumbur.getText().toString().length() == 19) {
                    editText_expirydate.requestFocus();
                }
                if (editText_cardnumbur.getText().toString().length()>15)
                {
                    int length=editText_cardnumbur.getText().toString().length();
                    card_lastfour.setText(editText_cardnumbur.getText().toString().substring(15,length));
                }
                if (editText_cardnumbur.getText().toString().length()==0)
                {
                    card_lastfour.setText("****");
                }
                if (!TextUtils.isEmpty(cardType))
                {
                    Log.i("editText_cardnumbur",cardType);
                    setCard_image();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // do with text
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0 && !CODE_PATTERN.matcher(s).matches()) {
                    String input = s.toString();
                    String numbersOnly = keepNumbersOnly(input);
                    String code = formatNumbersAsCode(numbersOnly);
                    editText_cardnumbur.removeTextChangedListener(this);
                    editText_cardnumbur.setText(code);
                    editText_cardnumbur.setSelection(code.length());
                    editText_cardnumbur.addTextChangedListener(this);
                }
            }

            private String keepNumbersOnly(CharSequence s) {
                return s.toString().replaceAll("[^0-9]", ""); // Should of
                // course be
                // more robust
            }

            private String formatNumbersAsCode(CharSequence s) {
                int groupDigits = 0;
                String tmp = "";
                int sSize = s.length();
                for (int i = 0; i < sSize; ++i) {
                    tmp += s.charAt(i);
                    ++groupDigits;
                    if (groupDigits == 4) {
                        tmp += "-";
                        groupDigits = 0;
                    }
                }
                return tmp;
            }
        });

        editText_expirydate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String current = s.toString();
                if (current.length() == 2 && start == 1) {
                    editText_expirydate.setText(current + "/");
                    editText_expirydate.setSelection(current.length() + 1);
                   /* int lenght=current.length();
                    card_expiry.setText(editText_expirydate.getText().toString().substring(0,lenght));*/
                }
                else if (current.length() == 2 && before == 1) {
                    current = current.substring(0, 1);
                    editText_expirydate.setText(current);
                    editText_expirydate.setSelection(current.length());
                  /*  int lenght=current.length();
                    card_expiry.setText(editText_expirydate.getText().toString().substring(0,lenght));*/
                }

                else if (current.length()>0 )
                {
                    int lenght=current.length();
                    card_expiry.setText(editText_expirydate.getText().toString().substring(0,lenght));
                }
                else if (current.length()==0)
                {
                    card_expiry.setText("****");
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

               if (editText_expirydate.getText().toString().length() == 5) {
                    editText_cvv.requestFocus();
                }

                }
        });

    editText_cvv.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (editText_cvv.getText().toString().length() == 4) {
                editText_cardholdername.requestFocus();
            }
        }
    });

    editText_cardholdername.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String cureent=s.toString();
            if (cureent.length()>0)
            {
                int length=cureent.length();
                card_name.setText(editText_cardholdername.getText().toString().substring(0,length));
            }

            else if (cureent.length()==0)
            {
                card_name.setText("*********");
            }

        }
    });

    }

    @Override
    protected boolean isValidate() {
        String msg = null;
        String month="";
        String enterd_year="";
        if (!editText_expirydate.getText().toString().isEmpty() && editText_expirydate.getText().toString().length()>=3){

            month=editText_expirydate.getText().toString().substring(0,editText_expirydate.getText().toString().length()-3);
            if (editText_expirydate.getText().toString().length()==4)
            {
                enterd_year=editText_expirydate.getText().toString().substring(editText_expirydate.getText().toString().length()-1,editText_expirydate.getText().toString().length());
            }
            else if (editText_expirydate.getText().toString().length()==5)
            {
                enterd_year=editText_expirydate.getText().toString().substring(editText_expirydate.getText().toString().length()-2,editText_expirydate.getText().toString().length());
            }

            Log.i("isValidate()","month:"+month);
            Log.i("isValidate()","year:"+ enterd_year);
            if (TextUtils.isEmpty(month))
            {
                msg = getString(R.string.msg_enter_month);
            }
             if (TextUtils.isEmpty(enterd_year))
            {
                msg = getString(R.string.msg_enter_year);
            }
        }


        else
        {
            msg = getString(R.string.msg_enter_month);
            editText_expirydate.requestFocus();
        }

if (msg==null)
{


        if (editText_cardnumbur.getText().toString().isEmpty() )
        {
            msg = getString(R.string.msg_enter_card_number);
            editText_cardnumbur.requestFocus();
        }
        else if (editText_expirydate.getText().toString().isEmpty() )
        {
            msg=getString(R.string.msg_enter_card_expiry);
            editText_expirydate.requestFocus();
        }


        else if (Integer.valueOf(month) > 12 )
        {
            msg = getString(R.string.msg_enter_valid_month);
            editText_expirydate.requestFocus();
        }
        else  if(Integer.valueOf(enterd_year) <= Integer.valueOf(current_year) ) {
            msg = getString(R.string.msg_enter_valid_year);
            editText_expirydate.requestFocus();
        }
        else if (editText_cvv.getText().toString().isEmpty())
        {
            msg = getString(R.string.msg_enter_cvv);
            editText_cvv.requestFocus();
        }
        else if (editText_cardholdername.getText().toString().isEmpty())
        {
            msg = getString(R.string.msg_enter_holer_name);
            editText_cardholdername.requestFocus();
        }

}
        if (msg != null) {
            Utils.showToast(msg, AddStripeCardActivity.this);
            return false;
        }


        return true;
    }

    @Override
    protected void findViewById() {
            card_name=findViewById(R.id.tv_cardname);
            card_lastfour=findViewById(R.id.tv_lastfourcardno);
            card_expiry=findViewById(R.id.tv_carddate);
            card_image=findViewById(R.id.iv_cardtype);
            editText_cardnumbur=findViewById(R.id.etCreditCardNumber);
            editText_expirydate=findViewById(R.id.et_expiry);
            editText_cvv=findViewById(R.id.etCvv);
            editText_cardholdername=findViewById(R.id.etCreditCardHolder);
            btn_addcard=findViewById(R.id.addcard);
            btn_addcard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isValidate())
                    {
                        getTokenFromCreditCard();
                    }
                }
            });
    }

    @Override
    protected void setViewListener() {
            btn_addcard.setOnClickListener(this);

    }

    @Override
    protected void onBackNavigation() {
            onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.addcard:


                break;

            default:
                break;
        }
    }
    private String getCreditCardType(String number) {
        if (!Utils.isBlank(number)) {
            if (Utils.hasAnyPrefix(number, com.stripe.android.model.Card
                    .PREFIXES_AMERICAN_EXPRESS)) {
                return AMERICAN_EXPRESS;
            } else if (Utils.hasAnyPrefix(number, com.stripe.android.model.Card
                    .PREFIXES_DISCOVER)) {
                return DISCOVER;
            } else if (Utils.hasAnyPrefix(number, com.stripe.android.model.Card.PREFIXES_JCB)) {
                return JCB;
            } else if (Utils.hasAnyPrefix(number, com.stripe.android.model.Card
                    .PREFIXES_DINERS_CLUB)) {
                return DINERS_CLUB;
            } else if (Utils.hasAnyPrefix(number, com.stripe.android.model.Card.PREFIXES_VISA)) {
                return VISA;
            } else if (Utils.hasAnyPrefix(number, com.stripe.android.model.Card
                    .PREFIXES_MASTERCARD)) {
                return MASTERCARD;
            } else {
                return UNKNOWN;
            }
        }
        return UNKNOWN;

    }

    private void getTokenFromCreditCard() {

        Utils.showCustomProgressDialog(AddStripeCardActivity.this, false);

        com.stripe.android.model.Card card = new com.stripe.android.model.Card
                .Builder(editText_cardnumbur.getText().toString(),
                Integer.parseInt(editText_expirydate.getText().toString().substring(0,editText_expirydate.getText().toString().length()-3)),
                Integer.parseInt(editText_expirydate.getText().toString().substring(editText_expirydate.getText().toString().length()-2,editText_expirydate.getText().toString().length())),
                editText_cvv.getText().toString())
                .build();
        if (card.validateCard()) {
           // if (StripePaymentActivity.gatewayItem != null && !TextUtils.isEmpty(StripePaymentActivity.gatewayItem.getPaymentKeyId())) {
               /* String stripeKey = StripePaymentActivity.gatewayItem.getPaymentKeyId();*/
//            String stripeKey = "pk_test_XwYfEHXj88Z7IhdTZSgUfOtx";
                AppLog.Log("STRIPE_KEY", stripeKey);

                if (getApplicationContext()!=null) {

                    new Stripe(AddStripeCardActivity.this,stripeKey).createToken(card, new ApiResultCallback<Token>() {
                        @Override
                        public void onSuccess(@NonNull Token result) {
                            String expDate = result.getCard().getExpMonth() + "," + result.getCard().getExpYear();
                            Log.e("tagCard", "etCreditCardNum.getText().toString() is " + expDate);
                            addCreditCard(result.getId(), editText_cardnumbur.getText().toString(), result.getCard().getLast4(), cardType,
                                    Const.Payment.STRIPE, editText_cvv.getText().toString(),expDate);
                        }


                        @Override
                        public void onError(@NonNull Exception e) {
                            AppLog.Log("STRIPE_KEY", e.toString());
                            Utils.showToast(getResources().getString(R.string.msg_card_invalid),AddStripeCardActivity.this);
                                Utils.hideCustomProgressDialog();
                        }
                    });


                }
                else {
                    Utils.showToast("aymentActivity.isFinishing()", AddStripeCardActivity.this);
                    Utils.hideCustomProgressDialog();
                }
          //  }
           /* else {
                Utils.showToast(getString(R.string.msg_number_payment_gateway), AddStripeCardActivity.this);
                Utils.hideCustomProgressDialog();
            }*/

        } else if (!card.validateNumber()) {
            // handleError("The card number that you entered is invalid");
            Utils.showToast(getString(R.string.msg_number_invalid), AddStripeCardActivity.this);
            Utils.hideCustomProgressDialog();
        } else if (!card.validateExpiryDate()) {
            // handleError("");
            Utils.showToast(getString(R.string.msg_invalid_month_or_year), AddStripeCardActivity.this);
            Utils.hideCustomProgressDialog();
        } else if (!card.validateCVC()) {
            // handleError("");
            Utils.showToast(getString(R.string.msg_cvc_invalid), AddStripeCardActivity.this);
            Utils.hideCustomProgressDialog();

        } else {
            // handleError("");
            Utils.showToast(getString(R.string.msg_card_invalid), AddStripeCardActivity.this);
            Utils.hideCustomProgressDialog();
        }
    }
    private void addCreditCard(String paymentToken, String fullNumber, String lastFourDigits, String cardType, String
            paymentId,String cvv ,String expDate) {
        Utils.showCustomProgressDialog(AddStripeCardActivity.this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.SERVER_TOKEN, this.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.USER_ID, this
                    .preferenceHelper
                    .getUserId());
            jsonObject.put(Const.Params.PAYMENT_TOKEN, paymentToken);
            jsonObject.put(Const.Params.CARD_NUMBER, fullNumber);
            jsonObject.put(Const.Params.LAST_FOUR, lastFourDigits);
            jsonObject.put(Const.Params.CARD_TYPE, cardType);
            jsonObject.put(Const.Params.PAYMENT_ID, paymentId);
            jsonObject.put(Const.Params.CARD_CVV,cvv);
            jsonObject.put(Const.Params.CARD_EXPIRY_DATE, expDate);
           // jsonObject.put(Const.Params.TEST, Const.Params.version_value);
            jsonObject.put(Const.Params.CARD_HOLDER_NAME, editText_cardholdername.getText().toString());
            jsonObject.put(Const.Params.TYPE, Const.Type.USER);
        } catch (JSONException e) {
            AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, e);
        }
        Log.i("addCreditCard",jsonObject.toString());


        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CardsResponse> responseCall = apiInterface.getAddCreditCard(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<CardsResponse>() {
            @Override
            public void onResponse(Call<CardsResponse> call, Response<CardsResponse> response) {

                AppLog.Log("addCreditCard", ApiClient.JSONResponse(response.body()));
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {

                            setResult(Activity.RESULT_OK);
                            finish();
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(), AddStripeCardActivity.this);
                }
            }

            @Override
            public void onFailure(Call<CardsResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(Const.Tag.PAYMENT_ACTIVITY, t);
            }
        });
    }

    public void setCard_image(){
        if (cardType.matches(Const.cardbrand.VISA))
        {
            Drawable res=getResources().getDrawable(R.drawable.visa);
            card_image.setImageDrawable(res);
        }
        else if (cardType.matches(Const.cardbrand.AMERICAN_EXPRESS))
        {
            Drawable res=getResources().getDrawable(R.drawable.amex);
            card_image.setImageDrawable(res);
        }
        else if (cardType.matches(Const.cardbrand.DINERS_CLUB))
        {
            Drawable res=getResources().getDrawable(R.drawable.dinersclub);
            card_image.setImageDrawable(res);
        }
        else if (cardType.matches(Const.cardbrand.DISCOVER))
        {
            Drawable res=getResources().getDrawable(R.drawable.discover);
            card_image.setImageDrawable(res);
        }
        else if (cardType.matches(Const.cardbrand.MASTERCARD))
        {
            Drawable res=getResources().getDrawable(R.drawable.mastercard);
            card_image.setImageDrawable(res);
        }
        else if (cardType.matches(Const.cardbrand.JCB))
        {
            card_image.setImageDrawable(null);
        }
        else if (cardType.matches(Const.cardbrand.UNIONPAY))
        {
            card_image.setImageDrawable(null);
        }
        else if (cardType.matches(Const.cardbrand.UNKNOWN))
        {
            card_image.setImageDrawable(null);
        }
    }
}
