package com.edelivery;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;

import android.view.View;

import com.edelivery.adapter.ViewPagerAdapter;
import com.edelivery.fragments.CartHistoryFragment;
import com.edelivery.fragments.HistoryDetailFragment;
import com.edelivery.fragments.HistoryFragment;
import com.edelivery.fragments.HistoryInvoiceFragment;
import com.edelivery.models.responsemodels.OrderHistoryDetailResponse;
import com.edelivery.parser.ApiClient;
import com.edelivery.parser.ApiInterface;
import com.edelivery.utils.AppLog;
import com.edelivery.utils.Const;
import com.edelivery.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryDetailActivity extends BaseAppCompatActivity {

    public ViewPagerAdapter adapter;
    public OrderHistoryDetailResponse detailResponse;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_detail);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initToolBar();
        findViewById();
        setViewListener();
        setToolbarRightIcon3(R.drawable.ic_icon_info, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadExtraData();
    }

    @Override
    protected boolean isValidate() {
        return false;
        //do something
    }

    @Override
    protected void findViewById() {
        //do something

        tabLayout = (TabLayout) findViewById(R.id.historyTabsLayout);
        viewPager = (ViewPager) findViewById(R.id.historyViewpager);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(getResources().getDimensionPixelSize(R.dimen
                    .dimen_app_tab_elevation));
        }
    }

    @Override
    protected void setViewListener() {
        //do something


    }

    @Override
    protected void onBackNavigation() {
        //do something
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        //do something
    }


    private void loadExtraData() {
        if (getIntent().getExtras() != null) {
            getOrderHistoryDetail(getIntent().getExtras().getString(Const.Params.ORDER_ID));
        }
    }

    /**
     * this method called a webservice for get order history detail
     *
     * @param orderId order id in string
     */
    private void getOrderHistoryDetail(final String orderId) {
        Utils.showCustomProgressDialog(this, false);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.ORDER_ID, orderId);
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getUserId());
            jsonObject.put(Const.Params.SERVER_TOKEN, preferenceHelper
                    .getSessionToken());
        } catch (JSONException e) {
            AppLog.handleException(HistoryFragment.class.getName(), e);
        }
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderHistoryDetailResponse> responseCall = apiInterface.getOrderHistoryDetail(ApiClient
                .makeJSONRequestBody(jsonObject));
        responseCall.enqueue(new Callback<OrderHistoryDetailResponse>() {
            @Override
            public void onResponse(Call<OrderHistoryDetailResponse> call,
                                   Response<OrderHistoryDetailResponse> response) {
                Utils.hideCustomProgressDialog();
                if (response != null && response.body() != null && parseContent.isSuccessful(response) && response.body().isSuccess()) {
                    detailResponse = response.body();
                    initTabLayout(viewPager);
                    setTitleOnToolBar(getResources().getString(R.string.text_order_number)
                            + " #" + detailResponse.getOrderHistoryDetail().getUniqueId());
                } else {
                    Utils.showErrorToast(response.body().getErrorCode(),
                            HistoryDetailActivity.this);
                }
            }

            @Override
            public void onFailure(Call<OrderHistoryDetailResponse> call, Throwable t) {
                Utils.hideCustomProgressDialog();
                AppLog.handleThrowable(HistoryFragment.class.getName(), t);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void initTabLayout(ViewPager viewPager) {
        if (adapter == null) {
            adapter = new ViewPagerAdapter(getSupportFragmentManager());
            adapter.addFragment(new HistoryDetailFragment(), getString(R.string
                    .text_other_detail));
            adapter.addFragment(new HistoryInvoiceFragment(), getString(R.string.text_invoice));
            adapter.addFragment(new CartHistoryFragment(), getString(R.string.text_reorder));
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        }
    }


}
